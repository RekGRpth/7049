.class public Lcom/google/googlenav/ui/aU;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lam/d;


# direct methods
.method public static a(Lam/e;I[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/16 v3, 0xf

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/google/googlenav/ui/aU;->a:Lam/d;

    if-nez v0, :cond_0

    const/high16 v0, -0x10000

    invoke-static {v5, v4, v0}, Lcom/google/googlenav/G;->a(IZI)Lam/d;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/aU;->a:Lam/d;

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aU;->a:Lam/d;

    invoke-interface {p0, v0}, Lam/e;->a(Lam/d;)V

    sget-object v0, Lcom/google/googlenav/ui/aU;->a:Lam/d;

    invoke-interface {v0}, Lam/d;->a()I

    move-result v7

    array-length v1, p2

    move v0, v4

    move v6, v4

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    invoke-interface {p0, v2, v3, v6}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v6, v7

    goto :goto_0

    :cond_1
    array-length v1, p3

    move v0, v4

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v2, p3, v0

    invoke-interface {p0, v2, v3, v6}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v6, v7

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v0

    add-int/lit8 v0, v0, -0x19

    const v1, 0x7fffffff

    sget-object v2, Lcom/google/googlenav/ui/aU;->a:Lam/d;

    move-object v3, p4

    invoke-static/range {v0 .. v5}, Lam/r;->a(IILam/d;Ljava/lang/String;IZ)[Ljava/lang/String;

    move-result-object v2

    sub-int v0, p1, v6

    div-int/2addr v0, v7

    add-int/lit8 v0, v0, -0x1

    array-length v1, v2

    sub-int v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v6

    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_3

    rem-int/lit8 v3, v0, 0x64

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {p0, v3, v4, v1}, Lam/e;->a(Ljava/lang/String;II)V

    aget-object v3, v2, v0

    const/16 v4, 0x19

    invoke-interface {p0, v3, v4, v1}, Lam/e;->a(Ljava/lang/String;II)V

    add-int/2addr v1, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method
