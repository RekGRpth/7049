.class public Lcom/google/googlenav/ui/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/ak;

.field private final b:Lcom/google/googlenav/ui/wizard/jv;

.field private final c:Lcom/google/googlenav/aA;

.field private volatile d:Ljava/util/List;

.field private volatile e:Z

.field private volatile f:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/aA;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ac;->f:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/ac;->a:Lcom/google/googlenav/ui/ak;

    iput-object p2, p0, Lcom/google/googlenav/ui/ac;->b:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p3, p0, Lcom/google/googlenav/ui/ac;->c:Lcom/google/googlenav/aA;

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/ad;

    const-string v2, "LBS_CRIPPLED_PREF"

    invoke-direct {v1, p0, v2, p1}, Lcom/google/googlenav/ui/ad;-><init>(Lcom/google/googlenav/ui/ac;Ljava/lang/String;Lcom/google/googlenav/ui/ak;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/ae;

    const-string v2, "WIFI_CRIPPLED_PREF"

    invoke-direct {v1, p0, v2, p1}, Lcom/google/googlenav/ui/ae;-><init>(Lcom/google/googlenav/ui/ac;Ljava/lang/String;Lcom/google/googlenav/ui/ak;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v1, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aj;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ac;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ac;->e:Z

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ac;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/ac;->f:Z

    return p1
.end method

.method private b()V
    .locals 3

    iget-object v1, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aj;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ac;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/ac;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ac;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/ac;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 5

    if-eqz p1, :cond_0

    const-string v0, "c"

    move-object v1, v0

    :goto_0
    const/16 v0, 0x5f

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x244

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aj;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/aj;->a(Ljava/lang/StringBuilder;)V

    goto :goto_1

    :cond_0
    const-string v0, "s"

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/ac;->b:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/ah;

    invoke-direct {v4, p0, v2, p1, v1}, Lcom/google/googlenav/ui/ah;-><init>(Lcom/google/googlenav/ui/ac;Ljava/lang/StringBuilder;ZLjava/lang/String;)V

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/ac;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/ac;->b()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/ac;)Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->a:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/ac;)Lcom/google/googlenav/aA;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->c:Lcom/google/googlenav/aA;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/ac;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->b:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/ac;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ac;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/google/googlenav/ui/ac;->e:Z

    const-string v0, "LBS_GO_AWAY_AND_DONT_COME_BACK"

    const/4 v1, -0x1

    new-instance v2, Lcom/google/googlenav/ui/af;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/af;-><init>(Lcom/google/googlenav/ui/ac;)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    return-void
.end method
