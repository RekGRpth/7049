.class public abstract Lcom/google/googlenav/ui/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/friend/bb;


# instance fields
.field private final a:Ljava/lang/String;

.field private volatile b:I

.field final synthetic c:Lcom/google/googlenav/ui/ac;

.field private volatile d:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/ac;Ljava/lang/String;)V
    .locals 1

    const/4 v0, -0x2

    iput-object p1, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/googlenav/ui/aj;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/aj;->d:I

    iput-object p2, p0, Lcom/google/googlenav/ui/aj;->a:Ljava/lang/String;

    return-void
.end method

.method private e()V
    .locals 6

    const/4 v4, -0x2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v0}, Lcom/google/googlenav/ui/ac;->g(Lcom/google/googlenav/ui/ac;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aj;

    iget v3, v0, Lcom/google/googlenav/ui/aj;->b:I

    if-eq v3, v4, :cond_1

    iget v0, v0, Lcom/google/googlenav/ui/aj;->d:I

    if-ne v0, v4, :cond_0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ac;->a(Lcom/google/googlenav/ui/ac;Z)Z

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v1}, Lcom/google/googlenav/ui/ac;->g(Lcom/google/googlenav/ui/ac;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aj;

    iget v4, v0, Lcom/google/googlenav/ui/aj;->b:I

    iget v5, v0, Lcom/google/googlenav/ui/aj;->d:I

    if-eq v4, v5, :cond_6

    iget v4, v0, Lcom/google/googlenav/ui/aj;->b:I

    if-ne v4, v2, :cond_3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aj;->d()V

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v0}, Lcom/google/googlenav/ui/ac;->a(Lcom/google/googlenav/ui/ac;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    iget-object v1, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v1}, Lcom/google/googlenav/ui/ac;->a(Lcom/google/googlenav/ui/ac;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ac;->b(Lcom/google/googlenav/ui/ac;Z)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->c:Lcom/google/googlenav/ui/ac;

    invoke-static {v0}, Lcom/google/googlenav/ui/ac;->g(Lcom/google/googlenav/ui/ac;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/ui/aj;->d:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aj;->a()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/aj;->b:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/aj;->e()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/aj;->b:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public c()V
    .locals 2

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/googlenav/ui/aj;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/aj;->d:I

    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-static {v0, v1, p0}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    return-void
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/aj;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/aj;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    return-void
.end method
