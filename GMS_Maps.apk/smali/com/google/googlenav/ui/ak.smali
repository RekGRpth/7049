.class public Lcom/google/googlenav/ui/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaV/i;
.implements Landroid/location/GpsStatus$Listener;


# static fields
.field private static final y:Ljava/util/Map;


# instance fields
.field private final a:LaH/m;

.field private final b:LaN/p;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/ui/s;

.field private final e:Lcom/google/googlenav/android/A;

.field private f:LaH/h;

.field private final g:Lo/S;

.field private final h:LaN/v;

.field private i:Ljava/lang/Boolean;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:J

.field private q:J

.field private r:Z

.field private s:Lo/D;

.field private t:LaH/t;

.field private u:Landroid/location/GpsStatus;

.field private v:J

.field private w:Ljava/lang/String;

.field private x:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "gps"

    const-string v2, "g"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "network"

    const-string v2, "wc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "wifi"

    const-string v2, "wc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "indoor"

    const-string v2, "i"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_cell"

    const-string v2, "fc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_wifi"

    const-string v2, "fw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_gps"

    const-string v2, "fg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_indoor"

    const-string v2, "fi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LaH/m;Lcom/google/googlenav/ui/s;LaN/u;LaN/p;Lcom/google/googlenav/android/A;)V
    .locals 6

    const/4 v5, 0x0

    const-wide/high16 v3, -0x8000000000000000L

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->j:Z

    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->k:Z

    iput v1, p0, Lcom/google/googlenav/ui/ak;->l:I

    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->m:Z

    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->n:Z

    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->o:Z

    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->p:J

    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->q:J

    iput-object v5, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->v:J

    iput-object v5, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    iput-object p2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    iput-object p3, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    iput-object p4, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    iput-object p5, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    new-instance v0, LaH/t;

    invoke-direct {v0}, LaH/t;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->t:LaH/t;

    new-instance v0, Lcom/google/googlenav/ui/al;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/al;-><init>(Lcom/google/googlenav/ui/ak;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->h:LaN/v;

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->h:LaN/v;

    invoke-virtual {p3, v0}, LaN/u;->a(LaN/v;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, LaV/h;->a(LaV/i;)V

    :cond_0
    return-void
.end method

.method private C()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->m:Z

    const/16 v0, 0x5f3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lo/S;)V

    return-void
.end method

.method private E()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()V
    .locals 7

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    const-string v0, "HAS_SHOWN_CALIBRATE_COMPASS_NOTIFICATION"

    new-instance v1, Lcom/google/googlenav/ui/ao;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/ao;-><init>(Lcom/google/googlenav/ui/ak;)V

    invoke-static {v0, v4, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    invoke-virtual {v0}, LaV/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LaV/h;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    const-string v0, "HAS_SHOWN_CALIBRATE_COMPASS_NOTIFICATION"

    const/4 v1, 0x1

    invoke-static {v0, v1, v5}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x66

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x65

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_0
.end method

.method private G()Z
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "GPS_FIX"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private H()V
    .locals 6

    const/4 v4, 0x1

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    invoke-static {v2}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbf/C;->k(Z)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "GPS_FIX"

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private I()Z
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->N()J

    move-result-wide v3

    const-wide/16 v5, 0xfa0

    add-long/2addr v5, v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v5, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->M()J

    move-result-wide v5

    const-wide/32 v7, 0x6ddd00

    add-long/2addr v5, v7

    cmp-long v3, v5, v3

    if-gez v3, :cond_1

    move v3, v1

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private J()V
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    return-void
.end method

.method private K()I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->r()LaH/h;

    move-result-object v1

    invoke-static {v1}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    if-ltz v0, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2}, LaH/E;->e(LaN/B;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v2

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v2, v1}, LaH/E;->b(LaN/B;)Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit16 v0, v0, 0x275

    :cond_0
    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    rem-int/lit8 v1, v0, 0x64

    sub-int/2addr v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    rem-int/lit8 v1, v0, 0xa

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private L()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    invoke-static {v0}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->a(Lo/D;)V

    :cond_0
    return-void
.end method

.method private M()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "c"

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v0, :cond_1

    const-string v0, "a"

    goto :goto_0

    :cond_1
    const-string v0, "n"

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;LaH/h;)LaH/h;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    return-object p1
.end method

.method public static a(ILaN/B;)LaN/Y;
    .locals 6

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    int-to-long v0, p0

    int-to-long v2, p0

    mul-long v1, v0, v2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, v0}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v4

    invoke-virtual {p1, v4}, LaN/B;->b(LaN/B;)J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-lez v4, :cond_0

    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZZ)LaN/Y;
    .locals 4

    const/16 v3, 0x13

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    if-eqz p1, :cond_4

    const/16 v0, 0xf

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v1}, LaH/m;->r()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2}, LaN/p;->g(LaN/B;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :cond_2
    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->m()LaN/Y;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, LaN/Y;->b(LaN/Y;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    return-object p1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 6

    const/4 v3, 0x0

    const-string v0, "y"

    const-string v0, "n"

    const-string v1, "sw"

    invoke-static {}, Lcom/google/googlenav/ui/ak;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "n"

    :goto_0
    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->c()Ljava/util/Map;

    move-result-object v1

    const/4 v0, 0x1

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaH/B;

    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    move v2, v3

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "network"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "sc"

    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "y"

    :goto_2
    invoke-static {v5, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_1

    :cond_2
    const-string v0, "y"

    goto :goto_0

    :cond_3
    const-string v0, "n"

    goto :goto_2

    :cond_4
    const-string v5, "gps"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v5, "sg"

    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "y"

    :goto_3
    invoke-static {v5, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    invoke-virtual {v0}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v3

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_a

    add-int/lit8 v0, v1, 0x1

    :goto_5
    move v1, v0

    goto :goto_4

    :cond_5
    const-string v0, "n"

    goto :goto_3

    :cond_6
    const-string v0, "ss"

    invoke-static {v0, v1, p1}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    goto :goto_1

    :cond_7
    move v2, v0

    :cond_8
    const-string v1, "sl"

    if-eqz v2, :cond_9

    const-string v0, "n"

    :goto_6
    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    return-void

    :cond_9
    const-string v0, "y"

    goto :goto_6

    :cond_a
    move v0, v1

    goto :goto_5
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->r:Z

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ak;)LaH/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    return-object v0
.end method

.method private b(Ljava/lang/StringBuilder;)V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->v:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const-string v2, "tl"

    iget-wide v3, p0, Lcom/google/googlenav/ui/ak;->v:J

    sub-long v3, v0, v3

    invoke-static {v2, v3, v4, p1}, Lbm/m;->a(Ljava/lang/String;JLjava/lang/StringBuilder;)V

    :cond_0
    iput-wide v0, p0, Lcom/google/googlenav/ui/ak;->v:J

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ak;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/ak;)LaH/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    return-object v0
.end method

.method private c(Ljava/lang/StringBuilder;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v0

    const-string v1, "o"

    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/ak;->b(Ljava/lang/StringBuilder;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/ak;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->o:Z

    return p1
.end method

.method static synthetic d(Lcom/google/googlenav/ui/ak;)Lo/S;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/ak;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/ak;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->n:Z

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/ak;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->o:Z

    return v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/ak;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->C()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/ak;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/ak;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->F()V

    return-void
.end method

.method public static w()Z
    .locals 1

    invoke-static {}, LaL/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaL/d;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "o"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "n"

    const-string v2, "p"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public B()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a"

    const-string v2, "c"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->c(Ljava/lang/StringBuilder;)V

    const-string v1, "n"

    const-string v2, "a"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lo/S;->c(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    return-void
.end method

.method public a(FF)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0}, Lo/S;->b()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->c(F)F

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1}, Lo/S;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40a00000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lo/S;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0, p1}, Lo/S;->a(F)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->p()I

    move-result v0

    if-ne v0, p1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->q()I

    move-result v0

    if-eq v0, p2, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->r()V

    goto :goto_0
.end method

.method public a(ILaH/m;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/an;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/googlenav/ui/an;-><init>(Lcom/google/googlenav/ui/ak;LaH/m;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public a(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x5f3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->z()V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    :cond_1
    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/am;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/am;-><init>(Lcom/google/googlenav/ui/ak;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a"

    const-string v2, "l"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "o"

    invoke-static {v1, p1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "n"

    invoke-static {v1, p2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    invoke-static {p1, p2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->b(Ljava/lang/StringBuilder;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    :cond_0
    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 3

    if-nez p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    const-string v0, "z"

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-static {v0, v1, p2}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    const-string v0, "y"

    const-string v0, "n"

    const-string v1, "rl"

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "y"

    :goto_1
    invoke-static {v1, v0, p2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->t:LaH/t;

    invoke-virtual {v0, p2}, LaH/t;->a(Ljava/lang/StringBuilder;)V

    return-void

    :cond_2
    const-string v1, "ls"

    sget-object v2, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_3
    const-string v0, "n"

    goto :goto_1
.end method

.method public a(Z)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v3

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, LaH/h;->b()Lo/D;

    move-result-object v2

    if-eqz v2, :cond_4

    move v2, v1

    :goto_1
    if-nez v2, :cond_1

    const/16 v4, 0xa

    if-le v3, v4, :cond_2

    :cond_1
    if-eqz v2, :cond_3

    const/16 v2, 0x12

    if-gt v3, v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    invoke-virtual {p0, v0, v1, p1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public a(ZZZ)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/ak;->a(ZZ)LaN/Y;

    move-result-object v2

    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->r:Z

    iget-object v3, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v3, v0, v2}, LaN/u;->a(LaN/B;LaN/Y;)V

    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->K()I

    move-result v0

    if-ltz v0, :cond_1

    const/16 v2, 0x1cd

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    :cond_1
    iput v4, p0, Lcom/google/googlenav/ui/ak;->l:I

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lbf/X;->bM()V

    :cond_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    :cond_4
    :goto_0
    if-nez v1, :cond_7

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aq;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aq;->a()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    const/16 v0, 0x292

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->p:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/googlenav/ui/ak;->p:J

    goto :goto_0

    :cond_6
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    return-void
.end method

.method public a(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1}, Lo/S;->f()Lo/D;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1, v0}, Lo/S;->b(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v2

    invoke-virtual {v2}, Ln/s;->e()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1, v0}, Lo/S;->b(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v3

    invoke-virtual {v3, v1}, Ln/s;->c(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {v2, v0}, Lo/S;->b(Z)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    goto :goto_0
.end method

.method public c()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    sget-object v2, LaE/d;->a:LaE/d;

    invoke-virtual {v2}, LaE/d;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->d()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->E()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->b()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->F()V

    :cond_1
    invoke-virtual {v2}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v4

    if-nez v4, :cond_5

    :goto_1
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->f()V

    :goto_2
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4}, Lbf/am;->x()Lbf/bU;

    move-result-object v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4}, Lbf/am;->v()Lbf/O;

    move-result-object v4

    if-eqz v4, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aJ()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/google/googlenav/mylocationnotifier/k;->g()V

    :cond_4
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->h()V

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    goto :goto_2
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 5

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/googlenav/ui/ak;->q:J

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    invoke-static {v0}, LaH/h;->a(LaH/h;)LaN/B;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1, v1, v1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->G()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->H()V

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aJ()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->g()V

    :cond_2
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->j:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v3

    invoke-virtual {v3}, LaH/h;->b()Lo/D;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iget-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v4}, LaN/u;->c()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v4}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->r:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    const/16 v0, 0x13

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1, v2, v0}, LaN/u;->a(LaN/B;LaN/Y;)V

    :cond_4
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    invoke-virtual {v3}, LaH/h;->b()Lo/D;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    if-eqz v1, :cond_8

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    invoke-virtual {v1, v2}, Ln/s;->d(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_9

    :cond_8
    if-eqz v0, :cond_5

    :cond_9
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto :goto_2
.end method

.method public g()[Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->K()I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1cf

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v1, "\n"

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x5e1

    goto :goto_0

    :cond_1
    const/16 v0, 0x5e2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()LaH/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    return-object v0
.end method

.method public j()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->a(LaH/h;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 1

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    return v0
.end method

.method public m()LaN/Y;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v1

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/ak;->a(ILaN/B;)LaN/Y;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->q:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->e(Z)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x117

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x1cc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onGpsStatusChanged(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method p()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->t()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method q()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->s()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public r()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aq;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aq;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public s()LaH/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    return-object v0
.end method

.method public t()Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->n()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v3}, LaH/m;->s()LaH/h;

    move-result-object v3

    invoke-static {v3}, LaH/h;->a(Landroid/location/Location;)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaN/u;->a(ILaN/B;)F

    move-result v2

    const/high16 v3, 0x40000000

    mul-float/2addr v2, v3

    float-to-long v2, v2

    div-int/lit8 v1, v1, 0x2

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/ak;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->f()Z

    move-result v0

    return v0
.end method

.method public x()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->j:Z

    return-void
.end method

.method public y()V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->N()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->x()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    goto :goto_0
.end method

.method public z()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a"

    const-string v2, "d"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->c(Ljava/lang/StringBuilder;)V

    const-string v1, "n"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    return-void
.end method
