.class public Lcom/google/googlenav/ui/android/AndroidView;
.super Lcom/google/googlenav/ui/android/BaseAndroidView;
.source "SourceFile"

# interfaces
.implements LaD/m;
.implements Lcom/google/googlenav/ui/b;


# instance fields
.field private final f:Lan/e;

.field private g:LaV/h;

.field private h:Lcom/google/googlenav/ui/android/F;

.field private i:Lcom/google/googlenav/ui/android/o;

.field private j:Lcom/google/googlenav/ui/android/o;

.field private final k:Landroid/graphics/Point;

.field private l:Z

.field private m:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lan/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lan/e;-><init>(Landroid/graphics/Canvas;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/android/AndroidView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/android/AndroidView;->setFocusableInTouchMode(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/AndroidView;->setWillNotDraw(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0, p1, p0}, LaD/g;->a(Landroid/content/Context;LaD/m;)V

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->i()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/AndroidView;)F
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->k()F

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MotionEvent;IIJ)V
    .locals 8

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    new-instance v0, Lat/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v3, v1, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v4, v1, Lcom/google/googlenav/ui/android/o;->b:I

    move v1, p2

    move v2, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/b;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    return-void

    :cond_0
    new-instance v0, Lat/b;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v1, p2

    move v2, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    goto :goto_0
.end method

.method private i()V
    .locals 1

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    new-instance v0, Lcom/google/googlenav/ui/android/F;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/F;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    new-instance v0, Lcom/google/googlenav/ui/android/o;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/o;-><init>(Lcom/google/googlenav/ui/android/AndroidView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    new-instance v0, Lcom/google/googlenav/ui/android/o;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/o;-><init>(Lcom/google/googlenav/ui/android/AndroidView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    return-void
.end method

.method private j()Z
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()F
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0}, LaD/g;->a()V

    return-void
.end method

.method public a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    new-instance v0, Lcom/google/googlenav/ui/android/b;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->c:Lcom/google/googlenav/ui/android/b;

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 5

    const/high16 v4, 0x3f000000

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->i()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    invoke-virtual {v0}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->m:Landroid/graphics/Canvas;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->save(I)I

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->k()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/android/F;->a(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    invoke-virtual {v0, v1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method public a(LaD/o;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(LaD/q;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(LaD/q;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    return v0
.end method

.method public a(LaD/u;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    invoke-virtual {v0}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->m:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v2, -0x1

    const/4 v3, 0x2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/b;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    iget-boolean v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->m()V

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    invoke-virtual {v1, p1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->a(Lam/e;)V

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->onDraw(Landroid/graphics/Canvas;)V

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->screenDrawn()V

    :cond_2
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v2, -0x1

    const/4 v3, 0x7

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v3, v3, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->a:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->b:I

    iget-object v5, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v5, v5, Lcom/google/googlenav/ui/android/o;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    new-instance v0, Lat/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v3, v3, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->b:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-object v7, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/b;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    new-instance v0, Lat/b;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-object v7, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    goto :goto_1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, -0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    return v3
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    return v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0, p1}, LaD/g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0
.end method
