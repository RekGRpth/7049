.class public abstract Lcom/google/googlenav/ui/android/BaseAndroidView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected b:Lcom/google/googlenav/android/i;

.field protected c:Lcom/google/googlenav/ui/android/b;

.field protected d:Z

.field protected final e:LaD/g;

.field private f:Landroid/view/View$OnTouchListener;

.field private g:Ljava/util/List;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->d:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    iput-object p1, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->a:Landroid/content/Context;

    new-instance v0, LaD/g;

    invoke-direct {v0}, LaD/g;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->e:LaD/g;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
.end method

.method public declared-synchronized a(Lcom/google/googlenav/ui/android/D;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b()V
.end method

.method public declared-synchronized b(Lcom/google/googlenav/ui/android/D;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->C()V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public abstract f()V
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public l()V
    .locals 0

    return-void
.end method

.method protected m()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/android/C;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/C;-><init>(Lcom/google/googlenav/ui/android/BaseAndroidView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/b;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/android/b;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/b;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/D;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/ui/android/D;->b(II)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public setIsLongpressEnabledForTest(Z)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    check-cast p0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->y()LaD/g;

    move-result-object v0

    invoke-virtual {v0, p1}, LaD/g;->a(Z)V

    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
