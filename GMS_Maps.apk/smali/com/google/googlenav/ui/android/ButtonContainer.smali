.class public Lcom/google/googlenav/ui/android/ButtonContainer;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private static b(I)I
    .locals 3

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no such view id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const v0, 0x7f100059

    :goto_0
    return v0

    :sswitch_1
    const v0, 0x7f10005a

    goto :goto_0

    :sswitch_2
    const v0, 0x7f10005c

    goto :goto_0

    :sswitch_3
    const v0, 0x7f10005b

    goto :goto_0

    :sswitch_4
    const v0, 0x7f100074

    goto :goto_0

    :sswitch_5
    const v0, 0x7f10005f

    goto :goto_0

    :sswitch_6
    const v0, 0x7f100061

    goto :goto_0

    :sswitch_7
    const v0, 0x7f10005d

    goto :goto_0

    :sswitch_8
    const v0, 0x7f100063

    goto :goto_0

    :sswitch_9
    const v0, 0x7f100064

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f100019 -> :sswitch_0
        0x7f10005e -> :sswitch_7
        0x7f100060 -> :sswitch_5
        0x7f100062 -> :sswitch_6
        0x7f1000c1 -> :sswitch_8
        0x7f1001ba -> :sswitch_4
        0x7f1002e8 -> :sswitch_9
        0x7f100382 -> :sswitch_1
        0x7f100383 -> :sswitch_2
        0x7f1003c7 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/google/googlenav/ui/android/ButtonContainer;->b(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ButtonContainer;->a:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public a(Landroid/app/Application;)V
    .locals 1

    const v0, 0x7f050014

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ButtonContainer;->a:Landroid/view/animation/Animation;

    return-void
.end method

.method public b()V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ButtonContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/actionbar/a;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->at()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v1

    :cond_0
    :goto_0
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ne v3, v2, :cond_1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eq v3, v1, :cond_2

    :cond_1
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v3, v2, v4, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method public c()Lcom/google/googlenav/ui/android/FloorPickerView;
    .locals 2

    const v0, 0x7f1001ba

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1001bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/FloorPickerView;

    return-object v0
.end method

.method public d()Lcom/google/googlenav/ui/android/ac;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/ac;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/ac;-><init>(Lcom/google/googlenav/ui/android/HeaderContainer;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/googlenav/ui/android/ac;

    const v0, 0x7f100055

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/HeaderContainer;

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/android/ac;-><init>(Lcom/google/googlenav/ui/android/HeaderContainer;)V

    move-object v0, v1

    goto :goto_0
.end method
