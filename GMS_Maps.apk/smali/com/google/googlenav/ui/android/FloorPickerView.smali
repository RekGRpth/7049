.class public Lcom/google/googlenav/ui/android/FloorPickerView;
.super Landroid/widget/ListView;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements Ln/u;


# instance fields
.field private a:I

.field private b:Lo/y;

.field private c:Ln/s;

.field private d:I

.field private volatile e:Lo/D;

.field private f:Lcom/google/googlenav/ui/android/W;

.field private final g:Ljava/util/Set;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->h:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/FloorPickerView;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->h:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/FloorPickerView;Lo/y;Lo/D;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    return-void
.end method

.method private a(Lo/y;Lo/D;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/D;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setSelectedPosition(I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b()V

    goto :goto_0
.end method

.method private static a(Lo/z;Lo/y;)V
    .locals 9

    if-nez p0, :cond_0

    const-string v0, "none"

    move-object v1, v0

    :goto_0
    if-nez p1, :cond_1

    const-string v0, "none"

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?sa=T&oi=m_map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lbm/l;->c:Lbm/l;

    invoke-virtual {v3}, Lbm/l;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x68

    const-string v4, "s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "l="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "b="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic a(Lo/y;Lo/y;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/y;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/FloorPickerView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    return v0
.end method

.method private static b(Lo/y;Lo/D;)I
    .locals 3

    const/4 v0, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lo/y;->b(Lo/D;)I

    move-result v1

    if-ltz v1, :cond_2

    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method private b()V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method private static b(Lo/y;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lo/y;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v0, :cond_2

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private static b(Lo/y;Lo/y;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/FloorPickerView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    return v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/D;)I

    move-result v0

    :cond_2
    iget v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/W;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private c(Lo/y;Lo/D;)V
    .locals 9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?sa=T&oi=m_map:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbm/l;->c:Lbm/l;

    invoke-virtual {v1}, Lbm/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez p2, :cond_0

    const-string v0, "0"

    :goto_0
    invoke-static {p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "1"

    :goto_1
    const/16 v3, 0x68

    const-string v4, "f"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "b="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v8

    invoke-virtual {v8}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "p="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x3

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "1"

    goto :goto_0

    :cond_1
    const-string v1, "0"

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic d(Lcom/google/googlenav/ui/android/FloorPickerView;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/android/FloorPickerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/android/FloorPickerView;)Lo/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    return v0
.end method

.method public a(I)Lo/z;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/X;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/X;->a()Lo/z;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaH/m;)V
    .locals 0

    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 2

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    invoke-static {v1, v0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/android/U;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/android/U;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lo/D;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/W;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a(Ln/s;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/android/S;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/android/S;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Ln/s;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Ln/s;Lo/y;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/android/T;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/ui/android/T;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Ln/s;Lo/y;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method a(Lo/D;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c()V

    return-void
.end method

.method a(Lo/y;)V
    .locals 7

    const-wide/16 v5, 0x1f4

    const/4 v1, -0x1

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/y;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->clearAnimation()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    iput v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    iput v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lcom/google/googlenav/ui/android/Q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/Q;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/google/googlenav/ui/android/W;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/android/W;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Landroid/content/Context;Lo/y;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c()V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lcom/google/googlenav/ui/android/R;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/R;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public b(Ln/s;)V
    .locals 0

    return-void
.end method

.method c(Ln/s;)V
    .locals 3

    invoke-virtual {p1}, Ln/s;->c()Lo/y;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {p1, v2}, Ln/s;->b(Lo/r;)Lo/E;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lo/E;->c()Lo/D;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c(Lo/y;Lo/D;)V

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    new-instance v0, Lcom/google/googlenav/ui/android/V;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/V;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setIndoorState(Ln/s;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    invoke-virtual {v0, p0}, Ln/s;->b(Ln/u;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ln/s;)V

    invoke-virtual {p1, p0}, Ln/s;->a(Ln/u;)V

    :cond_1
    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    return-void
.end method

.method public setSelectedLevel(Lo/y;Lo/z;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lo/z;->a()Lo/D;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectedPosition(I)V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/W;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/W;->notifyDataSetChanged()V

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/X;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/X;->a()Lo/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/z;Lo/y;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-virtual {v0, v1}, Ln/s;->a(Lo/y;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/s;

    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-virtual {v1, v0}, Ln/s;->a(Lo/D;)V

    goto :goto_0
.end method
