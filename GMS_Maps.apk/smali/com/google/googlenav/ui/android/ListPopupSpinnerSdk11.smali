.class public Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;
.super Landroid/widget/TextView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/googlenav/ui/android/ai;


# instance fields
.field private a:Landroid/widget/ListPopupWindow;

.field private b:Landroid/view/View$OnClickListener;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    invoke-super {p0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private e()I
    .locals 9

    const/4 v3, 0x0

    const/4 v8, -0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d:Landroid/widget/ListAdapter;

    if-nez v1, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->getSelectedItemPosition()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v4, v1, 0xf

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v7

    sub-int v2, v7, v1

    rsub-int/lit8 v2, v2, 0xf

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move-object v2, v3

    :goto_0
    if-ge v1, v7, :cond_0

    iget-object v4, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d:Landroid/widget/ListAdapter;

    invoke-interface {v4, v1, v2, v3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-nez v4, :cond_2

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-virtual {v2, v5, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->e()I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setContentWidth(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->e()I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setContentWidth(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->b:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a()V

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->c:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->b:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->c:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setSelection(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk11;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
