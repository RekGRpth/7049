.class public final Lcom/google/googlenav/ui/android/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/v;
.implements Lbf/au;
.implements Ln/q;


# instance fields
.field private final a:Lcom/google/googlenav/ui/android/FloorPickerView;

.field private final b:Lcom/google/googlenav/ui/s;

.field private final c:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private d:Lo/z;

.field private volatile e:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    iput-object p2, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    iput-object p3, p0, Lcom/google/googlenav/ui/android/N;->c:Lcom/google/googlenav/ui/android/AndroidVectorView;

    return-void
.end method

.method static synthetic a(LC/a;Lo/ad;)F
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/android/N;->b(LC/a;Lo/ad;)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/android/FloorPickerView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/googlenav/ui/android/N;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/android/N;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/ui/android/N;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/N;->a()V

    return-object v0
.end method

.method static synthetic a(LC/a;F)Lo/aQ;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/android/N;->b(LC/a;F)Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/N;Lo/z;)Lo/z;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    return-object p1
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, LR/a;->a([I)LR/a;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lbf/am;->a(Lbf/au;LR/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    new-instance v1, Lcom/google/googlenav/ui/android/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/O;-><init>(Lcom/google/googlenav/ui/android/N;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x15
    .end array-data
.end method

.method static synthetic a(Lo/z;)V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/ui/android/N;->b(Lo/z;)V

    return-void
.end method

.method private static b(LC/a;Lo/ad;)F
    .locals 5

    invoke-virtual {p0}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v1

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v0

    const/4 v2, 0x4

    new-array v2, v2, [I

    const/4 v3, 0x0

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v4

    sub-int v4, v1, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v4

    sub-int v1, v4, v1

    aput v1, v2, v3

    const/4 v1, 0x2

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    sub-int v3, v0, v3

    aput v3, v2, v1

    const/4 v1, 0x3

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    sub-int v0, v3, v0

    aput v0, v2, v1

    invoke-static {v2}, Lac/a;->a([I)I

    move-result v0

    invoke-virtual {p0}, LC/a;->l()I

    move-result v1

    invoke-virtual {p0}, LC/a;->k()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, LC/a;->c(FF)F

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private static b(LC/a;F)Lo/aQ;
    .locals 6

    invoke-virtual {p0}, LC/a;->l()I

    move-result v0

    invoke-virtual {p0}, LC/a;->k()I

    move-result v1

    int-to-float v2, v1

    mul-float/2addr v2, p1

    int-to-float v3, v0

    mul-float/2addr v3, p1

    int-to-float v4, v0

    sub-float/2addr v4, v3

    invoke-virtual {p0, v2, v4}, LC/a;->d(FF)Lo/T;

    move-result-object v4

    int-to-float v5, v1

    sub-float/2addr v5, v2

    int-to-float v0, v0

    sub-float/2addr v0, v3

    invoke-virtual {p0, v5, v0}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-virtual {p0, v2, v3}, LC/a;->d(FF)Lo/T;

    move-result-object v5

    int-to-float v1, v1

    sub-float/2addr v1, v2

    invoke-virtual {p0, v1, v3}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    invoke-static {v4, v0, v5, v1}, Lo/aQ;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 5

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->I()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    instance-of v3, v0, Lbf/bk;

    if-eqz v3, :cond_0

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-interface {v3, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ljava/util/Collection;)V

    return-void
.end method

.method private static b(Lo/z;)V
    .locals 7

    if-nez p0, :cond_0

    const-string v0, "none"

    :goto_0
    const/16 v1, 0x68

    const-string v2, "a"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "l="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    invoke-virtual {v0}, Lo/z;->g()Lo/aR;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/N;->e:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/u;->a(LaN/v;)V

    new-instance v0, Lo/aB;

    invoke-direct {v0}, Lo/aB;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    invoke-virtual {v1}, Lo/z;->a()Lo/D;

    move-result-object v1

    invoke-static {v1}, Lo/E;->a(Lo/D;)Lo/E;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aB;->a(Lo/at;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    invoke-virtual {v1}, Lo/z;->g()Lo/aR;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v1, v2, v0}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/N;->c:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->B()Ln/n;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Ln/n;->a(Ljava/util/Collection;Ln/q;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/N;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/N;->c()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/android/N;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/N;->e:Z

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/android/AndroidVectorView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->c:Lcom/google/googlenav/ui/android/AndroidVectorView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/android/N;)Lo/z;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    return-object v0
.end method


# virtual methods
.method public a(Lbf/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/N;->b()V

    return-void
.end method

.method public a(Lo/j;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/android/P;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/ui/android/P;-><init>(Lcom/google/googlenav/ui/android/N;Las/c;Lo/j;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/P;->g()V

    return-void
.end method

.method public a(ZZZII)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/N;->e:Z

    return-void
.end method

.method public b(Lbf/i;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ljava/util/Collection;)V

    return-void
.end method

.method public c(Lbf/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/N;->b()V

    return-void
.end method

.method public i()V
    .locals 0

    return-void
.end method
