.class Lcom/google/googlenav/ui/android/P;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lo/j;

.field final synthetic b:Lcom/google/googlenav/ui/android/N;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/android/N;Las/c;Lo/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    iput-object p3, p0, Lcom/google/googlenav/ui/android/P;->a:Lo/j;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 9

    const/4 v0, 0x0

    const/4 v8, -0x1

    const v2, 0x4188cccd

    iget-object v1, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/N;->b(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-virtual {v1, v3}, LaN/u;->b(LaN/v;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/N;->d(Lcom/google/googlenav/ui/android/N;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/N;->e(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n()LC/a;

    move-result-object v3

    const v1, 0x3dcccccd

    invoke-static {v3, v1}, Lcom/google/googlenav/ui/android/N;->a(LC/a;F)Lo/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lo/aQ;->i()[Lo/ae;

    move-result-object v4

    move v1, v0

    :goto_1
    if-nez v1, :cond_2

    array-length v5, v4

    if-ge v0, v5, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/android/P;->a:Lo/j;

    aget-object v5, v4, v0

    invoke-interface {v1, v5}, Lo/j;->a(Lo/ae;)Z

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-nez v1, :cond_0

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/N;->f(Lcom/google/googlenav/ui/android/N;)Lo/z;

    move-result-object v0

    invoke-virtual {v0}, Lo/z;->g()Lo/aR;

    move-result-object v0

    invoke-virtual {v0}, Lo/aR;->b()Lo/ad;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/android/N;->a(LC/a;Lo/ad;)F

    move-result v7

    cmpg-float v0, v7, v2

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/P;->a:Lo/j;

    invoke-interface {v0}, Lo/j;->a()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->f()Lo/T;

    move-result-object v1

    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->d()F

    move-result v3

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    :goto_2
    invoke-virtual {v6, v0, v8, v8}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/P;->b:Lcom/google/googlenav/ui/android/N;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/N;->f(Lcom/google/googlenav/ui/android/N;)Lo/z;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/android/N;->a(Lo/z;)V

    goto :goto_0

    :cond_3
    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->d()F

    move-result v3

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    move v2, v7

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    goto :goto_2
.end method
