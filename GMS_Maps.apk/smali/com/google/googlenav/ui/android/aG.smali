.class public Lcom/google/googlenav/ui/android/aG;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/c;

.field private final b:Lcom/google/googlenav/ui/view/t;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/android/aG;->b:Lcom/google/googlenav/ui/view/t;

    iput-object p1, p0, Lcom/google/googlenav/ui/android/aG;->a:Lcom/google/googlenav/ui/view/c;

    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/android/aG;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/c;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Lcom/google/googlenav/ui/android/aG;-><init>(Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)V

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    move-object v0, v1

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/aG;->a:Lcom/google/googlenav/ui/view/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/aG;->b:Lcom/google/googlenav/ui/view/t;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/c;->a(Lcom/google/googlenav/ui/view/t;)Z

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/aG;->a:Lcom/google/googlenav/ui/view/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/aG;->b:Lcom/google/googlenav/ui/view/t;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/c;->b(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    return v0
.end method
