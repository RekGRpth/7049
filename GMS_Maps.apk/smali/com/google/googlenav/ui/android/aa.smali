.class public Lcom/google/googlenav/ui/android/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/google/googlenav/ui/android/Z;


# direct methods
.method public constructor <init>()V
    .locals 2

    const v1, 0x7fffffff

    sget-object v0, Lcom/google/googlenav/ui/android/Z;->a:Lcom/google/googlenav/ui/android/Z;

    invoke-direct {p0, v1, v1, v0}, Lcom/google/googlenav/ui/android/aa;-><init>(IILcom/google/googlenav/ui/android/Z;)V

    return-void
.end method

.method public constructor <init>(IILcom/google/googlenav/ui/android/Z;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/googlenav/ui/android/aa;->a:I

    iput p2, p0, Lcom/google/googlenav/ui/android/aa;->b:I

    iput-object p3, p0, Lcom/google/googlenav/ui/android/aa;->c:Lcom/google/googlenav/ui/android/Z;

    return-void
.end method

.method public constructor <init>(Landroid/util/AttributeSet;)V
    .locals 5

    const v2, 0x7fffffff

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "maxLinesPortraitMode"

    invoke-interface {p1, v1, v0, v2}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/aa;->a:I

    const-string v0, "maxLinesPortraitMode"

    invoke-interface {p1, v1, v0, v2}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/aa;->b:I

    const-string v0, "lastElementStyle"

    invoke-interface {p1, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/android/Z;->a:Lcom/google/googlenav/ui/android/Z;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/aa;->c:Lcom/google/googlenav/ui/android/Z;

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/googlenav/ui/android/Z;->valueOf(Ljava/lang/String;)Lcom/google/googlenav/ui/android/Z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/aa;->c:Lcom/google/googlenav/ui/android/Z;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Landroid/view/InflateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Value not valid for lastElementStyle:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
