.class public Lcom/google/googlenav/ui/android/af;
.super Lcom/google/android/maps/driveabout/vector/f;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private volatile b:Landroid/view/View$OnClickListener;

.field private volatile c:Landroid/view/View$OnClickListener;

.field private volatile d:F

.field private volatile e:F

.field private volatile f:F

.field private volatile g:F

.field private volatile h:F

.field private volatile i:Lcom/google/googlenav/ui/android/ah;

.field private volatile j:Lcom/google/googlenav/ui/android/ah;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->a:Ljava/util/Map;

    iput-object v1, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    iput-object v1, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    sget-object v0, Lcom/google/googlenav/ui/android/ah;->a:Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->i:Lcom/google/googlenav/ui/android/ah;

    sget-object v0, Lcom/google/googlenav/ui/android/ah;->a:Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->j:Lcom/google/googlenav/ui/android/ah;

    return-void
.end method

.method private a(Landroid/view/View;FF)I
    .locals 6

    const/4 v1, 0x0

    const/high16 v5, 0x47800000

    const v0, 0x7f10001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    iget v2, p0, Lcom/google/googlenav/ui/android/af;->d:F

    sub-float v2, p2, v2

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    div-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    iput-object v1, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/android/af;->e:F

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const v3, 0x7f100049

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    shr-int/lit8 v4, v4, 0x1

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    add-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v3, p3

    iput v3, p0, Lcom/google/googlenav/ui/android/af;->e:F

    iget v3, p0, Lcom/google/googlenav/ui/android/af;->e:F

    int-to-float v4, v4

    sub-float/2addr v3, v4

    mul-float/2addr v3, v5

    div-float/2addr v3, p2

    float-to-int v3, v3

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/google/googlenav/ui/android/af;->a:Ljava/util/Map;

    sget-object v5, Lcom/google/googlenav/ui/android/ah;->b:Lcom/google/googlenav/ui/android/ah;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/android/af;->a(II)Lcom/google/android/maps/driveabout/vector/g;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->a()Landroid/view/View$OnClickListener;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    move v0, v3

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(II)Lcom/google/android/maps/driveabout/vector/g;
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/high16 v5, 0x10000

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/maps/driveabout/vector/g;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/g;-><init>()V

    new-instance v1, LE/p;

    const/16 v2, 0xc

    new-array v2, v2, [I

    aput p0, v2, v4

    aput v4, v2, v6

    aput v5, v2, v7

    aput p0, v2, v8

    const/4 v3, 0x4

    aput v4, v2, v3

    const/4 v3, 0x5

    aput v4, v2, v3

    const/4 v3, 0x6

    aput p1, v2, v3

    const/4 v3, 0x7

    aput v4, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput p1, v2, v3

    const/16 v3, 0xa

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v4, v2, v3

    invoke-direct {v1, v2}, LE/p;-><init>([I)V

    iput-object v1, v0, Lcom/google/android/maps/driveabout/vector/g;->a:LE/o;

    new-instance v1, LE/j;

    const/16 v2, 0x8

    new-array v2, v2, [I

    aput p0, v2, v4

    aput v4, v2, v6

    aput p0, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x4

    aput p1, v2, v3

    const/4 v3, 0x5

    aput v4, v2, v3

    const/4 v3, 0x6

    aput p1, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    invoke-direct {v1, v2}, LE/j;-><init>([I)V

    iput-object v1, v0, Lcom/google/android/maps/driveabout/vector/g;->b:LE/i;

    return-object v0
.end method

.method private a(F)Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->g:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->g:F

    iget v1, p0, Lcom/google/googlenav/ui/android/af;->e:F

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/View;FF)I
    .locals 8

    const/4 v1, 0x0

    const/high16 v7, 0x47800000

    const v0, 0x7f10004e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    iget v2, p0, Lcom/google/googlenav/ui/android/af;->d:F

    sub-float v2, p2, v2

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/android/af;->d:F

    add-float/2addr v3, v2

    mul-float v2, v7, v3

    div-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    iput-object v1, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/android/af;->f:F

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const v4, 0x7f10004d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/google/googlenav/ui/android/af;->d:F

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v4, p3

    sub-float v4, v6, v4

    iput v4, p0, Lcom/google/googlenav/ui/android/af;->f:F

    iget v4, p0, Lcom/google/googlenav/ui/android/af;->f:F

    sub-float/2addr v3, v4

    int-to-float v4, v5

    add-float/2addr v3, v4

    mul-float/2addr v3, v7

    div-float/2addr v3, p2

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/af;->a:Ljava/util/Map;

    sget-object v5, Lcom/google/googlenav/ui/android/ah;->d:Lcom/google/googlenav/ui/android/ah;

    invoke-static {v3, v2}, Lcom/google/googlenav/ui/android/af;->a(II)Lcom/google/android/maps/driveabout/vector/g;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BubbleButton;->a()Landroid/view/View$OnClickListener;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    move v0, v3

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private b(F)Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->h:F

    iget v1, p0, Lcom/google/googlenav/ui/android/af;->f:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->h:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(F)Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->g:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->h:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(F)Lcom/google/googlenav/ui/android/ah;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/af;->c(F)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/af;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/android/ah;->b:Lcom/google/googlenav/ui/android/ah;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/af;->b(F)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/android/ah;->d:Lcom/google/googlenav/ui/android/ah;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/android/ah;->c:Lcom/google/googlenav/ui/android/ah;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/googlenav/ui/android/ah;->a:Lcom/google/googlenav/ui/android/ah;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/vector/g;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/af;->i:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/g;

    return-object v0
.end method

.method public a(FF)V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->d:F

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    sub-float v0, p1, v0

    iput v0, p0, Lcom/google/googlenav/ui/android/af;->g:F

    iget v0, p0, Lcom/google/googlenav/ui/android/af;->g:F

    iget v1, p0, Lcom/google/googlenav/ui/android/af;->d:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/android/af;->h:F

    return-void
.end method

.method public b()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/ah;->a:Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->i:Lcom/google/googlenav/ui/android/ah;

    return-void
.end method

.method public b(FF)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/af;->d(F)Lcom/google/googlenav/ui/android/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->j:Lcom/google/googlenav/ui/android/ah;

    return-void
.end method

.method public c(FF)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/af;->d(F)Lcom/google/googlenav/ui/android/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/af;->i:Lcom/google/googlenav/ui/android/ah;

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/android/ag;->a:[I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/af;->j:Lcom/google/googlenav/ui/android/ah;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/ah;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/af;->e()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/af;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/af;->e()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/af;->e()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d(FF)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/af;->e()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100048

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/af;->e()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/googlenav/ui/android/af;->d:F

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/android/af;->a(Landroid/view/View;FF)I

    move-result v2

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/android/af;->b(Landroid/view/View;FF)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/af;->a:Ljava/util/Map;

    sget-object v3, Lcom/google/googlenav/ui/android/ah;->c:Lcom/google/googlenav/ui/android/ah;

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/android/af;->a(II)Lcom/google/android/maps/driveabout/vector/g;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
