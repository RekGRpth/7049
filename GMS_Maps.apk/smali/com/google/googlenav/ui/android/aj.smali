.class Lcom/google/googlenav/ui/android/aj;
.super Lcom/google/googlenav/ui/view/dialog/q;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;Landroid/content/Context;Landroid/view/View;Landroid/widget/ListAdapter;Landroid/widget/AdapterView$OnItemClickListener;I)V
    .locals 6

    iput-object p1, p0, Lcom/google/googlenav/ui/android/aj;->a:Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/dialog/q;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/ListAdapter;Landroid/widget/AdapterView$OnItemClickListener;I)V

    return-void
.end method


# virtual methods
.method protected I_()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/q;->I_()V

    const v0, 0x7f100129

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/aj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/aj;->a:Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/aj;->h()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/aj;->l()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/android/aj;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected a(Landroid/view/WindowManager$LayoutParams;)V
    .locals 3

    const v0, 0x7f100129

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/aj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    sub-int v0, v2, v0

    iget v2, p0, Lcom/google/googlenav/ui/android/aj;->b:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v0, p0, Lcom/google/googlenav/ui/android/aj;->c:I

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->H()I

    move-result v0

    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    :cond_0
    return-void
.end method

.method protected f()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/q;->f()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/aj;->a:Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;Z)Z

    return-void
.end method
