.class public Lcom/google/googlenav/ui/android/al;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaT/m;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/NotificationManager;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/googlenav/ui/android/al;->c:I

    iput-object p1, p0, Lcom/google/googlenav/ui/android/al;->a:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/al;->b:Landroid/app/NotificationManager;

    return-void
.end method


# virtual methods
.method public onOfflineDataUpdate(LaT/l;)V
    .locals 9

    const v4, 0x1080082

    const/4 v1, 0x1

    const/16 v8, 0x334

    const/4 v2, 0x0

    invoke-virtual {p1}, LaT/l;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LaT/l;->l()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LaT/l;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaT/l;->c()I

    move-result v0

    const/16 v3, 0x64

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p1}, LaT/l;->b()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/al;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, v8}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :pswitch_0
    if-nez v0, :cond_4

    const v4, 0x1080081

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LaT/l;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "%"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-instance v5, Landroid/app/Notification;

    const-wide/16 v6, 0x0

    invoke-direct {v5, v4, v3, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v4, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/googlenav/ui/android/al;->a:Landroid/content/Context;

    const-class v7, Lcom/google/android/maps/MapsActivity;

    invoke-direct {v4, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v6, Landroid/net/Uri$Builder;

    invoke-direct {v6}, Landroid/net/Uri$Builder;-><init>()V

    const-string v7, "http"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "maps.google.com"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "/my-places"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "offline"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/googlenav/ui/android/al;->a:Landroid/content/Context;

    invoke-static {v6, v2, v4, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v4, p0, Lcom/google/googlenav/ui/android/al;->a:Landroid/content/Context;

    invoke-virtual {v5, v4, v3, v1, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    if-eqz v0, :cond_5

    const/16 v0, 0x10

    :goto_3
    iput v0, v5, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Lcom/google/googlenav/ui/android/al;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, v8, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    invoke-virtual {p1}, LaT/l;->b()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/al;->c:I

    goto/16 :goto_0

    :cond_3
    const-string v1, ""

    goto :goto_2

    :cond_4
    const/16 v1, 0x332

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const-string v1, ""

    goto :goto_2

    :pswitch_1
    iget v3, p0, Lcom/google/googlenav/ui/android/al;->c:I

    if-ne v3, v1, :cond_0

    const/16 v1, 0x335

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const-string v1, ""

    goto :goto_2

    :cond_5
    const/4 v0, 0x2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
