.class public Lcom/google/googlenav/ui/android/an;
.super Lcom/google/googlenav/ui/view/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;Lam/f;)V
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f100382

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1, v4}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/android/an;->a(Lcom/google/googlenav/ui/view/c;)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p3, p4, v1}, Lbf/bk;->a(Lcom/google/googlenav/ai;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    const v2, 0x7f10001c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x34

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->ae:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/ui/android/an;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/aW;)V

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/android/an;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz p5, :cond_0

    check-cast p5, Lan/f;

    invoke-virtual {p5}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/text/SpannableStringBuilder;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-gt v0, p4, :cond_0

    const v0, 0x7f0401a2

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    add-int/lit8 v1, p4, 0x1

    invoke-virtual {p2, v0, p4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :goto_0
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    invoke-virtual {p2, p4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/aW;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/googlenav/ui/aW;

    aput-object p2, v0, v2

    const/4 v1, 0x1

    aput-object p3, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 8

    const/16 v7, 0x8

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v2, :cond_6

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/aW;

    move-object v2, v1

    move v3, v4

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    const v6, 0x7f10001b

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/an;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/aW;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    const v1, 0x7f100253

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_3

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/an;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v2, v3

    move v3, v5

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    add-int/lit8 v4, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/googlenav/ui/aW;

    if-nez v2, :cond_0

    move v2, v4

    goto :goto_1

    :cond_0
    invoke-static {v2, v6}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    iget-boolean v2, v2, Lcom/google/googlenav/ui/aW;->d:Z

    if-eqz v2, :cond_5

    add-int/lit8 v2, v3, 0x1

    invoke-direct {p0, v1, v0, v6, v3}, Lcom/google/googlenav/ui/android/an;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/text/SpannableStringBuilder;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->clear()V

    :goto_2
    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_2

    add-int/lit8 v2, v3, 0x1

    invoke-direct {p0, v1, v0, v6, v3}, Lcom/google/googlenav/ui/android/an;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/text/SpannableStringBuilder;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->clear()V

    move v3, v2

    :cond_2
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_4

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    move v3, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_4
    return-void

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move v3, v2

    move-object v2, v1

    goto/16 :goto_0
.end method
