.class public Lcom/google/googlenav/ui/android/ao;
.super Lcom/google/googlenav/ui/view/d;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/android/ap;

.field b:Lcom/google/googlenav/ui/android/ap;

.field c:Lcom/google/googlenav/ui/android/ap;

.field private final f:Z

.field private final g:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ai;ZZ)V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f100383

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1, v2}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    iput-boolean p4, p0, Lcom/google/googlenav/ui/android/ao;->f:Z

    iput-boolean p5, p0, Lcom/google/googlenav/ui/android/ao;->g:Z

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/android/ao;->a(Lcom/google/googlenav/ui/view/c;)V

    invoke-direct {p0, p3, p2}, Lcom/google/googlenav/ui/android/ao;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V

    invoke-direct {p0, p3, p2}, Lcom/google/googlenav/ui/android/ao;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V

    invoke-direct {p0, p3, p2}, Lcom/google/googlenav/ui/android/ao;->c(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100386

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ao;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100387

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v1, Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, v4, v0, v2, v2}, Lcom/google/googlenav/ui/android/ap;-><init>(Landroid/view/View;Landroid/view/ViewGroup;ZI)V

    iput-object v1, p0, Lcom/google/googlenav/ui/android/ao;->a:Lcom/google/googlenav/ui/android/ap;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ao;->a:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/android/ap;->a(Lcom/google/googlenav/ui/view/c;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/google/googlenav/ui/aV;->al:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->ak:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->am:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->am:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v8

    const v0, 0x7f100388

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100389

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f10038a

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f10038b

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v5}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v8}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10001c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x34

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->ae:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10038c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f10038e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f10038d

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/ao;->f:Z

    if-eqz v2, :cond_1

    new-instance v4, Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v5, 0x2

    invoke-direct {v4, v3, v2, v6, v5}, Lcom/google/googlenav/ui/android/ap;-><init>(Landroid/view/View;Landroid/view/ViewGroup;ZI)V

    iput-object v4, p0, Lcom/google/googlenav/ui/android/ao;->b:Lcom/google/googlenav/ui/android/ap;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->n()Lam/g;

    move-result-object v2

    sget-char v4, Lcom/google/googlenav/ui/bi;->aV:C

    invoke-interface {v2, v4}, Lam/g;->e(C)Lam/f;

    move-result-object v2

    check-cast v2, Lan/f;

    invoke-virtual {v2}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f02000b

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ao;->b:Lcom/google/googlenav/ui/android/ap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ao;->b:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/android/ap;->a(Lcom/google/googlenav/ui/view/c;)V

    :cond_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/googlenav/ui/android/ao;->g:Z

    if-eqz v1, :cond_2

    new-instance v2, Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v4, 0x3

    invoke-direct {v2, v3, v1, v6, v4}, Lcom/google/googlenav/ui/android/ap;-><init>(Landroid/view/View;Landroid/view/ViewGroup;ZI)V

    iput-object v2, p0, Lcom/google/googlenav/ui/android/ao;->b:Lcom/google/googlenav/ui/android/ap;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->n()Lam/g;

    move-result-object v1

    sget-char v2, Lcom/google/googlenav/ui/bi;->aW:C

    invoke-interface {v1, v2}, Lam/g;->e(C)Lam/f;

    move-result-object v1

    check-cast v1, Lan/f;

    invoke-virtual {v1}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100384

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f100385

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f10019c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/DistanceView;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v4, Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ao;->d()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v2, v6, v5}, Lcom/google/googlenav/ui/android/ap;-><init>(Landroid/view/View;Landroid/view/ViewGroup;ZI)V

    iput-object v4, p0, Lcom/google/googlenav/ui/android/ao;->c:Lcom/google/googlenav/ui/android/ap;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/ao;->c:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2, p2}, Lcom/google/googlenav/ui/android/ap;->a(Lcom/google/googlenav/ui/view/c;)V

    const v2, 0x7f0202ea

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-static {v1, v0, v2}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    const/high16 v0, 0x41400000

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/DistanceView;->setTextSize(F)V

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/view/android/DistanceView;->setVisibility(I)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
