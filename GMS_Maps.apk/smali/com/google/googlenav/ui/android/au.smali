.class Lcom/google/googlenav/ui/android/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/au;->a:Lcom/google/googlenav/ui/android/RealtimeScheduleTransitStationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(JJ)I
    .locals 1

    cmp-long v0, p1, p3

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    cmp-long v0, p1, p3

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/cm;Lcom/google/googlenav/cm;)I
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/cm;->b(Z)J

    move-result-wide v0

    invoke-virtual {p2, v2}, Lcom/google/googlenav/cm;->b(Z)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/android/au;->a(JJ)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/googlenav/cm;

    check-cast p2, Lcom/google/googlenav/cm;

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/android/au;->a(Lcom/google/googlenav/cm;Lcom/google/googlenav/cm;)I

    move-result v0

    return v0
.end method
