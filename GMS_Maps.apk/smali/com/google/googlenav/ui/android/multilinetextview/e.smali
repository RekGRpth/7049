.class Lcom/google/googlenav/ui/android/multilinetextview/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method protected constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    return v0
.end method

.method protected final a(Lcom/google/googlenav/ui/android/multilinetextview/e;)Lcom/google/googlenav/ui/android/multilinetextview/e;
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This cannot be nextToken after consuming has finished."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p1
.end method

.method public final a(Ljava/lang/String;)Lcom/google/googlenav/ui/android/multilinetextview/e;
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This token has finished consuming"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/android/multilinetextview/e;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected b(Ljava/lang/String;)Lcom/google/googlenav/ui/android/multilinetextview/e;
    .locals 3

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/multilinetextview/e;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a(Lcom/google/googlenav/ui/android/multilinetextview/e;)Lcom/google/googlenav/ui/android/multilinetextview/e;

    move-result-object p0

    goto :goto_0
.end method

.method protected b(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    iget v1, p1, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    iget v1, p1, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    iget-boolean v1, p1, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->c:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/googlenav/ui/android/multilinetextview/e;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->b:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/e;->a:I

    add-int/2addr v0, v1

    return v0
.end method
