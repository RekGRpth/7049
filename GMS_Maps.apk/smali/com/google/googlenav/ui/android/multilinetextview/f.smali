.class Lcom/google/googlenav/ui/android/multilinetextview/f;
.super Lcom/google/googlenav/ui/android/multilinetextview/e;
.source "SourceFile"


# instance fields
.field a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;-><init>(I)V

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    return-void
.end method


# virtual methods
.method protected b(Ljava/lang/String;)Lcom/google/googlenav/ui/android/multilinetextview/e;
    .locals 3

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/e;

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/multilinetextview/e;-><init>(I)V

    move-object p0, v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/googlenav/ui/android/multilinetextview/f;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/googlenav/ui/android/multilinetextview/f;

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    iget v3, p1, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/f;->a:I

    add-int/2addr v0, v1

    return v0
.end method
