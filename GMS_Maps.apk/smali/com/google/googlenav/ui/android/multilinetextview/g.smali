.class Lcom/google/googlenav/ui/android/multilinetextview/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/x;


# static fields
.field private static final a:Lcom/google/common/base/S;

.field private static final b:Lcom/google/common/collect/dh;

.field private static final c:Lcom/google/common/base/K;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/common/base/S;->a(C)Lcom/google/common/base/S;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->a:Lcom/google/common/base/S;

    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/h;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/multilinetextview/h;-><init>()V

    invoke-static {v0}, Lcom/google/common/collect/dh;->a(Ljava/util/Comparator;)Lcom/google/common/collect/dh;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->b:Lcom/google/common/collect/dh;

    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/i;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/multilinetextview/i;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->c:Lcom/google/common/base/K;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/g;->d:I

    return-void
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
    .locals 5

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    new-instance v0, Lcom/google/googlenav/ui/android/multilinetextview/f;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/multilinetextview/f;-><init>()V

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/android/multilinetextview/e;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private b(Ljava/lang/String;Ljava/util/List;)I
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/multilinetextview/g;->b(Ljava/util/List;)I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method private b(Ljava/util/List;)I
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->c:Lcom/google/common/base/K;

    invoke-static {p1, v0}, Lcom/google/common/collect/aT;->b(Ljava/lang/Iterable;Lcom/google/common/base/K;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/g;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/g;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 5

    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->a:Lcom/google/common/base/S;

    invoke-virtual {v0, p1}, Lcom/google/common/base/S;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/android/multilinetextview/g;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/android/multilinetextview/g;->a(Ljava/lang/String;Ljava/util/List;)I

    move-result v2

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/g;->d(Ljava/util/List;)I

    move-result v0

    new-instance v3, Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, v0, v2, v4}, Lcom/google/googlenav/ui/android/multilinetextview/b;-><init>(III)V

    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Ljava/util/ListIterator;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/android/multilinetextview/g;->c(Ljava/lang/String;Ljava/util/List;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/List;)I
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/android/multilinetextview/g;->b:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p0}, Lcom/google/common/collect/dh;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;Ljava/util/List;)Ljava/lang/Iterable;
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->c()I

    move-result v4

    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method private d(Ljava/util/List;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/g;->b(Ljava/util/List;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/util/List;)I
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/multilinetextview/g;->b(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    invoke-static {p2}, Lcom/google/googlenav/ui/android/multilinetextview/g;->c(Ljava/util/List;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 1

    invoke-static {p1}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/g;->b(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/g;->a(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
