.class public Lcom/google/googlenav/ui/android/p;
.super Lcom/google/googlenav/ui/p;
.source "SourceFile"

# interfaces
.implements LaN/r;


# static fields
.field private static final A:Landroid/graphics/Rect;

.field private static final z:Landroid/graphics/Rect;


# instance fields
.field private e:I

.field private f:F

.field private g:I

.field private final h:Landroid/graphics/PointF;

.field private i:Z

.field private final j:Landroid/graphics/PointF;

.field private k:F

.field private l:J

.field private m:Landroid/graphics/PointF;

.field private n:Landroid/graphics/PointF;

.field private o:Landroid/graphics/PointF;

.field private p:F

.field private q:Z

.field private r:Landroid/graphics/Paint;

.field private s:Z

.field private t:J

.field private u:J

.field private v:Lcom/google/googlenav/ui/android/L;

.field private w:Lcom/google/googlenav/ui/bG;

.field private x:I

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/android/p;->z:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/android/p;->A:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(LaN/p;Lcom/google/googlenav/ui/android/L;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/high16 v1, 0x3f800000

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/p;-><init>(LaN/p;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->f:F

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->j:Landroid/graphics/PointF;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iput-wide v2, p0, Lcom/google/googlenav/ui/android/p;->l:J

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->p:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    iput-wide v2, p0, Lcom/google/googlenav/ui/android/p;->t:J

    iput-object p2, p0, Lcom/google/googlenav/ui/android/p;->v:Lcom/google/googlenav/ui/android/L;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    return-void
.end method

.method private a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->w:Lcom/google/googlenav/ui/bG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->w:Lcom/google/googlenav/ui/bG;

    invoke-interface {v0}, Lcom/google/googlenav/ui/bG;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->g:I

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->g:I

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->d:Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->p()V

    return-void
.end method

.method private a(ILandroid/graphics/PointF;F)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iget v3, p0, Lcom/google/googlenav/ui/android/p;->p:F

    const/high16 v4, 0x3f800000

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v5, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-static {p2, p3, v0}, Lcom/google/googlenav/ui/android/p;->a(Landroid/graphics/PointF;FLandroid/graphics/PointF;)V

    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    :goto_0
    iget v2, p0, Lcom/google/googlenav/ui/android/p;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/googlenav/ui/android/p;->y:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float v4, p3, v2

    sub-float v2, v4, v2

    add-float/2addr v1, v2

    mul-float v2, p3, v3

    sub-float/2addr v2, v3

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-static {p1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/Y;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x230

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/googlenav/ui/android/p;->u:J

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    neg-int v1, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    neg-int v0, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/p;->a(IIZ)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/p;->c(I)V

    return-void

    :cond_0
    move p3, v0

    move v0, v1

    move v1, v2

    goto :goto_0
.end method

.method private a(J)V
    .locals 5

    const/high16 v4, 0x437f0000

    iget-wide v0, p0, Lcom/google/googlenav/ui/android/p;->t:J

    const-wide/16 v2, 0xe6

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->p()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->q()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/google/googlenav/ui/android/p;->t:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43660000

    div-float/2addr v0, v1

    mul-float/2addr v0, v4

    sub-float v0, v4, v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/PointF;F)V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/android/p;->l:J

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->p:F

    const/high16 v3, 0x3f800000

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->p:F

    iput v2, p0, Lcom/google/googlenav/ui/android/p;->f:F

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    invoke-virtual {v2, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->k:F

    div-float/2addr v2, v0

    invoke-static {p1, v2, v1}, Lcom/google/googlenav/ui/android/p;->a(Landroid/graphics/PointF;FLandroid/graphics/PointF;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    invoke-virtual {v2, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    return-void
.end method

.method private static a(Landroid/graphics/PointF;FLandroid/graphics/PointF;)V
    .locals 3

    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p0, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    mul-float v2, v0, p1

    sub-float v0, v2, v0

    mul-float v2, v1, p1

    sub-float v1, v2, v1

    iget v2, p2, Landroid/graphics/PointF;->x:F

    sub-float v0, v2, v0

    iput v0, p2, Landroid/graphics/PointF;->x:F

    iget v0, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/PointF;->y:F

    return-void
.end method

.method private b(J)V
    .locals 5

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/google/googlenav/ui/android/p;->l:J

    const-wide/16 v2, 0x14a

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/google/googlenav/ui/android/p;->l:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43a50000

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iget v3, p0, Lcom/google/googlenav/ui/android/p;->f:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->p:F

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/google/googlenav/ui/android/p;->n:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/google/googlenav/ui/android/p;->o:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method private c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v1}, LaN/p;->d()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->e(LaN/B;)I

    move-result v0

    if-le p1, v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, LaN/p;->a(LaN/Y;)V

    :cond_0
    return-void
.end method

.method private static d(I)F
    .locals 3

    const/4 v2, 0x1

    if-ltz p0, :cond_0

    shl-int v0, v2, p0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    neg-int v1, p0

    shl-int v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v1}, LaN/p;->d()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->d(LaN/B;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/googlenav/ui/android/p;->u:J

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->w:Lcom/google/googlenav/ui/bG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->w:Lcom/google/googlenav/ui/bG;

    invoke-interface {v0}, Lcom/google/googlenav/ui/bG;->b()V

    :cond_0
    return-void
.end method

.method private n()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->v:Lcom/google/googlenav/ui/android/L;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->v:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/L;->c()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method private o()V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->r()I

    move-result v0

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/googlenav/ui/android/p;->y:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/android/p;->a(ILandroid/graphics/PointF;)V

    return-void
.end method

.method private p()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method private q()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/PointF;->set(FF)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->k:F

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->p:F

    return-void
.end method

.method private r()I
    .locals 8

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    move v3, v4

    :goto_0
    if-eqz v3, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    div-float v0, v2, v0

    :goto_1
    float-to-int v2, v0

    :goto_2
    if-le v2, v4, :cond_2

    shr-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    move v3, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    goto :goto_1

    :cond_2
    const-wide/high16 v4, 0x3ff0000000000000L

    float-to-double v6, v0

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fd0000000000000L

    cmpl-double v0, v4, v6

    if-lez v0, :cond_4

    add-int/lit8 v0, v1, 0x1

    :goto_3
    iget v1, p0, Lcom/google/googlenav/ui/android/p;->e:I

    if-eqz v3, :cond_3

    neg-int v0, v0

    :cond_3
    add-int/2addr v0, v1

    return v0

    :cond_4
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public a(I)I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    :goto_0
    iget v1, p0, Lcom/google/googlenav/ui/android/p;->e:I

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v1, p1}, LaN/Y;->a(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/Y;->a(I)I

    move-result v0

    goto :goto_1
.end method

.method public a(ILandroid/graphics/PointF;)V
    .locals 3

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    sub-int v0, p1, v0

    invoke-static {v0}, Lcom/google/googlenav/ui/android/p;->d(I)F

    move-result v1

    invoke-direct {p0, p2, v1}, Lcom/google/googlenav/ui/android/p;->a(Landroid/graphics/PointF;F)V

    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->e:I

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    invoke-direct {p0, p1, p2, v1}, Lcom/google/googlenav/ui/android/p;->a(ILandroid/graphics/PointF;F)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(LaN/Y;II)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/high16 v0, 0x3f800000

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/p;->a(F)V

    invoke-virtual {p1}, LaN/Y;->a()I

    move-result v0

    new-instance v1, Landroid/graphics/PointF;

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/android/p;->a(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 4

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget v0, p0, Lcom/google/googlenav/ui/android/p;->p:F

    :cond_0
    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget v3, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->k()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->l()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public a(Lat/b;)V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->k()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->l()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->k()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->l()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    :cond_2
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/p;->a(Lat/b;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/bG;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/p;->w:Lcom/google/googlenav/ui/bG;

    return-void
.end method

.method public a(LaD/q;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v0, 0x1

    invoke-virtual {p1}, LaD/q;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->e:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->e:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->r()I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->q()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->p()V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v1}, LaN/p;->c()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v2}, LaN/p;->t()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v3}, LaN/p;->s()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/googlenav/ui/android/p;->a(LaN/Y;II)V

    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LaD/q;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, LaD/q;->c()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/android/p;->a(F)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, LaD/q;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/p;->d:Z

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->e:I

    if-eq v2, v4, :cond_5

    invoke-virtual {p1}, LaD/q;->c()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3fefae147ae147aeL

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    invoke-virtual {p1}, LaD/q;->c()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3ff003afb7e90ff9L

    cmpg-double v2, v2, v4

    if-gez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->j:Landroid/graphics/PointF;

    invoke-virtual {p1}, LaD/q;->a()F

    move-result v2

    invoke-virtual {p1}, LaD/q;->b()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    iget v1, p0, Lcom/google/googlenav/ui/android/p;->f:F

    invoke-virtual {p1}, LaD/q;->c()F

    move-result v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget-object v1, p0, Lcom/google/googlenav/ui/android/p;->j:Landroid/graphics/PointF;

    invoke-virtual {p1}, LaD/q;->c()F

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/android/p;->a(Landroid/graphics/PointF;FLandroid/graphics/PointF;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->r()I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/android/p;->g:I

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, LaD/q;->f()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/google/googlenav/ui/android/p;->d:Z

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->e:I

    if-eq v2, v4, :cond_6

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->o()V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Lan/e;Lam/f;IIIIIIII)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p2, Lan/f;

    invoke-virtual {p2}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/android/p;->z:Landroid/graphics/Rect;

    add-int v2, p7, p9

    add-int v3, p8, p10

    invoke-virtual {v0, p7, p8, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/googlenav/ui/android/p;->A:Landroid/graphics/Rect;

    add-int v2, p3, p5

    add-int v3, p4, p6

    invoke-virtual {v0, p3, p4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/android/p;->z:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/googlenav/ui/android/p;->A:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/p;->b(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->q()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->p()V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 13

    const/high16 v12, 0x40000000

    const/4 v4, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->e()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->x:I

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->f()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/p;->y:I

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->f:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->h:Landroid/graphics/PointF;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v8

    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->d:Z

    if-nez v3, :cond_6

    move v5, v4

    :goto_0
    if-eqz v5, :cond_7

    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    iget-object v6, p0, Lcom/google/googlenav/ui/android/p;->a:LaN/p;

    invoke-virtual {v6}, LaN/p;->d()LaN/B;

    move-result-object v6

    invoke-virtual {v3, v6, v4}, LaN/p;->a(LaN/B;Z)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    :goto_1
    if-nez v3, :cond_0

    if-eqz v5, :cond_1

    iget-wide v5, p0, Lcom/google/googlenav/ui/android/p;->l:J

    sub-long v5, v8, v5

    const-wide/16 v10, 0x3a98

    cmp-long v3, v5, v10

    if-lez v3, :cond_1

    :cond_0
    iput-boolean v4, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    iput-wide v8, p0, Lcom/google/googlenav/ui/android/p;->t:J

    :cond_1
    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    if-eqz v3, :cond_2

    invoke-direct {p0, v8, v9}, Lcom/google/googlenav/ui/android/p;->a(J)V

    :cond_2
    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/googlenav/ui/android/p;->d:Z

    if-nez v3, :cond_3

    invoke-direct {p0, v8, v9}, Lcom/google/googlenav/ui/android/p;->b(J)V

    iget v2, p0, Lcom/google/googlenav/ui/android/p;->p:F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->m:Landroid/graphics/PointF;

    :cond_3
    iget-wide v3, p0, Lcom/google/googlenav/ui/android/p;->u:J

    cmp-long v3, v3, v8

    if-gez v3, :cond_4

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->m()V

    :cond_4
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, p0, Lcom/google/googlenav/ui/android/p;->x:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    div-float/2addr v4, v12

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->k()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v4, p0, Lcom/google/googlenav/ui/android/p;->y:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    div-float/2addr v4, v12

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->l()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->b()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    iget-object v5, p0, Lcom/google/googlenav/ui/android/p;->b:Lam/f;

    invoke-interface {v5}, Lam/f;->a()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    check-cast v1, Lan/e;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/p;->b:Lam/f;

    float-to-int v5, v5

    float-to-int v6, v0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->a()I

    move-result v9

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->b()I

    move-result v10

    move-object v0, p0

    move v8, v7

    invoke-virtual/range {v0 .. v10}, Lcom/google/googlenav/ui/android/p;->a(Lan/e;Lam/f;IIIIIIII)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/p;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/p;->n()V

    :cond_5
    return-void

    :cond_6
    move v5, v7

    goto/16 :goto_0

    :cond_7
    move v3, v7

    goto/16 :goto_1
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/p;->r:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/p;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->q:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/p;->s:Z

    return v0
.end method
