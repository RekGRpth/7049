.class public Lcom/google/googlenav/ui/android/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[F

.field public static final b:[F

.field public static final c:[F

.field public static final d:[F


# instance fields
.field public volatile e:Ljava/lang/String;

.field public volatile f:I

.field public volatile g:[F

.field public volatile h:[F

.field public volatile i:Z

.field public volatile j:Z

.field private k:Lcom/google/googlenav/ui/android/r;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/ui/android/w;->a:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/ui/android/w;->b:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/googlenav/ui/android/w;->c:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/googlenav/ui/android/w;->d:[F

    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3e4ccccd
        0x3f4ccccd
        0x3f800000
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        0x3f4ccccd
        0x3f4ccccd
        0x3f4ccccd
        0x3f4ccccd
    .end array-data

    :array_3
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x3f4ccccd
    .end array-data
.end method

.method private constructor <init>(Lcom/google/googlenav/ui/android/r;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/googlenav/ui/android/w;->c:[F

    iput-object v0, p0, Lcom/google/googlenav/ui/android/w;->g:[F

    sget-object v0, Lcom/google/googlenav/ui/android/w;->b:[F

    iput-object v0, p0, Lcom/google/googlenav/ui/android/w;->h:[F

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/w;->i:Z

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/w;->j:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/android/w;->k:Lcom/google/googlenav/ui/android/r;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/w;-><init>(Lcom/google/googlenav/ui/android/r;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/w;->k:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->q(Lcom/google/googlenav/ui/android/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/w;->k:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->r(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->l()V

    return-void
.end method
