.class Lcom/google/googlenav/ui/android/y;
.super LS/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/r;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/r;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-direct {p0}, LS/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/y;-><init>(Lcom/google/googlenav/ui/android/r;)V

    return-void
.end method

.method private a(FF)Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->m()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p2, v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->m()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p2

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v0

    invoke-virtual {v0}, LD/b;->g()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;LD/b;)LD/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 11

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->f(Lcom/google/googlenav/ui/android/r;)LC/a;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;LC/a;)LC/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/r;->c()V

    :cond_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->g(Lcom/google/googlenav/ui/android/r;)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->d()LE/o;

    move-result-object v0

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x3e99999a

    invoke-interface {v9, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->e()LE/d;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v2

    iget-object v2, v2, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v3

    iget-object v3, v3, Lcom/google/googlenav/ui/android/w;->h:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-interface {v9, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->f()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->f()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL10;->glPointSize(F)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->g()LE/d;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->h()LE/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/googlenav/ui/android/w;->j:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/y;->j()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/googlenav/ui/android/w;->j:Z

    iget-object v10, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->i(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/driveabout/vector/aV;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v1

    iget-object v2, v1, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->i()I

    move-result v1

    int-to-float v5, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v1

    iget v6, v1, Lcom/google/googlenav/ui/android/w;->f:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;LD/b;)LD/b;

    iget-object v6, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->i(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/driveabout/vector/aV;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v1

    iget-object v1, v1, Lcom/google/googlenav/ui/android/w;->e:Ljava/lang/String;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->i()I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;[F)[F

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v0

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v0

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v1

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LE/i;->a(FF)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, LE/i;->a(FF)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LE/i;->a(FF)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->e(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p2}, LC/a;->k()I

    move-result v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->as()Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    :goto_1
    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LE/o;->a(LD/a;)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    const/4 v3, 0x0

    int-to-float v4, v0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LE/o;->a(FFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    const/4 v3, 0x0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v5}, Lcom/google/googlenav/ui/android/r;->k(Lcom/google/googlenav/ui/android/r;)[F

    move-result-object v5

    const/4 v6, 0x1

    aget v5, v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LE/o;->a(FFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    int-to-float v3, v1

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v5}, Lcom/google/googlenav/ui/android/r;->k(Lcom/google/googlenav/ui/android/r;)[F

    move-result-object v5

    const/4 v6, 0x1

    aget v5, v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LE/o;->a(FFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    int-to-float v3, v1

    int-to-float v4, v0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LE/o;->a(FFF)V

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v2

    iget-object v2, v2, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v3

    iget-object v3, v3, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v4}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v4

    iget-object v4, v4, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v5}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v5

    iget-object v5, v5, Lcom/google/googlenav/ui/android/w;->g:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    invoke-interface {v9, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->j()LE/o;

    move-result-object v2

    invoke-virtual {v2, p1}, LE/o;->d(LD/a;)V

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->k()LE/d;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, p1, v3}, LE/d;->a(LD/a;I)V

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->k(Lcom/google/googlenav/ui/android/r;)[F

    move-result-object v2

    const/4 v3, 0x0

    aget v2, v2, v3

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    int-to-float v0, v0

    const/4 v2, 0x0

    invoke-interface {v9, v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->k(Lcom/google/googlenav/ui/android/r;)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->k(Lcom/google/googlenav/ui/android/r;)[F

    move-result-object v1

    const/4 v2, 0x1

    aget v1, v1, v2

    const/high16 v2, 0x3f800000

    invoke-interface {v9, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, LD/a;->p()V

    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x45f00800

    invoke-interface {v9, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->j(Lcom/google/googlenav/ui/android/r;)LE/i;

    move-result-object v0

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p1, LD/a;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->h(Lcom/google/googlenav/ui/android/r;)LD/b;

    move-result-object v0

    invoke-virtual {v0, v9}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {v9, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, LD/a;->q()V

    :cond_3
    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public b(FFLC/a;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->l(Lcom/google/googlenav/ui/android/r;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->m(Lcom/google/googlenav/ui/android/r;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->l(Lcom/google/googlenav/ui/android/r;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->l()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    float-to-int v2, p1

    iput v2, v1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->b(Lcom/google/googlenav/ui/android/r;Z)Z

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->l()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p2, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    float-to-int v2, p2

    iput v2, v1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->b(Lcom/google/googlenav/ui/android/r;Z)Z

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->l()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    float-to-int v2, p1

    iput v2, v1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->b(Lcom/google/googlenav/ui/android/r;Z)Z

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {}, Lcom/google/googlenav/ui/android/r;->l()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, p2, v1

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    float-to-int v2, p2

    iput v2, v1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->b(Lcom/google/googlenav/ui/android/r;Z)Z

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-direct {p0, p2, v1}, Lcom/google/googlenav/ui/android/y;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;Z)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;I)I

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    invoke-direct {p0, p1, v1}, Lcom/google/googlenav/ui/android/y;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;Z)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;I)I

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-direct {p0, p2, v1}, Lcom/google/googlenav/ui/android/y;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;Z)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;I)I

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    invoke-direct {p0, p1, v1}, Lcom/google/googlenav/ui/android/y;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;Z)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;I)I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;LC/a;)LC/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/r;->c()V

    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/y;->j()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->i(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/driveabout/vector/aV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    return-void
.end method

.method public d(FFLo/T;LC/a;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/r;->l(Lcom/google/googlenav/ui/android/r;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/r;Z)Z

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public j_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/y;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->p(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/x;->b()Z

    move-result v0

    return v0
.end method
