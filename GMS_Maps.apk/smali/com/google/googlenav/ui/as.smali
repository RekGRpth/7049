.class public Lcom/google/googlenav/ui/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/au;

.field private final b:Lcom/google/android/maps/MapsActivity;

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/MapsActivity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/googlenav/ui/as;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/as;->d:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    new-instance v0, Lcom/google/googlenav/ui/at;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/at;-><init>(Lcom/google/googlenav/ui/as;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/au;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/au;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/as;->b()V

    return-void
.end method

.method public a(Z)V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/as;->c:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    iput-boolean v1, p0, Lcom/google/googlenav/ui/as;->d:Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/as;->d:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/as;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/as;->c:Z

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->b:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/as;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/au;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/ui/au;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/au;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/as;->a(Z)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/as;->d:Z

    return v0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    instance-of v0, v0, Lbf/av;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/as;->a:Lcom/google/googlenav/ui/au;

    check-cast v0, Lbf/av;

    invoke-virtual {v0}, Lbf/av;->a()Lcom/google/googlenav/actionbar/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/googlenav/actionbar/b;)V

    :cond_0
    return-void
.end method
