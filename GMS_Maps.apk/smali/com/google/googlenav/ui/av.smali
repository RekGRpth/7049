.class public Lcom/google/googlenav/ui/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;

.field private final b:Lbf/am;

.field private final c:Lcom/google/googlenav/android/aa;

.field private d:Z

.field private e:LaM/g;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lbf/am;Lcom/google/googlenav/android/aa;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/av;->d:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p2, p0, Lcom/google/googlenav/ui/av;->b:Lbf/am;

    iput-object p3, p0, Lcom/google/googlenav/ui/av;->c:Lcom/google/googlenav/android/aa;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/av;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/av;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/av;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/av;)Lbf/am;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/av;->b:Lbf/am;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/av;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/av;->d:Z

    return v0
.end method


# virtual methods
.method public a()LaM/g;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/ax;-><init>(Lcom/google/googlenav/ui/av;Lcom/google/googlenav/ui/aw;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    return-object v0
.end method

.method public b()V
    .locals 3

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->g()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/av;->c:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/aw;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/aw;-><init>(Lcom/google/googlenav/ui/av;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0
.end method
