.class public Lcom/google/googlenav/ui/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/F;


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:Lbf/m;

.field private final c:Lcom/google/googlenav/ui/br;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;LaB/s;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/ay;->a:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/ay;->b:Lbf/m;

    new-instance v0, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->r()Lam/f;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/ay;->c:Lcom/google/googlenav/ui/br;

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/bs;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->h(I)Lcom/google/googlenav/ap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ay;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/aB;

    iget-object v3, p0, Lcom/google/googlenav/ui/ay;->b:Lbf/m;

    invoke-direct {v2, p1, v0, v3, p0}, Lcom/google/googlenav/ui/aB;-><init>(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;Lbf/m;Lcom/google/googlenav/ui/ay;)V

    invoke-virtual {v1, v0, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/aA;->b()Landroid/widget/TextView;

    move-result-object v3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/ay;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v0, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ay;Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/ay;->a(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/view/View;)Lbj/bB;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/ay;->b(Landroid/view/View;)Lcom/google/googlenav/ui/aA;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lcom/google/googlenav/ui/aA;

    invoke-virtual {p2}, Lcom/google/googlenav/ui/aA;->a()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ay;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/googlenav/ui/aA;->b()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ay;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ba()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ay;->a:Lcom/google/googlenav/ai;

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/ay;->a(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/ay;->a(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040106

    return v0
.end method

.method public b(Landroid/view/View;)Lcom/google/googlenav/ui/aA;
    .locals 4

    new-instance v2, Lcom/google/googlenav/ui/aA;

    const v0, 0x7f1000ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100231

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/googlenav/ui/aA;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/google/googlenav/ui/az;)V

    return-object v2
.end method
