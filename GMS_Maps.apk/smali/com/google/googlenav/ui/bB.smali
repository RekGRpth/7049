.class public Lcom/google/googlenav/ui/bB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/F;


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ai;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    iput-boolean p2, p0, Lcom/google/googlenav/ui/bB;->b:Z

    return-void
.end method

.method public static a(Lcom/google/googlenav/ai;Z)Lcom/google/googlenav/ui/bB;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/ui/bB;->a(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/googlenav/ui/bB;->b(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/bB;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/bB;-><init>(Lcom/google/googlenav/ai;Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/googlenav/ai;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aj()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ag()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    check-cast p0, Lcom/google/googlenav/W;

    invoke-virtual {p0}, Lcom/google/googlenav/W;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private static b(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(Landroid/view/View;)Lbj/bB;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/bB;->b(Landroid/view/View;)Lcom/google/googlenav/ui/bD;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 5

    check-cast p2, Lcom/google/googlenav/ui/bD;

    iget-object v0, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bn()Z

    move-result v1

    iget-object v3, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->T()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/googlenav/ui/bC;->a(ZILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/googlenav/ui/bB;->b:Z

    invoke-static {p2, v2, v0, v1, v3}, Lcom/google/googlenav/ui/bC;->a(Lcom/google/googlenav/ui/bD;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/bB;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0f0106

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0401d7

    return v0
.end method

.method public b(Landroid/view/View;)Lcom/google/googlenav/ui/bD;
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/bC;->a(Landroid/view/View;)Lcom/google/googlenav/ui/bD;

    move-result-object v0

    return-object v0
.end method
