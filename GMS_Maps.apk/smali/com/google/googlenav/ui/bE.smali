.class public Lcom/google/googlenav/ui/bE;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/c;


# instance fields
.field private final a:LaN/u;

.field private b:Lcom/google/googlenav/ui/view/d;

.field private c:Lcom/google/googlenav/ui/view/d;

.field private final d:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(LaN/u;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/googlenav/ui/bE;->d:Landroid/graphics/Point;

    iput-object p1, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aC()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->A()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/bE;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/bE;->d:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/bE;->d:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    :cond_1
    return-void
.end method

.method public a(Lat/b;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/b;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v2

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LaN/u;->a(ZII)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {v2, v1}, LaN/u;->a(Z)Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->a(Z)Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->b:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->c:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {v0}, LaN/u;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/bE;->a(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {v0}, LaN/u;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/bE;->b(Z)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/bE;->a:LaN/u;

    invoke-virtual {v0}, LaN/u;->q()V

    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/bE;->a(Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/bE;->b(Z)V

    goto :goto_1
.end method
