.class public abstract Lcom/google/googlenav/ui/bF;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/r;
.implements Lcom/google/googlenav/common/h;


# instance fields
.field protected final a:LaN/p;

.field protected b:Z

.field protected c:Z

.field private d:Lcom/google/googlenav/ui/bG;


# direct methods
.method public constructor <init>(LaN/p;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/bF;->a:LaN/p;

    iput-boolean v0, p0, Lcom/google/googlenav/ui/bF;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/bF;->b:Z

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/bF;->a:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/Y;->a(I)I

    move-result v0

    return v0
.end method

.method protected abstract a()V
.end method

.method public final a(LaN/Y;II)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/bF;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/googlenav/ui/bF;->b(LaN/Y;II)V

    return-void
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/bG;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/bF;->d:Lcom/google/googlenav/ui/bG;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method protected abstract b()V
.end method

.method protected abstract b(LaN/Y;II)V
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/bF;->c:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/googlenav/ui/bF;->c:Z

    iput-boolean v1, p0, Lcom/google/googlenav/ui/bF;->b:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/bF;->b()V

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    return-void
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/bF;->b:Z

    return v0
.end method

.method public abstract g()Z
.end method
