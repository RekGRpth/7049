.class public Lcom/google/googlenav/ui/ba;
.super Landroid/support/v4/view/x;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ai;
.implements Landroid/widget/TabHost$OnTabChangeListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

.field private final c:Landroid/support/v4/view/ViewPager;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/google/googlenav/ui/bc;

.field private final f:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;Landroid/support/v4/view/ViewPager;Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/bc;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/view/x;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ba;->f:Ljava/util/List;

    iput-object p1, p0, Lcom/google/googlenav/ui/ba;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iput-object p3, p0, Lcom/google/googlenav/ui/ba;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p2, p0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ai;)V

    iput-object p4, p0, Lcom/google/googlenav/ui/ba;->d:Landroid/view/LayoutInflater;

    iput-object p5, p0, Lcom/google/googlenav/ui/ba;->e:Lcom/google/googlenav/ui/bc;

    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040186

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100034

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast p1, Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    return-object v0
.end method

.method public a(IFI)V
    .locals 0

    return-void
.end method

.method public a(ILandroid/view/View;)V
    .locals 3

    invoke-static {p1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    new-instance v0, Lcom/google/googlenav/ui/bb;

    iget-object v2, p0, Lcom/google/googlenav/ui/ba;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/bb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ba;->c()V

    return-void
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public b_(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setCurrentTab(I)V

    return-void
.end method

.method public c_(I)V
    .locals 0

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->getCurrentTab()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/ba;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/ba;->e:Lcom/google/googlenav/ui/bc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/ba;->e:Lcom/google/googlenav/ui/bc;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/bc;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/ba;->b:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iget-object v2, p0, Lcom/google/googlenav/ui/ba;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setCurrentTabContentView(Landroid/view/View;)V

    return-void
.end method
