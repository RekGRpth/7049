.class public Lcom/google/googlenav/ui/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I
    .locals 3

    const/4 v2, 0x1

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/f;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ltz v0, :cond_1

    if-nez v1, :cond_0

    if-gt v0, v2, :cond_1

    :cond_0
    if-ne v1, v2, :cond_2

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :cond_2
    return v0
.end method

.method private static a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-object v1, p0, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ne v1, p1, :cond_0

    aget-object v0, p0, v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public static a(Lax/b;ILcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/C;
    .locals 8

    const/4 v3, 0x0

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/o;->a(I)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {p0}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    new-instance v0, Lcom/google/googlenav/ui/view/q;

    invoke-static {p1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [I

    new-array v3, v3, [I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/q;-><init>(Ljava/lang/String;[I[ILjava/util/List;Z)V

    invoke-static {v0, v6, v7}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;Ljava/util/Vector;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/view/q;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/C;

    invoke-direct {v1, p2, v0}, Lcom/google/googlenav/ui/view/android/C;-><init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/q;)V

    return-object v1
.end method

.method public static a(Lcom/google/googlenav/ui/view/q;Ljava/util/Vector;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/view/q;
    .locals 12

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v4, v3, [I

    new-array v5, v3, [I

    invoke-static {v3}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    aput v1, v4, v2

    invoke-static {v0, p2, v1}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    aput v1, v5, v2

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-nez v1, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/aV;->bA:Lcom/google/googlenav/ui/aV;

    invoke-static {v7, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v8, 0x1

    if-ne v1, v8, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v8, :cond_2

    const/4 v10, 0x4

    invoke-virtual {v0, v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    const/4 v11, 0x6

    invoke-virtual {v10, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/google/googlenav/ui/aV;->av:Lcom/google/googlenav/ui/aV;

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-static {v7, v9}, Lcom/google/googlenav/ui/k;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/googlenav/ui/k;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/google/googlenav/ui/view/q;->d:[I

    iput-object v5, p0, Lcom/google/googlenav/ui/view/q;->e:[I

    iput-object v6, p0, Lcom/google/googlenav/ui/view/q;->f:Ljava/util/List;

    return-object p0
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f100171

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method private static a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/view/q;ILcom/google/googlenav/ui/view/android/S;)V
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p4}, Lcom/google/googlenav/ui/view/android/S;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040065

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100187

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f100188

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/q;->e:[I

    aget v3, v3, p3

    if-ne v3, v2, :cond_0

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v2, Lcom/google/googlenav/ui/i;

    invoke-direct {v2, p2, p3}, Lcom/google/googlenav/ui/i;-><init>(Lcom/google/googlenav/ui/view/q;I)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v2, Lcom/google/googlenav/ui/j;

    invoke-direct {v2, v1}, Lcom/google/googlenav/ui/j;-><init>(Landroid/widget/CheckBox;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/k;Lcom/google/googlenav/ui/view/q;ILcom/google/googlenav/ui/view/android/S;)V
    .locals 5

    invoke-virtual {p4}, Lcom/google/googlenav/ui/view/android/S;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040066

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10018a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/google/googlenav/ui/k;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f10018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    new-instance v1, Lcom/google/googlenav/ui/view/android/Q;

    invoke-virtual {p4}, Lcom/google/googlenav/ui/view/android/S;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p1, Lcom/google/googlenav/ui/k;->b:Ljava/util/List;

    invoke-virtual {p4}, Lcom/google/googlenav/ui/view/android/S;->r()Lcom/google/googlenav/ui/e;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/Q;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p1, Lcom/google/googlenav/ui/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v1, p2, Lcom/google/googlenav/ui/view/q;->e:[I

    aget v1, v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v1, Lcom/google/googlenav/ui/h;

    invoke-direct {v1, p2, p3}, Lcom/google/googlenav/ui/h;-><init>(Lcom/google/googlenav/ui/view/q;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void

    :cond_0
    iget-object v2, p1, Lcom/google/googlenav/ui/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Lax/b;Lcom/google/googlenav/ui/view/q;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/f;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    invoke-virtual {p0, v0}, Lax/b;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ui/view/android/S;Landroid/view/View;Lcom/google/googlenav/ui/view/q;)V
    .locals 6

    const v0, 0x7f100340

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/q;->c:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p2, Lcom/google/googlenav/ui/view/q;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p2, Lcom/google/googlenav/ui/view/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const v0, 0x7f100171

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v1, 0x0

    iget-object v2, p2, Lcom/google/googlenav/ui/view/q;->d:[I

    array-length v4, v2

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v5, v1, Lcom/google/googlenav/ui/k;

    if-eqz v5, :cond_2

    check-cast v1, Lcom/google/googlenav/ui/k;

    invoke-static {v0, v1, p2, v2, p0}, Lcom/google/googlenav/ui/f;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/k;Lcom/google/googlenav/ui/view/q;ILcom/google/googlenav/ui/view/android/S;)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    instance-of v5, v1, Lcom/google/googlenav/ui/aW;

    if-eqz v5, :cond_1

    check-cast v1, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1, p2, v2, p0}, Lcom/google/googlenav/ui/f;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/view/q;ILcom/google/googlenav/ui/view/android/S;)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p2, Lcom/google/googlenav/ui/view/q;->g:Z

    if-eqz v0, :cond_4

    const v0, 0x7f100163

    iget-object v1, p2, Lcom/google/googlenav/ui/view/q;->h:Lcom/google/googlenav/ui/aW;

    new-instance v2, Lcom/google/googlenav/ui/g;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/g;-><init>(Lcom/google/googlenav/ui/view/android/S;)V

    invoke-static {v0, v1, v2, p1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    :cond_4
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x4

    const-string v0, "o"

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "a="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "o="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .locals 8

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget-object v2, p0, v0

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_2

    aget-object v0, p1, v1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const-string v0, "c"

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v2, v5, p2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move v0, v3

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    if-nez v2, :cond_3

    const-string v0, "nc"

    const-string v1, ""

    const-string v2, ""

    invoke-static {v0, v1, v2, p2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public static a(Lax/b;)Z
    .locals 2

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/o;->a(I)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ui/view/q;)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/q;->e:[I

    array-length v0, v0

    new-array v1, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/q;->d:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/q;->e:[I

    aget v3, v3, v0

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/f;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
