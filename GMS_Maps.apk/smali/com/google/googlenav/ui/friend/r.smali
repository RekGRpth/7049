.class public Lcom/google/googlenav/ui/friend/r;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private final a:Lcom/google/googlenav/ui/friend/q;

.field private final b:Lbf/ag;

.field private final c:Lcom/google/googlenav/ui/aa;

.field private d:Lcom/google/googlenav/ui/view/android/J;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ListView;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Lcom/google/googlenav/ui/friend/A;

.field private final s:Lcom/google/googlenav/ui/friend/B;

.field private t:Landroid/view/MenuItem;

.field private u:Landroid/view/MenuItem;

.field private v:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Lbf/ag;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/friend/q;Lcom/google/googlenav/ui/aa;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/S;->p()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    iput-object p3, p0, Lcom/google/googlenav/ui/friend/r;->a:Lcom/google/googlenav/ui/friend/q;

    iput-object p4, p0, Lcom/google/googlenav/ui/friend/r;->c:Lcom/google/googlenav/ui/aa;

    new-instance v0, Lcom/google/googlenav/ui/friend/B;

    invoke-direct {v0, p2}, Lcom/google/googlenav/ui/friend/B;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->s:Lcom/google/googlenav/ui/friend/B;

    return-void
.end method

.method private A()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->a:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/q;->c()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaL/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaL/d;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->q:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->q:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private B()Ljava/util/List;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->c()Lcom/google/googlenav/friend/aK;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    iget-object v2, p0, Lcom/google/googlenav/ui/friend/r;->c:Lcom/google/googlenav/ui/aa;

    iget-object v3, p0, Lcom/google/googlenav/ui/friend/r;->s:Lcom/google/googlenav/ui/friend/B;

    iget-object v4, p0, Lcom/google/googlenav/ui/friend/r;->a:Lcom/google/googlenav/ui/friend/q;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/googlenav/ui/friend/r;->a(Lbf/ag;Lcom/google/googlenav/ui/aa;Lcom/google/googlenav/friend/L;Lcom/google/googlenav/ui/friend/q;Ljava/util/List;)V

    return-object v0
.end method

.method private C()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->w()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->m()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->l()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->D()V

    return-void
.end method

.method private D()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bT()Z

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->t:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->t:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->t:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->u:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->u:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->u:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->v:Landroid/view/MenuItem;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->v:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->v:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    const v0, 0x7f040086

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1001ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->n:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->n:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/friend/t;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/t;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1001eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->o:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->o:Landroid/widget/Button;

    const/16 v2, 0x189

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->o:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/friend/u;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/u;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1001ec

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->p:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->p:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/friend/v;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/v;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1001ed

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->q:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->q:Landroid/widget/Button;

    const/16 v2, 0x19c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->q:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/friend/w;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/w;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f100239

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f10001e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x215

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/friend/A;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/friend/A;-><init>(Lcom/google/googlenav/ui/friend/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iput-object v1, v0, Lcom/google/googlenav/ui/friend/A;->a:Landroid/view/View;

    const v0, 0x7f1001a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/friend/x;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/x;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->j()Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const v0, 0x7f1001a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/friend/y;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/y;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    const v2, 0x7f1001a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/friend/A;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    const v2, 0x7f1001e7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/friend/A;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, v0, Lcom/google/googlenav/ui/friend/A;->b:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/friend/z;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/z;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->m()V

    goto/16 :goto_0
.end method

.method private static a(Lbf/ag;Lcom/google/googlenav/ui/aa;Lcom/google/googlenav/friend/L;Lcom/google/googlenav/ui/friend/q;Ljava/util/List;)V
    .locals 11

    invoke-interface {p0}, Lbf/ag;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v9

    invoke-interface {p0}, Lbf/ag;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->f()Z

    move-result v10

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lbd/d;

    invoke-virtual {v8}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v1

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lbd/d;-><init>(Lcom/google/googlenav/friend/aI;Lcom/google/googlenav/ui/aa;Lbf/ag;Lcom/google/googlenav/friend/L;I)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x1

    if-le v9, v0, :cond_5

    invoke-interface {p0}, Lbf/ag;->bT()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lbj/bv;

    const/4 v1, 0x1

    const v2, 0x7f0400cd

    const/16 v3, 0xe0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v1, 0x0

    const/4 v0, 0x1

    move v7, v0

    move v0, v1

    :goto_0
    if-ge v7, v9, :cond_5

    invoke-virtual {v8, v7}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p3}, Lcom/google/googlenav/ui/friend/q;->d()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v10, :cond_2

    :cond_4
    if-nez v0, :cond_6

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v0, Lbj/bv;

    const/4 v2, 0x1

    const v3, 0x7f0400cd

    const/16 v4, 0x57a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    move v6, v0

    :goto_2
    new-instance v0, Lbd/d;

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lbd/d;-><init>(Lcom/google/googlenav/friend/aI;Lcom/google/googlenav/ui/aa;Lbf/ag;Lcom/google/googlenav/friend/L;I)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v6

    goto :goto_1

    :cond_5
    return-void

    :cond_6
    move v6, v0

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/friend/r;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/friend/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->m()V

    return-void
.end method

.method private l()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bT()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/friend/p;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, v0, Lcom/google/googlenav/ui/friend/A;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, v0, Lcom/google/googlenav/ui/friend/A;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, v0, Lcom/google/googlenav/ui/friend/A;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->r:Lcom/google/googlenav/ui/friend/A;

    iget-object v0, v0, Lcom/google/googlenav/ui/friend/A;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bS()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v1}, Lbf/ag;->bS()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bS()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->x()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->y()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->z()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->A()V

    return-void
.end method

.method private x()V
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->n:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->n:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_1
    if-nez v1, :cond_2

    const/16 v0, 0x15b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v0, 0x158

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x156

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->n:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private y()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bT()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->o:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->o:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private z()V
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bT()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    if-ne v1, v2, :cond_2

    const/16 v0, 0x164

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->p:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->p:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-le v1, v2, :cond_0

    const/16 v0, 0x161

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->p:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public Q_()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/H;

    instance-of v2, v0, LaB/p;

    if-eqz v2, :cond_0

    check-cast v0, LaB/p;

    invoke-interface {v0}, LaB/p;->Q_()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/r;->k()V

    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const v0, 0x7f020288

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    const/16 v0, 0x215

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 3

    const/4 v1, -0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    invoke-interface {v2, v0, v1, p2}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x12d

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x834

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x13d

    goto :goto_0

    :sswitch_4
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v2, 0x18e

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14e

    goto :goto_0

    :cond_0
    const/16 v0, 0x14f

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x13b

    goto :goto_0

    :sswitch_6
    const/16 v0, 0xa8c

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1001e3 -> :sswitch_2
        0x7f1001e5 -> :sswitch_3
        0x7f10021b -> :sswitch_1
        0x7f1004b4 -> :sswitch_0
        0x7f1004b5 -> :sswitch_4
        0x7f1004b6 -> :sswitch_5
        0x7f1004b7 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 2

    const v0, 0x7f1004b5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->v:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->a:Lcom/google/googlenav/ui/friend/q;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/friend/q;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x190

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->v:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0

    :cond_0
    const/16 v0, 0x18e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const v0, 0x7f020219

    new-instance v1, Lcom/google/googlenav/ui/friend/s;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/s;-><init>(Lcom/google/googlenav/ui/friend/r;)V

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {p0, v6, v0, v1, v2}, Lcom/google/googlenav/ui/friend/r;->a(ZILaA/f;[I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/r;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f040087

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/googlenav/ui/friend/r;->a(Landroid/view/LayoutInflater;Landroid/view/View;)V

    const v0, 0x7f1001ee

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->l:Landroid/view/View;

    const v3, 0x7f1001ef

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x25b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/friend/r;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->w()V

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/r;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/friend/r;->f:Lcom/google/googlenav/ui/e;

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->B()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x3

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->l()V

    return-object v2

    nop

    :array_0
    .array-data 4
        0xbbe
        0xbbd
    .end array-data
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->b:Lbf/ag;

    invoke-interface {v0}, Lbf/ag;->bT()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->B()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->C()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->a()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->C()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/friend/r;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x507

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f10021b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->t:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->t:Landroid/view/MenuItem;

    const/16 v1, 0x19e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1001e5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/r;->u:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/r;->u:Landroid/view/MenuItem;

    const/16 v1, 0x183

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1001e3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x70

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f1004b6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x29d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1004b7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x1d9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/r;->D()V

    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x195

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
