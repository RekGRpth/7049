.class public Lcom/google/googlenav/ui/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/r;
.implements Lcom/google/googlenav/common/util/n;


# static fields
.field private static final e:I


# instance fields
.field protected final a:LaN/p;

.field protected b:Lam/f;

.field protected final c:Landroid/graphics/Point;

.field protected d:Z

.field private f:Lam/e;

.field private g:Lcom/google/googlenav/ui/r;

.field private h:Landroid/graphics/Point;

.field private i:I

.field private j:I

.field private k:Lcom/google/googlenav/ui/q;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/p;->e:I

    return-void
.end method

.method public constructor <init>(LaN/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/p;->h:Landroid/graphics/Point;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/p;->d:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    return-void
.end method

.method public static b(I)I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    mul-int/lit8 v0, p0, 0x2

    div-int/lit8 p0, v0, 0x1

    :cond_0
    return p0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(II)Z
    .locals 1

    sget v0, Lcom/google/googlenav/ui/p;->e:I

    neg-int v0, v0

    if-lt p0, v0, :cond_0

    sget v0, Lcom/google/googlenav/ui/p;->e:I

    if-gt p0, v0, :cond_0

    sget v0, Lcom/google/googlenav/ui/p;->e:I

    neg-int v0, v0

    if-lt p1, v0, :cond_0

    sget v0, Lcom/google/googlenav/ui/p;->e:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    invoke-virtual {v0}, LaN/p;->c()LaN/Y;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/Y;->a(I)I

    move-result v0

    return v0
.end method

.method public a(II)Lcom/google/googlenav/ui/r;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/p;->b(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->g:Lcom/google/googlenav/ui/r;

    return-object v0
.end method

.method protected final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->k:Lcom/google/googlenav/ui/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->k:Lcom/google/googlenav/ui/q;

    invoke-interface {v0}, Lcom/google/googlenav/ui/q;->a()V

    :cond_0
    return-void
.end method

.method public final a(IIZ)V
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/googlenav/ui/p;->d:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    invoke-virtual {v0, p1, p2}, LaN/p;->c(II)V

    if-eqz p3, :cond_1

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    invoke-virtual {v1}, LaN/p;->d()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->d(LaN/B;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->h:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    return-void
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 2

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->x:I

    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public a(Lat/b;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p1}, Lat/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/p;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->a:LaN/p;

    invoke-virtual {v0, p0}, LaN/p;->a(LaN/r;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->h:Landroid/graphics/Point;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    iput-boolean v3, p0, Lcom/google/googlenav/ui/p;->d:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/p;->d:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/p;->h:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/p;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int v2, v1, v2

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, v0, v3

    invoke-static {v3, v2}, Lcom/google/googlenav/ui/p;->d(II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/p;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/p;->a(IIZ)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/p;->b()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/p;->k:Lcom/google/googlenav/ui/q;

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->f()I

    move-result v2

    const v3, 0xded7d6

    invoke-interface {v0, v3}, Lam/e;->a(I)V

    invoke-interface {v0, v4, v4, v1, v2}, Lam/e;->b(IIII)V

    return-void
.end method

.method public a(LaD/q;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->k:Lcom/google/googlenav/ui/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->k:Lcom/google/googlenav/ui/q;

    invoke-interface {v0}, Lcom/google/googlenav/ui/q;->b()V

    :cond_0
    return-void
.end method

.method protected b(II)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    if-nez v0, :cond_0

    iput p1, p0, Lcom/google/googlenav/ui/p;->i:I

    iput p2, p0, Lcom/google/googlenav/ui/p;->j:I

    invoke-static {p1}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v0

    invoke-static {p2}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v2

    const/4 v3, 0x0

    :try_start_0
    invoke-interface {v2, v0, v1, v3}, Lam/h;->a(IIZ)Lam/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v2}, Lam/f;->c()Lam/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/p;->f:Lam/e;

    iget-object v2, p0, Lcom/google/googlenav/ui/p;->f:Lam/e;

    invoke-static {v2, v0, v1}, Lcom/google/googlenav/ui/r;->b(Lam/e;II)Lcom/google/googlenav/ui/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/p;->g:Lcom/google/googlenav/ui/r;

    :cond_0
    return-void

    :catch_0
    move-exception v3

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    invoke-interface {v2, v0, v1, v4}, Lam/h;->a(IIZ)Lam/f;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->f()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v3}, Lam/f;->a()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v3}, Lam/f;->b()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v0, v3, v1, v2}, Lam/e;->a(Lam/f;II)V

    return-void
.end method

.method public c(II)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/p;->i:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/p;->j:I

    if-eq p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->d()V

    iput-object v1, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    iput-object v1, p0, Lcom/google/googlenav/ui/p;->f:Lam/e;

    iput-object v1, p0, Lcom/google/googlenav/ui/p;->g:Lcom/google/googlenav/ui/r;

    :cond_1
    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/p;->d:Z

    return v0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "DragBuffer"

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/p;->b:Lam/f;

    invoke-interface {v0}, Lam/f;->g()I

    move-result v0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/p;->d:Z

    return-void
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/p;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method
