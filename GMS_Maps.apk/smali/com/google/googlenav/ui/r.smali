.class public Lcom/google/googlenav/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/googlenav/ui/r;


# instance fields
.field private a:Lam/e;

.field private b:Lam/e;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lam/e;II)Lcom/google/googlenav/ui/r;
    .locals 7

    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/googlenav/ui/r;->a(Lam/e;IIIIII)Lcom/google/googlenav/ui/r;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lam/e;IIIIII)Lcom/google/googlenav/ui/r;
    .locals 8

    sget-object v0, Lcom/google/googlenav/ui/r;->i:Lcom/google/googlenav/ui/r;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/r;

    invoke-direct {v0}, Lcom/google/googlenav/ui/r;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/r;->i:Lcom/google/googlenav/ui/r;

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/r;->i:Lcom/google/googlenav/ui/r;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/r;->b(Lam/e;IIIIII)V

    sget-object v0, Lcom/google/googlenav/ui/r;->i:Lcom/google/googlenav/ui/r;

    return-object v0
.end method

.method public static b(Lam/e;II)Lcom/google/googlenav/ui/r;
    .locals 8

    const/4 v4, 0x0

    new-instance v0, Lcom/google/googlenav/ui/r;

    invoke-direct {v0}, Lcom/google/googlenav/ui/r;-><init>()V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v5, v4

    move v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/r;->b(Lam/e;IIIIII)V

    return-object v0
.end method

.method private b(Lam/e;IIIIII)V
    .locals 6

    iput p2, p0, Lcom/google/googlenav/ui/r;->c:I

    iput p3, p0, Lcom/google/googlenav/ui/r;->d:I

    iput p4, p0, Lcom/google/googlenav/ui/r;->e:I

    iput p5, p0, Lcom/google/googlenav/ui/r;->f:I

    iput p6, p0, Lcom/google/googlenav/ui/r;->g:I

    iput p7, p0, Lcom/google/googlenav/ui/r;->h:I

    iput-object p1, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    if-nez p4, :cond_0

    if-nez p5, :cond_0

    if-ne p6, p2, :cond_0

    if-ne p7, p3, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    instance-of v0, v0, Lam/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    check-cast v0, Lam/q;

    iget-object v1, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lam/q;->a(Lam/e;IIII)V

    goto :goto_0

    :cond_1
    new-instance v0, Lam/q;

    iget-object v1, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lam/q;-><init>(Lam/e;IIII)V

    iput-object v0, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    goto :goto_0
.end method


# virtual methods
.method public a()Lam/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    return-object v0
.end method

.method public b()Lam/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/r;->e:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/r;->f:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/r;->g:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/r;->h:I

    return v0
.end method

.method public g()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->b:Lam/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    const/high16 v1, -0x1000000

    invoke-interface {v0, v1}, Lam/e;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/r;->a:Lam/e;

    iget v1, p0, Lcom/google/googlenav/ui/r;->e:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/googlenav/ui/r;->f:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/googlenav/ui/r;->g:I

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/google/googlenav/ui/r;->h:I

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lam/e;->c(IIII)V

    return-void
.end method
