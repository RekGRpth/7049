.class public Lcom/google/googlenav/ui/view/android/C;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/q;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/q;)V
    .locals 1

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/C;->a:Lcom/google/googlenav/ui/view/q;

    return-void
.end method


# virtual methods
.method protected I_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/C;->g:Lcom/google/googlenav/ui/view/android/ae;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ae;->a()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/C;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_0
    return-void
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/C;->requestWindowFeature(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/C;->a:Lcom/google/googlenav/ui/view/q;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/q;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/C;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public P_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/C;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040067

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/C;->a:Lcom/google/googlenav/ui/view/q;

    invoke-static {p0, v0, v1}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/android/S;Landroid/view/View;Lcom/google/googlenav/ui/view/q;)V

    return-object v0
.end method

.method public h()Lcom/google/googlenav/ui/view/q;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/C;->a:Lcom/google/googlenav/ui/view/q;

    return-object v0
.end method
