.class Lcom/google/googlenav/ui/view/android/G;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/T;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/android/D;

.field private final b:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/view/android/D;Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/G;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/G;->b:Landroid/content/ContentResolver;

    return-void
.end method

.method private a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/16 v0, 0x40a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/G;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/D;->a(Lcom/google/googlenav/ui/view/android/D;)Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/cb;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    if-nez p3, :cond_1

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/G;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/G;->b:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/G;->a(Z)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/G;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/D;->a(Lcom/google/googlenav/ui/view/android/D;)Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/cd;

    const-string v4, "image/jpeg"

    invoke-direct {v2, v3, v4, v0}, Lcom/google/googlenav/ui/wizard/cd;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/cb;->a(Lcom/google/googlenav/ui/wizard/cd;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    if-nez p2, :cond_0

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/view/android/G;->a(Z)V

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_1
.end method
