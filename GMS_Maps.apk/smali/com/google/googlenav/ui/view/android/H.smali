.class Lcom/google/googlenav/ui/view/android/H;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/T;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/android/D;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/android/D;Ljava/lang/String;Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/H;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/H;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/H;->c:Landroid/content/ContentResolver;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/H;)Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/H;->c:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Ljava/io/File;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/I;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/I;-><init>(Lcom/google/googlenav/ui/view/android/H;Landroid/net/Uri;)V

    invoke-static {v0, p2}, Laa/c;->a(Laa/e;Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/H;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/view/android/H;->a(Landroid/net/Uri;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/H;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/D;->a(Lcom/google/googlenav/ui/view/android/D;)Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/cd;

    const-string v2, "image/jpeg"

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/H;->b:Ljava/lang/String;

    invoke-direct {v1, v5, v2, v3}, Lcom/google/googlenav/ui/wizard/cd;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cb;->a(Lcom/google/googlenav/ui/wizard/cd;)V

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MAPS"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/H;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/D;->a(Lcom/google/googlenav/ui/view/android/D;)Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/cb;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/H;->a:Lcom/google/googlenav/ui/view/android/D;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/D;->a(Lcom/google/googlenav/ui/view/android/D;)Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/cb;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
