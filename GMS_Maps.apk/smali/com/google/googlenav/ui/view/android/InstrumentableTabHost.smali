.class public Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;
.super Landroid/widget/TabHost;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public dispatchWindowFocusChanged(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/TabHost;->dispatchWindowFocusChanged(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->hasWindowFocus()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public setCurrentTabContentView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->a:Landroid/view/View;

    return-void
.end method
