.class public Lcom/google/googlenav/ui/view/android/L;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Lcom/google/googlenav/android/S;

.field private final d:Landroid/content/ContentResolver;

.field private final l:Lcom/google/googlenav/ui/wizard/cb;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/tmpGmmPhoto.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/view/android/L;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/cb;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/r;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/L;->l:Lcom/google/googlenav/ui/wizard/cb;

    iput p2, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/googlenav/ui/view/android/O;

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/view/android/O;-><init>(Lcom/google/googlenav/ui/view/android/L;Lcom/google/googlenav/ui/view/android/M;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(ILcom/google/googlenav/android/T;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/googlenav/ui/view/android/P;

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/view/android/P;-><init>(Lcom/google/googlenav/ui/view/android/L;Lcom/google/googlenav/ui/view/android/M;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(ILcom/google/googlenav/android/T;)V

    sget-object v0, Lcom/google/googlenav/ui/view/android/L;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->d:Landroid/content/ContentResolver;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/L;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/L;)Lcom/google/googlenav/android/S;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/L;)Lcom/google/googlenav/ui/wizard/cb;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->l:Lcom/google/googlenav/ui/wizard/cb;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/android/L;)Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->d:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/android/L;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/L;->l()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/android/L;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/L;->m()V

    return-void
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/L;->a:Ljava/lang/String;

    return-object v0
.end method

.method private l()V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget v0, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    if-nez v0, :cond_0

    const-string v0, "output"

    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/google/googlenav/ui/view/android/L;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v2

    iget v0, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/android/O;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/ui/view/android/O;-><init>(Lcom/google/googlenav/ui/view/android/L;Lcom/google/googlenav/ui/view/android/M;)V

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/P;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/ui/view/android/P;-><init>(Lcom/google/googlenav/ui/view/android/L;Lcom/google/googlenav/ui/view/android/M;)V

    goto :goto_0
.end method

.method private m()V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    if-eqz v1, :cond_0

    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "crop"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "outputX"

    iget v2, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "outputY"

    iget v2, p0, Lcom/google/googlenav/ui/view/android/L;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    new-instance v2, Lcom/google/googlenav/ui/view/android/P;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/view/android/P;-><init>(Lcom/google/googlenav/ui/view/android/L;Lcom/google/googlenav/ui/view/android/M;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/L;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040077

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1001b3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1001b4

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v3, 0x42

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0x43

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/google/googlenav/ui/view/android/M;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/android/M;-><init>(Lcom/google/googlenav/ui/view/android/L;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/N;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/N;-><init>(Lcom/google/googlenav/ui/view/android/L;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/S;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->c:Lcom/google/googlenav/android/S;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/S;->a(I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/L;->l:Lcom/google/googlenav/ui/wizard/cb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cb;->a(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/r;->onBackPressed()V

    return-void
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x50c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
