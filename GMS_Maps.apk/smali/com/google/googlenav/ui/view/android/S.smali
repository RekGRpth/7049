.class public abstract Lcom/google/googlenav/ui/view/android/S;
.super Lcom/google/googlenav/ui/view/android/aL;
.source "SourceFile"


# static fields
.field protected static e:Lcom/google/googlenav/android/BaseMapsActivity;

.field public static final i:Lcom/google/googlenav/ui/e;


# instance fields
.field private a:Z

.field private b:LaA/g;

.field private c:I

.field private d:Ljava/lang/CharSequence;

.field protected f:Lcom/google/googlenav/ui/e;

.field protected final g:Lcom/google/googlenav/ui/view/android/ae;

.field protected h:Landroid/view/View;

.field public j:Z

.field private l:Z

.field private m:I

.field private volatile n:Z

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/T;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/T;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/android/S;->i:Lcom/google/googlenav/ui/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->i:Lcom/google/googlenav/ui/e;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->i:Lcom/google/googlenav/ui/e;

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/S;->p()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;I)V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-static {p2}, Lcom/google/googlenav/ui/view/android/S;->a(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/aL;-><init>(Landroid/content/Context;I)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->d:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->j:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    new-instance v0, Lcom/google/googlenav/ui/view/android/ae;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/ae;-><init>(Lcom/google/googlenav/ui/view/android/S;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->g:Lcom/google/googlenav/ui/view/android/ae;

    return-void
.end method

.method private static a(I)I
    .locals 0

    return p0
.end method

.method public static a(Lcom/google/googlenav/android/BaseMapsActivity;)V
    .locals 0

    sput-object p0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    return-void
.end method

.method private b(Landroid/app/ActionBar;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->b:LaA/g;

    invoke-static {v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->b:LaA/g;

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    return-void
.end method

.method private b(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method private h()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/android/BaseMapsActivity;->showDialog(Landroid/app/Dialog;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->v()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->m()V

    goto :goto_1
.end method

.method private m()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/view/android/S;->o:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->o()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->c()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->w_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/googlenav/ui/view/android/bC;->a(Landroid/app/Dialog;Landroid/view/View;Lcom/google/googlenav/ui/e;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private o()V
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->b:LaA/g;

    invoke-static {v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->q()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f1001b0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1001b1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/S;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->b:LaA/g;

    invoke-virtual {v0, v2, v1}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static p()I
    .locals 1

    const v0, 0x7f0f0018

    return v0
.end method

.method private v()V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/S;->l:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/googlenav/ui/view/android/S;->m:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->a(Landroid/app/ActionBar;)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->b(Landroid/app/ActionBar;)V

    goto :goto_0
.end method


# virtual methods
.method protected I_()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    invoke-interface {v0}, Lcom/google/googlenav/ui/e;->h()V

    const/4 v0, 0x1

    return v0
.end method

.method protected O_()V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->P_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setUiOptions(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/S;->requestWindowFeature(I)Z

    goto :goto_0
.end method

.method public P_()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 0

    return-void
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->l:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "updateTitleBar() without first calling setupTitleBar()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/S;->d:Ljava/lang/CharSequence;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/googlenav/ui/view/android/S;->o:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    if-eqz v0, :cond_3

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0, p1}, LaA/h;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/CharSequence;II)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->l:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/S;->d:Ljava/lang/CharSequence;

    iput p2, p0, Lcom/google/googlenav/ui/view/android/S;->o:I

    iput p3, p0, Lcom/google/googlenav/ui/view/android/S;->m:I

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/S;->j:Z

    return-void
.end method

.method protected varargs a(ZILaA/f;[I)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/S;->a:Z

    iput p2, p0, Lcom/google/googlenav/ui/view/android/S;->c:I

    new-instance v0, LaA/g;

    invoke-direct {v0, p3, p4}, LaA/g;-><init>(LaA/f;[I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->b:LaA/g;

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/view/Menu;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method protected abstract c()Landroid/view/View;
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected final d(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    return-void
.end method

.method public final dismiss()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/view/android/bC;->a(Landroid/app/Dialog;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/android/BaseMapsActivity;->dismissDialog(Landroid/app/Dialog;)V

    goto :goto_0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public hide()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->dismiss()V

    return-void
.end method

.method protected i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/aL;->invalidateOptionsMenu()V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->b()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->closeOptionsMenu()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->n:Z

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    sget-object v1, Lcom/google/googlenav/ui/view/android/S;->i:Lcom/google/googlenav/ui/e;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    invoke-interface {v0}, Lcom/google/googlenav/ui/e;->h()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/aL;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/aL;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->setCanceledOnTouchOutside(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->O_()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->c()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->I_()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v0, 0x1

    const/16 v1, 0x54

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/view/android/S;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->onSearchRequested()Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    sget-object v2, Lcom/google/googlenav/ui/view/android/S;->i:Lcom/google/googlenav/ui/e;

    if-ne v1, v2, :cond_2

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/aL;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x5

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    const/4 v2, 0x3

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/aL;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->K_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/aL;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/S;->b(Landroid/view/Menu;)V

    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/aL;->onOptionsMenuClosed(Landroid/view/Menu;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->b(Landroid/view/Menu;)V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/aL;->onPanelClosed(ILandroid/view/Menu;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;->b(Landroid/view/Menu;)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->n:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->n:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public q()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->h:Landroid/view/View;

    return-object v0
.end method

.method public r()Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method public final s()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method public final show()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->n()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;->l()V

    goto :goto_0
.end method

.method public final t()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    return-void
.end method

.method public u()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/S;->j:Z

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/S;->d:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
