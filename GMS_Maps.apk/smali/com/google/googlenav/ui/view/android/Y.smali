.class public Lcom/google/googlenav/ui/view/android/Y;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/googlenav/ui/view/a;


# instance fields
.field protected b:Lcom/google/googlenav/ai;

.field protected final c:Lbf/C;

.field protected d:Lcom/google/googlenav/ui/view/android/J;

.field private l:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sput-object v0, Lcom/google/googlenav/ui/view/android/Y;->a:Lcom/google/googlenav/ui/view/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/C;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    return-void
.end method

.method private a(Ljava/lang/String;)Lbj/H;
    .locals 4

    const/4 v0, 0x1

    new-instance v1, Lbj/bv;

    const/4 v2, 0x3

    const v3, 0x7f04006b

    invoke-direct {v1, v2, v3, p1, v0}, Lbj/bv;-><init>(IILjava/lang/String;Z)V

    return-object v1
.end method

.method private l()Ljava/util/List;
    .locals 7

    const/4 v6, 0x1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/Y;->m()Lbj/H;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbj/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Lbj/b;-><init>(Lcom/google/googlenav/ai;Lbf/C;IZ)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->b()Z

    move-result v2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bK()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x246

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x12

    invoke-static {v0, v4, v6, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    :cond_0
    if-eqz v2, :cond_4

    const/16 v0, 0x71

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/Y;->a(Ljava/lang/String;)Lbj/H;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    const/16 v0, 0x4ff

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x25a

    invoke-static {v0, v4, v6, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    if-eqz v3, :cond_5

    const/16 v0, 0x52f

    :goto_1
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_6

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/Y;->a(Ljava/lang/String;)Lbj/H;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v2}, Lbf/C;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbf/C;->c(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v3, :cond_7

    const/16 v0, 0x449

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    const/16 v2, 0x5dc

    invoke-static {v0, v2, v6, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    :cond_3
    return-object v1

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v4}, Lbf/C;->bm()Z

    move-result v4

    invoke-static {v0, v4, v6, v1}, Lbf/aS;->a(Lcom/google/googlenav/ai;ZILjava/util/List;)V

    goto :goto_0

    :cond_5
    const/16 v0, 0x52e

    goto :goto_1

    :cond_6
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v2}, Lbf/C;->c()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    invoke-static {v0, v2, v6, v1}, Lbf/aS;->a(Ljava/lang/String;IILjava/util/List;)V

    goto :goto_2

    :cond_7
    const/16 v0, 0x44b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private m()Lbj/H;
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/view/android/Y;->a([Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v2}, Lbf/C;->bL()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v4}, Lbf/C;->bM()Z

    move-result v4

    new-instance v5, Lcom/google/googlenav/ui/view/android/bt;

    invoke-direct {v5}, Lcom/google/googlenav/ui/view/android/bt;-><init>()V

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    :goto_0
    invoke-virtual {v5, v6, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/bt;->a(LaN/B;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/view/android/bt;->b(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v1}, Lbf/C;->be()Lcom/google/googlenav/ui/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const v1, 0x7f0400c2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/view/android/bt;->d(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/view/android/bt;->d(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v1}, Lbf/C;->bH()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v1}, Lbf/C;->bG()Lam/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lam/f;)Lcom/google/googlenav/ui/view/android/bt;

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bt;->a()Lcom/google/googlenav/ui/view/android/bs;

    move-result-object v0

    return-object v0

    :cond_2
    sget-object v0, Lcom/google/googlenav/ui/aV;->aP:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method


# virtual methods
.method public I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method a([Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->d()[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v0, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget-object v5, v4, v0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->c()[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    move v0, v1

    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_3

    aget-object v5, v4, v0

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    aget-object v5, v4, v0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x259

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_9

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :goto_3
    if-eqz p2, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    move-object v2, v0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    array-length v4, p1

    if-lt v1, v4, :cond_8

    :cond_5
    return-object v2

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->c()[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    array-length v0, v4

    if-lez v0, :cond_3

    move v0, v1

    :goto_5
    array-length v5, v4

    if-ge v0, v5, :cond_3

    aget-object v5, v4, v0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    const/16 v0, 0x5e8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    aput-object v0, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_9
    move-object v0, v2

    goto :goto_3
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/Y;->l()Ljava/util/List;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->l:Landroid/widget/ListView;

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    const/4 v5, 0x4

    invoke-direct {v0, v3, v4, v2, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->d:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->l:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/Y;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->l:Landroid/widget/ListView;

    new-instance v2, Lcom/google/googlenav/ui/view/android/Z;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/Z;-><init>(Lcom/google/googlenav/ui/view/android/Y;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->l:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->l:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v1
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->b:Lcom/google/googlenav/ai;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->d:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->c:Lbf/C;

    invoke-virtual {v0}, Lbf/C;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/Y;->l()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/Y;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->I_()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/Y;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f1002d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1003a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/Y;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public w_()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/Y;->a([Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
