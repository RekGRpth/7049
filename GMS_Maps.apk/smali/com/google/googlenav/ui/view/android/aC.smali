.class public Lcom/google/googlenav/ui/view/android/aC;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private b:Lcom/google/googlenav/bc;

.field private c:Lcom/google/googlenav/bc;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/ui/view/android/aC;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f1001b5
        0x7f1001b6
        0x7f1001b7
        0x7f1001b8
        0x7f1001b9
    .end array-data
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/ce;Lcom/google/googlenav/bc;Lcom/google/googlenav/bc;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aC;->d:Z

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/aC;->b:Lcom/google/googlenav/bc;

    invoke-virtual {p2}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aC;->d:Z

    return-void
.end method

.method private a(Lcom/google/googlenav/bd;Landroid/view/View;)V
    .locals 5

    const v4, 0x7f100165

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/bd;->a()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/view/android/aC;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/google/googlenav/ui/view/android/aC;->a:[I

    aget v0, v1, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/aC;->d:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v4}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/aF;

    invoke-direct {v2, p0, p2}, Lcom/google/googlenav/ui/view/android/aF;-><init>(Lcom/google/googlenav/ui/view/android/aC;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/aG;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/view/android/aG;-><init>(Lcom/google/googlenav/ui/view/android/aC;Landroid/widget/CheckBox;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :goto_2
    const v1, 0x7f100166

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/bd;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aA:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f100167

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v4}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v4, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/aH;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/view/android/aH;-><init>(Lcom/google/googlenav/ui/view/android/aC;Landroid/widget/RadioButton;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/aI;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/view/android/aI;-><init>(Lcom/google/googlenav/ui/view/android/aC;Landroid/widget/RadioButton;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/aC;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aC;->l()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/aC;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aC;->h()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/aC;)Lcom/google/googlenav/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    return-object v0
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aC;->f:Lcom/google/googlenav/ui/e;

    const/16 v1, 0x2cb

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v2}, Lcom/google/googlenav/bc;->a()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    return-void
.end method

.method private l()V
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/aC;->a:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/view/android/aC;->a:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/aC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v3, 0x7f100165

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bd;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aC;->f:Lcom/google/googlenav/ui/e;

    const/16 v1, 0x2d2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v3}, Lcom/google/googlenav/bc;->a()I

    move-result v3

    invoke-interface {v0, v1, v3, v2}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 8

    const v4, 0x7f100031

    const v5, 0x7f100030

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aC;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aC;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f040079

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aC;->b:Lcom/google/googlenav/bc;

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    :goto_1
    move v2, v3

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bd;

    invoke-direct {p0, v0, v7}, Lcom/google/googlenav/ui/view/android/aC;->a(Lcom/google/googlenav/bd;Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_0
    const v0, 0x7f04007a

    goto :goto_0

    :cond_1
    const v0, 0x7f10002e

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    :goto_3
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_4
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/aC;->d:Z

    if-eqz v1, :cond_2

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x3a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/aD;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/aD;-><init>(Lcom/google/googlenav/ui/view/android/aC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0xd1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/aE;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/aE;-><init>(Lcom/google/googlenav/ui/view/android/aC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->d()Z

    move-result v1

    if-nez v1, :cond_3

    move v3, v6

    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    return-object v7

    :cond_4
    move v0, v5

    goto :goto_3

    :cond_5
    move v5, v4

    goto :goto_4

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aC;->c:Lcom/google/googlenav/bc;

    invoke-virtual {v0}, Lcom/google/googlenav/bc;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
