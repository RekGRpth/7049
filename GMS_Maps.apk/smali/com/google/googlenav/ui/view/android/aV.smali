.class public Lcom/google/googlenav/ui/view/android/aV;
.super Lcom/google/googlenav/ui/view/android/bn;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/googlenav/L;

.field protected final b:Lcom/google/googlenav/ui/wizard/jv;

.field private q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

.field private r:Landroid/support/v4/view/ViewPager;

.field private s:Lcom/google/googlenav/ui/ba;

.field private t:Lbk/a;

.field private u:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;)V
    .locals 2

    invoke-virtual {p2}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->l()Lcom/google/googlenav/L;

    move-result-object v0

    invoke-virtual {p2}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;Lcom/google/googlenav/L;Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ai;Lbf/m;Lcom/google/googlenav/L;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/bn;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/aV;->b:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method private J()Lcom/google/googlenav/ui/ba;
    .locals 7

    new-instance v0, Lcom/google/googlenav/ui/view/android/aY;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/aY;-><init>(Lcom/google/googlenav/ui/view/android/aV;Landroid/content/Context;Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;Landroid/support/v4/view/ViewPager;Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/bc;)V

    return-object v0
.end method

.method private K()Landroid/view/View;
    .locals 7

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->m()Ljava/util/List;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    const/16 v6, 0x24

    invoke-direct {v3, v4, v5, v2, v6}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->m:Lcom/google/googlenav/ui/view/android/J;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/br;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/br;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v1
.end method

.method private L()Landroid/view/View;
    .locals 6

    new-instance v5, Lbk/e;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    invoke-direct {v5, v0, v1, v2}, Lbk/e;-><init>(Landroid/widget/TabHost;ILcom/google/googlenav/L;)V

    new-instance v0, Lbk/a;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->b:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lbk/a;-><init>(Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/view/android/aV;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/br;Lbk/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    invoke-virtual {v0}, Lbk/a;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private M()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    const/16 v1, 0x578

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v1, 0x7f10029b

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v1, :cond_1

    const v1, 0x7f020269

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const v1, 0x7f020268

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/aV;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/aV;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/aV;)Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    return-object v0
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->h()V

    const/4 v0, 0x1

    return v0
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/bn;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020269

    :goto_1
    invoke-interface {p2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move v0, v1

    goto :goto_0

    :cond_0
    const v0, 0x7f020268

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    move v0, v1

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x843

    :goto_2
    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v0, 0x834

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1001e3 -> :sswitch_2
        0x7f100306 -> :sswitch_0
        0x7f1004bd -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 3

    const v0, 0x7f1004bd

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x43a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1001e3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x6e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0

    :cond_1
    const/16 v0, 0x580

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x99

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public c()Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    const/16 v3, 0x8

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/aC;->c(Lcom/google/googlenav/ai;)Z

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aS;->f(Lcom/google/googlenav/ai;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400db

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/aV;->a(Landroid/view/View;)V

    const v0, 0x1020012

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10001e

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029c

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->I()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Lcom/google/googlenav/ui/view/android/aW;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/aW;-><init>(Lcom/google/googlenav/ui/view/android/aV;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029b

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/google/googlenav/ui/view/android/aX;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/view/android/aX;-><init>(Lcom/google/googlenav/ui/view/android/aV;Landroid/widget/ImageView;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->N()V

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setup()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029d

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->r:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->J()Lcom/google/googlenav/ui/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->K()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e3

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->L()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->n()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->l()V

    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029a

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public h()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    invoke-virtual {v0}, Lbk/a;->b()V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->N()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->invalidateOptionsMenu()V

    :cond_1
    return-void
.end method

.method protected l()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected m()Ljava/util/List;
    .locals 9

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->o:Z

    if-eqz v0, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v2}, Lbf/m;->bs()Ljava/util/Hashtable;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Hashtable;Ljava/lang/Object;)Lbf/aQ;

    move-result-object v7

    new-instance v0, Lbj/N;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v4}, Lbf/m;->bf()Lcom/google/googlenav/ui/a;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbj/N;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->o()Z

    move-result v5

    invoke-virtual {p0, v6, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Z)V

    invoke-virtual {p0, v7, v6, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Lbf/aQ;Ljava/util/List;Z)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/aV;->d(Ljava/util/List;)V

    invoke-virtual {p0, v6, v7}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Lbf/aQ;)V

    new-instance v0, Lbj/P;

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v7, v3}, Lbj/P;-><init>(ILcom/google/googlenav/ai;Lbf/aQ;Lbf/i;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbj/bm;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v8, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4, v8}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lbj/bm;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbj/I;

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2}, Lbj/I;-><init>(ILcom/google/googlenav/ai;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->b(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->c(Ljava/util/List;Lbf/aQ;Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/view/android/aV;->a(Lbf/aQ;)Lbj/H;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lbj/aX;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v5}, Lbj/aX;-><init>(Lcom/google/googlenav/ai;IZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;)V

    new-instance v0, Lbj/al;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2, v5}, Lbj/al;-><init>(ILcom/google/googlenav/ai;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->b(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    check-cast v3, Lbf/bk;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v2

    invoke-virtual {v3}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    const/16 v1, 0x577

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x13

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/g;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v6

    goto/16 :goto_0
.end method

.method n()Landroid/view/View;
    .locals 3

    new-instance v0, Lbk/h;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    invoke-direct {v0, v1, v2}, Lbk/h;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/L;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk/h;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected o()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/google/googlenav/ui/view/android/aV;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1002a8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x3c9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->I()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    :cond_0
    const v0, 0x7f100306

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v0, :cond_3

    const v0, 0x7f020269

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    const v0, 0x7f1004bd

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->b(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->c(Landroid/view/Menu;)V

    const v0, 0x7f1001e3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->e(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->e(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->f(Landroid/view/Menu;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->g(Landroid/view/Menu;)V

    return v2

    :cond_3
    const v0, 0x7f020268

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public v()Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    return-object v0
.end method
