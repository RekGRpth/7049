.class Lcom/google/googlenav/ui/view/android/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/ac;->a:Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;Lcom/google/googlenav/ui/view/android/aa;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/ac;-><init>(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)V

    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ac;->a:Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;

    if-ne p1, v0, :cond_0

    instance-of v0, p2, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ac;->a:Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;->a(Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;)Lcom/google/googlenav/ui/view/android/X;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;->setOnCheckedChangeListener(Lcom/google/googlenav/ui/view/android/X;)V

    :cond_0
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ac;->a:Lcom/google/googlenav/ui/view/android/DescriptiveRadioGroup;

    if-ne p1, v0, :cond_0

    instance-of v0, p2, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/view/android/DescriptiveRadioButton;->setOnCheckedChangeListener(Lcom/google/googlenav/ui/view/android/X;)V

    :cond_0
    return-void
.end method
