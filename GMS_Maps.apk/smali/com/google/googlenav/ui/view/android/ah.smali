.class public Lcom/google/googlenav/ui/view/android/ah;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# static fields
.field protected static a:I


# instance fields
.field private b:Lbi/d;

.field private final c:Lax/b;

.field private d:Lcom/google/googlenav/ui/view/android/am;

.field private l:Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/google/googlenav/ui/view/android/ah;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;Lax/b;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/ui/view/android/ah;->p()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    sget v0, Lcom/google/googlenav/ui/view/android/ah;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    sput v0, Lcom/google/googlenav/ui/view/android/ah;->a:I

    :cond_0
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0400a9

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/view/ViewGroup;)V

    return-object v0

    :cond_0
    const v0, 0x7f0400a8

    goto :goto_0
.end method

.method private static a(Lax/b;)Ljava/lang/CharSequence;
    .locals 3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->w:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/aX;->a(Lcom/google/googlenav/ui/aW;)Ljava/util/Vector;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v2

    invoke-static {v1, v2}, Lbf/I;->a(Ljava/lang/String;Lax/y;)Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lax/b;Ljava/util/Vector;)Ljava/lang/CharSequence;
    .locals 3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->w:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-static {v1, v2}, Lbf/I;->a(Ljava/lang/String;Lax/y;)Ljava/util/Vector;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    invoke-static {}, Lbf/O;->bq()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x0

    const/16 v3, 0x8

    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/ak;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/view/android/ak;-><init>(Lcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/ui/view/android/ai;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/al;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/view/android/al;-><init>(Lcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/ui/view/android/ai;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v1, v1, Lbf/O;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/widget/ListView;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/ah;->l()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/content/Context;ILjava/util/List;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/CharSequence;Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100243

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100044

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->as()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/view/android/ai;

    invoke-direct {v1, p0, p5}, Lcom/google/googlenav/ui/view/android/ai;-><init>(Lcom/google/googlenav/ui/view/android/ah;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/aj;

    invoke-direct {v0, p0, p5}, Lcom/google/googlenav/ui/view/android/aj;-><init>(Lcom/google/googlenav/ui/view/android/ah;I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 7

    const/4 v6, 0x0

    const v2, 0x7f100242

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/ah;->a(Lax/b;)Ljava/lang/CharSequence;

    move-result-object v3

    const/16 v0, 0x1ad

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xee

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/view/View;ILjava/lang/CharSequence;Ljava/lang/String;I)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v1, v1, Lbf/O;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    check-cast v0, Lbf/O;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-static {v0, v1}, Lbf/I;->a(Lbf/O;Lax/b;)Ljava/util/Vector;

    move-result-object v0

    :cond_0
    const v2, 0x7f100244

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/android/ah;->a(Lax/b;Ljava/util/Vector;)Ljava/lang/CharSequence;

    move-result-object v3

    const/16 v0, 0x5bf

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xef

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/view/View;ILjava/lang/CharSequence;Ljava/lang/String;I)V

    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v0, v0, Lbf/O;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    check-cast v0, Lbf/O;

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0, v1}, Lbf/O;->d(Lax/b;)Z

    move-result v3

    :goto_0
    new-instance v0, Lcom/google/googlenav/ui/view/android/am;

    const v1, 0x7f10023a

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v5}, Lax/b;->aN()I

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v6}, Lax/b;->k()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/am;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/e;ZZII)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->d:Lcom/google/googlenav/ui/view/android/am;

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/ah;->b(Landroid/view/View;)V

    return-void

    :cond_2
    move v4, v6

    move v3, v6

    goto :goto_0
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    check-cast v2, Lax/w;

    new-instance v0, Lbi/e;

    invoke-direct {v0, v2}, Lbi/e;-><init>(Lax/w;)V

    invoke-virtual {v0}, Lbi/e;->a()Lbi/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->b:Lbi/d;

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/ah;->b:Lbi/d;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;-><init>(Landroid/content/Context;Lax/w;Lbi/d;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/m;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->l:Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->l:Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public static a(Lax/b;Lcom/google/googlenav/ui/e;Ljava/util/ArrayList;)V
    .locals 7

    const/4 v6, 0x7

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lax/b;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbj/p;

    const/16 v2, 0x434

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v2

    const/16 v3, 0xd7

    invoke-direct {v0, v2, v4, v3}, Lbj/p;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lax/b;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lbj/p;

    sget-object v3, Lcom/google/googlenav/ui/aV;->i:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v3, -0x1

    invoke-direct {v2, v0, v4, v3}, Lbj/p;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Lbj/m;

    const/16 v2, 0x4ca

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->bM:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v2, v5}, Lbj/m;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lax/b;->aA()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    const/16 v3, 0xd9

    invoke-static {p0, v2, v0, v3, v1}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/b;Lcom/google/googlenav/ui/bi;III)Lbj/by;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lax/b;->ax()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x5bc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bM:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lbj/m;

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v1, v0, v5}, Lbj/m;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lax/b;->S()Lax/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Lax/l;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lbj/i;

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x6

    const/16 v3, 0xd8

    invoke-direct {v1, v0, v2, v3}, Lbj/i;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x5e6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lbj/c;

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v2, 0xdb

    invoke-direct {v1, v0, v6, v2}, Lbj/c;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x5e5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lbj/c;

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v2, 0xda

    invoke-direct {v1, v0, v6, v2}, Lbj/c;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x503

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->F:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lbj/f;

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x1

    const/16 v3, 0xca

    invoke-direct {v1, v0, v2, v3}, Lbj/f;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbj/l;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lbj/l;-><init>(I)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 9

    const v8, 0x7f0f003f

    const v7, 0x7f0f003e

    const/16 v6, 0x8

    const/4 v2, 0x0

    const v0, 0x7f100245

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v1, v1, Lcom/google/googlenav/ui/wizard/bv;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f10024a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100246

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->aG()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->aG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v5, v0

    :goto_2
    if-eqz v5, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->M()I

    move-result v0

    move v1, v0

    :goto_3
    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    check-cast v0, Lax/s;

    invoke-virtual {v0}, Lax/s;->aU()I

    move-result v4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->F()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x25e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_4
    add-int/2addr v1, v4

    move-object v3, v0

    move v4, v1

    :goto_5
    if-eqz v5, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->O()I

    move-result v0

    move v1, v0

    :goto_6
    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v1, :cond_7

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_7
    const v0, 0x7f100247

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100248

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v5

    invoke-static {v4}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4, v7, v8}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_8
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-static {v0}, Lbf/I;->a(Lax/b;)Ljava/lang/CharSequence;

    move-result-object v1

    const v0, 0x7f100249

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_3
    move v5, v2

    goto/16 :goto_2

    :cond_4
    move v1, v2

    goto/16 :goto_3

    :cond_5
    const/16 v0, 0x3c

    if-le v4, v0, :cond_9

    const/16 v0, 0x1fc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_6

    :cond_7
    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v5}, Lax/b;->av()I

    move-result v5

    invoke-static {v1, v5}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v5

    invoke-virtual {v5, v1, v7, v8}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_8
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    :cond_9
    move-object v0, v3

    goto/16 :goto_4

    :cond_a
    move v4, v1

    goto/16 :goto_5
.end method

.method private l()Ljava/util/List;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/bv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->y()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/ah;->a(Lax/b;Lcom/google/googlenav/ui/e;Ljava/util/ArrayList;)V

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->az()I

    move-result v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    const/4 v5, -0x1

    invoke-static {v3, v4, v0, v5, v1}, Lcom/google/googlenav/ui/wizard/iB;->a(Lax/b;Lcom/google/googlenav/ui/bi;III)Lbj/by;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/ah;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/ah;->a(Ljava/util/ArrayList;)V

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    invoke-virtual {v3}, Lax/b;->f()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/ah;->c:Lax/b;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    invoke-static {v3, v0, v4, v5, v1}, Lbf/G;->a(Lax/b;IILcom/google/googlenav/ui/e;Z)Lbj/H;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private m()Landroid/widget/ListView;
    .locals 1

    const v0, 0x7f100026

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/ah;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method protected O_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setUiOptions(I)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/content/Context;ILjava/util/List;)Lcom/google/googlenav/ui/view/android/J;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    invoke-direct {v0, p1, v1, p3, p2}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    return-object v0
.end method

.method public a(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/ah;->m()Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->l:Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ah;->l:Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a()I

    move-result v1

    sget v2, Lcom/google/googlenav/ui/view/android/ah;->a:I

    sub-int v1, v2, v1

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_1
    sget v1, Lcom/google/googlenav/ui/view/android/ah;->a:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/16 v0, 0xfc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, -0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xec

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xcb

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xcc

    goto :goto_0

    :pswitch_5
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v2, 0x4dd

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xdc

    goto :goto_0

    :cond_0
    const/16 v0, 0xe6

    goto :goto_0

    :pswitch_6
    const/16 v0, 0xc9

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x5e6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f1004ad
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_1
    .end packed-switch
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/ah;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/ah;->a(Landroid/view/View;)V

    return-object v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->d:Lcom/google/googlenav/ui/view/android/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->d:Lcom/google/googlenav/ui/view/android/am;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/am;->c()V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8

    const v7, 0x7f1004b1

    const/16 v6, 0x4dd

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    check-cast v0, Lcom/google/googlenav/ui/wizard/bv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/view/android/ah;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v3, 0x7f110007

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/google/googlenav/ui/f;->a(Lax/b;)Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    return v2

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/ah;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ah;->f:Lcom/google/googlenav/ui/e;

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->a()Lax/b;

    move-result-object v4

    const v1, 0x7f1004b4

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    const/16 v5, 0x507

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    const v1, 0x7f1004ad

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    const/16 v5, 0x2f8

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Lbf/O;->bk()Z

    move-result v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    const v1, 0x7f1004af

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/16 v5, 0x1bd

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v1, 0x7f1004b0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/16 v1, 0x5f6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Lbf/O;->bl()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v4}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-virtual {v0}, Lbf/O;->bg()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-static {v4}, Lcom/google/googlenav/ui/f;->a(Lax/b;)Z

    move-result v1

    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v1, 0x7f1004b2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/16 v5, 0x37

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {v4}, Lax/b;->Y()Z

    move-result v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v1, 0x7f1004b3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/16 v1, 0x43b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {v4}, Lax/b;->e()Lcom/google/googlenav/E;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-virtual {v4}, Lax/b;->aw()LaN/B;

    move-result-object v1

    :goto_3
    invoke-virtual {v4}, Lax/b;->m()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v0, v1}, Lbf/O;->c(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v3, v2

    :cond_3
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    const/16 v1, 0xfa

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Lax/b;->e()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    goto :goto_3
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xfc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
