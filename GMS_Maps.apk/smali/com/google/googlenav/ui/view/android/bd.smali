.class public Lcom/google/googlenav/ui/view/android/bd;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:Lcom/google/googlenav/ui/wizard/jv;

.field private final c:Lcom/google/googlenav/ui/br;

.field private d:Lcom/google/googlenav/ui/view/android/J;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ai;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/r;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bd;->b:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/bd;->a:Lcom/google/googlenav/ai;

    new-instance v0, Lcom/google/googlenav/ui/br;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bd;->c:Lcom/google/googlenav/ui/br;

    return-void
.end method


# virtual methods
.method public Q_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bd;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bd;->h()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bd;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04007c

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bd;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v6, v1, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v3, p0, Lcom/google/googlenav/ui/view/android/bd;->d:Lcom/google/googlenav/ui/view/android/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bd;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v2
.end method

.method h()Ljava/util/List;
    .locals 9

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bd;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v7

    invoke-static {v7}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v7, :cond_0

    new-instance v0, Lbj/T;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bd;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bd;->c:Lcom/google/googlenav/ui/br;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bd;->b:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bd;->a:Lcom/google/googlenav/ai;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lbj/T;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ai;LaB/p;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v8
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
