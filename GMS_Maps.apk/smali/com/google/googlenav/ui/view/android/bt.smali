.class public Lcom/google/googlenav/ui/view/android/bt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ai;

.field private B:Z

.field private C:Z

.field private D:I

.field private E:Lcom/google/googlenav/ui/e;

.field private F:Lcom/google/googlenav/ui/br;

.field private G:I

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:I

.field private L:Z

.field private M:Lcom/google/googlenav/ar;

.field private N:I

.field private O:Z

.field private P:I

.field private a:Ljava/lang/CharSequence;

.field private b:[Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:[Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Z

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Ljava/lang/CharSequence;

.field private o:Ljava/lang/CharSequence;

.field private p:Lcom/google/googlenav/ac;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/CharSequence;

.field private s:Lcom/google/googlenav/aq;

.field private t:Ljava/util/Vector;

.field private u:Lcom/google/googlenav/ap;

.field private v:Lam/f;

.field private w:Lcom/google/googlenav/ui/view/a;

.field private x:Lan/f;

.field private y:LaN/B;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/android/bt;->L:Z

    const v0, 0x7f0400c4

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    iput v1, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    return-void
.end method

.method static synthetic A(Lcom/google/googlenav/ui/view/android/bt;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->D:I

    return v0
.end method

.method static synthetic B(Lcom/google/googlenav/ui/view/android/bt;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    return v0
.end method

.method static synthetic C(Lcom/google/googlenav/ui/view/android/bt;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    return v0
.end method

.method static synthetic D(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    return v0
.end method

.method static synthetic E(Lcom/google/googlenav/ui/view/android/bt;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    return v0
.end method

.method static synthetic F(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->I:Z

    return v0
.end method

.method static synthetic G(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->H:Z

    return v0
.end method

.method static synthetic H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->J:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic I(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->L:Z

    return v0
.end method

.method static synthetic J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->M:Lcom/google/googlenav/ar;

    return-object v0
.end method

.method static synthetic K(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->r:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic L(Lcom/google/googlenav/ui/view/android/bt;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->K:I

    return v0
.end method

.method static synthetic M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->m:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->s:Lcom/google/googlenav/aq;

    return-object v0
.end method

.method static synthetic P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->t:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->E:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/br;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->F:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->c:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->d:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->f:Z

    return v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic o(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic p(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ac;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->p:Lcom/google/googlenav/ac;

    return-object v0
.end method

.method static synthetic q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->u:Lcom/google/googlenav/ap;

    return-object v0
.end method

.method static synthetic s(Lcom/google/googlenav/ui/view/android/bt;)Lam/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->v:Lam/f;

    return-object v0
.end method

.method static synthetic t(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/view/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->w:Lcom/google/googlenav/ui/view/a;

    return-object v0
.end method

.method static synthetic u(Lcom/google/googlenav/ui/view/android/bt;)Lan/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->x:Lan/f;

    return-object v0
.end method

.method static synthetic v(Lcom/google/googlenav/ui/view/android/bt;)LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->y:LaN/B;

    return-object v0
.end method

.method static synthetic w(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->z:Z

    return v0
.end method

.method static synthetic x(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->A:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic y(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->B:Z

    return v0
.end method

.method static synthetic z(Lcom/google/googlenav/ui/view/android/bt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->C:Z

    return v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/view/android/bs;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/bs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bs;-><init>(Lcom/google/googlenav/ui/view/android/bt;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->D:I

    return-object p0
.end method

.method public a(LaN/B;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->y:LaN/B;

    return-object p0
.end method

.method public a(Lai/d;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Lai/d;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->g:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lai/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->h:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public a(Lam/f;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->v:Lam/f;

    return-object p0
.end method

.method public a(Lan/f;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->x:Lan/f;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ac;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->p:Lcom/google/googlenav/ac;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->A:Lcom/google/googlenav/ai;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->u:Lcom/google/googlenav/ap;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/aq;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->s:Lcom/google/googlenav/aq;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ar;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->M:Lcom/google/googlenav/ar;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/cx;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->I:Z

    invoke-virtual {p1}, Lcom/google/googlenav/cx;->a()Lcom/google/googlenav/cy;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->J:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->F:Lcom/google/googlenav/ui/br;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->E:Lcom/google/googlenav/ui/e;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->w:Lcom/google/googlenav/ui/view/a;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->a:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/aV;->bs:Lcom/google/googlenav/ui/aV;

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    return-object p0
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->l:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 3

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    :goto_0
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bt;->e:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/google/googlenav/ui/view/android/bt;->f:Z

    :cond_0
    return-object p0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    goto :goto_0
.end method

.method public a(Ljava/util/Vector;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->t:Ljava/util/Vector;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->z:Z

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    array-length v1, p1

    if-lez v1, :cond_2

    array-length v1, p1

    new-array v1, v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    array-length v3, p1

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, p1, v1

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne v2, v0, :cond_1

    const v0, 0x7f0f00a5

    :goto_1
    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    invoke-static {v4, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    aput-object v0, v5, v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0f0106

    goto :goto_1

    :cond_2
    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->i:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->B:Z

    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->j:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->C:Z

    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->k:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->K:I

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    return-object v0
.end method

.method public e(Z)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->H:Z

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    sget-object v1, Lcom/google/googlenav/ui/aV;->bl:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->m:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->n:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->o:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .locals 1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->r:Ljava/lang/CharSequence;

    :cond_0
    return-object p0
.end method
