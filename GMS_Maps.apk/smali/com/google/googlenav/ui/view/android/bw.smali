.class Lcom/google/googlenav/ui/view/android/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field static final synthetic e:Z


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/googlenav/ai;

.field d:Lcom/google/googlenav/ui/view/android/bs;

.field final synthetic f:Lcom/google/googlenav/ui/view/android/bs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/view/android/bs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/view/android/bs;I)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bw;->f:Lcom/google/googlenav/ui/view/android/bs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iput p2, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/16 v4, 0x578

    sget-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->c:Lcom/google/googlenav/ai;

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bw;->f:Lcom/google/googlenav/ui/view/android/bs;

    iget-object v1, v1, Lcom/google/googlenav/ui/view/android/bs;->E:Lcom/google/googlenav/ui/e;

    iget v2, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    iget v3, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    invoke-interface {v1, v2, v3, v0}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    check-cast p1, Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
