.class public Lcom/google/googlenav/ui/view/android/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/AlertDialog;

.field final b:Lbf/bk;

.field final c:I

.field final d:Ljava/lang/String;

.field final e:Z

.field final f:Z

.field g:Landroid/view/View;

.field h:Landroid/widget/TextView;

.field i:Landroid/view/View;

.field j:Landroid/widget/LinearLayout;

.field k:Ljava/util/List;


# direct methods
.method public constructor <init>(ILbf/bk;Ljava/lang/String;ZZ)V
    .locals 8

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v7

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/view/android/c;-><init>(ILbf/bk;Ljava/lang/String;ZZLandroid/view/LayoutInflater;Landroid/content/Context;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->i()Landroid/app/Activity;

    move-result-object v7

    goto :goto_0
.end method

.method constructor <init>(ILbf/bk;Ljava/lang/String;ZZLandroid/view/LayoutInflater;Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->k:Ljava/util/List;

    iput p1, p0, Lcom/google/googlenav/ui/view/android/c;->c:I

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/c;->b:Lbf/bk;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/c;->d:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/view/android/c;->e:Z

    iput-boolean p5, p0, Lcom/google/googlenav/ui/view/android/c;->f:Z

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040005

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p6, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/c;->a(Landroid/view/View;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->a:Landroid/app/AlertDialog;

    return-void

    :cond_0
    const v0, 0x7f040004

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;I)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/d;

    invoke-direct {v0, p0, p2}, Lcom/google/googlenav/ui/view/android/d;-><init>(Lcom/google/googlenav/ui/view/android/c;I)V

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/android/e;Lcom/google/googlenav/ui/view/android/f;)V
    .locals 3

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/f;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/e;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget v0, p1, Lcom/google/googlenav/ui/view/android/e;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/f;->b:Landroid/widget/ImageView;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/f;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Lcom/google/googlenav/ui/view/android/e;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    iget v1, p1, Lcom/google/googlenav/ui/view/android/e;->c:I

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/c;->a(Landroid/view/ViewGroup;I)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/c;->a(Lcom/google/googlenav/ui/view/android/f;)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/f;)V
    .locals 3

    const/4 v1, -0x2

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/f;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/f;->c:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x41800000

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void

    :cond_0
    const/high16 v0, 0x41600000

    goto :goto_0
.end method

.method private c()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040007

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f040006

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x6a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 6

    const v5, 0x7f10001f

    const/4 v2, 0x0

    const v0, 0x7f10001d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->g:Landroid/view/View;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->h:Landroid/widget/TextView;

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->i:Landroid/view/View;

    const v0, 0x7f100020

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->j:Landroid/widget/LinearLayout;

    move v1, v2

    :goto_0
    const/4 v0, 0x4

    if-ge v1, v0, :cond_0

    new-instance v3, Lcom/google/googlenav/ui/view/android/f;

    invoke-direct {v3}, Lcom/google/googlenav/ui/view/android/f;-><init>()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/c;->c()I

    move-result v0

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/c;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v4, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    iget-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    const v4, 0x7f100022

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->b:Landroid/widget/ImageView;

    iget-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    const v4, 0x7f100023

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->c:Landroid/widget/TextView;

    iget-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v3, Lcom/google/googlenav/ui/view/android/f;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->j:Landroid/widget/LinearLayout;

    iget-object v4, v3, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/c;->b()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/c;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/c;->a(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method a(Ljava/util/List;Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x0

    const/16 v5, 0x8

    move v1, v3

    :goto_0
    const/4 v0, 0x4

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/f;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/f;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    return-void

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/c;->k:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/f;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/c;->a(Lcom/google/googlenav/ui/view/android/e;Lcom/google/googlenav/ui/view/android/f;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->h:Landroid/widget/TextView;

    invoke-static {v0, p2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/c;->k:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/f;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/f;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public b()Ljava/util/List;
    .locals 5

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/c;->e:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/view/android/e;

    const/16 v2, 0xf2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020198

    const/16 v4, 0x25b

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/e;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/c;->f:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/googlenav/ui/view/android/e;

    const/16 v2, 0x68

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200ad

    const/16 v4, 0x10

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/e;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, Lcom/google/googlenav/ui/view/android/e;

    const/16 v2, 0x616

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0201dc

    const/16 v4, 0x2bc

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/e;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/view/android/e;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/c;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/e;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
