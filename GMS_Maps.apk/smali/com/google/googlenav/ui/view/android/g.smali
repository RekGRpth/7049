.class public Lcom/google/googlenav/ui/view/android/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Lbf/bk;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/googlenav/ui/view/android/h;

.field e:Lcom/google/googlenav/ui/view/android/c;

.field private final f:Ljava/lang/CharSequence;

.field private final g:Ljava/lang/CharSequence;

.field private final h:Ljava/lang/CharSequence;

.field private final i:Ljava/lang/CharSequence;

.field private final j:Ljava/lang/CharSequence;

.field private final k:Ljava/lang/CharSequence;

.field private final l:Z

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private final p:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbf/bk;IZ)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->f:Ljava/lang/CharSequence;

    if-nez p2, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    if-nez p3, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    if-nez p4, :cond_3

    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    if-nez p5, :cond_4

    :goto_4
    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    iput-object p6, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    iput p7, p0, Lcom/google/googlenav/ui/view/android/g;->p:I

    iput-object p8, p0, Lcom/google/googlenav/ui/view/android/g;->a:Lbf/bk;

    iput p9, p0, Lcom/google/googlenav/ui/view/android/g;->b:I

    iput-boolean p10, p0, Lcom/google/googlenav/ui/view/android/g;->l:Z

    iput v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/aV;->ac:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/aV;->z:Lcom/google/googlenav/ui/aV;

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {p3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->bP:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/googlenav/ui/aV;->A:Lcom/google/googlenav/ui/aV;

    invoke-static {p4, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_3

    :cond_4
    sget-object v0, Lcom/google/googlenav/ui/aV;->A:Lcom/google/googlenav/ui/aV;

    invoke-static {p5, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_4
.end method

.method public static a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;
    .locals 11

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v2

    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aK()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v0

    invoke-virtual {v0}, Lai/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lai/d;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x3f0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v6

    :cond_3
    new-instance v0, Lcom/google/googlenav/ui/view/android/g;

    move-object v1, p1

    move v7, p2

    move-object v8, p3

    move v9, p4

    move/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/googlenav/ui/view/android/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbf/bk;IZ)V

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/view/android/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/g;->a(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->d()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    new-instance v1, Lcom/google/googlenav/ui/view/android/c;

    iget v3, v0, Lcom/google/googlenav/ui/view/android/g;->p:I

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->d()Z

    move-result v5

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->e()Z

    move-result v6

    move v2, v3

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/c;-><init>(ILbf/bk;Ljava/lang/String;ZZ)V

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/g;->c:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    :cond_0
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/g;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 6

    const v5, 0x7f100414

    const v4, 0x7f100022

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/g;->l:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/view/android/h;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/h;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1003e3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->g:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f090095

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1001a4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1001a6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->m:Landroid/widget/ImageView;

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    new-instance v1, Lcom/google/googlenav/ui/view/android/i;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/i;-><init>(Lcom/google/googlenav/ui/view/android/g;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f10030e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->d:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->f:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/g;->p:I

    iput v1, v0, Lcom/google/googlenav/ui/view/android/i;->a:I

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/i;->b:Lcom/google/googlenav/ui/view/android/c;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->a:Landroid/view/View;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public b()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f04016c

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04016d

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/16 v2, 0xa

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    if-eqz v1, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
