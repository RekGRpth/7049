.class public Lcom/google/googlenav/ui/view/android/j;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/p;

.field private b:Lcom/google/googlenav/suggest/android/SuggestView;

.field private c:Lcom/google/googlenav/suggest/android/SuggestView;

.field private d:Landroid/view/View;

.field private l:Landroid/widget/RadioGroup;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Lo/D;

.field private q:LaN/B;

.field private final r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Lcom/google/googlenav/j;

.field private w:Z

.field private final x:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/p;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0018

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/k;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/k;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->x:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gR;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void

    :cond_0
    const v0, 0x7f0f001f

    goto :goto_0
.end method

.method private A()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clear()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method private B()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gR;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x4

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x3

    invoke-virtual {p0}, Lax/y;->q()Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v3

    invoke-virtual {p0}, Lax/y;->m()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lax/y;->m()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lax/y;->n()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lax/y;->n()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_1
    const/4 v4, -0x1

    invoke-static {v3, v0, v1, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/w;

    return-object v0
.end method

.method private a(Lax/y;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x109

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v0, 0x7f100177

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x608

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v0, 0x7f100175

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x5ca

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v0, 0x7f100176

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 2

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/suggest/android/SuggestView;Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/aV;->aE:Lcom/google/googlenav/ui/aV;

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->v()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->f(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    return-void
.end method

.method private a(ZLandroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setSuggestEnabled(Z)V

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    const-string v0, ""

    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lax/y;->o()Z

    move-result v1

    invoke-virtual {p3, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSuggestEnabled(Z)V

    invoke-virtual {p2}, Lax/y;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_2

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    invoke-direct {p0, p3, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    invoke-virtual {p3}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aE:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/text/Editable;Lcom/google/googlenav/ui/aV;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    throw v0

    :cond_3
    :try_start_3
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    sget-object v1, Lcom/google/googlenav/ui/aV;->aF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p3}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private a(LaN/B;)Z
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p1, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    invoke-static {v0, v2}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;Lax/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Lax/y;Lax/y;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Lax/y;->l()Lo/D;

    move-result-object v1

    invoke-virtual {p2}, Lax/y;->l()Lo/D;

    move-result-object v2

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v3, v1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v1

    invoke-virtual {v2}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v3, v2}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v2}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    const v2, 0x7f10016e

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(Z)V

    invoke-direct {p0, v1, p1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLandroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f100178

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->c(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->A()V

    return-void

    :pswitch_0
    const v0, 0x7f100174

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f100175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f100177

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f100176

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/u;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f10017a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x1ba

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100179

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/v;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/v;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    const/16 v0, 0x1cb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/l;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/l;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-static {v2, v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->A()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->c(Landroid/view/View;)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/j;)Lcom/google/googlenav/suggest/android/SuggestView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    invoke-virtual {v0}, Lax/o;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/view/android/n;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/android/n;-><init>(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lax/o;->a(Lax/r;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->d(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->d(Landroid/view/View;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/android/j;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->x:Ljava/lang/Runnable;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/f;->a(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->o()Lcom/google/googlenav/ui/view/p;

    move-result-object v0

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    invoke-virtual {v1, v2}, Lax/o;->a(I)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;Ljava/util/Vector;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/view/q;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/android/S;Landroid/view/View;Lcom/google/googlenav/ui/view/q;)V

    goto :goto_0
.end method

.method private e(I)Lcom/google/googlenav/ui/android/ad;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/m;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/m;-><init>(Lcom/google/googlenav/ui/view/android/j;I)V

    return-object v0
.end method

.method private e(Landroid/view/View;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    const v0, 0x7f100179

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->B()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/android/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->z()V

    return-void
.end method

.method private f(I)V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f10016b

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/android/j;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/view/android/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->x()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/view/android/j;)Lcom/google/googlenav/ui/view/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    return-object v0
.end method

.method private v()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->dismissDropDown()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->dismissDropDown()V

    goto :goto_0
.end method

.method private static w()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.inputmethod.latin/com.android.inputmethod.latin.LatinIME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()V
    .locals 1

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->j()V

    return-void
.end method

.method private y()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->z()V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const/16 v2, 0x3c6

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/c;->a(Landroid/app/Activity;Ljava/lang/String;)V

    const v1, 0x7f10016b

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v3}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/maps/MapsActivity;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const/16 v2, 0x3c5

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/c;->a(Landroid/app/Activity;Ljava/lang/String;)V

    const v1, 0x7f1000c5

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/maps/MapsActivity;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private z()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v2, v5, v5, v4}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lax/j;->a(Lax/y;)V

    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/view/android/j;->b(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2, v5, v5, v4}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {v2, v1}, Lax/j;->b(Lax/y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->h:Landroid/view/View;

    invoke-direct {p0, v4, v0}, Lcom/google/googlenav/ui/view/android/j;->a(ZLandroid/view/View;)V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {v0}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method protected I_()V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x37

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->as()Z

    move-result v1

    if-nez v1, :cond_1

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_1
    return-void
.end method

.method protected O_()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setUiOptions(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->requestWindowFeature(I)Z

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v0, 0x7f10016b

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f1000c5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f10016e

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/16 v0, 0xfc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, -0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->f:Lcom/google/googlenav/ui/e;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_0
    const/16 v0, 0xec

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->y()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->x()V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xd6

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xe4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f1004ac
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1004ad

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->B()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const v0, 0x7f10016b

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1000c5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected c()Landroid/view/View;
    .locals 8

    const v7, 0x7f100174

    const/4 v4, 0x0

    const v6, 0x7f100176

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->h()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->k()V

    const v0, 0x7f10016b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    const/16 v1, 0x587

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setEnoughToFilter(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/w;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/ui/view/android/w;-><init>(Lcom/google/googlenav/ui/view/android/j;Lcom/google/googlenav/ui/view/android/k;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelectAllOnFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/suggest/android/SuggestView;->setInputIndex(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/o;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/o;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/p;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/p;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f1000c5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    const/16 v1, 0x118

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setEnoughToFilter(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/w;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/ui/view/android/w;-><init>(Lcom/google/googlenav/ui/view/android/j;Lcom/google/googlenav/ui/view/android/k;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelectAllOnFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setInputIndex(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/q;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/r;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/r;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/s;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/s;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f100169

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    const v0, 0x7f1000c6

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->l:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->l:Landroid/widget/RadioGroup;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    const v0, 0x7f10016c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0xc4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xd4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10016f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0xc2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xd5

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/android/j;->b(Landroid/view/View;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f100170

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/16 v1, 0x5a3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xe4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100172

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/googlenav/ui/view/android/t;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/android/t;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-static {v0, v1, v4, v3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ad;Landroid/view/View;)Landroid/widget/Button;

    const/16 v0, 0x10a

    invoke-direct {p0, v3, v7, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    const v0, 0x7f100175

    const/16 v1, 0x5d6

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    const v0, 0x7f100177

    const/16 v1, 0x5df

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    const/16 v0, 0x232

    invoke-direct {p0, v3, v6, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    const v0, 0x7f10001e

    const/16 v1, 0xfc

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    :cond_0
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100175

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100177

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v3

    :cond_1
    const/16 v1, 0x8

    goto/16 :goto_0
.end method

.method public c(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    const v0, 0x7f10016b

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const v0, 0x7f1000c5

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_0
.end method

.method protected d()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/ui/view/android/j;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_1
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_1
.end method

.method protected f()V
    .locals 1

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->j()V

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    return-void
.end method

.method protected h()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040061

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f040062

    goto :goto_0
.end method

.method public k()V
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    :cond_0
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v4, v1

    :goto_0
    if-nez v4, :cond_3

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    if-nez v4, :cond_4

    :goto_2
    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v1, v4, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    invoke-static {}, LO/T;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    :cond_1
    return-void

    :cond_2
    invoke-interface {v0}, LaH/m;->r()LaH/h;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, LaH/h;->b()Lo/D;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, LaH/h;->a()LaN/B;

    move-result-object v1

    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->h:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->b(Landroid/view/View;)V

    return-void
.end method

.method protected m()V
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public n()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-eqz v0, :cond_3

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/android/j;->a(LaN/B;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    :goto_2
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public o()Lcom/google/googlenav/ui/view/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1004ad

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x2f8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1004ae

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x1bb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1004ac

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x5a3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->onStop()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->f()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->f()V

    return-void
.end method
