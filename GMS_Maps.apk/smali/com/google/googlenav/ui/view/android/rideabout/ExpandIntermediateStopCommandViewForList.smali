.class public Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/android/rideabout/f;


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field private final b:Lax/t;

.field private final c:Ljava/util/List;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:Lcom/google/googlenav/ui/view/android/rideabout/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->a:Ljava/lang/Object;

    return-void
.end method

.method private a()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/j;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/rideabout/j;->a(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->b:Lax/t;

    invoke-virtual {v0}, Lax/t;->O()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->d:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->e:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->a()V

    return-void
.end method

.method public setExpansionStateListener(Lcom/google/googlenav/ui/view/android/rideabout/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandViewForList;->f:Lcom/google/googlenav/ui/view/android/rideabout/g;

    return-void
.end method
