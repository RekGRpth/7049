.class public Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;
.super Landroid/widget/TableLayout;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p0, v3, v2}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->setColumnStretchable(IZ)V

    invoke-virtual {p0, v3, v2}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->setColumnShrinkable(IZ)V

    const/16 v0, 0x8

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->setPadding(IIII)V

    return-void
.end method

.method protected static a(Landroid/view/ViewGroup;)Landroid/view/LayoutInflater;
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;ILcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;)V
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p4}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->e()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p4, p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setExpandCommandView(Lcom/google/googlenav/ui/view/android/rideabout/f;)V

    invoke-virtual {p2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setExpandCommandView(Lcom/google/googlenav/ui/view/android/rideabout/f;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/t;

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/h;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/h;-><init>(II)V

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;Lbi/t;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/h;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/h;-><init>(II)V

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    new-instance v2, Lcom/google/googlenav/ui/view/android/rideabout/h;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentTransfer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentTransfer;->i()I

    move-result v0

    invoke-direct {v2, v3, v0}, Lcom/google/googlenav/ui/view/android/rideabout/h;-><init>(II)V

    invoke-virtual {v1, p3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 6

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v4

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v4, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->a()I

    move-result v5

    if-gt p1, v5, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1, v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->measure(II)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public a(Lbi/a;Lbi/t;)I
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/h;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/rideabout/h;

    iget v1, v1, Lcom/google/googlenav/ui/view/android/rideabout/h;->b:I

    move v2, v1

    :goto_1
    iget v1, v0, Lcom/google/googlenav/ui/view/android/rideabout/h;->a:I

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->getTop()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v1

    iget v0, v0, Lcom/google/googlenav/ui/view/android/rideabout/h;->b:I

    iget v4, p1, Lbi/a;->b:F

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->a(IIF)I

    move-result v0

    add-int v1, v3, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public a(ILandroid/widget/LinearLayout;Lbi/t;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Landroid/widget/TableRow;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentFirstWalk;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    sget-object v3, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentFirstWalk;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/rideabout/m;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V

    return-void
.end method

.method protected a(Landroid/widget/TableRow;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    invoke-virtual {p1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/rideabout/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/rideabout/m;ILcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;IILandroid/widget/LinearLayout;Lbi/t;Lbi/t;)V
    .locals 9

    new-instance v8, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v8, v2, p2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0, v8}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Landroid/widget/TableRow;)V

    new-instance v2, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentTransfer;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    sget-object v5, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-object v4, p1

    move v6, p5

    move v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentTransfer;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/rideabout/m;Lcom/google/googlenav/ui/view/android/rideabout/m;II)V

    move-object/from16 v0, p7

    invoke-virtual {v8, v2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    move-object/from16 v0, p8

    move-object/from16 v1, p9

    invoke-direct {p0, v8, v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;Lbi/t;)V

    invoke-direct {p0, p3, p4, p2, v8}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;ILcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/rideabout/m;ILcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;ILandroid/widget/LinearLayout;Lbi/t;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Landroid/widget/TableRow;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    sget-object v3, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    invoke-direct {v1, v2, p1, v3, p5}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/rideabout/m;Lcom/google/googlenav/ui/view/android/rideabout/m;I)V

    invoke-virtual {v0, v1, p6}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    invoke-direct {p0, v0, p7}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V

    invoke-direct {p0, p3, p4, p2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;ILcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/rideabout/m;IZIILcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;Landroid/widget/LinearLayout;Lbi/t;)V
    .locals 7

    new-instance v6, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v6, v0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Landroid/widget/TableRow;)V

    if-eqz p6, :cond_0

    invoke-virtual {v6, p6}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setExpandCommandView(Lcom/google/googlenav/ui/view/android/rideabout/f;)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-object v3, p1

    move v4, p5

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/rideabout/m;Lcom/google/googlenav/ui/view/android/rideabout/m;II)V

    invoke-virtual {v6, v0, p7}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    invoke-direct {p0, v6, p8}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V

    return-void
.end method

.method public b()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->g()I

    move-result v0

    goto :goto_0
.end method

.method public b(ILandroid/widget/LinearLayout;Lbi/t;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Landroid/widget/TableRow;)V

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentEnd;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentEnd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;Lbi/t;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/android/rideabout/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lbi/a;)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/h;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget v0, v0, Lcom/google/googlenav/ui/view/android/rideabout/h;->a:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public c()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->c()Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->h()I

    move-result v0

    add-int/lit8 v0, v0, 0xa

    goto :goto_0
.end method

.method public c(Lbi/a;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b:Ljava/util/HashMap;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/h;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/google/googlenav/ui/view/android/rideabout/h;->a:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p1, Lbi/a;->b:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->d()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    invoke-super/range {p0 .. p5}, Landroid/widget/TableLayout;->onLayout(ZIIII)V

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/i;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/i;->a(ZIIII)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    return-void
.end method
