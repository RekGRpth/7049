.class public Lcom/google/googlenav/ui/view/android/rideabout/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/google/googlenav/ui/view/android/rideabout/f;

.field private final d:Z

.field private e:I

.field private f:Z


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->e:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 4

    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/l;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/l;-><init>(Lcom/google/googlenav/ui/view/android/rideabout/k;)V

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->a:Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->c:Lcom/google/googlenav/ui/view/android/rideabout/f;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setExpandCommandView(Lcom/google/googlenav/ui/view/android/rideabout/f;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->d:Z

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setIntermediateStop(Z)V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-object v1

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lcom/google/googlenav/ui/view/android/rideabout/l;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->f:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lcom/google/googlenav/ui/view/android/rideabout/l;->a(Lcom/google/googlenav/ui/view/android/rideabout/l;)Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/RouteItemView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/rideabout/j;->f:Z

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040160

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
