.class Lcom/google/googlenav/ui/view/android/w;
.super Landroid/text/method/ArrowKeyMovementMethod;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/android/j;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/android/j;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/w;->a:Lcom/google/googlenav/ui/view/android/j;

    invoke-direct {p0}, Landroid/text/method/ArrowKeyMovementMethod;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/w;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/view/android/j;Lcom/google/googlenav/ui/view/android/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/w;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    return-void
.end method

.method private b()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/w;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/w;->b:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/w;->b:Z

    return v0
.end method

.method public canSelectArbitrarily()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/text/method/ArrowKeyMovementMethod;->initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V

    return-void
.end method

.method public onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/ArrowKeyMovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyOther(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/ArrowKeyMovementMethod;->onKeyOther(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/ArrowKeyMovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/w;->a:Lcom/google/googlenav/ui/view/android/j;

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/ui/view/android/j;I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/ArrowKeyMovementMethod;->onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/w;->a:Lcom/google/googlenav/ui/view/android/j;

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/ui/view/android/j;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/ArrowKeyMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
