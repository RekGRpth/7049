.class Lcom/google/googlenav/ui/view/dialog/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:LaG/f;


# direct methods
.method public constructor <init>(LaG/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/B;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;-><init>(Lcom/google/googlenav/ui/view/dialog/t;)V

    const v0, 0x7f10008e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->a(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/TableLayout;)Landroid/widget/TableLayout;

    const v0, 0x7f100090

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->a(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100092

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->b(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100093

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->c(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100094

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->d(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100091

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->a(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    const v0, 0x7f10008f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/dialog/B;->b(Lcom/google/googlenav/ui/view/dialog/B;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 3

    check-cast p2, Lcom/google/googlenav/ui/view/dialog/B;

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->a(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v1}, LaG/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->b(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v1}, LaG/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->c(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v1}, LaG/f;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->d(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v1}, LaG/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->e(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v0}, LaG/f;->g()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->f(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v0}, LaG/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/A;->a:LaG/f;

    invoke-virtual {v0}, LaG/f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->c(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f010f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->g(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TableLayout;

    move-result-object v0

    const v1, 0x7f02028f

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setBackgroundResource(I)V

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->c(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0110

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/B;->g(Lcom/google/googlenav/ui/view/dialog/B;)Landroid/widget/TableLayout;

    move-result-object v0

    const v1, 0x7f02028e

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public b()I
    .locals 1

    const v0, 0x7f04001c

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
