.class public Lcom/google/googlenav/ui/view/dialog/aH;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/dialog/aL;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/aL;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aH;->a:Lcom/google/googlenav/ui/view/dialog/aL;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/aH;)Lcom/google/googlenav/ui/view/dialog/aL;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aH;->a:Lcom/google/googlenav/ui/view/dialog/aL;

    return-object v0
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->onBackPressed()V

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f020218

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100039

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->w_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const/16 v0, 0x278

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/K;->ad()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/googlenav/K;->U()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f1002bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    const v0, 0x7f1002be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    const/16 v2, 0x293

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aI;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/aI;-><init>(Lcom/google/googlenav/ui/view/dialog/aH;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100172

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    const/16 v2, 0x69

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aJ;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/aJ;-><init>(Lcom/google/googlenav/ui/view/dialog/aH;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aK;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aK;-><init>(Lcom/google/googlenav/ui/view/dialog/aH;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aH;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aH;->w_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aH;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x26b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
