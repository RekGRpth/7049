.class public Lcom/google/googlenav/ui/view/dialog/ap;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private final e:Lcom/google/googlenav/ui/view/dialog/ao;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/ao;)V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "gmm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "continue"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "googleplussignup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->b:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ao;->d()V

    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->d:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->a:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->b:Z

    const-string v0, "gmm://googleplussignup?state=confirmed"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ao;->a()V

    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->a:Z

    goto :goto_0

    :cond_2
    const-string v0, "gmm://googleplussignup?state=cancelled"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ao;->b()V

    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    goto :goto_1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "gmm://googleplussignup?state=confirmed"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/ao;->a()V

    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    :goto_0
    return v0

    :cond_0
    const-string v1, "gmm://googleplussignup?state=cancelled"

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->e:Lcom/google/googlenav/ui/view/dialog/ao;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/ao;->b()V

    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/dialog/ap;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->b:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    if-nez v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ap;->b:Z

    if-nez v1, :cond_3

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ap;->c:Z

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
