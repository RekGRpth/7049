.class public Lcom/google/googlenav/ui/view/dialog/ar;
.super Lcom/google/googlenav/ui/view/android/aL;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/friend/aI;

.field private b:Z

.field private final c:Lcom/google/googlenav/ui/view/dialog/av;

.field private d:Z

.field private final e:Ljava/lang/CharSequence;

.field private f:LaB/a;

.field private g:Lcom/google/googlenav/ui/friend/D;

.field private h:LaB/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/google/googlenav/friend/aI;ZLaB/a;Lcom/google/googlenav/ui/friend/D;Lcom/google/googlenav/ui/view/dialog/av;)V
    .locals 1

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/aL;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->b:Z

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/ar;->e:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    iput-object p7, p0, Lcom/google/googlenav/ui/view/dialog/ar;->c:Lcom/google/googlenav/ui/view/dialog/av;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/view/dialog/ar;->d:Z

    iput-object p5, p0, Lcom/google/googlenav/ui/view/dialog/ar;->f:LaB/a;

    iput-object p6, p0, Lcom/google/googlenav/ui/view/dialog/ar;->g:Lcom/google/googlenav/ui/friend/D;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->setCancelable(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ar;)Lcom/google/googlenav/friend/aI;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->e:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f100025

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ar;->b(Landroid/widget/ImageView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->b:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    new-instance v1, LaB/n;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LaB/n;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/at;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/googlenav/ui/view/dialog/at;-><init>(Lcom/google/googlenav/ui/view/dialog/ar;Landroid/os/Handler;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ar;->h:LaB/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->f:LaB/a;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ar;->h:LaB/p;

    invoke-virtual {v1, v2}, LaB/a;->a(LaB/p;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->f:LaB/a;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ar;->h:LaB/p;

    invoke-virtual {v1, v0, v2}, LaB/a;->a(Ljava/util/Vector;LaB/p;)Z

    :cond_0
    return-void
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x165

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v1, 0x1

    const/16 v2, 0x166

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v1, 0x2

    const/16 v2, 0x167

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->d:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    const/16 v2, 0x163

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ar;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f04009b

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/as;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/as;-><init>(Lcom/google/googlenav/ui/view/dialog/ar;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ar;Landroid/widget/ImageView;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ar;->b(Landroid/widget/ImageView;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/ar;)Lcom/google/googlenav/ui/view/dialog/av;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->c:Lcom/google/googlenav/ui/view/dialog/av;

    return-object v0
.end method

.method private b(Landroid/widget/ImageView;)Z
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/friend/E;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/friend/E;-><init>(Ljava/lang/Long;I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ar;->g:Lcom/google/googlenav/ui/friend/D;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/friend/D;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->invalidate()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/ar;)LaB/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->h:LaB/p;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/dialog/ar;)LaB/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->f:LaB/a;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ar;->c:Lcom/google/googlenav/ui/view/dialog/av;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/av;->a()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/dialog/ar;->requestWindowFeature(I)Z

    :cond_0
    const v0, 0x7f04009a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/ar;->a()V

    const v0, 0x7f10021c

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x168

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/ar;->a:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v4}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1001bd

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->a(Landroid/widget/ImageView;)V

    const v0, 0x7f10012a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ar;->a(Landroid/widget/ListView;)V

    return-void
.end method
