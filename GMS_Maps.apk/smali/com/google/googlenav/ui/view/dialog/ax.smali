.class public Lcom/google/googlenav/ui/view/dialog/ax;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;

.field private b:Lcom/google/googlenav/ui/view/dialog/aD;

.field private c:Lcom/google/googlenav/ui/view/dialog/aB;

.field private d:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/aD;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->w_()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020218

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/ax;->a(Ljava/lang/CharSequence;II)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ax;)Lcom/google/googlenav/ui/view/dialog/aD;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;
    .locals 9

    const/4 v6, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    instance-of v2, v0, Lcom/google/googlenav/friend/history/x;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    instance-of v2, v0, Lcom/google/googlenav/friend/history/aa;

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    :cond_3
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/googlenav/friend/history/b;->b(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    const/16 v2, 0xe

    if-le v0, v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_5
    invoke-static {p2, p3}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v6

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/b;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->b()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v1

    if-ltz v1, :cond_6

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->e()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    new-instance v1, Lcom/google/googlenav/friend/history/X;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/googlenav/friend/history/X;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    :goto_3
    move v2, v0

    goto :goto_2

    :cond_7
    new-instance v4, Lcom/google/googlenav/friend/history/j;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v4, v0, v5, v1, v8}, Lcom/google/googlenav/friend/history/j;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;Lcom/google/googlenav/friend/history/U;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    goto :goto_3

    :cond_8
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/friend/history/s;

    if-nez v0, :cond_9

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v4, -0x7

    invoke-virtual {v2, v4}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    invoke-interface {v7, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_9
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    instance-of v1, v0, Lcom/google/googlenav/friend/history/s;

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    const/4 v0, 0x7

    invoke-virtual {v2, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-gtz v0, :cond_b

    :goto_4
    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    return-object v7

    :cond_b
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    goto :goto_4
.end method

.method static synthetic n()Lcom/google/googlenav/friend/history/b;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    return-object v0
.end method

.method private static o()Lcom/google/googlenav/friend/history/b;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/friend/history/b;->a()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public I_()V
    .locals 6

    const/4 v5, 0x1

    const v0, 0x7f020218

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ay;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ay;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    new-array v2, v5, [I

    const/4 v3, 0x0

    const/16 v4, 0xbc3

    aput v4, v2, v3

    invoke-virtual {p0, v5, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/ax;->a(ZILaA/f;[I)V

    return-void
.end method

.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/aD;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ax;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, -0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-gtz v0, :cond_0

    :goto_0
    iget-object v6, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    goto :goto_0
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/ax;->b(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    :cond_0
    instance-of v2, v0, Lcom/google/googlenav/friend/history/P;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/googlenav/friend/history/P;

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/friend/history/P;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/friend/history/P;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 6

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aA;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/aA;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/b;->g()I

    move-result v3

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/friend/history/b;->f()I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/friend/history/b;->e()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/b;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    :cond_0
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    invoke-static {}, Lcom/google/googlenav/friend/history/y;->d()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Landroid/view/View;
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e5

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f1002b7

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/view/dialog/aB;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;Lcom/google/googlenav/ui/view/dialog/ay;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/az;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/az;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ai;)V

    new-instance v4, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v4}, Lcom/google/googlenav/friend/history/b;-><init>()V

    const/4 v0, -0x2

    invoke-virtual {v4, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v2, Lcom/google/googlenav/friend/history/aa;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/friend/history/aa;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v5, -0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/friend/history/P;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/friend/history/P;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-object v6
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aB;->c()V

    return-void
.end method

.method public l()V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v4, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v4}, Lcom/google/googlenav/friend/history/b;-><init>()V

    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v5, -0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    return-void
.end method

.method public m()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/O;->a()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/aD;->a()V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x287

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02023a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x289

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
