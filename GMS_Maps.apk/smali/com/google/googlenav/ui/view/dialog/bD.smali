.class public Lcom/google/googlenav/ui/view/dialog/bD;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaT/m;


# static fields
.field private static final s:LaT/f;


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;

.field private final b:LaT/a;

.field private c:Lcom/google/googlenav/ui/view/dialog/bH;

.field private d:Landroid/view/LayoutInflater;

.field private volatile l:I

.field private m:Ljava/lang/String;

.field private volatile n:I

.field private volatile o:I

.field private final p:Ljava/text/DateFormat;

.field private q:I

.field private r:LaT/f;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, LaT/f;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1, v2}, LaT/f;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILandroid/graphics/Bitmap;)V

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->s:LaT/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->q:I

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bD;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->b:LaT/a;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->p:Ljava/text/DateFormat;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bD;)LaT/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->r:LaT/f;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bD;Lcom/google/googlenav/ui/view/dialog/bH;)Lcom/google/googlenav/ui/view/dialog/bH;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bD;->c:Lcom/google/googlenav/ui/view/dialog/bH;

    return-object p1
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LaT/f;)V
    .locals 6

    invoke-virtual {p1}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->i()I

    move-result v1

    invoke-virtual {p1}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->j()I

    move-result v2

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->G()I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/googlenav/ui/bi;->c:I

    :goto_0
    add-int/2addr v0, v4

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/android/ButtonContainer;->d()Lcom/google/googlenav/ui/android/ac;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v5

    invoke-virtual {v4}, Lcom/google/googlenav/ui/android/ac;->a()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-virtual {v5, v1, v2, v0, v4}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v2

    invoke-virtual {p1}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->k()LaN/B;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, LaN/B;

    invoke-virtual {v1, v2}, LaN/B;->a(LaN/Y;)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/ui/bi;->G()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2}, LaN/B;->b(LaN/Y;)I

    move-result v1

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v5

    invoke-direct {v0, v4, v1, v5}, LaN/B;-><init>(III)V

    :goto_1
    sget-object v1, Lcom/google/googlenav/ui/view/dialog/bD;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {p1}, LaT/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lbf/am;->a(LaN/B;LaN/Y;Ljava/lang/String;)Lbf/aM;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bD;LaT/f;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bD;->a(LaT/f;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bD;[LaT/f;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bD;->a([LaT/f;)V

    return-void
.end method

.method private a([LaT/f;)V
    .locals 4

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bD;->c:Lcom/google/googlenav/ui/view/dialog/bH;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/view/dialog/bH;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(LaT/f;)Landroid/app/AlertDialog;
    .locals 4

    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020215

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x346

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bF;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/googlenav/ui/view/dialog/bF;-><init>(Lcom/google/googlenav/ui/view/dialog/bD;Landroid/widget/EditText;LaT/f;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {p1}, LaT/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object v1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/bD;LaT/f;)Landroid/app/AlertDialog;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bD;->b(LaT/f;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/bD;)Lcom/google/googlenav/ui/view/dialog/bH;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->c:Lcom/google/googlenav/ui/view/dialog/bH;

    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    mul-int/lit8 v1, p0, 0xa

    div-int/lit16 v1, v1, 0x400

    int-to-double v1, v1

    const-wide/high16 v3, 0x4024000000000000L

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(LaT/f;)Landroid/app/AlertDialog;
    .locals 4

    invoke-virtual {p1}, LaT/f;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020215

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    if-eqz v0, :cond_1

    const/16 v1, 0x331

    :goto_1
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x315

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x61c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bG;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/googlenav/ui/view/dialog/bG;-><init>(Lcom/google/googlenav/ui/view/dialog/bD;ZLaT/f;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x32a

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/bD;LaT/f;)Landroid/app/AlertDialog;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bD;->c(LaT/f;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/bD;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    const/16 v0, 0x353

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/bD;->n:I

    invoke-static {v1}, Lcom/google/googlenav/ui/view/dialog/bD;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/bD;->o:I

    invoke-static {v1}, Lcom/google/googlenav/ui/view/dialog/bD;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x350

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x352

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/dialog/bD;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/dialog/bD;)Lcom/google/googlenav/android/aa;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->m()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/dialog/bD;)Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->p:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/view/dialog/bD;)LaT/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->b:LaT/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/view/dialog/bD;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->l:I

    return v0
.end method

.method static synthetic h()LaT/f;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->s:LaT/f;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/view/dialog/bD;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/view/dialog/bD;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->q:I

    return v0
.end method

.method static synthetic l()Lcom/google/googlenav/android/BaseMapsActivity;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    return-object v0
.end method

.method private m()Lcom/google/googlenav/android/aa;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->d:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040161

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->h:Landroid/view/View;

    return-object v0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public f()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bD;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->p()V

    :cond_0
    return-void
.end method

.method public onOfflineDataUpdate(LaT/l;)V
    .locals 3

    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaT/l;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->q:I

    :cond_0
    invoke-virtual {p1}, LaT/l;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LaT/l;->b()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->l:I

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->l:I

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bD;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->m:Ljava/lang/String;

    :cond_1
    invoke-virtual {p1}, LaT/l;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaT/l;->e()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->o:I

    :cond_2
    invoke-virtual {p1}, LaT/l;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, LaT/l;->d()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->n:I

    :cond_3
    invoke-virtual {p1}, LaT/l;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LaT/l;->g()LaT/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->r:LaT/f;

    :cond_4
    invoke-virtual {p1}, LaT/l;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, LaT/l;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bD;->m:Ljava/lang/String;

    :cond_5
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bD;->m()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bE;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/dialog/bE;-><init>(Lcom/google/googlenav/ui/view/dialog/bD;LaT/l;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method
