.class final Lcom/google/googlenav/ui/view/dialog/bH;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/bD;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/dialog/bD;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/view/dialog/bD;Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/bE;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/bH;-><init>(Lcom/google/googlenav/ui/view/dialog/bD;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/16 v8, 0x8

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bD;->c(Lcom/google/googlenav/ui/view/dialog/bD;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400cb

    invoke-virtual {v0, v1, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bS;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/bS;-><init>(Lcom/google/googlenav/ui/view/dialog/bE;)V

    const v0, 0x7f100277

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->a:Landroid/widget/ImageView;

    const v0, 0x7f100199

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->b:Landroid/widget/TextView;

    const v0, 0x7f1001e7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->c:Landroid/view/View;

    const v0, 0x7f100279

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->e:Landroid/widget/TextView;

    const v0, 0x7f100278

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->d:Landroid/widget/TextView;

    const v0, 0x7f1001fc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    const v0, 0x7f10027e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    const v0, 0x7f10027a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    const v0, 0x7f10027c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->j:Landroid/widget/TextView;

    const v0, 0x7f10027b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->k:Landroid/widget/ImageView;

    const v0, 0x7f10027f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    const v0, 0x7f10027d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bD;->b(Lcom/google/googlenav/ui/view/dialog/bD;)Lcom/google/googlenav/ui/view/dialog/bH;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/dialog/bH;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/f;

    invoke-virtual {p2, v9}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->a(Lcom/google/googlenav/ui/view/dialog/bD;)LaT/f;

    move-result-object v2

    if-nez v2, :cond_1

    if-nez p1, :cond_1

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->a:Landroid/widget/ImageView;

    const v2, 0x7f0202d4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->b:Landroid/widget/TextView;

    const/16 v2, 0x33c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->c:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bI;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bI;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bS;

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, LaT/f;->h()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0}, LaT/f;->h()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_2
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, LaT/f;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->a(Lcom/google/googlenav/ui/view/dialog/bD;)LaT/f;

    move-result-object v2

    if-eq v0, v2, :cond_8

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->c:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v3, ""

    const v2, 0x7f0f0035

    invoke-virtual {v0}, LaT/f;->a()I

    move-result v4

    if-ne v4, v10, :cond_5

    const/16 v2, 0x343

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v2, 0x7f0f0036

    :cond_2
    :goto_3
    iget-object v4, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bH;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_4
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->a(Lcom/google/googlenav/ui/view/dialog/bD;)LaT/f;

    move-result-object v2

    if-ne v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-ne v2, v10, :cond_f

    :cond_3
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    const v3, 0x7f0203e1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/bQ;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/view/dialog/bQ;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;LaT/f;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_4
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/bJ;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/googlenav/ui/view/dialog/bJ;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;LaT/f;Landroid/widget/ImageView;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v0}, LaT/f;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    const/16 v2, 0x342

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v2, 0x7f0f0036

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, LaT/f;->a()I

    move-result v4

    if-nez v4, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v4}, Lcom/google/googlenav/ui/view/dialog/bD;->f(Lcom/google/googlenav/ui/view/dialog/bD;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v0}, LaT/f;->c()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v4

    mul-int/lit8 v4, v4, 0xf

    invoke-static {v4}, Lcom/google/googlenav/ui/view/dialog/bD;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/bL;

    invoke-direct {v4, p0, p1, v0}, Lcom/google/googlenav/ui/view/dialog/bL;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;ILaT/f;)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v0}, LaT/f;->a()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    const/16 v2, 0x336

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v2, 0x7f0f0037

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/bM;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/ui/view/dialog/bM;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;LaT/f;)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    :cond_8
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-ne v2, v6, :cond_9

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->j:Landroid/widget/TextView;

    const/16 v3, 0x340

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->k:Landroid/widget/ImageView;

    const v3, 0x7f0202e4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_5
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bN;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/bN;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-ne v2, v6, :cond_b

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    const v3, 0x7f020192

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bO;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/bO;-><init>(Lcom/google/googlenav/ui/view/dialog/bH;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_6
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->c:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-ne v2, v10, :cond_c

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->d:Landroid/widget/TextView;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_7
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v3}, Lcom/google/googlenav/ui/view/dialog/bD;->i(Lcom/google/googlenav/ui/view/dialog/bD;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->j(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-eq v2, v5, :cond_d

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->e:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v4}, Lcom/google/googlenav/ui/view/dialog/bD;->j(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_8
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->j(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    if-eq v2, v5, :cond_e

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v3}, Lcom/google/googlenav/ui/view/dialog/bD;->j(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_4

    :cond_9
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bH;->a:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-static {v2}, Lcom/google/googlenav/ui/view/dialog/bD;->h(Lcom/google/googlenav/ui/view/dialog/bD;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->i:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->j:Landroid/widget/TextView;

    const/16 v3, 0x69

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->k:Landroid/widget/ImageView;

    const v3, 0x7f020192

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    :cond_a
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->j:Landroid/widget/TextView;

    const/16 v3, 0x347

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->k:Landroid/widget/ImageView;

    const v3, 0x7f020372

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    :cond_b
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->l:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_6

    :cond_c
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->d:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_7

    :cond_d
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    :cond_e
    iget-object v2, v1, Lcom/google/googlenav/ui/view/dialog/bS;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_4

    :cond_f
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/bS;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1
.end method
