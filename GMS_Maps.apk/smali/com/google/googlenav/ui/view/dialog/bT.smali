.class public Lcom/google/googlenav/ui/view/dialog/bT;
.super Lcom/google/googlenav/ui/view/dialog/bv;
.source "SourceFile"


# static fields
.field private static final n:Lcom/google/googlenav/ui/view/a;

.field private static final s:[I

.field private static final t:[I


# instance fields
.field protected final d:Lbf/bk;

.field protected final l:Lcom/google/googlenav/ui/bi;

.field protected m:Landroid/widget/ListView;

.field private o:Lcom/google/googlenav/ui/android/FlowLayout;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/ViewGroup;

.field private r:LaW/e;

.field private u:Lcom/google/googlenav/ui/android/LoadingFooterView;

.field private v:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x4

    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v1, 0x2c5

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->n:Lcom/google/googlenav/ui/view/a;

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    return-void

    :array_0
    .array-data 4
        0x7f100001
        0x7f100002
        0x7f100003
        0x7f100004
    .end array-data

    :array_1
    .array-data 4
        0x7f100005
        0x7f100006
        0x7f100007
        0x7f100008
    .end array-data
.end method

.method public constructor <init>(Lbf/bk;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x7f0f001a

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/dialog/bv;-><init>(Lbf/m;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->l:Lcom/google/googlenav/ui/bi;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->v:Landroid/widget/TextView;

    return-void
.end method

.method protected static a(Ljava/lang/String;Lam/g;)Lan/f;
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :goto_0
    invoke-interface {p1, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    return-object v0

    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ai;ILcom/google/googlenav/aZ;)Lbj/H;
    .locals 6

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    add-int/lit8 v0, p2, -0x1

    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/view/dialog/bT;->a(ILcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x34

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/g;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;

    move-result-object v0

    return-object v0
.end method

.method private a(ILcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .locals 1

    invoke-virtual {p2, p1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bi;->bf:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x17

    if-le v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x14

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/View;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const v1, 0x7f0400ae

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v1, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f1001a4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/bV;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/bV;-><init>(Lcom/google/googlenav/ui/view/dialog/bT;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    const v2, 0x7f10024f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    invoke-static {p3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const v1, 0x7f1001a6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/bW;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/bW;-><init>(Lcom/google/googlenav/ui/view/dialog/bT;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p2, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;I)V
    .locals 2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V
    .locals 9

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZLcom/google/googlenav/be;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZLcom/google/googlenav/be;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aq:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bX;

    invoke-direct {v1, p0, p4, p5, p7}, Lcom/google/googlenav/ui/view/dialog/bX;-><init>(Lcom/google/googlenav/ui/view/dialog/bT;IILcom/google/googlenav/be;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setSelected(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p8, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->v:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->R()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x201

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v1, 0x47c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aj()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aj()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ak()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->v:Landroid/widget/TextView;

    sget-object v2, Lcom/google/googlenav/ui/aV;->aO:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    :cond_2
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V
    .locals 12

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v10

    const/4 v0, 0x0

    const/4 v7, 0x0

    if-eqz v10, :cond_5

    array-length v2, v10

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v10, v1

    const-string v4, " loc:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    aget v2, v1, v0

    const/16 v0, 0x508

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2d0

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V

    const/4 v0, 0x1

    :cond_0
    const/4 v5, 0x0

    array-length v11, v10

    const/4 v1, 0x0

    move v9, v1

    move v8, v0

    :goto_1
    if-ge v9, v11, :cond_8

    aget-object v0, v10, v9

    const/4 v1, 0x3

    if-lt v8, v1, :cond_1

    array-length v1, v10

    add-int/lit8 v1, v1, -0x1

    if-ne v5, v1, :cond_4

    :cond_1
    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    move v0, v7

    move v1, v8

    :goto_2
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v7, v0

    move v8, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    aget v2, v0, v8

    const/16 v4, 0x2c6

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V

    add-int/lit8 v0, v8, 0x1

    add-int/lit8 v5, v5, 0x1

    move v1, v0

    move v0, v7

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    add-int/lit8 v9, v8, 0x1

    aget v2, v0, v8

    const/16 v0, 0x509

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2c7

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V

    move v0, v9

    :cond_5
    :goto_3
    if-eqz p3, :cond_6

    const/4 v1, 0x3

    if-ge v0, v1, :cond_6

    :goto_4
    const/4 v1, 0x3

    if-ge v0, v1, :cond_6

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/ui/android/FlowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz p3, :cond_7

    if-nez v7, :cond_7

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->t:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    return-void

    :cond_8
    move v0, v8

    goto :goto_3
.end method

.method private b(Lcom/google/googlenav/ai;ILcom/google/googlenav/aZ;)Lbj/H;
    .locals 8

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v4

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aQ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bg()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x206

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    move-object v4, v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v5

    new-instance v0, Lcom/google/googlenav/ui/view/android/aM;

    iget-object v7, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/view/android/aM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lan/f;LaN/B;ILbf/bk;)V

    move-object v4, v0

    goto :goto_0

    :cond_2
    move-object v3, v4

    goto :goto_2

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method private b(Landroid/view/LayoutInflater;Landroid/widget/ListView;Lcom/google/googlenav/aZ;)V
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f0400ab

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v2, p3}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f100250

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->v:Landroid/widget/TextView;

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->v:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V
    .locals 18

    const/4 v8, 0x0

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aH()[Lcom/google/googlenav/bc;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v12, v1

    :goto_0
    if-eqz v10, :cond_2

    array-length v11, v10

    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v11, :cond_2

    aget-object v15, v10, v9

    invoke-virtual {v15}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bc;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->d()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->e()Ljava/lang/String;

    move-result-object v4

    :goto_2
    array-length v2, v10

    if-ge v8, v2, :cond_a

    const/4 v2, 0x4

    if-ge v8, v2, :cond_a

    if-eqz v1, :cond_1

    const/4 v7, 0x1

    :goto_3
    sget-object v1, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    aget v3, v1, v8

    invoke-virtual {v15}, Lcom/google/googlenav/bc;->f()I

    move-result v5

    const/4 v6, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V

    add-int/lit8 v1, v8, 0x1

    invoke-interface {v13, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v8, v1

    goto :goto_1

    :cond_0
    invoke-virtual {v15}, Lcom/google/googlenav/bc;->b()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    goto :goto_3

    :cond_2
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aI()[Lcom/google/googlenav/be;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/aZ;->aI()[Lcom/google/googlenav/be;

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v1, 0x0

    move v11, v1

    move v1, v8

    :goto_5
    move/from16 v0, v17

    if-ge v11, v0, :cond_5

    aget-object v8, v16, v11

    const/4 v2, 0x4

    if-ge v1, v2, :cond_4

    invoke-virtual {v8}, Lcom/google/googlenav/be;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_4
    :goto_6
    :pswitch_0
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_5

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    add-int/lit8 v10, v1, 0x1

    aget v3, v2, v1

    invoke-virtual {v8}, Lcom/google/googlenav/be;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2d6

    const/4 v6, -0x1

    invoke-interface {v15, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZLcom/google/googlenav/be;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v14, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v10

    goto :goto_6

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    add-int/lit8 v10, v1, 0x1

    aget v3, v2, v1

    invoke-virtual {v8}, Lcom/google/googlenav/be;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2d6

    const/4 v6, -0x1

    invoke-interface {v15, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZLcom/google/googlenav/be;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v14, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v10

    goto :goto_6

    :cond_5
    move v2, v1

    :goto_7
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/google/googlenav/bc;

    if-eqz v8, :cond_8

    const/4 v1, 0x4

    if-ge v2, v1, :cond_8

    invoke-virtual {v8}, Lcom/google/googlenav/bc;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    add-int/lit8 v9, v2, 0x1

    aget v3, v1, v2

    invoke-virtual {v8}, Lcom/google/googlenav/bc;->f()I

    move-result v5

    const/4 v6, -0x1

    const/4 v7, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;ILjava/lang/CharSequence;IIZ)V

    invoke-interface {v13, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v9

    :goto_8
    if-eqz p3, :cond_6

    const/4 v2, 0x4

    if-ge v1, v2, :cond_6

    :goto_9
    const/4 v2, 0x4

    if-ge v1, v2, :cond_6

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/bT;->s:[I

    aget v2, v2, v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/FlowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_6
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "df"

    invoke-static {v3, v13}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "do"

    invoke-static {v3, v14}, Lcom/google/googlenav/aZ;->b(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_7

    const-string v1, "=1"

    :goto_a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/16 v1, 0x6f

    const-string v3, "fi"

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_7
    const-string v1, "=0"

    goto :goto_a

    :cond_8
    move v1, v2

    goto :goto_8

    :cond_9
    move v2, v8

    goto/16 :goto_7

    :cond_a
    move v1, v8

    goto/16 :goto_4

    :cond_b
    move-object v12, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bU()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bT;->q:Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->r:LaW/e;

    invoke-virtual {v1, p1, v2, v2}, LaW/e;->a(Ljava/lang/String;ZZ)V

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->q:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;I)V

    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private n()V
    .locals 5

    const/4 v4, 0x0

    const/16 v2, 0x8

    const/16 v3, 0xf

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bP()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bT()Lcom/google/googlenav/ui/view/u;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/bT;->n:Lcom/google/googlenav/ui/view/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setViewportListener(Lcom/google/googlenav/ui/view/u;Lcom/google/googlenav/ui/view/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->requestLayout()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/LoadingFooterView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v4, v4}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setViewportListener(Lcom/google/googlenav/ui/view/u;Lcom/google/googlenav/ui/view/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/google/googlenav/ui/android/LoadingFooterView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/ViewGroup;I)V

    goto :goto_0
.end method


# virtual methods
.method public Q_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->a()V

    :cond_0
    return-void
.end method

.method protected a(Lcom/google/googlenav/ai;I)Lan/f;
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p2, -0x1

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bb()Lbh/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    invoke-virtual {v0, v1, v2, p2}, Lbh/a;->a(ZBI)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->l:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->i()Lam/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Ljava/lang/String;Lam/g;)Lan/f;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/ai;IZ)Lbj/H;
    .locals 1

    const/16 v0, 0x2bc

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Lcom/google/googlenav/ai;IZI)Lbj/H;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ai;IZLcom/google/googlenav/aZ;)Lbj/H;
    .locals 1

    invoke-virtual {p4}, Lcom/google/googlenav/aZ;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;IZ)Lbj/H;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/aZ;)Lbj/H;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/aZ;)Lbj/H;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/bv;->a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0400c8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;I)Lan/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lan/f;)Lcom/google/googlenav/ui/view/android/bt;

    :cond_0
    return-object v0
.end method

.method protected a(Lcom/google/googlenav/F;)Ljava/util/List;
    .locals 2

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    invoke-virtual {p0, p1, v1, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/F;III)Ljava/util/List;
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v5

    :goto_0
    if-ge p2, p3, :cond_3

    invoke-interface {p1, p2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v5, v1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v1

    check-cast v1, LaR/D;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, LaR/D;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v6, p2, p4

    iget-object v7, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v7}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v7

    invoke-virtual {p0, v0, v6, v1, v7}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;IZLcom/google/googlenav/aZ;)Lbj/H;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v6, Laz/a;->a:Laz/a;

    invoke-virtual {v6}, Laz/a;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    if-ne p2, v2, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-object v4

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    if-eqz v0, :cond_0

    const-string v0, "19"

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020319

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/widget/ListView;)V
    .locals 3

    const v0, 0x7f0400a4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/LoadingFooterView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    const v1, 0x7f100232

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/LoadingFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x25d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->u:Lcom/google/googlenav/ui/android/LoadingFooterView;

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    return-void
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/widget/ListView;Lcom/google/googlenav/aZ;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f0400d5

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f100294

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/android/FlowLayout;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-direct {p0, v1, p3, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-direct {p0, v1, p3, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-virtual {v4, v1}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/android/FlowLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    new-instance v4, Lcom/google/googlenav/ui/android/aa;

    const/4 v5, 0x2

    sget-object v6, Lcom/google/googlenav/ui/android/Z;->b:Lcom/google/googlenav/ui/android/Z;

    invoke-direct {v4, v5, v3, v6}, Lcom/google/googlenav/ui/android/aa;-><init>(IILcom/google/googlenav/ui/android/Z;)V

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/android/FlowLayout;->setParams(Lcom/google/googlenav/ui/android/aa;)V

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {p2, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method protected a(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Landroid/view/LayoutInflater;Landroid/widget/ListView;Lcom/google/googlenav/aZ;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->w_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/LayoutInflater;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/aZ;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/H;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->as()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f1004b4

    if-ne v2, v3, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b(Lcom/google/googlenav/ai;IZI)Lbj/H;
    .locals 1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bt;->a()Lcom/google/googlenav/ui/view/android/bs;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/LayoutInflater;Landroid/widget/ListView;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f0400a2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    invoke-virtual {p0, v1, v3, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Landroid/view/LayoutInflater;Landroid/widget/ListView;Lcom/google/googlenav/aZ;)V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Landroid/view/View;)V

    const v0, 0x7f10002e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-object v2
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/view/dialog/bv;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bT;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_1
    return-void
.end method

.method protected i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public l()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->ae()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    :cond_0
    return-void
.end method

.method public m()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->b:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->b(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/bT;->o:Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ui/android/FlowLayout;Lcom/google/googlenav/aZ;Z)V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bT;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110020

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x2af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bU;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/bU;-><init>(Lcom/google/googlenav/ui/view/dialog/bT;)V

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bT;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
