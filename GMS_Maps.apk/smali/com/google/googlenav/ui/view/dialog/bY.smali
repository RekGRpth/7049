.class public Lcom/google/googlenav/ui/view/dialog/bY;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/iL;

.field private b:Landroid/widget/EditText;

.field private final c:Lcom/google/googlenav/ui/wizard/iT;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/iL;Lcom/google/googlenav/ui/wizard/iT;Z)V
    .locals 1

    const v0, 0x7f0f001b

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bY;->a:Lcom/google/googlenav/ui/wizard/iL;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/bY;->c:Lcom/google/googlenav/ui/wizard/iT;

    iput-boolean p3, p0, Lcom/google/googlenav/ui/view/dialog/bY;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bY;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/bY;)Lcom/google/googlenav/ui/wizard/iL;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->a:Lcom/google/googlenav/ui/wizard/iL;

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bY;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->c:Lcom/google/googlenav/ui/wizard/iT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iT;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bY;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bY;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 6

    const v1, 0x7f100031

    const v2, 0x7f100030

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bY;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f040168

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100025

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bY;->c:Lcom/google/googlenav/ui/wizard/iT;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/iT;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    const v0, 0x7f1003e1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->b:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->b:Landroid/widget/EditText;

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/bZ;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/dialog/bZ;-><init>(Lcom/google/googlenav/ui/view/dialog/bY;)V

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f10020b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v4, 0x79

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/dialog/bY;->d:Z

    if-eqz v0, :cond_1

    const v0, 0x7f1003e2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v4, 0x7f

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x7a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ca;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ca;-><init>(Lcom/google/googlenav/ui/view/dialog/bY;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cb;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/cb;-><init>(Lcom/google/googlenav/ui/view/dialog/bY;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f10002e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/friend/ad;->r()V

    return-object v3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method
