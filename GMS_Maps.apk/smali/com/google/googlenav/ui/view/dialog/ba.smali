.class public Lcom/google/googlenav/ui/view/dialog/ba;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/google/googlenav/bZ;

.field private final c:Landroid/widget/ListView;

.field private d:F


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bZ;Lbf/i;Lcom/google/googlenav/ui/view/android/bn;Landroid/view/ViewGroup;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    const v0, 0x7f1003c3

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->a:Landroid/view/View;

    const v0, 0x7f100026

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->c:Landroid/widget/ListView;

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bb;

    invoke-direct {v0, p0, p3, p4, p2}, Lcom/google/googlenav/ui/view/dialog/bb;-><init>(Lcom/google/googlenav/ui/view/dialog/ba;Lcom/google/googlenav/ui/view/android/bn;Landroid/view/ViewGroup;Lbf/i;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/ci;)V

    const/16 v0, 0x54

    const-string v1, "ts"

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(J)Landroid/graphics/Bitmap;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/googlenav/ui/bh;->b(J)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ba;)Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/ba;)Lcom/google/googlenav/android/c;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/ba;->d()Lcom/google/googlenav/android/c;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/util/List;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lbj/ab;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/be;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/be;-><init>(Lcom/google/googlenav/ui/view/dialog/ba;)V

    const/16 v3, 0x20

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbj/ab;-><init>(Lcom/google/googlenav/bZ;Lbj/af;II)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private d()Lcom/google/googlenav/android/c;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    return-object v0
.end method

.method private d(Ljava/util/List;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->k()I

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Lbj/v;

    const/16 v1, 0x23

    invoke-direct {v0, v1}, Lbj/v;-><init>(I)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v4

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v5, Lbj/ag;

    const/16 v6, 0x1f

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v5, v4, v0, v6, v7}, Lbj/ag;-><init>(Lcom/google/googlenav/cm;ZII)V

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private e(Ljava/util/List;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->n()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Lbj/V;

    const/16 v2, 0x2fc

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lbj/V;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bZ;->c(I)Lcom/google/googlenav/bZ;

    move-result-object v2

    new-instance v3, Lbj/Y;

    const/16 v4, 0x21

    const/16 v5, 0xfa5

    invoke-direct {v3, v2, v0, v4, v5}, Lbj/Y;-><init>(Lcom/google/googlenav/bZ;III)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static f(Ljava/util/List;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lbj/ag;

    if-eqz v0, :cond_1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/ag;

    invoke-virtual {v0}, Lbj/ag;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->c(Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->d(Ljava/util/List;)V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->b(Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->e(Ljava/util/List;)V

    return-object p1
.end method

.method public a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/z;->e:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bd;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bd;-><init>(Lcom/google/googlenav/ui/view/dialog/ba;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->g()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->b:Lcom/google/googlenav/bZ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/ci;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->d:F

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 2

    invoke-static {p1}, Lcom/google/googlenav/ui/view/dialog/ba;->f(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->a:Landroid/view/View;

    const v1, 0x7f1003c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x4c0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ba;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->d:F

    const/high16 v1, 0x3f800000

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ba;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
