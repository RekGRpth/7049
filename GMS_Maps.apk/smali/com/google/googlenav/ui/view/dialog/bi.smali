.class Lcom/google/googlenav/ui/view/dialog/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/suggest/android/SuggestView;

.field final synthetic b:Lcom/google/googlenav/ui/view/dialog/bf;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/view/dialog/bf;Lcom/google/googlenav/suggest/android/SuggestView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bi;->b:Lcom/google/googlenav/ui/view/dialog/bf;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/bi;->a:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bi;->a:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/bf;->l()Lcom/google/googlenav/android/BaseMapsActivity;

    move-result-object v0

    const/16 v1, 0x356

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bi;->b:Lcom/google/googlenav/ui/view/dialog/bf;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bf;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bi;->b:Lcom/google/googlenav/ui/view/dialog/bf;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bi;->a:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bf;->a(Ljava/lang/String;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/bf;->m()Lcom/google/googlenav/android/BaseMapsActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_0
.end method
