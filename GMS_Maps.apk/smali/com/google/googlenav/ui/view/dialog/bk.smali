.class public Lcom/google/googlenav/ui/view/dialog/bk;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ui/view/dialog/bl;

.field private final c:Lcom/google/googlenav/ui/view/dialog/bn;

.field private final d:Lcom/google/googlenav/ui/ak;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/view/dialog/bn;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    iput p1, p0, Lcom/google/googlenav/ui/view/dialog/bk;->a:I

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/bk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/bk;->d:Lcom/google/googlenav/ui/ak;

    return-void
.end method

.method static a(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/content/Intent;
    .locals 3

    const-string v0, "\n"

    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.apps.offers.VIEW_OFFER_DETAILS"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v2, "offer_title"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    const-string v2, "offer_id"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    const/16 v0, 0xc

    const-string v2, "offer_namespace"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    const/16 v0, 0xf

    const-string v2, "offer_type"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    const/16 v0, 0xa

    const-string v2, "offer_image_url"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    const/16 v0, 0xb

    const-string v2, "offer_description"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    const-string v0, "offer_cluster_doc_id"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "offer_merchant_name"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "offer_website"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "offer_address"

    const-string v2, "\n"

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ai;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "offer_phone_number"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    invoke-virtual {v2, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    const-string v2, "selected_redeem_location"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/CharSequence;ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;
    .locals 5

    const v4, 0x7f020351

    const/4 v3, 0x0

    const/4 v1, 0x0

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bk;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    new-instance v2, Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-direct {v2, p1, p2, v0, p3}, Lcom/google/googlenav/ui/view/dialog/bk;-><init>(ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/view/dialog/bn;)V

    new-array v0, v3, [I

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(ZILaA/f;[I)V

    const v0, 0x7f100025

    invoke-virtual {v2, p0, v0, v4}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/CharSequence;II)V

    return-object v2

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/view/dialog/bn;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100025

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bk;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->o()V

    return-void
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    const-class v2, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->d:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method private o()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->r()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    :cond_1
    return-void
.end method

.method private v()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/offers/k;->g()Lcom/google/android/apps/common/offerslib/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bm;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bm;-><init>(Lcom/google/googlenav/ui/view/dialog/bk;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/d;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/offers/k;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->b(Lcom/google/googlenav/ui/view/dialog/bk;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;I)V
    .locals 1

    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/bk;->b(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/android/M;

    new-instance v4, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;

    invoke-direct {v4, p0, v2, v1}, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;-><init>(Lcom/google/googlenav/ui/view/dialog/bk;Lcom/google/googlenav/android/R;Landroid/content/Intent;)V

    invoke-virtual {v2}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v2

    invoke-direct {v3, v4, v1, v2}, Lcom/google/googlenav/android/M;-><init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V

    invoke-virtual {v3}, Lcom/google/googlenav/android/M;->a()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected c()Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->h:Landroid/view/View;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->h()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->v()V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/view/View;)V

    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->m()Landroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/k;->a()Landroid/support/v4/app/t;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Landroid/support/v4/app/f;)Landroid/support/v4/app/t;

    invoke-virtual {v0}, Landroid/support/v4/app/t;->b()I

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    return-void
.end method

.method protected h()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->a:I

    return v0
.end method

.method public l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->m()Landroid/support/v4/app/k;

    move-result-object v0

    const v1, 0x7f1002f8

    invoke-virtual {v0, v1}, Landroid/support/v4/app/k;->a(I)Landroid/support/v4/app/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    return-object v0
.end method

.method public m()Landroid/support/v4/app/k;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/k;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 2

    const/16 v0, 0x58

    const-string v1, "m"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.offers.VIEW_MY_OFFERS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->o()V

    return-void
.end method
