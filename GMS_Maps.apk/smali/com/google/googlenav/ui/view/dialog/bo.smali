.class public Lcom/google/googlenav/ui/view/dialog/bo;
.super Lcom/google/googlenav/ui/view/dialog/bT;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/aJ;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bT;-><init>(Lbf/bk;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bo;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 4

    const v0, 0x7f100201

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/dialog/bo;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f1001b0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f1001b1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f020351

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bp;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/bp;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-virtual {v1, v2, v3}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x31d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v1, 0x7f10020a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bq;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bq;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private d(Landroid/view/View;)V
    .locals 4

    const v0, 0x7f100252

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x324

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f100023

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x329

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bs;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bs;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;IZ)Lbj/H;
    .locals 1

    const/16 v0, 0x2bc

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/googlenav/ui/view/dialog/bo;->b(Lcom/google/googlenav/ai;IZI)Lbj/H;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-gtz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const v1, 0x7f0400c5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->bJ:Lcom/google/googlenav/ui/aV;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/ai;->h(I)Lcom/google/googlenav/ap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/F;)Ljava/util/List;
    .locals 8

    const v7, 0x7f0400ca

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aD()I

    move-result v2

    if-lez v2, :cond_0

    if-ne v2, v6, :cond_2

    const/16 v0, 0x31e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v3, Lbj/bv;

    invoke-direct {v3, v6, v7, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1, v5, v2, v5}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-le v0, v2, :cond_1

    if-lez v2, :cond_3

    const/16 v0, 0x31f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v3, Lbj/bv;

    invoke-direct {v3, v6, v7, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    invoke-virtual {p0, p1, v2, v0, v5}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v1

    :cond_2
    const/16 v0, 0x320

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/16 v0, 0x321

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected a(Lcom/google/googlenav/F;III)Ljava/util/List;
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ne p3, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/common/offerslib/y;->a:Lcom/google/android/apps/common/offerslib/y;

    invoke-virtual {v2}, Lcom/google/android/apps/common/offerslib/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/offers/i;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    const v0, 0x7f020351

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/br;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/br;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004cc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->b:Lbf/m;

    const/16 v1, 0x76f

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f04010b

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bo;->c(Landroid/view/View;)V

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bo;->d(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Landroid/view/LayoutInflater;Landroid/widget/ListView;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bo;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    return-object v2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bo;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x2af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1004cc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x2db

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
