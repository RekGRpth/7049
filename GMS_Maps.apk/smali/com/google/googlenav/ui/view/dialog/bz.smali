.class public Lcom/google/googlenav/ui/view/dialog/bz;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/ia;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:Lbf/m;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/ia;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bz;->a:Lcom/google/googlenav/ui/wizard/ia;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/bz;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/bz;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/bz;->d:Lbf/m;

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/aY;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040126

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10032c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x3aa

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p1, Lcom/google/googlenav/aY;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10032d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v2, LaW/n;

    invoke-direct {v2}, LaW/n;-><init>()V

    const v3, 0x7f020325

    invoke-virtual {v2, v3}, LaW/n;->a(I)LaW/n;

    move-result-object v2

    const/16 v3, 0x3ab

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LaW/n;->a(Ljava/lang/String;)LaW/n;

    move-result-object v2

    invoke-virtual {v2}, LaW/n;->a()LaW/l;

    move-result-object v2

    invoke-virtual {v2}, LaW/l;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bB;

    invoke-direct {v3, p0, p1}, Lcom/google/googlenav/ui/view/dialog/bB;-><init>(Lcom/google/googlenav/ui/view/dialog/bz;Lcom/google/googlenav/aY;)V

    invoke-virtual {v2, v3}, LaW/l;->a(Landroid/view/View$OnClickListener;)V

    new-instance v2, LaW/n;

    invoke-direct {v2}, LaW/n;-><init>()V

    const v3, 0x7f020324

    invoke-virtual {v2, v3}, LaW/n;->a(I)LaW/n;

    move-result-object v2

    const/16 v3, 0x3a9

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LaW/n;->a(Ljava/lang/String;)LaW/n;

    move-result-object v2

    invoke-virtual {v2, v5}, LaW/n;->a(Z)LaW/n;

    move-result-object v2

    invoke-virtual {v2}, LaW/n;->a()LaW/l;

    move-result-object v2

    invoke-virtual {v2}, LaW/l;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bC;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/dialog/bC;-><init>(Lcom/google/googlenav/ui/view/dialog/bz;Lcom/google/googlenav/aY;)V

    invoke-virtual {v2, v0}, LaW/l;->a(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bz;)Lcom/google/googlenav/ui/wizard/ia;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bz;->a:Lcom/google/googlenav/ui/wizard/ia;

    return-object v0
.end method

.method private h()Lbj/H;
    .locals 9

    const/4 v7, 0x0

    const/4 v2, 0x0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bz;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v1, v4, v3}, Lbf/aR;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bz;->d:Lbf/m;

    if-nez v0, :cond_1

    move-object v5, v2

    :goto_0
    new-instance v0, Lbj/aE;

    move-object v6, v2

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZ)V

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-static {v3}, Lcom/google/common/collect/aT;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bA;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/view/dialog/bA;-><init>(Lcom/google/googlenav/ui/view/dialog/bz;Lbj/aE;)V

    invoke-virtual {v1, v2, v3}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bz;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    return-void
.end method

.method protected a(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->as()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bz;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040057

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v0, 0x482

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/bz;->b:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bz;->a(Landroid/view/View;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bz;->h()Lbj/H;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    new-instance v3, Lcom/google/googlenav/aY;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/bz;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v3, v4}, Lcom/google/googlenav/aY;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3}, Lcom/google/googlenav/aY;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/dialog/bz;->a(Lcom/google/googlenav/aY;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    :cond_0
    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bz;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/dialog/bz;->d:Lbf/m;

    invoke-direct {v3, v4, v5, v2, v7}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    return-object v1
.end method
