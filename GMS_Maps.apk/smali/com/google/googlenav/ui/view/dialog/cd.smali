.class public Lcom/google/googlenav/ui/view/dialog/cd;
.super Lcom/google/googlenav/ui/view/dialog/cc;
.source "SourceFile"


# static fields
.field static final synthetic b:Z


# instance fields
.field private final c:Lcom/google/googlenav/bu;

.field private final d:Lcom/google/googlenav/br;

.field private l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private m:Lcom/google/googlenav/ui/view/android/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/view/dialog/cd;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/view/dialog/cd;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/bu;Lcom/google/googlenav/br;Lcom/google/googlenav/ui/wizard/jb;)V
    .locals 0

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/dialog/cc;-><init>(Lcom/google/googlenav/ui/wizard/jb;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/cd;->d:Lcom/google/googlenav/br;

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 10

    const/4 v3, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x2

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {v1, v9}, Lcom/google/googlenav/common/io/protocol/b;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v0, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    invoke-static {v8}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v2, 0x3

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v9}, Lcom/google/googlenav/common/io/protocol/b;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v2, v2, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v0, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    invoke-static {v0, v9}, Lcom/google/googlenav/common/io/protocol/b;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v0, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v0, 0x54e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-virtual {v2, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/cd;)Lcom/google/googlenav/ui/view/android/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->m:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/br;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/br;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 6

    const/16 v4, 0x10

    const/4 v3, 0x0

    const v0, 0x7f1003fa

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x54d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v0, v0, Lcom/google/googlenav/bu;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {v1, v4, v4, v3, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cd;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/cd;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/dialog/cd;->a:Lcom/google/googlenav/ui/wizard/jb;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/wizard/jb;->g()Lcom/google/googlenav/ui/br;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/view/dialog/cd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/br;)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->m:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f1003fb

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f1003fc

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/cd;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cd;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/cd;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/br;)Ljava/util/List;
    .locals 9

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {p0, v4, v3}, Lbf/aR;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;)V

    new-instance v0, Lbj/aE;

    move-object v1, p0

    move-object v5, p1

    move-object v6, v2

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZ)V

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v0, v0, Lcom/google/googlenav/bu;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/bs;

    sget v2, Lcom/google/googlenav/ui/bi;->bE:I

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cd;->a:Lcom/google/googlenav/ui/wizard/jb;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/cg;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/cg;-><init>(Lcom/google/googlenav/ui/view/dialog/cd;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/jb;->a(Ljava/util/List;LaB/p;)V

    :cond_0
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->d:Lcom/google/googlenav/br;

    sget-object v1, Lcom/google/googlenav/br;->b:Lcom/google/googlenav/br;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x550

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->d:Lcom/google/googlenav/br;

    sget-object v1, Lcom/google/googlenav/br;->c:Lcom/google/googlenav/br;

    if-ne v0, v1, :cond_1

    const/16 v0, 0x551

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->d:Lcom/google/googlenav/br;

    sget-object v1, Lcom/google/googlenav/br;->a:Lcom/google/googlenav/br;

    if-eq v0, v1, :cond_2

    :cond_2
    const/16 v0, 0x552

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 9

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget v0, v0, Lcom/google/googlenav/bu;->f:I

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget v1, v1, Lcom/google/googlenav/bu;->g:I

    sget-boolean v2, Lcom/google/googlenav/ui/view/dialog/cd;->b:Z

    if-nez v2, :cond_0

    add-int/2addr v0, v1

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cd;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040176

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v0, 0x7f1003f3

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1003f4

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f1003f5

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f1003f6

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f1003f7

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/cd;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/googlenav/ui/view/dialog/cd;->a(Landroid/view/View;Ljava/lang/String;)V

    const/16 v6, 0x553

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/googlenav/ui/aV;->bV:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->bW:Lcom/google/googlenav/ui/aV;

    invoke-static {v6, v7, v8}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v0, 0x7f1003f8

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v6, v6, Lcom/google/googlenav/bu;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v6, v6, Lcom/google/googlenav/bu;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/cd;->h()V

    const/16 v6, 0x54f

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/google/googlenav/ui/view/dialog/ce;

    invoke-direct {v6, p0}, Lcom/google/googlenav/ui/view/dialog/ce;-><init>(Lcom/google/googlenav/ui/view/dialog/cd;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v0, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x557

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    sget-object v6, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    sget-object v7, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v6, v7}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const/16 v0, 0x544

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const/16 v0, 0x545

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const/16 v0, 0x546

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    const v0, 0x7f1003fd

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    invoke-virtual {v1}, Lcom/google/googlenav/bu;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x54b

    :goto_2
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cf;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/cf;-><init>(Lcom/google/googlenav/ui/view/dialog/cd;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v5

    :cond_1
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x556

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/cd;->c:Lcom/google/googlenav/bu;

    iget-object v8, v8, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v0, v6}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/16 v1, 0x54c

    goto :goto_2
.end method
