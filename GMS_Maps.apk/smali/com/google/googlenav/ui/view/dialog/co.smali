.class public Lcom/google/googlenav/ui/view/dialog/co;
.super Lcom/google/googlenav/ui/view/dialog/cc;
.source "SourceFile"


# static fields
.field private static final m:Lcom/google/googlenav/ui/aV;


# instance fields
.field b:I

.field c:I

.field d:I

.field l:I

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/util/List;

.field private final r:Lcom/google/googlenav/ui/br;

.field private final s:Lam/f;

.field private t:Landroid/widget/ListView;

.field private u:Lcom/google/googlenav/ui/view/dialog/cs;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/aV;->ag:Lcom/google/googlenav/ui/aV;

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/co;->m:Lcom/google/googlenav/ui/aV;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/google/googlenav/ui/br;Lam/f;Lcom/google/googlenav/ui/wizard/jb;)V
    .locals 1

    invoke-direct {p0, p5}, Lcom/google/googlenav/ui/view/dialog/cc;-><init>(Lcom/google/googlenav/ui/wizard/jb;)V

    const/16 v0, 0x572

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->n:Ljava/lang/String;

    const/16 v0, 0x573

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->o:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/co;->p:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/co;->q:Ljava/util/List;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/co;->r:Lcom/google/googlenav/ui/br;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/co;->s:Lam/f;

    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/co;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040177

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1003fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x574

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/co;->a(Landroid/view/View;Ljava/lang/String;)V

    const/16 v2, 0x571

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->bV:Lcom/google/googlenav/ui/aV;

    sget-object v4, Lcom/google/googlenav/ui/aV;->bX:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3, v4}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v0, 0x7f1003ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->v:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->v:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/co;->n:Ljava/lang/String;

    sget-object v3, Lcom/google/googlenav/ui/view/dialog/co;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/co;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(ILandroid/widget/ListView;)V
    .locals 4

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p2, p1}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/dialog/cs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->m()V

    :goto_2
    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/dialog/cs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    :goto_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->l()V

    goto :goto_2

    :cond_3
    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/co;ILandroid/widget/ListView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/co;->a(ILandroid/widget/ListView;)V

    return-void
.end method

.method private b(Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 5

    const/16 v4, 0xa

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/co;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04017b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->n:Ljava/lang/String;

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/co;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/co;)Lcom/google/googlenav/ui/view/dialog/cs;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 7

    const/16 v6, 0x8

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    iput v2, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/cs;->getCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/cs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    add-int/2addr v0, v4

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    const v0, 0x7f10040c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->p:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x56f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    sget-object v2, Lcom/google/googlenav/ui/aV;->bT:Lcom/google/googlenav/ui/aV;

    sget-object v3, Lcom/google/googlenav/ui/aV;->bU:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v0, 0x7f1003f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->x:Landroid/widget/TextView;

    const v0, 0x7f1003f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->y:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    return-void

    :cond_2
    const/16 v1, 0x56e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->p:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->m()V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->l()V

    goto :goto_4
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/co;->q:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/cs;-><init>(Lcom/google/googlenav/ui/view/dialog/co;Landroid/content/Context;Ljava/util/List;Lcom/google/googlenav/ui/view/dialog/cp;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    const v0, 0x7f10040b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/cq;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/cq;-><init>(Lcom/google/googlenav/ui/view/dialog/co;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/co;->a(Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/co;->b(Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cr;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/cr;-><init>(Lcom/google/googlenav/ui/view/dialog/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/co;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->h()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/dialog/co;)Lcom/google/googlenav/ui/br;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->r:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/dialog/co;)Lam/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->s:Lam/f;

    return-object v0
.end method

.method private h()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->u:Lcom/google/googlenav/ui/view/dialog/cs;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/cs;->getCount()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->v:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/co;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v0, 0x1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/co;->t:Landroid/widget/ListView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->d:I

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->l()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->m()V

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->o:Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->v:Landroid/widget/TextView;

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/co;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->w:Landroid/widget/TextView;

    sget-object v2, Lcom/google/googlenav/ui/view/dialog/co;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    return-void

    :cond_1
    iput v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    iput v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->n:Ljava/lang/String;

    goto :goto_2
.end method

.method private l()V
    .locals 5

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    if-ne v0, v2, :cond_0

    const/16 v0, 0x547

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->y:Landroid/widget/TextView;

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/16 v0, 0x548

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private m()V
    .locals 5

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    if-ne v0, v2, :cond_0

    const/16 v0, 0x549

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/co;->x:Landroid/widget/TextView;

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/16 v0, 0x54a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/co;->b(Landroid/view/View;)V

    const v0, 0x7f10040d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x570

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cp;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/cp;-><init>(Lcom/google/googlenav/ui/view/dialog/co;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/co;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040180

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/co;->c(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/co;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/co;->h()V

    return-object v0
.end method
