.class public Lcom/google/googlenav/ui/view/dialog/cu;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/android/aS;


# instance fields
.field private final a:Lbf/bH;

.field private b:Ljava/util/List;

.field private c:Landroid/widget/Spinner;

.field private d:Lcom/google/googlenav/ui/view/android/TransitLineView;

.field private l:Lcom/google/googlenav/ui/view/android/TransitLineView;


# direct methods
.method public constructor <init>(Lbf/bH;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/cu;->p()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/cu;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->b:Ljava/util/List;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    const v0, 0x7f100254

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->c:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v0}, Lbf/bH;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->j()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->b:Ljava/util/List;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cu;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090008

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/cu;->b:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/dialog/cu;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cu;->c:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->c:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/cw;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/view/dialog/cw;-><init>(Lcom/google/googlenav/ui/view/dialog/cu;Lcom/google/googlenav/ui/view/dialog/cv;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->c:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/cu;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cu;->b(I)V

    return-void
.end method

.method private a(Ljava/util/List;)[Ljava/lang/String;
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bR;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lcom/google/googlenav/bR;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private b(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v0}, Lbf/bH;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0, p1, v4}, Lcom/google/googlenav/bN;->a(II)V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v1}, Lbf/bH;->b()V

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->l()[Lcom/google/googlenav/ui/view/android/aT;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ui/view/android/aT;

    aget-object v3, v1, v4

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/cu;->l:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->k()Lcom/google/googlenav/bS;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/googlenav/bS;->a([Lcom/google/googlenav/ui/view/android/aT;)[[Lcom/google/googlenav/bO;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a([Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/cu;->d:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->k()Lcom/google/googlenav/bS;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bS;->a([Lcom/google/googlenav/ui/view/android/aT;)[[Lcom/google/googlenav/bO;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a([Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 10

    const v9, 0x7f10042d

    const/4 v5, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v0}, Lbf/bH;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->l()[Lcom/google/googlenav/ui/view/android/aT;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->k()Lcom/google/googlenav/bS;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->m()I

    move-result v3

    const v0, 0x7f100431

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/TransitLineView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->l:Lcom/google/googlenav/ui/view/android/TransitLineView;

    new-array v2, v5, [Lcom/google/googlenav/ui/view/android/aT;

    aget-object v0, v6, v8

    aput-object v0, v2, v8

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->l:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v7, v2}, Lcom/google/googlenav/bS;->a([Lcom/google/googlenav/ui/view/android/aT;)[[Lcom/google/googlenav/bO;

    move-result-object v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a(Lcom/google/googlenav/ui/view/android/aS;[Lcom/google/googlenav/ui/view/android/aT;I[[Lcom/google/googlenav/bO;Z)V

    const v0, 0x7f100430

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/TransitLineView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->d:Lcom/google/googlenav/ui/view/android/TransitLineView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->d:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v7, v6}, Lcom/google/googlenav/bS;->a([Lcom/google/googlenav/ui/view/android/aT;)[[Lcom/google/googlenav/bO;

    move-result-object v4

    move-object v1, p0

    move-object v2, v6

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a(Lcom/google/googlenav/ui/view/android/aS;[Lcom/google/googlenav/ui/view/android/aT;I[[Lcom/google/googlenav/bO;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->d:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v0, v9}, Lcom/google/googlenav/ui/view/android/TransitLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cu;->l:Lcom/google/googlenav/ui/view/android/TransitLineView;

    invoke-virtual {v1, v9}, Lcom/google/googlenav/ui/view/android/TransitLineView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/cx;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/cx;-><init>(Landroid/widget/ScrollView;Landroid/widget/ScrollView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cu;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cu;->b(I)V

    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v0}, Lbf/bH;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->h()Lbf/bG;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    return-void
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1004b4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cu;->f:Lcom/google/googlenav/ui/e;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cu;->a:Lbf/bH;

    invoke-virtual {v0}, Lbf/bH;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/bN;->h()Lbf/bG;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bG;->a()Lcom/google/googlenav/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cu;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/cu;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cu;->a(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/cu;->b(Landroid/view/View;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f1004b4

    const/16 v1, 0x507

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020264

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method
