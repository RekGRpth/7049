.class Lcom/google/googlenav/ui/view/dialog/cx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/widget/ScrollView;

.field private b:Landroid/widget/ScrollView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/widget/ScrollView;Landroid/widget/ScrollView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/cx;->c:I

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cx;->a:Landroid/widget/ScrollView;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/cx;->b:Landroid/widget/ScrollView;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/cx;->c:I

    if-ne v1, v2, :cond_2

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/cx;->c:I

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cx;->b:Landroid/widget/ScrollView;

    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cx;->b:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/cx;->c:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-eq v0, v1, :cond_0

    iput v2, p0, Lcom/google/googlenav/ui/view/dialog/cx;->c:I

    goto :goto_0
.end method
