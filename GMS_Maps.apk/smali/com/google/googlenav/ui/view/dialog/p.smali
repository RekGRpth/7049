.class public Lcom/google/googlenav/ui/view/dialog/p;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:I

.field protected b:I

.field protected c:I

.field protected d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;II)V
    .locals 0

    invoke-direct {p0, p4}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    iput p3, p0, Lcom/google/googlenav/ui/view/dialog/p;->a:I

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/dialog/p;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method protected I_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->l()V

    return-void
.end method

.method protected O_()V
    .locals 0

    return-void
.end method

.method public P_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/p;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->h()V

    return-void
.end method

.method protected a(Landroid/view/WindowManager$LayoutParams;)V
    .locals 3

    const v0, 0x7f100129

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/p;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->b:I

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->c:I

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v1, 0x0

    aget v1, v0, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/p;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->b:I

    const/4 v1, 0x1

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->c:I

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->b:I

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->c:I

    goto :goto_0
.end method

.method public l()V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, -0x2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/p;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/dialog/p;->a(Landroid/view/WindowManager$LayoutParams;)V

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x20100

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->cancel()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/p;->cancel()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
