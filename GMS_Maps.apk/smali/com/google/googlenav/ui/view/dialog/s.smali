.class public Lcom/google/googlenav/ui/view/dialog/s;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:[LaG/g;

.field private final b:Lcom/google/googlenav/ui/view/dialog/E;

.field private final c:Lcom/google/googlenav/ui/view/dialog/D;

.field private final d:LaG/h;

.field private l:Landroid/view/View;

.field private m:Landroid/support/v4/view/ViewPager;

.field private n:[Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;


# direct methods
.method public constructor <init>(LaG/h;Lcom/google/googlenav/ui/view/dialog/D;)V
    .locals 3

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    const/4 v0, 0x3

    new-array v0, v0, [LaG/g;

    const/4 v1, 0x0

    sget-object v2, LaG/g;->a:LaG/g;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaG/g;->b:LaG/g;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaG/g;->c:LaG/g;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->a:[LaG/g;

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/s;->d:LaG/h;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/E;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/dialog/E;-><init>(Lcom/google/googlenav/ui/view/dialog/t;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->b:Lcom/google/googlenav/ui/view/dialog/E;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/s;I)LaG/d;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->e(I)LaG/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/s;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->o:Landroid/view/View;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/s;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->a:[LaG/g;

    aget-object v0, v0, p1

    new-instance v1, LaG/a;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/s;->d:LaG/h;

    invoke-direct {v1, v2}, LaG/a;-><init>(LaG/h;)V

    invoke-virtual {v1, v0}, LaG/a;->a(LaG/g;)LaG/a;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, LaG/a;->a(II)LaG/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aM;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LaG/a;->a(Ljava/util/List;)LaG/a;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/x;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/dialog/x;-><init>(Lcom/google/googlenav/ui/view/dialog/s;I)V

    invoke-virtual {v0, v1}, LaG/a;->a(LaG/c;)LaG/a;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    aget-object v0, v0, p1

    const v1, 0x7f100088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/s;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/s;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/s;Ljava/util/List;LaG/j;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/s;->a(Ljava/util/List;LaG/j;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/s;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->b(Z)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->d:LaG/h;

    invoke-virtual {v0}, LaG/h;->a()LaG/j;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/dialog/s;->a(Ljava/util/List;LaG/j;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/y;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/dialog/y;-><init>(Lcom/google/googlenav/ui/view/dialog/s;Ljava/util/List;)V

    invoke-virtual {v0, v1}, LaG/j;->a(LaB/p;)I

    return-void
.end method

.method private a(Ljava/util/List;LaG/j;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    invoke-virtual {v0}, LaG/f;->h()LaG/m;

    move-result-object v0

    invoke-virtual {v0}, LaG/m;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/s;->d:LaG/h;

    invoke-virtual {v2, v0}, LaG/h;->b(LaG/m;)Lam/f;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v2}, LaG/m;->a(Lam/f;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_0

    invoke-virtual {p2, v0}, LaG/j;->a(LaG/m;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Ljava/util/List;Landroid/widget/ListView;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/A;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/view/dialog/A;-><init>(LaG/f;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/s;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/s;->f:Lcom/google/googlenav/ui/e;

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/dialog/z;-><init>(Lcom/google/googlenav/ui/view/dialog/s;Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/s;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->p:Landroid/view/View;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->e(I)LaG/d;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1}, LaG/d;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/s;->a(Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->f(I)Landroid/widget/ListView;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/dialog/s;->a(Ljava/util/List;Landroid/widget/ListView;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/s;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->b(I)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->b:Lcom/google/googlenav/ui/view/dialog/E;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/view/dialog/E;->a:Z

    if-ne v0, p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->b:Lcom/google/googlenav/ui/view/dialog/E;

    iput-boolean p1, v0, Lcom/google/googlenav/ui/view/dialog/E;->a:Z

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    aget-object v0, v0, v1

    const v2, 0x7f100089

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/ModalOverlay;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/s;->b:Lcom/google/googlenav/ui/view/dialog/E;

    iget-boolean v2, v2, Lcom/google/googlenav/ui/view/dialog/E;->a:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    const/16 v3, 0x258

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setHidden()V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/s;)Lcom/google/googlenav/ui/view/dialog/D;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    return-object v0
.end method

.method private c(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->f(I)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->a()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/s;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/s;->c(I)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/dialog/s;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private e(I)LaG/d;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->d:LaG/h;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/s;->a:[LaG/g;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LaG/h;->a(LaG/g;)LaG/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/dialog/s;)[Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    return-object v0
.end method

.method private f(I)Landroid/widget/ListView;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    aget-object v0, v0, p1

    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    const v1, 0x7f100086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    const v1, 0x7f100087

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->o:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/v;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/v;-><init>(Lcom/google/googlenav/ui/view/dialog/s;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->p:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/w;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/w;-><init>(Lcom/google/googlenav/ui/view/dialog/s;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private l()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/s;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/D;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/s;->w_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    const v0, 0x7f0200b9

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-virtual {p1, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    return-void
.end method

.method public c()Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/s;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040019

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    const v2, 0x7f100083

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    new-array v0, v5, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/s;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04001a

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/s;->n:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/s;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/C;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/C;-><init>(Lcom/google/googlenav/ui/view/dialog/s;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/t;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/t;-><init>(Lcom/google/googlenav/ui/view/dialog/s;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    const v2, 0x7f10008a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10001e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/s;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10008b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/u;-><init>(Lcom/google/googlenav/ui/view/dialog/s;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/s;->l()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->l:Landroid/view/View;

    return-object v0
.end method

.method protected d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->m:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f100084

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/view/View;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/s;->c:Lcom/google/googlenav/ui/view/dialog/D;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/D;->a()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x24b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
