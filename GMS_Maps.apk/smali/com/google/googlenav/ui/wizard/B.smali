.class public abstract Lcom/google/googlenav/ui/wizard/B;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field protected a:Lcom/google/googlenav/J;

.field protected b:LaR/R;

.field c:Ljava/lang/String;

.field private i:Lcom/google/googlenav/ui/view/dialog/bD;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->c()LaR/R;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->b:LaR/R;

    return-void
.end method


# virtual methods
.method protected a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/B;->a:Lcom/google/googlenav/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->j()V

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->b:LaR/R;

    const/4 v1, 0x0

    sget-object v2, LaR/O;->b:LaR/O;

    invoke-interface {v0, v1, v2}, LaR/R;->a(ZLaR/O;)V

    :cond_1
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->c(Z)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 14

    const/4 v13, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/ui/wizard/eB;

    invoke-virtual {v7}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/google/googlenav/ui/wizard/eB;-><init>(LaR/n;)V

    new-instance v9, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->f()LaR/n;

    move-result-object v0

    const/16 v2, 0x2ee

    invoke-direct {v9, v0, v2}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    new-instance v10, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->g()LaR/n;

    move-result-object v0

    invoke-direct {v10, v0}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;)V

    if-nez p1, :cond_7

    move v0, v4

    :goto_0
    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/googlenav/ui/wizard/eE;

    invoke-virtual {v7}, LaR/l;->l()LaR/n;

    move-result-object v2

    invoke-interface {v2}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/wizard/eE;-><init>(LaR/u;)V

    move-object v6, v0

    :goto_1
    if-nez p1, :cond_9

    move v0, v4

    :goto_2
    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/googlenav/ui/wizard/ex;

    invoke-virtual {v7}, LaR/l;->k()LaR/n;

    move-result-object v2

    const/16 v3, 0x2e9

    invoke-direct {v0, v2, v3}, Lcom/google/googlenav/ui/wizard/ex;-><init>(LaR/n;I)V

    :goto_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->j()Z

    move-result v2

    if-eqz v2, :cond_b

    new-instance v2, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->h()LaR/n;

    move-result-object v3

    const/16 v11, 0x2e8

    invoke-direct {v2, v3, v11}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    :goto_4
    new-instance v11, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->i()LaR/n;

    move-result-object v3

    const/16 v12, 0x2eb

    invoke-direct {v11, v3, v12}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    if-eqz p1, :cond_c

    move-object v3, v1

    :goto_5
    const/4 v7, 0x6

    new-array v7, v7, [Lcom/google/googlenav/ui/wizard/eI;

    aput-object v9, v7, v5

    aput-object v10, v7, v4

    aput-object v0, v7, v13

    const/4 v10, 0x3

    aput-object v2, v7, v10

    const/4 v10, 0x4

    aput-object v3, v7, v10

    const/4 v10, 0x5

    aput-object v11, v7, v10

    invoke-static {v7}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    :cond_0
    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    new-instance v1, Lcom/google/googlenav/ui/wizard/aX;

    const/16 v10, 0x2ec

    invoke-direct {v1, v7, v10}, Lcom/google/googlenav/ui/wizard/aX;-><init>(Ljava/util/List;I)V

    new-array v7, v13, [Lcom/google/googlenav/ui/wizard/eI;

    aput-object v8, v7, v5

    aput-object v9, v7, v4

    invoke-static {v7}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/ui/wizard/aX;

    invoke-direct {v5, v4}, Lcom/google/googlenav/ui/wizard/aX;-><init>(Ljava/util/List;)V

    new-instance v4, Lcom/google/googlenav/ui/wizard/eF;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/B;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v4, p0, v7}, Lcom/google/googlenav/ui/wizard/eF;-><init>(Lcom/google/googlenav/ui/wizard/B;Lcom/google/googlenav/ui/wizard/jv;)V

    if-nez p1, :cond_1

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v7, Lcom/google/googlenav/ui/view/dialog/bD;

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/B;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v7, v8}, Lcom/google/googlenav/ui/view/dialog/bD;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v7, p0, Lcom/google/googlenav/ui/wizard/B;->i:Lcom/google/googlenav/ui/view/dialog/bD;

    const/16 v7, 0x35b

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "offline"

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/B;->i:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-virtual {v4, v7, v8, v9}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V

    :cond_1
    const/16 v7, 0x57b

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "stars"

    invoke-virtual {v4, v7, v8, v5}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    const/16 v5, 0x432

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "recent"

    invoke-virtual {v4, v5, v7, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    if-eqz v0, :cond_2

    const/16 v1, 0xfc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "directions"

    invoke-virtual {v4, v1, v5, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    :cond_2
    if-eqz v3, :cond_3

    const/16 v0, 0x2d7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mymaps"

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    :cond_3
    if-eqz v2, :cond_4

    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "checked-in"

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    :cond_4
    const/16 v0, 0x3f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "rated"

    invoke-virtual {v4, v0, v1, v11}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    if-eqz v6, :cond_5

    const/16 v0, 0x4f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "queries"

    invoke-virtual {v4, v0, v1, v6}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;)V

    :goto_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    :cond_6
    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void

    :cond_7
    move v0, v5

    goto/16 :goto_0

    :cond_8
    move-object v6, v1

    goto/16 :goto_1

    :cond_9
    move v0, v5

    goto/16 :goto_2

    :cond_a
    move-object v0, v1

    goto/16 :goto_3

    :cond_b
    move-object v2, v1

    goto/16 :goto_4

    :cond_c
    new-instance v3, Lcom/google/googlenav/ui/wizard/eA;

    invoke-virtual {v7}, LaR/l;->j()LaR/n;

    move-result-object v7

    const/16 v12, 0x2ea

    invoke-direct {v3, v7, v12}, Lcom/google/googlenav/ui/wizard/eA;-><init>(LaR/n;I)V

    goto/16 :goto_5

    :cond_d
    const-string v0, "stars"

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;)V

    goto :goto_6
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/B;->a(Z)V

    return-void
.end method

.method protected b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected c()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->a()V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/B;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/B;->b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0
.end method
