.class public Lcom/google/googlenav/ui/wizard/aF;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aF;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/aF;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aF;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/h;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/h;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/ad;->u()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/h;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->v()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/friend/ad;->t()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/aK;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/aF;->b(Lcom/google/googlenav/ui/wizard/aK;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aK;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aF;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {p1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    const/4 v3, 0x1

    new-instance v4, Lcom/google/googlenav/ui/wizard/aI;

    invoke-direct {v4, p0, p2}, Lcom/google/googlenav/ui/wizard/aI;-><init>(Lcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/aK;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ui/wizard/aK;)V
    .locals 8

    const/16 v0, 0x81

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x7f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v0, 0x7e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v0, 0x80

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aF;->a:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v7, Lcom/google/googlenav/ui/wizard/aJ;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/ui/wizard/aJ;-><init>(Lcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/aK;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aK;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/aF;->a(Lcom/google/googlenav/h;)V

    invoke-virtual {p1}, Lcom/google/googlenav/h;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/h;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/aF;->b(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aK;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/wizard/aF;->a(Lcom/google/googlenav/ui/wizard/aK;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/aK;)V
    .locals 3

    const-string v0, "home_speedbump_ack"

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/ui/wizard/aG;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/aG;-><init>(Lcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/aK;)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ZLcom/google/googlenav/friend/ba;)V

    return-void
.end method
