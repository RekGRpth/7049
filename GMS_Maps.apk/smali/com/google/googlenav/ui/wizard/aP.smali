.class public Lcom/google/googlenav/ui/wizard/aP;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lbf/a;

.field private final b:Lcom/google/googlenav/android/aa;


# direct methods
.method public constructor <init>(Lbf/a;Lcom/google/googlenav/android/aa;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aP;->a:Lbf/a;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/aP;->b:Lcom/google/googlenav/android/aa;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/aP;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aP;->b:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/ui/wizard/aW;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/aP;->a(Lcom/google/googlenav/ui/wizard/aW;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/aW;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/aP;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/aV;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/aV;-><init>(Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/ui/wizard/aW;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/aP;)Lbf/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aP;->a:Lbf/a;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aW;)V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/friend/ad;->y()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aP;->a:Lbf/a;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/aP;->b:Lcom/google/googlenav/android/aa;

    new-instance v3, Lcom/google/googlenav/ui/wizard/aQ;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/googlenav/ui/wizard/aQ;-><init>(Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aW;)V

    invoke-virtual {v0, v1, v2, v3}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    return-void
.end method
