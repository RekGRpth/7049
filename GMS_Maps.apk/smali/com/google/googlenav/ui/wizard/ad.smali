.class Lcom/google/googlenav/ui/wizard/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/L;
.implements Lcom/google/googlenav/ui/wizard/al;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/S;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/S;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ad;-><init>(Lcom/google/googlenav/ui/wizard/S;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/aU;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/aU;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->c(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/aU;)Lcom/google/googlenav/aU;

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/i;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/i;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    invoke-virtual {v3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/h;->a(ZZ)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/i;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/google/googlenav/h;->h()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0}, Lcom/google/googlenav/friend/aD;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v1

    if-ne v3, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v1, v3, p1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/google/googlenav/h;->h()Z

    move-result v0

    if-nez v0, :cond_4

    new-array v0, v1, [Lcom/google/googlenav/friend/aD;

    invoke-static {}, Lcom/google/googlenav/friend/aD;->i()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-array v0, v1, [Lcom/google/googlenav/friend/aD;

    invoke-static {}, Lcom/google/googlenav/friend/aD;->j()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/an;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->h()V

    :cond_0
    return-void
.end method

.method public a(ZLaG/n;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p2}, LaG/n;->e()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/R;

    new-instance v2, Lcom/google/googlenav/ui/wizard/ae;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/wizard/ae;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    invoke-direct {v1, p2, v2}, Lcom/google/googlenav/ui/view/dialog/R;-><init>(LaG/n;Lcom/google/googlenav/ui/view/dialog/W;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(Landroid/app/Dialog;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->A()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->n(Lcom/google/googlenav/ui/wizard/S;)V

    goto :goto_0
.end method

.method public a(ZLcom/google/googlenav/a;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/q;->a(Ljava/util/List;)Lcom/google/googlenav/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aM;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/a;

    invoke-virtual {v0}, Lcom/google/googlenav/a;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/googlenav/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v1, v3, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ad;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->b(Lcom/google/googlenav/ui/wizard/S;)LaG/h;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/ah;->a(Ljava/util/List;LaG/h;)V

    :goto_0
    return-void

    :cond_2
    const/16 v0, 0xab

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(II)Landroid/widget/Toast;

    goto :goto_0
.end method
