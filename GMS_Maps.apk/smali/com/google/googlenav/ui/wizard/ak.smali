.class Lcom/google/googlenav/ui/wizard/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/f;
.implements Lbf/g;
.implements Lcom/google/googlenav/r;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/ah;

.field private b:Lcom/google/googlenav/a;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/ah;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/ah;Lcom/google/googlenav/ui/wizard/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ak;-><init>(Lcom/google/googlenav/ui/wizard/ah;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/F;)V
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/googlenav/friend/aK;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/aK;-><init>(Lcom/google/googlenav/F;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/a;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->c(Lcom/google/googlenav/ui/wizard/ah;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ah;->b(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2, p0}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    goto :goto_0
.end method

.method public a(ZLcom/google/googlenav/a;Lcom/google/googlenav/h;Ljava/util/List;)V
    .locals 3

    if-eqz p1, :cond_2

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-virtual {p3}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aM;->a(Lcom/google/googlenav/a;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->c(Lcom/google/googlenav/ui/wizard/ah;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ah;->b(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    invoke-static {p3}, Lcom/google/googlenav/RatingReminderManager;->a(Lcom/google/googlenav/h;)V

    :goto_0
    invoke-virtual {p3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p3}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/ad;->s()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    goto :goto_0
.end method
