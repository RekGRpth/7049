.class public Lcom/google/googlenav/ui/wizard/bI;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lax/b;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_1
    invoke-static {p0}, Lau/b;->h(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/bI;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/bI;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lax/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    packed-switch p1, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v0, p2}, Lax/b;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/bI;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bI;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bI;->b:Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/bK;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/bK;-><init>(Lcom/google/googlenav/ui/wizard/bI;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bI;->a()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bI;->a(Lax/b;)V

    return-void
.end method

.method protected e()Ljava/util/Vector;
    .locals 15

    const/4 v14, 0x3

    const/16 v13, 0x11

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->ab()Lax/h;

    move-result-object v4

    move v0, v1

    :goto_0
    invoke-virtual {v4}, Lax/h;->d()I

    move-result v2

    if-ge v0, v2, :cond_7

    invoke-virtual {v4, v0}, Lax/h;->a(I)Lax/m;

    move-result-object v2

    invoke-virtual {v2}, Lax/m;->q()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v2}, Lax/m;->o()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v5

    invoke-virtual {v5}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/bI;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v6

    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v7

    invoke-virtual {v7}, Lax/t;->y()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v2

    invoke-virtual {v2}, Lax/t;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lbj/m;

    sget-object v7, Lcom/google/googlenav/ui/aV;->P:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v7}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v6, v2, v1}, Lbj/m;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v3, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move v2, v1

    :goto_1
    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v13, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v6

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v7, v6}, Lax/b;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_2
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/googlenav/ui/aV;->C:Lcom/google/googlenav/ui/aV;

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v9, Lcom/google/googlenav/ui/wizard/bL;

    invoke-static {v8}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-direct {v9, v8}, Lcom/google/googlenav/ui/wizard/bL;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v7, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/bI;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v9, Lcom/google/googlenav/ui/bi;->R:C

    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/ui/wizard/bL;

    invoke-static {v7}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v7

    new-instance v9, Lcom/google/googlenav/ui/wizard/bJ;

    invoke-direct {v9, p0, v6}, Lcom/google/googlenav/ui/wizard/bJ;-><init>(Lcom/google/googlenav/ui/wizard/bI;I)V

    invoke-direct {v8, v7, v9}, Lcom/google/googlenav/ui/wizard/bL;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_7
    return-object v3
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bI;->b:Z

    return v0
.end method
