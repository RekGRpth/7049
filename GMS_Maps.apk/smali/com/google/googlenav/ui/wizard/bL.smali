.class Lcom/google/googlenav/ui/wizard/bL;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Ljava/lang/CharSequence;

.field final b:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/bM;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bM;-><init>(Lcom/google/googlenav/ui/wizard/bL;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lcom/google/googlenav/ui/wizard/bM;

    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f040170

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
