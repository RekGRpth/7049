.class public Lcom/google/googlenav/ui/wizard/br;
.super Lcom/google/googlenav/ui/wizard/hH;
.source "SourceFile"


# static fields
.field private static final l:[I

.field private static final m:[I

.field private static final n:[I

.field private static final o:[I


# instance fields
.field private k:LaH/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/ui/wizard/br;->l:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/ui/wizard/br;->m:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/googlenav/ui/wizard/br;->n:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/googlenav/ui/wizard/br;->o:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x46b
        0x468
        0x46a
        0x469
    .end array-data

    :array_1
    .array-data 4
        0x475
        0x477
        0x474
        0x479
        0x476
        0x478
    .end array-data

    :array_2
    .array-data 4
        0x45f
        0x461
        0x45e
        0x460
    .end array-data

    :array_3
    .array-data 4
        0x472
        0x46d
        0x471
        0x46f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaH/m;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/hH;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/br;->k:LaH/m;

    return-void
.end method

.method private static a(Ljava/util/List;II)V
    .locals 3

    new-instance v0, Lbj/bt;

    invoke-static {p1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-direct {v0, v1, v2, p2}, Lbj/bt;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;I)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/br;->f()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    return-object v0
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;LaN/H;IILjava/lang/String;Lcom/google/googlenav/ui/wizard/C;Z)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/hQ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/hQ;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p4, v0, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p5, v0, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p6, v0, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    if-eqz p9, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/br;->c:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const/16 v1, 0x467

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const/4 v1, 0x4

    iput v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/br;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p7, v0, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/googlenav/ui/wizard/br;->j:Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/br;->j()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/br;->c:I

    goto :goto_0
.end method

.method protected a(Law/g;)V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/br;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/br;->k:LaH/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v4, v4, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/J;->a(LaH/m;LaN/B;ZLjava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const/16 v1, 0x473

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_1
    const/16 v1, 0x45d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v3, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_2
    const/16 v1, 0x467

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_3
    const/16 v1, 0x46c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_4
    sget-object v1, Lcom/google/googlenav/ui/wizard/br;->l:[I

    aget v1, v1, p2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/googlenav/ui/wizard/br;->b(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_5
    sget-object v1, Lcom/google/googlenav/ui/wizard/br;->m:[I

    aget v1, v1, p2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/googlenav/ui/wizard/br;->b(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_6
    sget-object v1, Lcom/google/googlenav/ui/wizard/br;->n:[I

    aget v1, v1, p2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/googlenav/ui/wizard/br;->b(Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_7
    sget-object v1, Lcom/google/googlenav/ui/wizard/br;->o:[I

    aget v1, v1, p2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/googlenav/ui/wizard/br;->b(Ljava/lang/String;II)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5de -> :sswitch_0
        0x5df -> :sswitch_1
        0x5e1 -> :sswitch_3
        0x5e2 -> :sswitch_2
        0x5fa -> :sswitch_5
        0x5fb -> :sswitch_6
        0x5fc -> :sswitch_4
        0x5fd -> :sswitch_7
    .end sparse-switch
.end method

.method protected b()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/hH;->b()V

    return-void
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public f()Ljava/util/List;
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/br;->c:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v1

    :pswitch_0
    const/16 v0, 0x467

    const/16 v2, 0x5e2

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    const/16 v0, 0x473

    const/16 v2, 0x5de

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    const/16 v0, 0x45d

    const/16 v2, 0x5df

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    const/16 v0, 0x46c

    const/16 v2, 0x5e1

    invoke-static {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/br;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    :goto_1
    :pswitch_3
    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->o:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->o:[I

    aget v2, v2, v0

    const/16 v3, 0x5fd

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :goto_2
    :pswitch_4
    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->l:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->l:[I

    aget v2, v2, v0

    const/16 v3, 0x5fc

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :goto_3
    :pswitch_5
    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->m:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->m:[I

    aget v2, v2, v0

    const/16 v3, 0x5fa

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :goto_4
    :pswitch_6
    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->n:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/wizard/br;->n:[I

    aget v2, v2, v0

    const/16 v3, 0x5fb

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/br;->a(Ljava/util/List;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected g()Lcom/google/googlenav/ui/view/android/aL;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/bt;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bt;-><init>(Lcom/google/googlenav/ui/wizard/br;)V

    return-object v0
.end method
