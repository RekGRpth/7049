.class Lcom/google/googlenav/ui/wizard/cG;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/cC;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/cC;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/cC;Lcom/google/googlenav/ui/wizard/cD;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/cG;-><init>(Lcom/google/googlenav/ui/wizard/cC;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v5, 0x43

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/a;->d()Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->K()Ljava/util/Vector;

    move-result-object v0

    if-le v4, v6, :cond_1

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    const-string v1, "f"

    const-string v2, "r"

    invoke-static {v5, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v1

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v1, v0}, Lbf/am;->g(Lbf/i;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->J()Ljava/util/Vector;

    move-result-object v0

    if-le v4, v6, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v1

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v1, v0}, Lbf/am;->h(Lbf/i;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto :goto_0

    :pswitch_3
    const-string v0, "f"

    const-string v3, "l"

    invoke-static {v5, v0, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->e()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbf/am;->d(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/J;->b(Z)V

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "f"

    const-string v3, "t"

    invoke-static {v5, v0, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->d()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/J;->a(ZZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->b()I

    move-result v0

    if-eq v0, v1, :cond_5

    const-string v0, "f"

    const-string v2, "s"

    invoke-static {v5, v0, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(I)Z

    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/J;->a(I)Z

    goto :goto_2

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->b()I

    move-result v0

    if-eq v0, v7, :cond_6

    const-string v0, "f"

    const-string v1, "n"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/googlenav/J;->a(I)Z

    :goto_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/J;->a(I)Z

    goto :goto_3

    :pswitch_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->c()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "f"

    const-string v2, "c"

    invoke-static {v5, v0, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->c_(Z)V

    :goto_4
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->b(Lcom/google/googlenav/ui/wizard/cC;)Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/J;->c_(Z)V

    goto :goto_4

    :pswitch_8
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v1

    check-cast v1, Lbf/y;

    if-nez v1, :cond_a

    const-string v1, "f"

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v1, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "LayerTransit"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lbf/am;->b(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    :cond_9
    :goto_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v1}, Lbf/y;->bI()V

    invoke-virtual {v1}, Lbf/y;->bJ()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbf/am;->h(Lbf/i;)V

    goto :goto_5

    :pswitch_9
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "My Maps"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_b
    const-string v0, "f"

    const/16 v1, 0x3ed

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lcom/google/googlenav/ui/wizard/cC;)Lbf/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cC;->b(Lbf/am;)Lcom/google/googlenav/ui/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/av;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cG;->a:Lcom/google/googlenav/ui/wizard/cC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cC;->a()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
