.class public Lcom/google/googlenav/ui/wizard/cO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/cT;

.field private final b:Lcom/google/googlenav/ui/wizard/cR;

.field private c:Z

.field private d:Lcom/google/googlenav/friend/af;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/cT;Lcom/google/googlenav/ui/wizard/cR;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cO;->a:Lcom/google/googlenav/ui/wizard/cT;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->a:Lcom/google/googlenav/ui/wizard/cT;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cQ;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/cQ;-><init>(Lcom/google/googlenav/ui/wizard/cO;Lcom/google/googlenav/ui/wizard/cP;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cT;->a(Lcom/google/googlenav/ui/wizard/cY;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cO;Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/friend/af;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cO;)Lcom/google/googlenav/ui/wizard/cS;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->f()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/cO;)Lcom/google/googlenav/ui/wizard/cR;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    return-object v0
.end method

.method private f()Lcom/google/googlenav/ui/wizard/cS;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->g()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/google/googlenav/ui/wizard/cS;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->g:Lcom/google/googlenav/ui/wizard/cS;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->g()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->e:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->c()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/cO;->c:Z

    if-eqz v1, :cond_9

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->d()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->g:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->h()Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->c:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->b()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->j()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->d:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->i()Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->b:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->i()Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->b()Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->f:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->g:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_9
    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->d()Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->g:Lcom/google/googlenav/ui/wizard/cS;

    goto :goto_0

    :cond_a
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->h()Z

    move-result v1

    if-nez v1, :cond_b

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->a:Lcom/google/googlenav/ui/wizard/cS;

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->i()Z

    move-result v1

    if-nez v1, :cond_c

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->b:Lcom/google/googlenav/ui/wizard/cS;

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->i()Z

    move-result v1

    if-eqz v1, :cond_d

    if-nez v0, :cond_d

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->f:Lcom/google/googlenav/ui/wizard/cS;

    goto/16 :goto_0

    :cond_d
    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->g:Lcom/google/googlenav/ui/wizard/cS;

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/af;->b(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->f()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/cR;->a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    :cond_0
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/cO;->c:Z

    new-instance v0, Lcom/google/googlenav/friend/af;

    invoke-direct {v0}, Lcom/google/googlenav/friend/af;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->a:Lcom/google/googlenav/ui/wizard/cT;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cT;->a(Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/af;->d(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->f()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/cR;->a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/af;->c(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->f()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/cR;->a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->a:Lcom/google/googlenav/ui/wizard/cT;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cT;->c(Lcom/google/googlenav/friend/af;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cO;->f()Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/cR;->a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cO;->b:Lcom/google/googlenav/ui/wizard/cR;

    sget-object v1, Lcom/google/googlenav/ui/wizard/cS;->i:Lcom/google/googlenav/ui/wizard/cS;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cO;->d:Lcom/google/googlenav/friend/af;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/cR;->a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method
