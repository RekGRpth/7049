.class public Lcom/google/googlenav/ui/wizard/cZ;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private b:Z

.field private c:Lcom/google/googlenav/ui/wizard/cS;

.field private i:Lcom/google/googlenav/ui/wizard/cO;

.field private j:Lcom/google/googlenav/ui/wizard/db;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->h:Lcom/google/googlenav/ui/wizard/cS;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->c:Lcom/google/googlenav/ui/wizard/cS;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cZ;->a:Lcom/google/googlenav/J;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cS;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->c:Lcom/google/googlenav/ui/wizard/cS;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cZ;Lcom/google/googlenav/ui/wizard/cS;)Lcom/google/googlenav/ui/wizard/cS;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cZ;->c:Lcom/google/googlenav/ui/wizard/cS;

    return-object p1
.end method

.method private a(ZLcom/google/googlenav/ui/wizard/db;Lcom/google/googlenav/ui/wizard/cO;)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/cZ;->b:Z

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cZ;->j:Lcom/google/googlenav/ui/wizard/db;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cZ;->j()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cO;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/db;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->j:Lcom/google/googlenav/ui/wizard/db;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method private e()Lcom/google/googlenav/ui/wizard/cO;
    .locals 4

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/reporting/f;

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/cT;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cZ;->a:Lcom/google/googlenav/J;

    invoke-interface {v2}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v2

    invoke-virtual {v2}, LaB/o;->b()LaB/s;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/googlenav/ui/wizard/cT;-><init>(LaB/s;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/reporting/s;)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/dc;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/wizard/dc;-><init>(Lcom/google/googlenav/ui/wizard/cZ;Lcom/google/googlenav/ui/wizard/da;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/cO;

    invoke-direct {v2, v1, v0}, Lcom/google/googlenav/ui/wizard/cO;-><init>(Lcom/google/googlenav/ui/wizard/cT;Lcom/google/googlenav/ui/wizard/cR;)V

    return-object v2
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cZ;->h()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cZ;->a()V

    :cond_0
    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected a(ZLcom/google/googlenav/ui/wizard/db;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cZ;->e()Lcom/google/googlenav/ui/wizard/cO;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/wizard/cZ;->a(ZLcom/google/googlenav/ui/wizard/db;Lcom/google/googlenav/ui/wizard/cO;)V

    return-void
.end method

.method protected b()V
    .locals 7

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->h:Lcom/google/googlenav/ui/wizard/cS;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->c:Lcom/google/googlenav/ui/wizard/cS;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/cZ;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/cO;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    sget-object v0, Lcom/google/googlenav/ui/wizard/cS;->h:Lcom/google/googlenav/ui/wizard/cS;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->c:Lcom/google/googlenav/ui/wizard/cS;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/cZ;->j:Lcom/google/googlenav/ui/wizard/db;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->b:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cZ;->j:Lcom/google/googlenav/ui/wizard/db;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cZ;->a()V

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->b:Z

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/cZ;->j:Lcom/google/googlenav/ui/wizard/db;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cZ;->j()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cZ;->i:Lcom/google/googlenav/ui/wizard/cO;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cO;->e()V

    return-void
.end method
