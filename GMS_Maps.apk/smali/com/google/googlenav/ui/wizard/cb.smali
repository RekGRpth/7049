.class public Lcom/google/googlenav/ui/wizard/cb;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Lcom/google/googlenav/ui/wizard/cc;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private e()Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public a(ILcom/google/googlenav/ui/wizard/cc;)V
    .locals 0

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    iput p1, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->j()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/cd;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/wizard/cc;->a(Lcom/google/googlenav/ui/wizard/cd;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->h()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cb;->e()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/view/android/L;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/L;-><init>(Lcom/google/googlenav/ui/wizard/cb;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/view/android/D;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/D;-><init>(Lcom/google/googlenav/ui/wizard/cb;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/cc;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    return-void
.end method

.method public d()V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/cb;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    return-void
.end method
