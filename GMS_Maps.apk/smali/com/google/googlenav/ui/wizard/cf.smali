.class public Lcom/google/googlenav/ui/wizard/cf;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lcom/google/googlenav/aw;

.field private c:I

.field private i:Z


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/16 v1, 0x9e

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cf;->a()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/au;)[Lcom/google/googlenav/aw;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/cf;->a([Lcom/google/googlenav/aw;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/cf;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a([Lcom/google/googlenav/aw;)V
    .locals 7

    new-instance v0, Lcom/google/googlenav/ui/wizard/ci;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/cf;->c:I

    iget-boolean v3, p0, Lcom/google/googlenav/ui/wizard/cf;->i:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cf;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/cf;->d:Lcom/google/googlenav/ui/wizard/jv;

    move-object v1, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/ci;-><init>([Lcom/google/googlenav/aw;IZLcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method private e()V
    .locals 7

    new-instance v2, Lcom/google/googlenav/ui/wizard/cg;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/cg;-><init>(Lcom/google/googlenav/ui/wizard/cf;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/ch;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/wizard/ch;-><init>(Lcom/google/googlenav/ui/wizard/cf;Law/c;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cf;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/googlenav/f;->a(Lcom/google/googlenav/g;Ljava/lang/String;)Lcom/google/googlenav/f;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cf;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cf;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/cf;->g:I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cf;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cf;->b:[Lcom/google/googlenav/aw;

    iput p3, p0, Lcom/google/googlenav/ui/wizard/cf;->c:I

    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/cf;->i:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cf;->j()V

    return-void
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/cf;->g:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->b:[Lcom/google/googlenav/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->b:[Lcom/google/googlenav/aw;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/cf;->a([Lcom/google/googlenav/aw;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cf;->e()V

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->b:[Lcom/google/googlenav/aw;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->h:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cf;->b:[Lcom/google/googlenav/aw;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/cf;->c:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cf;->a()V

    iget-boolean v3, p0, Lcom/google/googlenav/ui/wizard/cf;->i:Z

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/cf;->a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/ci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cf;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/ci;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ci;->h()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cf;->a()V

    return-void
.end method
