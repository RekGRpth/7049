.class public Lcom/google/googlenav/ui/wizard/co;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/googlenav/aw;

.field private final b:Lcom/google/googlenav/android/aa;

.field private final c:Z

.field private final d:Ljava/util/WeakHashMap;


# direct methods
.method public constructor <init>(Landroid/widget/Gallery;[Lcom/google/googlenav/aw;Lcom/google/googlenav/android/aa;Z)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/co;->b:Lcom/google/googlenav/android/aa;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/co;->c:Z

    return-void
.end method

.method private a(ILandroid/view/ViewGroup;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/co;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/googlenav/aw;->c:Ljava/lang/String;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f04013c

    const/4 v2, 0x0

    invoke-static {v0, p2, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/co;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/googlenav/aw;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 5

    sget v0, Lcom/google/googlenav/ui/bi;->bA:I

    invoke-static {p2, v0}, Lcom/google/googlenav/aP;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/aP;

    const/4 v3, 0x1

    new-instance v4, Lcom/google/googlenav/ui/wizard/cp;

    invoke-direct {v4, p0, p1, p2, v0}, Lcom/google/googlenav/ui/wizard/cp;-><init>(Lcom/google/googlenav/ui/wizard/co;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v0, v3, v4}, Lcom/google/googlenav/aP;-><init>(Ljava/lang/String;ZLcom/google/googlenav/aR;)V

    invoke-virtual {v1, v2}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(Landroid/widget/ImageView;Ljava/lang/String;[B)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/co;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/cq;

    invoke-direct {v2, p0, p1, p3}, Lcom/google/googlenav/ui/wizard/cq;-><init>(Lcom/google/googlenav/ui/wizard/co;Landroid/widget/ImageView;[B)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/co;Landroid/widget/ImageView;Ljava/lang/String;[B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/co;->a(Landroid/widget/ImageView;Ljava/lang/String;[B)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/co;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/googlenav/aw;->c:Ljava/lang/String;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/ui/wizard/co;->a(ILandroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/co;->a:[Lcom/google/googlenav/aw;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/googlenav/aw;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const v0, 0x7f04013c

    const/4 v2, 0x0

    invoke-static {v0, p3, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/co;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/co;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/ui/wizard/co;->a(ILandroid/view/ViewGroup;)V

    goto :goto_1
.end method
