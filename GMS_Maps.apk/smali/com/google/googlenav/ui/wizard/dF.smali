.class public Lcom/google/googlenav/ui/wizard/dF;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Z

.field d:Ljava/lang/Boolean;

.field e:LaN/B;

.field f:I

.field g:Ljava/lang/String;

.field h:Lcom/google/googlenav/ui/wizard/dy;

.field i:I

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:B

.field m:Z

.field n:Z

.field o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:Ljava/util/List;

.field r:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    const v0, 0x1869f

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dF;->f:I

    iput v1, p0, Lcom/google/googlenav/ui/wizard/dF;->i:I

    return-void
.end method


# virtual methods
.method public a(B)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/ui/wizard/dF;->l:B

    return-object p0
.end method

.method public a(I)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/ui/wizard/dF;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No callback provided to LocationSelectionWizard."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No sourceOption provided to LocationSelectionWizard."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public b(I)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/dF;->i:I

    return-object p0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->g:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    return-object p0
.end method

.method b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->j:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->m:Z

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->k:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->n:Z

    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/ui/wizard/dF;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->r:Z

    return-object p0
.end method
