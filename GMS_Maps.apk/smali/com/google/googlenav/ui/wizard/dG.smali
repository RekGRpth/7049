.class public Lcom/google/googlenav/ui/wizard/dG;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dG;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 12

    const/16 v11, 0x32f

    const/16 v10, 0x328

    const/4 v9, 0x2

    const/4 v8, 0x1

    new-instance v4, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v4, p1, v8}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->f(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->g(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/16 v0, 0x1d0

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->h(Lcom/google/googlenav/ui/wizard/dg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/text/SpannableStringBuilder;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dJ;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dJ;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x32d

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x313

    :goto_2
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aw:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dK;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/wizard/dK;-><init>(Lcom/google/googlenav/ui/wizard/dG;I)V

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    move v3, v0

    :goto_3
    if-ge v3, v5, :cond_a

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/googlenav/friend/ad;->v()V

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_4
    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {v0, v9}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    :cond_3
    new-instance v6, Landroid/text/SpannableStringBuilder;

    sget-object v7, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v7}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-direct {v6, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0, v6}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/text/SpannableStringBuilder;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dL;

    invoke-direct {v2, p0, v1, v3}, Lcom/google/googlenav/ui/wizard/dL;-><init>(Lcom/google/googlenav/ui/wizard/dG;Ljava/lang/Integer;I)V

    invoke-virtual {v0, v6, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    const/16 v0, 0x3e5

    goto/16 :goto_0

    :cond_5
    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_6
    const/16 v1, 0x312

    goto :goto_2

    :cond_7
    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ne v9, v1, :cond_8

    invoke-static {}, Lcom/google/googlenav/friend/ad;->u()V

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_4

    :cond_8
    invoke-static {}, Lcom/google/googlenav/friend/ad;->t()V

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_4

    :cond_9
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_4

    :cond_a
    return-object v4
.end method

.method public c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dG;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ea

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/dg;->d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f02022a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dG;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dG;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/dH;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dH;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f10002e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    const/16 v2, 0x4fe

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/dI;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dI;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-object v1

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f020228

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f020215

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
