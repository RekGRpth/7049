.class public Lcom/google/googlenav/ui/wizard/dR;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/eb;

.field private final b:Lcom/google/googlenav/ui/wizard/H;

.field private final c:Lcom/google/googlenav/ui/wizard/dX;

.field private final d:Lcom/google/googlenav/ui/wizard/aP;

.field private final e:Lcom/google/googlenav/ui/wizard/jv;

.field private final f:Lcom/google/googlenav/ui/wizard/dV;

.field private g:Lcom/google/googlenav/h;

.field private h:Lcom/google/googlenav/ui/wizard/dW;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/H;Lcom/google/googlenav/ui/wizard/dX;Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/ui/wizard/dW;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dR;->e:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/dR;->d:Lcom/google/googlenav/ui/wizard/aP;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/dR;->b:Lcom/google/googlenav/ui/wizard/H;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/dR;->c:Lcom/google/googlenav/ui/wizard/dX;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/dR;->h:Lcom/google/googlenav/ui/wizard/dW;

    new-instance v0, Lcom/google/googlenav/ui/wizard/dT;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dT;-><init>(Lcom/google/googlenav/ui/wizard/dR;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->a:Lcom/google/googlenav/ui/wizard/eb;

    new-instance v0, Lcom/google/googlenav/ui/wizard/dV;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/dV;-><init>(Lcom/google/googlenav/ui/wizard/dR;Lcom/google/googlenav/ui/wizard/dS;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->f:Lcom/google/googlenav/ui/wizard/dV;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->b:Lcom/google/googlenav/ui/wizard/H;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dR;->f:Lcom/google/googlenav/ui/wizard/dV;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/H;->a(Lcom/google/googlenav/ui/wizard/L;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dX;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->c:Lcom/google/googlenav/ui/wizard/dX;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->g:Lcom/google/googlenav/h;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/H;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->b:Lcom/google/googlenav/ui/wizard/H;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->e:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dW;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->h:Lcom/google/googlenav/ui/wizard/dW;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/aP;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->d:Lcom/google/googlenav/ui/wizard/aP;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/eb;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->a:Lcom/google/googlenav/ui/wizard/eb;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/h;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dR;->g:Lcom/google/googlenav/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->c:Lcom/google/googlenav/ui/wizard/dX;

    const/16 v1, 0x606

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dX;->a(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->b:Lcom/google/googlenav/ui/wizard/H;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/H;->a()V

    return-void
.end method

.method public b(Lcom/google/googlenav/h;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dR;->g:Lcom/google/googlenav/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->c:Lcom/google/googlenav/ui/wizard/dX;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dR;->c:Lcom/google/googlenav/ui/wizard/dX;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dX;->a(Lcom/google/googlenav/h;)V

    :cond_0
    return-void
.end method
