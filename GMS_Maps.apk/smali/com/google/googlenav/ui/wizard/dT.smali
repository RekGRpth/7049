.class Lcom/google/googlenav/ui/wizard/dT;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/eb;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dR;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/dR;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/googlenav/a;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->d(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->h(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->e(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dW;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dW;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dT;Lcom/google/googlenav/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dT;->a(Lcom/google/googlenav/a;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dT;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dT;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->d(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    const/16 v2, 0x1b0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->h(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->e(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dW;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dW;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/16 v0, 0x90

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(II)Landroid/widget/Toast;

    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->a(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dX;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/dX;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->c(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/H;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/H;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/16 v0, 0xa4

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(II)Landroid/widget/Toast;

    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->a(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/dX;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/dX;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->c(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/H;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/H;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public c()V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->d(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0xb5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v3, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dR;->f(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/ui/wizard/aP;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dT;->a:Lcom/google/googlenav/ui/wizard/dR;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dR;->b(Lcom/google/googlenav/ui/wizard/dR;)Lcom/google/googlenav/h;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dU;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dU;-><init>(Lcom/google/googlenav/ui/wizard/dT;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/aP;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aW;)V

    return-void
.end method
