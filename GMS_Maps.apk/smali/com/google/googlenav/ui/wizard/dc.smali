.class Lcom/google/googlenav/ui/wizard/dc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/cR;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/cZ;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/cZ;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/cZ;Lcom/google/googlenav/ui/wizard/da;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dc;-><init>(Lcom/google/googlenav/ui/wizard/cZ;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;
    .locals 7

    const/4 v3, 0x0

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/dialog/aW;-><init>()V

    const/16 v1, 0x264

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x27a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x277

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x279

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x272

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->e(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aR;

    const v2, 0x7f020218

    new-instance v4, Lcom/google/googlenav/ui/wizard/df;

    invoke-direct {v4, p0, v3}, Lcom/google/googlenav/ui/wizard/df;-><init>(Lcom/google/googlenav/ui/wizard/dc;Lcom/google/googlenav/ui/wizard/da;)V

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v5}, Lcom/google/googlenav/ui/wizard/cZ;->d(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/J;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v5

    invoke-virtual {v5}, LaB/o;->b()LaB/s;

    move-result-object v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/dialog/aR;-><init>(Lcom/google/googlenav/ui/view/dialog/aW;ILjava/lang/String;Lcom/google/googlenav/ui/view/dialog/aV;Lcom/google/googlenav/friend/af;LaB/s;)V

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/S;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/S;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/cZ;->h:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method private b(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;
    .locals 7

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/dialog/aW;-><init>()V

    const/16 v1, 0x215

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x2a4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x2a2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x2a3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v0

    const/16 v1, 0x2a0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aW;->e(Ljava/lang/String;)Lcom/google/googlenav/ui/view/dialog/aW;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aR;

    const v2, 0x7f020219

    const-string v3, "location_graphic.png"

    new-instance v4, Lcom/google/googlenav/ui/wizard/df;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/googlenav/ui/wizard/df;-><init>(Lcom/google/googlenav/ui/wizard/dc;Lcom/google/googlenav/ui/wizard/da;)V

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v5}, Lcom/google/googlenav/ui/wizard/cZ;->d(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/J;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v5

    invoke-virtual {v5}, LaB/o;->b()LaB/s;

    move-result-object v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/dialog/aR;-><init>(Lcom/google/googlenav/ui/view/dialog/aW;ILjava/lang/String;Lcom/google/googlenav/ui/view/dialog/aV;Lcom/google/googlenav/friend/af;LaB/s;)V

    return-object v0
.end method

.method private c(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aH;

    new-instance v1, Lcom/google/googlenav/ui/wizard/dd;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/dd;-><init>(Lcom/google/googlenav/ui/wizard/dc;)V

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aH;-><init>(Lcom/google/googlenav/ui/view/dialog/aL;)V

    return-object v0
.end method

.method private d(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aM;

    new-instance v1, Lcom/google/googlenav/ui/wizard/de;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/de;-><init>(Lcom/google/googlenav/ui/wizard/dc;)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->b()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->j()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/aM;-><init>(Lcom/google/googlenav/ui/view/dialog/aQ;ZZ)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dc;->b(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/wizard/cS;Lcom/google/googlenav/friend/af;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->a(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0, p1}, Lcom/google/googlenav/ui/wizard/cZ;->a(Lcom/google/googlenav/ui/wizard/cZ;Lcom/google/googlenav/ui/wizard/cS;)Lcom/google/googlenav/ui/wizard/cS;

    sget-object v0, Lcom/google/googlenav/ui/wizard/da;->a:[I

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/cZ;->a(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/cS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/dc;->a(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dc;->a(Lcom/google/googlenav/ui/view/android/S;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/dc;->c(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dc;->a(Lcom/google/googlenav/ui/view/android/S;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/dc;->b(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dc;->a(Lcom/google/googlenav/ui/view/android/S;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/dc;->d(Lcom/google/googlenav/friend/af;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dc;->a(Lcom/google/googlenav/ui/view/android/S;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->b(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cO;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x15a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->b(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/cO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cO;->d()V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->c(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/db;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->c(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/db;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/db;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cZ;->a()V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cZ;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->c(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/db;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/cZ;->c(Lcom/google/googlenav/ui/wizard/cZ;)Lcom/google/googlenav/ui/wizard/db;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/db;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dc;->a:Lcom/google/googlenav/ui/wizard/cZ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cZ;->a()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
