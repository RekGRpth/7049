.class Lcom/google/googlenav/ui/wizard/dj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dQ;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;

.field private volatile b:Z


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/dg;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dj;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dj;->b:Z

    return-void
.end method

.method public a(Z)V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dj;->b:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->s()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/dg;->j(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, LaH/h;->a(Landroid/location/Location;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dg;Z)Z

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dg;Lax/y;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dj;->a:Lcom/google/googlenav/ui/wizard/dg;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method
