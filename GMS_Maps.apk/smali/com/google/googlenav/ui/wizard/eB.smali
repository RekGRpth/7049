.class Lcom/google/googlenav/ui/wizard/eB;
.super Lcom/google/googlenav/ui/wizard/eI;
.source "SourceFile"


# instance fields
.field private final a:LaR/E;


# direct methods
.method constructor <init>(LaR/n;)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LaR/n;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/E;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eB;->a:LaR/E;

    return-void
.end method

.method private a(IIJ)Lcom/google/googlenav/ui/wizard/eC;
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eB;->a:LaR/E;

    invoke-virtual {v0, p1}, LaR/E;->a(I)LaR/a;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x2f4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    move v3, v0

    :goto_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/eC;

    invoke-static {p2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/4 v4, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v5, v4, v3, v6}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/eC;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;JLcom/google/googlenav/ui/view/a;)V

    return-object v0

    :cond_0
    const/16 v1, 0x2f2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    move v3, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v1}, LaR/a;->e()Ljava/lang/String;

    move-result-object v2

    move v3, v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x1e1

    const-wide v3, 0x7fffffffffffffffL

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/eB;->a(IIJ)Lcom/google/googlenav/ui/wizard/eC;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    const/16 v2, 0x61b

    const-wide v3, 0x7ffffffffffffffeL

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/eB;->a(IIJ)Lcom/google/googlenav/ui/wizard/eC;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
