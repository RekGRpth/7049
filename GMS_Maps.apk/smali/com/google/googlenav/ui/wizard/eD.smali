.class Lcom/google/googlenav/ui/wizard/eD;
.super Lcom/google/googlenav/ui/wizard/eI;
.source "SourceFile"


# instance fields
.field private final a:LaR/n;

.field private final b:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(LaR/n;)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eD;->b:Ljava/text/DateFormat;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eD;->a:LaR/n;

    return-void
.end method

.method constructor <init>(LaR/n;I)V
    .locals 1

    invoke-static {p2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eD;->b:Ljava/text/DateFormat;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eD;->a:LaR/n;

    return-void
.end method

.method private a(LaR/D;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eD;->b:Ljava/text/DateFormat;

    invoke-static {p1, v0}, LaR/G;->b(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 13

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eD;->a:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->b()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eD;->a:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v12

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_2

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v12, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LaR/D;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, LaR/D;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/be;

    invoke-virtual {v6}, LaR/D;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/eD;->a(LaR/D;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, LaR/D;->i()J

    move-result-wide v3

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v5, v8, v8, v6}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/be;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;JLcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v11
.end method
