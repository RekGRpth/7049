.class public Lcom/google/googlenav/ui/wizard/ec;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/eg;

.field private b:Landroid/widget/ViewSwitcher;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/google/googlenav/ui/view/android/J;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/eg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ec;->a:Lcom/google/googlenav/ui/wizard/eg;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ec;)Lcom/google/googlenav/ui/view/android/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->d:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "HOME"

    :cond_0
    return-object v0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZZ)V
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private a(Ljava/util/List;Lcom/google/googlenav/friend/i;)V
    .locals 5

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/ec;->c(Lcom/google/googlenav/friend/i;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Lbj/bv;

    const/4 v2, 0x1

    const v3, 0x7f0400cd

    const/16 v4, 0x229

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v2, Lcom/google/googlenav/ui/wizard/ee;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/ui/wizard/ee;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lbj/bv;

    const/4 v1, 0x2

    const v2, 0x7f0400ce

    const/16 v3, 0x228

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/ec;)Lcom/google/googlenav/ui/wizard/eg;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->a:Lcom/google/googlenav/ui/wizard/eg;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/friend/i;)Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/wizard/ec;->a(Ljava/util/List;Lcom/google/googlenav/friend/i;)V

    return-object v0
.end method

.method private c(Lcom/google/googlenav/friend/i;)Ljava/util/Hashtable;
    .locals 10

    const/4 v9, 0x5

    const/4 v7, 0x4

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/i;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    move v0, v2

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v4, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {v5, v8, v8}, Lcom/google/googlenav/ui/wizard/ec;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZZ)V

    invoke-static {v5}, Lcom/google/googlenav/ui/wizard/ec;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-static {v6}, Lcom/google/googlenav/ui/wizard/ec;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1

    invoke-static {v0, v8, v2}, Lcom/google/googlenav/ui/wizard/ec;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZZ)V

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-static {v6, v2, v2}, Lcom/google/googlenav/ui/wizard/ec;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZZ)V

    invoke-virtual {v3, v7, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    return-object v3
.end method


# virtual methods
.method protected O_()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ec;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f050012

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    const/16 v0, 0x226

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ec;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/i;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ec;->b(Lcom/google/googlenav/friend/i;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ec;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400eb

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->b:Landroid/widget/ViewSwitcher;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->b:Landroid/widget/ViewSwitcher;

    const v1, 0x7f1001ef

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->b:Landroid/widget/ViewSwitcher;

    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->c:Landroid/widget/ListView;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ec;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v4, v0, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/ec;->d:Lcom/google/googlenav/ui/view/android/J;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ec;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/google/googlenav/ui/wizard/ed;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/ed;-><init>(Lcom/google/googlenav/ui/wizard/ec;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->b:Landroid/widget/ViewSwitcher;

    return-object v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ec;->a:Lcom/google/googlenav/ui/wizard/eg;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/eg;->a()V

    return-void
.end method
