.class public Lcom/google/googlenav/ui/wizard/em;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/ep;

.field private b:Lcom/google/googlenav/ui/wizard/aP;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private e()Lcom/google/googlenav/ui/wizard/dX;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/dX;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/em;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/em;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/em;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/em;->g:I

    return v0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/ep;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/em;->j()V

    return-void
.end method

.method protected b()V
    .locals 6

    new-instance v2, Lcom/google/googlenav/ui/wizard/H;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->H()Lcom/google/googlenav/friend/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/H;-><init>(Lcom/google/googlenav/friend/j;Lcom/google/googlenav/android/aa;)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/dX;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iget-boolean v1, v1, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    invoke-direct {v0, v1, p0}, Lcom/google/googlenav/ui/wizard/dX;-><init>(ZLcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v0, Lcom/google/googlenav/ui/wizard/aP;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/em;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/wizard/aP;-><init>(Lbf/a;Lcom/google/googlenav/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->b:Lcom/google/googlenav/ui/wizard/aP;

    new-instance v0, Lcom/google/googlenav/ui/wizard/dR;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/em;->e()Lcom/google/googlenav/ui/wizard/dX;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/em;->b:Lcom/google/googlenav/ui/wizard/aP;

    new-instance v5, Lcom/google/googlenav/ui/wizard/en;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/wizard/en;-><init>(Lcom/google/googlenav/ui/wizard/em;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/dR;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/H;Lcom/google/googlenav/ui/wizard/dX;Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/ui/wizard/dW;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/em;->e()Lcom/google/googlenav/ui/wizard/dX;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dR;->a()Lcom/google/googlenav/ui/wizard/eb;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/dX;->a(Lcom/google/googlenav/ui/wizard/eb;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dR;->a(Lcom/google/googlenav/h;)V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/em;->e()Lcom/google/googlenav/ui/wizard/dX;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dX;->a(Lcom/google/googlenav/ui/wizard/eb;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->b:Lcom/google/googlenav/ui/wizard/aP;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ep;->b:Lcom/google/googlenav/ui/wizard/eo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ep;->b:Lcom/google/googlenav/ui/wizard/eo;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/eo;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->b:Lcom/google/googlenav/ui/wizard/eo;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    :cond_1
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/em;->a:Lcom/google/googlenav/ui/wizard/ep;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/em;->a()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/em;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/em;->a()V

    return-void
.end method
