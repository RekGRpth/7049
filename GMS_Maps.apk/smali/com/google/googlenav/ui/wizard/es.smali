.class public final Lcom/google/googlenav/ui/wizard/es;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/eq;

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/eq;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/es;->a:Lcom/google/googlenav/ui/wizard/eq;

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput v0, p0, Lcom/google/googlenav/ui/wizard/es;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/es;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/eq;Landroid/content/Context;Lcom/google/googlenav/ui/wizard/er;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/es;-><init>(Lcom/google/googlenav/ui/wizard/eq;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/es;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/es;->b:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/es;->c:I

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/es;->a:Lcom/google/googlenav/ui/wizard/eq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/eq;->c(Lcom/google/googlenav/ui/wizard/eq;)Lcom/google/googlenav/ui/wizard/es;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/es;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v5, v1, Ljava/lang/String;

    if-nez p2, :cond_1

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/es;->a:Lcom/google/googlenav/ui/wizard/eq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/eq;->a(Lcom/google/googlenav/ui/wizard/eq;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040171

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1003ef

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    :goto_0
    new-instance v4, Lcom/google/googlenav/ui/wizard/ev;

    invoke-direct {v4, v6}, Lcom/google/googlenav/ui/wizard/ev;-><init>(Lcom/google/googlenav/ui/wizard/er;)V

    iput-object v0, v4, Lcom/google/googlenav/ui/wizard/ev;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object p2, v2

    move-object v2, v4

    :goto_1
    if-eqz v5, :cond_2

    iget-object v3, v2, Lcom/google/googlenav/ui/wizard/ev;->a:Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/googlenav/ui/aV;->v:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lcom/google/googlenav/ui/wizard/ev;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/es;->a:Lcom/google/googlenav/ui/wizard/eq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/eq;->a(Lcom/google/googlenav/ui/wizard/eq;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040170

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1003ee

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/ev;

    move-object v2, v0

    goto :goto_1

    :cond_2
    check-cast v1, LaQ/c;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, LaQ/c;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v5}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, LaQ/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, LaQ/c;->c()Ljava/lang/String;

    move-result-object v0

    const-string v5, "\n"

    invoke-static {v0, v5}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v0, v3

    :goto_3
    if-ge v0, v6, :cond_3

    aget-object v3, v5, v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/google/googlenav/ui/aV;->v:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v7}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, v2, Lcom/google/googlenav/ui/wizard/ev;->a:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/util/List;)V

    iget-object v0, v2, Lcom/google/googlenav/ui/wizard/ev;->a:Landroid/widget/TextView;

    new-instance v2, Lcom/google/googlenav/ui/wizard/et;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/wizard/et;-><init>(Lcom/google/googlenav/ui/wizard/es;LaQ/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
