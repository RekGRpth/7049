.class Lcom/google/googlenav/ui/wizard/fB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/af;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fq;

.field private b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/ui/wizard/fA;

.field private d:Lcom/google/googlenav/ui/wizard/fH;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/fq;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/fA;Lcom/google/googlenav/ui/wizard/fH;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/fB;->d:Lcom/google/googlenav/ui/wizard/fH;

    return-void
.end method

.method private a(ZZ)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->as()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->a()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    return-void

    :cond_1
    if-eqz p2, :cond_2

    const/16 v0, 0x38b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fB;->d:Lcom/google/googlenav/ui/wizard/fH;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/wizard/fq;->b(Lcom/google/googlenav/ui/wizard/fq;Z)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x36e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/16 v0, 0x381

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/fB;->a(ZZ)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/wizard/fB;->a(ZZ)V

    return-void
.end method
