.class public Lcom/google/googlenav/ui/wizard/fM;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private A:Z

.field private a:Landroid/view/LayoutInflater;

.field private b:Landroid/view/inputmethod/InputMethodManager;

.field private c:Lcom/google/googlenav/ui/wizard/fV;

.field private d:Landroid/widget/ArrayAdapter;

.field private l:Landroid/app/Dialog;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/view/View;

.field private q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/ImageButton;

.field private u:Landroid/widget/ListView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Ljava/lang/String;

.field private final z:Lcom/google/googlenav/ui/view/android/aB;


# direct methods
.method public constructor <init>()V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/android/aB;Z)V
    .locals 3

    const v0, 0x7f0f0018

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/fM;->A:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->w_()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/android/aB;->b()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/CharSequence;II)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fM;)Lcom/google/googlenav/ui/view/android/aB;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fM;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fM;->y:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/fU;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/fU;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f1001b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1001b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aB;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/fN;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/fN;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    const v0, 0x7f100039

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aB;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1000a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->n:Landroid/view/View;

    const v0, 0x7f1000a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const v0, 0x7f1000a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fO;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fO;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fM;)Lcom/google/googlenav/ui/wizard/fV;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->c:Lcom/google/googlenav/ui/wizard/fV;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f1000b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->r:Landroid/view/View;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f1003db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    const/16 v1, 0x4ff

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fP;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fP;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fQ;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fQ;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f1003dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->t:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->t:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fR;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fR;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f100036

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    const v0, 0x7f1000b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->v:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fM;->o()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fM;->v()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/fW;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/wizard/fW;-><init>(Lcom/google/googlenav/ui/wizard/fM;Landroid/content/Context;Lcom/google/googlenav/ui/wizard/fN;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fS;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fS;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fM;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/fM;)Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/fM;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->b:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/fM;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/fM;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->a:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private o()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04000a

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->w:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->w:Landroid/view/View;

    const v1, 0x7f100034

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x25

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->w:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->w:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/wizard/fT;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fT;-><init>(Lcom/google/googlenav/ui/wizard/fM;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->a:Landroid/view/LayoutInflater;

    const v1, 0x1090004

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->x:Landroid/view/View;

    return-void
.end method

.method private v()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->x:Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method


# virtual methods
.method public I_()V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aB;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aB;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aB;->e()LaA/f;

    move-result-object v1

    new-array v2, v5, [I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/view/android/aB;->d()I

    move-result v4

    aput v4, v2, v3

    invoke-virtual {p0, v5, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/fM;->a(ZILaA/f;[I)V

    :cond_0
    return-void
.end method

.method protected O_()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method public a(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/fV;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fM;->c:Lcom/google/googlenav/ui/wizard/fV;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fM;->y:Ljava/lang/String;

    const/16 v0, 0x4f5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/fM;->a(I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    new-instance v2, Lcom/google/googlenav/h;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1001e5

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->c:Lcom/google/googlenav/ui/wizard/fV;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fV;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/view/Menu;)Z
    .locals 2

    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fM;->v:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->w:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->a:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04001f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/fM;->a(Landroid/view/View;)V

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/fM;->b(Landroid/view/View;)V

    const v0, 0x7f100089

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/ModalOverlay;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    sget-object v2, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    const/16 v3, 0x4f5

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fM;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aB;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->l:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->l:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->l:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->c:Lcom/google/googlenav/ui/wizard/fV;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fV;->b()V

    return-void
.end method

.method protected i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->q:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setHidden()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->o:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/fM;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1001e5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x438

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f10049b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fM;->A:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    const/16 v1, 0x24b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fM;->a(Landroid/view/MenuItem;)V

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fM;->z:Lcom/google/googlenav/ui/view/android/aB;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aB;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
