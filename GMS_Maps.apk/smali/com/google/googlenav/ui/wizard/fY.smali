.class public Lcom/google/googlenav/ui/wizard/fY;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/ga;

.field private b:Lcom/google/googlenav/ui/wizard/gc;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    return v0
.end method

.method a(Lcom/google/googlenav/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/wizard/gb;->a(Lcom/google/googlenav/h;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/ga;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public b()V
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/gc;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ga;->a(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/ga;->b(Lcom/google/googlenav/ui/wizard/ga;)LaH/h;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/wizard/fZ;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/wizard/fZ;-><init>(Lcom/google/googlenav/ui/wizard/fY;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/gc;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;LaH/h;Lcom/google/googlenav/ui/wizard/gh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    new-instance v0, Lcom/google/googlenav/ui/wizard/fM;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ga;->c(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/view/android/aB;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ga;->d(Lcom/google/googlenav/ui/wizard/ga;)Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/wizard/fM;-><init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/android/aB;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gc;->a()Lcom/google/googlenav/ui/wizard/fV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->a(Lcom/google/googlenav/ui/wizard/fV;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/gc;->a(Lcom/google/googlenav/ui/wizard/fM;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ga;->b(Lcom/google/googlenav/ui/wizard/ga;)LaH/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gc;->a(LaH/h;)V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gc;->a(Lcom/google/googlenav/ui/wizard/fM;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->a(Lcom/google/googlenav/ui/wizard/fV;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->a(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    :cond_2
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->j()V

    return-void
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gb;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gc;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->e()V

    :cond_1
    return-void
.end method
