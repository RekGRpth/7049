.class public Lcom/google/googlenav/ui/wizard/fe;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private volatile a:I

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    new-instance v0, Lcom/google/googlenav/ui/wizard/ff;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ff;-><init>(Lcom/google/googlenav/ui/wizard/fe;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->b:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/googlenav/ui/wizard/fg;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fg;-><init>(Lcom/google/googlenav/ui/wizard/fe;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fe;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fe;I)I
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fe;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fe;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->c:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fe;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    return v0
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    iput p1, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    const-string v0, "OFFER_SETTINGS_PREFERENCE"

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f100030

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/fh;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fh;-><init>(Lcom/google/googlenav/ui/wizard/fe;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    const/16 v0, 0x328

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(II)Landroid/widget/Toast;

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/wizard/fj;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fj;-><init>(Lcom/google/googlenav/ui/wizard/fe;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fe;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/fe;->a:I

    return v0
.end method

.method protected f()V
    .locals 3

    const-string v0, "OFFER_SETTINGS_PREFERENCE"

    const/4 v1, -0x1

    new-instance v2, Lcom/google/googlenav/ui/wizard/fi;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/fi;-><init>(Lcom/google/googlenav/ui/wizard/fe;)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    return-void
.end method
