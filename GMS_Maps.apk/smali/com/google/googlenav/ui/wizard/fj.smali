.class public Lcom/google/googlenav/ui/wizard/fj;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fe;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/fe;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fj;->a:Lcom/google/googlenav/ui/wizard/fe;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    return-void
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fj;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040109

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f100199

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x325

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->C:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1002f9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/16 v1, 0x327

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->W:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fj;->a:Lcom/google/googlenav/ui/wizard/fe;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fe;->a(Lcom/google/googlenav/ui/wizard/fe;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f1002fa

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const/16 v3, 0x326

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->W:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fj;->a:Lcom/google/googlenav/ui/wizard/fe;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/fe;->b(Lcom/google/googlenav/ui/wizard/fe;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fj;->a:Lcom/google/googlenav/ui/wizard/fe;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/fe;->c(Lcom/google/googlenav/ui/wizard/fe;)I

    move-result v3

    if-ne v3, v5, :cond_0

    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fj;->a:Lcom/google/googlenav/ui/wizard/fe;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/fe;->a(Landroid/view/View;)V

    return-object v2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x324

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
