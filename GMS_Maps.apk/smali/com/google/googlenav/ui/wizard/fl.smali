.class public Lcom/google/googlenav/ui/wizard/fl;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/ui/view/dialog/bk;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fl;->j()V

    return-void
.end method

.method protected b()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/fm;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fm;-><init>(Lcom/google/googlenav/ui/wizard/fl;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fl;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f100034

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    return-void
.end method
