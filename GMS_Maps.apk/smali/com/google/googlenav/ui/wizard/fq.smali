.class public Lcom/google/googlenav/ui/wizard/fq;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static final c:I


# instance fields
.field protected a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/ui/wizard/fA;

.field private final i:Lcom/google/googlenav/J;

.field private final j:Lcom/google/googlenav/android/BaseMapsActivity;

.field private k:Lcom/google/googlenav/ai;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Lcom/google/googlenav/ui/wizard/fH;

.field private q:Z

.field private r:Landroid/view/LayoutInflater;

.field private s:Landroid/app/AlertDialog;

.field private final t:LaM/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x400

    :goto_0
    sput v0, Lcom/google/googlenav/ui/wizard/fq;->c:I

    return-void

    :cond_0
    const/16 v0, 0x200

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;Lcom/google/googlenav/android/BaseMapsActivity;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/fr;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fr;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fq;->i:Lcom/google/googlenav/J;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    new-instance v0, Lcom/google/googlenav/ui/wizard/fs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fs;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    return-void
.end method

.method private A()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bm;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x65

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->I()Ljava/lang/String;

    move-result-object v1

    const-string v2, "o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/br;->b:Lcom/google/googlenav/br;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/br;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->C()V

    goto :goto_0
.end method

.method private B()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fC;->h()V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    const/16 v1, 0x36e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    goto :goto_0
.end method

.method private C()V
    .locals 6

    const/16 v5, 0x36e

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ay;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    sget v3, Lcom/google/googlenav/ui/wizard/fq;->c:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbm/a;->a(Ljava/lang/String;IZ)Lam/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ay;-><init>(Lam/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Lcom/google/googlenav/ay;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v1, Lcom/google/googlenav/ui/wizard/ft;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/ft;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    goto :goto_0
.end method

.method private static D()Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PHOTO_UPLOAD_LEGAL_INFO_DISMISS"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()V
    .locals 4

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "support.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/gmm/bin/answer.py"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hl"

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "answer"

    const-string v3, "1650741"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    return-void
.end method

.method private F()V
    .locals 10

    const/4 v1, 0x1

    const/16 v0, 0x65

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->I()Ljava/lang/String;

    move-result-object v2

    const-string v3, "s"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ae;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    new-instance v6, Lcom/google/googlenav/ui/wizard/fB;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/googlenav/ui/wizard/fB;-><init>(Lcom/google/googlenav/ui/wizard/fq;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/fA;Lcom/google/googlenav/ui/wizard/fH;)V

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/google/googlenav/ae;-><init>(Lcom/google/googlenav/ai;ILjava/lang/String;Lcom/google/googlenav/af;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    const/16 v1, 0x380

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/16 v1, 0x384

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x383

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private H()V
    .locals 6

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->G()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x385

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x35d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x42f

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fw;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fw;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fv;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fv;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fu;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fu;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fx;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fx;-><init>(Lcom/google/googlenav/ui/wizard/fq;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private I()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_0

    const-string v0, "wp"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "w"

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fq;->r:Landroid/view/LayoutInflater;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->C()V

    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->invalidateOptionsMenu()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f10003f

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f100031

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fq;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fq;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->i:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fq;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fH;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/fq;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    return v0
.end method

.method public static g()V
    .locals 3

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "PHOTO_UPLOAD_LEGAL_INFO_DISMISS"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/fq;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->r:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/fq;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    return v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->F()V

    return-void
.end method

.method static synthetic l(Lcom/google/googlenav/ui/wizard/fq;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->A()V

    return-void
.end method

.method static synthetic n(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->E()V

    return-void
.end method

.method static synthetic z()I
    .locals 1

    sget v0, Lcom/google/googlenav/ui/wizard/fq;->c:I

    return v0
.end method


# virtual methods
.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    new-instance v0, Lcom/google/googlenav/ui/wizard/fC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fC;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    :cond_1
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->j()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/wizard/fq;->D()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->H()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->A()V

    goto :goto_0
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    return-void
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/fC;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->i()V

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled()Z

    move-result v0

    return v0
.end method

.method public i()V
    .locals 4

    const/16 v0, 0x35d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x370

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/aL;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/fz;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/fz;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected y()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    check-cast v0, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    return v0
.end method
