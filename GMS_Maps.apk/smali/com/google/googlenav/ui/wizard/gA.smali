.class public Lcom/google/googlenav/ui/wizard/gA;
.super Landroid/database/MatrixCursor;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/google/googlenav/ui/wizard/gF;

.field private final c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "label"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "icon_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "icon_id_chip"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "icon_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sort"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/ui/wizard/gA;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/gA;->a:[Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->c:Ljava/util/Map;

    return-void
.end method

.method static a(ILcom/google/googlenav/friend/aD;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(ILcom/google/googlenav/friend/aD;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v0

    const/16 v0, 0x9

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->e()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v1, 0x4

    sget-object v0, Lcom/google/googlenav/friend/aD;->b:Lcom/google/common/collect/Q;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p2, v1

    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x7

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->h()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/16 v0, 0x8

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/wizard/gA;->a(ILcom/google/googlenav/friend/aD;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    return-object p2
.end method

.method private d()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/gF;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gF;-><init>(Lcom/google/googlenav/ui/wizard/gA;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->b:Lcom/google/googlenav/ui/wizard/gF;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/gA;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;Lcom/google/googlenav/ui/wizard/gB;)V
    .locals 6

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aD;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gA;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gA;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3, v0, v1}, Lcom/google/googlenav/ui/wizard/gA;->a(ILcom/google/googlenav/friend/aD;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-super {p0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    invoke-interface {p2, v0, v3}, Lcom/google/googlenav/ui/wizard/gB;->a(Lcom/google/googlenav/friend/aD;I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gA;->d()V

    return-void
.end method

.method public addRow(Ljava/lang/Iterable;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must use PlusoneAudienceCursor.addRows"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addRow([Ljava/lang/Object;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must use PlusoneAudienceCursor.addRows"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Lcom/google/googlenav/friend/aG;
    .locals 2

    sget-object v0, Lcom/google/googlenav/friend/aD;->a:Lcom/google/common/collect/Q;

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/gA;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aG;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/friend/aD;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->c:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/gA;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aD;

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->b:Lcom/google/googlenav/ui/wizard/gF;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gF;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->b:Lcom/google/googlenav/ui/wizard/gF;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gF;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gA;->b:Lcom/google/googlenav/ui/wizard/gF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gF;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
