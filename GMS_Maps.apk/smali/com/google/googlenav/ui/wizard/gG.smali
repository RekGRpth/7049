.class public Lcom/google/googlenav/ui/wizard/gG;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/gJ;

.field private b:Lcom/google/googlenav/ui/wizard/gK;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gO;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/gH;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/gH;-><init>(Lcom/google/googlenav/ui/wizard/gG;Lcom/google/googlenav/ui/wizard/gI;)V

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    return v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/gJ;)V
    .locals 1

    const-string v0, "o"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->g(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public b()V
    .locals 5

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    new-instance v0, Lcom/google/googlenav/ui/wizard/gK;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->b()LaB/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/gG;->a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gO;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/gJ;->c()Z

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/gJ;->d()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/gK;-><init>(LaB/s;Lcom/google/googlenav/ui/wizard/gO;ZZ)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gU;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->h()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gG;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/googlenav/ui/wizard/gU;-><init>(Ljava/lang/String;ILaB/s;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/gU;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gK;->a()Lcom/google/googlenav/ui/wizard/gM;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gU;->a(Lcom/google/googlenav/ui/wizard/gM;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/gU;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gU;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->f()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gK;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gU;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->j()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gI;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->a()V

    const-string v0, "d"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->g(Ljava/lang/String;)V

    return-void
.end method
