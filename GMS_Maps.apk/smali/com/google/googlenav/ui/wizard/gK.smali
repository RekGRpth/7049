.class public Lcom/google/googlenav/ui/wizard/gK;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private final b:Lcom/google/googlenav/ui/wizard/gO;

.field private c:Lcom/google/googlenav/ui/wizard/gM;

.field private d:Lcom/google/googlenav/ui/wizard/gN;

.field private e:Lcom/google/googlenav/ui/wizard/gU;

.field private f:Lcom/google/googlenav/ui/wizard/gP;

.field private final g:Z


# direct methods
.method public constructor <init>(LaB/s;Lcom/google/googlenav/ui/wizard/gO;ZZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gK;->b:Lcom/google/googlenav/ui/wizard/gO;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/gK;->g:Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gM;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gM;-><init>(Lcom/google/googlenav/ui/wizard/gK;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->c:Lcom/google/googlenav/ui/wizard/gM;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gN;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/gN;-><init>(Lcom/google/googlenav/ui/wizard/gK;Lcom/google/googlenav/ui/wizard/gL;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->d:Lcom/google/googlenav/ui/wizard/gN;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gP;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/googlenav/ui/wizard/gP;-><init>(LaB/s;ZZ)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->f:Lcom/google/googlenav/ui/wizard/gP;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->f:Lcom/google/googlenav/ui/wizard/gP;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gK;->d:Lcom/google/googlenav/ui/wizard/gN;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gP;->a(Lcom/google/googlenav/ui/wizard/gT;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gU;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->e:Lcom/google/googlenav/ui/wizard/gU;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gK;)Lcom/google/googlenav/ui/wizard/gO;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->b:Lcom/google/googlenav/ui/wizard/gO;

    return-object v0
.end method

.method static synthetic b()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/wizard/gK;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static c()Ljava/util/List;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/aD;->i()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/friend/aD;->j()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/friend/aD;->i()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gK;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gK;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gK;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/gM;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->c:Lcom/google/googlenav/ui/wizard/gM;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gU;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gK;->e:Lcom/google/googlenav/ui/wizard/gU;

    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 1

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gK;->a:Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->e:Lcom/google/googlenav/ui/wizard/gU;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gU;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->f:Lcom/google/googlenav/ui/wizard/gP;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gP;->a()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gK;->f:Lcom/google/googlenav/ui/wizard/gP;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gP;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method
