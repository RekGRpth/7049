.class public Lcom/google/googlenav/ui/wizard/gP;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/gT;

.field private final b:LaB/s;

.field private final c:Z

.field private d:Z


# direct methods
.method public constructor <init>(LaB/s;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gP;->b:LaB/s;

    iput-boolean p2, p0, Lcom/google/googlenav/ui/wizard/gP;->c:Z

    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/gP;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gP;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gP;->b(Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gP;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gP;->c:Z

    return v0
.end method

.method private b(Ljava/util/Collection;)V
    .locals 5

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aD;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gP;->b:LaB/s;

    invoke-virtual {v4}, LaB/s;->c()I

    move-result v4

    if-lt v3, v4, :cond_3

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gP;->b:LaB/s;

    new-instance v2, Lcom/google/googlenav/ui/wizard/gR;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/gR;-><init>(Lcom/google/googlenav/ui/wizard/gP;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->h()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/googlenav/friend/aF;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/friend/aF;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gP;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gP;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gP;)Lcom/google/googlenav/ui/wizard/gT;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gP;->a:Lcom/google/googlenav/ui/wizard/gT;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    new-instance v0, Lcom/google/googlenav/friend/N;

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/gP;->c:Z

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gP;->d:Z

    new-instance v3, Lcom/google/googlenav/ui/wizard/gS;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/googlenav/ui/wizard/gS;-><init>(Lcom/google/googlenav/ui/wizard/gP;Lcom/google/googlenav/ui/wizard/gQ;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/friend/N;-><init>(ZZLcom/google/googlenav/friend/O;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gT;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gP;->a:Lcom/google/googlenav/ui/wizard/gT;

    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gP;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gP;->d:Z

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/gQ;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gQ;-><init>(Lcom/google/googlenav/ui/wizard/gP;)V

    invoke-static {p1, v0}, Lcom/google/common/collect/S;->a(Ljava/util/Collection;Lcom/google/common/base/K;)Ljava/util/Collection;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Ljava/util/ArrayList;

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gP;->b(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gP;->a:Lcom/google/googlenav/ui/wizard/gT;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/gT;->a(Ljava/util/Collection;)V

    return-void

    :cond_1
    check-cast v0, Ljava/util/ArrayList;

    goto :goto_1

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method
