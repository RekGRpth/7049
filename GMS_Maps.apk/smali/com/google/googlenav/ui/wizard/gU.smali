.class public Lcom/google/googlenav/ui/wizard/gU;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:LaB/s;

.field private b:Lcom/google/googlenav/ui/wizard/gM;

.field private c:Lcom/google/googlenav/ui/view/android/ModalOverlay;

.field private d:Landroid/widget/ListView;

.field private l:Landroid/view/ViewGroup;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private final o:Ljava/lang/String;

.field private final p:I

.field private q:Lcom/google/googlenav/ui/wizard/gC;

.field private r:Landroid/widget/EditText;

.field private s:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILaB/s;Lcom/google/googlenav/ui/e;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, p4, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gU;->o:Ljava/lang/String;

    iput p2, p0, Lcom/google/googlenav/ui/wizard/gU;->p:I

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gU;->a:LaB/s;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gU;)Lcom/google/googlenav/ui/wizard/gM;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->b:Lcom/google/googlenav/ui/wizard/gM;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gU;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->s:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gU;)Lcom/google/googlenav/ui/wizard/gC;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gU;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->d:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->c:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->l:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gM;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gU;->b:Lcom/google/googlenav/ui/wizard/gM;

    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 5

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {p2}, Lcom/google/common/collect/dA;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->s:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/gX;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/gX;-><init>(Lcom/google/googlenav/ui/wizard/gU;Ljava/util/List;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/gA;

    invoke-direct {v2}, Lcom/google/googlenav/ui/wizard/gA;-><init>()V

    invoke-virtual {v2, p1, v1}, Lcom/google/googlenav/ui/wizard/gA;->a(Ljava/util/Collection;Lcom/google/googlenav/ui/wizard/gB;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/gC;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gU;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gU;->a:LaB/s;

    invoke-direct {v1, v3, v2, v4}, Lcom/google/googlenav/ui/wizard/gC;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/wizard/gA;LaB/s;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    invoke-virtual {v1, p2}, Lcom/google/googlenav/ui/wizard/gC;->a(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gU;->d:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gU;->d:Landroid/widget/ListView;

    new-instance v2, Lcom/google/googlenav/ui/wizard/gY;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/gY;-><init>(Lcom/google/googlenav/ui/wizard/gU;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gU;->d:Landroid/widget/ListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gC;->notifyDataSetChanged()V

    return-void
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->c:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setState(Lcom/google/googlenav/ui/view/android/bc;I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->l:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gU;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012e

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->o:Ljava/lang/String;

    const v2, 0x7f100039

    iget v3, p0, Lcom/google/googlenav/ui/wizard/gU;->p:I

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/googlenav/ui/wizard/gU;->a(Ljava/lang/CharSequence;II)V

    const v0, 0x7f100089

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/ModalOverlay;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->c:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    const v0, 0x7f10033e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->d:Landroid/widget/ListView;

    const v0, 0x7f10033d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->r:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->r:Landroid/widget/EditText;

    new-instance v2, Lcom/google/googlenav/ui/wizard/gZ;

    invoke-direct {v2, p0, v4}, Lcom/google/googlenav/ui/wizard/gZ;-><init>(Lcom/google/googlenav/ui/wizard/gU;Lcom/google/googlenav/ui/wizard/gV;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->r:Landroid/widget/EditText;

    const/16 v2, 0x5e7

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gU;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    const v0, 0x7f10002e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->l:Landroid/view/ViewGroup;

    const v0, 0x7f10033f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->m:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->m:Landroid/widget/Button;

    const/16 v2, 0x35c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->m:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/wizard/gV;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/gV;-><init>(Lcom/google/googlenav/ui/wizard/gU;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100172

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->n:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->n:Landroid/widget/Button;

    const/16 v2, 0x69

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->n:Landroid/widget/Button;

    new-instance v2, Lcom/google/googlenav/ui/wizard/gW;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/gW;-><init>(Lcom/google/googlenav/ui/wizard/gU;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gU;->h()V

    return-object v1
.end method

.method public h()V
    .locals 1

    const/16 v0, 0x3d0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/gU;->a(I)V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->c:Lcom/google/googlenav/ui/view/android/ModalOverlay;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ModalOverlay;->setHidden()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->l:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gU;->q:Lcom/google/googlenav/ui/wizard/gC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gC;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
