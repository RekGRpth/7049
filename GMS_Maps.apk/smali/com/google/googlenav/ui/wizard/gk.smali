.class public Lcom/google/googlenav/ui/wizard/gk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaM/h;
.implements LaW/r;
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Lcom/google/googlenav/friend/bf;
.implements Lcom/google/googlenav/ui/view/c;
.implements Lcom/google/googlenav/ui/wizard/E;
.implements Lcom/google/googlenav/ui/wizard/dy;
.implements Lcom/google/googlenav/ui/wizard/gj;


# static fields
.field private static M:Ljava/util/List;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private A:LaN/H;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/Object;

.field private E:Lcom/google/googlenav/ui/wizard/gy;

.field private F:Z

.field private G:J

.field private H:Lcom/google/googlenav/bf;

.field private I:Z

.field private J:Lcom/google/googlenav/ui/view/android/S;

.field private K:Lcom/google/googlenav/ui/view/android/S;

.field private L:LaW/g;

.field private final N:Lcom/google/googlenav/actionbar/b;

.field private O:Lcom/google/googlenav/ui/wizard/dy;

.field protected a:Lcom/google/googlenav/a;

.field private i:I

.field private final j:Lcom/google/googlenav/J;

.field private final k:LaH/m;

.field private final l:Lcom/google/googlenav/ui/ak;

.field private final m:LaN/u;

.field private n:Lbf/am;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/googlenav/ai;

.field private t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private u:Ljava/util/List;

.field private v:Ljava/util/List;

.field private w:Lcom/google/googlenav/aV;

.field private x:Ljava/util/List;

.field private y:Z

.field private z:LaN/B;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput v2, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gw;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gw;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->O:Lcom/google/googlenav/ui/wizard/dy;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gl;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gl;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->N:Lcom/google/googlenav/actionbar/b;

    invoke-static {}, Lcom/google/googlenav/aV;->a()Lcom/google/googlenav/aV;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->w:Lcom/google/googlenav/aV;

    return-void
.end method

.method public static C()V
    .locals 0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->i()V

    return-void
.end method

.method public static D()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static E()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    return-object v0
.end method

.method private F()Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v3, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-eq v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    iget-wide v4, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/32 v6, 0xdbba0

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-nez v2, :cond_6

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    invoke-virtual {v3}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v4}, LaN/u;->f()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    invoke-virtual {v2, v3}, LaN/B;->a(LaN/B;)J

    move-result-wide v2

    const-wide/16 v4, 0x9c4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method private G()V
    .locals 3

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    return-void
.end method

.method private H()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->p()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->o()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->K()V

    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->y()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    goto :goto_1
.end method

.method private I()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LaW/g;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private J()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->W()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LaW/g;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private K()V
    .locals 13

    new-instance v0, LaW/g;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->W()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v8

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v11

    iget-object v12, p0, Lcom/google/googlenav/ui/wizard/gk;->w:Lcom/google/googlenav/aV;

    move-object v9, p0

    move-object v10, p0

    invoke-direct/range {v0 .. v12}, LaW/g;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZZLcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/bu;Lcom/google/googlenav/aV;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, LaW/g;->show()V

    return-void

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    if-eqz v0, :cond_0

    new-instance v0, LaW/F;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bU()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LaW/F;-><init>(Lcom/google/googlenav/ui/wizard/gj;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private M()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gv;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gv;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    new-instance v1, Lcom/google/googlenav/f;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method private N()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    :cond_0
    return-void
.end method

.method private O()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->R()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->S()V

    goto :goto_0
.end method

.method private R()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->U()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    invoke-direct {p0, v0, v1, v1}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private S()V
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->a:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static T()Ljava/util/List;
    .locals 3

    const/16 v2, 0xa

    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x2

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x7

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x6

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ag()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    return-object v0
.end method

.method private U()LaN/B;
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LaH/h;->getTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    invoke-virtual {v0, v3}, LaN/B;->a(LaN/B;)J

    move-result-wide v3

    const-wide/16 v5, 0x9c4

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    iget-wide v3, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    sub-long v3, v1, v3

    const-wide/32 v5, 0x493e0

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    :cond_0
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    iput-wide v1, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aT;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/16 v0, 0x3bc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private X()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->h()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    return-void
.end method

.method private Y()Lcom/google/googlenav/layer/m;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v1, "lmq:*"

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v1, "*"

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v2, "gr"

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v2, "geo:coupons"

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x12

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;)Lbf/am;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/bd;

    const-string v1, ""

    invoke-direct {v0, v5, v1, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bc;

    const/16 v2, 0x130

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    aput-object p1, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v6, v2, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x5f8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/bg;->k(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/bg;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/bg;)V

    return-object v0
.end method

.method static a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)Lcom/google/googlenav/ui/wizard/gk;
    .locals 6

    new-instance v0, Lcom/google/googlenav/ui/wizard/gk;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/gk;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)V

    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;
    .locals 8

    const/4 v7, 0x7

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    new-instance v4, LaW/S;

    const-string v5, "pl"

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v4, v0, v5, v6}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v3, v4}, LaW/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaW/S;)LaW/R;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    new-instance v3, LaW/R;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    new-instance v4, LaW/S;

    const/4 v5, -0x1

    const-string v6, "pl"

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v0, v4}, LaW/R;-><init>(Ljava/lang/String;LaW/S;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method private a(LaN/B;LaN/H;)V
    .locals 3

    new-instance v0, LaW/q;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/gk;->T()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, LaW/q;-><init>(Ljava/util/List;LaN/B;LaW/r;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1, p2}, LaN/u;->a(LaN/H;)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->b(LaN/H;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaW/q;->b(II)V

    :cond_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(LaN/B;LaN/H;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    if-eqz v0, :cond_1

    const-string v0, "n"

    :goto_0
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    const/16 v1, 0x57

    const-string v2, "stp"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "e="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/bg;

    invoke-direct {v1}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v2

    invoke-virtual {v2}, LaM/f;->k()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/friend/bg;->d(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/friend/bg;->e(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->a(LaN/H;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->d(I)Lcom/google/googlenav/friend/bg;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->b(LaN/H;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->e(I)Lcom/google/googlenav/friend/bg;

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/H;)V

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v1

    invoke-virtual {v2, v1}, Law/h;->c(Law/g;)V

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void

    :cond_1
    const-string v0, "a"

    goto/16 :goto_0
.end method

.method private a(LaN/H;)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->r()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/aZ;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    :cond_0
    const-string v0, "Not available yet"

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->an()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    new-instance v3, LaW/S;

    const-string v4, "pl"

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v3, v0, v4, v5}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    new-instance v4, LaW/R;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v2, v5, v3}, LaW/R;-><init>(Ljava/lang/String;Ljava/lang/String;ILaW/S;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/googlenav/bg;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v3, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v3, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->V()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->d(Ljava/lang/String;)Lcom/google/googlenav/bg;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v3

    if-eqz v3, :cond_2

    move v0, v1

    :goto_1
    move-object v1, v3

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    :cond_0
    :goto_3
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xdbba0

    cmp-long v3, v3, v5

    if-gez v3, :cond_4

    :goto_4
    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->c:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;LaN/B;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gk;->b(LaN/B;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/gz;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/wizard/gz;->a(Lcom/google/googlenav/ai;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/gq;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/gq;-><init>(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ui/wizard/gz;)V

    new-instance v2, Lcom/google/googlenav/f;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->c:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    invoke-virtual {p2}, LaN/H;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/H;)V

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/aZ;)V

    invoke-direct {p0, v0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0, p2, p1}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gk;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ai;)Lcom/google/googlenav/ai;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    return-object p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->d(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->f(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->g(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->a(LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/bg;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/bg;)V

    return-object v0
.end method

.method private b(LaN/B;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    new-instance v1, Lcom/google/googlenav/bg;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    invoke-direct {v1, v2}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->d(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/bg;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/gx;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gx;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    :goto_0
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    return-void

    :cond_0
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method private b(Ljava/util/List;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaW/g;->a(Ljava/util/List;Lcom/google/googlenav/bu;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gk;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/ai;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, v0}, LaN/u;->a(LaN/H;)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v3, v0}, LaN/u;->b(LaN/H;)I

    move-result v3

    invoke-static {v1, v0, v2, v3}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Lcom/google/googlenav/aZ;)Lbf/x;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->L()V

    return-void
.end method

.method private c(Lcom/google/googlenav/bg;)V
    .locals 9

    const/16 v8, 0x10

    const/4 v6, 0x0

    const/16 v5, 0x57

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    invoke-virtual {v7}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-string v2, "lkl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/bf;->f:LaN/H;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_6

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v0, "f"

    const-string v1, "v"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v1, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :cond_2
    :goto_3
    return-void

    :cond_3
    const-string v0, "nlkl"

    const-string v1, ""

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_1

    const-string v0, "f"

    const-string v1, "h"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    const-string v0, "f"

    const-string v1, "w"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x292

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    if-eqz v7, :cond_2

    invoke-virtual {v7}, LaH/h;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/gm;

    invoke-direct {v2, p0, v7}, Lcom/google/googlenav/ui/wizard/gm;-><init>(Lcom/google/googlenav/ui/wizard/gk;LaH/h;)V

    invoke-direct {v0, v1, v2}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    invoke-virtual {v0}, Las/d;->g()V

    goto :goto_3
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gk;)LaW/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    return-object v0
.end method

.method private d(Lcom/google/googlenav/bg;)V
    .locals 6

    const/4 v5, 0x5

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/bc;

    const-string v3, ""

    const/4 v4, 0x0

    invoke-direct {v2, v5, v3, v4}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "*"

    invoke-virtual {p1, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/gk;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->G()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/gk;)LaH/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/gk;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->R()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/gk;)LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/gk;)Lcom/google/googlenav/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/gk;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/go;

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bm;->h()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/go;-><init>(Lcom/google/googlenav/ui/wizard/gk;Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method public B()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    return v0
.end method

.method public C_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v1, v0}, LaW/g;->b(Lcom/google/googlenav/bu;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v1, v0}, LaW/g;->a(Lcom/google/googlenav/bu;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Q()V

    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 0

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public S_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public T_()V
    .locals 0

    return-void
.end method

.method public U_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/E;)V

    return-void
.end method

.method public V_()V
    .locals 2

    const/16 v0, 0x57

    const-string v1, "ph"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gs;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    return-void
.end method

.method public W_()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    new-instance v1, Lcom/google/googlenav/h;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-direct {v1, v2}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    return-void
.end method

.method public X_()V
    .locals 2

    const/16 v0, 0x57

    const-string v1, "p"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gu;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gu;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    return-void
.end method

.method public Y_()V
    .locals 6

    const/16 v5, 0xc8

    const/4 v4, 0x1

    const/16 v0, 0x57

    const-string v1, "d"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v2}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0x50f

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0x61d

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->e(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public Z_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    new-instance v0, LaW/F;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bU()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LaW/F;-><init>(Lcom/google/googlenav/ui/wizard/gj;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->show()V

    :cond_0
    return-void
.end method

.method public a(LaN/B;)I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(LaN/B;)I

    move-result v0

    goto :goto_0
.end method

.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    goto :goto_0
.end method

.method public a(ILaH/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->X()V

    return-void
.end method

.method public a(J)V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/gp;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/gp;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(LaW/o;)V
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p1}, LaW/o;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, LaW/o;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    invoke-virtual {p1}, LaW/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, LaW/o;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LaW/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LaW/o;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x58

    const-string v3, "s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "t=l"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "ub=f"

    aput-object v6, v4, v5

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->d(Lcom/google/googlenav/bg;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Y()Lcom/google/googlenav/layer/m;

    move-result-object v3

    invoke-interface {v2, v1, v0, v3}, Lcom/google/googlenav/J;->a(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lbf/aJ;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->f(Lbf/i;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/bk;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/J;->a(Ljava/util/List;Lcom/google/googlenav/bf;)Lbf/bk;

    move-result-object v0

    goto :goto_2
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->b()V

    const/4 v0, 0x4

    invoke-static {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {p3, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->Z_()V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->M()V

    return-void
.end method

.method public a(Lbf/am;I)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    const/16 v0, 0x57

    const-string v1, "ac"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    iput p2, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->j()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    goto :goto_0
.end method

.method public a(Lbf/i;)V
    .locals 2

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    const/16 v0, 0x57

    const-string v1, "rpc"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    const/4 v2, 0x0

    const-string v3, "plrp"

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 6

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->a:Lcom/google/googlenav/a;

    invoke-static {v3}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/a;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->a:Lcom/google/googlenav/a;

    invoke-virtual {v1}, Lcom/google/googlenav/a;->h()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    :goto_0
    if-eqz v4, :cond_1

    move v1, v2

    move-object v3, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v5, 0x2

    invoke-static {v0, v5}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v3, v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v3, v0

    :cond_2
    const/4 v0, 0x3

    invoke-static {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v3, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    if-nez v0, :cond_3

    const/4 v0, 0x4

    invoke-static {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    :cond_3
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->M()V

    :goto_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    return-void

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    goto :goto_2

    :cond_5
    move-object v4, v1

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/F;)V
    .locals 3

    const/16 v2, 0x57

    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string v0, "u"

    iget-object v1, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "c"

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    if-eqz p3, :cond_0

    const/16 v1, 0x57

    const-string v2, "t"

    invoke-virtual {p3}, LaW/R;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/bg;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "t="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v5, LaW/o;

    invoke-direct {v5, v0, p0}, LaW/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/gj;)V

    invoke-virtual {v5}, LaW/o;->e()LaW/w;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v5}, LaW/o;->d()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/util/List;)V

    if-eqz v1, :cond_2

    const/16 v0, 0x6f

    const-string v1, "pi"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->U_()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x70e -> :sswitch_2
        0x9cd -> :sswitch_1
        0x9d4 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 3

    check-cast p1, Lcom/google/googlenav/ui/view/a;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gk;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public aa_()V
    .locals 6

    const/4 v5, 0x0

    const/16 v0, 0xc

    const/16 v1, 0x57

    const-string v2, "sloc"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/16 v4, 0xc8

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->e(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->O:Lcom/google/googlenav/ui/wizard/dy;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method public ab_()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac_()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->l()Lcom/google/googlenav/L;

    move-result-object v1

    iget-object v0, v0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/L;->a(Ljava/lang/String;Lcom/google/googlenav/android/T;)V

    :cond_0
    return-void
.end method

.method public ad_()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/bg;)V

    :cond_0
    return-void
.end method

.method public ae_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    if-nez v0, :cond_0

    const-string v0, "PLACES_NULL_LAYER"

    invoke-static {v0}, Lbm/m;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->G()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    invoke-virtual {v0, v3, v1, v2}, LaW/g;->a(Ljava/lang/String;ZZ)V

    :cond_2
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    :cond_3
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Q()V

    :cond_4
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->h()V

    :cond_5
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->A()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->H()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->show()V

    :cond_6
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->h()LaR/n;

    move-result-object v0

    sget-object v1, LaR/O;->h:LaR/O;

    invoke-interface {v0, v1}, LaR/n;->a(LaR/O;)V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    sget-object v1, LaR/O;->h:LaR/O;

    invoke-interface {v0, v1}, LaR/n;->a(LaR/O;)V

    goto/16 :goto_0
.end method

.method public b(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0, p1}, LaW/g;->a(Lcom/google/googlenav/ai;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/wizard/F;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->H()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->b(LaH/A;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, LaW/g;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->X()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/am;I)V

    return-void
.end method

.method public e()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/util/List;)V

    return-void
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->y()V

    return-void
.end method

.method public h()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->N()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->a()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    goto :goto_0
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    return-void
.end method

.method public k()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->j()V

    const/16 v0, 0xa

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 2

    const/16 v0, 0x57

    const-string v1, "r"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gr;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gr;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 0

    return-void
.end method

.method public q()V
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    new-instance v0, Lcom/google/googlenav/ui/wizard/gt;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gt;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    return-void
.end method

.method public w()Lcom/google/googlenav/actionbar/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->N:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public y()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, LaW/g;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->X()V

    :cond_0
    return-void
.end method
