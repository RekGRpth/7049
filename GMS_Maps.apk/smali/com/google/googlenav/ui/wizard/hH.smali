.class public Lcom/google/googlenav/ui/wizard/hH;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dy;


# static fields
.field private static final k:Ljava/util/Set;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/google/googlenav/ui/wizard/hQ;

.field protected c:I

.field protected i:I

.field protected j:Lcom/google/googlenav/ui/wizard/C;

.field private l:Z

.field private m:Lcom/google/googlenav/ui/wizard/hZ;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/hH;->k:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    new-instance v0, Lcom/google/googlenav/ui/wizard/hZ;

    invoke-direct {v0, p1, p0}, Lcom/google/googlenav/ui/wizard/hZ;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    return-void
.end method

.method private B()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[v"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/wizard/hI;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hI;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(LaZ/b;)LaZ/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/hZ;->a(LaZ/a;)V

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    return-void

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    goto :goto_0
.end method

.method private C()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    :cond_1
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->g()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->y()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0

    :pswitch_2
    new-instance v1, Lcom/google/googlenav/ui/wizard/hJ;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hJ;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/google/googlenav/ui/wizard/hR;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hR;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private D()Z
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/hH;->k:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hH;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->D()Z

    move-result v0

    return v0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    return-void
.end method

.method public static z()Ljava/lang/String;
    .locals 4

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/16 v0, 0x457

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v1, 0x458

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public A()LaM/g;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    return-object v0
.end method

.method protected R_()S
    .locals 1

    const/16 v0, 0x4a

    return v0
.end method

.method public S_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public T_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    return-void
.end method

.method public a(Lat/a;)I
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method protected a(LaZ/b;)LaZ/a;
    .locals 13

    new-instance v0, LaZ/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v4, v4, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v5, v5, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v6, v6, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v8, v8, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v9, v9, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    iget-object v10, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v10, v10, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    iget-object v11, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v11, v11, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    move-object v12, p1

    invoke-direct/range {v0 .. v12}, LaZ/a;-><init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IILjava/lang/String;LaZ/b;)V

    return-object v0
.end method

.method protected a(I)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "m"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->C()V

    return-void
.end method

.method public a(LaN/H;IILcom/google/googlenav/ui/wizard/C;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/hQ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/hQ;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p3, v0, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    return-void
.end method

.method protected a(Law/g;)V
    .locals 2

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    invoke-static {}, Lcom/google/googlenav/android/V;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    const/16 v1, 0x511

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x45c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v2}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/16 v3, 0x43e

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/16 v2, 0x452

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method protected a(Ljava/lang/String;II)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "s"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 8

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v7, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/C;->a(IILjava/lang/Object;)Z

    move-result v7

    :goto_0
    return v7

    :sswitch_0
    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "a"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-ne v0, v6, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->B()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->B()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    check-cast p3, Ljava/lang/String;

    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->i()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "p"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-nez v1, :cond_2

    move v4, v7

    :cond_2
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/d;->a(Z)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x44e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x44d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-nez v1, :cond_3

    move v4, v7

    :cond_3
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/d;->a(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1f5 -> :sswitch_3
        0x1f6 -> :sswitch_4
        0x5dd -> :sswitch_0
        0x5e3 -> :sswitch_5
        0x5e4 -> :sswitch_9
        0x5e5 -> :sswitch_2
        0x5ee -> :sswitch_6
        0x5ef -> :sswitch_7
        0x5f0 -> :sswitch_8
        0x5f1 -> :sswitch_1
    .end sparse-switch
.end method

.method protected b()V
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->C()V

    return-void
.end method

.method protected b(Ljava/lang/String;II)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "ss"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "si"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->i()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    return-void
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected g()Lcom/google/googlenav/ui/view/android/aL;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "b"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->i:I

    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iput v4, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public p()Z
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()Lcom/google/googlenav/ui/view/android/aL;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
