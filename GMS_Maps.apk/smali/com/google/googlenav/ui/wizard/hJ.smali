.class public Lcom/google/googlenav/ui/wizard/hJ;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/hH;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hH;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/wizard/hJ;->p()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public P_()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 14

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hJ;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f04015d

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f100199

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    const v3, 0x7f100234

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const v3, 0x7f1003cb

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f100235

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const v4, 0x7f1003cc

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    const v5, 0x7f1003cd

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    const v6, 0x7f1003ce

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f100236

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const v7, 0x7f1003cf

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f1003d0

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v8}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/16 v8, 0x445

    :goto_1
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/googlenav/K;->an()Z

    move-result v13

    if-eqz v13, :cond_5

    :cond_0
    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    sget-object v13, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v8, v13}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setLines(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x440

    :goto_3
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v8, 0x4

    if-ne v1, v8, :cond_8

    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/16 v1, 0x451

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v9, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4
    sget-object v9, Lcom/google/googlenav/ui/aV;->bt:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/hK;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/hK;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/EditText;)V

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v3, Lcom/google/googlenav/ui/d;

    const/4 v9, 0x0

    invoke-direct {v3, v9}, Lcom/google/googlenav/ui/d;-><init>(Z)V

    iput-object v3, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    invoke-virtual {v5, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/hL;

    invoke-direct {v1, p0, v5}, Lcom/google/googlenav/ui/wizard/hL;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v1, 0x44a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->clear()V

    const/16 v1, 0x453

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->Y:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    const/16 v1, 0x454

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->Z:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/hM;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hM;-><init>(Lcom/google/googlenav/ui/wizard/hJ;)V

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_5
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x7f1003d2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v4, Lcom/google/googlenav/ui/d;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/google/googlenav/ui/d;-><init>(Z)V

    iput-object v4, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    :cond_2
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    const v3, 0x7f1003d3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/hH;->z()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f1003d5

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/16 v4, 0x44c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->Y:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_6
    const v4, 0x7f1003d1

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v5, Lcom/google/googlenav/ui/wizard/hN;

    invoke-direct {v5, p0, v1, v3}, Lcom/google/googlenav/ui/wizard/hN;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/CheckBox;Landroid/widget/TextView;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_7
    const v1, 0x7f10002e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_b

    const v1, 0x7f100031

    :goto_8
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_c

    const v3, 0x7f100030

    :goto_9
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v4}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0x456

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_a
    new-instance v4, Lcom/google/googlenav/ui/wizard/hO;

    invoke-direct {v4, p0, v2}, Lcom/google/googlenav/ui/wizard/hO;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/EditText;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    const/16 v2, 0x6a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/hP;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hP;-><init>(Lcom/google/googlenav/ui/wizard/hJ;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_3
    const v0, 0x7f04015c

    goto/16 :goto_0

    :cond_4
    const/16 v8, 0x444

    goto/16 :goto_1

    :cond_5
    iget-object v13, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v13, v13, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v13, v8}, Lcom/google/googlenav/ui/view/android/aL;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_6
    const/16 v1, 0x43f

    goto/16 :goto_3

    :cond_7
    const/16 v1, 0x450

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    :cond_8
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    :cond_9
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_a
    const v1, 0x7f1003d1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f1003d4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f1003d5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_b
    const v1, 0x7f100030

    goto/16 :goto_8

    :cond_c
    const v3, 0x7f100031

    goto/16 :goto_9

    :cond_d
    const/16 v4, 0xde

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a
.end method

.method protected d()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hJ;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
