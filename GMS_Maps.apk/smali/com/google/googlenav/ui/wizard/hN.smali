.class Lcom/google/googlenav/ui/wizard/hN;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/widget/TextView;

.field final synthetic c:Lcom/google/googlenav/ui/wizard/hJ;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/CheckBox;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hN;->c:Lcom/google/googlenav/ui/wizard/hJ;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hN;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hN;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hN;->a:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hN;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hN;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hN;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hN;->c:Lcom/google/googlenav/ui/wizard/hJ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    const/16 v2, 0x5e4

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/googlenav/ui/wizard/hH;->a(IILjava/lang/Object;)Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hN;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
