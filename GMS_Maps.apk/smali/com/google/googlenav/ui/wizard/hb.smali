.class public Lcom/google/googlenav/ui/wizard/hb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/hg;

.field private final b:Lcom/google/googlenav/ai;

.field private final c:Lcom/google/googlenav/ui/wizard/jv;

.field private d:Lcom/google/googlenav/f;

.field private e:Ljava/util/List;

.field private f:Z

.field private g:Lcom/google/googlenav/ui/wizard/jq;

.field private h:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hg;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/hc;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hc;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->h:Ljava/util/Comparator;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->a:Lcom/google/googlenav/ui/wizard/hg;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Lcom/google/googlenav/f;)Lcom/google/googlenav/f;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Lcom/google/googlenav/ui/wizard/jq;)Lcom/google/googlenav/ui/wizard/jq;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->h:Ljava/util/Comparator;

    return-object v0
.end method

.method private d()V
    .locals 7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/he;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/he;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hb;->a()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/hb;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ui/wizard/hg;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->a:Lcom/google/googlenav/ui/wizard/hg;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/hd;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/hd;-><init>(Lcom/google/googlenav/ui/wizard/hb;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    invoke-static {v1, v0}, Lcom/google/googlenav/f;->a(Lcom/google/googlenav/g;Lcom/google/googlenav/ai;)Lcom/google/googlenav/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hb;->d()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cx;

    iget-object v0, v0, Lcom/google/googlenav/cx;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/wizard/jq;

    new-instance v2, Lcom/google/googlenav/ui/wizard/hf;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hf;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    const/16 v3, 0x3f2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/ui/wizard/jq;-><init>(Lcom/google/googlenav/ui/wizard/js;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jq;->show()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x3fa

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jq;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    :cond_0
    return-void
.end method
