.class Lcom/google/googlenav/ui/wizard/hd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/g;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/googlenav/ui/wizard/hb;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/hb;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hd;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 10

    const/4 v9, 0x7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hb;->a(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/hb;->a(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/wizard/hb;->a(Lcom/google/googlenav/ui/wizard/hb;Lcom/google/googlenav/f;)Lcom/google/googlenav/f;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/wizard/hb;->a(Lcom/google/googlenav/ui/wizard/hb;Ljava/util/List;)Ljava/util/List;

    if-eqz p2, :cond_4

    new-instance v0, Lcom/google/googlenav/ai;

    invoke-direct {v0, p2}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/16 v2, 0x4b

    sget-object v3, LbK/x;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ai;->a(ILcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    invoke-virtual {v2, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/16 v5, 0x1c

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x1e

    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/hb;->b(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/List;

    move-result-object v7

    const/16 v8, 0x12

    invoke-virtual {v4, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4, v5, v6}, Lcom/google/googlenav/cx;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/cx;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hb;->b(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/hb;->c(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hb;->d(Lcom/google/googlenav/ui/wizard/hb;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/hb;->a(Lcom/google/googlenav/ui/wizard/hb;Z)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hb;->e(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hd;->b:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hb;->b()V

    goto :goto_0
.end method
