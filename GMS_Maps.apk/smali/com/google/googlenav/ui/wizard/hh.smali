.class public Lcom/google/googlenav/ui/wizard/hh;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field protected a:I

.field private b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/J;

.field private i:Lbf/am;

.field private j:Z

.field private k:Lcom/google/googlenav/ui/br;

.field private l:LaB/s;

.field private m:Ljava/lang/String;

.field private n:Lbf/bk;

.field private o:Lcom/google/googlenav/ui/wizard/hb;

.field private p:Lcom/google/googlenav/ui/wizard/hg;

.field private q:LaM/h;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    new-instance v0, Lcom/google/googlenav/ui/wizard/hi;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hi;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->p:Lcom/google/googlenav/ui/wizard/hg;

    new-instance v0, Lcom/google/googlenav/ui/wizard/ho;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ho;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-interface {p2}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->l:LaB/s;

    new-instance v0, Lcom/google/googlenav/ui/br;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->l:LaB/s;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->k:Lcom/google/googlenav/ui/br;

    return-void
.end method

.method private A()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->f(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    :cond_1
    return-void
.end method

.method private B()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    instance-of v1, v0, Lbf/m;

    if-eqz v1, :cond_0

    check-cast v0, Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bp()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lbf/i;->J()V

    goto :goto_0
.end method

.method private C()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bK()[I

    move-result-object v0

    aget v1, v0, v8

    aget v2, v0, v9

    add-int/2addr v1, v2

    const/16 v2, 0x55

    const-string v3, "d"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "s="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bF()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "a="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bJ()[Lcom/google/googlenav/ak;

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "f="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "l="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v0, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "d="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v0, v0, v9

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->bG()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hh;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/hh;)LaY/c;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->z()LaY/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/hh;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/hh;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->A()V

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/hh;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/hh;)LaM/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/hh;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->y()V

    return-void
.end method

.method private y()V
    .locals 9

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hb;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    new-instance v1, LaY/c;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    new-instance v3, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-direct {v1, p0, v2, v3, v0}, LaY/c;-><init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;Lcom/google/googlenav/ui/wizard/hb;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaY/c;

    new-instance v2, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    invoke-virtual {v0, v2}, LaY/c;->a(Lcom/google/googlenav/ui/br;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/googlenav/cv;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    new-instance v4, Lcom/google/googlenav/ui/wizard/hw;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->k:Lcom/google/googlenav/ui/br;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->z()LaY/c;

    move-result-object v7

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/googlenav/ui/wizard/hw;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;LaY/c;Lcom/google/googlenav/J;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/cv;-><init>(IJLcom/google/googlenav/cw;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Z)V

    return-void
.end method

.method private z()LaY/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaY/c;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    new-instance v0, Lcom/google/googlenav/ui/wizard/hb;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->p:Lcom/google/googlenav/ui/wizard/hg;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/googlenav/ui/wizard/hb;-><init>(Lcom/google/googlenav/ui/wizard/hg;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->j()V

    return-void
.end method

.method protected a(Lcom/google/googlenav/ai;Z)V
    .locals 9

    const/4 v4, 0x0

    const/4 v8, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/wizard/hu;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/wizard/hu;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;)V

    :goto_1
    new-instance v1, Lcom/google/googlenav/bF;

    const-string v2, "lo-gmm"

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/google/googlenav/bF;-><init>(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/bG;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v0, 0x42d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v5, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/googlenav/ui/wizard/ht;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->i()Lcom/google/googlenav/ui/wizard/hx;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ht;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V

    goto :goto_1
.end method

.method public a(Lcom/google/googlenav/ay;)V
    .locals 8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x406

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x405

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x61c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x315

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/hk;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/ui/wizard/hk;-><init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ay;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/cx;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ai;->n(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/ai;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p4}, Lcom/google/googlenav/ai;->a(Ljava/lang/Integer;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-nez v0, :cond_3

    const/16 v0, 0x40f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    const-string v1, "plrp"

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/ai;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/googlenav/F;->a(I)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v1, p1, p2, p3}, Lbf/bk;->a(IILjava/lang/Object;)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xe -> :sswitch_0
        0x6a4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected b()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bm;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    sget-object v1, Lcom/google/googlenav/br;->a:Lcom/google/googlenav/br;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/br;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->y()V

    goto :goto_0
.end method

.method protected c()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v0, v1}, Lbf/am;->h(Lbf/i;)V

    :cond_0
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hb;->c()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    return-void
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    return-void
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/ui/wizard/hj;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hj;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    return-void
.end method

.method public f()V
    .locals 9

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->C()V

    new-instance v2, Lcom/google/googlenav/ui/wizard/hq;

    new-instance v0, Lcom/google/googlenav/ui/wizard/hl;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hl;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    invoke-direct {v2, v0}, Lcom/google/googlenav/ui/wizard/hq;-><init>(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x1b5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/hr;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    new-instance v8, Lcom/google/googlenav/ui/wizard/hm;

    invoke-direct {v8, p0}, Lcom/google/googlenav/ui/wizard/hm;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    move-object v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/googlenav/ui/wizard/hr;-><init>(Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V

    new-instance v0, LaY/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v0, v1, v2, v4, v3}, LaY/a;-><init>(JLcom/google/googlenav/common/io/protocol/ProtoBuf;LaY/b;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    return-void
.end method

.method public g()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    const/16 v2, 0x2a

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/ai;I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    return-void
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bL()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->m(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->A()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    goto :goto_0
.end method

.method i()Lcom/google/googlenav/ui/wizard/hx;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/hn;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hn;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    return-object v0
.end method
