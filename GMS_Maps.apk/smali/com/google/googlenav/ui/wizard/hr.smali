.class Lcom/google/googlenav/ui/wizard/hr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaY/b;


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private final b:Lbf/am;

.field private final c:Lcom/google/googlenav/ai;

.field private final d:Lcom/google/googlenav/ui/wizard/hx;

.field private final e:Lcom/google/googlenav/ui/wizard/hq;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hr;->b:Lbf/am;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hr;->e:Lcom/google/googlenav/ui/wizard/hq;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->e:Lcom/google/googlenav/ui/wizard/hq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hq;->a(Lcom/google/googlenav/ui/wizard/hq;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aC;->b(Lcom/google/googlenav/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bk()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->b:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->f(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
