.class Lcom/google/googlenav/ui/wizard/ht;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/bG;


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:Lcom/google/googlenav/J;

.field private final c:Lbf/am;

.field private final d:Lcom/google/googlenav/ui/wizard/hq;

.field private final e:Lcom/google/googlenav/ui/wizard/hx;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ht;->c:Lbf/am;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hq;->a(Lcom/google/googlenav/ui/wizard/hq;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bF()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ai;->n(I)V

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/aC;->a(Lcom/google/googlenav/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bk()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->j()Lbf/by;

    move-result-object v0

    invoke-virtual {v0}, Lbf/by;->bJ()LaR/n;

    move-result-object v3

    invoke-interface {v3}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-interface {v3, v0, v4}, LaR/n;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->c:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbf/i;->f(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->b()V

    if-eqz v1, :cond_4

    const/16 v0, 0x411

    :goto_2
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ax;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/ui/wizard/hx;->a(Lcom/google/googlenav/ai;Z)V

    :cond_2
    :goto_3
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->a()V

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    :cond_4
    const/16 v0, 0x41e

    goto :goto_2

    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    const/16 v0, 0x410

    goto :goto_3

    :cond_6
    const/16 v0, 0x40f

    goto :goto_3
.end method
