.class Lcom/google/googlenav/ui/wizard/hw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/cw;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/google/googlenav/ai;

.field private final c:Lcom/google/googlenav/ui/br;

.field private final d:LaY/c;

.field private final e:Lcom/google/googlenav/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/wizard/hp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/wizard/hw;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;LaY/c;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hw;->b:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hw;->c:Lcom/google/googlenav/ui/br;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hw;->d:LaY/c;

    sget-boolean v0, Lcom/google/googlenav/ui/wizard/hw;->a:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hw;->e:Lcom/google/googlenav/J;

    return-void
.end method


# virtual methods
.method public a(ZLjava/util/List;)V
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/googlenav/ay;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ay;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3}, Lcom/google/googlenav/ay;->b()Lcom/google/googlenav/ui/bs;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ax;->a(Lcom/google/googlenav/ay;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/hv;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hw;->d:LaY/c;

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/hv;-><init>(LaY/c;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->d:LaY/c;

    invoke-virtual {v0}, LaY/c;->J_()V

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->e:Lcom/google/googlenav/J;

    const/16 v1, 0x408

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hw;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Z)V

    goto :goto_1
.end method
