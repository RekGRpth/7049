.class public Lcom/google/googlenav/ui/wizard/iC;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/googlenav/ui/wizard/iG;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iC;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/google/googlenav/android/T;)Lcom/google/googlenav/ui/view/dialog/o;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/iD;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/iD;-><init>(Lcom/google/googlenav/ui/wizard/iC;Lcom/google/googlenav/android/T;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iC;)Lcom/google/googlenav/ui/wizard/iG;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/iC;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ui/wizard/iG;)Lcom/google/googlenav/android/T;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/iE;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/iE;-><init>(Lcom/google/googlenav/ui/wizard/iC;Lcom/google/googlenav/ui/wizard/iG;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/wizard/iG;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iC;->b(Lcom/google/googlenav/ui/wizard/iG;)Lcom/google/googlenav/android/T;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/android/T;)Lcom/google/googlenav/ui/view/dialog/o;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/k;

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/k;-><init>(Lcom/google/googlenav/ui/view/dialog/o;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iC;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iC;->j()V

    return-void
.end method

.method protected c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/iG;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method
