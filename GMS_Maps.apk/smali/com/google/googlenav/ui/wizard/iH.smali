.class public Lcom/google/googlenav/ui/wizard/iH;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/J;

.field private i:Lcom/google/googlenav/bh;

.field private j:Lcom/google/googlenav/ui/view/android/aL;

.field private final k:Lcom/google/googlenav/bi;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bh;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    new-instance v0, Lcom/google/googlenav/ui/wizard/iI;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/iI;-><init>(Lcom/google/googlenav/ui/wizard/iH;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->k:Lcom/google/googlenav/bi;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/iH;->c:Lcom/google/googlenav/J;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iH;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->c:Lcom/google/googlenav/J;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x511

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->k:Lcom/google/googlenav/bi;

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/iH;->a(ILcom/google/googlenav/bi;)V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    return-void
.end method

.method private a(ILcom/google/googlenav/bi;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v3, p2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v4, p2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v3, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v4, p2}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x517
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_4
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private static a(Ljava/util/List;II)V
    .locals 3

    new-instance v0, Lbj/bt;

    invoke-static {p1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-direct {v0, v1, v2, p2}, Lbj/bt;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;I)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/iH;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private b(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iH;->j()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/iH;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    return v0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-static {v2, v3}, LaN/B;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->O()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->O()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "num"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->O()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "start"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :cond_0
    :goto_1
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "/maps/place"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v4, "cid"

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v4, "view"

    const-string v5, "feature"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v4, "mcsrc"

    const-string v5, "photo"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz v0, :cond_1

    const-string v4, "start"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "num"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "http"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "www.google.com"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "/maps/photos/flagImage"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "hl"

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "latlng"

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "photofeatureurl"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v0

    array-length v0, v0

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method private y()I
    .locals 2

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x4b

    const-string v1, "b"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iH;->f()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/bh;->c(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iH;->b(I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iH;->a(I)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iH;->a(I)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/iH;->i()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iH;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x514
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->h:Lcom/google/googlenav/ui/view/android/aL;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->j:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v0, Lcom/google/googlenav/ui/wizard/iK;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/iK;-><init>(Lcom/google/googlenav/ui/wizard/iH;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->j:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->j:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->j:Lcom/google/googlenav/ui/view/android/aL;

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iH;->i:Lcom/google/googlenav/bh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iH;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/bh;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    return-void
.end method

.method public f()Ljava/util/List;
    .locals 7

    const/16 v2, 0x518

    const/16 v6, 0x516

    const/16 v5, 0x515

    const/16 v4, 0x51b

    const/16 v3, 0x519

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const/16 v1, 0x51e

    const/16 v2, 0x514

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x523

    invoke-static {v0, v1, v5}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x51d

    const/16 v2, 0x517

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x51f

    const/16 v2, 0x520

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/iH;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x3c4

    const/16 v2, 0x521

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    :cond_0
    invoke-static {v0, v3, v6}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    goto :goto_0

    :pswitch_1
    const/16 v1, 0x51c

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    invoke-static {v0, v4, v3}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    invoke-static {v0, v5, v4}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x51a

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    goto :goto_0

    :pswitch_2
    const/16 v1, 0x51c

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x513

    const/16 v2, 0x51d

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x51a

    const/16 v2, 0x51e

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    invoke-static {v0, v6, v4}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x51f

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    goto :goto_0

    :pswitch_3
    const/16 v1, 0x521

    const/16 v2, 0x522

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x520

    const/16 v2, 0x523

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x525

    const/16 v2, 0x524

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    const/16 v1, 0x525

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/wizard/iH;->a(Ljava/util/List;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public h()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/iH;->y()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->h()V

    :cond_0
    return-void
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/iH;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
