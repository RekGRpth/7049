.class public Lcom/google/googlenav/ui/wizard/iL;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/dialog/bw;

.field private b:Lcom/google/googlenav/ui/wizard/iS;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private i:Ljava/lang/String;

.field private j:LaN/B;

.field private k:LaC/a;

.field private l:Lcom/google/googlenav/ui/wizard/iT;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaC/a;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iL;->k:LaC/a;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;)Lcom/google/googlenav/ui/wizard/iS;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/lang/String;JJ)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;JJ)V
    .locals 10

    const-wide/16 v8, 0xa

    const/4 v4, 0x3

    const/4 v6, 0x0

    const/16 v3, 0x15

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/google/googlenav/friend/ad;->p()V

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/bw;->hide()V

    :cond_0
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x4

    invoke-virtual {v7, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p4, p5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v7, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    if-ne v1, v2, :cond_1

    const/16 v1, 0x10

    invoke-virtual {v7, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    if-ne v1, v2, :cond_2

    move v0, v6

    :cond_2
    div-long v1, p2, v8

    long-to-int v1, v1

    div-long v2, p4, v8

    long-to-int v2, v2

    invoke-static {v0, p1, v1, v2}, LaR/a;->a(ILjava/lang/String;II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x15a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/iQ;

    invoke-direct {v1, p0, v7}, Lcom/google/googlenav/ui/wizard/iQ;-><init>(Lcom/google/googlenav/ui/wizard/iL;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sget-object v2, LaR/O;->d:LaR/O;

    invoke-interface {v0, v8, v1, v2}, LaR/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    return-void

    :cond_3
    invoke-static {}, Lcom/google/googlenav/friend/ad;->q()V

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v3

    if-ltz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bw;

    new-instance v2, Lcom/google/googlenav/ui/wizard/iP;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/iP;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-direct {v0, v1, v2, p0}, Lcom/google/googlenav/ui/view/dialog/bw;-><init>(Ljava/util/List;Lcom/google/googlenav/ui/view/dialog/by;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->show()V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    return v0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->j()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    if-eqz v0, :cond_0

    const-string v0, "home_speedbump_ack"

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_1
    int-to-long v2, v1

    int-to-long v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    invoke-virtual {v0}, LaN/B;->d()I

    move-result v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    invoke-virtual {v0}, LaN/B;->f()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->k:LaC/a;

    const/4 v1, 0x5

    new-instance v2, Lcom/google/googlenav/ui/wizard/iM;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/iM;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-virtual {v0, p1, v1, v2}, LaC/a;->a(Ljava/lang/String;ILaC/c;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 3

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bY;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bY;-><init>(Lcom/google/googlenav/ui/wizard/iL;Lcom/google/googlenav/ui/wizard/iT;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->f()V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->dismiss()V

    :cond_0
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    return-void
.end method

.method public d()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    iget-boolean v3, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->a()V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/iL;->a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/iS;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->a()V

    return-void
.end method

.method public f()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bY;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bY;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;I)V

    new-instance v0, Lcom/google/googlenav/ui/wizard/iR;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/iR;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->e()V

    goto :goto_0
.end method
