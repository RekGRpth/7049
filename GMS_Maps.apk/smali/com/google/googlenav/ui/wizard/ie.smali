.class public Lcom/google/googlenav/ui/wizard/ie;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/ih;


# direct methods
.method static synthetic a(Lcom/google/googlenav/ui/wizard/ie;)Lcom/google/googlenav/ui/wizard/ih;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->h()V

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    return v0
.end method

.method protected b()V
    .locals 2

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->e()Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/if;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/if;-><init>(Lcom/google/googlenav/ui/wizard/ie;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->j()V

    return-void
.end method

.method protected e()Lcom/google/googlenav/ui/view/android/S;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/ii;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/ii;-><init>(Lcom/google/googlenav/ui/wizard/ie;Lcom/google/googlenav/ui/e;)V

    return-object v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ih;->a:Lcom/google/googlenav/ui/wizard/ig;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ig;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->a()V

    return-void
.end method
