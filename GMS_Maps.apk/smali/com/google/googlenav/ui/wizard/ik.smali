.class public Lcom/google/googlenav/ui/wizard/ik;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static b:Landroid/media/AudioManager;


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/in;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method static synthetic a(Landroid/media/AudioManager;)Landroid/media/AudioManager;
    .locals 0

    sput-object p0, Lcom/google/googlenav/ui/wizard/ik;->b:Landroid/media/AudioManager;

    return-object p0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ik;)Lcom/google/googlenav/ui/wizard/in;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    return-object v0
.end method

.method static synthetic e()Landroid/media/AudioManager;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/ik;->b:Landroid/media/AudioManager;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/im;Ljava/util/EnumSet;Ljava/util/EnumSet;)V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/in;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/in;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/in;->a:Lcom/google/googlenav/ui/wizard/im;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    iput-object p2, v0, Lcom/google/googlenav/ui/wizard/in;->b:Ljava/util/EnumSet;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/in;->c:Ljava/util/EnumSet;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ik;->j()V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f100449

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method protected b()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/io;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/io;-><init>(Lcom/google/googlenav/ui/wizard/ik;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ik;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ik;->j()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ik;->a:Lcom/google/googlenav/ui/wizard/in;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/in;->a:Lcom/google/googlenav/ui/wizard/im;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/im;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ik;->a()V

    return-void
.end method
