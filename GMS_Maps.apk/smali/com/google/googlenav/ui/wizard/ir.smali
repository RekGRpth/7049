.class Lcom/google/googlenav/ui/wizard/ir;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/io;

.field private final b:Landroid/widget/CheckBox;

.field private final c:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/io;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ir;->a:Lcom/google/googlenav/ui/wizard/io;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ir;->c:Landroid/widget/CheckBox;

    return-void
.end method

.method private a(Landroid/widget/CompoundButton;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->a:Lcom/google/googlenav/ui/wizard/io;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/io;->a(Lcom/google/googlenav/ui/wizard/io;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->c:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ir;Landroid/widget/CompoundButton;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ir;->a(Landroid/widget/CompoundButton;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    if-ne p1, v1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ir;->a(Landroid/widget/CompoundButton;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, LaS/a;->w()LaS/c;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/wizard/il;->a:[I

    invoke-virtual {v1}, LaS/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :pswitch_0
    new-instance v1, Lcom/google/googlenav/ui/wizard/is;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/googlenav/ui/wizard/is;-><init>(Lcom/google/googlenav/ui/wizard/ir;Landroid/widget/CompoundButton;LaS/a;)V

    invoke-virtual {v0, v1}, LaS/a;->a(LaS/b;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ir;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-static {}, Lcom/google/googlenav/ui/wizard/io;->l()Lcom/google/googlenav/android/BaseMapsActivity;

    move-result-object v0

    const/16 v1, 0x4b2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/16 v0, 0x61

    const-string v1, "f"

    const-string v2, "l"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ir;->a(Landroid/widget/CompoundButton;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
