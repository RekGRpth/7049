.class public Lcom/google/googlenav/ui/wizard/iu;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/ix;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/io;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/io;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iu;->a:Lcom/google/googlenav/ui/wizard/io;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/iu;->b:Z

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/iu;->a(J)V

    return-void
.end method

.method public a(I)V
    .locals 2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaS/a;->b(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaS/a;->b(Z)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/iu;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/googlenav/ui/wizard/iu;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, v1}, LaS/a;->b(Z)V

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/iu;->b:Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iu;->a:Lcom/google/googlenav/ui/wizard/io;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/io;->a(Lcom/google/googlenav/ui/wizard/io;)V

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/iu;->b:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/iu;->a(J)V

    :cond_0
    return-void
.end method
