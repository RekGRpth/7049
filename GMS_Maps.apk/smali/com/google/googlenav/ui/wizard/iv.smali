.class Lcom/google/googlenav/ui/wizard/iv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/io;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/io;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iv;->a:Lcom/google/googlenav/ui/wizard/io;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/googlenav/ui/wizard/iv;->b:I

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    if-lez p2, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/iv;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ik;->e()Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/iv;->b:I

    const/16 v2, 0x14

    invoke-virtual {v0, v1, p2, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method
