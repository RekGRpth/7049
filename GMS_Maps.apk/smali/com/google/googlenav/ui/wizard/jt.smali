.class public Lcom/google/googlenav/ui/wizard/jt;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Law/d;

.field private c:Lcom/google/googlenav/ui/wizard/A;

.field private i:Z

.field private j:J

.field private k:J

.field private l:Lcom/google/googlenav/ui/wizard/ju;

.field private m:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jt;->m:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/jt;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jt;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/jt;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/jt;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/util/List;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->a:Ljava/lang/String;

    const-string v1, "\n"

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/googlenav/ui/aV;->az:Lcom/google/googlenav/ui/aV;

    sget-object v3, Lcom/google/googlenav/ui/aV;->aC:Lcom/google/googlenav/ui/aV;

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-static {v4, v0}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    aget-object v4, v1, v0

    invoke-static {v4, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->h()V

    :cond_0
    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/jt;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/jt;->c:Lcom/google/googlenav/ui/wizard/A;

    iput-wide p4, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    iput-boolean p6, p0, Lcom/google/googlenav/ui/wizard/jt;->i:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->j()V

    return-void
.end method

.method protected b()V
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/wizard/ju;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->m:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/ju;-><init>(Lcom/google/googlenav/ui/wizard/jt;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->l:Lcom/google/googlenav/ui/wizard/ju;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->l:Lcom/google/googlenav/ui/wizard/ju;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ju;->show()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->setInputFocusForTesting(I)V

    iget-wide v0, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/jt;->k:J

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->setInputFocusForTesting(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->l:Lcom/google/googlenav/ui/wizard/ju;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->l:Lcom/google/googlenav/ui/wizard/ju;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ju;->dismiss()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->l:Lcom/google/googlenav/ui/wizard/ju;

    :cond_0
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->c:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    return-void
.end method

.method public d()V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jt;->c:Lcom/google/googlenav/ui/wizard/A;

    iget-wide v3, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    iget-boolean v5, p0, Lcom/google/googlenav/ui/wizard/jt;->i:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/jt;->c:Lcom/google/googlenav/ui/wizard/A;

    iput-wide v3, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    iput-boolean v5, p0, Lcom/google/googlenav/ui/wizard/jt;->i:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->j()V

    return-void
.end method

.method public e()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/wizard/jt;->k:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/googlenav/ui/wizard/jt;->j:J

    sub-long v0, v2, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    new-instance v2, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Las/d;->b(I)V

    invoke-virtual {v2, v0, v1}, Las/d;->a(J)V

    invoke-virtual {v2}, Las/d;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->a()V

    goto :goto_0
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->b:Law/d;

    invoke-interface {v0}, Law/d;->Z()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->o()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->c:Lcom/google/googlenav/ui/wizard/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->a()V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jt;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    :cond_3
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->d:Lcom/google/googlenav/ui/wizard/jv;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jt;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xc -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public n()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jt;->a()V

    return-void
.end method
