.class public Lcom/google/googlenav/ui/wizard/ju;
.super Landroid/app/ProgressDialog;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/jt;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jt;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ju;->a:Lcom/google/googlenav/ui/wizard/jt;

    invoke-direct {p0, p2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/ui/wizard/jt;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/ju;->setProgressStyle(I)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ju;->setIndeterminate(Z)V

    :goto_0
    invoke-static {p1}, Lcom/google/googlenav/ui/wizard/jt;->b(Lcom/google/googlenav/ui/wizard/jt;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ju;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p0}, Lcom/google/googlenav/ui/wizard/ju;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ju;->setProgressStyle(I)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/ju;->setIndeterminate(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ju;->a:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->h()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
