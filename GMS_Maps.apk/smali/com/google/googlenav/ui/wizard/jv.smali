.class public Lcom/google/googlenav/ui/wizard/jv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Lbm/i;

.field private static l:Z


# instance fields
.field protected final a:Lcom/google/googlenav/J;

.field protected final b:LaN/p;

.field protected final c:LaN/u;

.field public final d:Lcom/google/googlenav/ui/wizard/jC;

.field private final e:Lcom/google/googlenav/ui/wizard/z;

.field private final g:LaH/m;

.field private final h:Lcom/google/googlenav/android/aa;

.field private i:Z

.field private final j:Lcom/google/googlenav/aA;

.field private final k:Lcom/google/googlenav/friend/j;

.field private final m:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lbm/i;

    const-string v1, "layers open"

    const-string v2, "lo"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/ui/wizard/jv;->l:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;LaN/p;LaN/u;LaH/m;Lcom/google/googlenav/aA;ZLcom/google/googlenav/ui/wizard/z;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    new-instance v0, Lcom/google/googlenav/ui/wizard/jC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/jC;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    iput-object p8, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/jv;->g:LaH/m;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jv;->b:LaN/p;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    iput-boolean p6, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    iput-object p7, p0, Lcom/google/googlenav/ui/wizard/jv;->e:Lcom/google/googlenav/ui/wizard/z;

    iput-object p9, p0, Lcom/google/googlenav/ui/wizard/jv;->k:Lcom/google/googlenav/friend/j;

    return-void
.end method

.method private a(Ljava/lang/String;LaM/g;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Ljava/lang/String;)V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, p2}, LaM/f;->a(LaM/g;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public A()LaH/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->g:LaH/m;

    return-object v0
.end method

.method public B()Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    return-object v0
.end method

.method public C()LaN/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->b:LaN/p;

    return-object v0
.end method

.method public D()LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    return-object v0
.end method

.method public E()Lcom/google/googlenav/aA;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    return-object v0
.end method

.method public F()Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method public G()LaB/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    return-object v0
.end method

.method public H()Lcom/google/googlenav/friend/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->k:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method public I()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->e()Lax/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/k;)V

    return-void
.end method

.method public J()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/k;)V

    return-void
.end method

.method public K()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/bN;->e()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/l;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    return-void
.end method

.method public L()Lcom/google/googlenav/ui/wizard/jC;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    return-object v0
.end method

.method public M()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ai()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    return-void
.end method

.method public N()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lat/b;)I

    move-result v0

    return v0
.end method

.method a()V
    .locals 2

    const/16 v0, 0xa

    const-string v1, "i"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->c()V

    return-void
.end method

.method public a(ILat/a;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jA;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/googlenav/ui/wizard/jA;-><init>(Lcom/google/googlenav/ui/wizard/jv;ILat/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public a(ILcom/google/googlenav/ui/wizard/cc;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->j()Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cb;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    :cond_0
    return-void
.end method

.method public a(I[Lba/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(I[Lba/f;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(LaG/h;)V
    .locals 1

    if-nez p1, :cond_0

    new-instance p1, LaG/h;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v0

    invoke-direct {p1, v0}, LaG/h;-><init>(LaB/s;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->c()Lcom/google/googlenav/ui/wizard/G;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/G;->a(LaG/h;)V

    return-void
.end method

.method public a(LaN/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, v0}, Lba/b;->a(LaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;LaN/H;IILcom/google/googlenav/ui/wizard/C;Z)V
    .locals 10

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->I()Lcom/google/googlenav/ui/wizard/br;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object v7, p4

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/ui/wizard/br;->a(LaN/B;Lo/D;Ljava/lang/String;LaN/H;IILjava/lang/String;Lcom/google/googlenav/ui/wizard/C;Z)V

    return-void
.end method

.method public a(Lax/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/b;)V

    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lax/b;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lax/b;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    :cond_2
    return-void
.end method

.method public a(Lax/b;LaN/H;IILjava/util/List;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->M()Lcom/google/googlenav/ui/wizard/bV;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/bV;->a(Lax/b;LaN/H;IILjava/util/List;)V

    return-void
.end method

.method public a(Lax/j;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/j;)V

    return-void
.end method

.method public a(Lbf/am;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/am;I)V

    return-void
.end method

.method public a(Lbf/am;Z)V
    .locals 3

    sget-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    const/16 v0, 0x43

    const-string v1, "d"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->i()V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->a(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->r()Lcom/google/googlenav/ui/wizard/cC;

    move-result-object v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cC;->b(Lbf/am;)Lcom/google/googlenav/ui/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/av;->a()LaM/g;

    move-result-object v2

    invoke-virtual {v1, v2}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lbf/am;)V

    sget-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    return-void
.end method

.method public a(Lcom/google/googlenav/L;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->t()Lcom/google/googlenav/ui/wizard/cu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cu;->a(Lcom/google/googlenav/L;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V
    .locals 4

    invoke-virtual {p1, p3}, Lcom/google/googlenav/aZ;->g(I)Lcom/google/googlenav/bc;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/googlenav/bc;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, p3, v2, v3}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->k()Lcom/google/googlenav/ui/wizard/ce;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v1, v0}, Lcom/google/googlenav/ui/wizard/ce;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Lcom/google/googlenav/bc;Lcom/google/googlenav/bc;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->E()Lcom/google/googlenav/ui/wizard/hy;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iH;->a(Lcom/google/googlenav/ai;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iH;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iH;->j()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V
    .locals 8

    const/4 v1, 0x0

    const/16 v7, 0x55

    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "ns"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    if-nez p4, :cond_0

    if-eqz v2, :cond_4

    :cond_0
    const-string v3, "e"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    if-nez p4, :cond_2

    move-object v0, v1

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x1

    if-eqz v2, :cond_1

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_1
    :goto_1
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->C()Lcom/google/googlenav/ui/wizard/hh;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "s="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    const-string v0, "e"

    invoke-static {v7, v0}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .locals 8

    const/4 v1, 0x0

    const/16 v7, 0x65

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "ns"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    if-nez p3, :cond_0

    if-eqz v2, :cond_4

    :cond_0
    const-string v3, "e"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    if-nez p3, :cond_2

    move-object v0, v1

    :goto_0
    aput-object v0, v4, v6

    const/4 v0, 0x1

    if-eqz v2, :cond_1

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_1
    :goto_1
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->G()Lcom/google/googlenav/ui/wizard/fq;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v6, p4}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    const-string v0, "e"

    invoke-static {v7, v0}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lcom/google/googlenav/bZ;)V
    .locals 3

    const/16 v0, 0x54

    const-string v1, "ts"

    const-string v2, "c"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->f()Lcom/google/googlenav/ui/wizard/bx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bx;->a(Lcom/google/googlenav/bZ;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/bu;Lcom/google/googlenav/br;Lcom/google/googlenav/ui/wizard/jj;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->T()Lcom/google/googlenav/ui/wizard/jb;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/jb;->a(Lcom/google/googlenav/bu;Lcom/google/googlenav/br;Lcom/google/googlenav/ui/wizard/jj;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->P()Lcom/google/googlenav/ui/wizard/iL;

    move-result-object v0

    invoke-virtual {v0, p4, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/iL;->a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->v()Lcom/google/googlenav/ui/wizard/aZ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/aZ;->a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lcom/google/googlenav/ui/r;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/s;LaH/h;Lcom/google/googlenav/ui/view/android/aB;ZLcom/google/googlenav/ui/wizard/gb;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->B()Lcom/google/googlenav/ui/wizard/fY;

    move-result-object v6

    new-instance v0, Lcom/google/googlenav/ui/wizard/ga;

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ga;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/gb;LaH/h;Lcom/google/googlenav/ui/view/android/aB;Z)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/wizard/fY;->a(Lcom/google/googlenav/ui/wizard/ga;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V
    .locals 7

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/Q;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/Q;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;ZZZ)V

    if-eqz p3, :cond_1

    invoke-static {p3}, Lcom/google/googlenav/friend/ad;->d(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/h;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "u"

    invoke-static {v3, v1, v2}, Lbm/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/friend/ad;->d(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/google/googlenav/ui/wizard/jB;

    invoke-static {}, Lcom/google/googlenav/friend/W;->e()Lcom/google/googlenav/friend/W;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/google/googlenav/ui/wizard/jB;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/ui/wizard/Q;Lcom/google/googlenav/ui/wizard/jw;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->n()Lcom/google/googlenav/ui/wizard/M;

    move-result-object v2

    invoke-virtual {p0, v2, v1, p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/M;Lcom/google/googlenav/friend/bd;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/Q;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->z()Lcom/google/googlenav/ui/wizard/eP;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/aL;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->S()Lcom/google/googlenav/ui/wizard/ew;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ew;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/C;)V
    .locals 1

    instance-of v0, p1, Lcom/google/googlenav/ui/wizard/jt;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/E;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/wizard/E;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->j()V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->j()V

    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/M;Lcom/google/googlenav/friend/bd;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/Q;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    :cond_0
    invoke-virtual {p2, p0}, Lcom/google/googlenav/friend/bd;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p4}, Lcom/google/googlenav/ui/wizard/M;->a(Lcom/google/googlenav/ui/wizard/Q;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/bo;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->K()Lcom/google/googlenav/ui/wizard/iy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iy;->a(Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/ct;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->s()Lcom/google/googlenav/ui/wizard/cr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cr;->a(Lcom/google/googlenav/ui/wizard/ct;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dF;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/el;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->p()Lcom/google/googlenav/ui/wizard/eh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eh;->a(Lcom/google/googlenav/ui/wizard/el;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/ep;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->q()Lcom/google/googlenav/ui/wizard/em;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/em;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gJ;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->o()Lcom/google/googlenav/ui/wizard/gG;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gG;->a(Lcom/google/googlenav/ui/wizard/gJ;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/iG;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->N()Lcom/google/googlenav/ui/wizard/iC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/ui/wizard/iG;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/x;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->a()Lcom/google/googlenav/ui/wizard/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/ui/wizard/x;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(Ljava/lang/String;ILaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;LaN/B;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(Ljava/lang/String;LaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jt;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/ui/s;)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {p4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ia;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/J;Lbf/am;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .locals 9

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .locals 9

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;LaM/g;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1, p3, p2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .locals 8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->J()Lcom/google/googlenav/ui/wizard/ib;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/ib;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->m()Lcom/google/googlenav/ui/wizard/cx;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cx;->a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    return-void
.end method

.method public a(ZLcom/google/googlenav/ui/wizard/db;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->w()Lcom/google/googlenav/ui/wizard/cZ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cZ;->a(ZLcom/google/googlenav/ui/wizard/db;)V

    return-void
.end method

.method public a([Lcom/google/googlenav/aw;IZ)V
    .locals 6

    const/16 v0, 0x65

    const-string v1, "vg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "st="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "gt=t"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->l()Lcom/google/googlenav/ui/wizard/cf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/cf;->a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V

    return-void
.end method

.method public a(Lat/a;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lat/a;->b()I

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LaN/B;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(LaN/B;)I

    move-result v0

    return v0
.end method

.method public b(Lat/a;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lat/a;)I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->a()V

    return-void
.end method

.method public b(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->f()Lcom/google/googlenav/ui/wizard/bx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bx;->a(Lax/b;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Q()Lcom/google/googlenav/ui/wizard/iU;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iU;->a(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/s;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->z()Lcom/google/googlenav/ui/wizard/eP;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/J;)V

    return-void
.end method

.method b(Lcom/google/googlenav/ui/wizard/C;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    new-instance v2, Lcom/google/googlenav/ui/wizard/jx;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/jx;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    const/16 v0, 0x65

    const-string v1, "vg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "st="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "gt=m"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->l()Lcom/google/googlenav/ui/wizard/cf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, v5}, Lcom/google/googlenav/ui/wizard/cf;->a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V

    return-void
.end method

.method public c(Lat/a;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lat/a;)I

    move-result v0

    return v0
.end method

.method c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->U()Lcom/google/googlenav/ui/wizard/jk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jk;->j()V

    return-void
.end method

.method public c(Lax/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    return-void
.end method

.method public c(Lcom/google/googlenav/ui/s;)V
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/ui/wizard/jv;->l:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/s;->e(Z)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->R()Lcom/google/googlenav/ui/wizard/fl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/fl;->a(Ljava/lang/String;)V

    return-void
.end method

.method public d()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->u()Lcom/google/googlenav/ui/wizard/cH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cH;->j()V

    goto :goto_0
.end method

.method public d(Lax/b;)V
    .locals 1

    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/b;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jw;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/jw;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public e(Lax/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->f()V

    :goto_0
    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lax/b;->B()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->d(Lax/b;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->A()Lcom/google/googlenav/ui/wizard/fe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fe;->j()V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->V()Lcom/google/googlenav/ui/wizard/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eq;->j()V

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/jy;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/jy;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->L()Lcom/google/googlenav/ui/wizard/ik;

    move-result-object v2

    invoke-virtual {v1}, LaS/a;->u()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1}, LaS/a;->t()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/im;Ljava/util/EnumSet;Ljava/util/EnumSet;)V

    const/16 v0, 0x61

    const-string v1, "f"

    const-string v2, "o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->e()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->j()V

    return-void
.end method

.method public k()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->j()V

    const/16 v0, 0xa

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/google/googlenav/ui/wizard/C;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    return-void
.end method

.method public o()Lcom/google/googlenav/ui/wizard/A;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    instance-of v1, v0, Lcom/google/googlenav/ui/wizard/dg;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    :goto_1
    return-object v0

    :cond_1
    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/bN;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    goto :goto_1
.end method

.method public p()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->k()I

    move-result v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    return v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public q()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jz;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/wizard/jz;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lax/b;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method public r()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->ah_()I

    move-result v0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/ca;

    return v0
.end method

.method public w()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->af_()V

    :cond_0
    return-void
.end method

.method public x()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->m()V

    :cond_0
    return-void
.end method

.method public y()Lcom/google/googlenav/ui/wizard/z;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->e:Lcom/google/googlenav/ui/wizard/z;

    return-object v0
.end method

.method public z()Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method
