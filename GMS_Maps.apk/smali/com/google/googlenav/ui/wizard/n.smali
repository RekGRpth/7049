.class Lcom/google/googlenav/ui/wizard/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/n;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lcom/google/googlenav/ui/wizard/o;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/o;-><init>(Lcom/google/googlenav/ui/wizard/n;Lcom/google/googlenav/ui/wizard/f;)V

    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/wizard/o;->a:Landroid/widget/TextView;

    const v0, 0x1020015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/wizard/o;->b:Landroid/widget/TextView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 3

    check-cast p2, Lcom/google/googlenav/ui/wizard/o;

    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/o;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/n;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/o;->b:Landroid/widget/TextView;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/n;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1, v2}, Lcom/google/googlenav/d;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x1090004

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/n;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method
