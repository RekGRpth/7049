.class Lcom/google/googlenav/wallpaper/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/wallpaper/b;

.field private final b:Lcom/google/googlenav/wallpaper/f;

.field private c:Landroid/location/Location;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/wallpaper/b;)V
    .locals 6

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/g;->a:Lcom/google/googlenav/wallpaper/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/wallpaper/f;

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/g;->a:Lcom/google/googlenav/wallpaper/b;

    const-wide v2, 0x4042b5bb9496249aL

    const-wide v4, -0x3fa17abfbdf090f7L

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/wallpaper/f;-><init>(Lcom/google/googlenav/wallpaper/b;DD)V

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/g;->b:Lcom/google/googlenav/wallpaper/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/g;->c:Landroid/location/Location;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/wallpaper/g;-><init>(Lcom/google/googlenav/wallpaper/b;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/wallpaper/f;
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/g;->c:Landroid/location/Location;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/g;->b:Lcom/google/googlenav/wallpaper/f;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/googlenav/wallpaper/f;

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/g;->a:Lcom/google/googlenav/wallpaper/b;

    iget-object v2, p0, Lcom/google/googlenav/wallpaper/g;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/googlenav/wallpaper/g;->c:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/wallpaper/f;-><init>(Lcom/google/googlenav/wallpaper/b;DD)V

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/g;->c:Landroid/location/Location;

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/g;->a:Lcom/google/googlenav/wallpaper/b;

    invoke-virtual {v0}, Lcom/google/googlenav/wallpaper/b;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/g;->a:Lcom/google/googlenav/wallpaper/b;

    invoke-static {v0}, Lcom/google/googlenav/wallpaper/b;->b(Lcom/google/googlenav/wallpaper/b;)V

    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
