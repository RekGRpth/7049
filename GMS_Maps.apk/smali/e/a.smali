.class public Le/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Le/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Le/b;

    invoke-direct {v0}, Le/b;-><init>()V

    sput-object v0, Le/a;->a:Le/d;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Le/c;

    invoke-direct {v0}, Le/c;-><init>()V

    sput-object v0, Le/a;->a:Le/d;

    goto :goto_0
.end method

.method public static a(Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 1

    sget-object v0, Le/a;->a:Le/d;

    invoke-interface {v0, p0}, Le/d;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    return v0
.end method
