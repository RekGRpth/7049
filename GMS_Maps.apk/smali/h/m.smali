.class public Lh/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lh/l;


# instance fields
.field private final a:Lh/o;

.field private final b:Lh/i;

.field private final c:Lh/j;

.field private final d:Lh/j;


# direct methods
.method public constructor <init>()V
    .locals 5

    const v2, 0x3f7d70a4

    const-wide/16 v3, 0x1388

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh/o;

    new-instance v1, Lh/d;

    invoke-direct {v1, v2}, Lh/d;-><init>(F)V

    invoke-direct {v0, v1}, Lh/o;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lh/m;->a:Lh/o;

    new-instance v0, Lh/j;

    new-instance v1, Lh/d;

    invoke-direct {v1, v2}, Lh/d;-><init>(F)V

    invoke-direct {v0, v1}, Lh/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lh/m;->c:Lh/j;

    new-instance v0, Lh/i;

    new-instance v1, Lh/b;

    const/high16 v2, 0x3f800000

    invoke-direct {v1, v2}, Lh/b;-><init>(F)V

    invoke-direct {v0, v1}, Lh/i;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lh/m;->b:Lh/i;

    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0, v3, v4}, Lh/o;->setDuration(J)V

    iget-object v0, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v0, v3, v4}, Lh/i;->setDuration(J)V

    iget-object v0, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v0, v3, v4}, Lh/j;->setDuration(J)V

    new-instance v0, Lh/j;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, v1}, Lh/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lh/m;->d:Lh/j;

    iget-object v0, p0, Lh/m;->d:Lh/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/m;->d:Lh/j;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/m;->d:Lh/j;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lh/j;->setDuration(J)V

    iget-object v0, p0, Lh/m;->d:Lh/j;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lh/j;->setRepeatCount(I)V

    iget-object v0, p0, Lh/m;->d:Lh/j;

    invoke-virtual {v0}, Lh/j;->start()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)I
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v1}, Lh/o;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lh/m;->d:Lh/j;

    invoke-virtual {v1, p1, p2}, Lh/j;->a(J)V

    iget-object v1, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v1}, Lh/o;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v1}, Lh/i;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v1}, Lh/j;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v1}, Lh/o;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v1, v0

    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/T;

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v3

    div-double v0, v1, v3

    const-wide/high16 v2, 0x4059000000000000L

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lh/m;->a:Lh/o;

    iget-object v1, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v1}, Lh/o;->b()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v2}, Lh/o;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lh/o;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0, p1, p2}, Lh/o;->b(J)V

    iget-object v0, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v0, p1, p2}, Lh/i;->b(J)V

    iget-object v0, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v0, p1, p2}, Lh/j;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LC/a;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lo/S;)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v1}, Lh/i;->b()F

    move-result v1

    iget-object v2, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v2}, Lh/j;->b()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lo/S;->a(Lo/T;FI)V

    iget-object v0, p0, Lh/m;->d:Lh/j;

    invoke-virtual {v0}, Lh/j;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lo/S;->b(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lo/S;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v0

    iget-object v1, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v1}, Lh/o;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh/o;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lh/m;->a:Lh/o;

    invoke-virtual {v0}, Lh/o;->start()V

    :cond_1
    iget-object v0, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v0}, Lh/i;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lo/S;->b()F

    move-result v0

    iget-object v1, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v1}, Lh/i;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lh/m;->b:Lh/i;

    invoke-virtual {p1}, Lo/S;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lh/i;->a(F)V

    iget-object v0, p0, Lh/m;->b:Lh/i;

    invoke-virtual {v0}, Lh/i;->start()V

    :cond_3
    iget-object v0, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v0}, Lh/j;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lo/S;->c()I

    move-result v0

    iget-object v1, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v1}, Lh/j;->a()I

    move-result v1

    if-eq v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lh/m;->c:Lh/j;

    invoke-virtual {p1}, Lo/S;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lh/j;->a(I)V

    iget-object v0, p0, Lh/m;->c:Lh/j;

    invoke-virtual {v0}, Lh/j;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
