.class public Lk/d;
.super Lk/g;
.source "SourceFile"


# instance fields
.field private d:Lo/aQ;

.field private final e:Ljava/util/List;

.field private final f:Lo/T;

.field private g:LC/a;

.field private h:Lo/aQ;

.field private i:F

.field private final j:F

.field private k:J


# direct methods
.method public constructor <init>(LA/c;ILA/b;)V
    .locals 2

    invoke-direct {p0, p1, p3}, Lk/g;-><init>(LA/c;LA/b;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lk/d;->e:Ljava/util/List;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lk/d;->f:Lo/T;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lk/d;->k:J

    mul-int v0, p2, p2

    int-to-float v0, v0

    iput v0, p0, Lk/d;->j:F

    return-void
.end method

.method private a(Lo/aq;Lo/T;Z)V
    .locals 6

    const/4 v5, 0x1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lk/d;->h:Lo/aQ;

    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aQ;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/high16 v1, 0x20000000

    shr-int/2addr v1, v0

    iget-object v2, p0, Lk/d;->f:Lo/T;

    invoke-virtual {p1}, Lo/aq;->e()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p1}, Lo/aq;->f()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Lo/T;->d(II)V

    iget-object v2, p0, Lk/d;->g:LC/a;

    iget-object v3, p0, Lk/d;->f:Lo/T;

    invoke-virtual {v2, v3, v5}, LC/a;->a(Lo/T;Z)F

    move-result v2

    iget-object v3, p0, Lk/d;->g:LC/a;

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v3, v1, v2}, LC/a;->b(FF)F

    move-result v1

    iget v2, p0, Lk/d;->i:F

    mul-float/2addr v2, v1

    mul-float/2addr v1, v2

    iget v2, p0, Lk/d;->j:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_3

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, p1, p2}, Lk/d;->b(Lo/aq;Lo/T;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-direct {p0, v0, p2, v5}, Lk/d;->a(Lo/aq;Lo/T;Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->k()Lo/aB;

    move-result-object v0

    iget-object v1, p0, Lk/d;->b:LA/b;

    invoke-interface {v1}, LA/b;->a()Lo/aB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aB;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lk/d;->k:J

    return-wide v0
.end method

.method public a(LC/a;)Ljava/util/List;
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v3

    iget-object v0, p0, Lk/d;->d:Lo/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/d;->d:Lo/aQ;

    invoke-virtual {v3, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lk/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lk/d;->k:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, p0, Lk/d;->k:J

    invoke-virtual {v3}, Lo/aQ;->c()Lo/ae;

    move-result-object v0

    check-cast v0, Lo/t;

    invoke-virtual {v0}, Lo/t;->g()Lo/T;

    move-result-object v1

    invoke-virtual {v0}, Lo/t;->f()Lo/T;

    move-result-object v0

    invoke-virtual {v1, v0}, Lo/T;->c(Lo/T;)F

    move-result v0

    invoke-virtual {p1}, LC/a;->k()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LC/a;->c(FF)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lk/d;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lk/d;->g:LC/a;

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    iput-object v1, p0, Lk/d;->h:Lo/aQ;

    invoke-virtual {p1}, LC/a;->q()F

    move-result v1

    const v4, 0x3c8efa35

    mul-float/2addr v1, v4

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v1

    iput v1, p0, Lk/d;->i:F

    invoke-virtual {v3}, Lo/aQ;->a()Lo/aR;

    move-result-object v1

    iget-object v4, p0, Lk/d;->b:LA/b;

    invoke-interface {v4}, LA/b;->a()Lo/aB;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v5

    invoke-direct {p0, v0, v5, v2}, Lk/d;->a(Lo/aq;Lo/T;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iput-object v3, p0, Lk/d;->d:Lo/aQ;

    iget-object v0, p0, Lk/d;->e:Ljava/util/List;

    goto :goto_0
.end method
