.class abstract Lm/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(III)I
    .locals 5

    const-wide/16 v3, 0x0

    invoke-virtual {p0, p1, p2, p3}, Lm/a;->a(III)D

    move-result-wide v0

    cmpl-double v2, v0, v3

    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    cmpl-double v0, v0, v3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract a(I)D
.end method

.method public a(III)D
    .locals 12

    invoke-virtual {p0, p1}, Lm/a;->a(I)D

    move-result-wide v0

    invoke-virtual {p0, p1}, Lm/a;->b(I)D

    move-result-wide v2

    invoke-virtual {p0, p2}, Lm/a;->a(I)D

    move-result-wide v4

    invoke-virtual {p0, p2}, Lm/a;->b(I)D

    move-result-wide v6

    invoke-virtual {p0, p3}, Lm/a;->a(I)D

    move-result-wide v8

    invoke-virtual {p0, p3}, Lm/a;->b(I)D

    move-result-wide v10

    invoke-static/range {v0 .. v11}, Lm/y;->a(DDDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public abstract a()I
.end method

.method public a(II)I
    .locals 8

    invoke-virtual {p0, p1}, Lm/a;->a(I)D

    move-result-wide v0

    invoke-virtual {p0, p1}, Lm/a;->b(I)D

    move-result-wide v2

    invoke-virtual {p0, p2}, Lm/a;->a(I)D

    move-result-wide v4

    invoke-virtual {p0, p2}, Lm/a;->b(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lm/y;->a(DDDD)I

    move-result v0

    return v0
.end method

.method abstract a(I[DII)V
.end method

.method public a(IIIII)Z
    .locals 8

    const/4 v3, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2}, Lm/a;->a(II)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p3}, Lm/a;->a(II)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p4}, Lm/a;->a(II)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, p5}, Lm/a;->a(II)I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, p4, p1, p5}, Lm/a;->c(III)I

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    invoke-direct {p0, p2, p1, p5}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0, p4, p1, p3}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-direct {p0, p2, p1, p4}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0, p3, p1, p5}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p0, p2, p1}, Lm/a;->a(II)I

    move-result v2

    invoke-virtual {p0, p4, p1}, Lm/a;->a(II)I

    move-result v3

    if-eq v2, v3, :cond_6

    invoke-virtual {p0, p3, p1}, Lm/a;->a(II)I

    move-result v2

    invoke-virtual {p0, p5, p1}, Lm/a;->a(II)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    invoke-direct {p0, p2, p1, p4}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {p0, p2, p1}, Lm/a;->a(II)I

    move-result v2

    invoke-virtual {p0, p4, p1}, Lm/a;->a(II)I

    move-result v3

    if-eq v2, v3, :cond_8

    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p2, p1, p5}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    invoke-direct {p0, p3, p1, p5}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {p0, p3, p1}, Lm/a;->a(II)I

    move-result v2

    invoke-virtual {p0, p5, p1}, Lm/a;->a(II)I

    move-result v3

    if-eq v2, v3, :cond_a

    invoke-direct {p0, p3, p1, p2}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p3, p1, p4}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v2

    if-nez v2, :cond_14

    move v2, p5

    move v4, p4

    move v5, p3

    move v6, p2

    :goto_1
    invoke-direct {p0, p2, p1, p5}, Lm/a;->c(III)I

    move-result v7

    if-nez v7, :cond_c

    move v2, p3

    move v4, p4

    move v5, p5

    move v6, p2

    :cond_c
    invoke-direct {p0, p3, p1, p4}, Lm/a;->c(III)I

    move-result v7

    if-nez v7, :cond_d

    move v2, p2

    move v4, p5

    move v5, p4

    move v6, p3

    :cond_d
    invoke-direct {p0, p4, p1, p5}, Lm/a;->c(III)I

    move-result v7

    if-nez v7, :cond_e

    move v2, p3

    move v4, p2

    move v5, p5

    move v6, p4

    :cond_e
    if-eq v6, v3, :cond_10

    invoke-virtual {p0, p1, v6}, Lm/a;->a(II)I

    move-result v3

    invoke-virtual {p0, p1, v5}, Lm/a;->a(II)I

    move-result v5

    if-eq v3, v5, :cond_f

    invoke-direct {p0, v6, p1, v4}, Lm/a;->c(III)I

    move-result v3

    invoke-direct {p0, v6, p1, v2}, Lm/a;->c(III)I

    move-result v5

    if-ne v3, v5, :cond_f

    invoke-direct {p0, v4, p1, v6}, Lm/a;->c(III)I

    move-result v3

    invoke-direct {p0, v4, p1, v2}, Lm/a;->c(III)I

    move-result v2

    if-eq v3, v2, :cond_1

    :cond_f
    move v0, v1

    goto/16 :goto_0

    :cond_10
    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p2, p1, p5}, Lm/a;->c(III)I

    move-result v3

    if-eq v2, v3, :cond_12

    invoke-direct {p0, p2, p1, p4}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_11

    :goto_2
    invoke-direct {p0, p3, p1, p2}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p3, p1, p4}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_1

    move v0, v1

    goto/16 :goto_0

    :cond_11
    move p3, p5

    goto :goto_2

    :cond_12
    invoke-direct {p0, p2, p1, p3}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p2, p1, p4}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_13

    invoke-direct {p0, p4, p1, p3}, Lm/a;->c(III)I

    move-result v2

    invoke-direct {p0, p4, p1, p5}, Lm/a;->c(III)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_13
    move v0, v1

    goto/16 :goto_0

    :cond_14
    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    goto :goto_1
.end method

.method public abstract b(I)D
.end method

.method public b(III)D
    .locals 8

    invoke-virtual {p0, p1}, Lm/a;->a(I)D

    move-result-wide v0

    invoke-virtual {p0, p2}, Lm/a;->a(I)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-virtual {p0, p3}, Lm/a;->b(I)D

    move-result-wide v2

    invoke-virtual {p0, p2}, Lm/a;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    invoke-virtual {p0, p1}, Lm/a;->b(I)D

    move-result-wide v2

    invoke-virtual {p0, p2}, Lm/a;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-virtual {p0, p3}, Lm/a;->a(I)D

    move-result-wide v4

    invoke-virtual {p0, p2}, Lm/a;->a(I)D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    neg-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public abstract c(I)I
.end method

.method public abstract d(I)I
.end method

.method public abstract e(I)Lm/c;
.end method

.method public f(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget v0, p0, Lm/a;->a:I

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public g(I)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget v0, p0, Lm/a;->a:I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method protected h(I)I
    .locals 2

    iget v0, p0, Lm/a;->a:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lm/a;->a:I

    rem-int v0, p1, v0

    if-gez v0, :cond_1

    iget v1, p0, Lm/a;->a:I

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method
