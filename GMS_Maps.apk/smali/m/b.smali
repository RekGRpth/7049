.class public Lm/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final a:Lm/a;

.field private final b:D

.field private final c:D


# direct methods
.method public constructor <init>(Lm/a;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm/b;->a:Lm/a;

    invoke-virtual {p1, p2}, Lm/a;->a(I)D

    move-result-wide v0

    iput-wide v0, p0, Lm/b;->b:D

    invoke-virtual {p1, p2}, Lm/a;->b(I)D

    move-result-wide v0

    iput-wide v0, p0, Lm/b;->c:D

    return-void
.end method


# virtual methods
.method public a([I[I)I
    .locals 21

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/b;->a:Lm/a;

    const/4 v2, 0x1

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Lm/a;->a(I)D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lm/b;->a:Lm/a;

    const/4 v4, 0x1

    aget v4, p1, v4

    invoke-virtual {v3, v4}, Lm/a;->b(I)D

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/b;->a:Lm/a;

    const/4 v6, 0x1

    aget v6, p2, v6

    invoke-virtual {v5, v6}, Lm/a;->a(I)D

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/b;->a:Lm/a;

    const/4 v6, 0x1

    aget v6, p2, v6

    invoke-virtual {v5, v6}, Lm/a;->b(I)D

    move-result-wide v19

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->b:D

    cmpl-double v5, v5, v1

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->c:D

    cmpl-double v5, v5, v3

    if-nez v5, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->b:D

    cmpl-double v5, v5, v17

    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->c:D

    cmpl-double v5, v5, v19

    if-nez v5, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->b:D

    move-object/from16 v0, p0

    iget-wide v7, v0, Lm/b;->c:D

    invoke-static/range {v1 .. v8}, Lm/y;->a(DDDD)I

    move-result v5

    if-ltz v5, :cond_2

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->b:D

    move-object/from16 v0, p0

    iget-wide v7, v0, Lm/b;->c:D

    invoke-static/range {v1 .. v8}, Lm/y;->a(DDDD)I

    move-result v5

    if-nez v5, :cond_4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->c:D

    cmpg-double v5, v3, v5

    if-gez v5, :cond_4

    :cond_2
    const/4 v5, 0x1

    move v13, v5

    :goto_1
    move-object/from16 v0, p0

    iget-wide v9, v0, Lm/b;->b:D

    move-object/from16 v0, p0

    iget-wide v11, v0, Lm/b;->c:D

    move-wide/from16 v5, v17

    move-wide/from16 v7, v19

    invoke-static/range {v5 .. v12}, Lm/y;->a(DDDD)I

    move-result v5

    if-ltz v5, :cond_3

    move-object/from16 v0, p0

    iget-wide v9, v0, Lm/b;->b:D

    move-object/from16 v0, p0

    iget-wide v11, v0, Lm/b;->c:D

    move-wide/from16 v5, v17

    move-wide/from16 v7, v19

    invoke-static/range {v5 .. v12}, Lm/y;->a(DDDD)I

    move-result v5

    if-gez v5, :cond_5

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->c:D

    cmpg-double v5, v19, v5

    if-gez v5, :cond_5

    :cond_3
    const/4 v5, 0x1

    :goto_2
    if-ne v13, v5, :cond_9

    move-object/from16 v0, p0

    iget-wide v5, v0, Lm/b;->b:D

    move-object/from16 v0, p0

    iget-wide v7, v0, Lm/b;->c:D

    move-wide/from16 v9, v17

    move-wide/from16 v11, v19

    move-wide v13, v1

    move-wide v15, v3

    invoke-static/range {v5 .. v16}, Lm/y;->a(DDDDDD)D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpl-double v7, v5, v7

    if-nez v7, :cond_7

    move-wide/from16 v5, v17

    move-wide/from16 v7, v19

    invoke-static/range {v1 .. v8}, Lm/y;->a(DDDD)I

    move-result v1

    if-lez v1, :cond_6

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    move v13, v5

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    :cond_6
    const/4 v1, -0x1

    goto/16 :goto_0

    :cond_7
    const-wide/16 v1, 0x0

    cmpl-double v1, v5, v1

    if-lez v1, :cond_8

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_8
    const/4 v1, -0x1

    goto/16 :goto_0

    :cond_9
    if-eqz v13, :cond_a

    const/4 v1, -0x1

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, [I

    check-cast p2, [I

    invoke-virtual {p0, p1, p2}, Lm/b;->a([I[I)I

    move-result v0

    return v0
.end method
