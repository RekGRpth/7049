.class Lm/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lm/z;

.field private final b:Lm/z;

.field private final c:Lm/z;

.field private final d:Lm/z;


# direct methods
.method public constructor <init>(Lm/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lm/r;->b()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {p1}, Lm/r;->b()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->b:Lm/z;

    invoke-virtual {p1}, Lm/r;->b()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->c:Lm/z;

    invoke-virtual {p1}, Lm/r;->b()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->d:Lm/z;

    return-void
.end method

.method public constructor <init>(Lm/z;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lm/z;->c()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {p1}, Lm/z;->c()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->b:Lm/z;

    invoke-virtual {p1}, Lm/z;->c()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->c:Lm/z;

    invoke-virtual {p1}, Lm/z;->c()Lm/z;

    move-result-object v0

    iput-object v0, p0, Lm/d;->d:Lm/z;

    return-void
.end method

.method private constructor <init>(Lm/z;Lm/z;Lm/z;Lm/z;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm/d;->a:Lm/z;

    iput-object p2, p0, Lm/d;->b:Lm/z;

    iput-object p3, p0, Lm/d;->c:Lm/z;

    iput-object p4, p0, Lm/d;->d:Lm/z;

    return-void
.end method

.method private a(ID)D
    .locals 10

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->g(I)D

    move-result-wide v4

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->h(I)D

    move-result-wide v0

    cmpl-double v2, v4, p2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v2, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v2, p1}, Lm/z;->g(I)D

    move-result-wide v6

    iget-object v2, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v2, p1}, Lm/z;->h(I)D

    move-result-wide v2

    cmpl-double v8, v6, p2

    if-nez v8, :cond_2

    move-wide v0, v2

    goto :goto_0

    :cond_2
    sub-double/2addr v6, v4

    const-wide/16 v8, 0x0

    cmpl-double v8, v6, v8

    if-nez v8, :cond_3

    cmpl-double v4, v0, v2

    if-gtz v4, :cond_0

    move-wide v0, v2

    goto :goto_0

    :cond_3
    sub-double/2addr v2, v0

    sub-double v4, p2, v4

    mul-double/2addr v2, v4

    div-double/2addr v2, v6

    add-double/2addr v0, v2

    goto :goto_0
.end method

.method private a(IIIII)V
    .locals 1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1, p2}, Lm/z;->e(II)V

    iget-object v0, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v0, p1, p3}, Lm/z;->e(II)V

    iget-object v0, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v0, p1, p4}, Lm/z;->e(II)V

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, p1, p5}, Lm/z;->e(II)V

    return-void
.end method

.method private c(III)I
    .locals 0

    if-gt p1, p2, :cond_0

    :goto_0
    return p1

    :cond_0
    sub-int/2addr p1, p3

    goto :goto_0
.end method

.method private c(I)V
    .locals 1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->a(I)V

    iget-object v0, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->a(I)V

    iget-object v0, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->a(I)V

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->a(I)V

    return-void
.end method

.method private d(I)I
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->b(I)D

    move-result-wide v2

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->c(I)D

    move-result-wide v4

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v2, v3}, Lm/d;->a(ID)D

    move-result-wide v6

    cmpg-double v6, v6, v4

    if-gez v6, :cond_0

    iget-object v6, p0, Lm/d;->a:Lm/z;

    iget v6, v6, Lm/z;->d:I

    if-ge v0, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    add-int/lit8 v1, v0, -0x1

    :cond_1
    return v1
.end method

.method private d(III)I
    .locals 25

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lm/z;->b(I)D

    move-result-wide v21

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lm/z;->c(I)D

    move-result-wide v23

    const/4 v4, 0x0

    move/from16 v20, v4

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->a:Lm/z;

    iget v4, v4, Lm/z;->d:I

    move/from16 v0, v20

    if-ge v0, v4, :cond_3

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-direct {v0, v1, v2, v3}, Lm/d;->a(ID)D

    move-result-wide v4

    cmpl-double v6, v4, v23

    if-lez v6, :cond_1

    :cond_0
    :goto_1
    return v20

    :cond_1
    cmpl-double v4, v4, v23

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->b:Lm/z;

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lm/z;->g(I)D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->b:Lm/z;

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lm/z;->h(I)D

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Lm/z;->b(I)D

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Lm/z;->c(I)D

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Lm/z;->b(I)D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Lm/z;->c(I)D

    move-result-wide v10

    invoke-static/range {v4 .. v15}, Lm/y;->a(DDDDDD)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_0

    move-wide/from16 v16, v8

    move-wide/from16 v18, v10

    invoke-static/range {v12 .. v19}, Lm/y;->a(DDDD)I

    move-result v4

    if-nez v4, :cond_2

    rem-int/lit8 v4, v20, 0x2

    if-eqz v4, :cond_0

    :cond_2
    add-int/lit8 v4, v20, 0x1

    move/from16 v20, v4

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->a:Lm/z;

    iget v0, v4, Lm/z;->d:I

    move/from16 v20, v0

    goto :goto_1
.end method

.method private e(III)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lm/d;->g(II)I

    move-result v0

    invoke-direct {p0, v0, p3}, Lm/d;->h(II)I

    move-result v0

    return v0
.end method

.method private e(II)Z
    .locals 27

    if-ltz p1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    iget v1, v1, Lm/z;->d:I

    move/from16 v0, p1

    if-ge v0, v1, :cond_c

    if-ltz p2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    iget v1, v1, Lm/z;->d:I

    move/from16 v0, p2

    if-ge v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lm/z;->g(I)D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lm/z;->h(I)D

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Lm/z;->g(I)D

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v7, v0}, Lm/z;->h(I)D

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Lm/z;->g(I)D

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Lm/z;->h(I)D

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Lm/z;->g(I)D

    move-result-wide v21

    move-object/from16 v0, p0

    iget-object v13, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Lm/z;->h(I)D

    move-result-wide v23

    cmpl-double v13, v1, v21

    if-nez v13, :cond_0

    cmpl-double v13, v3, v23

    if-eqz v13, :cond_1

    :cond_0
    cmpl-double v13, v9, v5

    if-nez v13, :cond_2

    cmpl-double v13, v11, v7

    if-nez v13, :cond_2

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_2
    cmpl-double v13, v1, v9

    if-nez v13, :cond_3

    cmpl-double v13, v3, v11

    if-eqz v13, :cond_4

    :cond_3
    cmpl-double v13, v5, v21

    if-nez v13, :cond_9

    cmpl-double v13, v7, v23

    if-nez v13, :cond_9

    :cond_4
    cmpl-double v1, v1, v9

    if-nez v1, :cond_8

    cmpl-double v1, v3, v11

    if-nez v1, :cond_8

    cmpl-double v1, v5, v21

    if-nez v1, :cond_8

    cmpl-double v1, v7, v23

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lm/z;->j(I)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lm/z;->j(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lm/z;->j(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lm/z;->j(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/d;->a:Lm/z;

    invoke-virtual {v5, v1}, Lm/z;->d(I)I

    move-result v1

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lm/d;->a:Lm/z;

    invoke-virtual {v3, v2}, Lm/z;->d(I)I

    move-result v2

    if-ne v2, v4, :cond_6

    const/4 v2, 0x1

    :goto_2
    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    :cond_8
    const/4 v1, 0x0

    goto :goto_0

    :cond_9
    invoke-static/range {v1 .. v12}, Lm/y;->a(DDDDDD)D

    move-result-wide v25

    move-wide v13, v1

    move-wide v15, v3

    move-wide/from16 v17, v5

    move-wide/from16 v19, v7

    invoke-static/range {v13 .. v24}, Lm/y;->a(DDDDDD)D

    move-result-wide v13

    mul-double v13, v13, v25

    const-wide/16 v15, 0x0

    cmpl-double v13, v13, v15

    if-lez v13, :cond_a

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_a
    move-wide/from16 v13, v21

    move-wide/from16 v15, v23

    move-wide/from16 v17, v1

    move-wide/from16 v19, v3

    invoke-static/range {v9 .. v20}, Lm/y;->a(DDDDDD)D

    move-result-wide v1

    move-wide/from16 v13, v21

    move-wide/from16 v15, v23

    move-wide/from16 v17, v5

    move-wide/from16 v19, v7

    invoke-static/range {v9 .. v20}, Lm/y;->a(DDDDDD)D

    move-result-wide v3

    mul-double/2addr v1, v3

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_b

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private f(II)I
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v0, p1, p2}, Lm/z;->f(II)V

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->j(I)I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v1, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v1, p1, v2}, Lm/z;->f(II)V

    :cond_0
    return v0
.end method

.method private g(II)I
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lm/d;->b:Lm/z;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lm/z;->h(II)I

    move-result v0

    :goto_0
    if-eq v0, v2, :cond_0

    invoke-direct {p0, v0, p1}, Lm/d;->i(II)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lm/d;->b:Lm/z;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, p2, v0}, Lm/z;->h(II)I

    move-result v0

    goto :goto_0

    :cond_0
    if-ne v0, v2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Could not find egde in EdgeList."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return v0
.end method

.method private h(II)I
    .locals 22

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->c:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lm/z;->j(I)I

    move-result v21

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, v21

    invoke-virtual {v1, v0}, Lm/z;->b(I)D

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, v21

    invoke-virtual {v1, v0}, Lm/z;->c(I)D

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lm/z;->g(I)D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lm/z;->h(I)D

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Lm/z;->g(I)D

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lm/d;->b:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v7, v0}, Lm/z;->h(I)D

    move-result-wide v7

    invoke-static/range {v1 .. v12}, Lm/y;->a(DDDDDD)D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpg-double v5, v5, v7

    if-gtz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lm/z;->b(I)D

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v5, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lm/z;->c(I)D

    move-result-wide v19

    move-wide v13, v1

    move-wide v15, v3

    invoke-static/range {v9 .. v20}, Lm/y;->a(DDDDDD)D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lm/d;->a:Lm/z;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lm/z;->j(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    move/from16 v1, v21

    goto :goto_0
.end method

.method private i(II)Z
    .locals 1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p2, p1}, Lm/z;->i(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    invoke-direct {p0, p1}, Lm/d;->d(I)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lm/d;->h(II)I

    move-result v0

    return v0
.end method

.method public a(IILm/c;)I
    .locals 7

    const/4 v5, -0x1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1, p2}, Lm/z;->g(II)I

    move-result v0

    if-gez v0, :cond_0

    invoke-direct {p0, p1, p2, p1}, Lm/d;->d(III)I

    move-result v1

    sget-object v0, Lm/e;->a:[I

    invoke-virtual {p3}, Lm/c;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lm/j;

    const-string v1, "Impossible EdgeList start case."

    invoke-direct {v0, v1}, Lm/j;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1}, Lm/z;->d(I)I

    move-result v0

    invoke-direct {p0, v0, p1, p1}, Lm/d;->e(III)I

    move-result v4

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    :goto_0
    return v5

    :cond_0
    invoke-direct {p0, p2, p1, p2}, Lm/d;->d(III)I

    move-result v2

    move-object v1, p0

    move v3, p2

    move v4, p1

    move v6, v5

    invoke-direct/range {v1 .. v6}, Lm/d;->a(IIIII)V

    invoke-direct {p0, p2}, Lm/d;->d(I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lm/d;->f(II)I

    move-result v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lm/u;II)Lm/d;
    .locals 15

    invoke-virtual/range {p1 .. p1}, Lm/u;->c()Lm/z;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lm/u;->c()Lm/z;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lm/u;->c()Lm/z;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lm/u;->c()Lm/z;

    move-result-object v6

    move/from16 v0, p2

    move/from16 v1, p3

    if-le v0, v1, :cond_7

    sub-int v2, p2, p3

    add-int/lit8 v7, v2, -0x1

    const/4 v2, 0x0

    :goto_0
    iget-object v8, p0, Lm/d;->a:Lm/z;

    iget v8, v8, Lm/z;->d:I

    if-ge v2, v8, :cond_d

    iget-object v8, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v8, v2}, Lm/z;->j(I)I

    move-result v8

    move/from16 v0, p3

    if-le v8, v0, :cond_0

    move/from16 v0, p2

    if-lt v8, v0, :cond_4

    :cond_0
    iget-object v9, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v9, v2}, Lm/z;->j(I)I

    move-result v9

    move/from16 v0, p3

    if-le v9, v0, :cond_1

    move/from16 v0, p2

    if-lt v9, v0, :cond_4

    :cond_1
    iget-object v10, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v10, v2}, Lm/z;->j(I)I

    move-result v10

    iget-object v11, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v11, v2}, Lm/z;->j(I)I

    move-result v11

    const/4 v12, 0x1

    new-array v12, v12, [I

    const/4 v13, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v8, v0, v7}, Lm/d;->c(III)I

    move-result v14

    aput v14, v12, v13

    invoke-virtual {v3, v12}, Lm/z;->b([I)V

    const/4 v12, 0x1

    new-array v12, v12, [I

    const/4 v13, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v9, v0, v7}, Lm/d;->c(III)I

    move-result v9

    aput v9, v12, v13

    invoke-virtual {v4, v12}, Lm/z;->b([I)V

    move/from16 v0, p3

    if-le v10, v0, :cond_2

    move/from16 v0, p2

    if-lt v10, v0, :cond_5

    :cond_2
    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v10, v0, v7}, Lm/d;->c(III)I

    move-result v10

    aput v10, v8, v9

    invoke-virtual {v5, v8}, Lm/z;->b([I)V

    :goto_1
    move/from16 v0, p3

    if-le v11, v0, :cond_3

    move/from16 v0, p2

    if-lt v11, v0, :cond_6

    :cond_3
    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v11, v0, v7}, Lm/d;->c(III)I

    move-result v10

    aput v10, v8, v9

    invoke-virtual {v6, v8}, Lm/z;->b([I)V

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v8, v0, v7}, Lm/d;->c(III)I

    move-result v8

    aput v8, v9, v10

    invoke-virtual {v5, v9}, Lm/z;->b([I)V

    goto :goto_1

    :cond_6
    new-instance v2, Lm/j;

    const-string v3, "When cutting edge list, we lost a merge vertex."

    invoke-direct {v2, v3}, Lm/j;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    const/4 v2, 0x0

    :goto_2
    iget-object v7, p0, Lm/d;->a:Lm/z;

    iget v7, v7, Lm/z;->d:I

    if-ge v2, v7, :cond_d

    iget-object v7, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v7, v2}, Lm/z;->j(I)I

    move-result v7

    move/from16 v0, p2

    if-lt v7, v0, :cond_8

    move/from16 v0, p3

    if-gt v7, v0, :cond_8

    iget-object v8, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v8, v2}, Lm/z;->j(I)I

    move-result v8

    move/from16 v0, p2

    if-lt v8, v0, :cond_8

    move/from16 v0, p3

    if-gt v8, v0, :cond_8

    iget-object v9, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v9, v2}, Lm/z;->j(I)I

    move-result v9

    iget-object v10, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v10, v2}, Lm/z;->j(I)I

    move-result v10

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    sub-int v13, v7, p2

    aput v13, v11, v12

    invoke-virtual {v3, v11}, Lm/z;->b([I)V

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    sub-int v8, v8, p2

    aput v8, v11, v12

    invoke-virtual {v4, v11}, Lm/z;->b([I)V

    const/4 v8, -0x1

    if-ne v9, v8, :cond_9

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v9, v7, v8

    invoke-virtual {v5, v7}, Lm/z;->b([I)V

    :goto_3
    const/4 v7, -0x1

    if-ne v10, v7, :cond_b

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v10, v7, v8

    invoke-virtual {v6, v7}, Lm/z;->b([I)V

    :cond_8
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    move/from16 v0, p2

    if-lt v9, v0, :cond_a

    move/from16 v0, p3

    if-gt v9, v0, :cond_a

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    sub-int v9, v9, p2

    aput v9, v7, v8

    invoke-virtual {v5, v7}, Lm/z;->b([I)V

    goto :goto_3

    :cond_a
    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    sub-int v7, v7, p2

    aput v7, v8, v9

    invoke-virtual {v5, v8}, Lm/z;->b([I)V

    goto :goto_3

    :cond_b
    move/from16 v0, p2

    if-lt v10, v0, :cond_c

    move/from16 v0, p3

    if-gt v10, v0, :cond_c

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    sub-int v9, v10, p2

    aput v9, v7, v8

    invoke-virtual {v6, v7}, Lm/z;->b([I)V

    goto :goto_4

    :cond_c
    new-instance v2, Lm/j;

    const-string v3, "When cutting edge list, we lost a merge vertex."

    invoke-direct {v2, v3}, Lm/j;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d
    new-instance v2, Lm/d;

    invoke-direct {v2, v3, v4, v5, v6}, Lm/d;-><init>(Lm/z;Lm/z;Lm/z;Lm/z;)V

    return-object v2
.end method

.method public a(III)V
    .locals 7

    const/4 v5, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lm/d;->a:Lm/z;

    iget v0, v0, Lm/z;->d:I

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, p3, p2}, Lm/d;->d(III)I

    move-result v1

    :cond_0
    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    add-int/lit8 v2, v1, 0x1

    move-object v1, p0

    move v3, p2

    move v4, p1

    move v6, v5

    invoke-direct/range {v1 .. v6}, Lm/d;->a(IIIII)V

    return-void
.end method

.method public a(II)Z
    .locals 6

    const/4 v4, -0x1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p2, p1}, Lm/z;->g(II)I

    move-result v0

    if-gez v0, :cond_2

    move v3, p1

    move v2, p2

    :goto_0
    invoke-direct {p0, v2, v3, v2}, Lm/d;->d(III)I

    move-result v1

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0, v1}, Lm/d;->e(II)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    invoke-direct {p0, v1, v0}, Lm/d;->e(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v3, p2

    move v2, p1

    goto :goto_0
.end method

.method public b(I)I
    .locals 4

    invoke-direct {p0, p1}, Lm/d;->d(I)I

    move-result v1

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, v1}, Lm/z;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1, p1}, Lm/d;->h(II)I

    move-result v0

    :goto_0
    iget-object v2, p0, Lm/d;->d:Lm/z;

    const/4 v3, -0x1

    invoke-virtual {v2, v1, v3}, Lm/z;->f(II)V

    return v0

    :cond_0
    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, v1}, Lm/z;->j(I)I

    move-result v0

    goto :goto_0
.end method

.method public b(IILm/c;)I
    .locals 5

    const/4 v0, -0x1

    iget-object v1, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v1, p1, p2}, Lm/z;->g(II)I

    move-result v1

    if-gez v1, :cond_2

    invoke-direct {p0, p1, p2}, Lm/d;->g(II)I

    move-result v1

    iget-object v2, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v2, v1}, Lm/z;->i(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, v1}, Lm/z;->j(I)I

    move-result v0

    :cond_0
    sget-object v2, Lm/e;->a:[I

    invoke-virtual {p3}, Lm/c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-direct {p0, v1}, Lm/d;->c(I)V

    return v0

    :pswitch_0
    invoke-direct {p0, p2}, Lm/d;->d(I)I

    move-result v2

    iget-object v3, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v3, v2}, Lm/z;->i(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v0, v2}, Lm/z;->j(I)I

    move-result v0

    :cond_1
    iget-object v3, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v3, v2, p2}, Lm/z;->f(II)V

    iget-object v3, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v3, v2, p2}, Lm/z;->f(II)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2, p1}, Lm/d;->g(II)I

    move-result v2

    invoke-direct {p0, p1}, Lm/d;->d(I)I

    move-result v3

    iget-object v1, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v1, v3}, Lm/z;->i(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v1, v3}, Lm/z;->j(I)I

    move-result v1

    iget-object v4, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v4, v3, v0}, Lm/z;->f(II)V

    move v0, v1

    :cond_3
    iget-object v1, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v1, v3, p1}, Lm/z;->f(II)V

    move v1, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public b(III)V
    .locals 6

    const/4 v5, -0x1

    invoke-direct {p0, p2, p3, p2}, Lm/d;->d(III)I

    move-result v1

    iget-object v0, p0, Lm/d;->a:Lm/z;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Lm/z;->j(I)I

    move-result v0

    iget-object v2, p0, Lm/d;->b:Lm/z;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Lm/z;->j(I)I

    move-result v2

    invoke-direct {p0, v0, v2, p2}, Lm/d;->e(III)I

    move-result v4

    invoke-direct {p0, p2}, Lm/d;->d(I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lm/d;->f(II)I

    move-object v0, p0

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    move-object v0, p0

    move v2, p2

    move v3, p1

    move v4, v5

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    return-void
.end method

.method public b(II)Z
    .locals 2

    iget-object v0, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v0, p1, p2}, Lm/z;->g(II)I

    move-result v0

    if-gez v0, :cond_1

    invoke-direct {p0, p1, p2}, Lm/d;->g(II)I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lm/d;->c(I)V

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1, v0}, Lm/d;->e(II)Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v0, v1}, Lm/d;->e(II)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    invoke-direct {p0, p2, p1}, Lm/d;->g(II)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(II)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v3, -0x1

    iget-object v1, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v1, p2, v0}, Lm/z;->h(II)I

    move-result v1

    :goto_0
    if-eq v1, v3, :cond_0

    invoke-direct {p0, v1, p1}, Lm/d;->i(II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lm/d;->b:Lm/z;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, p2, v1}, Lm/z;->h(II)I

    move-result v1

    goto :goto_0

    :cond_0
    if-eq v1, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public d(II)V
    .locals 6

    invoke-direct {p0, p1, p2, p2}, Lm/d;->d(III)I

    move-result v1

    const/4 v5, -0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lm/d;->a(IIIII)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x3

    const-string v2, "{start:  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lm/d;->a:Lm/z;

    invoke-virtual {v2, v1}, Lm/z;->p(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n end:    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lm/d;->b:Lm/z;

    invoke-virtual {v2, v1}, Lm/z;->p(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n helper: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lm/d;->c:Lm/z;

    invoke-virtual {v2, v1}, Lm/z;->p(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n merge:  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lm/d;->d:Lm/z;

    invoke-virtual {v2, v1}, Lm/z;->p(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
