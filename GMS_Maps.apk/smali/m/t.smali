.class public Lm/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:D

.field private final b:D


# direct methods
.method private constructor <init>(DDZ)V
    .locals 6

    const-wide v4, -0x3ff6de04abbbd2e8L

    const-wide v0, 0x400921fb54442d18L

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p5, :cond_0

    cmpl-double v2, p1, v4

    if-nez v2, :cond_2

    cmpl-double v2, p3, v0

    if-eqz v2, :cond_2

    move-wide v2, v0

    :goto_0
    cmpl-double v4, p3, v4

    if-nez v4, :cond_1

    cmpl-double v4, p1, v0

    if-eqz v4, :cond_1

    move-wide p3, v0

    move-wide p1, v2

    :cond_0
    :goto_1
    iput-wide p1, p0, Lm/t;->a:D

    iput-wide p3, p0, Lm/t;->b:D

    return-void

    :cond_1
    move-wide p1, v2

    goto :goto_1

    :cond_2
    move-wide v2, p1

    goto :goto_0
.end method

.method public static a(I)D
    .locals 4

    invoke-static {p0}, Lo/T;->c(I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x41c0000000000000L

    div-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a()Lm/t;
    .locals 6

    new-instance v0, Lm/t;

    const-wide v1, 0x400921fb54442d18L

    const-wide v3, -0x3ff6de04abbbd2e8L

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    return-object v0
.end method

.method public static a(DD)Lm/t;
    .locals 12

    const/4 v5, 0x1

    const-wide v3, -0x3ff6de04abbbd2e8L

    const-wide v6, 0x400921fb54442d18L

    cmpl-double v0, p0, v3

    if-nez v0, :cond_2

    move-wide v1, v6

    :goto_0
    cmpl-double v0, p2, v3

    if-nez v0, :cond_1

    move-wide v3, v6

    :goto_1
    invoke-static {v1, v2, v3, v4}, Lm/t;->b(DD)D

    move-result-wide v8

    cmpg-double v0, v8, v6

    if-gtz v0, :cond_0

    new-instance v0, Lm/t;

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    :goto_2
    return-object v0

    :cond_0
    new-instance v6, Lm/t;

    move-wide v7, v3

    move-wide v9, v1

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lm/t;-><init>(DDZ)V

    move-object v0, v6

    goto :goto_2

    :cond_1
    move-wide v3, p2

    goto :goto_1

    :cond_2
    move-wide v1, p0

    goto :goto_0
.end method

.method public static b(DD)D
    .locals 6

    const-wide v4, 0x400921fb54442d18L

    sub-double v0, p2, p0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    add-double v0, p2, v4

    sub-double v2, p0, v4

    sub-double/2addr v0, v2

    goto :goto_0
.end method

.method public static b()Lm/t;
    .locals 6

    new-instance v0, Lm/t;

    const-wide v1, -0x3ff6de04abbbd2e8L

    const-wide v3, 0x400921fb54442d18L

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    return-object v0
.end method

.method public static c(D)I
    .locals 4

    const-wide v0, 0x400921fb54442d18L

    div-double v0, p0, v0

    const-wide/high16 v2, 0x41c0000000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a(D)Z
    .locals 2

    const-wide v0, -0x3ff6de04abbbd2e8L

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    const-wide p1, 0x400921fb54442d18L

    :cond_0
    invoke-virtual {p0, p1, p2}, Lm/t;->b(D)Z

    move-result v0

    return v0
.end method

.method public a(Lm/t;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lm/t;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lm/t;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-gez v2, :cond_3

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_4

    :cond_3
    invoke-virtual {p0}, Lm/t;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lm/t;->g()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lm/t;->e()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {p1}, Lm/t;->f()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    move v1, v0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_9

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public b(Lm/t;)Lm/t;
    .locals 8

    const/4 v5, 0x1

    invoke-virtual {p1}, Lm/t;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lm/t;->b(D)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lm/t;->b(D)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lm/t;->a(Lm/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    goto :goto_0

    :cond_1
    invoke-static {}, Lm/t;->b()Lm/t;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lm/t;

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lm/t;->b(D)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lm/t;

    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lm/t;->f()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lm/t;->b(D)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move-object v0, p1

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lm/t;->b(DD)D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lm/t;->b(DD)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_7

    new-instance v0, Lm/t;

    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lm/t;

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    goto/16 :goto_0
.end method

.method public b(D)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lm/t;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    cmpl-double v2, p1, v2

    if-gez v2, :cond_0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Lm/t;->f()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_4

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    cmpg-double v2, p1, v2

    if-lez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public c()D
    .locals 2

    iget-wide v0, p0, Lm/t;->a:D

    return-wide v0
.end method

.method public d()D
    .locals 2

    iget-wide v0, p0, Lm/t;->b:D

    return-wide v0
.end method

.method public e()Z
    .locals 4

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lm/t;

    if-eqz v1, :cond_0

    check-cast p1, Lm/t;

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {p1}, Lm/t;->c()D

    move-result-wide v3

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v1

    invoke-virtual {p1}, Lm/t;->d()D

    move-result-wide v3

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public f()Z
    .locals 4

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 4

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()D
    .locals 8

    const-wide v6, 0x400921fb54442d18L

    const-wide/high16 v0, 0x3fe0000000000000L

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v4

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    invoke-virtual {p0}, Lm/t;->g()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_1

    add-double/2addr v0, v6

    goto :goto_0

    :cond_1
    sub-double/2addr v0, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    const-wide/16 v4, 0x25

    const-wide/16 v0, 0x11

    mul-long/2addr v0, v4

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    mul-long/2addr v0, v4

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public i()Lm/t;
    .locals 6

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Lm/t;->b()Lm/t;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lm/t;

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v1

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v3

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lm/t;-><init>(DDZ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lm/t;->c()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lm/t;->d()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
