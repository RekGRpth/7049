.class public Ln/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/r;

.field private final b:Lo/j;

.field private final c:Lo/T;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lo/r;Lo/j;Lo/T;[Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln/a;->a:Lo/r;

    iput-object p2, p0, Ln/a;->b:Lo/j;

    if-nez p3, :cond_0

    invoke-interface {p2}, Lo/j;->a()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->f()Lo/T;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Ln/a;->c:Lo/T;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/a;->d:Ljava/util/Set;

    array-length v1, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p4, v0

    iget-object v3, p0, Ln/a;->d:Ljava/util/Set;

    invoke-static {v2}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Ljava/util/Collection;Lo/ae;)Ljava/util/Collection;
    .locals 4

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    invoke-virtual {v0, p1}, Ln/a;->a(Lo/ae;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ln/a;
    .locals 9

    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const-string v3, "\\s+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    if-lt v3, v0, :cond_0

    aget-object v3, v2, v8

    invoke-static {v3}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-static {v4}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v4

    const/4 v5, 0x2

    aget-object v5, v2, v5

    invoke-static {v5}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v5

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    array-length v6, v2

    if-le v6, v0, :cond_2

    aget-object v1, v2, v0

    invoke-static {v1}, Ln/a;->b(Ljava/lang/String;)Lo/T;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    :cond_2
    invoke-virtual {v4}, LaN/B;->c()I

    move-result v6

    invoke-virtual {v4}, LaN/B;->e()I

    move-result v4

    invoke-static {v6, v4}, Lo/T;->b(II)Lo/T;

    move-result-object v4

    invoke-virtual {v5}, LaN/B;->c()I

    move-result v6

    invoke-virtual {v5}, LaN/B;->e()I

    move-result v5

    invoke-static {v6, v5}, Lo/T;->b(II)Lo/T;

    move-result-object v5

    array-length v6, v2

    sub-int/2addr v6, v0

    new-array v6, v6, [Ljava/lang/String;

    array-length v7, v6

    invoke-static {v2, v0, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Ln/a;

    invoke-static {v4, v5}, Lo/ad;->a(Lo/T;Lo/T;)Lo/ad;

    move-result-object v2

    invoke-direct {v0, v3, v2, v1, v6}, Ln/a;-><init>(Lo/r;Lo/j;Lo/T;[Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public static a(Lo/T;D)Lo/ad;
    .locals 2

    invoke-virtual {p0}, Lo/T;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    invoke-static {p0, v0}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lo/T;D)Lo/r;
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {p1, p2, p3}, Ln/a;->a(Lo/T;D)Lo/ad;

    move-result-object v4

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    invoke-virtual {v0, v4}, Ln/a;->a(Lo/ae;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ln/a;->c()Lo/T;

    move-result-object v3

    invoke-virtual {v3, p1}, Lo/T;->d(Lo/T;)F

    move-result v3

    if-eqz v1, :cond_0

    cmpg-float v6, v3, v2

    if-gez v6, :cond_2

    :cond_0
    invoke-virtual {v0}, Ln/a;->a()Lo/r;

    move-result-object v0

    move v1, v3

    :goto_1
    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method static b(Ljava/lang/String;)Lo/T;
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x6

    invoke-static {p0}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    invoke-static {v1, v0}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0x1:0x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0xe

    if-gt v0, v2, :cond_1

    const-string v2, "0"

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    :try_start_0
    invoke-static {v2}, Lcom/google/googlenav/common/i;->b(Ljava/lang/String;)I

    move-result v2

    invoke-static {v0}, Lcom/google/googlenav/common/i;->b(Ljava/lang/String;)I

    move-result v3

    new-instance v0, Lo/T;

    invoke-direct {v0, v2, v3}, Lo/T;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lo/r;
    .locals 1

    iget-object v0, p0, Ln/a;->a:Lo/r;

    return-object v0
.end method

.method a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Ln/a;->d:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Ln/a;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public a(Lo/ae;)Z
    .locals 1

    iget-object v0, p0, Ln/a;->b:Lo/j;

    invoke-interface {v0, p1}, Lo/j;->a(Lo/ae;)Z

    move-result v0

    return v0
.end method

.method public b()Lo/ad;
    .locals 1

    iget-object v0, p0, Ln/a;->b:Lo/j;

    invoke-interface {v0}, Lo/j;->a()Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method public c()Lo/T;
    .locals 1

    iget-object v0, p0, Ln/a;->c:Lo/T;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ln/a;

    if-eqz v2, :cond_3

    check-cast p1, Ln/a;

    iget-object v2, p1, Ln/a;->a:Lo/r;

    iget-object v3, p0, Ln/a;->a:Lo/r;

    invoke-virtual {v2, v3}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Ln/a;->b:Lo/j;

    iget-object v3, p0, Ln/a;->b:Lo/j;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Ln/a;->c:Lo/T;

    iget-object v3, p0, Ln/a;->c:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Ln/a;->d:Ljava/util/Set;

    iget-object v3, p0, Ln/a;->d:Ljava/util/Set;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Ln/a;->b:Lo/j;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ln/a;->c:Lo/T;

    invoke-virtual {v1}, Lo/T;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ln/a;->d:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ln/a;->a:Lo/r;

    invoke-virtual {v1}, Lo/r;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ln/a;->a:Lo/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ln/a;->b:Lo/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ln/a;->c:Lo/T;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ln/a;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
