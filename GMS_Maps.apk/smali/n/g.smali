.class public Ln/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/e;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Z

.field private d:Z

.field private volatile e:Ln/e;

.field private volatile f:Ln/e;

.field private final g:Ljava/util/List;

.field private final h:Ln/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ln/g;->b:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ln/g;->c:Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ln/g;->g:Ljava/util/List;

    new-instance v0, Ln/h;

    invoke-direct {v0, p0}, Ln/h;-><init>(Ln/g;)V

    iput-object v0, p0, Ln/g;->h:Ln/i;

    invoke-direct {p0}, Ln/g;->b()Z

    return-void
.end method

.method private a()V
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->F()Z

    move-result v3

    if-eqz v3, :cond_3

    move v2, v1

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Ln/g;->h:Ln/i;

    invoke-interface {v1}, Ln/i;->a()Ln/e;

    move-result-object v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v0, p0, Ln/g;->h:Ln/i;

    const-string v2, "/new.building.list"

    invoke-interface {v0, v2}, Ln/i;->a(Ljava/lang/String;)Ln/e;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    iget-object v0, p0, Ln/g;->h:Ln/i;

    const-string v2, "/building.list"

    invoke-interface {v0, v2}, Ln/i;->a(Ljava/lang/String;)Ln/e;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Ln/g;->f:Ln/e;

    iput-object v1, p0, Ln/g;->e:Ln/e;

    return-void

    :cond_2
    move-object v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private b(Ln/f;)V
    .locals 1

    iget-object v0, p0, Ln/g;->f:Ln/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Ln/f;)V

    :cond_0
    iget-object v0, p0, Ln/g;->e:Ln/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Ln/f;)V

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Ln/g;->d:Z

    if-eqz v2, :cond_1

    monitor-exit v3

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Ln/g;->c:Z

    if-eqz v2, :cond_3

    invoke-static {}, LR/o;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    iput-boolean v2, p0, Ln/g;->c:Z

    move v2, v1

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_0

    invoke-direct {p0}, Ln/g;->a()V

    iget-object v2, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Ln/g;->d:Z

    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/f;

    invoke-direct {p0, v0}, Ln/g;->b(Ln/f;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .locals 5

    invoke-direct {p0}, Ln/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    iget-object v1, p0, Ln/g;->f:Ln/e;

    if-eqz v1, :cond_8

    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Ln/g;->e:Ln/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    move-object v2, v0

    :cond_1
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    if-eq v1, v0, :cond_2

    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    if-ne v2, v0, :cond_3

    :cond_2
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    invoke-virtual {v0}, Ln/a;->a()Lo/r;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-static {v1}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    invoke-virtual {v0}, Ln/a;->a()Lo/r;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_0

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Ln/f;)V
    .locals 2

    invoke-direct {p0}, Ln/g;->b()Z

    iget-object v1, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Ln/g;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1}, Ln/g;->b(Ln/f;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lo/o;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ln/g;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ln/g;->f:Ln/e;

    if-eqz v1, :cond_2

    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/o;)Z

    move-result v0

    :cond_2
    if-nez v0, :cond_0

    iget-object v1, p0, Ln/g;->e:Ln/e;

    if-eqz v1, :cond_0

    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/o;)Z

    move-result v0

    goto :goto_0
.end method
