.class Ln/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/r;


# instance fields
.field final synthetic a:Ln/n;

.field private final b:Ln/q;

.field private final c:Ljava/util/Set;

.field private final d:Lo/k;


# direct methods
.method public constructor <init>(Ln/n;Ljava/util/Collection;Ln/q;)V
    .locals 1

    iput-object p1, p0, Ln/p;->a:Ln/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lo/k;

    invoke-direct {v0}, Lo/k;-><init>()V

    iput-object v0, p0, Ln/p;->d:Lo/k;

    invoke-static {p2}, Lcom/google/common/collect/dA;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/p;->c:Ljava/util/Set;

    iput-object p3, p0, Ln/p;->b:Ln/q;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Ln/p;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Ln/p;->a:Ln/n;

    invoke-virtual {v1, p0}, Ln/n;->a(Ln/r;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v2, p0, Ln/p;->a:Ln/n;

    invoke-virtual {v2, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, v2}, Ln/p;->a(Lo/aq;Ln/l;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public declared-synchronized a(Lo/aq;Ln/l;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ln/p;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    sget-object v0, Ln/n;->a:Ln/l;

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Ln/p;->d:Lo/k;

    invoke-virtual {v0, p2}, Lo/k;->a(Lo/j;)V

    :cond_0
    iget-object v0, p0, Ln/p;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln/p;->a:Ln/n;

    invoke-virtual {v0, p0}, Ln/n;->b(Ln/r;)V

    iget-object v0, p0, Ln/p;->b:Ln/q;

    iget-object v1, p0, Ln/p;->d:Lo/k;

    invoke-interface {v0, v1}, Ln/q;->a(Lo/j;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
