.class public Ln/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/c;


# static fields
.field private static b:Ln/s;

.field private static final p:Lo/D;


# instance fields
.field private final a:Ljava/util/Map;

.field private final c:LR/h;

.field private final d:LR/h;

.field private e:Lo/r;

.field private f:Lo/D;

.field private g:Lo/y;

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/List;

.field private final j:Ljava/util/Set;

.field private final k:Ljava/lang/Object;

.field private final l:Lr/n;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ln/e;

.field private volatile q:Lo/D;

.field private volatile r:Lo/D;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Lo/D;

    new-instance v1, Lo/r;

    invoke-direct {v1, v2, v3, v2, v3}, Lo/r;-><init>(JJ)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lo/D;-><init>(Lo/r;I)V

    sput-object v0, Ln/s;->p:Lo/D;

    return-void
.end method

.method constructor <init>(Lr/n;)V
    .locals 2

    const/16 v1, 0x64

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ln/s;->a:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/s;->h:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ln/s;->i:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/s;->j:Ljava/util/Set;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ln/s;->k:Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ln/s;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ln/s;->n:Ljava/util/Map;

    new-instance v0, LR/h;

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/s;->c:LR/h;

    new-instance v0, LR/h;

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/s;->d:LR/h;

    iput-object p1, p0, Ln/s;->l:Lr/n;

    new-instance v0, Ln/g;

    invoke-direct {v0}, Ln/g;-><init>()V

    iput-object v0, p0, Ln/s;->o:Ln/e;

    return-void
.end method

.method public static a()Ln/s;
    .locals 1

    sget-object v0, Ln/s;->b:Ln/s;

    return-object v0
.end method

.method public static a(Lr/n;)Ln/s;
    .locals 1

    sget-object v0, Ln/s;->b:Ln/s;

    if-nez v0, :cond_0

    new-instance v0, Ln/s;

    invoke-direct {v0, p0}, Ln/s;-><init>(Lr/n;)V

    sput-object v0, Ln/s;->b:Ln/s;

    :cond_0
    sget-object v0, Ln/s;->b:Ln/s;

    return-object v0
.end method

.method static synthetic a(Ln/s;Lo/y;)V
    .locals 0

    invoke-direct {p0, p1}, Ln/s;->d(Lo/y;)V

    return-void
.end method

.method private a(Lo/r;Lo/D;Lo/D;)V
    .locals 6

    iget-object v2, p0, Ln/s;->c:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Ln/s;->d:LR/h;

    invoke-virtual {v0, p1, p3}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Ln/s;->l:Lr/n;

    invoke-virtual {p3}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v3

    if-nez v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ln/s;->p:Lo/D;

    if-ne p2, v0, :cond_2

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v3}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    invoke-virtual {v0, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Ln/s;->d:LR/h;

    iget-object v5, p0, Ln/s;->c:LR/h;

    invoke-virtual {v5, v0}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v4, p0, Ln/s;->c:LR/h;

    sget-object v5, Ln/s;->p:Lo/D;

    invoke-virtual {v4, v0, v5}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Ln/s;->l:Lr/n;

    invoke-virtual {p2}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v0

    if-nez v0, :cond_3

    monitor-exit v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private a(Lo/r;Ls/c;)V
    .locals 1

    iget-object v0, p0, Ln/s;->l:Lr/n;

    invoke-virtual {v0, p1}, Lr/n;->b(Lo/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ln/s;->l:Lr/n;

    invoke-virtual {v0, p1, p2}, Lr/n;->a(Lo/r;Ls/c;)V

    goto :goto_0
.end method

.method private a(Lo/y;Lo/D;)V
    .locals 4

    invoke-virtual {p1, p2}, Lo/y;->a(Lo/D;)Lo/z;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    invoke-virtual {v1}, Lo/z;->a()Lo/D;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Ln/s;->a(Lo/r;Lo/D;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ln/t;

    invoke-direct {v3, p0}, Ln/t;-><init>(Ln/s;)V

    invoke-direct {p0, v0, v3}, Ln/s;->a(Lo/r;Ls/c;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(Lo/y;)Lo/D;
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Ln/s;->c:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Ln/s;->c:LR/h;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v0, v3}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lo/y;->c()Lo/z;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Ln/s;->p:Lo/D;

    :goto_0
    iget-object v3, p0, Ln/s;->c:LR/h;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v3, Ln/s;->p:Lo/D;

    if-eq v0, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Ln/s;->d(Lo/y;)V

    :cond_1
    return-object v0

    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private d(Lo/y;)V
    .locals 3

    invoke-direct {p0}, Ln/s;->m()V

    iget-object v1, p0, Ln/s;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/u;

    invoke-interface {v0, p0, p1}, Ln/u;->a(Ln/s;Lo/y;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private k()V
    .locals 3

    iget-object v1, p0, Ln/s;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/u;

    invoke-interface {v0, p0}, Ln/u;->a(Ln/s;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private l()V
    .locals 3

    invoke-direct {p0}, Ln/s;->m()V

    iget-object v1, p0, Ln/s;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/u;

    invoke-interface {v0, p0}, Ln/u;->b(Ln/s;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private m()V
    .locals 6

    iget-object v2, p0, Ln/s;->n:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Ln/s;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Ln/s;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p0}, Ln/s;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v1

    invoke-virtual {p0, v1}, Ln/s;->b(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ln/s;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ln/k;

    if-nez v1, :cond_1

    new-instance v1, Ln/k;

    invoke-direct {v1, v0}, Ln/k;-><init>(Lo/z;)V

    iget-object v4, p0, Ln/s;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v4, p0, Ln/s;->n:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {v1, v0}, Ln/k;->a(Lo/z;)Z

    goto :goto_1

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a(Lo/r;ZZZ)Ln/k;
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Ln/s;->n:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Ln/s;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit v3

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-eqz p3, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_5

    move-object v2, v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_1

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Ln/s;->l:Lr/n;

    invoke-virtual {v0, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ln/s;->a(Lo/r;Ls/c;)V

    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    if-nez v0, :cond_3

    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v1, Ln/k;

    invoke-direct {v1, v0}, Ln/k;-><init>(Lo/z;)V

    if-eqz p4, :cond_4

    iget-object v0, p0, Ln/s;->n:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Ln/k;->a(Lo/r;)Ln/k;

    move-result-object v2

    invoke-virtual {v2}, Ln/k;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    iget-object v5, p0, Ln/s;->n:Ljava/util/Map;

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v2, v1

    goto :goto_1
.end method

.method public a(Lo/r;)Lo/D;
    .locals 2

    iget-object v1, p0, Ln/s;->c:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->c:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v1, p0, Ln/s;->l:Lr/n;

    invoke-virtual {v1, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v1}, Ln/s;->c(Lo/y;)Lo/D;

    move-result-object v0

    :cond_0
    :goto_0
    sget-object v1, Ln/s;->p:Lo/D;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-direct {p0, p1, p0}, Ln/s;->a(Lo/r;Ls/c;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)V
    .locals 5

    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Ln/s;->j:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ln/s;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Ln/s;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Ln/s;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Ln/s;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Ln/s;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Ln/s;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    iget-object v3, p0, Ln/s;->l:Lr/n;

    invoke-virtual {v3, v0}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Ln/s;->i:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Ln/s;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-direct {p0, v0, p0}, Ln/s;->a(Lo/r;Ls/c;)V

    goto :goto_1

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Ln/s;->l()V

    goto :goto_0
.end method

.method public a(Ln/u;)V
    .locals 2

    iget-object v0, p0, Ln/s;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lo/D;)V
    .locals 2

    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->f:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ln/s;->c:LR/h;

    invoke-virtual {v0}, LR/h;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Ln/s;->f:Lo/D;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-direct {p0, v0, p0}, Ln/s;->a(Lo/r;Ls/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lo/D;Lo/D;)V
    .locals 0

    iput-object p1, p0, Ln/s;->q:Lo/D;

    iput-object p2, p0, Ln/s;->r:Lo/D;

    invoke-direct {p0}, Ln/s;->m()V

    return-void
.end method

.method public a(Lo/r;ILo/y;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v3, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Ln/s;->f:Lo/D;

    if-eqz v4, :cond_2

    iget-object v4, p0, Ln/s;->f:Lo/D;

    invoke-virtual {v4}, Lo/D;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v4, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v2, p0, Ln/s;->f:Lo/D;

    const/4 v4, 0x0

    iput-object v4, p0, Ln/s;->f:Lo/D;

    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    invoke-direct {p0, p3, v2}, Ln/s;->a(Lo/y;Lo/D;)V

    :cond_3
    invoke-direct {p0, p3}, Ln/s;->c(Lo/y;)Lo/D;

    iget-object v3, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Ln/s;->e:Lo/r;

    invoke-virtual {p1, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Ln/s;->g:Lo/y;

    if-eqz v2, :cond_4

    invoke-virtual {p3}, Lo/y;->a()Lo/r;

    move-result-object v2

    iget-object v4, p0, Ln/s;->g:Lo/y;

    invoke-virtual {v4}, Lo/y;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_4
    invoke-virtual {p3}, Lo/y;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Ln/s;->g:Lo/y;

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    iput-object v2, p0, Ln/s;->g:Lo/y;

    move v2, v0

    :goto_1
    const/4 v4, 0x0

    iput-object v4, p0, Ln/s;->e:Lo/r;

    :goto_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_5

    invoke-direct {p0}, Ln/s;->k()V

    :cond_5
    iget-object v2, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v3, p0, Ln/s;->h:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v1, p0, Ln/s;->h:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Ln/s;->i:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ln/s;->l()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_6
    :try_start_4
    iput-object p3, p0, Ln/s;->g:Lo/y;

    move v2, v0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_1

    :cond_9
    move v2, v1

    goto :goto_2
.end method

.method public a(Lo/y;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    sget-object v1, Ln/s;->p:Lo/D;

    invoke-virtual {p0, v0, v1}, Ln/s;->a(Lo/r;Lo/D;)Z

    invoke-direct {p0, p1}, Ln/s;->d(Lo/y;)V

    :cond_0
    return-void
.end method

.method a(Lo/r;Lo/D;)Z
    .locals 3

    iget-object v1, p0, Ln/s;->c:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->c:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    invoke-virtual {p2, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Ln/s;->c:LR/h;

    invoke-virtual {v2, p1, p2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, v0}, Ln/s;->a(Lo/r;Lo/D;Lo/D;)V

    :cond_1
    monitor-exit v1

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lo/r;)Lo/E;
    .locals 2

    invoke-virtual {p0, p1}, Ln/s;->a(Lo/r;)Lo/D;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lo/F;

    invoke-direct {v1}, Lo/F;-><init>()V

    invoke-virtual {v1, v0}, Lo/F;->a(Lo/D;)Lo/F;

    move-result-object v0

    invoke-virtual {v0}, Lo/F;->a()Lo/E;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lo/y;)Lo/z;
    .locals 1

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Ln/s;->a(Lo/r;)Lo/D;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lo/y;->a(Lo/D;)Lo/z;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Ln/s;->q:Lo/D;

    iput-object v0, p0, Ln/s;->r:Lo/D;

    invoke-direct {p0}, Ln/s;->m()V

    return-void
.end method

.method public b(Ln/u;)V
    .locals 1

    iget-object v0, p0, Ln/s;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(Lo/D;)Z
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Ln/s;->q:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ln/s;->r:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lo/D;Lo/D;)Z
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Ln/s;->q:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln/s;->r:Lo/D;

    invoke-virtual {p2, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lo/r;)Lo/D;
    .locals 3

    iget-object v1, p0, Ln/s;->c:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    sget-object v2, Ln/s;->p:Lo/D;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Lo/y;
    .locals 2

    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->g:Lo/y;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Lo/D;)Z
    .locals 3

    invoke-virtual {p0}, Ln/s;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/util/List;
    .locals 2

    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(Lo/r;)V
    .locals 3

    if-nez p1, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Ln/s;->g:Lo/y;

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Ln/s;->e:Lo/r;

    const/4 v0, 0x0

    iput-object v0, p0, Ln/s;->g:Lo/y;

    const/4 v0, 0x1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ln/s;->k()V

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    iget-object v1, p0, Ln/s;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Ln/s;->e:Lo/r;

    invoke-virtual {p1, v0}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ln/s;->g:Lo/y;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ln/s;->g:Lo/y;

    invoke-virtual {v0}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    :try_start_3
    iput-object p1, p0, Ln/s;->e:Lo/r;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-direct {p0, p1, p0}, Ln/s;->a(Lo/r;Ls/c;)V

    goto :goto_0
.end method

.method public d(Lo/D;)Z
    .locals 2

    iget-object v1, p0, Ln/s;->c:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->c:LR/h;

    invoke-virtual {v0}, LR/h;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Ln/s;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    invoke-virtual {p0, v0}, Ln/s;->b(Lo/y;)Lo/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public e(Lo/r;)Ln/k;
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1, v1}, Ln/s;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 2

    invoke-virtual {p0}, Ln/s;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    invoke-virtual {p0, v0}, Ln/s;->b(Lo/y;)Lo/z;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Ln/s;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    invoke-virtual {p0, v0}, Ln/s;->b(Lo/y;)Lo/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public h()Z
    .locals 2

    invoke-virtual {p0}, Ln/s;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/util/Set;
    .locals 2

    iget-object v1, p0, Ln/s;->n:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/s;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()Ln/e;
    .locals 1

    iget-object v0, p0, Ln/s;->o:Ln/e;

    return-object v0
.end method
