.class public Ln/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/e;
.implements Lv/c;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lo/T;

.field private final d:Lo/T;

.field private volatile e:Z

.field private volatile f:Lo/T;

.field private final g:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile h:Ln/x;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    const v1, 0x4c4b40

    const v2, 0x3d0900

    new-instance v0, Lo/T;

    invoke-direct {v0, v1, v1}, Lo/T;-><init>(II)V

    new-instance v1, Lo/T;

    invoke-direct {v1, v2, v2}, Lo/T;-><init>(II)V

    invoke-direct {p0, p1, v0, v1}, Ln/v;-><init>(Ljava/lang/String;Lo/T;Lo/T;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lo/T;Lo/T;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln/v;->b:Ljava/lang/String;

    iput-object p2, p0, Ln/v;->c:Lo/T;

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p3}, Lo/T;->f()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v0

    invoke-virtual {p3}, Lo/T;->g()I

    move-result v1

    if-lt v0, v1, :cond_0

    iput-object p3, p0, Ln/v;->d:Lo/T;

    :goto_0
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ln/v;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Ln/v;->f:Lo/T;

    new-instance v0, Ln/x;

    invoke-direct {v0}, Ln/x;-><init>()V

    iput-object v0, p0, Ln/v;->h:Ln/x;

    return-void

    :cond_0
    iget-object v0, p0, Ln/v;->c:Lo/T;

    iput-object v0, p0, Ln/v;->d:Lo/T;

    goto :goto_0
.end method

.method static a(Lo/T;Lo/T;)Lo/aR;
    .locals 5

    const/16 v2, 0xf

    invoke-virtual {p0, p1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->b(ILo/T;)Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->g()Lo/T;

    move-result-object v1

    invoke-virtual {p0, p1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->b(ILo/T;)Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->h()Lo/T;

    move-result-object v0

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v3

    if-le v2, v3, :cond_0

    new-instance v2, Lo/T;

    const/high16 v3, 0x40000000

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lo/T;-><init>(II)V

    invoke-virtual {v0, v2}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    :cond_0
    new-instance v2, Lo/ad;

    invoke-direct {v2, v1, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-static {v2}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Ln/v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Ln/v;->a(Lv/a;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Ln/v;)V
    .locals 0

    invoke-direct {p0}, Ln/v;->a()V

    return-void
.end method

.method private a(Lo/T;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ln/v;->e:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ln/v;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object p1, p0, Ln/v;->f:Lo/T;

    new-instance v0, Ln/w;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ln/w;-><init>(Ln/v;Las/c;)V

    invoke-virtual {v0}, Ln/w;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(Lv/a;)V
    .locals 5

    invoke-virtual {p1}, Lv/a;->e()[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Ln/v;->f:Lo/T;

    iget-object v2, p0, Ln/v;->c:Lo/T;

    invoke-static {v1, v2}, Ln/v;->a(Lo/T;Lo/T;)Lo/aR;

    move-result-object v1

    iget-object v2, p0, Ln/v;->f:Lo/T;

    iget-object v3, p0, Ln/v;->d:Lo/T;

    invoke-static {v2, v3}, Ln/v;->a(Lo/T;Lo/T;)Lo/aR;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-static {v0, v1}, Ln/j;->a(Ljava/io/Reader;Lo/aR;)Ln/j;

    move-result-object v0

    new-instance v3, Ln/x;

    invoke-direct {v3, v0, v1, v2}, Ln/x;-><init>(Ln/j;Lo/aR;Lo/aR;)V

    iput-object v3, p0, Ln/v;->h:Ln/x;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ln/v;->e:Z

    return-void

    :catch_0
    move-exception v0

    const-string v1, "LazyBuildingBoundProvider"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .locals 3

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iget-object v1, p0, Ln/v;->h:Ln/x;

    iget-boolean v2, p0, Ln/v;->e:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Ln/x;->c:Lo/aR;

    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/ae;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-direct {p0, v2}, Ln/v;->a(Lo/T;)V

    :cond_1
    iget-object v2, v1, Ln/x;->b:Lo/aR;

    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Ln/x;->a:Ln/j;

    invoke-virtual {v0, p1}, Ln/j;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Ln/v;->a:Ljava/util/Collection;

    goto :goto_0
.end method

.method public a(Ln/f;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ln/v;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lv/a;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p1

    :try_start_0
    iget-boolean v0, p0, Ln/v;->e:Z

    if-nez v0, :cond_1

    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lv/a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Ln/v;->b(Lv/a;)V

    iget-object v0, p0, Ln/v;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/f;

    invoke-interface {v0}, Ln/f;->a()V

    goto :goto_1

    :cond_2
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Lo/o;)Z
    .locals 1

    iget-object v0, p0, Ln/v;->h:Ln/x;

    iget-object v0, v0, Ln/x;->a:Ln/j;

    invoke-virtual {v0, p1}, Ln/j;->a(Lo/o;)Z

    move-result v0

    return v0
.end method
