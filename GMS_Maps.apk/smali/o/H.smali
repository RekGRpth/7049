.class public Lo/H;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lo/ak;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Lo/b;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lo/ak;

    const/4 v1, -0x1

    invoke-direct {v0, v2, v2, v1}, Lo/ak;-><init>(Lo/aj;Ljava/lang/String;I)V

    sput-object v0, Lo/H;->a:Lo/ak;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lo/b;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter labelElements can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    invoke-virtual {v0}, Lo/I;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lo/I;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0}, Lo/I;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo/H;->d:Ljava/lang/String;

    iput-object p2, p0, Lo/H;->c:Lo/b;

    iput-object p1, p0, Lo/H;->b:Ljava/util/List;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;
    .locals 3

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0, p1, p2, v2}, Lo/I;->a(Ljava/io/DataInput;Lo/as;Lo/ak;Ljava/util/List;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-le v1, v0, :cond_1

    invoke-static {p0, p1}, Lo/b;->a(Ljava/io/DataInput;Lo/as;)Lo/b;

    move-result-object v0

    :goto_1
    new-instance v1, Lo/H;

    invoke-direct {v1, v2, v0}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    return-object v1

    :cond_1
    sget-object v0, Lo/b;->b:Lo/b;

    goto :goto_1
.end method

.method static synthetic e()Lo/ak;
    .locals 1

    sget-object v0, Lo/H;->a:Lo/ak;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/H;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Lo/I;
    .locals 1

    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public c()Lo/b;
    .locals 1

    iget-object v0, p0, Lo/H;->c:Lo/b;

    return-object v0
.end method

.method public d()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    invoke-virtual {v0}, Lo/I;->l()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x18

    iget-object v1, p0, Lo/H;->d:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/H;->c:Lo/b;

    invoke-virtual {v1}, Lo/b;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    check-cast p1, Lo/H;

    iget-object v1, p0, Lo/H;->c:Lo/b;

    if-nez v1, :cond_3

    iget-object v1, p1, Lo/H;->c:Lo/b;

    if-nez v1, :cond_0

    :cond_2
    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    iget-object v1, p1, Lo/H;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lo/H;->c:Lo/b;

    iget-object v2, p1, Lo/H;->c:Lo/b;

    invoke-virtual {v1, v2}, Lo/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lo/H;->c:Lo/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/H;->b:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lo/H;->c:Lo/b;

    invoke-virtual {v0}, Lo/b;->hashCode()I

    move-result v0

    goto :goto_0
.end method
