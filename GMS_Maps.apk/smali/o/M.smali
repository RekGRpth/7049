.class public Lo/M;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private a:Lo/X;

.field private b:Lo/aj;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private final g:[I


# direct methods
.method public constructor <init>(Lo/X;Lo/aj;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/M;->a:Lo/X;

    iput-object p2, p0, Lo/M;->b:Lo/aj;

    iput p3, p0, Lo/M;->c:I

    iput-object p4, p0, Lo/M;->d:Ljava/lang/String;

    iput p5, p0, Lo/M;->e:I

    iput p6, p0, Lo/M;->f:I

    iput-object p7, p0, Lo/M;->g:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/M;
    .locals 8

    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v0

    invoke-static {p0, v0}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v1

    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v4

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v5

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v6

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v7, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    aput v3, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lo/M;

    invoke-virtual {v4}, Lo/ak;->a()Lo/aj;

    move-result-object v2

    invoke-virtual {v4}, Lo/ak;->c()I

    move-result v3

    invoke-virtual {v4}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v7}, Lo/M;-><init>(Lo/X;Lo/aj;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Lo/X;
    .locals 1

    iget-object v0, p0, Lo/M;->a:Lo/X;

    return-object v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/M;->b:Lo/aj;

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/M;->e:I

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/M;->g:[I

    return-object v0
.end method

.method public m()I
    .locals 2

    iget-object v0, p0, Lo/M;->a:Lo/X;

    invoke-virtual {v0}, Lo/X;->h()I

    move-result v0

    add-int/lit8 v0, v0, 0x28

    iget-object v1, p0, Lo/M;->d:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/M;->b:Lo/aj;

    invoke-static {v1}, Lo/O;->a(Lo/aj;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
