.class Lo/R;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/aO;


# instance fields
.field final synthetic a:Lo/P;

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>(Lo/P;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lo/R;->a:Lo/P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lo/R;->b:I

    iput v0, p0, Lo/R;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lo/P;Lo/Q;)V
    .locals 0

    invoke-direct {p0, p1}, Lo/R;-><init>(Lo/P;)V

    return-void
.end method


# virtual methods
.method public a()Lo/n;
    .locals 3

    iget-object v0, p0, Lo/R;->a:Lo/P;

    invoke-static {v0}, Lo/P;->a(Lo/P;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lo/R;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lo/R;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    return-object v0
.end method

.method public b()Lo/n;
    .locals 2

    iget-object v0, p0, Lo/R;->a:Lo/P;

    invoke-static {v0}, Lo/P;->a(Lo/P;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lo/R;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    return-object v0
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lo/R;->b:I

    iput v0, p0, Lo/R;->c:I

    return-void
.end method

.method public d()V
    .locals 1

    iget v0, p0, Lo/R;->c:I

    iput v0, p0, Lo/R;->b:I

    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lo/R;->b:I

    iget-object v1, p0, Lo/R;->a:Lo/P;

    invoke-static {v1}, Lo/P;->a(Lo/P;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lo/R;->a()Lo/n;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
