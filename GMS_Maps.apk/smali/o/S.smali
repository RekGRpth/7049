.class public Lo/S;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lo/T;

.field protected b:F

.field protected c:I

.field protected d:Lo/T;

.field protected e:F

.field protected f:Z

.field protected g:Lo/D;

.field protected h:Z

.field protected i:F

.field protected j:Z

.field protected k:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lo/S;->k()V

    return-void
.end method

.method public constructor <init>(Lo/S;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lo/S;->a(Lo/S;)V

    return-void
.end method


# virtual methods
.method public a()Lo/T;
    .locals 1

    iget-object v0, p0, Lo/S;->a:Lo/T;

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lo/S;->b:F

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lo/S;->c:I

    return-void
.end method

.method public a(Lo/D;)V
    .locals 0

    iput-object p1, p0, Lo/S;->g:Lo/D;

    return-void
.end method

.method public final a(Lo/S;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lo/S;->k()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lo/S;->a:Lo/T;

    iget v1, p1, Lo/S;->b:F

    iget v2, p1, Lo/S;->c:I

    invoke-virtual {p0, v0, v1, v2}, Lo/S;->a(Lo/T;FI)V

    iget-object v0, p1, Lo/S;->d:Lo/T;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lo/S;->d:Lo/T;

    iget v0, p1, Lo/S;->e:F

    iput v0, p0, Lo/S;->e:F

    iget-boolean v0, p1, Lo/S;->f:Z

    iput-boolean v0, p0, Lo/S;->f:Z

    iget-object v0, p1, Lo/S;->g:Lo/D;

    iput-object v0, p0, Lo/S;->g:Lo/D;

    iget-boolean v0, p1, Lo/S;->h:Z

    iput-boolean v0, p0, Lo/S;->h:Z

    iget v0, p1, Lo/S;->i:F

    iput v0, p0, Lo/S;->i:F

    iget-boolean v0, p1, Lo/S;->j:Z

    iput-boolean v0, p0, Lo/S;->j:Z

    iget v0, p1, Lo/S;->k:F

    iput v0, p0, Lo/S;->k:F

    goto :goto_0

    :cond_1
    new-instance v0, Lo/T;

    iget-object v1, p1, Lo/S;->d:Lo/T;

    invoke-direct {v0, v1}, Lo/T;-><init>(Lo/T;)V

    goto :goto_1
.end method

.method public a(Lo/T;)V
    .locals 0

    iput-object p1, p0, Lo/S;->a:Lo/T;

    return-void
.end method

.method public final a(Lo/T;FI)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lo/S;->a:Lo/T;

    iput p2, p0, Lo/S;->b:F

    iput p3, p0, Lo/S;->c:I

    return-void

    :cond_0
    new-instance v0, Lo/T;

    invoke-direct {v0, p1}, Lo/T;-><init>(Lo/T;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lo/S;->f:Z

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lo/S;->b:F

    return v0
.end method

.method public b(F)V
    .locals 2

    const/high16 v0, 0x3f800000

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lo/S;->k:F

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lo/S;->h:Z

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lo/S;->c:I

    return v0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lo/S;->j:Z

    return-void
.end method

.method public d()Lo/T;
    .locals 2

    iget-object v0, p0, Lo/S;->d:Lo/T;

    iget-object v1, p0, Lo/S;->a:Lo/T;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/T;

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lo/S;->f:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lo/S;

    iget-object v2, p0, Lo/S;->a:Lo/T;

    iget-object v3, p1, Lo/S;->a:Lo/T;

    invoke-static {v2, v3}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lo/S;->b:F

    iget v3, p1, Lo/S;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lo/S;->c:I

    iget v3, p1, Lo/S;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lo/S;->d:Lo/T;

    iget-object v3, p1, Lo/S;->d:Lo/T;

    invoke-static {v2, v3}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lo/S;->e:F

    iget v3, p1, Lo/S;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lo/S;->f:Z

    iget-boolean v3, p1, Lo/S;->f:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lo/S;->g:Lo/D;

    iget-object v3, p1, Lo/S;->g:Lo/D;

    invoke-static {v2, v3}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lo/S;->h:Z

    iget-boolean v3, p1, Lo/S;->h:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lo/S;->i:F

    iget v3, p1, Lo/S;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lo/S;->j:Z

    iget-boolean v3, p1, Lo/S;->j:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lo/S;->k:F

    iget v3, p1, Lo/S;->k:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Lo/D;
    .locals 1

    iget-object v0, p0, Lo/S;->g:Lo/D;

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lo/S;->h:Z

    return v0
.end method

.method public h()F
    .locals 1

    iget v0, p0, Lo/S;->i:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lo/S;->a:Lo/T;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lo/S;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lo/S;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lo/S;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lo/S;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lo/S;->g:Lo/D;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lo/S;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lo/S;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lo/S;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lo/S;->k:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lo/S;->j:Z

    return v0
.end method

.method public j()F
    .locals 1

    iget v0, p0, Lo/S;->k:F

    return v0
.end method

.method public final k()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-object v3, p0, Lo/S;->a:Lo/T;

    iput v1, p0, Lo/S;->b:F

    const/4 v0, -0x1

    iput v0, p0, Lo/S;->c:I

    iput-object v3, p0, Lo/S;->d:Lo/T;

    iput v1, p0, Lo/S;->e:F

    iput-boolean v2, p0, Lo/S;->f:Z

    iput-object v3, p0, Lo/S;->g:Lo/D;

    iput-boolean v2, p0, Lo/S;->h:Z

    iput v1, p0, Lo/S;->i:F

    iput-boolean v2, p0, Lo/S;->j:Z

    const/high16 v0, 0x3f800000

    iput v0, p0, Lo/S;->k:F

    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lo/S;->a:Lo/T;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
