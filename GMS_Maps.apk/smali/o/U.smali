.class public Lo/U;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/aq;

.field private final b:Lo/T;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lo/o;

.field private final f:Lo/aj;

.field private final g:I

.field private final h:[I

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:F

.field private m:F

.field private final n:[Lo/a;

.field private final o:Lo/H;

.field private final p:Lo/H;

.field private final q:[Lo/c;

.field private final r:I

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lo/aq;Lo/T;Lo/o;[Lo/a;Lo/H;Lo/H;[Lo/c;Ljava/lang/String;Lo/aj;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;[I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, -0x40800000

    iput v1, p0, Lo/U;->l:F

    const/high16 v1, -0x40800000

    iput v1, p0, Lo/U;->m:F

    iput-object p3, p0, Lo/U;->e:Lo/o;

    iput-object p4, p0, Lo/U;->n:[Lo/a;

    iput-object p5, p0, Lo/U;->o:Lo/H;

    iput-object p6, p0, Lo/U;->p:Lo/H;

    iput-object p7, p0, Lo/U;->q:[Lo/c;

    iput-object p8, p0, Lo/U;->d:Ljava/lang/String;

    iput-object p9, p0, Lo/U;->f:Lo/aj;

    iput p10, p0, Lo/U;->r:I

    iput-object p11, p0, Lo/U;->s:Ljava/lang/String;

    iput p12, p0, Lo/U;->g:I

    iput p13, p0, Lo/U;->i:I

    const/4 v1, -0x1

    move/from16 v0, p14

    if-ne v0, v1, :cond_0

    const/16 p14, 0x1e

    :cond_0
    move/from16 v0, p14

    iput v0, p0, Lo/U;->j:I

    move/from16 v0, p15

    iput v0, p0, Lo/U;->k:I

    move-object/from16 v0, p16

    iput-object v0, p0, Lo/U;->t:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lo/U;->c:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lo/U;->h:[I

    iput-object p1, p0, Lo/U;->a:Lo/aq;

    iput-object p2, p0, Lo/U;->b:Lo/T;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/U;
    .locals 21

    invoke-static/range {p0 .. p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v6, v4, [Lo/a;

    invoke-virtual/range {p1 .. p1}, Lo/as;->b()Lo/aq;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lo/as;->a()I

    move-result v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5}, Lo/a;->a(Ljava/io/DataInput;Lo/aq;I)Lo/a;

    move-result-object v7

    aput-object v7, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    aget-object v2, v6, v2

    invoke-virtual {v2}, Lo/a;->b()Lo/T;

    move-result-object v4

    invoke-static/range {p0 .. p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v13}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v13}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v10

    new-array v9, v10, [Lo/c;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_1

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lo/c;->a(Ljava/io/DataInput;I)Lo/c;

    move-result-object v11

    aput-object v11, v9, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v14

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v15

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v16

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v17

    const/4 v5, 0x0

    const/4 v2, 0x1

    move/from16 v0, v17

    invoke-static {v2, v0}, Lo/O;->a(II)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static/range {p0 .. p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v5

    :cond_2
    :goto_2
    const/4 v10, 0x0

    invoke-static/range {v17 .. v17}, Lo/U;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Li/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    :cond_3
    const/16 v18, 0x0

    invoke-static/range {v17 .. v17}, Lo/U;->c(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v18

    :cond_4
    invoke-static/range {v17 .. v17}, Lo/U;->b(I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v19

    :goto_3
    invoke-static/range {p0 .. p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v11

    new-array v0, v11, [I

    move-object/from16 v20, v0

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v11, :cond_8

    invoke-static/range {p0 .. p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v12

    aput v12, v20, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    const/4 v2, 0x2

    move/from16 v0, v17

    invoke-static {v2, v0}, Lo/O;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v5

    goto :goto_2

    :cond_6
    invoke-virtual {v7}, Lo/H;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lo/H;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_7

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_7

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v12, 0xa

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_3

    :cond_8
    new-instance v2, Lo/U;

    invoke-virtual {v13}, Lo/ak;->a()Lo/aj;

    move-result-object v11

    invoke-virtual {v13}, Lo/ak;->c()I

    move-result v12

    invoke-virtual {v13}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v13

    invoke-direct/range {v2 .. v20}, Lo/U;-><init>(Lo/aq;Lo/T;Lo/o;[Lo/a;Lo/H;Lo/H;[Lo/c;Ljava/lang/String;Lo/aj;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;[I)V

    return-object v2
.end method

.method public static a(Lo/aq;Lo/T;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/o;Ljava/lang/String;Lo/aj;II[I)Lo/U;
    .locals 19

    new-instance v0, Lo/a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v7}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    const/4 v1, 0x1

    new-array v12, v1, [Lo/a;

    const/4 v1, 0x0

    aput-object v0, v12, v1

    const/4 v10, 0x0

    const/4 v9, 0x0

    if-eqz p3, :cond_0

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lo/I;

    const/4 v1, 0x1

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v10, Lo/H;

    sget-object v0, Lo/b;->b:Lo/b;

    invoke-direct {v10, v11, v0}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    :cond_0
    if-eqz p2, :cond_4

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lo/I;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "styleid"

    const/4 v8, 0x0

    move-object/from16 v4, p2

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v10, :cond_2

    new-instance v5, Lo/H;

    sget-object v0, Lo/b;->b:Lo/b;

    invoke-direct {v5, v11, v0}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    move-object v6, v9

    :goto_0
    if-nez v6, :cond_1

    new-instance v6, Lo/H;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lo/b;->b:Lo/b;

    invoke-direct {v6, v0, v1}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    :cond_1
    new-instance v0, Lo/U;

    const/4 v1, 0x0

    new-array v7, v1, [Lo/c;

    const/4 v10, 0x0

    const-string v11, "styleid"

    const/4 v13, 0x0

    const/16 v14, 0x14

    const/16 v16, 0x0

    if-nez p2, :cond_3

    const-string v17, ""

    :goto_1
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p5

    move-object v4, v12

    move-object/from16 v8, p4

    move-object/from16 v9, p7

    move/from16 v12, p8

    move/from16 v15, p9

    move-object/from16 v18, p10

    invoke-direct/range {v0 .. v18}, Lo/U;-><init>(Lo/aq;Lo/T;Lo/o;[Lo/a;Lo/H;Lo/H;[Lo/c;Ljava/lang/String;Lo/aj;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;[I)V

    return-object v0

    :cond_2
    new-instance v6, Lo/H;

    sget-object v0, Lo/b;->b:Lo/b;

    invoke-direct {v6, v11, v0}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    move-object v5, v10

    goto :goto_0

    :cond_3
    move-object/from16 v17, p2

    goto :goto_1

    :cond_4
    move-object v6, v9

    move-object v5, v10

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    const/16 v0, 0x20

    invoke-static {v0, p0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .locals 1

    const/16 v0, 0x80

    invoke-static {v0, p0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .locals 1

    const/16 v0, 0x40

    invoke-static {v0, p0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lo/o;
    .locals 1

    iget-object v0, p0, Lo/U;->e:Lo/o;

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lo/U;->l:F

    return-void
.end method

.method public b()Lo/aq;
    .locals 1

    iget-object v0, p0, Lo/U;->a:Lo/aq;

    return-object v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Lo/U;->m:F

    return-void
.end method

.method public c()Lo/T;
    .locals 1

    iget-object v0, p0, Lo/U;->b:Lo/T;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/U;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/U;->f:Lo/aj;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/U;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lo/U;->i:I

    return v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/U;->g:I

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lo/U;->j:I

    return v0
.end method

.method public k()F
    .locals 1

    iget v0, p0, Lo/U;->l:F

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/U;->h:[I

    return-object v0
.end method

.method public m()I
    .locals 7

    const/4 v2, 0x0

    const/16 v0, 0x70

    iget-object v1, p0, Lo/U;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lo/U;->c:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lo/U;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lo/U;->d:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lo/U;->n:[Lo/a;

    if-eqz v1, :cond_2

    iget-object v5, p0, Lo/U;->n:[Lo/a;

    array-length v6, v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v4, v5, v3

    invoke-virtual {v4}, Lo/a;->d()I

    move-result v4

    add-int/2addr v4, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_2
    move v1, v2

    :cond_3
    iget-object v3, p0, Lo/U;->q:[Lo/c;

    if-eqz v3, :cond_4

    iget-object v5, p0, Lo/U;->q:[Lo/c;

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v4, v5, v3

    invoke-virtual {v4}, Lo/c;->c()I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_4
    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->o:Lo/H;

    invoke-static {v2}, Lo/O;->a(Lo/H;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->p:Lo/H;

    invoke-static {v2}, Lo/O;->a(Lo/H;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->e:Lo/o;

    invoke-static {v2}, Lo/O;->a(Lo/o;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->f:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->s:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/U;->t:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public n()F
    .locals 1

    iget v0, p0, Lo/U;->m:F

    return v0
.end method

.method public o()[Lo/a;
    .locals 1

    iget-object v0, p0, Lo/U;->n:[Lo/a;

    return-object v0
.end method

.method public p()Lo/H;
    .locals 1

    iget-object v0, p0, Lo/U;->o:Lo/H;

    return-object v0
.end method

.method public q()Lo/H;
    .locals 1

    iget-object v0, p0, Lo/U;->p:Lo/H;

    return-object v0
.end method

.method public r()[Lo/c;
    .locals 1

    iget-object v0, p0, Lo/U;->q:[Lo/c;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/U;->t:Ljava/lang/String;

    return-object v0
.end method

.method public t()Z
    .locals 2

    const/16 v0, 0x10

    iget v1, p0, Lo/U;->k:I

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 2

    const/16 v0, 0x100

    iget v1, p0, Lo/U;->k:I

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method
