.class public final Lo/W;
.super Lo/ae;
.source "SourceFile"


# instance fields
.field private a:[Lo/T;

.field private volatile b:Lo/ad;


# direct methods
.method public constructor <init>([Lo/T;)V
    .locals 0

    invoke-direct {p0}, Lo/ae;-><init>()V

    iput-object p1, p0, Lo/W;->a:[Lo/T;

    return-void
.end method


# virtual methods
.method public a(I)Lo/T;
    .locals 1

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lo/ad;
    .locals 1

    iget-object v0, p0, Lo/W;->b:Lo/ad;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    invoke-static {v0}, Lo/ad;->a([Lo/T;)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lo/W;->b:Lo/ad;

    :cond_0
    iget-object v0, p0, Lo/W;->b:Lo/ad;

    return-object v0
.end method

.method public a(Lo/T;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lo/W;->a()Lo/ad;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/ad;->a(Lo/T;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lo/W;->a:[Lo/T;

    array-length v6, v0

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    add-int/lit8 v3, v6, -0x1

    aget-object v0, v0, v3

    move v3, v2

    move-object v4, v0

    move v0, v2

    :goto_1
    if-ge v3, v6, :cond_2

    iget-object v5, p0, Lo/W;->a:[Lo/T;

    aget-object v5, v5, v3

    invoke-static {v4, v5, p1}, Lo/V;->b(Lo/T;Lo/T;Lo/T;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    move-object v4, v5

    goto :goto_1

    :cond_2
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    array-length v0, v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lo/W;

    if-eqz v0, :cond_1

    check-cast p1, Lo/W;

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    iget-object v1, p1, Lo/W;->a:[Lo/T;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lo/W;->a:[Lo/T;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
