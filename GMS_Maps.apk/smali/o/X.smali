.class public final Lo/X;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/ThreadLocal;


# instance fields
.field private final b:[I

.field private volatile c:Lo/ad;

.field private volatile d:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lo/Y;

    invoke-direct {v0}, Lo/Y;-><init>()V

    sput-object v0, Lo/X;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>([I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/X;->b:[I

    const/high16 v0, -0x40800000

    iput v0, p0, Lo/X;->d:F

    return-void
.end method

.method synthetic constructor <init>([ILo/Y;)V
    .locals 0

    invoke-direct {p0, p1}, Lo/X;-><init>([I)V

    return-void
.end method

.method private a(FIIILo/T;Lo/T;Lo/T;Lo/T;[Z)I
    .locals 15

    move/from16 v0, p3

    move-object/from16 v1, p5

    invoke-virtual {p0, v0, v1}, Lo/X;->a(ILo/T;)V

    move/from16 v0, p4

    move-object/from16 v1, p6

    invoke-virtual {p0, v0, v1}, Lo/X;->a(ILo/T;)V

    const/4 v8, -0x1

    add-int v6, p3, p2

    move/from16 v5, p1

    :goto_0
    add-int/lit8 v4, p4, -0x1

    if-gt v6, v4, :cond_0

    move-object/from16 v0, p8

    invoke-virtual {p0, v6, v0}, Lo/X;->a(ILo/T;)V

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p8

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v4

    cmpl-float v7, v4, v5

    if-lez v7, :cond_3

    move v8, v6

    :goto_1
    add-int v6, v6, p2

    move v5, v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    if-ltz v8, :cond_2

    const/4 v4, 0x1

    const/4 v5, 0x1

    aput-boolean v5, p9, v8

    add-int/lit8 v5, p3, 0x1

    if-le v8, v5, :cond_1

    move-object v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    invoke-direct/range {v4 .. v13}, Lo/X;->a(FIIILo/T;Lo/T;Lo/T;Lo/T;[Z)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v5, p4, -0x1

    if-ge v8, v5, :cond_2

    move-object v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    invoke-direct/range {v5 .. v14}, Lo/X;->a(FIIILo/T;Lo/T;Lo/T;Lo/T;[Z)I

    move-result v5

    add-int/2addr v4, v5

    :cond_2
    return v4

    :cond_3
    move v4, v5

    goto :goto_1
.end method

.method public static a(Ljava/io/DataInput;Lo/aq;)Lo/X;
    .locals 3

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0, p1, v2, v0}, Lo/T;->a(Ljava/io/DataInput;Lo/aq;[II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lo/X;

    invoke-direct {v0, v2}, Lo/X;-><init>([I)V

    return-object v0
.end method

.method public static a(Lo/T;Lo/T;)Lo/X;
    .locals 2

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lo/T;->a([II)V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lo/T;->a([II)V

    new-instance v1, Lo/X;

    invoke-direct {v1, v0}, Lo/X;-><init>([I)V

    return-object v1
.end method

.method public static a([I)Lo/X;
    .locals 1

    new-instance v0, Lo/X;

    invoke-direct {v0, p0}, Lo/X;-><init>([I)V

    return-object v0
.end method


# virtual methods
.method public a(F)Lo/T;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    invoke-virtual {p0, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/high16 v1, 0x3f800000

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    invoke-virtual {p0}, Lo/X;->c()Lo/T;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lo/X;->d()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p0, v1}, Lo/X;->b(I)F

    move-result v2

    cmpl-float v4, v2, v0

    if-ltz v4, :cond_2

    div-float v2, v0, v2

    new-instance v3, Lo/T;

    invoke-direct {v3}, Lo/T;-><init>()V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    invoke-virtual {p0, v1, v3}, Lo/X;->a(ILo/T;)V

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Lo/X;->a(ILo/T;)V

    invoke-static {v3, v0, v2, v0}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    goto :goto_0

    :cond_2
    sub-float v2, v0, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lo/X;->c()Lo/T;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lo/T;
    .locals 5

    mul-int/lit8 v0, p1, 0x3

    new-instance v1, Lo/T;

    iget-object v2, p0, Lo/X;->b:[I

    aget v2, v2, v0

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lo/T;-><init>(III)V

    return-object v1
.end method

.method public a()Lo/ad;
    .locals 3

    iget-object v0, p0, Lo/X;->c:Lo/ad;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    invoke-static {p0}, Lo/ad;->a(Lo/X;)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lo/X;->c:Lo/ad;

    :cond_0
    :goto_0
    iget-object v0, p0, Lo/X;->c:Lo/ad;

    return-object v0

    :cond_1
    new-instance v0, Lo/ad;

    new-instance v1, Lo/T;

    invoke-direct {v1}, Lo/T;-><init>()V

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    invoke-direct {v0, v1, v2}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lo/X;->c:Lo/ad;

    goto :goto_0
.end method

.method public a(ILo/T;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lo/X;->b:[I

    aget v1, v1, v0

    iput v1, p2, Lo/T;->a:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p2, Lo/T;->b:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p2, Lo/T;->c:I

    return-void
.end method

.method public a(ILo/T;Lo/T;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lo/X;->b:[I

    aget v1, v1, v0

    iget v2, p2, Lo/T;->a:I

    sub-int/2addr v1, v2

    iput v1, p3, Lo/T;->a:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iget v2, p2, Lo/T;->b:I

    sub-int/2addr v1, v2

    iput v1, p3, Lo/T;->b:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iget v1, p2, Lo/T;->c:I

    sub-int/2addr v0, v1

    iput v0, p3, Lo/T;->c:I

    return-void
.end method

.method public a(Lo/T;)V
    .locals 3

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    iget-object v1, p0, Lo/X;->b:[I

    aget v1, v1, v0

    iput v1, p1, Lo/T;->a:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p1, Lo/T;->b:I

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p1, Lo/T;->c:I

    return-void
.end method

.method public a(FI)[Z
    .locals 10

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    new-array v9, v0, [Z

    array-length v1, v9

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    :goto_0
    array-length v0, v9

    if-ge v3, v0, :cond_1

    aput-boolean v4, v9, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    aput-boolean v4, v9, v3

    add-int/lit8 v1, v0, -0x1

    aput-boolean v4, v9, v1

    mul-float v1, p1, p1

    add-int/lit8 v4, v0, -0x1

    new-instance v5, Lo/T;

    invoke-direct {v5}, Lo/T;-><init>()V

    new-instance v6, Lo/T;

    invoke-direct {v6}, Lo/T;-><init>()V

    new-instance v7, Lo/T;

    invoke-direct {v7}, Lo/T;-><init>()V

    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    move-object v0, p0

    move v2, p2

    invoke-direct/range {v0 .. v9}, Lo/X;->a(FIIILo/T;Lo/T;Lo/T;Lo/T;[Z)I

    :cond_1
    return-object v9
.end method

.method public b(I)F
    .locals 6

    mul-int/lit8 v0, p1, 0x3

    add-int/lit8 v1, v0, 0x3

    iget-object v2, p0, Lo/X;->b:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iget-object v2, p0, Lo/X;->b:[I

    add-int/lit8 v4, v1, 0x1

    aget v1, v2, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v5, v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v2, 0x1

    aget v2, v3, v2

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v5, 0x1

    aget v3, v3, v5

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public b(F)Lo/X;
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    div-int/lit8 v10, v0, 0x3

    new-array v9, v10, [Z

    aput-boolean v2, v9, v3

    add-int/lit8 v0, v10, -0x1

    aput-boolean v2, v9, v0

    mul-float v1, p1, p1

    add-int/lit8 v4, v10, -0x1

    new-instance v5, Lo/T;

    invoke-direct {v5}, Lo/T;-><init>()V

    new-instance v6, Lo/T;

    invoke-direct {v6}, Lo/T;-><init>()V

    new-instance v7, Lo/T;

    invoke-direct {v7}, Lo/T;-><init>()V

    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lo/X;->a(FIIILo/T;Lo/T;Lo/T;Lo/T;[Z)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-eq v0, v10, :cond_0

    mul-int/lit8 v0, v0, 0x3

    new-array v1, v0, [I

    move v0, v3

    :goto_1
    if-ge v3, v10, :cond_3

    aget-boolean v2, v9, v3

    if-eqz v2, :cond_2

    mul-int/lit8 v2, v3, 0x3

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lo/X;->b:[I

    add-int/lit8 v6, v2, 0x1

    aget v2, v5, v2

    aput v2, v1, v0

    add-int/lit8 v2, v4, 0x1

    iget-object v0, p0, Lo/X;->b:[I

    add-int/lit8 v5, v6, 0x1

    aget v0, v0, v6

    aput v0, v1, v4

    add-int/lit8 v0, v2, 0x1

    iget-object v4, p0, Lo/X;->b:[I

    add-int/lit8 v6, v5, 0x1

    aget v4, v4, v5

    aput v4, v1, v2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    new-instance p0, Lo/X;

    invoke-direct {p0, v1}, Lo/X;-><init>([I)V

    goto :goto_0
.end method

.method public b(Lo/T;)Lo/X;
    .locals 5

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    new-array v1, v0, [I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lo/X;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lo/X;->b:[I

    aget v2, v2, v0

    iget v3, p1, Lo/T;->a:I

    add-int/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget v4, p1, Lo/T;->b:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v0, 0x2

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    iget v4, p1, Lo/T;->c:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lo/X;

    invoke-direct {v0, v1}, Lo/X;-><init>([I)V

    return-object v0
.end method

.method public c()Lo/T;
    .locals 5

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    new-instance v1, Lo/T;

    iget-object v2, p0, Lo/X;->b:[I

    aget v2, v2, v0

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lo/T;-><init>(III)V

    return-object v1
.end method

.method public c(F)Lo/X;
    .locals 25

    invoke-virtual/range {p0 .. p0}, Lo/X;->e()Z

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lo/X;->b()I

    move-result v6

    add-int/lit8 v7, v6, -0x1

    const/4 v2, 0x2

    if-le v6, v2, :cond_0

    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    const/4 v2, 0x3

    if-gt v6, v2, :cond_1

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v8, Lo/Z;

    invoke-direct {v8, v6}, Lo/Z;-><init>(I)V

    if-eqz v5, :cond_3

    add-int/lit8 v2, v7, -0x1

    :goto_1
    add-int/lit8 v3, v2, -0x1

    mul-int/lit8 v3, v3, 0x3

    mul-int/lit8 v4, v2, 0x3

    add-int/lit8 v9, v2, 0x1

    rem-int/2addr v9, v6

    mul-int/lit8 v9, v9, 0x3

    add-int/lit8 v2, v2, 0x2

    rem-int/2addr v2, v6

    mul-int/lit8 v2, v2, 0x3

    new-instance v10, Lo/T;

    move-object/from16 v0, p0

    iget-object v11, v0, Lo/X;->b:[I

    aget v11, v11, v3

    move-object/from16 v0, p0

    iget-object v12, v0, Lo/X;->b:[I

    add-int/lit8 v3, v3, 0x1

    aget v3, v12, v3

    const/4 v12, 0x0

    invoke-direct {v10, v11, v3, v12}, Lo/T;-><init>(III)V

    new-instance v11, Lo/T;

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/X;->b:[I

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lo/X;->b:[I

    add-int/lit8 v4, v4, 0x1

    aget v4, v12, v4

    const/4 v12, 0x0

    invoke-direct {v11, v3, v4, v12}, Lo/T;-><init>(III)V

    new-instance v12, Lo/T;

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/X;->b:[I

    aget v3, v3, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lo/X;->b:[I

    add-int/lit8 v9, v9, 0x1

    aget v4, v4, v9

    const/4 v9, 0x0

    invoke-direct {v12, v3, v4, v9}, Lo/T;-><init>(III)V

    new-instance v9, Lo/T;

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/X;->b:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lo/X;->b:[I

    add-int/lit8 v13, v2, 0x1

    aget v4, v4, v13

    const/4 v13, 0x0

    invoke-direct {v9, v3, v4, v13}, Lo/T;-><init>(III)V

    new-instance v13, Lo/T;

    invoke-direct {v13}, Lo/T;-><init>()V

    if-eqz v5, :cond_4

    invoke-virtual {v11, v12}, Lo/T;->c(Lo/T;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_4

    const/4 v3, 0x1

    move v4, v3

    :goto_2
    if-nez v5, :cond_8

    invoke-virtual {v8, v10}, Lo/Z;->a(Lo/T;)Z

    if-nez v2, :cond_6

    invoke-virtual {v10, v11}, Lo/T;->c(Lo/T;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v11, v12}, Lo/T;->c(Lo/T;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_2

    invoke-virtual {v8, v11}, Lo/Z;->a(Lo/T;)Z

    :cond_2
    invoke-virtual {v8, v12}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v8}, Lo/Z;->d()Lo/X;

    move-result-object p0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    :cond_5
    invoke-virtual {v11, v12}, Lo/T;->b(Lo/T;)V

    invoke-virtual {v12, v9}, Lo/T;->b(Lo/T;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/X;->b:[I

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lo/X;->b:[I

    add-int/lit8 v15, v2, 0x1

    aget v14, v14, v15

    const/4 v15, 0x0

    invoke-virtual {v9, v3, v14, v15}, Lo/T;->a(III)V

    :cond_6
    invoke-virtual {v10, v11}, Lo/T;->c(Lo/T;)F

    move-result v3

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_8

    add-int/lit8 v2, v2, 0x3

    mul-int/lit8 v3, v6, 0x3

    if-ne v2, v3, :cond_5

    invoke-virtual {v10, v12}, Lo/T;->c(Lo/T;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v12, v9}, Lo/T;->c(Lo/T;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_7

    invoke-virtual {v8, v12}, Lo/Z;->a(Lo/T;)Z

    :cond_7
    invoke-virtual {v8, v9}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v8}, Lo/Z;->d()Lo/X;

    move-result-object p0

    goto/16 :goto_0

    :cond_8
    move v3, v2

    :goto_3
    mul-int/lit8 v14, v6, 0x3

    if-ge v3, v14, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lo/X;->b:[I

    aget v14, v14, v3

    move-object/from16 v0, p0

    iget-object v15, v0, Lo/X;->b:[I

    add-int/lit8 v16, v3, 0x1

    aget v15, v15, v16

    invoke-virtual {v9, v14, v15}, Lo/T;->d(II)V

    if-eqz v4, :cond_9

    add-int/lit8 v14, v7, -0x1

    mul-int/lit8 v14, v14, 0x3

    if-ne v3, v14, :cond_a

    invoke-virtual {v9, v13}, Lo/T;->b(Lo/T;)V

    :cond_9
    invoke-virtual {v11, v12}, Lo/T;->c(Lo/T;)F

    move-result v14

    cmpl-float v15, v14, p1

    if-lez v15, :cond_c

    if-eqz v5, :cond_b

    if-ne v3, v2, :cond_b

    invoke-virtual {v13, v11}, Lo/T;->b(Lo/T;)V

    :goto_4
    invoke-virtual {v10, v11}, Lo/T;->b(Lo/T;)V

    invoke-virtual {v11, v12}, Lo/T;->b(Lo/T;)V

    invoke-virtual {v12, v9}, Lo/T;->b(Lo/T;)V

    :goto_5
    add-int/lit8 v3, v3, 0x3

    goto :goto_3

    :cond_a
    mul-int/lit8 v14, v7, 0x3

    if-ne v3, v14, :cond_9

    goto :goto_5

    :cond_b
    invoke-virtual {v8, v11}, Lo/Z;->a(Lo/T;)Z

    goto :goto_4

    :cond_c
    invoke-virtual {v10, v11}, Lo/T;->c(Lo/T;)F

    move-result v15

    invoke-virtual {v12, v9}, Lo/T;->c(Lo/T;)F

    move-result v16

    add-float/2addr v15, v14

    float-to-double v0, v15

    move-wide/from16 v17, v0

    add-float v14, v14, v16

    float-to-double v14, v14

    iget v0, v11, Lo/T;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v19, v0

    mul-double v19, v19, v17

    iget v0, v12, Lo/T;->a:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v14

    add-double v19, v19, v21

    add-double v21, v17, v14

    div-double v19, v19, v21

    iget v0, v11, Lo/T;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v21, v0

    mul-double v21, v21, v17

    iget v0, v12, Lo/T;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v23, v0

    mul-double v23, v23, v14

    add-double v21, v21, v23

    add-double v14, v14, v17

    div-double v14, v21, v14

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-int v14, v14

    move/from16 v0, v16

    invoke-virtual {v11, v0, v14}, Lo/T;->d(II)V

    invoke-virtual {v12, v9}, Lo/T;->b(Lo/T;)V

    goto :goto_5

    :cond_d
    if-nez v5, :cond_e

    invoke-virtual {v11, v12}, Lo/T;->c(Lo/T;)F

    move-result v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_f

    :cond_e
    invoke-virtual {v8, v11}, Lo/Z;->a(Lo/T;)Z

    :cond_f
    if-eqz v5, :cond_10

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lo/Z;->a(I)Lo/T;

    move-result-object v2

    invoke-virtual {v8, v2}, Lo/Z;->a(Lo/T;)Z

    :goto_6
    invoke-virtual {v8}, Lo/Z;->a()I

    move-result v2

    if-eq v2, v6, :cond_0

    invoke-virtual {v8}, Lo/Z;->d()Lo/X;

    move-result-object p0

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v8, v12}, Lo/Z;->a(Lo/T;)Z

    goto :goto_6
.end method

.method public c(I)Lo/X;
    .locals 9

    const/high16 v8, 0x40000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v0, -0x20000000

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-gez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v4

    new-instance v5, Lo/Z;

    invoke-direct {v5, v4}, Lo/Z;-><init>(I)V

    new-instance v6, Lo/T;

    invoke-direct {v6}, Lo/T;-><init>()V

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_5

    invoke-virtual {p0, v3, v6}, Lo/X;->a(ILo/T;)V

    if-eqz v0, :cond_4

    iget v7, v6, Lo/T;->a:I

    if-ge v7, p1, :cond_2

    iget v2, v6, Lo/T;->a:I

    add-int/2addr v2, v8

    iput v2, v6, Lo/T;->a:I

    move v2, v1

    :cond_2
    :goto_3
    invoke-virtual {v5, v6}, Lo/Z;->a(Lo/T;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget v7, v6, Lo/T;->a:I

    if-le v7, p1, :cond_2

    iget v2, v6, Lo/T;->a:I

    sub-int/2addr v2, v8

    iput v2, v6, Lo/T;->a:I

    move v2, v1

    goto :goto_3

    :cond_5
    if-eqz v2, :cond_0

    invoke-virtual {v5}, Lo/Z;->d()Lo/X;

    move-result-object p0

    goto :goto_0
.end method

.method public d()F
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lo/X;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lo/X;->b(I)F

    move-result v3

    add-float/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lo/X;->d:F

    :cond_1
    iget v0, p0, Lo/X;->d:F

    return v0
.end method

.method public d(I)F
    .locals 4

    mul-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lo/X;->b:[I

    add-int/lit8 v2, v0, 0x3

    aget v1, v1, v2

    iget-object v2, p0, Lo/X;->b:[I

    aget v2, v2, v0

    sub-int/2addr v1, v2

    iget-object v2, p0, Lo/X;->b:[I

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lo/X;->b:[I

    add-int/lit8 v0, v0, 0x1

    aget v0, v3, v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Lo/V;->a(II)F

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lo/X;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lo/X;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x3

    iget-object v3, p0, Lo/X;->b:[I

    aget v3, v3, v1

    iget-object v4, p0, Lo/X;->b:[I

    aget v4, v4, v2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lo/X;->b:[I

    aget v3, v3, v0

    iget-object v4, p0, Lo/X;->b:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lo/X;->b:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lo/X;->b:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v4, v2

    if-ne v3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lo/X;

    if-eqz v0, :cond_1

    check-cast p1, Lo/X;

    iget-object v0, p0, Lo/X;->b:[I

    iget-object v1, p1, Lo/X;->b:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 9

    const/high16 v0, -0x20000000

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lm/t;->a()Lm/t;

    move-result-object v2

    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {v1, v4}, Lo/T;->i(Lo/T;)V

    const/4 v1, 0x1

    :goto_1
    if-ge v1, v3, :cond_2

    new-instance v5, Lo/T;

    invoke-direct {v5, v4}, Lo/T;-><init>(Lo/T;)V

    invoke-virtual {p0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v6

    invoke-virtual {v6, v4}, Lo/T;->i(Lo/T;)V

    invoke-virtual {v5}, Lo/T;->f()I

    move-result v5

    invoke-static {v5}, Lo/T;->c(I)I

    move-result v5

    invoke-static {v5}, Lm/t;->a(I)D

    move-result-wide v5

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v7

    invoke-static {v7}, Lo/T;->c(I)I

    move-result v7

    invoke-static {v7}, Lm/t;->a(I)D

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Lm/t;->a(DD)Lm/t;

    move-result-object v5

    invoke-virtual {v2, v5}, Lm/t;->b(Lm/t;)Lm/t;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lm/t;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lm/t;->a(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lm/t;->a(D)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lm/t;->i()Lm/t;

    move-result-object v0

    invoke-virtual {v0}, Lm/t;->h()D

    move-result-wide v0

    invoke-static {v0, v1}, Lm/t;->c(D)I

    move-result v0

    invoke-static {v0}, Lo/T;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public g()Lo/X;
    .locals 6

    iget-object v0, p0, Lo/X;->b:[I

    array-length v1, v0

    new-array v2, v1, [I

    iget-object v3, p0, Lo/X;->b:[I

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lo/X;->b:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    sub-int v4, v1, v0

    add-int/lit8 v4, v4, -0x3

    aget v4, v3, v4

    aput v4, v2, v0

    add-int/lit8 v4, v0, 0x1

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x2

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v4, v0, 0x2

    sub-int v5, v1, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v3, v5

    aput v5, v2, v4

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    new-instance v0, Lo/X;

    invoke-direct {v0, v2}, Lo/X;-><init>([I)V

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lo/X;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit16 v0, v0, 0xa0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lo/X;->b:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    return v0
.end method
