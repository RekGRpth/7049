.class public Lo/aB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lo/aB;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lo/aB;->a:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/common/collect/Maps;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Lo/aB;Lo/av;)Lo/at;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/aB;)I
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lo/av;->values()[Lo/av;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    invoke-virtual {p0, v0}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v5

    invoke-virtual {p1, v0}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v0

    if-nez v5, :cond_1

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-interface {v5, v0}, Lo/at;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(LA/c;)Lo/aB;
    .locals 4

    new-instance v1, Lo/aB;

    invoke-direct {v1}, Lo/aB;-><init>()V

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/at;

    invoke-interface {v0, p1}, Lo/at;->a(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Lo/aB;->a(Lo/at;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public a(Lo/av;)Lo/at;
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/at;

    return-object v0
.end method

.method public a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 3

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/at;

    invoke-interface {v0, p1}, Lo/at;->a(LA/c;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2}, Lo/at;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lo/at;)V
    .locals 2

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {p1}, Lo/at;->a()Lo/av;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public b(Lo/av;)Z
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lo/aB;

    invoke-virtual {p0, p1}, Lo/aB;->a(Lo/aB;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    check-cast p1, Lo/aB;

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    iget-object v1, p1, Lo/aB;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_1
    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lo/aB;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
