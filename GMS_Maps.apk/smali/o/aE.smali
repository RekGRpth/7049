.class public Lo/aE;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/Integer;


# instance fields
.field private b:Lo/o;

.field private c:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lo/aE;->a:[Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()Lo/aE;
    .locals 1

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-object p0
.end method

.method public a(I)Lo/aE;
    .locals 2

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lo/aD;)Lo/aE;
    .locals 5

    invoke-static {p1}, Lo/aD;->a(Lo/aD;)Lo/o;

    move-result-object v0

    iput-object v0, p0, Lo/aE;->b:Lo/o;

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-static {p1}, Lo/aD;->b(Lo/aD;)[Ljava/lang/Integer;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lo/aE;->c:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Lo/o;)Lo/aE;
    .locals 0

    iput-object p1, p0, Lo/aE;->b:Lo/o;

    return-object p0
.end method

.method public b()Lo/aD;
    .locals 4

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    new-instance v1, Lo/aD;

    iget-object v2, p0, Lo/aE;->b:Lo/o;

    iget-object v0, p0, Lo/aE;->c:Ljava/util/Set;

    sget-object v3, Lo/aE;->a:[Ljava/lang/Integer;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-direct {v1, v2, v0}, Lo/aD;-><init>(Lo/o;[Ljava/lang/Integer;)V

    return-object v1
.end method
