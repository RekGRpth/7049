.class public Lo/aI;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lo/o;

.field protected final b:Lo/n;

.field protected final c:I

.field protected final d:I


# direct methods
.method protected constructor <init>(Lo/o;Lo/n;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/aI;->a:Lo/o;

    iput-object p2, p0, Lo/aI;->b:Lo/n;

    iput p3, p0, Lo/aI;->c:I

    iput p4, p0, Lo/aI;->d:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/aI;
    .locals 4

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    const/4 v0, -0x1

    invoke-static {v2}, Lo/aI;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, p1}, Lo/aL;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v3

    invoke-static {v2}, Lo/aI;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    :cond_0
    new-instance v1, Lo/aJ;

    invoke-direct {v1, v3, v2, v0}, Lo/aJ;-><init>(Lo/n;II)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x2

    invoke-static {v2, v0}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v0

    :goto_1
    new-instance v1, Lo/aK;

    invoke-direct {v1, v0}, Lo/aK;-><init>(Lo/o;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(I)Z
    .locals 1

    invoke-static {p0}, Lo/aI;->b(I)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method
