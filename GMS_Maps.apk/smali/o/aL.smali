.class public Lo/aL;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/m;


# instance fields
.field private final a:Lo/aq;

.field private final b:I

.field private final c:B

.field private final d:[Lo/n;

.field private final e:Lo/al;

.field private final f:[Ljava/lang/String;

.field private final g:J

.field private final h:[Ljava/lang/String;

.field private final i:[Ljava/lang/String;

.field private final j:[Ljava/lang/String;

.field private final k:I

.field private final l:LA/c;

.field private final m:[Lo/aI;

.field private final n:I

.field private o:J


# direct methods
.method protected constructor <init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lo/aL;->i:[Ljava/lang/String;

    iput-object p1, p0, Lo/aL;->e:Lo/al;

    iput-object p2, p0, Lo/aL;->f:[Ljava/lang/String;

    iput-object p3, p0, Lo/aL;->a:Lo/aq;

    iput p4, p0, Lo/aL;->b:I

    iput-byte p5, p0, Lo/aL;->c:B

    iput-object p7, p0, Lo/aL;->h:[Ljava/lang/String;

    iput-object p8, p0, Lo/aL;->j:[Ljava/lang/String;

    iput p9, p0, Lo/aL;->k:I

    iput-object p10, p0, Lo/aL;->d:[Lo/n;

    iput-object p11, p0, Lo/aL;->l:LA/c;

    iput-object p12, p0, Lo/aL;->m:[Lo/aI;

    iput p6, p0, Lo/aL;->n:I

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lo/aL;->g:J

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lo/aL;->o:J

    return-void
.end method

.method public static a(Lo/aq;Ljava/io/DataInput;IBIILA/c;JJ)Lo/aL;
    .locals 19

    invoke-static/range {p1 .. p1}, Lo/aL;->a(Ljava/io/DataInput;)V

    invoke-static/range {p1 .. p1}, Lo/aq;->a(Ljava/io/DataInput;)Lo/aq;

    move-result-object v2

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->c()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->d()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->b()I

    move-result v4

    if-eq v3, v4, :cond_1

    :cond_0
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Expected tile coords: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v11

    if-lez v11, :cond_2

    add-int/lit16 v11, v11, 0x7d0

    :cond_2
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v9, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v10, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-static/range {p1 .. p2}, Lo/al;->a(Ljava/io/DataInput;I)Lo/al;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    new-instance v5, Lo/as;

    move/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1, v3, v4}, Lo/as;-><init>(ILo/aq;Lo/al;[Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    new-array v12, v6, [Lo/n;

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v6, :cond_6

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lo/aL;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v7

    aput-object v7, v12, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    new-array v14, v6, [Lo/aI;

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v6, :cond_7

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lo/aI;->a(Ljava/io/DataInput;Lo/as;)Lo/aI;

    move-result-object v7

    aput-object v7, v14, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    new-instance v2, Lo/aL;

    move-object/from16 v5, p0

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p5

    move-object/from16 v13, p6

    move-wide/from16 v15, p7

    move-wide/from16 v17, p9

    invoke-direct/range {v2 .. v18}, Lo/aL;-><init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V

    return-object v2
.end method

.method public static a(Lo/aq;[BILA/c;JJ)Lo/aL;
    .locals 14

    invoke-static/range {p1 .. p2}, Lo/aL;->a([BI)[J

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    aget-wide v3, v0, v1

    long-to-int v8, v3

    const/4 v1, 0x2

    aget-wide v2, v0, v2

    long-to-int v9, v2

    const/4 v2, 0x3

    aget-wide v3, v0, v1

    long-to-int v1, v3

    const/4 v3, 0x4

    aget-wide v4, v0, v2

    long-to-int v2, v4

    const/4 v5, 0x5

    aget-wide v3, v0, v3

    aget-wide v5, v0, v5

    long-to-int v0, v5

    int-to-byte v10, v0

    packed-switch v1, :pswitch_data_0

    const/16 v0, 0x1a

    :goto_0
    add-int v6, p2, v0

    array-length v0, p1

    sub-int v7, v0, v6

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v7}, Lo/aL;->a(Lo/aq;IIJ[BII)V

    :try_start_0
    invoke-static {p1, v6, v7}, LR/f;->a([BII)LR/g;

    move-result-object v0

    invoke-virtual {v0}, LR/g;->a()[B

    move-result-object v13

    invoke-virtual {v0}, LR/g;->b()I

    move-result v0

    new-instance v3, Laq/a;

    invoke-direct {v3, v13}, Laq/a;-><init>([B)V

    move-object v2, p0

    move v4, v1

    move v5, v10

    move v6, v8

    move v7, v9

    move-object/from16 v8, p3

    move-wide/from16 v9, p4

    move-wide/from16 v11, p6

    invoke-static/range {v2 .. v12}, Lo/aL;->a(Lo/aq;Ljava/io/DataInput;IBIILA/c;JJ)Lo/aL;

    move-result-object v1

    invoke-virtual {v3}, Laq/a;->a()I

    move-result v2

    if-eq v2, v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Byte stream not fully read for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lo/aL;->d()Lo/aq;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const/16 v0, 0x1b

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {v13}, Lbm/g;->a([B)V
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method static a(Ljava/io/DataInput;Lo/as;)Lo/n;
    .locals 4

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown feature type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    invoke-static {p0, p1}, Lo/l;->a(Ljava/io/DataInput;Lo/as;)Lo/l;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_2
    invoke-static {p0, p1}, Lo/af;->a(Ljava/io/DataInput;Lo/as;)Lo/af;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p0, p1}, Lo/f;->a(Ljava/io/DataInput;Lo/as;)Lo/f;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-static {p0, p1}, Lo/g;->a(Ljava/io/DataInput;Lo/as;)Lo/g;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-static {p0, p1}, Lo/M;->a(Ljava/io/DataInput;Lo/as;)Lo/M;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-static {p0, p1}, Lo/ac;->a(Ljava/io/DataInput;Lo/as;)Lo/ac;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-static {p0, p1}, Lo/U;->a(Ljava/io/DataInput;Lo/as;)Lo/U;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    invoke-static {p0, p1}, Lo/K;->a(Ljava/io/DataInput;Lo/as;)Lo/K;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    invoke-static {p0, p1}, Lo/aC;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    invoke-static {p0, p1}, Lo/L;->b(Ljava/io/DataInput;Lo/as;)Lo/K;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public static a(II[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lo/O;->a(I[BI)V

    const/4 v0, 0x4

    invoke-static {p1, p2, v0}, Lo/O;->a(I[BI)V

    return-void
.end method

.method private static a(Ljava/io/DataInput;)V
    .locals 4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const v1, 0x44524154

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TILE_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method private static a(Lo/aq;IIJ[BII)V
    .locals 12

    new-instance v11, Lr/y;

    invoke-direct {v11}, Lr/y;-><init>()V

    const/16 v3, 0x28

    new-array v10, v3, [B

    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v3

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v4

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v5

    move v6, p1

    move v7, p2

    move-wide v8, p3

    invoke-static/range {v3 .. v10}, Lr/y;->a(IIIIIJ[B)V

    const/16 v3, 0x100

    invoke-virtual {v11, v10, v3}, Lr/y;->a([BI)V

    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {v11, v0, v1, v2}, Lr/y;->a([BII)V

    return-void
.end method

.method public static a(Lo/ap;)Z
    .locals 2

    invoke-interface {p0}, Lo/ap;->g()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->q:LA/c;

    if-ne v0, v1, :cond_0

    move-object v0, p0

    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->r()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    instance-of v0, p0, Lo/N;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([BI)[J
    .locals 11

    const/16 v8, 0xa

    const/4 v1, 0x0

    new-instance v0, Laq/a;

    invoke-direct {v0, p0}, Laq/a;-><init>([B)V

    invoke-virtual {v0, p1}, Laq/a;->skipBytes(I)I

    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v2

    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v3

    invoke-static {v0}, Lo/aL;->a(Ljava/io/DataInput;)V

    invoke-virtual {v0}, Laq/a;->readUnsignedShort()I

    move-result v4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_0

    if-eq v4, v8, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version mismatch: 9 or 10 expected, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v5

    invoke-virtual {v0}, Laq/a;->readLong()J

    move-result-wide v6

    if-ne v4, v8, :cond_1

    invoke-virtual {v0}, Laq/a;->readUnsignedByte()I

    move-result v0

    :goto_0
    const/4 v8, 0x6

    new-array v8, v8, [J

    int-to-long v9, v2

    aput-wide v9, v8, v1

    const/4 v1, 0x1

    int-to-long v2, v3

    aput-wide v2, v8, v1

    const/4 v1, 0x2

    int-to-long v2, v4

    aput-wide v2, v8, v1

    const/4 v1, 0x3

    int-to-long v2, v5

    aput-wide v2, v8, v1

    const/4 v1, 0x4

    aput-wide v6, v8, v1

    const/4 v1, 0x5

    int-to-long v2, v0

    aput-wide v2, v8, v1

    return-object v8

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lo/aL;)[Lo/n;
    .locals 1

    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    return-object v0
.end method

.method public static s()J
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->e()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static t()I
    .locals 4

    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static u()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lo/aL;->g:J

    return-wide v0
.end method

.method public a(I)Lo/n;
    .locals 1

    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, Lo/aL;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lo/aL;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lo/aL;->o:J

    return-wide v0
.end method

.method public b(I)Lo/aI;
    .locals 1

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, Lo/aL;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lo/aL;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/google/googlenav/common/a;)V
    .locals 4

    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-static {}, Lo/aL;->s()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lo/aL;->o:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lo/aL;->o:J

    goto :goto_0
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/aL;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public d()Lo/aq;
    .locals 1

    iget-object v0, p0, Lo/aL;->a:Lo/aq;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lo/aL;->b:I

    return v0
.end method

.method public f()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/aL;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public g()LA/c;
    .locals 1

    iget-object v0, p0, Lo/aL;->l:LA/c;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lo/aL;->n:I

    return v0
.end method

.method public i()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/k;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    invoke-virtual {p0}, Lo/aL;->n()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lo/aL;->o()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    :cond_3
    :goto_0
    return v1

    :cond_4
    invoke-virtual {p0}, Lo/aL;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    move v1, v0

    goto :goto_0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    array-length v0, v0

    return v0
.end method

.method public k()Lo/aO;
    .locals 2

    new-instance v0, Lo/aP;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lo/aP;-><init>(Lo/aL;Lo/aM;)V

    return-object v0
.end method

.method public m()B
    .locals 1

    iget-byte v0, p0, Lo/aL;->c:B

    return v0
.end method

.method public n()Z
    .locals 1

    iget-byte v0, p0, Lo/aL;->c:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    iget-byte v0, p0, Lo/aL;->c:B

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    iget v0, p0, Lo/aL;->k:I

    return v0
.end method

.method protected q()[Lo/n;
    .locals 1

    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    return-object v0
.end method

.method public r()I
    .locals 1

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
