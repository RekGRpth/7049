.class public final Lo/aR;
.super Lo/aS;
.source "SourceFile"


# instance fields
.field private b:Lo/ad;

.field private c:Lo/T;

.field private d:Lo/T;

.field private e:I

.field private f:I

.field private g:I

.field private volatile h:Lo/T;

.field private volatile i:Lo/T;

.field private volatile j:Lo/T;

.field private volatile k:Lo/T;

.field private volatile l:Lo/T;

.field private volatile m:Lo/T;


# direct methods
.method private constructor <init>(Lo/ad;)V
    .locals 4

    const/high16 v3, 0x40000000

    invoke-direct {p0}, Lo/aS;-><init>()V

    iput-object p1, p0, Lo/aR;->b:Lo/ad;

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v2

    iget v0, v1, Lo/T;->a:I

    if-gez v0, :cond_1

    iget v0, v1, Lo/T;->a:I

    neg-int v0, v0

    iput v0, p0, Lo/aR;->e:I

    :cond_0
    :goto_0
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lo/aR;->c:Lo/T;

    iget-object v0, p0, Lo/aR;->c:Lo/T;

    invoke-virtual {v1, v0}, Lo/T;->i(Lo/T;)V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lo/aR;->d:Lo/T;

    iget-object v0, p0, Lo/aR;->d:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->i(Lo/T;)V

    iget-object v0, p0, Lo/aR;->c:Lo/T;

    iget v0, v0, Lo/T;->a:I

    iget-object v3, p0, Lo/aR;->d:Lo/T;

    iget v3, v3, Lo/T;->a:I

    if-le v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lo/aR;->a:Z

    iget v0, v1, Lo/T;->a:I

    iget v1, p0, Lo/aR;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lo/aR;->f:I

    iget v0, v2, Lo/T;->a:I

    iget v1, p0, Lo/aR;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lo/aR;->g:I

    return-void

    :cond_1
    iget v0, v2, Lo/T;->a:I

    if-le v0, v3, :cond_0

    iget v0, v2, Lo/T;->a:I

    sub-int v0, v3, v0

    iput v0, p0, Lo/aR;->e:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lo/ad;)Lo/aR;
    .locals 1

    new-instance v0, Lo/aR;

    invoke-direct {v0, p0}, Lo/aR;-><init>(Lo/ad;)V

    return-object v0
.end method


# virtual methods
.method public a(I)Lo/T;
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lo/aR;->h:Lo/T;

    if-nez v0, :cond_0

    new-instance v0, Lo/T;

    iget-object v1, p0, Lo/aR;->d:Lo/T;

    iget v1, v1, Lo/T;->a:I

    iget-object v2, p0, Lo/aR;->c:Lo/T;

    iget v2, v2, Lo/T;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->h:Lo/T;

    :cond_0
    iget-object v0, p0, Lo/aR;->h:Lo/T;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lo/aR;->d:Lo/T;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lo/aR;->i:Lo/T;

    if-nez v0, :cond_1

    new-instance v0, Lo/T;

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->a:I

    iget-object v2, p0, Lo/aR;->d:Lo/T;

    iget v2, v2, Lo/T;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->i:Lo/T;

    :cond_1
    iget-object v0, p0, Lo/aR;->i:Lo/T;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lo/aR;->c:Lo/T;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a()Lo/aR;
    .locals 0

    return-object p0
.end method

.method public a(I[Lo/T;)V
    .locals 6

    const/4 v1, 0x3

    const/4 v5, 0x2

    const v2, -0x20000001

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lo/aR;->a:Z

    if-eqz v0, :cond_4

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v3}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v4}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v4}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lo/aR;->j:Lo/T;

    if-nez v0, :cond_0

    new-instance v0, Lo/T;

    iget-object v1, p0, Lo/aR;->d:Lo/T;

    iget v1, v1, Lo/T;->b:I

    invoke-direct {v0, v2, v1}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->j:Lo/T;

    :cond_0
    iget-object v0, p0, Lo/aR;->j:Lo/T;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lo/aR;->k:Lo/T;

    if-nez v0, :cond_1

    new-instance v0, Lo/T;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lo/aR;->d:Lo/T;

    iget v2, v2, Lo/T;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->k:Lo/T;

    :cond_1
    iget-object v0, p0, Lo/aR;->k:Lo/T;

    aput-object v0, p2, v3

    invoke-virtual {p0, v5}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v5}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v3

    invoke-virtual {p0, v1}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v1}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v3

    iget-object v0, p0, Lo/aR;->l:Lo/T;

    if-nez v0, :cond_2

    new-instance v0, Lo/T;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lo/aR;->c:Lo/T;

    iget v2, v2, Lo/T;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->l:Lo/T;

    :cond_2
    iget-object v0, p0, Lo/aR;->l:Lo/T;

    aput-object v0, p2, v4

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lo/aR;->m:Lo/T;

    if-nez v0, :cond_3

    new-instance v0, Lo/T;

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->b:I

    invoke-direct {v0, v2, v1}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lo/aR;->m:Lo/T;

    :cond_3
    iget-object v0, p0, Lo/aR;->m:Lo/T;

    aput-object v0, p2, v3

    invoke-virtual {p0, v3}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v3

    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lo/aR;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lo/T;)Z
    .locals 2

    iget v0, p1, Lo/T;->a:I

    iget v1, p0, Lo/aR;->e:I

    add-int/2addr v0, v1

    const v1, 0x3fffffff

    and-int/2addr v0, v1

    iget v1, p0, Lo/aR;->f:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lo/aR;->g:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lo/T;->b:I

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lo/T;->b:I

    iget-object v1, p0, Lo/aR;->d:Lo/T;

    iget v1, v1, Lo/T;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lo/ae;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v7, 0x20000000

    const/high16 v6, -0x20000000

    const/high16 v5, 0x40000000

    iget-boolean v0, p0, Lo/aR;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lo/aR;->b:Lo/ad;

    invoke-virtual {v0, p1}, Lo/ad;->b(Lo/ae;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v0

    iget-object v3, p0, Lo/aR;->c:Lo/T;

    iget v3, v3, Lo/T;->b:I

    iget-object v4, v0, Lo/ad;->a:Lo/T;

    iget v4, v4, Lo/T;->b:I

    if-gt v3, v4, :cond_2

    iget-object v3, p0, Lo/aR;->d:Lo/T;

    iget v3, v3, Lo/T;->b:I

    iget-object v4, v0, Lo/ad;->b:Lo/T;

    iget v4, v4, Lo/T;->b:I

    if-ge v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lo/ad;->a:Lo/T;

    iget v3, v3, Lo/T;->a:I

    iget-object v0, v0, Lo/ad;->b:Lo/T;

    iget v0, v0, Lo/T;->a:I

    iget-object v4, p0, Lo/aR;->c:Lo/T;

    iget v4, v4, Lo/T;->a:I

    if-gt v4, v3, :cond_4

    if-gt v7, v0, :cond_0

    :cond_4
    if-gt v6, v3, :cond_5

    iget-object v4, p0, Lo/aR;->d:Lo/T;

    iget v4, v4, Lo/T;->a:I

    if-ge v4, v0, :cond_0

    :cond_5
    if-ge v3, v6, :cond_8

    add-int/2addr v3, v5

    :cond_6
    :goto_1
    if-ge v0, v6, :cond_9

    add-int/2addr v0, v5

    :cond_7
    :goto_2
    iget-object v4, p0, Lo/aR;->c:Lo/T;

    iget v4, v4, Lo/T;->a:I

    if-gt v4, v3, :cond_a

    iget-object v3, p0, Lo/aR;->d:Lo/T;

    iget v3, v3, Lo/T;->a:I

    if-lt v3, v0, :cond_a

    move v0, v1

    :goto_3
    move v1, v0

    goto :goto_0

    :cond_8
    if-lt v3, v7, :cond_6

    sub-int/2addr v3, v5

    goto :goto_1

    :cond_9
    if-lt v0, v7, :cond_7

    sub-int/2addr v0, v5

    goto :goto_2

    :cond_a
    move v0, v2

    goto :goto_3
.end method

.method public b()Lo/ad;
    .locals 1

    iget-object v0, p0, Lo/aR;->b:Lo/ad;

    return-object v0
.end method

.method public b(Lo/ae;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lo/aR;->a:Z

    if-nez v1, :cond_1

    iget-object v0, p0, Lo/aR;->b:Lo/ad;

    invoke-virtual {v0, p1}, Lo/ad;->a(Lo/ae;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lo/ad;

    if-eqz v1, :cond_4

    check-cast p1, Lo/ad;

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->b:I

    iget-object v2, p1, Lo/ad;->b:Lo/T;

    iget v2, v2, Lo/T;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lo/aR;->d:Lo/T;

    iget v1, v1, Lo/T;->b:I

    iget-object v2, p1, Lo/ad;->a:Lo/T;

    iget v2, v2, Lo/T;->b:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->a:I

    iget-object v2, p1, Lo/ad;->b:Lo/T;

    iget v2, v2, Lo/T;->a:I

    if-gt v1, v2, :cond_2

    const/high16 v1, 0x20000000

    iget-object v2, p1, Lo/ad;->a:Lo/T;

    iget v2, v2, Lo/T;->a:I

    if-gt v1, v2, :cond_3

    :cond_2
    const/high16 v1, -0x20000000

    iget-object v2, p1, Lo/ad;->b:Lo/T;

    iget v2, v2, Lo/T;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lo/aR;->d:Lo/T;

    iget v1, v1, Lo/T;->a:I

    iget-object v2, p1, Lo/ad;->a:Lo/T;

    iget v2, v2, Lo/T;->a:I

    if-lt v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lo/aS;->b(Lo/ae;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic c()Lo/ae;
    .locals 1

    invoke-virtual {p0}, Lo/aR;->b()Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method public d()Lo/T;
    .locals 2

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iget-object v1, p0, Lo/aR;->b:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo/T;->i(Lo/T;)V

    return-object v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lo/aR;->b:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lo/aR;->d:Lo/T;

    iget v0, v0, Lo/T;->b:I

    iget-object v1, p0, Lo/aR;->c:Lo/T;

    iget v1, v1, Lo/T;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public g()Lo/T;
    .locals 1

    iget-object v0, p0, Lo/aR;->c:Lo/T;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-boolean v0, p0, Lo/aR;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public i()Lo/T;
    .locals 1

    iget-object v0, p0, Lo/aR;->d:Lo/T;

    return-object v0
.end method
