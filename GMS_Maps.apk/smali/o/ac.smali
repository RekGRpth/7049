.class public Lo/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:I

.field private final d:Lo/aj;

.field private final e:[I


# direct methods
.method public constructor <init>(I[BILo/aj;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lo/ac;->a:I

    iput-object p2, p0, Lo/ac;->b:[B

    iput p3, p0, Lo/ac;->c:I

    iput-object p4, p0, Lo/ac;->d:Lo/aj;

    iput-object p5, p0, Lo/ac;->e:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/ac;
    .locals 7

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    new-array v2, v0, [B

    invoke-interface {p0, v2}, Ljava/io/DataInput;->readFully([B)V

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v3

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v5, v4, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    aput v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lo/ac;

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lo/ac;-><init>(I[BILo/aj;[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .locals 1

    sget-object v0, Lo/o;->a:Lo/o;

    return-object v0
.end method

.method public b()[B
    .locals 1

    iget-object v0, p0, Lo/ac;->b:[B

    return-object v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/ac;->d:Lo/aj;

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/ac;->c:I

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/ac;->e:[I

    return-object v0
.end method

.method public m()I
    .locals 1

    iget-object v0, p0, Lo/ac;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x20

    return v0
.end method
