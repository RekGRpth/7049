.class public Lo/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/X;

.field private final c:[Lo/H;

.field private final d:Lo/aj;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/af;->a:Lo/o;

    iput-object p2, p0, Lo/af;->b:Lo/X;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    new-array p3, v0, [Lo/H;

    :cond_0
    iput-object p3, p0, Lo/af;->c:[Lo/H;

    iput-object p4, p0, Lo/af;->d:Lo/aj;

    iput p5, p0, Lo/af;->e:I

    iput-object p6, p0, Lo/af;->f:Ljava/lang/String;

    iput p7, p0, Lo/af;->g:I

    iput p8, p0, Lo/af;->h:I

    iput p9, p0, Lo/af;->i:I

    iput-object p10, p0, Lo/af;->j:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/af;
    .locals 11

    const/4 v0, 0x0

    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v1

    invoke-static {p0, v1}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v2

    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v3, v4, [Lo/H;

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-static {p0, p1, v6}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v5

    aput-object v5, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v7

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v8

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    const/4 v1, 0x0

    const/4 v5, 0x1

    invoke-static {v5, v4}, Lo/O;->a(II)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    :cond_1
    :goto_1
    ushr-int/lit8 v9, v4, 0x2

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v10, v4, [I

    :goto_2
    if-ge v0, v4, :cond_3

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v10, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x2

    invoke-static {v5, v4}, Lo/O;->a(II)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v0, Lo/af;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lo/H;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lo/o;
    .locals 1

    iget-object v0, p0, Lo/af;->a:Lo/o;

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lo/H;->b()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lo/X;
    .locals 1

    iget-object v0, p0, Lo/af;->b:Lo/X;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    array-length v0, v0

    goto :goto_0
.end method

.method public c(I)Lo/H;
    .locals 1

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    array-length v0, v0

    goto :goto_0
.end method

.method public d(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lo/af;->c:[Lo/H;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lo/H;->b()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lo/af;->c:[Lo/H;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lo/H;->a(I)Lo/I;

    move-result-object v1

    invoke-virtual {v1}, Lo/I;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/af;->d:Lo/aj;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lo/af;->g:I

    return v0
.end method

.method public g()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/af;->h:I

    return v0
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/af;->j:[I

    return-object v0
.end method

.method public m()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lo/af;->b:Lo/X;

    invoke-virtual {v1}, Lo/X;->h()I

    move-result v1

    add-int/lit8 v3, v1, 0x38

    iget-object v1, p0, Lo/af;->c:[Lo/H;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lo/af;->c:[Lo/H;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lo/H;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lo/af;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/af;->d:Lo/aj;

    invoke-static {v1}, Lo/O;->a(Lo/aj;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/af;->f:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method

.method public n()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 2

    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v0

    invoke-virtual {v0}, Lo/aj;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lo/af;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lo/af;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lo/af;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lo/af;->g:I

    const/16 v1, 0x80

    if-ge v0, v1, :cond_2

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 2

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method
