.class public Lo/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/at;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/ag;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Lo/av;
    .locals 1

    sget-object v0, Lo/av;->a:Lo/av;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/16 v0, 0xa

    iget-object v1, p0, Lo/ag;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(LA/c;)Z
    .locals 1

    sget-object v0, LA/c;->r:LA/c;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lo/at;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lo/ag;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lo/at;)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lo/ag;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lo/at;

    invoke-virtual {p0, p1}, Lo/ag;->b(Lo/at;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lo/ag;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lo/ag;

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lo/ag;

    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    iget-object v1, p1, Lo/ag;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lo/ag;->a:Ljava/lang/String;

    goto :goto_0
.end method
