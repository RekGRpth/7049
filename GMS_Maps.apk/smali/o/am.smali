.class public final Lo/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/X;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Lo/X;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/am;->a:Lo/X;

    iput p2, p0, Lo/am;->b:I

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    iput v0, p0, Lo/am;->c:I

    return-void
.end method

.method public constructor <init>(Lo/X;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/am;->a:Lo/X;

    iput p2, p0, Lo/am;->b:I

    iput p3, p0, Lo/am;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget v0, p0, Lo/am;->c:I

    iget v1, p0, Lo/am;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(I)Lo/T;
    .locals 2

    iget-object v0, p0, Lo/am;->a:Lo/X;

    iget v1, p0, Lo/am;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public a(ILo/T;)V
    .locals 2

    iget-object v0, p0, Lo/am;->a:Lo/X;

    iget v1, p0, Lo/am;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2}, Lo/X;->a(ILo/T;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lo/am;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lo/am;->c:I

    return v0
.end method

.method public d()Lo/X;
    .locals 1

    iget-object v0, p0, Lo/am;->a:Lo/X;

    return-object v0
.end method

.method public e()Lo/X;
    .locals 6

    invoke-virtual {p0}, Lo/am;->a()I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    new-instance v3, Lo/T;

    invoke-direct {v3}, Lo/T;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v4, p0, Lo/am;->a:Lo/X;

    iget v5, p0, Lo/am;->b:I

    add-int/2addr v5, v0

    invoke-virtual {v4, v5, v3}, Lo/X;->a(ILo/T;)V

    invoke-virtual {v3, v2, v0}, Lo/T;->a([II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lo/X;->a([I)Lo/X;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lo/am;

    if-eqz v2, :cond_3

    check-cast p1, Lo/am;

    iget v2, p1, Lo/am;->b:I

    iget v3, p0, Lo/am;->b:I

    if-ne v2, v3, :cond_2

    iget v2, p1, Lo/am;->c:I

    iget v3, p0, Lo/am;->c:I

    if-ne v2, v3, :cond_2

    iget-object v2, p1, Lo/am;->a:Lo/X;

    iget-object v3, p0, Lo/am;->a:Lo/X;

    invoke-virtual {v2, v3}, Lo/X;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()Lo/ad;
    .locals 7

    iget-object v0, p0, Lo/am;->a:Lo/X;

    iget v1, p0, Lo/am;->b:I

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v5

    iget v2, v5, Lo/T;->a:I

    iget v1, v5, Lo/T;->b:I

    iget v0, p0, Lo/am;->b:I

    add-int/lit8 v0, v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    iget v6, p0, Lo/am;->c:I

    if-ge v0, v6, :cond_4

    iget-object v6, p0, Lo/am;->a:Lo/X;

    invoke-virtual {v6, v0, v5}, Lo/X;->a(ILo/T;)V

    iget v6, v5, Lo/T;->a:I

    if-ge v6, v4, :cond_0

    iget v4, v5, Lo/T;->a:I

    :cond_0
    iget v6, v5, Lo/T;->a:I

    if-le v6, v3, :cond_1

    iget v3, v5, Lo/T;->a:I

    :cond_1
    iget v6, v5, Lo/T;->b:I

    if-ge v6, v2, :cond_2

    iget v2, v5, Lo/T;->b:I

    :cond_2
    iget v6, v5, Lo/T;->b:I

    if-le v6, v1, :cond_3

    iget v1, v5, Lo/T;->b:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v5, v4, v2}, Lo/T;->d(II)V

    new-instance v0, Lo/T;

    invoke-direct {v0, v3, v1}, Lo/T;-><init>(II)V

    new-instance v1, Lo/ad;

    invoke-direct {v1, v5, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lo/am;->b:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/am;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/am;->a:Lo/X;

    invoke-virtual {v1}, Lo/X;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/am;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/am;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/am;->a:Lo/X;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
