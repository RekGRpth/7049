.class public Lo/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Lo/aB;

.field private h:Lo/aq;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lo/aq;-><init>(IIILo/aB;)V

    return-void
.end method

.method public constructor <init>(IIILo/aB;)V
    .locals 3

    const/high16 v2, 0x20000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lo/aq;->h:Lo/aq;

    iput p1, p0, Lo/aq;->d:I

    iput p2, p0, Lo/aq;->e:I

    iput p3, p0, Lo/aq;->f:I

    if-nez p4, :cond_0

    new-instance p4, Lo/aB;

    invoke-direct {p4}, Lo/aB;-><init>()V

    :cond_0
    iput-object p4, p0, Lo/aq;->g:Lo/aB;

    rsub-int/lit8 v0, p1, 0x12

    iput v0, p0, Lo/aq;->c:I

    const/high16 v0, 0x40000000

    shr-int/2addr v0, p1

    iget v1, p0, Lo/aq;->e:I

    mul-int/2addr v1, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lo/aq;->a:I

    iget v1, p0, Lo/aq;->f:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    sub-int/2addr v0, v2

    neg-int v0, v0

    iput v0, p0, Lo/aq;->b:I

    return-void
.end method

.method public static a(Lo/aR;I)Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;
    .locals 11

    const/4 v10, 0x2

    const/4 v3, 0x0

    if-gez p1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v5

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v6

    invoke-virtual {v5}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {v5}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {v6}, Lo/aq;->c()I

    move-result v7

    invoke-virtual {v6}, Lo/aq;->d()I

    move-result v8

    const/4 v0, 0x1

    shl-int v9, v0, p1

    if-le v1, v7, :cond_2

    sub-int v0, v9, v1

    add-int/2addr v0, v7

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    :goto_1
    if-gez v4, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_2
    sub-int v0, v7, v1

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    if-gt v4, v10, :cond_4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v4, v10, :cond_0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    if-le v1, v7, :cond_8

    move v4, v1

    :goto_2
    if-ge v4, v9, :cond_7

    move v1, v2

    :goto_3
    if-gt v1, v8, :cond_5

    new-instance v5, Lo/aq;

    invoke-direct {v5, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    :cond_7
    if-gt v3, v7, :cond_0

    move v1, v2

    :goto_4
    if-gt v1, v8, :cond_6

    new-instance v4, Lo/aq;

    invoke-direct {v4, p1, v3, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    move v3, v1

    :goto_5
    if-gt v3, v7, :cond_0

    move v1, v2

    :goto_6
    if-gt v1, v8, :cond_9

    new-instance v4, Lo/aq;

    invoke-direct {v4, p1, v3, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5
.end method

.method public static a(IIILo/aB;)Lo/aq;
    .locals 5

    const/high16 v4, 0x20000000

    const/16 v1, 0x1e

    const/4 v0, 0x0

    if-gtz p0, :cond_0

    new-instance v1, Lo/aq;

    invoke-direct {v1, v0, v0, v0}, Lo/aq;-><init>(III)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v1, :cond_1

    move p0, v1

    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    add-int v2, p1, v4

    shr-int/2addr v2, v1

    neg-int v3, p2

    add-int/2addr v3, v4

    shr-int v1, v3, v1

    const/4 v3, 0x1

    shl-int/2addr v3, p0

    if-gez v2, :cond_3

    add-int/2addr v2, v3

    :cond_2
    :goto_1
    if-gez v1, :cond_4

    :goto_2
    new-instance v1, Lo/aq;

    invoke-direct {v1, p0, v2, v0, p3}, Lo/aq;-><init>(IIILo/aB;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v2, v3, :cond_2

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_4
    if-lt v1, v3, :cond_5

    add-int/lit8 v0, v3, -0x1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(ILo/T;)Lo/aq;
    .locals 2

    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v1

    invoke-static {p0, v0, v1}, Lo/aq;->b(III)Lo/aq;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/aq;
    .locals 4

    new-instance v0, Lo/aq;

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lo/aq;-><init>(III)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lo/aq;
    .locals 4

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    new-instance v3, Lo/aq;

    invoke-direct {v3, v0, v1, v2}, Lo/aq;-><init>(III)V

    return-object v3
.end method

.method public static b(Lo/aR;I)Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/aq;->b(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lo/aR;ILo/aB;)Ljava/util/ArrayList;
    .locals 8

    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->b(IIILo/aB;)Lo/aq;

    move-result-object v0

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v1, v2, p2}, Lo/aq;->b(IIILo/aB;)Lo/aq;

    move-result-object v2

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v3

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v5

    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v6

    invoke-virtual {v0}, Lo/aq;->e()I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->f()I

    move-result v0

    invoke-static {v4, v0}, Lo/T;->f(II)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lo/aq;->e()I

    move-result v0

    invoke-virtual {v2}, Lo/aq;->f()I

    move-result v2

    invoke-static {v0, v2}, Lo/T;->f(II)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    shl-int v2, v0, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-le v1, v5, :cond_7

    move v4, v1

    :goto_0
    if-ge v4, v2, :cond_4

    move v1, v3

    :goto_1
    if-gez v1, :cond_2

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_2
    if-gt v1, v6, :cond_3

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    move v4, v1

    :goto_3
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_4
    if-gez v1, :cond_5

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    move v1, v2

    :goto_5
    if-gt v1, v6, :cond_6

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_7
    move v4, v1

    :goto_6
    if-gt v4, v5, :cond_0

    move v1, v3

    :goto_7
    if-gez v1, :cond_8

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_8
    move v1, v2

    :goto_8
    if-gt v1, v6, :cond_9

    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_6
.end method

.method public static b(III)Lo/aq;
    .locals 4

    const/high16 v3, 0x20000000

    if-ltz p0, :cond_0

    const/16 v0, 0x1e

    if-gt p0, v0, :cond_0

    const/high16 v0, -0x20000000

    if-le p2, v0, :cond_0

    if-le p2, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    add-int v0, p1, v3

    shr-int/2addr v0, v1

    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v1

    const/4 v1, 0x1

    shl-int/2addr v1, p0

    if-gez v0, :cond_3

    add-int/2addr v0, v1

    :cond_2
    :goto_1
    new-instance v1, Lo/aq;

    invoke-direct {v1, p0, v0, v2}, Lo/aq;-><init>(III)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-lt v0, v1, :cond_2

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method private static b(IIILo/aB;)Lo/aq;
    .locals 4

    const/high16 v3, 0x20000000

    const/16 v0, 0x1e

    const/4 v1, 0x0

    if-gtz p0, :cond_0

    new-instance v0, Lo/aq;

    invoke-direct {v0, v1, v1, v1}, Lo/aq;-><init>(III)V

    :goto_0
    return-object v0

    :cond_0
    if-le p0, v0, :cond_1

    move p0, v0

    :cond_1
    rsub-int/lit8 v0, p0, 0x1e

    add-int v1, p1, v3

    shr-int/2addr v1, v0

    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v0

    new-instance v0, Lo/aq;

    invoke-direct {v0, p0, v1, v2, p3}, Lo/aq;-><init>(IIILo/aB;)V

    goto :goto_0
.end method

.method public static b(ILo/T;)Lo/aq;
    .locals 3

    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lo/aq;)I
    .locals 2

    iget v0, p0, Lo/aq;->d:I

    iget v1, p1, Lo/aq;->d:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lo/aq;->e:I

    iget v1, p1, Lo/aq;->e:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lo/aq;->f:I

    iget v1, p1, Lo/aq;->f:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    iget-object v1, p1, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, v1}, Lo/aB;->a(Lo/aB;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lo/aq;->f:I

    iget v1, p1, Lo/aq;->f:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lo/aq;->e:I

    iget v1, p1, Lo/aq;->e:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lo/aq;->d:I

    iget v1, p1, Lo/aq;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a()Lo/aq;
    .locals 5

    iget-object v0, p0, Lo/aq;->h:Lo/aq;

    if-nez v0, :cond_0

    new-instance v0, Lo/aq;

    iget v1, p0, Lo/aq;->d:I

    iget v2, p0, Lo/aq;->e:I

    iget v3, p0, Lo/aq;->f:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lo/aq;-><init>(IIILo/aB;)V

    iput-object v0, p0, Lo/aq;->h:Lo/aq;

    :cond_0
    iget-object v0, p0, Lo/aq;->h:Lo/aq;

    return-object v0
.end method

.method public a(I)Lo/aq;
    .locals 3

    iget v0, p0, Lo/aq;->d:I

    sub-int/2addr v0, p1

    if-gtz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lo/aq;->e:I

    shr-int/2addr v1, v0

    iget v2, p0, Lo/aq;->f:I

    shr-int v0, v2, v0

    invoke-virtual {p0, p1, v1, v0}, Lo/aq;->a(III)Lo/aq;

    move-result-object p0

    goto :goto_0
.end method

.method public a(III)Lo/aq;
    .locals 2

    new-instance v0, Lo/aq;

    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-direct {v0, p1, p2, p3, v1}, Lo/aq;-><init>(IIILo/aB;)V

    return-object v0
.end method

.method public a(LA/c;)Lo/aq;
    .locals 1

    invoke-virtual {p0}, Lo/aq;->k()Lo/aB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/aB;->a(LA/c;)Lo/aB;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aB;)Lo/aq;
    .locals 4

    new-instance v0, Lo/aq;

    iget v1, p0, Lo/aq;->d:I

    iget v2, p0, Lo/aq;->e:I

    iget v3, p0, Lo/aq;->f:I

    invoke-direct {v0, v1, v2, v3, p1}, Lo/aq;-><init>(IIILo/aB;)V

    return-object v0
.end method

.method public a(Lo/av;)Lo/at;
    .locals 1

    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, p1}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v0

    return-object v0
.end method

.method public a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, p1, p2}, Lo/aB;->a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget v0, p0, Lo/aq;->d:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lo/aq;->e:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    iget v0, p0, Lo/aq;->f:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lo/aq;->d:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lo/aq;->e:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lo/aq;

    invoke-virtual {p0, p1}, Lo/aq;->a(Lo/aq;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lo/aq;->f:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lo/aq;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lo/aq;

    if-eqz v1, :cond_0

    check-cast p1, Lo/aq;

    iget v1, p0, Lo/aq;->e:I

    iget v2, p1, Lo/aq;->e:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lo/aq;->f:I

    iget v2, p1, Lo/aq;->f:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lo/aq;->d:I

    iget v2, p1, Lo/aq;->d:I

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    iget-object v1, p1, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, v1}, Lo/aB;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lo/aq;->b:I

    return v0
.end method

.method public g()Lo/T;
    .locals 3

    new-instance v0, Lo/T;

    iget v1, p0, Lo/aq;->a:I

    iget v2, p0, Lo/aq;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    return-object v0
.end method

.method public h()Lo/T;
    .locals 4

    const/high16 v0, 0x40000000

    iget v1, p0, Lo/aq;->d:I

    shr-int/2addr v0, v1

    new-instance v1, Lo/T;

    iget v2, p0, Lo/aq;->a:I

    add-int/2addr v2, v0

    iget v3, p0, Lo/aq;->b:I

    add-int/2addr v0, v3

    invoke-direct {v1, v2, v0}, Lo/T;-><init>(II)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lo/aq;->d:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/aq;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/aq;->f:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1}, Lo/aB;->b()Z

    move-result v1

    if-nez v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1}, Lo/aB;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public i()Lo/ad;
    .locals 6

    const/high16 v0, 0x40000000

    iget v1, p0, Lo/aq;->d:I

    shr-int/2addr v0, v1

    new-instance v1, Lo/ad;

    new-instance v2, Lo/T;

    iget v3, p0, Lo/aq;->a:I

    iget v4, p0, Lo/aq;->b:I

    invoke-direct {v2, v3, v4}, Lo/T;-><init>(II)V

    new-instance v3, Lo/T;

    iget v4, p0, Lo/aq;->a:I

    add-int/2addr v4, v0

    iget v5, p0, Lo/aq;->b:I

    add-int/2addr v0, v5

    invoke-direct {v3, v4, v0}, Lo/T;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v1
.end method

.method public j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->H:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public k()Lo/aB;
    .locals 1

    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lo/aq;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/aq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/aq;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
