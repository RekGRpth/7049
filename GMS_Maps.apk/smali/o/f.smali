.class public Lo/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/aF;

.field private final c:[B

.field private final d:Lo/aj;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/aF;[BLo/aj;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/f;->a:Lo/o;

    iput-object p2, p0, Lo/f;->b:Lo/aF;

    iput-object p3, p0, Lo/f;->c:[B

    iput-object p4, p0, Lo/f;->d:Lo/aj;

    iput p5, p0, Lo/f;->e:I

    iput-object p6, p0, Lo/f;->f:Ljava/lang/String;

    iput p7, p0, Lo/f;->g:I

    iput p8, p0, Lo/f;->h:I

    iput-object p9, p0, Lo/f;->i:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/f;
    .locals 10

    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v0

    invoke-static {p0, v0}, Lo/aF;->a(Ljava/io/DataInput;Lo/aq;)Lo/aF;

    move-result-object v2

    invoke-virtual {v2}, Lo/aF;->a()I

    move-result v0

    new-array v3, v0, [B

    invoke-interface {p0, v3}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v8

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {v0, v8}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v9, v4, [I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_2

    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0, v8}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_0

    :cond_2
    new-instance v0, Lo/f;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v9}, Lo/f;-><init>(Lo/o;Lo/aF;[BLo/aj;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .locals 1

    iget-object v0, p0, Lo/f;->a:Lo/o;

    return-object v0
.end method

.method public b()Lo/aF;
    .locals 1

    iget-object v0, p0, Lo/f;->b:Lo/aF;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lo/f;->c:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()[B
    .locals 1

    iget-object v0, p0, Lo/f;->c:[B

    return-object v0
.end method

.method public e()Lo/aj;
    .locals 1

    iget-object v0, p0, Lo/f;->d:Lo/aj;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lo/f;->e:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lo/f;->g:I

    return v0
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lo/f;->h:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 2

    iget v0, p0, Lo/f;->h:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lo/f;->i:[I

    return-object v0
.end method

.method public m()I
    .locals 3

    iget-object v0, p0, Lo/f;->b:Lo/aF;

    invoke-virtual {v0}, Lo/aF;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x38

    iget-object v1, p0, Lo/f;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/f;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    iget-object v2, p0, Lo/f;->f:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/f;->d:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method
