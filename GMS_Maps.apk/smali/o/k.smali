.class public Lo/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/j;


# static fields
.field static final a:Lo/ad;


# instance fields
.field private b:Ljava/util/List;

.field private c:Lo/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lo/k;->b()Lo/ad;

    move-result-object v0

    sput-object v0, Lo/k;->a:Lo/ad;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lo/k;->b:Ljava/util/List;

    sget-object v0, Lo/k;->a:Lo/ad;

    iput-object v0, p0, Lo/k;->c:Lo/ad;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lo/k;->b:Ljava/util/List;

    sget-object v0, Lo/k;->a:Lo/ad;

    iput-object v0, p0, Lo/k;->c:Lo/ad;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lo/k;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    invoke-virtual {p0, v0}, Lo/k;->a(Lo/j;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public varargs constructor <init>([Lo/j;)V
    .locals 1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lo/k;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private static b()Lo/ad;
    .locals 2

    const/high16 v1, -0x80000000

    new-instance v0, Lo/T;

    invoke-direct {v0, v1, v1}, Lo/T;-><init>(II)V

    new-instance v1, Lo/ad;

    invoke-direct {v1, v0, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v1
.end method


# virtual methods
.method public a()Lo/ad;
    .locals 1

    iget-object v0, p0, Lo/k;->c:Lo/ad;

    return-object v0
.end method

.method public a(Lo/j;)V
    .locals 3

    invoke-interface {p1}, Lo/j;->a()Lo/ad;

    move-result-object v0

    iget-object v1, p0, Lo/k;->c:Lo/ad;

    sget-object v2, Lo/k;->a:Lo/ad;

    if-ne v1, v2, :cond_0

    new-instance v1, Lo/ad;

    iget-object v2, v0, Lo/ad;->a:Lo/T;

    invoke-static {v2}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v2

    iget-object v0, v0, Lo/ad;->b:Lo/T;

    invoke-static {v0}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v1, p0, Lo/k;->c:Lo/ad;

    :goto_0
    iget-object v0, p0, Lo/k;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v1, p0, Lo/k;->c:Lo/ad;

    invoke-virtual {v1, v0}, Lo/ad;->b(Lo/ad;)V

    goto :goto_0
.end method

.method public a(Lo/T;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lo/k;->c:Lo/ad;

    invoke-virtual {v0, p1}, Lo/ad;->a(Lo/T;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lo/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lo/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    invoke-interface {v0, p1}, Lo/j;->a(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Lo/ae;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v0

    iget-object v1, p0, Lo/k;->c:Lo/ad;

    invoke-virtual {v1, v0}, Lo/ad;->a(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lo/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lo/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    invoke-interface {v0, p1}, Lo/j;->a(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
