.class public Lo/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/r;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:Lo/T;

.field private final e:J

.field private f:Z


# direct methods
.method protected constructor <init>(Lo/r;Ljava/util/List;IZLo/T;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo/y;->a:Lo/r;

    iput-object p2, p0, Lo/y;->b:Ljava/util/List;

    iput p3, p0, Lo/y;->c:I

    iput-boolean p4, p0, Lo/y;->f:Z

    iput-object p5, p0, Lo/y;->d:Lo/T;

    iput-wide p6, p0, Lo/y;->e:J

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lo/y;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    invoke-static {v6}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {p0, v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lo/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/z;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v4

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-ltz v3, :cond_3

    if-lt v3, v6, :cond_4

    :cond_3
    move v3, v0

    :cond_4
    if-nez v4, :cond_5

    if-nez v6, :cond_6

    :cond_5
    const/4 v3, -0x1

    :cond_6
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/T;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v5

    :cond_7
    new-instance v0, Lo/y;

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lo/y;-><init>(Lo/r;Ljava/util/List;IZLo/T;J)V

    move-object v5, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/z;)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    iget-object v1, p0, Lo/y;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public a()Lo/r;
    .locals 1

    iget-object v0, p0, Lo/y;->a:Lo/r;

    return-object v0
.end method

.method public a(I)Lo/z;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lo/y;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lo/y;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    goto :goto_0
.end method

.method public a(Lo/D;)Lo/z;
    .locals 1

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/r;)Lo/z;
    .locals 3

    iget-object v0, p0, Lo/y;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v2

    invoke-virtual {p1, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, Lo/y;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lo/y;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lo/D;)I
    .locals 1

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/y;->a(Lo/z;)I

    move-result v0

    return v0
.end method

.method public b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lo/y;->b:Ljava/util/List;

    return-object v0
.end method

.method public c()Lo/z;
    .locals 1

    iget v0, p0, Lo/y;->c:I

    invoke-virtual {p0, v0}, Lo/y;->a(I)Lo/z;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lo/y;->f:Z

    return v0
.end method

.method public e()Lo/T;
    .locals 1

    iget-object v0, p0, Lo/y;->d:Lo/T;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Building: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/y;->a:Lo/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
