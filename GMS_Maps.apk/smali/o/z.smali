.class public Lo/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lo/D;

.field private final f:Lo/aR;


# direct methods
.method private constructor <init>(Lo/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILo/aR;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lo/z;->a:Ljava/util/List;

    iput-object p3, p0, Lo/z;->b:Ljava/lang/String;

    iput-object p4, p0, Lo/z;->c:Ljava/lang/String;

    iput p5, p0, Lo/z;->d:I

    new-instance v0, Lo/D;

    invoke-direct {v0, p1, p6}, Lo/D;-><init>(Lo/r;I)V

    iput-object v0, p0, Lo/z;->e:Lo/D;

    iput-object p7, p0, Lo/z;->f:Lo/aR;

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/z;
    .locals 11

    const/4 v10, 0x7

    const/4 v6, 0x5

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x2

    invoke-virtual {p0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v0, v5

    :goto_1
    if-ge v0, v3, :cond_2

    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v3, :cond_3

    if-eqz v4, :cond_9

    move-object v0, v4

    :goto_2
    move-object v3, v0

    :cond_3
    if-nez v4, :cond_4

    move-object v4, v3

    :cond_4
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    :cond_5
    const/high16 v6, -0x80000000

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    :cond_6
    const/4 v7, 0x0

    invoke-virtual {p0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lo/T;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v7

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/T;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v0

    invoke-virtual {v7}, Lo/T;->f()I

    move-result v8

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v9

    if-le v8, v9, :cond_7

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v8

    const/high16 v9, 0x40000000

    add-int/2addr v8, v9

    invoke-virtual {v0, v8}, Lo/T;->a(I)V

    :cond_7
    new-instance v8, Lo/ad;

    invoke-direct {v8, v7, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-static {v8}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v7

    :cond_8
    new-instance v0, Lo/z;

    invoke-direct/range {v0 .. v7}, Lo/z;-><init>(Lo/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILo/aR;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, ""

    goto :goto_2
.end method


# virtual methods
.method public a()Lo/D;
    .locals 1

    iget-object v0, p0, Lo/z;->e:Lo/D;

    return-object v0
.end method

.method public b()Lo/r;
    .locals 1

    iget-object v0, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lo/z;->a:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lo/z;->d:I

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0}, Lo/D;->b()I

    move-result v0

    return v0
.end method

.method public g()Lo/aR;
    .locals 1

    iget-object v0, p0, Lo/z;->f:Lo/aR;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
