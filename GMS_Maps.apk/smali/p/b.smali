.class public Lp/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# instance fields
.field private b:Lr/z;

.field private c:Lt/t;

.field private d:Lo/ad;

.field private e:Lo/aq;

.field private f:Lo/aq;


# direct methods
.method public constructor <init>(Lr/z;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp/b;->b:Lr/z;

    new-instance v0, Lt/t;

    const/16 v1, 0x24

    invoke-direct {v0, v1}, Lt/t;-><init>(I)V

    iput-object v0, p0, Lp/b;->c:Lt/t;

    sput-boolean p2, Lp/b;->a:Z

    return-void
.end method

.method private a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 6

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v3, p0, Lp/b;->c:Lt/t;

    invoke-virtual {v3, v0}, Lt/t;->c(Lo/aq;)Lo/ap;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lp/b;->b:Lr/z;

    const/4 v5, 0x1

    invoke-interface {v3, v0, v5}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v5, p0, Lp/b;->c:Lt/t;

    invoke-virtual {v5, v0, v3}, Lt/t;->a(Lo/aq;Lo/ap;)V

    :cond_0
    move-object v0, v3

    if-eqz v0, :cond_1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    if-nez p2, :cond_5

    new-instance v1, Ls/a;

    invoke-direct {v1, v2}, Ls/a;-><init>(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lp/b;->b:Lr/z;

    invoke-interface {v3, v0, v1}, Lr/z;->a(Lo/aq;Ls/e;)V

    goto :goto_1

    :cond_4
    const-wide/16 v2, 0x4e20

    :try_start_0
    invoke-virtual {v1, v2, v3}, Ls/a;->a(J)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {v1}, Ls/a;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    iget-object v2, p0, Lp/b;->c:Lt/t;

    invoke-interface {v0}, Lo/ap;->d()Lo/aq;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lt/t;->a(Lo/aq;Lo/ap;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v0, "RoadGraph"

    const-string v2, "Interupted while waiting for tiles"

    invoke-static {v0, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    return-object v4
.end method


# virtual methods
.method public a(Lo/T;D)Ljava/util/Iterator;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    double-to-int v0, p2

    invoke-static {p1, v0}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    iget-object v2, p0, Lp/b;->d:Lo/ad;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lp/b;->d:Lo/ad;

    invoke-virtual {v0, v2}, Lo/ad;->c(Lo/ad;)Lo/ad;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v2, 0xe

    invoke-static {v0, v2}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v0

    sget-boolean v2, Lp/b;->a:Z

    invoke-direct {p0, v0, v2}, Lp/b;->a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    check-cast v0, Lp/d;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lp/c;

    invoke-direct {v0, v1}, Lp/c;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Lp/e;)Lp/e;
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x2

    const/4 v2, -0x2

    invoke-virtual {p1}, Lp/e;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p1, v0}, Lp/e;->b(I)Lo/T;

    move-result-object v4

    invoke-virtual {p1}, Lp/e;->e()Lo/T;

    move-result-object v5

    invoke-virtual {v5}, Lo/T;->f()I

    move-result v0

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    if-le v0, v6, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v5}, Lo/T;->g()I

    move-result v6

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v4

    if-le v6, v4, :cond_3

    :goto_1
    new-instance v2, Lo/T;

    invoke-direct {v2, v0, v1}, Lo/T;-><init>(II)V

    invoke-virtual {v5, v2}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v5, v0}, Lo/ad;->a(Lo/T;Lo/T;)Lo/ad;

    move-result-object v0

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lp/b;->a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    invoke-interface {v0}, Lo/ap;->d()Lo/aq;

    move-result-object v4

    iget-object v5, p0, Lp/b;->f:Lo/aq;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lp/b;->e:Lo/aq;

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v5

    iget-object v6, p0, Lp/b;->f:Lo/aq;

    invoke-virtual {v6}, Lo/aq;->c()I

    move-result v6

    if-gt v5, v6, :cond_1

    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v5

    iget-object v6, p0, Lp/b;->e:Lo/aq;

    invoke-virtual {v6}, Lo/aq;->c()I

    move-result v6

    if-lt v5, v6, :cond_1

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v5

    iget-object v6, p0, Lp/b;->f:Lo/aq;

    invoke-virtual {v6}, Lo/aq;->d()I

    move-result v6

    if-gt v5, v6, :cond_1

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    iget-object v5, p0, Lp/b;->e:Lo/aq;

    invoke-virtual {v5}, Lo/aq;->d()I

    move-result v5

    if-ge v4, v5, :cond_4

    :cond_1
    move-object v0, v3

    :goto_2
    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    check-cast v0, Lp/d;

    invoke-virtual {v0, p1}, Lp/d;->a(Lp/e;)Lp/e;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v3

    goto :goto_2
.end method

.method public a(Lo/ad;)V
    .locals 4

    const/16 v3, 0xe

    const/4 v2, 0x0

    iput-object p1, p0, Lp/b;->d:Lo/ad;

    iget-object v0, p0, Lp/b;->d:Lo/ad;

    if-nez v0, :cond_0

    iput-object v2, p0, Lp/b;->f:Lo/aq;

    iput-object v2, p0, Lp/b;->e:Lo/aq;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    invoke-static {v3, v0, v1, v2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lp/b;->e:Lo/aq;

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v0, v1, v2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lp/b;->f:Lo/aq;

    goto :goto_0
.end method
