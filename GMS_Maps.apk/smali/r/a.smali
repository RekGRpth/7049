.class public abstract Lr/a;
.super Lr/d;
.source "SourceFile"


# instance fields
.field protected final a:LA/c;

.field private volatile i:Z

.field private final j:I

.field private final k:Ljava/util/List;

.field private final l:I

.field private final m:I

.field private final n:F

.field private o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method protected constructor <init>(Law/p;Ljava/lang/String;LA/c;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lt/g;)V
    .locals 12

    invoke-virtual {p3}, LA/c;->d()Lt/M;

    move-result-object v5

    move/from16 v0, p11

    move-object/from16 v1, p13

    invoke-static {p2, p3, v0, v1}, Lr/a;->a(Ljava/lang/String;LA/c;ZLt/g;)Lt/f;

    move-result-object v6

    invoke-static {p3}, Lr/a;->a(LA/c;)I

    move-result v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v8, p9

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p12

    invoke-direct/range {v2 .. v11}, Lr/d;-><init>(Law/p;Ljava/lang/String;Lt/M;Lt/f;IZILjava/util/Locale;Ljava/io/File;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lr/a;->i:Z

    iput-object p3, p0, Lr/a;->a:LA/c;

    move/from16 v0, p4

    iput v0, p0, Lr/a;->j:I

    move-object/from16 v0, p5

    iput-object v0, p0, Lr/a;->k:Ljava/util/List;

    move/from16 v0, p6

    iput v0, p0, Lr/a;->m:I

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lr/a;->l:I

    :goto_0
    move/from16 v0, p8

    iput v0, p0, Lr/a;->n:F

    return-void

    :cond_1
    invoke-static/range {p4 .. p4}, Lr/a;->a(I)I

    move-result v2

    iput v2, p0, Lr/a;->l:I

    goto :goto_0
.end method

.method static a(I)I
    .locals 3

    const/16 v2, 0x80

    const/4 v0, 0x0

    move v1, p0

    :goto_0
    if-le v1, v2, :cond_0

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v1, v2, :cond_1

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    return v0
.end method

.method private static a(LA/c;)I
    .locals 1

    sget-object v0, LA/c;->d:LA/c;

    if-ne p0, v0, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xbb8

    goto :goto_0
.end method

.method static synthetic a(Lr/a;)I
    .locals 1

    iget v0, p0, Lr/a;->j:I

    return v0
.end method

.method private static a(Ljava/lang/String;LA/c;ZLt/g;)Lt/f;
    .locals 1

    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0, p2, p3}, LA/c;->a(Ljava/lang/String;ZLt/g;)Lt/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lr/a;)I
    .locals 1

    iget v0, p0, Lr/a;->m:I

    return v0
.end method

.method static synthetic c(Lr/a;)F
    .locals 1

    iget v0, p0, Lr/a;->n:F

    return v0
.end method

.method static synthetic d(Lr/a;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lr/a;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lr/a;)Z
    .locals 1

    iget-boolean v0, p0, Lr/a;->i:Z

    return v0
.end method

.method static synthetic f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic g(Lr/a;)I
    .locals 1

    iget v0, p0, Lr/a;->l:I

    return v0
.end method


# virtual methods
.method public a()LA/c;
    .locals 1

    iget-object v0, p0, Lr/a;->a:LA/c;

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, p1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "jrg"

    const-string v1, "setting spolightDescription"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lr/a;->b()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lr/a;->i:Z

    return-void
.end method
