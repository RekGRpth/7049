.class public abstract Lr/d;
.super LR/c;
.source "SourceFile"

# interfaces
.implements Law/q;
.implements Lr/t;
.implements Lr/z;


# instance fields
.field private a:Ljava/util/Locale;

.field protected b:Lr/B;

.field protected c:Lr/h;

.field volatile d:I

.field protected e:Lcom/google/googlenav/common/a;

.field volatile f:I

.field volatile g:I

.field private volatile i:Lr/i;

.field private final j:Ljava/util/concurrent/locks/ReentrantLock;

.field private final k:Law/p;

.field private l:Landroid/os/Handler;

.field private m:Landroid/os/Looper;

.field private n:Z

.field private final o:Ljava/util/List;

.field private final p:LR/h;

.field private final q:Ljava/util/Map;

.field private final r:I

.field private s:Z

.field private t:Lcom/google/googlenav/bE;

.field private final u:Ljava/util/ArrayList;

.field private volatile v:Z

.field private w:Ls/e;


# direct methods
.method protected constructor <init>(Law/p;Ljava/lang/String;Lt/M;Lt/f;IZILjava/util/Locale;Ljava/io/File;)V
    .locals 7

    invoke-direct {p0, p2}, LR/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lr/d;->o:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->s:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->v:Z

    new-instance v0, Lr/e;

    invoke-direct {v0, p0}, Lr/e;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->w:Ls/e;

    new-instance v0, Lr/B;

    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v2, p3

    move-object v3, p4

    move v4, p6

    move-object v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Lr/B;-><init>(Ljava/lang/String;Lt/M;Lt/f;ZLjava/util/Locale;Ljava/io/File;)V

    iput-object v0, p0, Lr/d;->b:Lr/B;

    iput p5, p0, Lr/d;->r:I

    iput-object p8, p0, Lr/d;->a:Ljava/util/Locale;

    iput-object p1, p0, Lr/d;->k:Law/p;

    invoke-virtual {p0}, Lr/d;->m()Lr/h;

    move-result-object v0

    iput-object v0, p0, Lr/d;->c:Lr/h;

    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-static {v0, p0}, Lr/h;->a(Lr/h;Lr/d;)Lr/d;

    new-instance v0, Lr/f;

    invoke-direct {v0, p0, p7}, Lr/f;-><init>(Lr/d;I)V

    iput-object v0, p0, Lr/d;->p:LR/h;

    return-void
.end method

.method private a(Lr/k;Z)Landroid/util/Pair;
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v1

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v2

    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    invoke-interface {v1, v2}, Lt/M;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v3, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v3}, Lo/ap;->a(Lcom/google/googlenav/common/a;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    invoke-interface {v2, v1}, Lt/M;->a(Lo/ap;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v2}, Lcom/google/googlenav/bE;->a()V

    :cond_1
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lr/d;->a(Lo/ap;Z)Lr/k;

    move-result-object v2

    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {p0, p1, v5, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    move-object v0, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_9

    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1, v2}, Lt/f;->b(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p0, p1, v5, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-interface {v1, v2}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v3

    if-eqz v3, :cond_9

    iget-object v4, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v3, v4}, Lo/ap;->a(Lcom/google/googlenav/common/a;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-interface {v1, v3}, Lt/f;->a(Lo/ap;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v1}, Lcom/google/googlenav/bE;->c()V

    :cond_5
    invoke-direct {p0, p1, v2}, Lr/d;->a(Lr/k;Lo/aq;)V

    :goto_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v0}, Lcom/google/googlenav/bE;->b()V

    :cond_7
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, v2, v3}, Lt/M;->a(Lo/aq;Lo/ap;)V

    :cond_8
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v0

    invoke-direct {p0, v3, v0}, Lr/d;->a(Lo/ap;Z)Lr/k;

    move-result-object v0

    invoke-virtual {p0, p1, v5, v3}, Lr/d;->a(Lr/k;ILo/ap;)V

    goto :goto_3

    :cond_9
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v1}, Lcom/google/googlenav/bE;->c()V

    :cond_a
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lr/k;->a(I)V

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private a(Lo/ap;Z)Lr/k;
    .locals 8

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lr/d;->c()I

    move-result v1

    const/4 v0, 0x0

    if-eq v1, v6, :cond_1

    invoke-interface {p1}, Lo/ap;->e()I

    move-result v2

    if-eq v1, v2, :cond_1

    move v1, v4

    :goto_0
    if-eqz v1, :cond_0

    new-instance v0, Lr/k;

    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    iget-object v2, p0, Lr/d;->w:Ls/e;

    sget-object v3, Lr/c;->b:Lr/c;

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    invoke-direct {p0, v6, v1}, Lr/d;->a(II)V

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez p2, :cond_2

    iget-object v1, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {p1, v1}, Lo/ap;->b(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lo/ap;->h()I

    move-result v6

    move v1, v4

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_0
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lr/d;->n()V

    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0}, Law/p;->w()J

    move-result-wide v3

    const-wide/16 v5, 0x64

    rem-long/2addr v3, v5

    const-wide/16 v5, 0x8

    cmp-long v0, v3, v5

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lo/aL;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "z="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x6d

    const-string v6, "u"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    aput-object v0, v7, v2

    aput-object v3, v7, v1

    const/4 v0, 0x2

    aput-object v4, v7, v0

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private a(IIIIII)V
    .locals 12

    iget-object v1, p0, Lr/d;->k:Law/p;

    invoke-interface {v1}, Law/p;->w()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x8

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "f="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "n="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "d="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lo/aL;->t()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x6d

    const-string v9, "b"

    const/4 v10, 0x7

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    const/4 v1, 0x1

    aput-object v2, v10, v1

    const/4 v1, 0x2

    aput-object v3, v10, v1

    const/4 v1, 0x3

    aput-object v4, v10, v1

    const/4 v1, 0x4

    aput-object v5, v10, v1

    const/4 v1, 0x5

    aput-object v6, v10, v1

    const/4 v1, 0x6

    aput-object v7, v10, v1

    invoke-static {v10}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v9, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lr/d;)V
    .locals 0

    invoke-direct {p0}, Lr/d;->o()V

    return-void
.end method

.method static synthetic a(Lr/d;Lo/aq;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/d;->b(Lo/aq;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lo/ar;Lr/c;Ls/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lr/d;->b(Lo/ar;Lr/c;Ls/e;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lr/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/d;->a(Lr/h;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lr/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/d;->b(Lr/k;)V

    return-void
.end method

.method private a(Lr/h;)V
    .locals 16

    invoke-direct/range {p0 .. p0}, Lr/d;->s()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lr/d;->s:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lr/d;->s:Z

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->p:LR/h;

    invoke-virtual {v1}, LR/h;->f()I

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->p:LR/h;

    invoke-virtual {v1}, LR/h;->h()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr/k;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lr/d;->b(Lr/k;)V

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lr/h;->a()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lr/d;->c()I

    move-result v2

    if-eq v1, v2, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lr/d;->a(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->o:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    :goto_1
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lr/h;->c()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lr/h;->c()I

    move-result v8

    if-ge v1, v8, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v10

    invoke-virtual {v10}, Lr/k;->c()Lo/aq;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lr/d;->a()LA/c;

    move-result-object v11

    invoke-virtual {v8, v11}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v11

    invoke-virtual {v10}, Lr/k;->g()I

    move-result v8

    const/4 v12, -0x1

    if-eq v8, v12, :cond_4

    add-int/lit8 v7, v7, 0x1

    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v8, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->d:I

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->d:I

    :try_start_0
    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v8

    if-eqz v8, :cond_8

    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->g:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->g:I

    :goto_3
    const/4 v8, 0x0

    if-eqz v9, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->c(I)[B

    move-result-object v12

    if-eqz v12, :cond_5

    array-length v8, v12

    new-array v8, v8, [B

    const/4 v13, 0x0

    const/4 v14, 0x0

    array-length v15, v12

    invoke-static {v12, v13, v8, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->b(I)Lo/ap;

    move-result-object v12

    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lr/d;->b:Lr/B;

    iget-object v13, v13, Lr/B;->a:Lt/M;

    if-eqz v13, :cond_6

    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v13

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lr/d;->b:Lr/B;

    iget-object v13, v13, Lr/B;->a:Lt/M;

    invoke-interface {v13, v11, v12}, Lt/M;->a(Lo/aq;Lo/ap;)V

    :cond_6
    if-eqz v9, :cond_7

    invoke-interface {v9, v11, v12, v8}, Lt/f;->a(Lo/aq;Lo/ap;[B)V

    :cond_7
    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8, v12}, Lr/d;->a(Lr/k;ILo/ap;)V

    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v8

    if-eqz v8, :cond_9

    add-int/lit8 v4, v4, 0x1

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->f:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->f:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v8

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": Could not parse tile: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v8}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v8, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8, v11}, Lr/d;->a(Lr/k;ILo/ap;)V

    goto :goto_4

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_a
    :try_start_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/K;->u()Z

    move-result v8

    if-eqz v8, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->b(Lr/k;Lo/aq;)Z

    move-result v8

    if-eqz v8, :cond_b

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->a(Lr/k;Lo/aq;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->a(Lr/k;Lo/aq;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_d
    invoke-direct/range {p0 .. p0}, Lr/d;->t()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct/range {p0 .. p0}, Lr/d;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lr/d;->a(IIIIII)V

    goto/16 :goto_1
.end method

.method private a(Lr/k;)V
    .locals 2

    iget-object v0, p0, Lr/d;->i:Lr/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/d;->i:Lr/i;

    invoke-virtual {v0}, Lr/i;->a()V

    :cond_0
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private a(Lr/k;Lo/aq;)V
    .locals 2

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, p2}, Lt/M;->a_(Lo/aq;)V

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lr/d;->a(Lr/k;ILo/ap;)V

    return-void
.end method

.method static synthetic a(Lr/d;Z)Z
    .locals 0

    iput-boolean p1, p0, Lr/d;->v:Z

    return p1
.end method

.method private b(Lo/aq;)V
    .locals 4

    iget-object v2, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/A;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1}, Lr/A;->a(Lr/z;Lo/aq;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lr/d;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private b(Lo/ar;Lr/c;Ls/e;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lr/u;->a(Lr/c;Z)I

    move-result v0

    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    :goto_0
    invoke-interface {p1}, Lo/ar;->c()Lo/aq;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_0

    invoke-interface {v1, v2, p3, v0}, Lt/f;->a(Lo/aq;Ls/e;I)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    invoke-interface {p3, v2, v4, v3}, Ls/e;->a(Lo/aq;ILo/ap;)V

    goto :goto_0

    :cond_1
    iput-boolean v4, p0, Lr/d;->v:Z

    invoke-direct {p0}, Lr/d;->t()V

    return-void
.end method

.method static synthetic b(Lr/d;)V
    .locals 0

    invoke-direct {p0}, Lr/d;->q()V

    return-void
.end method

.method private b(Lr/k;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lr/d;->s()V

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v0

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v3

    sget-object v0, Lr/z;->h:Lo/aq;

    invoke-virtual {v0, v3}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v6, v7}, Lr/d;->a(Lr/k;ILo/ap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/k;

    invoke-virtual {p1}, Lr/k;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lr/k;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_2
    invoke-direct {p0, p1, v5}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lr/k;

    move v8, v2

    move-object v2, v1

    move v1, v8

    :goto_1
    if-nez v1, :cond_3

    invoke-virtual {p0, p1, v6, v7}, Lr/d;->a(Lr/k;ILo/ap;)V

    :cond_3
    if-eqz v2, :cond_0

    invoke-static {v2}, Lr/d;->c(Lr/k;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_9

    invoke-virtual {v2}, Lr/k;->j()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v2}, Lr/k;->a(Lr/k;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lr/k;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object v2, p1

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lr/k;->k()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lr/k;->j()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    move-object v2, p1

    goto :goto_1

    :cond_7
    invoke-direct {p0, p1, v5}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lr/k;

    move v8, v2

    move-object v2, v1

    move v1, v8

    goto :goto_1

    :cond_8
    move v1, v2

    move-object v2, p1

    goto :goto_1

    :cond_9
    iget-boolean v0, p0, Lr/d;->s:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v2}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lr/d;->p:LR/h;

    invoke-virtual {v0, v3}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/k;

    if-eqz v0, :cond_a

    invoke-virtual {v0, v2}, Lr/k;->a(Lr/k;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lr/d;->p:LR/h;

    invoke-virtual {v0, v3, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0, v2}, Lr/h;->a(Lr/k;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0}, Lr/d;->p()V

    :cond_c
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v1

    invoke-static {v1, v3}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lr/h;->a(Landroid/util/Pair;Lr/k;)V

    iget v0, p0, Lr/d;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lr/d;->d:I

    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0}, Lr/h;->d()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v2}, Lr/k;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    invoke-direct {p0}, Lr/d;->p()V

    goto/16 :goto_0

    :cond_e
    iget-boolean v0, p0, Lr/d;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iput-boolean v5, p0, Lr/d;->n:Z

    goto/16 :goto_0
.end method

.method private b(Lr/k;Lo/aq;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lr/d;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lo/ap;->h()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v2}, Lo/ap;->c(Lcom/google/googlenav/common/a;)V

    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    invoke-interface {v2, p2, v1}, Lt/M;->a(Lo/aq;Lo/ap;)V

    :cond_0
    iget-object v2, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v2}, Lr/B;->b()Lt/f;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2, p2}, Lt/f;->a(Lo/aq;)[B

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2, p2, v1, v3}, Lt/f;->a(Lo/aq;Lo/ap;[B)V

    :cond_1
    invoke-virtual {p0, p1, v0, v1}, Lr/d;->a(Lr/k;ILo/ap;)V

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private c(Lo/aq;)Lo/ap;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    invoke-interface {v1, p1}, Lt/M;->b(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, p1}, Lt/M;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lt/f;->b(Lo/aq;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1, p1}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lr/d;)V
    .locals 0

    invoke-direct {p0}, Lr/d;->r()V

    return-void
.end method

.method private static c(Lr/k;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic d(Lr/d;)I
    .locals 1

    iget v0, p0, Lr/d;->r:I

    return v0
.end method

.method static synthetic e(Lr/d;)Z
    .locals 1

    iget-boolean v0, p0, Lr/d;->v:Z

    return v0
.end method

.method static synthetic f(Lr/d;)V
    .locals 0

    invoke-direct {p0}, Lr/d;->t()V

    return-void
.end method

.method private n()V
    .locals 4

    iget-object v2, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/A;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lr/A;->a(Lr/z;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lr/d;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private o()V
    .locals 1

    invoke-direct {p0}, Lr/d;->s()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->n:Z

    invoke-direct {p0}, Lr/d;->p()V

    return-void
.end method

.method private p()V
    .locals 3

    invoke-direct {p0}, Lr/d;->s()V

    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0}, Lr/h;->c()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ll/g;

    const-string v1, "addRequest"

    iget-object v2, p0, Lr/d;->c:Lr/h;

    invoke-direct {v0, v1, v2}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget-object v0, p0, Lr/d;->k:Law/p;

    iget-object v1, p0, Lr/d;->c:Lr/h;

    invoke-interface {v0, v1}, Law/p;->c(Law/g;)V

    iget-object v0, p0, Lr/d;->o:Ljava/util/List;

    iget-object v1, p0, Lr/d;->c:Lr/h;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lr/d;->m()Lr/h;

    move-result-object v0

    iput-object v0, p0, Lr/d;->c:Lr/h;

    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-static {v0, p0}, Lr/h;->a(Lr/h;Lr/d;)Lr/d;

    :cond_0
    return-void
.end method

.method private q()V
    .locals 1

    invoke-direct {p0}, Lr/d;->s()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/d;->s:Z

    return-void
.end method

.method private r()V
    .locals 5

    invoke-direct {p0}, Lr/d;->s()V

    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lr/d;->o:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lr/d;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/h;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lr/h;->c()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v3

    iget-object v4, p0, Lr/d;->q:Ljava/util/Map;

    invoke-virtual {v3}, Lr/k;->c()Lo/aq;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lr/d;->d:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lr/d;->d:I

    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v3

    invoke-direct {p0, v3}, Lr/d;->b(Lr/k;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final s()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on DashServerTileStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private t()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->b:Lt/f;

    invoke-interface {v0}, Lt/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lr/d;->i:Lr/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/d;->i:Lr/i;

    invoke-virtual {v0}, Lr/i;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lr/i;

    invoke-direct {v0, p0}, Lr/i;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->i:Lr/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private u()Z
    .locals 2

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->b:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->o:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/aq;Z)Lo/ap;
    .locals 5

    new-instance v1, Lr/j;

    invoke-direct {v1}, Lr/j;-><init>()V

    new-instance v0, Lr/k;

    invoke-direct {v0, p1, v1}, Lr/k;-><init>(Lo/aq;Ls/e;)V

    invoke-direct {p0, v0, p2}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lr/k;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v3, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    invoke-static {v1}, Lr/j;->a(Lr/j;)Lo/ap;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;
    .locals 8

    const/4 v5, 0x0

    new-instance v0, Lr/k;

    const/4 v6, -0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-object v0
.end method

.method public a(IZLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Network Error! "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public a(Law/g;)V
    .locals 3

    instance-of v0, p1, Lr/h;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lr/h;

    invoke-static {v0}, Lr/h;->a(Lr/h;)Lr/d;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/bE;)V
    .locals 0

    iput-object p1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public a(Ljava/util/Locale;)V
    .locals 4

    iget-object v0, p0, Lr/d;->a:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(Ljava/util/Locale;)V

    iput-object p1, p0, Lr/d;->a:Ljava/util/Locale;

    invoke-direct {p0}, Lr/d;->n()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1
.end method

.method public a(Lo/aq;Ls/e;)V
    .locals 1

    new-instance v0, Lr/k;

    invoke-direct {v0, p1, p2}, Lr/k;-><init>(Lo/aq;Ls/e;)V

    invoke-direct {p0, v0}, Lr/d;->a(Lr/k;)V

    return-void
.end method

.method public a(Lo/ar;Lr/c;Ls/e;)V
    .locals 3

    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lr/A;)V
    .locals 3

    iget-object v1, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lr/k;ILo/ap;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    move-object v2, p1

    :goto_0
    if-eqz v2, :cond_2

    if-nez p2, :cond_1

    invoke-virtual {v2}, Lr/k;->b()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v3

    invoke-static {v3}, Lr/u;->a(Lr/c;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p3}, Lo/ap;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v3

    iget-object v4, v2, Lr/k;->b:Ls/e;

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v5

    invoke-static {v5, v1}, Lr/u;->a(Lr/c;Z)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lt/f;->a(Lo/aq;Ls/e;I)V

    move v0, v1

    :goto_1
    invoke-static {v2}, Lr/k;->b(Lr/k;)Lr/k;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v3, 0x4

    invoke-static {v2, v3, p3}, Lr/k;->a(Lr/k;ILo/ap;)V

    goto :goto_1

    :cond_1
    invoke-static {v2, p2, p3}, Lr/k;->a(Lr/k;ILo/ap;)V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lr/d;->v:Z

    invoke-direct {p0}, Lr/d;->t()V

    :cond_3
    return-void
.end method

.method public a(Lo/aq;)Z
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(Lo/aq;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->c()V

    invoke-direct {p0}, Lr/d;->n()V

    return-void
.end method

.method public b(Law/g;)V
    .locals 0

    return-void
.end method

.method public b(Lo/aq;Ls/e;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    new-instance v2, Lr/k;

    invoke-direct {v2, p1, p2, v0, v1}, Lr/k;-><init>(Lo/aq;Ls/e;ZZ)V

    invoke-direct {p0, v2}, Lr/d;->a(Lr/k;)V

    return-void
.end method

.method public b(Lr/A;)V
    .locals 3

    iget-object v1, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->d()I

    move-result v0

    return v0
.end method

.method public c(Lo/aq;Ls/e;)V
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Lr/k;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v0, v2}, Lr/k;-><init>(Lo/aq;Ls/e;ZZ)V

    invoke-direct {p0, v1}, Lr/d;->a(Lr/k;)V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->g()Z

    move-result v0

    return v0
.end method

.method public e()J
    .locals 2

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0, p0}, Law/p;->a(Law/q;)V

    invoke-virtual {p0}, Lr/d;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lr/d;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->h()V

    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0, p0}, Law/p;->b(Law/q;)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->i()V

    return-void
.end method

.method public j()J
    .locals 4

    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0}, Law/p;->p()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lr/d;->k:Law/p;

    invoke-interface {v2}, Law/p;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lr/d;->d:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    sub-long v0, v2, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 0

    return-void
.end method

.method public l()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    new-instance v0, Lr/g;

    invoke-direct {v0, p0}, Lr/g;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->a()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method protected abstract m()Lr/h;
.end method
