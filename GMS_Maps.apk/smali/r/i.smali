.class Lr/i;
.super LR/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lr/d;

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method constructor <init>(Lr/d;)V
    .locals 2

    iput-object p1, p0, Lr/i;->a:Lr/d;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheCommitter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lr/d;->d(Lr/d;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->c:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lr/i;->start()V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->b:Z

    return-void
.end method

.method b()Z
    .locals 1

    iget-boolean v0, p0, Lr/i;->c:Z

    return v0
.end method

.method public l()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lr/i;->a:Lr/d;

    iget-object v0, v0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->b()Lt/f;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lr/i;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lr/i;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->e(Lr/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0, v4}, Lr/d;->a(Lr/d;Z)Z

    invoke-interface {v1}, Lt/f;->f_()V

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->c:Z

    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->f(Lr/d;)V

    goto :goto_1

    :cond_1
    iput-boolean v4, p0, Lr/i;->b:Z

    :try_start_1
    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->d(Lr/d;)I

    move-result v0

    :goto_3
    if-lez v0, :cond_2

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Lr/i;->sleep(J)V

    iget-object v2, p0, Lr/i;->a:Lr/d;

    invoke-static {v2}, Lr/d;->e(Lr/d;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-boolean v0, p0, Lr/i;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lr/i;->a:Lr/d;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lr/d;->a(Lr/d;Z)Z

    invoke-interface {v1}, Lt/f;->f_()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    add-int/lit16 v0, v0, -0x3e8

    goto :goto_3
.end method
