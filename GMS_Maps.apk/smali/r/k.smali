.class public Lr/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lr/v;


# instance fields
.field final a:Lo/aq;

.field final b:Ls/e;

.field final c:Z

.field final d:Lr/c;

.field final e:Z

.field final f:Z

.field final g:Z

.field h:I

.field volatile i:Z

.field private j:Lr/k;


# direct methods
.method protected constructor <init>(Lo/aq;Ls/e;)V
    .locals 8

    const/4 v4, 0x0

    sget-object v3, Lr/c;->b:Lr/c;

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    return-void
.end method

.method protected constructor <init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lr/k;->i:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lr/k;->j:Lr/k;

    iput-object p1, p0, Lr/k;->a:Lo/aq;

    iput-object p2, p0, Lr/k;->b:Ls/e;

    iput-object p3, p0, Lr/k;->d:Lr/c;

    sget-object v1, Lr/c;->e:Lr/c;

    invoke-virtual {p3, v1}, Lr/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lr/c;->d:Lr/c;

    invoke-virtual {p3, v1}, Lr/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lr/k;->c:Z

    iput-boolean p4, p0, Lr/k;->e:Z

    iput p6, p0, Lr/k;->h:I

    iput-boolean p5, p0, Lr/k;->f:Z

    iput-boolean p7, p0, Lr/k;->g:Z

    return-void
.end method

.method protected constructor <init>(Lo/aq;Ls/e;ZZ)V
    .locals 8

    sget-object v3, Lr/c;->b:Lr/c;

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p3

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    return-void
.end method

.method private a(ILo/ap;)V
    .locals 2

    iget-object v0, p0, Lr/k;->b:Ls/e;

    iget-object v1, p0, Lr/k;->a:Lo/aq;

    invoke-interface {v0, v1, p1, p2}, Ls/e;->a(Lo/aq;ILo/ap;)V

    return-void
.end method

.method static synthetic a(Lr/k;ILo/ap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lr/k;->a(ILo/ap;)V

    return-void
.end method

.method static synthetic b(Lr/k;)Lr/k;
    .locals 1

    iget-object v0, p0, Lr/k;->j:Lr/k;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/k;->i:Z

    return-void
.end method

.method protected a(I)V
    .locals 0

    iput p1, p0, Lr/k;->h:I

    return-void
.end method

.method a(Lr/k;)V
    .locals 1

    iget-object v0, p0, Lr/k;->j:Lr/k;

    iput-object v0, p1, Lr/k;->j:Lr/k;

    iput-object p1, p0, Lr/k;->j:Lr/k;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lr/k;->i:Z

    return v0
.end method

.method public c()Lo/aq;
    .locals 1

    iget-object v0, p0, Lr/k;->a:Lo/aq;

    return-object v0
.end method

.method protected d()Lr/c;
    .locals 1

    iget-object v0, p0, Lr/k;->d:Lr/c;

    return-object v0
.end method

.method protected e()Z
    .locals 1

    iget-boolean v0, p0, Lr/k;->c:Z

    return v0
.end method

.method protected f()Z
    .locals 1

    iget-boolean v0, p0, Lr/k;->e:Z

    return v0
.end method

.method protected g()I
    .locals 1

    iget v0, p0, Lr/k;->h:I

    return v0
.end method

.method protected h()Z
    .locals 1

    iget-boolean v0, p0, Lr/k;->f:Z

    return v0
.end method

.method protected i()Z
    .locals 1

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    iget-object p0, p0, Lr/k;->j:Lr/k;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected j()Z
    .locals 1

    iget-boolean v0, p0, Lr/k;->g:Z

    return v0
.end method

.method protected k()Z
    .locals 1

    iget-object v0, p0, Lr/k;->j:Lr/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
