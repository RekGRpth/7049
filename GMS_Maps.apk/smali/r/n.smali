.class public Lr/n;
.super LR/c;
.source "SourceFile"

# interfaces
.implements Law/q;


# static fields
.field private static a:Lr/n;


# instance fields
.field private final b:Lcom/google/googlenav/common/a;

.field private final c:Law/h;

.field private final d:Lt/u;

.field private final e:Ljava/io/File;

.field private f:Z

.field private g:Landroid/os/Handler;

.field private final h:Ljava/util/Map;

.field private i:Z


# direct methods
.method protected constructor <init>(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)V
    .locals 2

    const-string v0, "ibs"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lr/n;->c:Law/h;

    iput-object p4, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    new-instance v0, Lt/u;

    iget-object v1, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p3, v1}, Lt/u;-><init>(Ljava/util/Locale;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Lr/n;->d:Lt/u;

    iput-object p2, p0, Lr/n;->e:Ljava/io/File;

    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lr/n;->h:Ljava/util/Map;

    return-void
.end method

.method public static a()Lr/n;
    .locals 1

    sget-object v0, Lr/n;->a:Lr/n;

    return-object v0
.end method

.method public static a(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)Lr/n;
    .locals 1

    sget-object v0, Lr/n;->a:Lr/n;

    if-nez v0, :cond_0

    new-instance v0, Lr/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lr/n;-><init>(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)V

    sput-object v0, Lr/n;->a:Lr/n;

    :cond_0
    sget-object v0, Lr/n;->a:Lr/n;

    return-object v0
.end method

.method static synthetic a(Lr/n;)V
    .locals 0

    invoke-direct {p0}, Lr/n;->h()V

    return-void
.end method

.method static synthetic a(Lr/n;Lr/p;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/n;->a(Lr/p;)V

    return-void
.end method

.method static synthetic a(Lr/n;Ls/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/n;->a(Ls/d;)V

    return-void
.end method

.method private a(Lr/p;)V
    .locals 6

    const/4 v5, 0x1

    invoke-direct {p0}, Lr/n;->j()V

    iget-object v1, p1, Lr/p;->a:Lo/r;

    iget-object v2, p1, Lr/p;->b:Ls/c;

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, v1}, Lt/u;->a(Lo/r;)Lo/y;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz v2, :cond_0

    iget-object v3, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v3, v0}, Lt/u;->a(Lo/y;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v1, v3, v4}, Ls/c;->a(Lo/r;ILo/y;)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    invoke-virtual {v0, v3}, Lo/y;->a(Lcom/google/googlenav/common/a;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, v0}, Ls/c;->a(Lo/r;ILo/y;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/d;

    if-nez v0, :cond_4

    new-instance v0, Ls/d;

    invoke-direct {v0, v1}, Ls/d;-><init>(Lo/r;)V

    iget-object v3, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v0, v2}, Ls/d;->a(Ls/c;)V

    :cond_5
    invoke-virtual {v0}, Ls/d;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lr/n;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v5, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iput-boolean v5, p0, Lr/n;->i:Z

    goto :goto_1
.end method

.method private a(Ls/d;)V
    .locals 3

    invoke-direct {p0}, Lr/n;->j()V

    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Ls/d;->e_()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lt/u;->c(Lo/r;)V

    :cond_1
    :goto_0
    invoke-virtual {p1, v0}, Ls/d;->a(Lo/y;)V

    return-void

    :cond_2
    invoke-virtual {p1}, Ls/d;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lt/u;->a(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/y;

    move-result-object v0

    goto :goto_0
.end method

.method public static b()V
    .locals 1

    sget-object v0, Lr/n;->a:Lr/n;

    if-eqz v0, :cond_0

    sget-object v0, Lr/n;->a:Lr/n;

    invoke-virtual {v0}, Lr/n;->d()V

    const/4 v0, 0x0

    sput-object v0, Lr/n;->a:Lr/n;

    :cond_0
    return-void
.end method

.method static synthetic b(Lr/n;Ls/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lr/n;->b(Ls/d;)V

    return-void
.end method

.method private b(Ls/d;)V
    .locals 2

    invoke-direct {p0}, Lr/n;->j()V

    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ls/d;->a(Lo/y;)V

    return-void
.end method

.method private h()V
    .locals 3

    invoke-direct {p0}, Lr/n;->j()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/n;->i:Z

    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    :try_start_0
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/d;

    invoke-virtual {v0}, Ls/d;->d()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lr/n;->c:Law/h;

    invoke-virtual {v2, v0}, Law/h;->c(Law/g;)V

    invoke-virtual {v0}, Ls/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lr/n;->c:Law/h;

    invoke-virtual {v1}, Law/h;->h()V

    throw v0

    :cond_1
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0}, Law/h;->h()V

    return-void
.end method

.method private i()V
    .locals 1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lr/n;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private j()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on IndoorBuildingStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lo/r;)Lo/y;
    .locals 2

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, p1}, Lt/u;->b(Lo/r;)Lo/y;

    move-result-object v0

    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v1, v0}, Lt/u;->a(Lo/y;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Law/g;)V
    .locals 3

    invoke-interface {p1}, Law/g;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public a(Lo/r;Ls/c;)V
    .locals 4

    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lr/p;

    invoke-direct {v3, p1, p2}, Lr/p;-><init>(Lo/r;Ls/c;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public b(Law/g;)V
    .locals 3

    invoke-interface {p1}, Law/g;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public b(Lo/r;)Z
    .locals 2

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, p1}, Lt/u;->b(Lo/r;)Lo/y;

    move-result-object v0

    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v1, v0}, Lt/u;->a(Lo/y;)Z

    move-result v0

    return v0
.end method

.method public c(Lo/r;)Lo/z;
    .locals 1

    invoke-virtual {p0, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    invoke-virtual {p0}, Lr/n;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0, p0}, Law/h;->a(Law/q;)V

    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :try_start_0
    invoke-virtual {p0}, Lr/n;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->c()V

    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lr/n;->i()V

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->a()V

    return-void
.end method

.method public f()V
    .locals 1

    invoke-direct {p0}, Lr/n;->i()V

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->b()V

    return-void
.end method

.method public g()J
    .locals 2

    invoke-direct {p0}, Lr/n;->i()V

    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public k()V
    .locals 0

    return-void
.end method

.method public l()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lr/o;

    invoke-direct {v0, p0}, Lr/o;-><init>(Lr/n;)V

    iput-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr/n;->d:Lt/u;

    iget-object v1, p0, Lr/n;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Lt/u;->a(Ljava/io/File;)V

    :cond_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lr/n;->f:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lr/n;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
