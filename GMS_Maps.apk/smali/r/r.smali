.class Lr/r;
.super Lr/h;
.source "SourceFile"


# instance fields
.field a:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:Lcom/google/googlenav/common/a;


# direct methods
.method constructor <init>(Lcom/google/googlenav/common/a;)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lr/h;-><init>(I)V

    new-array v0, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lr/r;->a:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p1, p0, Lr/r;->b:Lcom/google/googlenav/common/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0}, Lr/r;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dB;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    invoke-virtual {p0}, Lr/r;->c()I

    move-result v0

    if-eq v2, v0, :cond_1

    :cond_0
    return v5

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lr/r;->a:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected a(Lr/k;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lr/r;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v0

    check-cast v0, Lr/s;

    invoke-virtual {p0, v2}, Lr/r;->a(I)Lr/k;

    move-result-object v1

    invoke-virtual {v1}, Lr/k;->c()Lo/aq;

    move-result-object v1

    check-cast v1, Lr/s;

    invoke-virtual {v0, v1}, Lr/s;->a(Lr/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x24

    return v0
.end method

.method public b(I)Lo/ap;
    .locals 9

    const/4 v8, 0x3

    iget-object v0, p0, Lr/r;->a:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v2, v0, p1

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v4, Lo/al;

    invoke-direct {v4}, Lo/al;-><init>()V

    invoke-virtual {p0, p1}, Lr/r;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    check-cast v0, Lr/s;

    invoke-virtual {v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v4}, Lo/al;->a()Lo/aj;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lr/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/aj;Lo/aq;)Lo/n;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0x14

    if-ne v6, v7, :cond_3

    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lo/n;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lo/n;

    invoke-virtual {v0}, Lr/s;->l()Lo/J;

    move-result-object v5

    const-wide/16 v2, -0x1

    invoke-virtual {v5}, Lo/J;->e()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v2, p0, Lr/r;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    invoke-virtual {v5}, Lo/J;->f()J

    move-result-wide v5

    add-long/2addr v2, v5

    :cond_2
    new-instance v5, Lo/aN;

    invoke-direct {v5}, Lo/aN;-><init>()V

    invoke-virtual {v5, v4}, Lo/aN;->a(Lo/al;)Lo/aN;

    move-result-object v4

    invoke-virtual {v4, v0}, Lo/aN;->a(Lo/aq;)Lo/aN;

    move-result-object v0

    invoke-virtual {v0, v1}, Lo/aN;->a([Lo/n;)Lo/aN;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lo/aN;->a(J)Lo/aN;

    move-result-object v0

    invoke-virtual {v0}, Lo/aN;->a()Lo/aL;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 11

    const/16 v10, 0x16

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x2

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dB;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v0, 0x80

    invoke-virtual {v2, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, v1}, Lr/r;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    check-cast v0, Lr/s;

    invoke-virtual {v0}, Lr/s;->l()Lo/J;

    move-result-object v0

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0x15

    invoke-virtual {v0}, Lo/J;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lo/J;->d()[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    invoke-virtual {v3, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v5, v8, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v10, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_1
    invoke-virtual {p0}, Lr/r;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lr/r;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/wireless/googlenav/proto/j2me/hs;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v4, 0x8

    invoke-virtual {v3, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v4

    invoke-virtual {v3, v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v4

    invoke-virtual {v3, v9, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x4

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v9, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method
