.class Lt/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:I

.field final c:I

.field final d:I

.field final e:I

.field final f:I

.field final g:I

.field final h:I


# direct methods
.method constructor <init>(JIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lt/o;->a:J

    iput p3, p0, Lt/o;->b:I

    iput p6, p0, Lt/o;->c:I

    iput p4, p0, Lt/o;->d:I

    iput p5, p0, Lt/o;->e:I

    iput p7, p0, Lt/o;->f:I

    iput p8, p0, Lt/o;->g:I

    iput p9, p0, Lt/o;->h:I

    return-void
.end method

.method private static a(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x5

    return v0
.end method

.method static a([BIII)Lt/o;
    .locals 10

    invoke-static {p0, p1}, Lt/h;->c([BI)J

    move-result-wide v1

    add-int/lit8 v0, p1, 0x8

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v4

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v5

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v7

    invoke-static {v4}, Lt/o;->a(I)I

    move-result v3

    invoke-static {v4}, Lt/o;->b(I)I

    move-result v6

    invoke-static {v5}, Lt/o;->c(I)I

    move-result v4

    invoke-static {v5}, Lt/o;->d(I)I

    move-result v5

    new-instance v0, Lt/o;

    move v8, p2

    move v9, p3

    invoke-direct/range {v0 .. v9}, Lt/o;-><init>(JIIIIIII)V

    return-object v0
.end method

.method private static b(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x1f

    return v0
.end method

.method static b([BI)J
    .locals 2

    invoke-static {p0, p1}, Lt/h;->c([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method private static c(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x18

    return v0
.end method

.method static c([BI)I
    .locals 1

    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v0

    invoke-static {v0}, Lt/o;->c(I)I

    move-result v0

    return v0
.end method

.method private static d(I)I
    .locals 1

    const v0, 0xffffff

    and-int/2addr v0, p0

    return v0
.end method

.method static d([BI)I
    .locals 1

    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v0

    invoke-static {v0}, Lt/o;->d(I)I

    move-result v0

    return v0
.end method

.method static e([BI)I
    .locals 1

    add-int/lit8 v0, p1, 0x8

    invoke-static {p0, v0}, Lt/h;->a([BI)I

    move-result v0

    invoke-static {v0}, Lt/o;->b(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method a([BI)V
    .locals 4

    iget-wide v0, p0, Lt/o;->a:J

    invoke-static {p1, p2, v0, v1}, Lt/h;->a([BIJ)V

    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lt/o;->b:I

    shl-int/lit8 v1, v1, 0x5

    iget v2, p0, Lt/o;->c:I

    or-int/2addr v1, v2

    invoke-static {v1}, Lt/o;->a(I)I

    move-result v2

    iget v3, p0, Lt/o;->b:I

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not pack data offset of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lt/o;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v1}, Lt/o;->b(I)I

    move-result v2

    iget v3, p0, Lt/o;->c:I

    if-eq v2, v3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not pack refCount of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lt/o;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1, v0, v1}, Lt/h;->a([BII)V

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lt/o;->d:I

    shl-int/lit8 v1, v1, 0x18

    iget v2, p0, Lt/o;->e:I

    or-int/2addr v1, v2

    invoke-static {p1, v0, v1}, Lt/h;->a([BII)V

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lt/o;->f:I

    invoke-static {p1, v0, v1}, Lt/h;->a([BII)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lt/o;

    iget v2, p0, Lt/o;->g:I

    iget v3, p1, Lt/o;->g:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lt/o;->h:I

    iget v3, p1, Lt/o;->h:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lt/o;->g:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lt/o;->h:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lt/o;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Off:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KeyLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DataLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Checksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Shard:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ShardIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt/o;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
