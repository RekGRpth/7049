.class Lu/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lu/a;

.field private b:Z

.field private c:I

.field private d:Lo/aq;

.field private e:Ljava/util/LinkedHashSet;

.field private f:Ljava/util/LinkedHashSet;

.field private g:Ljava/util/Iterator;


# direct methods
.method public constructor <init>(Lu/a;Z)V
    .locals 1

    iput-object p1, p0, Lu/b;->a:Lu/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lu/b;->c:I

    invoke-static {}, Lcom/google/common/collect/dA;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-static {}, Lcom/google/common/collect/dA;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    iput-boolean p2, p0, Lu/b;->b:Z

    return-void
.end method

.method private b(Z)V
    .locals 3

    iget v0, p0, Lu/b;->c:I

    iget-object v1, p0, Lu/b;->a:Lu/a;

    invoke-static {v1}, Lu/a;->c(Lu/a;)I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lu/b;->a:Lu/a;

    invoke-static {v0}, Lu/a;->e(Lu/a;)Lg/a;

    move-result-object v0

    iget-object v1, p0, Lu/b;->d:Lo/aq;

    iget-object v2, p0, Lu/b;->a:Lu/a;

    invoke-static {v2}, Lu/a;->d(Lu/a;)Lo/T;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lg/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lu/b;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/b;->c:I

    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    iput-object v1, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    iput-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)Lu/c;
    .locals 5

    iget-object v0, p0, Lu/b;->d:Lo/aq;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lu/b;->b(Z)V

    :cond_0
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lu/b;->d:Lo/aq;

    new-instance v1, Lu/c;

    iget-object v2, p0, Lu/b;->d:Lo/aq;

    iget-object v0, p0, Lu/b;->a:Lu/a;

    invoke-static {v0}, Lu/a;->b(Lu/a;)J

    move-result-wide v3

    iget-boolean v0, p0, Lu/b;->b:Z

    if-nez v0, :cond_2

    iget v0, p0, Lu/b;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, v2, v3, v4, v0}, Lu/c;-><init>(Lo/aq;JZ)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lu/b;->c:I

    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lu/b;->d:Lo/aq;

    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lu/b;->a:Lu/a;

    invoke-static {v1}, Lu/a;->a(Lu/a;)Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    return-void
.end method
