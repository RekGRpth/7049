.class public Lu/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:LF/T;


# instance fields
.field protected volatile a:LD/a;

.field private c:LB/a;

.field private final d:Lr/z;

.field private volatile e:LB/e;

.field private final f:LR/a;

.field private final g:Ljava/util/List;

.field private h:LA/b;

.field private final i:LR/h;

.field private j:I

.field private k:I

.field private final l:Lr/A;

.field private m:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/LinkedList;

.field private p:Ljava/util/Map;

.field private q:Ljava/util/Map;

.field private volatile r:I

.field private final s:Lu/a;

.field private t:Lu/c;

.field private final u:Ls/e;

.field private final v:Ls/e;

.field private final w:Lu/h;

.field private volatile x:J

.field private y:Lcom/google/googlenav/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LF/aa;

    invoke-direct {v0}, LF/aa;-><init>()V

    sput-object v0, Lu/d;->b:LF/T;

    return-void
.end method

.method public constructor <init>(LA/c;LB/e;Lu/a;LR/a;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LR/h;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lu/d;->i:LR/h;

    iput v3, p0, Lu/d;->j:I

    iput v3, p0, Lu/d;->k:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lu/d;->n:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lu/d;->p:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lu/d;->q:Ljava/util/Map;

    iput v3, p0, Lu/d;->r:I

    new-instance v0, Lu/f;

    invoke-direct {v0, p0, v2}, Lu/f;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->u:Ls/e;

    new-instance v0, Lu/g;

    invoke-direct {v0, p0, v2}, Lu/g;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->v:Ls/e;

    new-instance v0, Lu/h;

    invoke-direct {v0, p0, v2}, Lu/h;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->w:Lu/h;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    iput-object p2, p0, Lu/d;->e:LB/e;

    iput-object v2, p0, Lu/d;->c:LB/a;

    iput-object p4, p0, Lu/d;->f:LR/a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lu/d;->g:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    iput-object v0, p0, Lu/d;->d:Lr/z;

    new-instance v0, Lu/e;

    invoke-direct {v0, p0}, Lu/e;-><init>(Lu/d;)V

    iput-object v0, p0, Lu/d;->l:Lr/A;

    iget-object v0, p0, Lu/d;->d:Lr/z;

    iget-object v1, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v1}, Lr/z;->a(Lr/A;)V

    :goto_0
    iput-object p3, p0, Lu/d;->s:Lu/a;

    return-void

    :cond_0
    iput-object v2, p0, Lu/d;->d:Lr/z;

    iput-object v2, p0, Lu/d;->l:Lr/A;

    goto :goto_0
.end method

.method public constructor <init>(LA/c;Lu/a;)V
    .locals 2

    new-instance v0, LB/e;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LB/e;-><init>(LA/c;Ljava/util/Set;)V

    sget-object v1, LR/a;->a:LR/a;

    invoke-direct {p0, p1, v0, p2, v1}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    return-void
.end method

.method private a(Lo/aq;ILo/ap;)LF/T;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lu/d;->a(Lo/aq;ILo/ap;Z)LF/T;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/aq;ILo/ap;Z)LF/T;
    .locals 8

    const/4 v5, 0x1

    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lu/d;->i:LR/h;

    invoke-virtual {v1, p1, p1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lu/d;->h:LA/b;

    invoke-virtual {p1}, Lo/aq;->k()Lo/aB;

    move-result-object v2

    invoke-interface {v1, v2}, LA/b;->a(Lo/aB;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    const/4 v4, 0x0

    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lo/aq;->k()Lo/aB;

    move-result-object v1

    if-eqz v1, :cond_4

    instance-of v1, p3, Lo/aL;

    if-eqz v1, :cond_4

    iget-object v6, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v6

    :try_start_2
    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, p3

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr/D;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v3}, Lr/D;->a(Lo/aq;Z)Lo/ap;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object v0, v2

    check-cast v0, Lo/aL;

    move-object v1, v0

    move-object v0, v3

    check-cast v0, Lo/aL;

    move-object v2, v0

    invoke-static {v1, v2}, Lo/P;->a(Lo/aL;Lo/aL;)Lo/aL;

    move-result-object v2

    move v1, v4

    :goto_2
    move v4, v1

    goto :goto_1

    :cond_2
    move-object v0, p3

    check-cast v0, Lo/aL;

    move-object v3, v0

    invoke-virtual {v1, p1, v3}, Lr/D;->a(Lo/aq;Lo/aL;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v5

    goto :goto_2

    :cond_3
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object p3, v2

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lu/d;->b(Lo/aq;ILo/ap;)LF/T;

    move-result-object v1

    if-eqz v4, :cond_0

    const-wide/16 v2, 0xbb8

    invoke-interface {v1, v2, v3}, LF/T;->a(J)V

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    :cond_5
    move v1, v4

    goto :goto_2
.end method

.method static synthetic a(Lu/d;Lo/aq;)LF/T;
    .locals 1

    invoke-direct {p0, p1}, Lu/d;->c(Lo/aq;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lu/d;Lo/aq;ILo/ap;)LF/T;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lu/d;->a(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lu/d;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lu/d;Lu/c;)Lu/c;
    .locals 0

    iput-object p1, p0, Lu/d;->t:Lu/c;

    return-object p1
.end method

.method private a(LB/e;Z)V
    .locals 2

    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    invoke-virtual {v0, v1, p1}, LB/a;->b(LD/a;LB/e;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    invoke-virtual {v0, v1, p1}, LB/a;->a(LD/a;LB/e;)V

    goto :goto_0
.end method

.method private a(Lo/aq;LF/T;J)V
    .locals 6

    if-eqz p2, :cond_0

    sget-object v0, Lu/d;->b:LF/T;

    if-eq p2, v0, :cond_1

    move-object v2, p2

    :goto_0
    invoke-virtual {p0}, Lu/d;->j()Z

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lu/d;->a(Lo/aq;LF/T;ZJ)V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a(Lo/aq;LF/T;ZJ)V
    .locals 9

    const/4 v0, 0x0

    iget-object v7, p0, Lu/d;->o:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu/i;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lu/i;->a(Lo/aq;LF/T;ZJ)V

    :cond_0
    monitor-exit v7

    return-void

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v6, v0

    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu/i;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lu/i;->a(Lo/aq;LF/T;ZJ)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lo/aq;ZLs/e;)V
    .locals 2

    iget-object v1, p0, Lu/d;->i:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lu/d;->i:LR/h;

    invoke-virtual {v0, p1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lu/d;->d:Lr/z;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0, p1, p3}, Lr/z;->c(Lo/aq;Ls/e;)V

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lu/d;->d(Lo/aq;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0, p1, p3}, Lr/z;->a(Lo/aq;Ls/e;)V

    goto :goto_0
.end method

.method static synthetic a(Lu/d;Lo/aq;LF/T;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lu/d;->a(Lo/aq;LF/T;J)V

    return-void
.end method

.method static synthetic a(Lu/d;Lo/aq;ZLs/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lu/d;->a(Lo/aq;ZLs/e;)V

    return-void
.end method

.method private a(Lo/aq;LF/T;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {p2, v1}, LF/T;->a(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lu/d;->p:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lu/d;->u:Ls/e;

    invoke-direct {p0, p1, v0, v1}, Lu/d;->a(Lo/aq;ZLs/e;)V

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1, p2}, Lu/d;->b(Lo/aq;LF/T;)V

    goto :goto_0
.end method

.method static synthetic a(Lu/d;Lo/aq;LF/T;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lu/d;->a(Lo/aq;LF/T;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lu/d;)LB/e;
    .locals 1

    iget-object v0, p0, Lu/d;->e:LB/e;

    return-object v0
.end method

.method private b(Lo/aq;ILo/ap;)LF/T;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lu/d;->a:LD/a;

    if-eqz v2, :cond_4

    if-nez p2, :cond_4

    instance-of v0, p3, Lo/aL;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lu/d;->f:LR/a;

    invoke-static {p3, v0, v2}, LF/Y;->a(Lo/ap;LR/a;LD/a;)LF/Y;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    sget-object v0, Lu/d;->b:LF/T;

    :cond_0
    iget-object v1, p0, Lu/d;->c:LB/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v2, v3, p1, v0}, LB/a;->a(LD/a;LB/e;Lo/aq;LF/T;)V

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    instance-of v0, p3, Lo/x;

    if-eqz v0, :cond_4

    invoke-static {p3, v2}, LF/k;->a(Lo/ap;LD/a;)LF/k;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lu/d;Lo/aq;ILo/ap;)LF/T;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lu/d;->c(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method private b(Lo/aq;LF/T;)V
    .locals 0

    return-void
.end method

.method private c(Lo/aq;)LF/T;
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v2, v3, p1, v0}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v0

    sget-object v1, Lu/d;->b:LF/T;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v0, v1}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private c(Lo/aq;ILo/ap;)LF/T;
    .locals 6

    const/4 v1, 0x0

    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p3

    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->r()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p3

    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, p1, v4}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v2, Lu/d;->b:LF/T;

    if-eq v0, v2, :cond_2

    check-cast v0, LF/Y;

    check-cast p3, Lo/aL;

    invoke-virtual {p3}, Lo/aL;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LF/Y;->b(J)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    :try_start_0
    iget v0, p0, Lu/d;->k:I

    iget v3, p0, Lu/d;->j:I

    add-int/2addr v0, v3

    and-int/lit8 v0, v0, 0x7f

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_3

    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lu/d;->i:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    iget v0, p0, Lu/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/d;->k:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_5
    :try_start_3
    iget v0, p0, Lu/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/d;->j:I

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v0, 0x1

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2, p1, v0}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p1, p2, v0}, Lu/d;->a(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lu/d;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lu/d;->p:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lu/d;)Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method private d(Lo/aq;)V
    .locals 5

    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    sget-object v0, Lr/I;->h:Lo/aq;

    if-eq p1, v0, :cond_1

    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lu/d;->d:Lr/z;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v0, p0, Lu/d;->d:Lr/z;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    check-cast v0, Lo/aL;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    iget-object v4, p0, Lu/d;->w:Lu/h;

    invoke-virtual {v0, p1, v1, v4}, Lr/D;->a(Lo/aq;Lo/aL;Ls/e;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic e(Lu/d;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lu/d;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lu/d;)I
    .locals 2

    iget v0, p0, Lu/d;->r:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lu/d;->r:I

    return v0
.end method

.method static synthetic g(Lu/d;)Lu/a;
    .locals 1

    iget-object v0, p0, Lu/d;->s:Lu/a;

    return-object v0
.end method

.method static synthetic h(Lu/d;)I
    .locals 2

    iget v0, p0, Lu/d;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lu/d;->r:I

    return v0
.end method

.method static synthetic i(Lu/d;)Ls/e;
    .locals 1

    iget-object v0, p0, Lu/d;->v:Ls/e;

    return-object v0
.end method

.method static synthetic j(Lu/d;)Lu/c;
    .locals 1

    iget-object v0, p0, Lu/d;->t:Lu/c;

    return-object v0
.end method

.method static synthetic l()LF/T;
    .locals 1

    sget-object v0, Lu/d;->b:LF/T;

    return-object v0
.end method

.method private final m()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iget-wide v2, p0, Lu/d;->x:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called from renderer thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()LA/c;
    .locals 1

    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0}, Lr/z;->a()LA/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aq;)LF/T;
    .locals 6

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->a:LD/a;

    iget-object v4, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v3, v4, p1, v2}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v1

    sget-object v3, Lu/d;->b:LF/T;

    if-ne v1, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v3, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v3}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_2
    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->a:LD/a;

    iget-object v4, p0, Lu/d;->e:LB/e;

    invoke-virtual {p1}, Lo/aq;->a()Lo/aq;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5, v2}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v1

    sget-object v2, Lu/d;->b:LF/T;

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_3

    iget-object v2, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v2}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v1, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lu/d;->x:J

    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0, p1, p2}, LB/a;->a(J)V

    :cond_0
    return-void
.end method

.method public a(LA/b;)V
    .locals 0

    iput-object p1, p0, Lu/d;->h:LA/b;

    return-void
.end method

.method public a(LA/c;)V
    .locals 4

    iget-object v0, p0, Lu/d;->d:Lr/z;

    instance-of v0, v0, Lr/I;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Modifiers not supported on store \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p1, LA/c;->x:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only modifiers may be added, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    instance-of v1, v0, Lr/D;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Modifier store \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lr/z;->a()LA/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' must be a vector modifier store"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    monitor-exit v1

    :goto_0
    return-void

    :cond_3
    iget-object v2, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v2}, Lr/z;->a(Lr/A;)V

    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    check-cast v0, Lr/D;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    invoke-virtual {v0}, Lr/D;->a()LA/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown tile store "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v2, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v2

    :try_start_4
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    new-instance v0, LB/e;

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    iget-object v3, p0, Lu/d;->f:LR/a;

    invoke-direct {v0, v2, v1, v3}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    iput-object v0, p0, Lu/d;->e:LB/e;

    goto :goto_0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public a(LD/a;)V
    .locals 1

    const-string v0, "GLState should not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lu/d;->a:LD/a;

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    invoke-static {v0}, LB/a;->a(Lcom/google/googlenav/common/a;)V

    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    iput-object v0, p0, Lu/d;->c:LB/a;

    return-void
.end method

.method public a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V
    .locals 8

    invoke-direct {p0}, Lu/d;->m()V

    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->e(LD/a;LB/e;)V

    iget-object v7, p0, Lu/d;->s:Lu/a;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lu/d;->s:Lu/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lu/a;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->b()Lu/c;

    move-result-object v0

    iget-object v1, p0, Lu/d;->t:Lu/c;

    if-nez v1, :cond_0

    iget-object v1, v0, Lu/c;->a:Lo/aq;

    iget-boolean v2, v0, Lu/c;->b:Z

    iget-object v3, p0, Lu/d;->v:Ls/e;

    invoke-direct {p0, v1, v2, v3}, Lu/d;->a(Lo/aq;ZLs/e;)V

    :cond_0
    iput-object v0, p0, Lu/d;->t:Lu/c;

    monitor-exit v7

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    invoke-direct {p0}, Lu/d;->m()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    sget-object v3, Lu/d;->b:LF/T;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v2, v3, v1}, LB/a;->a(LD/a;LB/e;Ljava/util/List;)V

    return-void
.end method

.method public a(Lu/i;)V
    .locals 2

    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lu/d;->e:LB/e;

    invoke-direct {p0, v0, p1}, Lu/d;->a(LB/e;Z)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public b(Lo/aq;)LF/T;
    .locals 2

    invoke-direct {p0, p1}, Lu/d;->c(Lo/aq;)LF/T;

    move-result-object v0

    sget-object v1, Lu/d;->b:LF/T;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, v0}, Lu/d;->a(Lo/aq;LF/T;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0
.end method

.method public b(LA/c;)V
    .locals 4

    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v3}, Lr/z;->b(Lr/A;)V

    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    invoke-virtual {v0}, Lr/I;->a()LA/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v2

    :try_start_2
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    new-instance v0, LB/e;

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    iget-object v3, p0, Lu/d;->f:LR/a;

    invoke-direct {v0, v2, v1, v3}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    iput-object v0, p0, Lu/d;->e:LB/e;

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b(Ljava/util/List;)V
    .locals 4

    invoke-direct {p0}, Lu/d;->m()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    sget-object v3, Lu/d;->b:LF/T;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v2, v3, v1}, LB/a;->b(LD/a;LB/e;Ljava/util/List;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0, p1}, LB/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    invoke-direct {p0}, Lu/d;->m()V

    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->e(LD/a;LB/e;)V

    return-void
.end method

.method public d()V
    .locals 3

    invoke-direct {p0}, Lu/d;->m()V

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->d(LD/a;LB/e;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lu/d;->m()V

    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->a()V

    return-void
.end method

.method public f()LB/e;
    .locals 1

    iget-object v0, p0, Lu/d;->e:LB/e;

    return-object v0
.end method

.method public g()V
    .locals 1

    invoke-direct {p0}, Lu/d;->m()V

    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0}, LB/a;->b()V

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/e;

    invoke-direct {p0, v0, v2}, Lu/d;->a(LB/e;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public i()V
    .locals 3

    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->c(LD/a;LB/e;)V

    invoke-virtual {p0}, Lu/d;->h()Z

    :cond_0
    return-void
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lu/d;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lu/d;->r:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lu/d;->s:Lu/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    return-object v0
.end method
