.class Lv/f;
.super Law/a;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field b:Lv/a;

.field final synthetic c:Lv/d;


# direct methods
.method private constructor <init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;)V
    .locals 0

    iput-object p1, p0, Lv/f;->c:Lv/d;

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p2, p0, Lv/f;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p3, p0, Lv/f;->b:Lv/a;

    return-void
.end method

.method synthetic constructor <init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;Lv/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lv/f;-><init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v0}, Lv/a;->i()V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 2

    iget-object v0, p0, Lv/f;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    array-length v1, v0

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fj;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v2, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v2, v0}, Lv/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    iget-object v3, p0, Lv/f;->c:Lv/d;

    invoke-static {v3}, Lv/d;->a(Lv/d;)Lt/e;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v2}, Lv/a;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lv/f;->c:Lv/d;

    invoke-static {v2}, Lv/d;->a(Lv/d;)Lt/e;

    move-result-object v2

    invoke-virtual {v2, v0}, Lt/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x27

    return v0
.end method
