.class public Lx/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LD/b;

.field private final b:LE/i;

.field private final c:Lx/b;

.field private final d:Lx/a;


# direct methods
.method public constructor <init>(ILx/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-static {p1, v0}, LE/l;->b(II)LE/l;

    move-result-object v0

    iput-object v0, p0, Lx/c;->b:LE/i;

    new-instance v0, Lx/b;

    invoke-direct {v0}, Lx/b;-><init>()V

    iput-object v0, p0, Lx/c;->c:Lx/b;

    iput-object p2, p0, Lx/c;->d:Lx/a;

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lx/c;->a:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lx/c;->a:LD/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0}, LE/i;->b()I

    move-result v0

    return v0
.end method

.method public a(II)V
    .locals 3

    if-lez p2, :cond_0

    iget-object v0, p0, Lx/c;->d:Lx/a;

    iget-object v1, p0, Lx/c;->c:Lx/b;

    invoke-virtual {v0, p1, v1}, Lx/a;->a(ILx/b;)V

    iget-object v0, p0, Lx/c;->b:LE/i;

    iget-object v1, p0, Lx/c;->c:Lx/b;

    iget v1, v1, Lx/b;->a:I

    iget-object v2, p0, Lx/c;->c:Lx/b;

    iget v2, v2, Lx/b;->b:I

    invoke-virtual {v0, v1, v2, p2}, LE/i;->a(III)V

    :cond_0
    return-void
.end method

.method public a(LD/a;)V
    .locals 2

    iget-object v0, p0, Lx/c;->a:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->a()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lx/c;->c()V

    :cond_0
    iget-object v0, p0, Lx/c;->d:Lx/a;

    invoke-virtual {v0, p1}, Lx/a;->b(LD/a;)V

    iget-object v0, p0, Lx/c;->a:LD/b;

    if-nez v0, :cond_1

    iget-object v0, p0, Lx/c;->d:Lx/a;

    invoke-virtual {v0, p1}, Lx/a;->a(LD/a;)LD/b;

    move-result-object v0

    iput-object v0, p0, Lx/c;->a:LD/b;

    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->f()V

    :cond_1
    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0}, LE/i;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    invoke-direct {p0}, Lx/c;->c()V

    return-void
.end method

.method public c(LD/a;)V
    .locals 1

    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    invoke-direct {p0}, Lx/c;->c()V

    return-void
.end method
