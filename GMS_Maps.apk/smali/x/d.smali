.class public Lx/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(FFFF)I
    .locals 7

    const/16 v6, 0xff

    const/4 v5, 0x0

    const/high16 v3, 0x437f0000

    const/high16 v4, 0x3f000000

    mul-float v0, p0, v3

    add-float/2addr v0, v4

    float-to-int v0, v0

    invoke-static {v0, v5, v6}, Lx/d;->a(III)I

    move-result v0

    mul-float v1, p1, v3

    add-float/2addr v1, v4

    float-to-int v1, v1

    invoke-static {v1, v5, v6}, Lx/d;->a(III)I

    move-result v1

    mul-float v2, p2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-static {v2, v5, v6}, Lx/d;->a(III)I

    move-result v2

    mul-float/2addr v3, p3

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v3, v5, v6}, Lx/d;->a(III)I

    move-result v3

    shl-int/lit8 v0, v0, 0x18

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method public static a(FI)I
    .locals 2

    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    mul-float/2addr v0, p0

    const v1, 0xffffff

    and-int/2addr v1, p1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(I)I
    .locals 3

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, p0, 0xff

    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0xe

    return v0
.end method

.method private static a(III)I
    .locals 0

    if-ge p0, p1, :cond_0

    :goto_0
    return p1

    :cond_0
    if-le p0, p2, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method public static a(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 5

    const v4, 0xff00

    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v4

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    shr-int/lit8 v1, p1, 0x8

    and-int/2addr v1, v4

    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    and-int v2, p1, v4

    shr-int/lit8 v3, p1, 0x8

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    shl-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v4

    and-int/lit16 v4, p1, 0xff

    or-int/2addr v3, v4

    invoke-interface {p0, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method public static b(I)F
    .locals 7

    const/high16 v6, 0x40800000

    const/high16 v5, 0x40000000

    const/high16 v4, 0x42700000

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    and-int/lit16 v2, p0, 0xff

    int-to-float v2, v2

    cmpl-float v3, v0, v1

    if-nez v3, :cond_0

    cmpl-float v3, v1, v2

    if-nez v3, :cond_0

    const/high16 v0, -0x40800000

    :goto_0
    return v0

    :cond_0
    cmpl-float v3, v0, v1

    if-ltz v3, :cond_1

    cmpl-float v3, v1, v2

    if-ltz v3, :cond_1

    sub-float/2addr v1, v2

    mul-float/2addr v1, v4

    sub-float/2addr v0, v2

    div-float v0, v1, v0

    goto :goto_0

    :cond_1
    cmpl-float v3, v1, v0

    if-lez v3, :cond_2

    cmpl-float v3, v0, v2

    if-ltz v3, :cond_2

    sub-float/2addr v0, v2

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    sub-float v0, v5, v0

    mul-float/2addr v0, v4

    goto :goto_0

    :cond_2
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_3

    cmpl-float v3, v2, v0

    if-lez v3, :cond_3

    sub-float/2addr v2, v0

    sub-float v0, v1, v0

    div-float v0, v2, v0

    add-float/2addr v0, v5

    mul-float/2addr v0, v4

    goto :goto_0

    :cond_3
    cmpl-float v3, v2, v1

    if-lez v3, :cond_4

    cmpl-float v3, v1, v0

    if-lez v3, :cond_4

    sub-float/2addr v1, v0

    sub-float v0, v2, v0

    div-float v0, v1, v0

    sub-float v0, v6, v0

    mul-float/2addr v0, v4

    goto :goto_0

    :cond_4
    cmpl-float v3, v2, v0

    if-lez v3, :cond_5

    cmpl-float v3, v0, v1

    if-ltz v3, :cond_5

    sub-float/2addr v0, v1

    sub-float v1, v2, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v6

    mul-float/2addr v0, v4

    goto :goto_0

    :cond_5
    const/high16 v3, 0x40c00000

    sub-float/2addr v2, v1

    sub-float/2addr v0, v1

    div-float v0, v2, v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v4

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 2

    invoke-static {p0}, Lx/d;->b(I)F

    move-result v0

    const/high16 v1, 0x42a00000

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/high16 v1, 0x43200000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static e(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x8

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static f(I)I
    .locals 1

    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static g(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method
