.class public Lx/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/microedition/khronos/opengles/GL11;


# instance fields
.field private a:Lx/g;

.field private b:Lx/g;

.field private c:Lx/g;

.field private d:Lx/g;

.field private e:Lx/f;

.field private f:I

.field private g:I

.field private h:I

.field private i:[F

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lx/f;

    invoke-direct {v0}, Lx/f;-><init>()V

    iput-object v0, p0, Lx/e;->e:Lx/f;

    iput v2, p0, Lx/e;->f:I

    iput v2, p0, Lx/e;->g:I

    const/16 v0, 0x2100

    iput v0, p0, Lx/e;->h:I

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lx/e;->i:[F

    iput-boolean v2, p0, Lx/e;->j:Z

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "GL20 class is not ready for production use"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lx/g;

    const/16 v1, 0x1700

    invoke-direct {v0, v1}, Lx/g;-><init>(I)V

    iput-object v0, p0, Lx/e;->a:Lx/g;

    new-instance v0, Lx/g;

    const/16 v1, 0x1701

    invoke-direct {v0, v1}, Lx/g;-><init>(I)V

    iput-object v0, p0, Lx/e;->b:Lx/g;

    new-instance v0, Lx/g;

    const/16 v1, 0x1702

    invoke-direct {v0, v1}, Lx/g;-><init>(I)V

    iput-object v0, p0, Lx/e;->c:Lx/g;

    iget-object v0, p0, Lx/e;->a:Lx/g;

    iput-object v0, p0, Lx/e;->d:Lx/g;

    iget-object v0, p0, Lx/e;->i:[F

    const/high16 v1, 0x3f000000

    aput v1, v0, v2

    iget-object v0, p0, Lx/e;->i:[F

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000

    aput v2, v0, v1

    return-void
.end method

.method private a()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public glActiveTexture(I)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glAlphaFunc(IF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glAlphaFuncx(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glBindBuffer(II)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glBindBuffer(II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glBindTexture(II)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBindTexture(II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glBlendFunc(II)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBlendFunc(II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glBufferData(IILjava/nio/Buffer;I)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferData(IILjava/nio/Buffer;I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glBufferSubData(IIILjava/nio/Buffer;)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferSubData(IIILjava/nio/Buffer;)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glClear(I)V
    .locals 2

    invoke-static {p1}, Landroid/opengl/GLES10;->glClear(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glClearColor(FFFF)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColor(FFFF)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glClearColorx(IIII)V
    .locals 3

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Draw Count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lx/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lx/e;->f:I

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColorx(IIII)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glClearDepthf(F)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClearDepthx(I)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClearStencil(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glClearStencil(I)V

    return-void
.end method

.method public glClientActiveTexture(I)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClipPlanef(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glClipPlanex(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glColor4f(FFFF)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4f(FFFF)V

    return-void
.end method

.method public glColor4ub(BBBB)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glColor4x(IIII)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4x(IIII)V

    return-void
.end method

.method public glColorMask(ZZZZ)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glColorPointer(IIII)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glColorPointer(IIII)V

    return-void
.end method

.method public glColorPointer(IIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColorPointer(IIILjava/nio/Buffer;)V

    return-void
.end method

.method public glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glCompressedTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glCopyTexImage2D(IIIIIIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glCopyTexSubImage2D(IIIIIIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glCullFace(I)V
    .locals 2

    invoke-static {p1}, Landroid/opengl/GLES10;->glCullFace(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDeleteBuffers(ILjava/nio/IntBuffer;)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDeleteBuffers(I[II)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glDeleteBuffers(I[II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDeleteTextures(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glDeleteTextures(I[II)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDeleteTextures(I[II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDepthFunc(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glDepthFunc(I)V

    return-void
.end method

.method public glDepthMask(Z)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glDepthRangef(FF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glDepthRangex(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glDisable(I)V
    .locals 2

    invoke-static {p1}, Landroid/opengl/GLES10;->glDisable(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDisableClientState(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    return-void
.end method

.method public glDrawArrays(III)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lx/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lx/e;->f:I

    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->b:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->a:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDrawElements(IIII)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lx/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lx/e;->f:I

    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->b:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->a:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glDrawElements(IIII)V

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glDrawElements(IIILjava/nio/Buffer;)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lx/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lx/e;->f:I

    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->b:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lx/e;->a:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-static {v0}, Lx/f;->b(Lx/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glDrawElements(IIILjava/nio/Buffer;)V

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glEnable(I)V
    .locals 2

    invoke-static {p1}, Landroid/opengl/GLES10;->glEnable(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glEnableClientState(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    return-void
.end method

.method public glFinish()V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFlush()V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogf(IF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogx(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFogxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFrontFace(I)V
    .locals 2

    invoke-static {p1}, Landroid/opengl/GLES10;->glFrontFace(I)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glFrustumf(FFFFFF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glFrustumx(IIIIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGenBuffers(ILjava/nio/IntBuffer;)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glGenBuffers(ILjava/nio/IntBuffer;)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glGenBuffers(I[II)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glGenBuffers(I[II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glGenTextures(ILjava/nio/IntBuffer;)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glGenTextures(ILjava/nio/IntBuffer;)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glGenTextures(I[II)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGenTextures(I[II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glGetBooleanv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetBooleanv(I[ZI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetBufferParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetBufferParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetClipPlanef(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetClipPlanex(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetError()I
    .locals 1

    invoke-static {}, Landroid/opengl/GLES10;->glGetError()I

    move-result v0

    return v0
.end method

.method public glGetFixedv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetFixedv(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetFloatv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetFloatv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetIntegerv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetIntegerv(I[II)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGetIntegerv(I[II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glGetLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetLightfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetLightxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetMaterialfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetMaterialxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetPointerv(I[Ljava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetString(I)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public glGetTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexEnviv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexEnvxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameterfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glGetTexParameterxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glHint(II)V
    .locals 2

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glHint(II)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glIsBuffer(I)Z
    .locals 1

    invoke-direct {p0}, Lx/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public glIsEnabled(I)Z
    .locals 1

    invoke-direct {p0}, Lx/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public glIsTexture(I)Z
    .locals 1

    invoke-direct {p0}, Lx/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public glLightModelf(IF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightModelfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightModelfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightModelx(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightModelxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightModelxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightf(IIF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightx(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLightxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLineWidth(F)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidth(F)V

    return-void
.end method

.method public glLineWidthx(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidthx(I)V

    return-void
.end method

.method public glLoadIdentity()V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0}, Lx/f;->a()Lx/f;

    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V

    return-void
.end method

.method public glLoadMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLoadMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lx/f;->b([FI)V

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    return-void
.end method

.method public glLoadMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLoadMatrixx([II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glLogicOp(I)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialf(IIF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialx(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMaterialxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMatrixMode(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unexpected value"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lx/e;->a:Lx/g;

    iput-object v0, p0, Lx/e;->d:Lx/g;

    :goto_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lx/e;->b:Lx/g;

    iput-object v0, p0, Lx/e;->d:Lx/g;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lx/e;->c:Lx/g;

    iput-object v0, p0, Lx/e;->d:Lx/g;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1700
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public glMultMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMultMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lx/f;->a([FI)V

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glMultMatrixf([FI)V

    return-void
.end method

.method public glMultMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMultMatrixx([II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMultiTexCoord4f(IFFFF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glMultiTexCoord4x(IIIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glNormal3f(FFF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glNormal3x(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glNormalPointer(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glNormalPointer(IILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glOrthof(FFFFFF)V
    .locals 0

    invoke-static/range {p1 .. p6}, Landroid/opengl/GLES10;->glOrthof(FFFFFF)V

    return-void
.end method

.method public glOrthox(IIIIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPixelStorei(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterf(IF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterfv(I[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterx(II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointParameterxv(I[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointSize(F)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glPointSize(F)V

    return-void
.end method

.method public glPointSizePointerOES(IILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPointSizex(I)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glPolygonOffset(FF)V
    .locals 0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    return-void
.end method

.method public glPolygonOffsetx(II)V
    .locals 2

    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    return-void
.end method

.method public glPopMatrix()V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->b()Lx/f;

    invoke-static {}, Landroid/opengl/GLES10;->glPopMatrix()V

    return-void
.end method

.method public glPushMatrix()V
    .locals 2

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    iget-object v1, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v1}, Lx/g;->a()Lx/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lx/f;->a(Lx/f;)V

    invoke-static {}, Landroid/opengl/GLES10;->glPushMatrix()V

    return-void
.end method

.method public glReadPixels(IIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glRotatef(FFFF)V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lx/f;->a(FFFF)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatef(FFFF)V

    return-void
.end method

.method public glRotatex(IIII)V
    .locals 5

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lx/f;->a(FFFF)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatex(IIII)V

    return-void
.end method

.method public glSampleCoverage(FZ)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glSampleCoveragex(IZ)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glScalef(FFF)V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lx/f;->b(FFF)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalef(FFF)V

    return-void
.end method

.method public glScalex(III)V
    .locals 4

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lx/f;->b(FFF)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalex(III)V

    return-void
.end method

.method public glScissor(IIII)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glShadeModel(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glShadeModel(I)V

    return-void
.end method

.method public glStencilFunc(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glStencilMask(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES10;->glStencilMask(I)V

    return-void
.end method

.method public glStencilOp(III)V
    .locals 0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glStencilOp(III)V

    return-void
.end method

.method public glTexCoordPointer(IIII)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glTexCoordPointer(IIII)V

    const-string v0, "GL20"

    const-string v1, "glTexCoordPointer"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glTexCoordPointer(IIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    return-void
.end method

.method public glTexEnvf(IIF)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnvfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnvfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnvi(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnviv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnvx(III)V
    .locals 0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexEnvx(III)V

    return-void
.end method

.method public glTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexEnvxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameterf(IIF)V
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterf(IIF)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public glTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameterfv(II[FI)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameteri(III)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameteriv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameterx(III)V
    .locals 0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    return-void
.end method

.method public glTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexParameterxv(II[II)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-direct {p0}, Lx/e;->a()V

    return-void
.end method

.method public glTranslatef(FFF)V
    .locals 1

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lx/f;->a(FFF)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    return-void
.end method

.method public glTranslatex(III)V
    .locals 4

    iget-object v0, p0, Lx/e;->d:Lx/g;

    invoke-virtual {v0}, Lx/g;->c()Lx/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lx/f;->a(FFF)V

    int-to-float v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    return-void
.end method

.method public glVertexPointer(IIII)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glVertexPointer(IIII)V

    return-void
.end method

.method public glVertexPointer(IIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    return-void
.end method

.method public glViewport(IIII)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glViewport(IIII)V

    const-string v0, "GL20"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
