.class public Lx/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[F

.field private static final b:[F

.field private static final c:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x14

    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lx/r;->a:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lx/r;->b:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lx/r;->c:[F

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        -0x40800000
        0x3f800000
        0x0
        0x0
        0x0
        -0x40800000
        -0x40800000
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        -0x40800000
        0x0
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public static a(LE/q;LE/e;)V
    .locals 8

    const/4 v2, 0x0

    const v0, 0x3d80adfd

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v4, v3

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v5

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    move v3, v1

    move v1, v2

    :goto_0
    const/16 v6, 0x64

    if-ge v0, v6, :cond_1

    add-float v6, v3, v2

    add-float v7, v1, v2

    invoke-interface {p0, v6, v7, v2}, LE/q;->a(FFF)V

    if-eqz p1, :cond_0

    int-to-short v6, v0

    invoke-interface {p1, v6}, LE/e;->d(I)V

    :cond_0
    neg-float v6, v1

    mul-float/2addr v6, v4

    add-float/2addr v6, v3

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    mul-float v3, v6, v5

    mul-float/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static b(LE/q;LE/e;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v2, 0x0

    const v1, 0x3d80adfd

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v5, v3

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v6

    const/high16 v4, 0x3f800000

    invoke-interface {p0, v2, v2, v2}, LE/q;->a(FFF)V

    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, LE/e;->d(I)V

    :cond_0
    move v1, v2

    move v3, v4

    :goto_0
    const/16 v7, 0x64

    if-ge v0, v7, :cond_2

    add-float v7, v3, v2

    add-float v8, v1, v2

    invoke-interface {p0, v7, v8, v2}, LE/q;->a(FFF)V

    if-eqz p1, :cond_1

    add-int/lit8 v7, v0, 0x1

    int-to-short v7, v7

    invoke-interface {p1, v7}, LE/e;->d(I)V

    :cond_1
    neg-float v7, v1

    mul-float/2addr v7, v5

    add-float/2addr v7, v3

    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    mul-float v3, v7, v6

    mul-float/2addr v1, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    invoke-interface {p1, v0}, LE/e;->d(I)V

    :goto_1
    return-void

    :cond_3
    add-float v0, v4, v2

    invoke-interface {p0, v0, v2, v2}, LE/q;->a(FFF)V

    goto :goto_1
.end method
