.class public abstract Lz/D;
.super Lz/o;
.source "SourceFile"


# instance fields
.field protected g:Lz/C;

.field protected final h:Ljava/lang/Class;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;)V
    .locals 1

    sget-object v0, Lz/p;->d:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    iput-object p1, p0, Lz/D;->h:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public a(Lz/k;Lz/o;)V
    .locals 2

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string v0, "ShaderState"

    const-string v1, "glUseProgram"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lz/k;Lz/j;)Z
    .locals 3

    invoke-super {p0, p1, p2}, Lz/o;->a(Lz/k;Lz/j;)Z

    move-result v0

    iget-object v1, p0, Lz/D;->g:Lz/C;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lz/k;->d()Lz/B;

    move-result-object v1

    iget-object v2, p0, Lz/D;->h:Ljava/lang/Class;

    invoke-interface {v1, v2}, Lz/B;->a(Ljava/lang/Class;)Lz/C;

    move-result-object v1

    iput-object v1, p0, Lz/D;->g:Lz/C;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lz/D;->g:Lz/C;

    invoke-virtual {v1, p1, p2}, Lz/C;->a(Lz/k;Lz/j;)Z

    :cond_1
    return v0
.end method
