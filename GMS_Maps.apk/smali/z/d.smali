.class public Lz/d;
.super Lz/o;
.source "SourceFile"


# instance fields
.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lz/p;->g:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    const/4 v0, 0x1

    iput v0, p0, Lz/d;->g:I

    const/4 v0, 0x0

    iput v0, p0, Lz/d;->h:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    sget-object v0, Lz/p;->g:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    const/4 v0, 0x1

    iput v0, p0, Lz/d;->g:I

    const/4 v0, 0x0

    iput v0, p0, Lz/d;->h:I

    invoke-virtual {p0, p1, p2}, Lz/d;->a(II)V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 6

    const/16 v5, 0x305

    const/16 v4, 0x304

    const/16 v3, 0x303

    const/16 v2, 0x302

    const/4 v1, 0x1

    iget-boolean v0, p0, Lz/d;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    if-eqz p1, :cond_1

    if-eq p1, v1, :cond_1

    const/16 v0, 0x306

    if-eq p1, v0, :cond_1

    const/16 v0, 0x307

    if-eq p1, v0, :cond_1

    if-eq p1, v2, :cond_1

    if-eq p1, v3, :cond_1

    if-eq p1, v4, :cond_1

    if-eq p1, v5, :cond_1

    const/16 v0, 0x308

    if-eq p1, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid srcFactor value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz p2, :cond_2

    if-eq p2, v1, :cond_2

    const/16 v0, 0x300

    if-eq p2, v0, :cond_2

    const/16 v0, 0x301

    if-eq p2, v0, :cond_2

    if-eq p2, v2, :cond_2

    if-eq p2, v3, :cond_2

    if-eq p2, v4, :cond_2

    if-eq p2, v5, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid dstFactor value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput p1, p0, Lz/d;->g:I

    iput p2, p0, Lz/d;->h:I

    return-void
.end method

.method a(Lz/k;Lz/o;)V
    .locals 1

    if-nez p2, :cond_0

    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    :cond_0
    return-void
.end method
