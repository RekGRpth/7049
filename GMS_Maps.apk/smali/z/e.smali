.class public Lz/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[F

.field b:I

.field private c:Lz/A;

.field private volatile d:[F

.field private e:[F

.field private f:[F

.field private g:[F

.field private h:Z

.field private i:B

.field private j:Z

.field private final k:F

.field private final l:F

.field private final m:F

.field private final n:Lz/g;

.field private o:Ljava/util/List;


# direct methods
.method public constructor <init>(Lz/A;I[F)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->d:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->e:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->a:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->f:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->g:[F

    iput-boolean v2, p0, Lz/e;->h:Z

    iput v2, p0, Lz/e;->b:I

    iput-boolean v2, p0, Lz/e;->j:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lz/e;->o:Ljava/util/List;

    iput-object p1, p0, Lz/e;->c:Lz/A;

    int-to-byte v0, p2

    iput-byte v0, p0, Lz/e;->i:B

    const/high16 v0, -0x40800000

    iput v0, p0, Lz/e;->k:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lz/e;->l:F

    const/4 v0, 0x0

    iput v0, p0, Lz/e;->m:F

    sget-object v0, Lz/g;->c:Lz/g;

    iput-object v0, p0, Lz/e;->n:Lz/g;

    iget-object v0, p0, Lz/e;->d:[F

    invoke-static {p3, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/e;->h:Z

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lz/e;->f:[F

    iget-object v2, p0, Lz/e;->a:[F

    iget-object v4, p0, Lz/e;->d:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lz/e;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/e;->b:I

    return-void
.end method

.method a(Lz/A;II)V
    .locals 9

    iget-object v8, p0, Lz/e;->g:[F

    monitor-enter v8

    :try_start_0
    sget-object v0, Lz/f;->a:[I

    iget-object v1, p0, Lz/e;->n:Lz/g;

    invoke-virtual {v1}, Lz/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unimplemented projection type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lz/e;->n:Lz/g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lz/e;->g:[F

    invoke-virtual {p0, v0, p2, p3}, Lz/e;->a([FII)V

    :goto_0
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/e;->h:Z

    invoke-virtual {p0}, Lz/e;->f()V

    monitor-exit v8

    return-void

    :pswitch_2
    iget-object v1, p0, Lz/e;->g:[F

    const/4 v2, 0x0

    int-to-float v3, p2

    const/4 v4, 0x0

    int-to-float v5, p3

    iget v6, p0, Lz/e;->k:F

    iget v7, p0, Lz/e;->l:F

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lz/e;->a([FFFFFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a([FFFFFFF)V
    .locals 4

    const/high16 v2, 0x40000000

    const/4 v3, 0x0

    cmpl-float v0, p2, p3

    if-eqz v0, :cond_0

    cmpl-float v0, p5, p4

    if-eqz v0, :cond_0

    cmpl-float v0, p6, p7

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    sub-float v1, p3, p2

    div-float v1, v2, v1

    aput v1, p1, v0

    const/4 v0, 0x1

    aput v3, p1, v0

    const/4 v0, 0x2

    aput v3, p1, v0

    const/4 v0, 0x3

    aput v3, p1, v0

    const/4 v0, 0x4

    aput v3, p1, v0

    const/4 v0, 0x5

    sub-float v1, p5, p4

    div-float v1, v2, v1

    aput v1, p1, v0

    const/4 v0, 0x6

    aput v3, p1, v0

    const/4 v0, 0x7

    aput v3, p1, v0

    const/16 v0, 0x8

    aput v3, p1, v0

    const/16 v0, 0x9

    aput v3, p1, v0

    const/16 v0, 0xa

    const/high16 v1, -0x40000000

    sub-float v2, p7, p6

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xb

    aput v3, p1, v0

    const/16 v0, 0xc

    add-float v1, p3, p2

    neg-float v1, v1

    sub-float v2, p3, p2

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xd

    add-float v1, p5, p4

    neg-float v1, v1

    sub-float v2, p5, p4

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xe

    add-float v1, p7, p6

    neg-float v1, v1

    sub-float v2, p7, p6

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xf

    const/high16 v1, 0x3f800000

    aput v1, p1, v0

    goto :goto_0
.end method

.method public a([FII)V
    .locals 9

    const/high16 v8, 0x40000000

    const/4 v7, 0x0

    if-nez p3, :cond_0

    const/high16 v0, 0x3f800000

    :goto_0
    iget v1, p0, Lz/e;->k:F

    iget v2, p0, Lz/e;->m:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    neg-float v2, v1

    neg-float v3, v1

    div-float/2addr v3, v0

    div-float v0, v1, v0

    const/4 v4, 0x0

    iget v5, p0, Lz/e;->k:F

    mul-float/2addr v5, v8

    sub-float v6, v1, v2

    div-float/2addr v5, v6

    aput v5, p1, v4

    const/4 v4, 0x1

    aput v7, p1, v4

    const/4 v4, 0x2

    aput v7, p1, v4

    const/4 v4, 0x3

    aput v7, p1, v4

    const/4 v4, 0x4

    aput v7, p1, v4

    const/4 v4, 0x5

    iget v5, p0, Lz/e;->k:F

    mul-float/2addr v5, v8

    sub-float v6, v0, v3

    div-float/2addr v5, v6

    aput v5, p1, v4

    const/4 v4, 0x6

    aput v7, p1, v4

    const/4 v4, 0x7

    aput v7, p1, v4

    const/16 v4, 0x8

    add-float v5, v1, v2

    sub-float/2addr v1, v2

    div-float v1, v5, v1

    aput v1, p1, v4

    const/16 v1, 0x9

    add-float v2, v0, v3

    sub-float/2addr v0, v3

    div-float v0, v2, v0

    aput v0, p1, v1

    const/16 v0, 0xa

    iget v1, p0, Lz/e;->l:F

    iget v2, p0, Lz/e;->k:F

    add-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lz/e;->l:F

    iget v3, p0, Lz/e;->k:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xb

    const/high16 v1, -0x40800000

    aput v1, p1, v0

    const/16 v0, 0xc

    aput v7, p1, v0

    const/16 v0, 0xd

    aput v7, p1, v0

    const/16 v0, 0xe

    iget v1, p0, Lz/e;->l:F

    mul-float/2addr v1, v8

    iget v2, p0, Lz/e;->k:F

    mul-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lz/e;->l:F

    iget v3, p0, Lz/e;->k:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    const/16 v0, 0xf

    aput v7, p1, v0

    return-void

    :cond_0
    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    goto/16 :goto_0
.end method

.method a(Lz/k;Lz/j;)Z
    .locals 3

    iget-boolean v0, p2, Lz/j;->e:Z

    iget-boolean v1, p0, Lz/e;->j:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p2, Lz/j;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/e;->j:Z

    iget-boolean v0, p0, Lz/e;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p0}, Lz/A;->a(Lz/e;)V

    iget-object v0, p0, Lz/e;->c:Lz/A;

    iget-object v1, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v1}, Lz/A;->b()I

    move-result v1

    iget-object v2, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v2}, Lz/A;->c()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lz/e;->a(Lz/A;II)V

    :goto_1
    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p1, p2}, Lz/A;->a(Lz/k;Lz/j;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p0}, Lz/A;->b(Lz/e;)V

    goto :goto_1
.end method

.method public b()Lz/A;
    .locals 1

    iget-object v0, p0, Lz/e;->c:Lz/A;

    return-object v0
.end method

.method c()V
    .locals 6

    iget-object v1, p0, Lz/e;->g:[F

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lz/e;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lz/e;->g:[F

    const/4 v2, 0x0

    iget-object v3, p0, Lz/e;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0}, Lz/e;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz/e;->h:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method d()V
    .locals 0

    return-void
.end method

.method public e()B
    .locals 1

    iget-byte v0, p0, Lz/e;->i:B

    return v0
.end method

.method f()V
    .locals 3

    iget-object v0, p0, Lz/e;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/y;

    iget-object v2, p0, Lz/e;->g:[F

    invoke-interface {v0, p0, v2}, Lz/y;->a(Lz/e;[F)V

    goto :goto_0

    :cond_0
    return-void
.end method
