.class public Lz/r;
.super Lz/i;
.source "SourceFile"


# static fields
.field private static final m:Lz/L;


# instance fields
.field final g:I

.field final h:I

.field final i:I

.field final j:Lo/T;

.field private k:[F

.field private final l:I

.field private n:F

.field private o:[F

.field private p:Z

.field private q:F

.field private final r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lz/L;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v2, v2, v1}, Lz/L;-><init>(FFF)V

    sput-object v0, Lz/r;->m:Lz/L;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Z)V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Lz/i;-><init>()V

    new-array v0, v2, [F

    iput-object v0, p0, Lz/r;->k:[F

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lz/r;->j:Lo/T;

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lz/r;->o:[F

    iput v1, p0, Lz/r;->i:I

    iput v1, p0, Lz/r;->h:I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v0

    iput v0, p0, Lz/r;->g:I

    new-instance v0, Lz/K;

    invoke-direct {v0}, Lz/K;-><init>()V

    iput-object v0, p0, Lz/r;->a:Lz/K;

    sget-object v0, Lz/s;->a:[I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/E;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x10

    iput v0, p0, Lz/r;->l:I

    :goto_0
    iput-boolean p2, p0, Lz/r;->r:Z

    return-void

    :pswitch_0
    iput v2, p0, Lz/r;->l:I

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x8

    iput v0, p0, Lz/r;->l:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(F)V
    .locals 1

    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iput p1, p0, Lz/r;->q:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/r;->b:Z

    return-void
.end method

.method public a(Lo/T;F)V
    .locals 0

    invoke-virtual {p0, p1, p2, p2, p2}, Lz/r;->a(Lo/T;FFF)V

    return-void
.end method

.method public a(Lo/T;FFF)V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iget-object v0, p0, Lz/r;->j:Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    iget-object v0, p0, Lz/r;->o:[F

    const/4 v1, 0x0

    aput p2, v0, v1

    iget-object v0, p0, Lz/r;->o:[F

    aput p3, v0, v2

    iget-object v0, p0, Lz/r;->o:[F

    const/4 v1, 0x2

    aput p4, v0, v1

    iput-boolean v2, p0, Lz/r;->p:Z

    iput-boolean v2, p0, Lz/r;->b:Z

    return-void
.end method

.method public a(Lz/K;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lz/M;)V
    .locals 1

    iget v0, p0, Lz/r;->l:I

    invoke-super {p0, p1, v0}, Lz/i;->a(Lz/M;I)V

    return-void
.end method

.method public a(Lz/o;)V
    .locals 1

    iget v0, p0, Lz/r;->l:I

    invoke-super {p0, p1, v0}, Lz/i;->a(Lz/o;I)V

    return-void
.end method

.method public b(Lo/T;F)V
    .locals 1

    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iget-object v0, p0, Lz/r;->j:Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    iput p2, p0, Lz/r;->n:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz/r;->p:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/r;->b:Z

    return-void
.end method
