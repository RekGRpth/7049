.class public Lcom/android/galaxy4/GalaxyRS;
.super Ljava/lang/Object;
.source "GalaxyRS.java"


# static fields
.field public static final BG_STAR_COUNT:I = 0x2af8

.field public static final SPACE_CLOUDSTAR_COUNT:I = 0x19

.field public static final STATIC_STAR_COUNT:I = 0x32


# instance fields
.field private mBgAllocation:Landroid/renderscript/Allocation;

.field private mBgStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mBgStarsMesh:Landroid/renderscript/Mesh;

.field private mCloudAllocation:Landroid/renderscript/Allocation;

.field private mDensityDPI:I

.field private mHeight:I

.field private mInited:Z

.field private final mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

.field private mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

.field private mRS:Landroid/renderscript/RenderScriptGL;

.field private mRes:Landroid/content/res/Resources;

.field private mScript:Lcom/android/galaxy4/ScriptC_galaxy;

.field private mSpaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

.field private mSpaceCloudsMesh:Landroid/renderscript/Mesh;

.field private mStaticStar2Allocation:Landroid/renderscript/Allocation;

.field private mStaticStarAllocation:Landroid/renderscript/Allocation;

.field private mStaticStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mStaticStarsMesh:Landroid/renderscript/Mesh;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/galaxy4/GalaxyRS;->mInited:Z

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    return-void
.end method

.method private createProgramFragment()V
    .locals 9

    const/4 v8, 0x0

    new-instance v4, Landroid/renderscript/Sampler$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v6}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v6, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v6}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v6, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v6}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v6, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v6}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v6, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v6}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    invoke-virtual {v4}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v5

    new-instance v1, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v6}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    new-instance v1, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v6}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v6, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v7, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGB:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v1, v6, v7, v8}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v1}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v3

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v3}, Lcom/android/galaxy4/ScriptC_galaxy;->set_fragBg(Landroid/renderscript/ProgramFragment;)V

    invoke-virtual {v3, v5, v8}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f050003

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    sget-object v6, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v2

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v6}, Landroid/renderscript/Sampler;->CLAMP_LINEAR(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v6

    invoke-virtual {v2, v6, v8}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v2}, Lcom/android/galaxy4/ScriptC_galaxy;->set_fragSpaceClouds(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const/high16 v7, 0x7f050000

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v2

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v2}, Lcom/android/galaxy4/ScriptC_galaxy;->set_fragBgStars(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f050005

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    sget-object v6, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    sget-object v6, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v2

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v2}, Lcom/android/galaxy4/ScriptC_galaxy;->set_fragStaticStars(Landroid/renderscript/ProgramFragment;)V

    return-void
.end method

.method private createProgramFragmentStore()V
    .locals 3

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v1}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScriptGL;->bindProgramStore(Landroid/renderscript/ProgramStore;)V

    return-void
.end method

.method private createProgramRaster()V
    .locals 3

    new-instance v0, Landroid/renderscript/ProgramRaster$Builder;

    iget-object v2, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v2}, Landroid/renderscript/ProgramRaster$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/renderscript/ProgramRaster$Builder;->setPointSpriteEnabled(Z)Landroid/renderscript/ProgramRaster$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramRaster$Builder;->create()Landroid/renderscript/ProgramRaster;

    move-result-object v1

    iget-object v2, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v2, v1}, Landroid/renderscript/RenderScriptGL;->bindProgramRaster(Landroid/renderscript/ProgramRaster;)V

    return-void
.end method

.method private getProjectionNormalized(II)Landroid/renderscript/Matrix4f;
    .locals 11
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    new-instance v10, Landroid/renderscript/Matrix4f;

    invoke-direct {v10}, Landroid/renderscript/Matrix4f;-><init>()V

    if-le p1, p2, :cond_0

    int-to-float v1, p1

    int-to-float v3, p2

    div-float v2, v1, v3

    neg-float v1, v2

    const/high16 v3, -0x40800000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/high16 v6, 0x42c80000

    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    :goto_0
    const/high16 v1, 0x43340000

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    invoke-virtual {v10, v1, v3, v4, v5}, Landroid/renderscript/Matrix4f;->loadRotate(FFFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    const/high16 v1, -0x40800000

    const/high16 v3, 0x3f800000

    const/high16 v4, 0x3f800000

    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadScale(FFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadTranslate(FFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    return-object v0

    :cond_0
    int-to-float v1, p2

    int-to-float v3, p1

    div-float v2, v1, v3

    const/high16 v4, -0x40800000

    const/high16 v5, 0x3f800000

    neg-float v6, v2

    const/high16 v8, 0x3f800000

    const/high16 v9, 0x42c80000

    move-object v3, v0

    move v7, v2

    invoke-virtual/range {v3 .. v9}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    goto :goto_0
.end method

.method private loadTexture(I)Landroid/renderscript/Allocation;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    invoke-static {v1, v2, p1}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v0

    return-object v0
.end method

.method private loadTextures()V
    .locals 2

    const v0, 0x7f020004

    invoke-direct {p0, v0}, Lcom/android/galaxy4/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStarAllocation:Landroid/renderscript/Allocation;

    const v0, 0x7f020005

    invoke-direct {p0, v0}, Lcom/android/galaxy4/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStar2Allocation:Landroid/renderscript/Allocation;

    const v0, 0x7f020002

    invoke-direct {p0, v0}, Lcom/android/galaxy4/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mCloudAllocation:Landroid/renderscript/Allocation;

    const/high16 v0, 0x7f020000

    invoke-direct {p0, v0}, Lcom/android/galaxy4/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mBgAllocation:Landroid/renderscript/Allocation;

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mCloudAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->set_textureSpaceCloud(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStarAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->set_textureStaticStar(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStar2Allocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->set_textureStaticStar2(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mBgAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->set_textureBg(Landroid/renderscript/Allocation;)V

    return-void
.end method

.method private updateProjectionMatrices(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iput p2, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    new-instance v1, Landroid/renderscript/Matrix4f;

    invoke-direct {v1}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v3, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iget v4, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    invoke-virtual {v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadOrthoWindow(II)V

    iget v3, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iget v4, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    invoke-direct {p0, v3, v4}, Lcom/android/galaxy4/GalaxyRS;->getProjectionNormalized(II)Landroid/renderscript/Matrix4f;

    move-result-object v2

    new-instance v0, Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    invoke-direct {v0}, Lcom/android/galaxy4/ScriptField_VpConsts$Item;-><init>()V

    iput-object v2, v0, Lcom/android/galaxy4/ScriptField_VpConsts$Item;->MVP:Landroid/renderscript/Matrix4f;

    iget v3, p0, Lcom/android/galaxy4/GalaxyRS;->mDensityDPI:I

    int-to-float v3, v3

    const/high16 v4, 0x43700000

    div-float/2addr v3, v4

    iput v3, v0, Lcom/android/galaxy4/ScriptField_VpConsts$Item;->scaleSize:F

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v0, v4, v5}, Lcom/android/galaxy4/ScriptField_VpConsts;->set(Lcom/android/galaxy4/ScriptField_VpConsts$Item;IZ)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v3}, Lcom/android/galaxy4/ScriptC_galaxy;->invoke_positionParticles()V

    return-void
.end method


# virtual methods
.method public createProgramVertex()V
    .locals 9

    const/4 v8, 0x0

    new-instance v1, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    new-instance v2, Landroid/renderscript/Matrix4f;

    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v6, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iget v7, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    invoke-virtual {v2, v6, v7}, Landroid/renderscript/Matrix4f;->loadOrthoWindow(II)V

    invoke-virtual {v1, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    new-instance v4, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v4}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Landroid/renderscript/ProgramVertexFixedFunction;

    invoke-virtual {v6, v1}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v3}, Lcom/android/galaxy4/ScriptC_galaxy;->set_vertBg(Landroid/renderscript/ProgramVertex;)V

    iget v6, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iget v7, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    invoke-direct {p0, v6, v7}, Lcom/android/galaxy4/GalaxyRS;->updateProjectionMatrices(II)V

    new-instance v0, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f050004

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceCloudsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v6, v8}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v5

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Landroid/renderscript/Program;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v6, v5}, Landroid/renderscript/RenderScriptGL;->bindProgramVertex(Landroid/renderscript/ProgramVertex;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v5}, Lcom/android/galaxy4/ScriptC_galaxy;->set_vertSpaceClouds(Landroid/renderscript/ProgramVertex;)V

    new-instance v0, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f050001

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStarsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v6, v8}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v5

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Landroid/renderscript/Program;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v6, v5}, Landroid/renderscript/RenderScriptGL;->bindProgramVertex(Landroid/renderscript/ProgramVertex;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v5}, Lcom/android/galaxy4/ScriptC_galaxy;->set_vertBgStars(Landroid/renderscript/ProgramVertex;)V

    new-instance v0, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f050006

    invoke-virtual {v0, v6, v7}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStarsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v6, v8}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v5

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Landroid/renderscript/Program;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v6, v5}, Landroid/renderscript/RenderScriptGL;->bindProgramVertex(Landroid/renderscript/ProgramVertex;)V

    iget-object v6, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v6, v5}, Lcom/android/galaxy4/ScriptC_galaxy;->set_vertStaticStars(Landroid/renderscript/ProgramVertex;)V

    return-void
.end method

.method public init(ILandroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;II)V
    .locals 8
    .param p1    # I
    .param p2    # Landroid/renderscript/RenderScriptGL;
    .param p3    # Landroid/content/res/Resources;
    .param p4    # I
    .param p5    # I

    const/4 v7, 0x1

    iget-boolean v3, p0, Lcom/android/galaxy4/GalaxyRS;->mInited:Z

    if-nez v3, :cond_0

    iput p1, p0, Lcom/android/galaxy4/GalaxyRS;->mDensityDPI:I

    iput-object p2, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iput-object p3, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    iput p4, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iput p5, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x0

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    new-instance v3, Lcom/android/galaxy4/ScriptField_Particle;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v5, 0x19

    invoke-direct {v3, v4, v5}, Lcom/android/galaxy4/ScriptField_Particle;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    new-instance v0, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    sget-object v3, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v0}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v3

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceCloudsMesh:Landroid/renderscript/Mesh;

    new-instance v3, Lcom/android/galaxy4/ScriptField_Particle;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v5, 0x2af8

    invoke-direct {v3, v4, v5}, Lcom/android/galaxy4/ScriptField_Particle;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStars:Lcom/android/galaxy4/ScriptField_Particle;

    new-instance v1, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v3}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStars:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    sget-object v3, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v1, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v1}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v3

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStarsMesh:Landroid/renderscript/Mesh;

    new-instance v3, Lcom/android/galaxy4/ScriptField_Particle;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v5, 0x32

    invoke-direct {v3, v4, v5}, Lcom/android/galaxy4/ScriptField_Particle;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStars:Lcom/android/galaxy4/ScriptField_Particle;

    new-instance v2, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v2, v3}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStars:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    sget-object v3, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v2, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v2}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v3

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStarsMesh:Landroid/renderscript/Mesh;

    new-instance v3, Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v5, p0, Lcom/android/galaxy4/GalaxyRS;->mRes:Landroid/content/res/Resources;

    const v6, 0x7f050002

    invoke-direct {v3, v4, v5, v6}, Lcom/android/galaxy4/ScriptC_galaxy;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceCloudsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->set_spaceCloudsMesh(Landroid/renderscript/Mesh;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mSpaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->bind_spaceClouds(Lcom/android/galaxy4/ScriptField_Particle;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStarsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->set_bgStarsMesh(Landroid/renderscript/Mesh;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mBgStars:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->bind_bgStars(Lcom/android/galaxy4/ScriptField_Particle;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStarsMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->set_staticStarsMesh(Landroid/renderscript/Mesh;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mStaticStars:Lcom/android/galaxy4/ScriptField_Particle;

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->bind_staticStars(Lcom/android/galaxy4/ScriptField_Particle;)V

    new-instance v3, Lcom/android/galaxy4/ScriptField_VpConsts;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v3, v4, v7}, Lcom/android/galaxy4/ScriptField_VpConsts;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mPvConsts:Lcom/android/galaxy4/ScriptField_VpConsts;

    invoke-virtual {p0}, Lcom/android/galaxy4/GalaxyRS;->createProgramVertex()V

    invoke-direct {p0}, Lcom/android/galaxy4/GalaxyRS;->createProgramRaster()V

    invoke-direct {p0}, Lcom/android/galaxy4/GalaxyRS;->createProgramFragmentStore()V

    invoke-direct {p0}, Lcom/android/galaxy4/GalaxyRS;->createProgramFragment()V

    invoke-direct {p0}, Lcom/android/galaxy4/GalaxyRS;->loadTextures()V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    iget v4, p0, Lcom/android/galaxy4/GalaxyRS;->mDensityDPI:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/android/galaxy4/ScriptC_galaxy;->set_densityDPI(F)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v4, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v3, v4}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    iget-object v3, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v3}, Lcom/android/galaxy4/ScriptC_galaxy;->invoke_positionParticles()V

    iput-boolean v7, p0, Lcom/android/galaxy4/GalaxyRS;->mInited:Z

    :cond_0
    return-void
.end method

.method public resize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/galaxy4/GalaxyRS;->mWidth:I

    iput p2, p0, Lcom/android/galaxy4/GalaxyRS;->mHeight:I

    invoke-virtual {p0}, Lcom/android/galaxy4/GalaxyRS;->createProgramVertex()V

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/galaxy4/GalaxyRS;->mScript:Lcom/android/galaxy4/ScriptC_galaxy;

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/android/galaxy4/GalaxyRS;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    return-void
.end method
