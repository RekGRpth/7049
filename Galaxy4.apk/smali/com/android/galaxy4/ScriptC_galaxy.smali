.class public Lcom/android/galaxy4/ScriptC_galaxy;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_galaxy.java"


# static fields
.field private static final mExportFuncIdx_positionParticles:I = 0x0

.field private static final mExportVarIdx_bgStars:I = 0x2

.field private static final mExportVarIdx_bgStarsMesh:I = 0x5

.field private static final mExportVarIdx_densityDPI:I = 0x13

.field private static final mExportVarIdx_fragBg:I = 0xe

.field private static final mExportVarIdx_fragBgStars:I = 0xb

.field private static final mExportVarIdx_fragSpaceClouds:I = 0xa

.field private static final mExportVarIdx_fragStaticStars:I = 0xc

.field private static final mExportVarIdx_spaceClouds:I = 0x1

.field private static final mExportVarIdx_spaceCloudsMesh:I = 0x4

.field private static final mExportVarIdx_staticStars:I = 0x3

.field private static final mExportVarIdx_staticStarsMesh:I = 0x6

.field private static final mExportVarIdx_textureBg:I = 0x12

.field private static final mExportVarIdx_textureSpaceCloud:I = 0xf

.field private static final mExportVarIdx_textureStaticStar:I = 0x10

.field private static final mExportVarIdx_textureStaticStar2:I = 0x11

.field private static final mExportVarIdx_vertBg:I = 0xd

.field private static final mExportVarIdx_vertBgStars:I = 0x8

.field private static final mExportVarIdx_vertSpaceClouds:I = 0x7

.field private static final mExportVarIdx_vertStaticStars:I = 0x9

.field private static final mExportVarIdx_vpConstants:I


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_bgStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_bgStarsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_densityDPI:F

.field private mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragBgStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragSpaceClouds:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragStaticStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_spaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_spaceCloudsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_staticStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_staticStarsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_textureBg:Landroid/renderscript/Allocation;

.field private mExportVar_textureSpaceCloud:Landroid/renderscript/Allocation;

.field private mExportVar_textureStaticStar:Landroid/renderscript/Allocation;

.field private mExportVar_textureStaticStar2:Landroid/renderscript/Allocation;

.field private mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertBgStars:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertSpaceClouds:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertStaticStars:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vpConstants:Lcom/android/galaxy4/ScriptField_VpConsts;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__MESH:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__F32:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_bgStars(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x2

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStars:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_spaceClouds(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_staticStars(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x3

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStars:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_vpConstants(Lcom/android/galaxy4/ScriptField_VpConsts;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_VpConsts;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vpConstants:Lcom/android/galaxy4/ScriptField_VpConsts;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_bgStars()Lcom/android/galaxy4/ScriptField_Particle;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStars:Lcom/android/galaxy4/ScriptField_Particle;

    return-object v0
.end method

.method public get_bgStarsMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStarsMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_densityDPI()F
    .locals 1

    iget v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_densityDPI:F

    return v0
.end method

.method public get_fragBg()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_fragBgStars()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBgStars:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_fragSpaceClouds()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragSpaceClouds:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_fragStaticStars()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragStaticStars:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_spaceClouds()Lcom/android/galaxy4/ScriptField_Particle;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    return-object v0
.end method

.method public get_spaceCloudsMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceCloudsMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_staticStars()Lcom/android/galaxy4/ScriptField_Particle;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStars:Lcom/android/galaxy4/ScriptField_Particle;

    return-object v0
.end method

.method public get_staticStarsMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStarsMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_textureBg()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureBg:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_textureSpaceCloud()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureSpaceCloud:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_textureStaticStar()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_textureStaticStar2()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar2:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_vertBg()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_vertBgStars()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBgStars:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_vertSpaceClouds()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertSpaceClouds:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_vertStaticStars()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertStaticStars:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_vpConstants()Lcom/android/galaxy4/ScriptField_VpConsts;
    .locals 1

    iget-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vpConstants:Lcom/android/galaxy4/ScriptField_VpConsts;

    return-object v0
.end method

.method public invoke_positionParticles()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->invoke(I)V

    return-void
.end method

.method public set_bgStarsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStarsMesh:Landroid/renderscript/Mesh;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_densityDPI(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_densityDPI:F

    const/16 v0, 0x13

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_fragBg(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xe

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_fragBgStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBgStars:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xb

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_fragSpaceClouds(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragSpaceClouds:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_fragStaticStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragStaticStars:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0xc

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_spaceCloudsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceCloudsMesh:Landroid/renderscript/Mesh;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_staticStarsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStarsMesh:Landroid/renderscript/Mesh;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_textureBg(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureBg:Landroid/renderscript/Allocation;

    const/16 v0, 0x12

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_textureSpaceCloud(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureSpaceCloud:Landroid/renderscript/Allocation;

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_textureStaticStar(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar:Landroid/renderscript/Allocation;

    const/16 v0, 0x10

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_textureStaticStar2(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar2:Landroid/renderscript/Allocation;

    const/16 v0, 0x11

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_vertBg(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

    const/16 v0, 0xd

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_vertBgStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBgStars:Landroid/renderscript/ProgramVertex;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_vertSpaceClouds(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertSpaceClouds:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_vertStaticStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertStaticStars:Landroid/renderscript/ProgramVertex;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method
