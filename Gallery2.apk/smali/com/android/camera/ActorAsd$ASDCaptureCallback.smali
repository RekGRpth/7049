.class final Lcom/android/camera/ActorAsd$ASDCaptureCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$ASDCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorAsd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ASDCaptureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorAsd;


# direct methods
.method private constructor <init>(Lcom/android/camera/ActorAsd;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/ActorAsd;Lcom/android/camera/ActorAsd$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/ActorAsd;
    .param p2    # Lcom/android/camera/ActorAsd$1;

    invoke-direct {p0, p1}, Lcom/android/camera/ActorAsd$ASDCaptureCallback;-><init>(Lcom/android/camera/ActorAsd;)V

    return-void
.end method


# virtual methods
.method public onDetecte(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const-string v2, "ModeActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDetected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mCurrentASDMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v4}, Lcom/android/camera/ActorAsd;->access$100(Lcom/android/camera/ActorAsd;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-boolean v2, v2, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2}, Lcom/android/camera/ActorAsd;->access$200(Lcom/android/camera/ActorAsd;)Landroid/content/res/TypedArray;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, v3, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/ActorAsd;->access$202(Lcom/android/camera/ActorAsd;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, v3, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08002b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/ActorAsd;->access$302(Lcom/android/camera/ActorAsd;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    :cond_2
    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2}, Lcom/android/camera/ActorAsd;->access$300(Lcom/android/camera/ActorAsd;)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ModeActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Scene mode from ASD: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2}, Lcom/android/camera/ActorAsd;->access$100(Lcom/android/camera/ActorAsd;)I

    move-result v2

    if-eq v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v3, v3, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "pref_camera_scenemode_key"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2, p1}, Lcom/android/camera/ActorAsd;->access$102(Lcom/android/camera/ActorAsd;I)I

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    iget-object v2, v2, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->onSharedPreferenceChanged()V

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2}, Lcom/android/camera/ActorAsd;->access$200(Lcom/android/camera/ActorAsd;)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, p1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-virtual {v2, v0}, Lcom/android/camera/ActorAsd;->updateAsdIndicator(I)V

    iget-object v2, p0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;->this$0:Lcom/android/camera/ActorAsd;

    invoke-static {v2, p1}, Lcom/android/camera/ActorAsd;->access$102(Lcom/android/camera/ActorAsd;I)I

    goto/16 :goto_0
.end method
