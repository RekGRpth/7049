.class Lcom/android/camera/ActorAsd;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ActorAsd$ASDCaptureCallback;
    }
.end annotation


# instance fields
.field private final mASDCaptureCallback:Lcom/android/camera/ActorAsd$ASDCaptureCallback;

.field private mASDDrawableIds:Landroid/content/res/TypedArray;

.field private mASDModes:Landroid/content/res/TypedArray;

.field private mCurrentASDMode:I


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/ActorAsd;->mCurrentASDMode:I

    new-instance v0, Lcom/android/camera/ActorAsd$ASDCaptureCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/ActorAsd$ASDCaptureCallback;-><init>(Lcom/android/camera/ActorAsd;Lcom/android/camera/ActorAsd$1;)V

    iput-object v0, p0, Lcom/android/camera/ActorAsd;->mASDCaptureCallback:Lcom/android/camera/ActorAsd$ASDCaptureCallback;

    return-void
.end method

.method static synthetic access$100(Lcom/android/camera/ActorAsd;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorAsd;

    iget v0, p0, Lcom/android/camera/ActorAsd;->mCurrentASDMode:I

    return v0
.end method

.method static synthetic access$102(Lcom/android/camera/ActorAsd;I)I
    .locals 0
    .param p0    # Lcom/android/camera/ActorAsd;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ActorAsd;->mCurrentASDMode:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/camera/ActorAsd;)Landroid/content/res/TypedArray;
    .locals 1
    .param p0    # Lcom/android/camera/ActorAsd;

    iget-object v0, p0, Lcom/android/camera/ActorAsd;->mASDDrawableIds:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/camera/ActorAsd;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    .locals 0
    .param p0    # Lcom/android/camera/ActorAsd;
    .param p1    # Landroid/content/res/TypedArray;

    iput-object p1, p0, Lcom/android/camera/ActorAsd;->mASDDrawableIds:Landroid/content/res/TypedArray;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/camera/ActorAsd;)Landroid/content/res/TypedArray;
    .locals 1
    .param p0    # Lcom/android/camera/ActorAsd;

    iget-object v0, p0, Lcom/android/camera/ActorAsd;->mASDModes:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/camera/ActorAsd;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    .locals 0
    .param p0    # Lcom/android/camera/ActorAsd;
    .param p1    # Landroid/content/res/TypedArray;

    iput-object p1, p0, Lcom/android/camera/ActorAsd;->mASDModes:Landroid/content/res/TypedArray;

    return-object p1
.end method


# virtual methods
.method public animateCapture()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->animateByIntentOrZSD()V

    return-void
.end method

.method public onPausePre()V
    .locals 3

    invoke-super {p0}, Lcom/android/camera/ModeActor;->onPausePre()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public restoreModeUI(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/ActorAsd;->updateAsdIndicator(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "pref_camera_coloreffect_key"

    aput-object v2, v1, v3

    aput-object v6, v1, v4

    const-string v2, "pref_camera_whitebalance_key"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    aput-object v6, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "pref_camera_scenemode_key"

    aput-object v1, v0, v3

    const-string v1, "auto"

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    return-void
.end method

.method public setCaptureModeSettings()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setWhiteBalance(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "none"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setColorEffect(Ljava/lang/String;)V

    return-void
.end method

.method public updateAsdIndicator(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v1, Lcom/android/camera/ActorAsd$1;

    invoke-direct {v1, p0, p1}, Lcom/android/camera/ActorAsd$1;-><init>(Lcom/android/camera/ActorAsd;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public updateCaptureModeUI(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "pref_camera_coloreffect_key"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getColorEffect()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "pref_camera_whitebalance_key"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getWhiteBalance()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "pref_camera_scenemode_key"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    iget v0, p0, Lcom/android/camera/ActorAsd;->mCurrentASDMode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f020068

    invoke-virtual {p0, v0}, Lcom/android/camera/ActorAsd;->updateAsdIndicator(I)V

    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ActorAsd;->mASDDrawableIds:Landroid/content/res/TypedArray;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActorAsd;->mASDDrawableIds:Landroid/content/res/TypedArray;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActorAsd;->mASDModes:Landroid/content/res/TypedArray;

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ActorAsd;->mASDDrawableIds:Landroid/content/res/TypedArray;

    iget v1, p0, Lcom/android/camera/ActorAsd;->mCurrentASDMode:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/camera/ActorAsd;->updateAsdIndicator(I)V

    goto :goto_0
.end method

.method public updateModePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/ActorAsd;->mASDCaptureCallback:Lcom/android/camera/ActorAsd$ASDCaptureCallback;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    return-void
.end method
