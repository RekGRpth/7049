.class Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;
.super Ljava/lang/Thread;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;


# direct methods
.method constructor <init>(Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v3, v3, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v3}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v3

    new-array v1, v3, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v4, v4, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v4}, Lcom/android/camera/ActorBurst;->access$200(Lcom/android/camera/ActorBurst;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v3, v3, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v3}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v3

    if-ge v2, v3, :cond_1

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v3, v3, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v4, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v4, v4, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v4, v4, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    invoke-virtual {v3, v1, v4}, Lcom/android/camera/ActorBurst;->saveBurstPicture([Ljava/lang/String;Landroid/location/Location;)V

    iget-object v3, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;->this$1:Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    iget-object v3, v3, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v3, v3, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v3, v3, Lcom/android/camera/Camera;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x61

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
