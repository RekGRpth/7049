.class Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorBurst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BurstJpegPictureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorBurst;


# direct methods
.method public constructor <init>(Lcom/android/camera/ActorBurst;Landroid/location/Location;)V
    .locals 0
    .param p2    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p1, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 2
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    iget-object v0, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/ActorBurst;->access$002(Lcom/android/camera/ActorBurst;I)I

    iget-object v0, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/ModeActor;->updateSavingHint(Z)V

    iget-object v0, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->resumePreview()V

    iget-object v0, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    new-instance v0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback$1;-><init>(Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
