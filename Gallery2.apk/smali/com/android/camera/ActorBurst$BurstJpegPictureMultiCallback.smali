.class Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorBurst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BurstJpegPictureMultiCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorBurst;


# direct methods
.method public constructor <init>(Lcom/android/camera/ActorBurst;Landroid/location/Location;)V
    .locals 0
    .param p2    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p1, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 21
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v1}, Lcom/android/camera/ActorBurst;->access$008(Lcom/android/camera/ActorBurst;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v1, Lcom/android/camera/Camera;->mRemainPictureView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mRemainPictureView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v2}, Lcom/android/camera/ActorBurst;->access$000(Lcom/android/camera/ActorBurst;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v2}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getStereo3DType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/Storage;->generateStereoType(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    invoke-virtual {v1}, Lcom/android/camera/Camera$ImageNamer;->getUri()Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    invoke-virtual {v1}, Lcom/android/camera/Camera$ImageNamer;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Lcom/android/camera/Exif;->getOrientation([B)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v5, v2, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    move-object/from16 v0, v20

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, v20

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v2, v2, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget v8, v2, Lcom/android/camera/ActivityBase;->mThumbnailViewWidth:I

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v10}, Lcom/android/camera/Camera$ImageSaver;->addImage([BLandroid/net/Uri;Ljava/lang/String;Landroid/location/Location;IIIII)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v1}, Lcom/android/camera/ActorBurst;->access$000(Lcom/android/camera/ActorBurst;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v2}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v2

    if-ge v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v11, v1, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    move-object/from16 v0, v20

    iget v15, v0, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, v20

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getJpegRotation()I

    move-result v17

    const/16 v18, 0x0

    invoke-virtual/range {v11 .. v18}, Lcom/android/camera/Camera$ImageNamer;->prepareUri(Landroid/content/ContentResolver;JIIII)V

    :cond_0
    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPictureTaken: mCurrentShotsNum = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v5}, Lcom/android/camera/ActorBurst;->access$000(Lcom/android/camera/ActorBurst;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", mBurstShotNum = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v5}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v1}, Lcom/android/camera/ActorBurst;->access$000(Lcom/android/camera/ActorBurst;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    invoke-static {v2}, Lcom/android/camera/ActorBurst;->access$100(Lcom/android/camera/ActorBurst;)I

    move-result v2

    if-lt v1, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->resumePreview()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->setCameraState(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/camera/ActorBurst;->access$002(Lcom/android/camera/ActorBurst;I)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorBurst;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/camera/ModeActor;->updateSavingHint(Z)V

    new-instance v1, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback$1;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback$1;-><init>(Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method
