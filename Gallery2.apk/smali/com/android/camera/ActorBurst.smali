.class Lcom/android/camera/ActorBurst;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;,
        Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;
    }
.end annotation


# instance fields
.field private mBurstPrefix:Ljava/lang/String;

.field private final mBurstShotNum:I

.field private final mCaptureMode:Ljava/lang/String;

.field private mCurrentShotsNum:I

.field private mIsBurstMutiCallBack:Z


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;
    .param p7    # I

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ActorBurst;->mIsBurstMutiCallBack:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    const-string v0, "burstshot"

    iput-object v0, p0, Lcom/android/camera/ActorBurst;->mCaptureMode:Ljava/lang/String;

    const-string v0, "burst"

    iput-object v0, p0, Lcom/android/camera/ActorBurst;->mBurstPrefix:Ljava/lang/String;

    iput p7, p0, Lcom/android/camera/ActorBurst;->mBurstShotNum:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ActorBurst;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorBurst;

    iget v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$002(Lcom/android/camera/ActorBurst;I)I
    .locals 0
    .param p0    # Lcom/android/camera/ActorBurst;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    return p1
.end method

.method static synthetic access$008(Lcom/android/camera/ActorBurst;)I
    .locals 2
    .param p0    # Lcom/android/camera/ActorBurst;

    iget v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/ActorBurst;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorBurst;

    iget v0, p0, Lcom/android/camera/ActorBurst;->mBurstShotNum:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/ActorBurst;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/camera/ActorBurst;

    iget-object v0, p0, Lcom/android/camera/ActorBurst;->mBurstPrefix:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public canShot()Z
    .locals 4

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getRemainPictures()J

    move-result-wide v0

    iget v2, p0, Lcom/android/camera/ActorBurst;->mBurstShotNum:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeRemaining(I)I
    .locals 0
    .param p1    # I

    return p1
.end method

.method public ensureCaptureTempPath()V
    .locals 0

    return-void
.end method

.method public getCaptureTempPath()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ActorBurst;->mBurstPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPictureCallback(Landroid/location/Location;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p1    # Landroid/location/Location;

    iget-boolean v0, p0, Lcom/android/camera/ActorBurst;->mIsBurstMutiCallBack:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/ActorBurst$BurstJpegPictureMultiCallback;-><init>(Lcom/android/camera/ActorBurst;Landroid/location/Location;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/ActorBurst$BurstJpegPictureCallback;-><init>(Lcom/android/camera/ActorBurst;Landroid/location/Location;)V

    goto :goto_0
.end method

.method public isBurstShotInternal()Z
    .locals 2

    iget v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    iget v1, p0, Lcom/android/camera/ActorBurst;->mBurstShotNum:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPausePre()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/ModeActor;->onPausePre()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ActorBurst;->mCurrentShotsNum:I

    return-void
.end method

.method public onShutter()V
    .locals 0

    return-void
.end method

.method public onShutterButtonLongPressed()V
    .locals 0

    return-void
.end method

.method protected saveBurstPicture([Ljava/lang/String;Landroid/location/Location;)V
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # Landroid/location/Location;

    invoke-virtual {p0, p1, p2}, Lcom/android/camera/ModeActor;->saveBulkPictures([Ljava/lang/String;Landroid/location/Location;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected setCaptureMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "burstshot"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v1, p0, Lcom/android/camera/ActorBurst;->mBurstShotNum:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setBurstShotNum(I)V

    return-void
.end method

.method public updateModePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    return-void
.end method
