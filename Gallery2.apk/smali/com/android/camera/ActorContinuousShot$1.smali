.class Lcom/android/camera/ActorContinuousShot$1;
.super Ljava/lang/Thread;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/ActorContinuousShot;->onConinuousShotDone(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mImageNamer:Lcom/android/camera/Camera$ImageNamer;

.field private mImageSaver:Lcom/android/camera/Camera$ImageSaver;

.field final synthetic this$0:Lcom/android/camera/ActorContinuousShot;


# direct methods
.method constructor <init>(Lcom/android/camera/ActorContinuousShot;)V
    .locals 1

    iput-object p1, p0, Lcom/android/camera/ActorContinuousShot$1;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    iput-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    iput-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageSaver;->waitDoneInSubThread()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$1;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageNamer;->cleanOldUriAndRequestSync()V

    return-void
.end method
