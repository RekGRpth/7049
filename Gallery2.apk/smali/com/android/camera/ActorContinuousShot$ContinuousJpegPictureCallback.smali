.class Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorContinuousShot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ContinuousJpegPictureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorContinuousShot;


# direct methods
.method public constructor <init>(Lcom/android/camera/ActorContinuousShot;Landroid/location/Location;)V
    .locals 0
    .param p2    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p1, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 12
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v9, 0x0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-virtual {v0, v9}, Lcom/android/camera/ActorContinuousShot;->onShutterButtonFocus(Z)V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0, v11}, Lcom/android/camera/ActorContinuousShot;->access$102(Lcom/android/camera/ActorContinuousShot;Z)Z

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPictureTaken("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") stop shot!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Continuous Shot, onPictureTaken: mCurrentShotsNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v2}, Lcom/android/camera/ActorContinuousShot;->access$200(Lcom/android/camera/ActorContinuousShot;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mContinuousShotPerformed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v2}, Lcom/android/camera/ActorContinuousShot;->access$500(Lcom/android/camera/ActorContinuousShot;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v10

    invoke-static {p1}, Lcom/android/camera/Exif;->getOrientation([B)I

    move-result v8

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    iget-object v1, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v4, v1, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    iget v5, v10, Landroid/hardware/Camera$Size;->width:I

    iget v6, v10, Landroid/hardware/Camera$Size;->height:I

    iget-object v1, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget v7, v1, Lcom/android/camera/ActivityBase;->mThumbnailViewWidth:I

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v9}, Lcom/android/camera/Camera$ImageSaver;->addImage([BLandroid/net/Uri;Ljava/lang/String;Landroid/location/Location;IIIII)V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0}, Lcom/android/camera/ActorContinuousShot;->access$208(Lcom/android/camera/ActorContinuousShot;)I

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0}, Lcom/android/camera/ActorContinuousShot;->access$300(Lcom/android/camera/ActorContinuousShot;)Lcom/android/camera/ActorContinuousShot$MemoryManager;

    move-result-object v0

    array-length v1, p1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/ActorContinuousShot$MemoryManager;->refresh(J)V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0}, Lcom/android/camera/ActorContinuousShot;->access$200(Lcom/android/camera/ActorContinuousShot;)I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v1}, Lcom/android/camera/ActorContinuousShot;->access$400(Lcom/android/camera/ActorContinuousShot;)I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0}, Lcom/android/camera/ActorContinuousShot;->access$300(Lcom/android/camera/ActorContinuousShot;)Lcom/android/camera/ActorContinuousShot$MemoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ActorContinuousShot$MemoryManager;->isNeedStopCapture()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-virtual {v0, v9}, Lcom/android/camera/ActorContinuousShot;->onShutterButtonFocus(Z)V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0, v11}, Lcom/android/camera/ActorContinuousShot;->access$102(Lcom/android/camera/ActorContinuousShot;Z)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    invoke-static {v0}, Lcom/android/camera/ActorContinuousShot;->access$300(Lcom/android/camera/ActorContinuousShot;)Lcom/android/camera/ActorContinuousShot$MemoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    invoke-virtual {v1}, Lcom/android/camera/Camera$ImageSaver;->getWaitingDataSize()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/ActorContinuousShot$MemoryManager;->isNeedSlowDown(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;->this$0:Lcom/android/camera/ActorContinuousShot;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->slowdownContinuousShot()V

    goto/16 :goto_0
.end method
