.class Lcom/android/camera/ActorContinuousShot;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$ContinuousShotDone;
.implements Landroid/hardware/Camera$ZSDPreviewDone;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ActorContinuousShot$MemoryManager;,
        Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;
    }
.end annotation


# static fields
.field public static final CAPTURE_NORMAL:I = 0x1

.field private static final UPDATE_THUMB_COUNT:I = 0x2


# instance fields
.field private mBurstSound:Landroid/media/SoundPool;

.field private mContinuousShotPerformed:Z

.field private mCurrentShotsNum:I

.field private mIgnoreClick:Z

.field private mMaxCaptureNum:I

.field private mMemoryManager:Lcom/android/camera/ActorContinuousShot$MemoryManager;

.field private mSoundID:I

.field private mStreamID:I

.field private mThumbUpdateCount:I

.field private mWaitSavingDoneThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    const-string v0, "40"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/camera/ActorContinuousShot;->mMaxCaptureNum:I

    iput-boolean v3, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    iput-boolean v3, p0, Lcom/android/camera/ActorContinuousShot;->mIgnoreClick:Z

    iput v3, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    iput v3, p0, Lcom/android/camera/ActorContinuousShot;->mThumbUpdateCount:I

    new-instance v0, Lcom/android/camera/ActorContinuousShot$MemoryManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/ActorContinuousShot$MemoryManager;-><init>(Lcom/android/camera/ActorContinuousShot;Lcom/android/camera/ActorContinuousShot$1;)V

    iput-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mMemoryManager:Lcom/android/camera/ActorContinuousShot$MemoryManager;

    const-string v0, "ModeActor"

    const-string v1, "ActorContinuousShot construction"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ShutterButton$OnShutterButtonListener;)V

    new-instance v0, Landroid/media/SoundPool;

    const/16 v1, 0xa

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mBurstSound:Landroid/media/SoundPool;

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mBurstSound:Landroid/media/SoundPool;

    const-string v1, "/system/media/audio/ui/camera_shutter.ogg"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/camera/ActorContinuousShot;->mSoundID:I

    return-void
.end method

.method static synthetic access$102(Lcom/android/camera/ActorContinuousShot;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ActorContinuousShot;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ActorContinuousShot;->mIgnoreClick:Z

    return p1
.end method

.method static synthetic access$200(Lcom/android/camera/ActorContinuousShot;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorContinuousShot;

    iget v0, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$208(Lcom/android/camera/ActorContinuousShot;)I
    .locals 2
    .param p0    # Lcom/android/camera/ActorContinuousShot;

    iget v0, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/camera/ActorContinuousShot;)Lcom/android/camera/ActorContinuousShot$MemoryManager;
    .locals 1
    .param p0    # Lcom/android/camera/ActorContinuousShot;

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mMemoryManager:Lcom/android/camera/ActorContinuousShot$MemoryManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/camera/ActorContinuousShot;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorContinuousShot;

    iget v0, p0, Lcom/android/camera/ActorContinuousShot;->mMaxCaptureNum:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/camera/ActorContinuousShot;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ActorContinuousShot;

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    return v0
.end method

.method private cancelContinuousShot()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelContinuousShot()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mBurstSound:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mStreamID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    return-void
.end method


# virtual methods
.method public animateCapture()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->animateByIntentOrZSD()V

    :cond_0
    return-void
.end method

.method public applySpecialCapture()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/camera/ModeActor$RenderInCapture;

    invoke-direct {v0, p0}, Lcom/android/camera/ModeActor$RenderInCapture;-><init>(Lcom/android/camera/ModeActor;)V

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    goto :goto_0
.end method

.method public checkMode(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/android/camera/ModeActor;->checkMode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "normal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPictureCallback(Landroid/location/Location;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p1    # Landroid/location/Location;

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/ActorContinuousShot$ContinuousJpegPictureCallback;-><init>(Lcom/android/camera/ActorContinuousShot;Landroid/location/Location;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lowStorage()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/camera/ActorContinuousShot;->onShutterButtonFocus(Z)V

    return-void
.end method

.method public onBurstSaveDone()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->resumePreview()V

    :cond_0
    return-void
.end method

.method public onConinuousShotDone(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onContinuousShotDone, pictures saved = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/camera/ActorContinuousShot$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ActorContinuousShot$1;-><init>(Lcom/android/camera/ActorContinuousShot;)V

    iput-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0, v3, v3}, Lcom/android/camera/ActorContinuousShot;->updateSavingHint(ZZ)V

    return-void
.end method

.method public onPausePre()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput-boolean v4, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPausePre mPaused ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->breakTimer()V

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->interruptRenderThread()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pref_camera_self_timer_key"

    aput-object v1, v0, v3

    const-string v1, "0"

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    invoke-direct {p0}, Lcom/android/camera/ActorContinuousShot;->cancelContinuousShot()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageNamer;->cleanOldUriAndRequestSync()V

    :cond_4
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setCShotDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    goto :goto_0
.end method

.method public onPreviewDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraRotation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->interruptRenderThread()V

    :cond_0
    return-void
.end method

.method public onPreviewStartDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() mPaused = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v4}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/android/camera/ActorContinuousShot;->updateSavingHint(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/camera/ActorContinuousShot;->updateSavingHint(Z)V

    goto :goto_0
.end method

.method public onShutterButtonClick()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mIgnoreClick:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->checkCameraState()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->checkSelfTimerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "normal"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setBurstShotNum(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->doSnap()V

    goto :goto_0
.end method

.method public onShutterButtonFocus(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShutterButtonFocus, pressed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_0

    const-string v0, "ModeActor"

    const-string v1, "Button up Msg received, start to Cancel continuous shot"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    invoke-direct {p0}, Lcom/android/camera/ActorContinuousShot;->cancelContinuousShot()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/android/camera/ActorContinuousShot;->updateSavingHint(ZZ)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    :cond_0
    iput-boolean v3, p0, Lcom/android/camera/ActorContinuousShot;->mIgnoreClick:Z

    return-void
.end method

.method public onShutterButtonLongPressed()V
    .locals 7

    const/4 v4, 0x1

    const/high16 v2, 0x3f800000

    const-string v0, "ModeActor"

    const-string v1, "onShutterButtonLongPressed!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->checkCameraState()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/camera/Util;->clearMemoryLimit()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mMemoryManager:Lcom/android/camera/ActorContinuousShot$MemoryManager;

    invoke-virtual {v0}, Lcom/android/camera/ActorContinuousShot$MemoryManager;->initMemory()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageSaver;->enlargeQueueLimit()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setCShotDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    iput-boolean v4, p0, Lcom/android/camera/ActorContinuousShot;->mContinuousShotPerformed:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "continuousshot"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mMaxCaptureNum:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setBurstShotNum(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->clearFocusOnContinuous()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->doSnap()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mBurstSound:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mSoundID:I

    const/4 v5, -0x1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v0

    iput v0, p0, Lcom/android/camera/ActorContinuousShot;->mStreamID:I

    goto :goto_0
.end method

.method public restoreModeUI(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageSaver:Lcom/android/camera/Camera$ImageSaver;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageSaver;->resetQueueLimit()V

    iget-object v0, p0, Lcom/android/camera/ActorContinuousShot;->mBurstSound:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setCShotDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    :cond_0
    return-void
.end method

.method public setCaptureMode()V
    .locals 4

    invoke-super {p0}, Lcom/android/camera/ModeActor;->setCaptureMode()V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v2, "pref_camera_shot_number"

    const-string v3, "40"

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ActorContinuousShot;->mMaxCaptureNum:I

    return-void
.end method

.method public setCaptureModeSettings()V
    .locals 4

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v2, "pref_camera_shot_number"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setBurstShotNum(I)V

    return-void
.end method

.method public updateSavingHint(Z)V
    .locals 6
    .param p1    # Z

    iget-boolean v1, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSavingHint, saving = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v1, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    if-eqz p1, :cond_2

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c008b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/android/camera/ActorContinuousShot;->mCurrentShotsNum:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/RotateDialogController;->showWaitingDialog(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateSavingHint(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSavingHint, saving = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " shotDone = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/camera/ActorContinuousShot;->updateSavingHint(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/camera/ModeActor;->updateSavingHint(Z)V

    goto :goto_0
.end method

.method public updateShutterListener()V
    .locals 2

    const-string v0, "ModeActor"

    const-string v1, "updateShutterListener in Continuous shot mode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonLongPressListener(Lcom/android/camera/ShutterButton$OnShutterButtonLongPressListener;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ShutterButton$OnShutterButtonListener;)V

    return-void
.end method

.method public updateThumbnailInSaver(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x2

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mThumbUpdateCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/camera/ActorContinuousShot;->mThumbUpdateCount:I

    iget v1, p0, Lcom/android/camera/ActorContinuousShot;->mThumbUpdateCount:I

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/camera/ActorContinuousShot;->mThumbUpdateCount:I

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gt p1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
