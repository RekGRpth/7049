.class Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorEv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EVJpegPictureMultiCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorEv;


# direct methods
.method public constructor <init>(Lcom/android/camera/ActorEv;Landroid/location/Location;)V
    .locals 0
    .param p2    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p1, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const-string v0, "ModeActor"

    const-string v1, "EV picture taken"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    invoke-static {v0}, Lcom/android/camera/ActorEv;->access$108(Lcom/android/camera/ActorEv;)I

    iget-object v0, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    iget-object v1, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    invoke-static {v1}, Lcom/android/camera/ActorEv;->access$100(Lcom/android/camera/ActorEv;)I

    move-result v1

    iget-object v2, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    iget-object v2, v2, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/camera/ActorEv;->saveEvPictureForMultiCallBack([BILandroid/location/Location;)V

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    invoke-static {v1}, Lcom/android/camera/ActorEv;->access$100(Lcom/android/camera/ActorEv;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    invoke-static {v0}, Lcom/android/camera/ActorEv;->access$200(Lcom/android/camera/ActorEv;)V

    iget-object v0, p0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;->this$0:Lcom/android/camera/ActorEv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/ActorEv;->access$102(Lcom/android/camera/ActorEv;I)I

    :cond_0
    return-void
.end method
