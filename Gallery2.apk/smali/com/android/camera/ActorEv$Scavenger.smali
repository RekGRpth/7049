.class public Lcom/android/camera/ActorEv$Scavenger;
.super Ljava/lang/Thread;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorEv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Scavenger"
.end annotation


# instance fields
.field private mAbanbonedImages:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/camera/ActorEv;


# direct methods
.method public constructor <init>(Lcom/android/camera/ActorEv;[Ljava/lang/String;)V
    .locals 5
    .param p2    # [Ljava/lang/String;

    const/4 v4, 0x3

    iput-object p1, p0, Lcom/android/camera/ActorEv$Scavenger;->this$0:Lcom/android/camera/ActorEv;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/camera/ActorEv$Scavenger;->mAbanbonedImages:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v1, p2, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ActorEv$Scavenger;->mAbanbonedImages:[Ljava/lang/String;

    new-instance v2, Ljava/lang/String;

    aget-object v3, p2, v0

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/android/camera/ActorEv$Scavenger;->mAbanbonedImages:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
