.class Lcom/android/camera/ActorEv;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ActorEv$Scavenger;,
        Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;,
        Lcom/android/camera/ActorEv$EVJpegPictureCallback;
    }
.end annotation


# static fields
.field protected static final INTENT_EV_IMG_PREFIX:Ljava/lang/String; = "/icp0"

.field protected static final MAX_EV_NUM:I = 0x3

.field private static final SLEEP_TIME_FOR_SHUTTER_SOUND:I = 0x8c


# instance fields
.field private mCurrentEVNum:I

.field private mEVPaths:[Ljava/lang/String;

.field protected mEVPrefix:Ljava/lang/String;

.field protected mEvImageSelected:[Ljava/lang/String;

.field private mIsEvMutiCallBack:Z

.field private mRestoreSceneMode:Ljava/lang/String;

.field private mUpdateThumbInLaunch:Z


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v1, 0x1

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/android/camera/ActorEv;->mIsEvMutiCallBack:Z

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    const-string v0, "/cap0"

    iput-object v0, p0, Lcom/android/camera/ActorEv;->mEVPrefix:Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/camera/ActorEv;->mUpdateThumbInLaunch:Z

    const-string v0, "pref_camera_scenemode_key"

    const-string v1, "auto"

    invoke-virtual {p5, v0, v1}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActorEv;->mRestoreSceneMode:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ActorEv;)V
    .locals 0
    .param p0    # Lcom/android/camera/ActorEv;

    invoke-direct {p0}, Lcom/android/camera/ActorEv;->fireEvSelector()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/camera/ActorEv;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorEv;

    iget v0, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    return v0
.end method

.method static synthetic access$102(Lcom/android/camera/ActorEv;I)I
    .locals 0
    .param p0    # Lcom/android/camera/ActorEv;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    return p1
.end method

.method static synthetic access$108(Lcom/android/camera/ActorEv;)I
    .locals 2
    .param p0    # Lcom/android/camera/ActorEv;

    iget v0, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/ActorEv;)V
    .locals 0
    .param p0    # Lcom/android/camera/ActorEv;

    invoke-direct {p0}, Lcom/android/camera/ActorEv;->multiCallBackfireEvSelector()V

    return-void
.end method

.method private fireEvSelector()V
    .locals 14

    iget-boolean v12, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v12, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v9, Landroid/content/Intent;

    iget-object v12, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const-class v13, Lcom/android/camera/PicturePicker;

    invoke-direct {v9, v12, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0, v10, v11}, Lcom/android/camera/ModeActor;->createName(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v12, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v12, v12, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-eqz v12, :cond_1

    const-string v12, "/icp0"

    :goto_1
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x3

    new-array v8, v12, [Ljava/lang/String;

    const/4 v4, 0x0

    :goto_2
    const/4 v12, 0x3

    if-ge v4, v12, :cond_2

    new-instance v2, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    aput-object v5, v8, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    iget-object v12, p0, Lcom/android/camera/ActorEv;->mEVPrefix:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v12, "paths"

    invoke-virtual {v7, v12, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v12, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v12, v12, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-eqz v12, :cond_3

    const-string v12, "pictures-to-pick"

    const/4 v13, 0x1

    invoke-virtual {v7, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v9}, Landroid/content/Intent;->getFlags()I

    move-result v12

    const/high16 v13, 0x40000000

    or-int/2addr v12, v13

    invoke-virtual {v9, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v9, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :try_start_0
    const-string v12, "ModeActor"

    const-string v13, "sleep 140ms for shuttersound"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v12, 0x8c

    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v12, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v13, 0x9

    invoke-virtual {v12, v9, v13}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v12, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/android/camera/Camera;->setCameraState(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_3
.end method

.method private multiCallBackfireEvSelector()V
    .locals 5

    iget-boolean v3, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const-class v4, Lcom/android/camera/PicturePicker;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "paths"

    iget-object v4, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v3, v3, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-eqz v3, :cond_1

    const-string v3, "pictures-to-pick"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v3

    const/high16 v4, 0x40000000

    or-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_1
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :try_start_0
    const-string v3, "ModeActor"

    const-string v4, "sleep 140ms for shuttersound"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v3, 0x8c

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v4, 0x9

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private restoreEVPreferenceSettings()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/camera/ActorEv;->mRestoreSceneMode:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    return-void
.end method

.method private writeEVPreferenceSettings()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public canShot()Z
    .locals 4

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getRemainPictures()J

    move-result-wide v0

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCaptureTempPath()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v1, v1, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/icp0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ActorEv;->mEVPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastThumbnail()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/ActorEv;->mUpdateThumbInLaunch:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->getLastThumbnail()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ActorEv;->mUpdateThumbInLaunch:Z

    goto :goto_0
.end method

.method public getPictureCallback(Landroid/location/Location;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p1    # Landroid/location/Location;

    iget-boolean v0, p0, Lcom/android/camera/ActorEv;->mIsEvMutiCallBack:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/ActorEv$EVJpegPictureMultiCallback;-><init>(Lcom/android/camera/ActorEv;Landroid/location/Location;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/camera/ActorEv$EVJpegPictureCallback;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/ActorEv$EVJpegPictureCallback;-><init>(Lcom/android/camera/ActorEv;Landroid/location/Location;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const-string v8, "ModeActor"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onActivityResult: requestCode:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " resultCode:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x9

    if-ne v8, p1, :cond_4

    const/4 v8, -0x1

    if-ne p2, v8, :cond_5

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v8, "ModeActor"

    const-string v9, "onActivityResult, EV_SELECT, extra == null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const-string v8, "paths"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    iget-object v8, p0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    const/4 v9, 0x2

    aget-object v8, v8, v9

    if-eqz v8, :cond_3

    :cond_2
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/camera/ActorEv;->mUpdateThumbInLaunch:Z

    :cond_3
    iget-object v8, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x13

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4
    iget-boolean v8, p0, Lcom/android/camera/ActorEv;->mIsEvMutiCallBack:Z

    if-eqz v8, :cond_0

    const/4 v3, 0x0

    :goto_0
    const/4 v8, 0x3

    if-ge v3, v8, :cond_0

    iget-object v8, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v9, v8, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    iget-boolean v8, p0, Lcom/android/camera/ActorEv;->mIsEvMutiCallBack:Z

    if-eqz v8, :cond_7

    iget-object v0, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v6, v0, v4

    if-eqz v6, :cond_6

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_7
    iget-object v8, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v8, v8, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-eqz v8, :cond_8

    const-string v1, "/icp0"

    :goto_2
    const/4 v3, 0x0

    :goto_3
    const/4 v8, 0x3

    if-ge v3, v8, :cond_4

    new-instance v7, Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/android/camera/ActorEv;->mEVPrefix:Ljava/lang/String;

    goto :goto_2
.end method

.method public onPausePre()V
    .locals 3

    invoke-super {p0}, Lcom/android/camera/ModeActor;->onPausePre()V

    iget-boolean v1, p0, Lcom/android/camera/ActorEv;->mIsEvMutiCallBack:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    if-lez v1, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/camera/ActorEv;->mCurrentEVNum:I

    new-instance v1, Lcom/android/camera/ActorEv$Scavenger;

    iget-object v2, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/android/camera/ActorEv$Scavenger;-><init>(Lcom/android/camera/ActorEv;[Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public prepareUri(Landroid/content/ContentResolver;JIIII)V
    .locals 2
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const-string v0, "ModeActor"

    const-string v1, "EV shot no need to prepareUri in capture"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public restoreModeUI(Z)V
    .locals 4
    .param p1    # Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ActorEv;->restoreEVPreferenceSettings()V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pref_camera_scenemode_key"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    return-void
.end method

.method public saveEVPictures()V
    .locals 30

    const/16 v22, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v19

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    array-length v0, v3

    move/from16 v16, v0

    const/4 v13, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v13, v0, :cond_4

    aget-object v9, v3, v13

    if-nez v9, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_1
    const/16 v26, 0x2f

    move/from16 v0, v26

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v26

    add-int/lit8 v26, v26, 0x1

    const/16 v27, 0x2e

    move/from16 v0, v27

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v26

    if-eqz v26, :cond_0

    const/4 v6, 0x0

    :try_start_0
    new-instance v7, Landroid/media/ExifInterface;

    invoke-direct {v7, v9}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v7

    :goto_2
    invoke-static {v6}, Lcom/android/camera/Util;->getExifOrientation(Landroid/media/ExifInterface;)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/android/camera/Camera;->getJpegRotation()I

    move-result v26

    add-int v26, v26, v17

    move/from16 v0, v26

    rem-int/lit16 v0, v0, 0xb4

    move/from16 v26, v0

    if-nez v26, :cond_3

    move-object/from16 v0, v19

    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v10, v0, Landroid/hardware/Camera$Size;->height:I

    :goto_3
    new-instance v23, Landroid/content/ContentValues;

    const/16 v26, 0xb

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v26, "title"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v26, "_display_name"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ".jpg"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v26, "datetaken"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v26, "mime_type"

    const-string v27, "image/jpeg"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v26, "_data"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v26, "_size"

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v26, "orientation"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v26, "width"

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v26, "height"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v26, "mpo_type"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v26, "stereo_type"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    const-string v26, "latitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/location/Location;->getLatitude()D

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v26, "longitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mLastJpegLoc:Landroid/location/Location;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/location/Location;->getLongitude()D

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/android/camera/Camera;->getCameraResolver()Landroid/content/ContentResolver;

    move-result-object v26

    sget-object v27, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v22

    move-object v14, v9

    goto/16 :goto_1

    :catch_0
    move-exception v5

    const-string v26, "ModeActor"

    const-string v27, "cannot read exif"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_3
    move-object/from16 v0, v19

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v10, v0, Landroid/hardware/Camera$Size;->width:I

    goto/16 :goto_3

    :catch_1
    move-exception v20

    const-string v26, "ModeActor"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Failed to write MediaStore"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_2
    move-exception v4

    const-string v26, "ModeActor"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Failed to write MediaStore"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    const/4 v12, 0x0

    :goto_4
    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v12, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ActorEv;->mEvImageSelected:[Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v27, v26, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_5
    if-eqz v22, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/android/camera/Camera;->getPreviewFrameLayout()Lcom/android/camera/PreviewFrameLayout;

    move-result-object v15

    move-object/from16 v0, v19

    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-virtual {v15}, Landroid/view/View;->getWidth()I

    move-result v28

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v26, v26, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-virtual {v15}, Landroid/view/View;->getHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v26, v26, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-int v11, v0

    move/from16 v0, v25

    invoke-static {v0, v11}, Ljava/lang/Math;->max(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v26, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v22

    invoke-static {v14, v0, v1, v2}, Lcom/android/camera/Thumbnail;->createThumbnail(Ljava/lang/String;IILandroid/net/Uri;)Lcom/android/camera/Thumbnail;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    const-string v26, "ModeActor"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Ev shot update uri = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/android/camera/Util;->broadcastNewPicture(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0xd

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected saveEvPictureForMultiCallBack([BILandroid/location/Location;)V
    .locals 10
    .param p1    # [B
    .param p2    # I
    .param p3    # Landroid/location/Location;

    iget-boolean v7, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/camera/Util;->createJpegName(J)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/camera/Storage;->DIRECTORY:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/camera/ActorEv;->mEVPaths:[Ljava/lang/String;

    add-int/lit8 v8, p2, -0x1

    aput-object v4, v7, v8

    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v6, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v7, "ModeActor"

    const-string v8, "saveEvPictureForMultiCallBack exit"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    goto :goto_0

    :catch_1
    move-exception v2

    :goto_1
    :try_start_3
    const-string v7, "ModeActor"

    const-string v8, "Failed to write image"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    const-string v7, "ModeActor"

    const-string v8, "saveEvPictureForMultiCallBack exit"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v7

    :goto_2
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :goto_3
    throw v7

    :catch_3
    move-exception v2

    const-string v8, "ModeActor"

    const-string v9, "saveEvPictureForMultiCallBack exit"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catchall_1
    move-exception v7

    move-object v5, v6

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v5, v6

    goto :goto_1
.end method

.method public setCaptureModeSettings()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V

    return-void
.end method

.method public updateCaptureModeUI(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    return-void
.end method

.method public updateModePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ActorEv;->writeEVPreferenceSettings()V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    return-void
.end method
