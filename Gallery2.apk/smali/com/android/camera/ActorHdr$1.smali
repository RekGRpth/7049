.class Lcom/android/camera/ActorHdr$1;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/ActorHdr;->restoreModeUI(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorHdr;


# direct methods
.method constructor <init>(Lcom/android/camera/ActorHdr;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ActorHdr$1;->this$0:Lcom/android/camera/ActorHdr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/camera/ActorHdr$1;->this$0:Lcom/android/camera/ActorHdr;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v1, Lcom/android/camera/Camera;->mNormalCaptureIndicatorButton:Lcom/android/camera/ui/ControlBarIndicatorButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/camera/ActorHdr$1;->this$0:Lcom/android/camera/ActorHdr;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showHDRIndicator(Z)V

    return-void
.end method
