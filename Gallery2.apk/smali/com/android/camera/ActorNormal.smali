.class Lcom/android/camera/ActorNormal;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$ZSDPreviewDone;


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public animateCapture()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->animateByIntentOrZSD()V

    return-void
.end method

.method public applySpecialCapture()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/camera/ModeActor$RenderInCapture;

    invoke-direct {v0, p0}, Lcom/android/camera/ModeActor$RenderInCapture;-><init>(Lcom/android/camera/ModeActor;)V

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    goto :goto_0
.end method

.method public computeRemaining(I)I
    .locals 0
    .param p1    # I

    return p1
.end method

.method public onPreviewDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraRotation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->interruptRenderThread()V

    :cond_0
    return-void
.end method

.method public onShutterButtonLongPressed()V
    .locals 5

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_camera_normal_capture_key"

    invoke-virtual {v2, v3}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "Normal"

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getEntry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c00b4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v2, Lcom/android/camera/ui/RotateTextToast;

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getOrientation()I

    move-result v4

    invoke-direct {v2, v3, v1, v4}, Lcom/android/camera/ui/RotateTextToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    invoke-virtual {v2}, Lcom/android/camera/ui/RotateTextToast;->show()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c00b3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public updateModePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    return-void
.end method
