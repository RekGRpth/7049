.class Lcom/android/camera/ActorSingle3DAutorama;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$AUTORAMACallback;
.implements Landroid/hardware/Camera$AUTORAMAMVCallback;


# static fields
.field private static final AUTORAMA_IDLE:I = 0x0

.field private static final AUTORAMA_MERGING:I = 0x2

.field private static final AUTORAMA_STARTED:I = 0x1

.field private static final DIRECTION_DOWN:I = 0x3

.field private static final DIRECTION_LEFT:I = 0x1

.field private static final DIRECTION_RIGHT:I = 0x0

.field private static final DIRECTION_UNKNOWN:I = 0x4

.field private static final DIRECTION_UP:I = 0x2

.field private static final NUM_AUTORAMA_CAPTURE:I = 0x2

.field private static final TARGET_DISTANCE_HORIZONTAL:I = 0x20

.field private static final TARGET_DISTANCE_VERTICAL:I = 0x18


# instance fields
.field private mCenterWindow:Landroid/widget/ImageView;

.field private mCurrentNum:I

.field private mDirection:I

.field private mDisplayOrientaion:I

.field private mLocker:Ljava/lang/Object;

.field private mNaviWindow:Landroid/view/View;

.field private mNormalWindowDrawable:Landroid/graphics/drawable/Drawable;

.field private mOnCancelClickListener:Landroid/view/View$OnClickListener;

.field private mOrientaion:I

.field private mPanoView:Landroid/view/View;

.field private mPaused:Z

.field private mPreviewHeight:I

.field private mPreviewSize:Landroid/hardware/Camera$Size;

.field private mPreviewWidth:I

.field private mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

.field private mShowingCollimatedDrawable:Z

.field private mState:I

.field private mStopProcess:Z

.field private mStopping:Z


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPreviewWidth:I

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPreviewHeight:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mDirection:I

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    const/16 v0, 0x10e

    iput v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mLocker:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopProcess:Z

    iput-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    new-instance v0, Lcom/android/camera/ActorSingle3DAutorama$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ActorSingle3DAutorama$1;-><init>(Lcom/android/camera/ActorSingle3DAutorama;)V

    iput-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOnCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1}, Lcom/android/camera/ActorSingle3DAutorama;->initializeViews(Landroid/app/Activity;)V

    new-instance v0, Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x3

    invoke-direct {v0, p1, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ActorSingle3DAutorama;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/ActorSingle3DAutorama;->doStop(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/camera/ActorSingle3DAutorama;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/camera/ActorSingle3DAutorama;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopping:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/camera/ActorSingle3DAutorama;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNormalWindowDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/ActorSingle3DAutorama;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/ActorSingle3DAutorama;->onHardwareStopped(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/camera/ActorSingle3DAutorama;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mLocker:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/camera/ActorSingle3DAutorama;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopProcess:Z

    return p1
.end method

.method static synthetic access$500(Lcom/android/camera/ActorSingle3DAutorama;)Lcom/mediatek/camera/ui/ProgressIndicator;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/camera/ActorSingle3DAutorama;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/camera/ActorSingle3DAutorama;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/camera/ActorSingle3DAutorama;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/camera/ActorSingle3DAutorama;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/camera/ActorSingle3DAutorama;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    return-object v0
.end method

.method private doStart()V
    .locals 3

    const/4 v2, 0x2

    const-string v0, "ModeActor"

    const-string v1, "doStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v2}, Lcom/android/camera/CameraManager$CameraProxy;->start3DSHOT(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v2}, Lcom/android/camera/CameraManager$CameraProxy;->startAUTORAMA(I)V

    goto :goto_0
.end method

.method private doStop(Z)V
    .locals 6
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "ModeActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doStop isMerge "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraHolder;->isSameCameraDevice(Lcom/android/camera/CameraManager$CameraProxy;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "ModeActor"

    const-string v4, "mCameraDevice.stop3DSHOT()"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/camera/CameraManager$CameraProxy;->stop3DSHOT(I)V

    :goto_1
    monitor-exit v0

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lcom/android/camera/CameraManager$CameraProxy;->stopAUTORAMA(I)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    :try_start_1
    const-string v1, "ModeActor"

    const-string v2, "doStop device is release? "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private drawNaviWindow()V
    .locals 7

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int v0, v2, v3

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int v1, v2, v3

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    add-int/lit8 v3, v1, 0x20

    iget-object v4, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    add-int/lit8 v5, v1, 0x20

    iget-object v6, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    :goto_0
    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    add-int/lit8 v3, v0, 0x20

    add-int/lit8 v4, v0, 0x20

    iget-object v5, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private initializeViews(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const v1, 0x7f0b0085

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    const v1, 0x7f0b00a9

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    const v1, 0x7f0b00a5

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNormalWindowDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private onHardwareStopped(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHardwareStopped isMerge: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, p1}, Lcom/android/camera/Camera;->onAutoramaCaptureDone(Z)V

    return-void
.end method

.method private stopAsync(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopAsync mStopping: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopping:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopping:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopping:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/camera/ActorSingle3DAutorama$2;

    invoke-direct {v1, p0, p1}, Lcom/android/camera/ActorSingle3DAutorama$2;-><init>(Lcom/android/camera/ActorSingle3DAutorama;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mLocker:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopProcess:Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public applySpecialCapture()Z
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOnCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCancelButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->setCapturePath()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    invoke-virtual {p0}, Lcom/android/camera/ActorSingle3DAutorama;->start()Z

    const/4 v0, 0x1

    return v0
.end method

.method public checkStopProcess()V
    .locals 3

    :goto_0
    iget-boolean v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopProcess:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mLocker:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v0, "ModeActor"

    const-string v2, "Stop3DSHOT is running, so wait for it."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mLocker:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    const-string v0, "ModeActor"

    const-string v2, "Stop3DSHOT is done"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public enableFD(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public hasCaptured()Z
    .locals 3

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasCaptured mCurrentNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideSingle3DUI()V
    .locals 4

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/camera/ActorSingle3DAutorama$3;

    invoke-direct {v1, p0}, Lcom/android/camera/ActorSingle3DAutorama$3;-><init>(Lcom/android/camera/ActorSingle3DAutorama;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onCapture()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x2

    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCapture: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    if-eq v1, v5, :cond_2

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    if-ne v1, v5, :cond_3

    :cond_2
    const-string v1, "ModeActor"

    const-string v2, "autorama done"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    invoke-direct {p0, v6}, Lcom/android/camera/ActorSingle3DAutorama;->onHardwareStopped(Z)V

    :goto_1
    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    if-ne v1, v5, :cond_0

    invoke-virtual {p0, v6}, Lcom/android/camera/ActorSingle3DAutorama;->stop(Z)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    if-ge v1, v5, :cond_5

    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    const v2, 0x7f05000d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    iget-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_4
    iput-boolean v6, p0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/camera/ActorSingle3DAutorama$4;

    invoke-direct {v2, p0}, Lcom/android/camera/ActorSingle3DAutorama$4;-><init>(Lcom/android/camera/ActorSingle3DAutorama;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_5
    const-string v1, "ModeActor"

    const-string v2, "onCapture is called in abnormal state"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onFrame(II)V
    .locals 16
    .param p1    # I
    .param p2    # I

    const-string v13, "T"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "xy="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " direction="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPaused:Z

    if-nez v13, :cond_0

    const/4 v13, 0x4

    move/from16 v0, p2

    if-eq v0, v13, :cond_0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    const/4 v14, 0x1

    if-ge v13, v14, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPaused:Z

    if-nez v13, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/android/camera/ActorSingle3DAutorama;->drawNaviWindow()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/high16 v13, -0x10000

    and-int v13, v13, p1

    shr-int/lit8 v13, v13, 0x10

    int-to-short v9, v13

    const v13, 0xffff

    and-int v13, v13, p1

    int-to-short v11, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/view/View;->getPaddingLeft()I

    move-result v14

    add-int v1, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/view/View;->getPaddingTop()I

    move-result v14

    add-int v2, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v13

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v14, v14, Landroid/hardware/Camera$Size;->width:I

    int-to-float v14, v14

    div-float v10, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v13

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget v14, v14, Landroid/hardware/Camera$Size;->height:I

    int-to-float v14, v14

    div-float v12, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mDisplayOrientaion:I

    const/16 v14, 0xb4

    if-ne v13, v14, :cond_c

    const/4 v13, 0x1

    move/from16 v0, p2

    if-eq v0, v13, :cond_3

    if-nez p2, :cond_b

    :cond_3
    rsub-int/lit8 p2, p2, 0x1

    :goto_1
    neg-int v13, v9

    int-to-short v9, v13

    neg-int v13, v11

    int-to-short v11, v13

    :cond_4
    :goto_2
    int-to-float v13, v9

    mul-float/2addr v13, v10

    float-to-int v13, v13

    int-to-short v9, v13

    int-to-float v13, v11

    mul-float/2addr v13, v12

    float-to-int v13, v13

    int-to-short v11, v13

    const/4 v4, 0x0

    const/4 v5, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_3
    const-string v13, "ModeActor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onFrame x = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    int-to-float v15, v9

    div-float/2addr v15, v10

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " y = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    int-to-float v15, v11

    div-float/2addr v15, v12

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " cwx = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " cwy = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " screenPosX = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " screenPosY = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mDisplayOrientaion:I

    const/16 v14, 0x5a

    if-ne v13, v14, :cond_6

    const/4 v13, 0x1

    move/from16 v0, p2

    if-eq v0, v13, :cond_5

    if-nez p2, :cond_d

    :cond_5
    rsub-int/lit8 p2, p2, 0x3

    :goto_4
    move v6, v4

    move v4, v5

    move v5, v6

    move v6, v8

    move v8, v3

    move v3, v6

    :cond_6
    if-gez v4, :cond_7

    const/4 v4, 0x0

    :cond_7
    if-gez v5, :cond_8

    const/4 v5, 0x0

    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v13

    add-int v14, v4, v8

    if-ge v13, v14, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v13

    sub-int v4, v13, v8

    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v13

    add-int v14, v5, v3

    if-ge v13, v14, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v13

    sub-int v5, v13, v3

    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    add-int v14, v4, v8

    add-int v15, v5, v3

    invoke-virtual {v13, v4, v5, v14, v15}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    :cond_b
    rsub-int/lit8 p2, p2, 0x5

    goto/16 :goto_1

    :cond_c
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ActorSingle3DAutorama;->mDisplayOrientaion:I

    const/16 v14, 0x5a

    if-ne v13, v14, :cond_4

    move v6, v10

    move v10, v12

    neg-float v12, v6

    move v7, v1

    move v1, v2

    move v2, v7

    goto/16 :goto_2

    :pswitch_0
    neg-int v13, v9

    add-int/2addr v13, v1

    const/high16 v14, 0x42000000

    mul-float/2addr v14, v10

    float-to-int v14, v14

    add-int v4, v13, v14

    neg-int v13, v11

    add-int v5, v13, v2

    goto/16 :goto_3

    :pswitch_1
    neg-int v13, v9

    add-int/2addr v13, v1

    const/high16 v14, -0x3e000000

    mul-float/2addr v14, v10

    float-to-int v14, v14

    add-int v4, v13, v14

    neg-int v13, v11

    add-int v5, v13, v2

    goto/16 :goto_3

    :pswitch_2
    neg-int v13, v9

    add-int v4, v13, v1

    neg-int v13, v11

    add-int/2addr v13, v2

    const/high16 v14, -0x3e400000

    mul-float/2addr v14, v12

    float-to-int v14, v14

    add-int v5, v13, v14

    goto/16 :goto_3

    :pswitch_3
    neg-int v13, v9

    add-int v4, v13, v1

    neg-int v13, v11

    add-int/2addr v13, v2

    const/high16 v14, 0x41c00000

    mul-float/2addr v14, v12

    float-to-int v14, v14

    add-int v5, v13, v14

    goto/16 :goto_3

    :cond_d
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/camera/ModeActor;->onPause()V

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onPausePre()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPaused:Z

    return-void
.end method

.method public onShutterButtonLongPressed()V
    .locals 0

    return-void
.end method

.method public prepareUri(Landroid/content/ContentResolver;JIIII)V
    .locals 2
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const-string v0, "ModeActor"

    const-string v1, "Single3d shot no need to prepareUri in capture"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public restoreModeUI(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pref_camera_hdr_key"

    aput-object v3, v1, v2

    aput-object v6, v1, v4

    const/4 v2, 0x2

    const-string v3, "pref_camera_flashmode_key"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v6, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v5}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->startFaceDetection()V

    return-void
.end method

.method protected setCaptureMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPreviewSize:Landroid/hardware/Camera$Size;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->stopFaceDetection()V

    return-void
.end method

.method public setCaptureModeSettings()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "off"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDisplayOrientation(I)V
    .locals 3
    .param p1    # I

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOrientation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mDisplayOrientaion:I

    return-void
.end method

.method public setOrientation(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    iget v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    :cond_0
    return-void
.end method

.method public start()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "ModeActor"

    const-string v3, "Start to capture"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v2, v2, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iput-object v2, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mStopping:Z

    if-nez v2, :cond_0

    iput v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    iput v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCurrentNum:I

    iput-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mShowingCollimatedDrawable:Z

    const/4 v2, 0x4

    iput v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mDirection:I

    iput-boolean v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPaused:Z

    invoke-direct {p0}, Lcom/android/camera/ActorSingle3DAutorama;->doStart()V

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mPanoView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mCenterWindow:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v2, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v2, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->showPostSingle3DControlAlert()V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v2, 0x7f0c0096

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showSingle3DGuide(I)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->clearFocusOnContinuous()V

    :goto_0
    return v0

    :cond_0
    const-string v0, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mState:I

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/camera/ActorSingle3DAutorama;->stopAsync(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->onAutoramaMergeStarted()V

    invoke-virtual {p0}, Lcom/android/camera/ActorSingle3DAutorama;->hideSingle3DUI()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v1, 0x7f0c0140

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showSingle3DGuide(I)V

    goto :goto_1
.end method

.method public updateCaptureModeUI(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pref_camera_scenemode_key"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "pref_camera_hdr_key"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "off"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "pref_camera_flashmode_key"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "off"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    return-void
.end method

.method public updateViews(Lcom/android/camera/Camera;)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/ActorSingle3DAutorama;->initializeViews(Landroid/app/Activity;)V

    new-instance v0, Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x3

    invoke-direct {v0, p1, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActorSingle3DAutorama;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v1, p0, Lcom/android/camera/ActorSingle3DAutorama;->mOrientaion:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    return-void
.end method
