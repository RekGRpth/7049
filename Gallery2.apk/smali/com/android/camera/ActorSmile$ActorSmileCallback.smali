.class final Lcom/android/camera/ActorSmile$ActorSmileCallback;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Landroid/hardware/Camera$SmileCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ActorSmile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ActorSmileCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ActorSmile;


# direct methods
.method private constructor <init>(Lcom/android/camera/ActorSmile;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/ActorSmile;Lcom/android/camera/ActorSmile$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/ActorSmile;
    .param p2    # Lcom/android/camera/ActorSmile$1;

    invoke-direct {p0, p1}, Lcom/android/camera/ActorSmile$ActorSmileCallback;-><init>(Lcom/android/camera/ActorSmile;)V

    return-void
.end method


# virtual methods
.method public onSmile()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    invoke-static {v0}, Lcom/android/camera/ActorSmile;->access$100(Lcom/android/camera/ActorSmile;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v0, "ModeActor"

    const-string v1, "Smile callback in error state, please check"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "smile detected, mstat:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    invoke-static {v2}, Lcom/android/camera/ActorSmile;->access$100(Lcom/android/camera/ActorSmile;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    iget-boolean v0, v0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->actorCapture()V

    iget-object v0, p0, Lcom/android/camera/ActorSmile$ActorSmileCallback;->this$0:Lcom/android/camera/ActorSmile;

    invoke-virtual {v0}, Lcom/android/camera/ActorSmile;->stopSmileDetection()V

    :cond_1
    return-void
.end method
