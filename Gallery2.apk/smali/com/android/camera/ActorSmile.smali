.class Lcom/android/camera/ActorSmile;
.super Lcom/android/camera/ModeActor;
.source "ModeActor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ActorSmile$1;,
        Lcom/android/camera/ActorSmile$ActorSmileCallback;
    }
.end annotation


# static fields
.field private static final SMILESHOT_IN_PROGRESS:I = 0x1

.field private static final SMILESHOT_STANDBY:I


# instance fields
.field private final mSmileCallback:Lcom/android/camera/ActorSmile$ActorSmileCallback;

.field private mSmileScenes:[Ljava/lang/String;

.field private mStatus:I


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    invoke-direct/range {p0 .. p6}, Lcom/android/camera/ModeActor;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V

    new-instance v0, Lcom/android/camera/ActorSmile$ActorSmileCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/ActorSmile$ActorSmileCallback;-><init>(Lcom/android/camera/ActorSmile;Lcom/android/camera/ActorSmile$1;)V

    iput-object v0, p0, Lcom/android/camera/ActorSmile;->mSmileCallback:Lcom/android/camera/ActorSmile$ActorSmileCallback;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActorSmile;->mSmileScenes:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/android/camera/ActorSmile;)I
    .locals 1
    .param p0    # Lcom/android/camera/ActorSmile;

    iget v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    return v0
.end method

.method private openSmileShutterMode()V
    .locals 2

    const-string v0, "ModeActor"

    const-string v1, "openSmileShutterMode "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-nez v0, :cond_0

    const-string v0, "ModeActor"

    const-string v1, "CameraDevice is null, ignore"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    iget-object v0, p0, Lcom/android/camera/ActorSmile;->mSmileCallback:Lcom/android/camera/ActorSmile$ActorSmileCallback;

    invoke-virtual {p0, v0}, Lcom/android/camera/ActorSmile;->startSmileDetection(Landroid/hardware/Camera$SmileCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public computeRemaining(I)I
    .locals 0
    .param p1    # I

    return p1
.end method

.method public doCancelCapture()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2, v1}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    if-ne v2, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->stopSmileDetection()V

    move v0, v1

    goto :goto_0

    :cond_1
    iput v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    goto :goto_0
.end method

.method public doSmileShutter()Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doSmileShutter mStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->actorCapture()V

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->stopSmileDetection()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCaptureTempPath()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleSDcardUnmount()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->stopSmileDetection()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    goto :goto_0
.end method

.method public isInShutterProgress()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->doCancelCapture()Z

    return-void
.end method

.method public onPausePre()V
    .locals 2

    iget v0, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->stopSmileDetection()V

    :cond_0
    invoke-super {p0}, Lcom/android/camera/ModeActor;->onPausePre()V

    return-void
.end method

.method public readyToCapture()Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "ModeActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " readyToCapture? mStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    if-eq v1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ActorSmile;->openSmileShutterMode()V

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public restoreModeUI(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/camera/ActorSmile;->doCancelCapture()Z

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->ensureFDState(Z)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const-string v1, "pref_camera_scenemode_key"

    invoke-virtual {v0, v1, v2, v2}, Lcom/android/camera/Camera;->overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method protected setCaptureMode()V
    .locals 2

    invoke-super {p0}, Lcom/android/camera/ModeActor;->setCaptureMode()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->ensureFDState(Z)V

    return-void
.end method

.method public startSmileDetection(Landroid/hardware/Camera$SmileCallback;)V
    .locals 3
    .param p1    # Landroid/hardware/Camera$SmileCallback;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v2, 0x7f0b00f7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, p1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1}, Lcom/android/camera/CameraManager$CameraProxy;->startSDPreview()V

    return-void
.end method

.method public stopSmileDetection()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const v2, 0x7f0b00f7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1}, Lcom/android/camera/CameraManager$CameraProxy;->cancelSDPreview()V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->checkStorage()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/camera/ActorSmile;->mStatus:I

    return-void
.end method

.method public updateCaptureModeUI(Z)V
    .locals 4
    .param p1    # Z

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const-string v2, "pref_camera_scenemode_key"

    iget-object v3, p0, Lcom/android/camera/ActorSmile;->mSmileScenes:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/android/camera/Camera;->overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    return-void
.end method

.method public updateModePreference()V
    .locals 4

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v2, "pref_camera_scenemode_key"

    const v3, 0x7f0c0100

    invoke-virtual {p0, v3}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ActorSmile;->mSmileScenes:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/camera/Util;->hasItem(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pref_camera_scenemode_key"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "auto"

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    :cond_0
    return-void
.end method
