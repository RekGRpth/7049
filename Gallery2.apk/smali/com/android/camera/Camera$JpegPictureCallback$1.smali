.class Lcom/android/camera/Camera$JpegPictureCallback$1;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/Camera$JpegPictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/camera/Camera$JpegPictureCallback;


# direct methods
.method constructor <init>(Lcom/android/camera/Camera$JpegPictureCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    iget-object v0, v0, Lcom/android/camera/Camera$JpegPictureCallback;->this$0:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    iget-object v0, v0, Lcom/android/camera/Camera$JpegPictureCallback;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$1000(Lcom/android/camera/Camera;)V

    iget-object v0, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    iget-object v0, v0, Lcom/android/camera/Camera$JpegPictureCallback;->this$0:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    iget-object v0, v0, Lcom/android/camera/Camera$JpegPictureCallback;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->startFaceDetection()V

    iget-object v0, p0, Lcom/android/camera/Camera$JpegPictureCallback$1;->this$1:Lcom/android/camera/Camera$JpegPictureCallback;

    iget-object v0, v0, Lcom/android/camera/Camera$JpegPictureCallback;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->checkStorage()V

    :cond_0
    return-void
.end method
