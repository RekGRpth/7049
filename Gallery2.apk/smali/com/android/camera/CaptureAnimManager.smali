.class public Lcom/android/camera/CaptureAnimManager;
.super Ljava/lang/Object;
.source "CaptureAnimManager.java"


# static fields
.field private static final CAPTURE_ANIM_DURATION:F = 700.0f

.field private static final GAP_RATIO:F = 0.1f

.field private static final TAG:Ljava/lang/String; = "CaptureAnimManager"

.field private static final TOTAL_RATIO:F = 1.1f

.field private static final ZOOM_DELTA:F = 0.2f

.field private static final ZOOM_IN_BEGIN:F = 0.8f


# instance fields
.field private mAnimOrientation:I

.field private mAnimStartTime:J

.field private mCenterDelta:F

.field private mCenterX:F

.field private mCenterY:F

.field private mDrawHeight:I

.field private mDrawWidth:I

.field private mGap:F

.field private mHalfGap:F

.field private final mSlideInterpolator:Landroid/view/animation/Interpolator;

.field private final mZoomInInterpolator:Landroid/view/animation/Interpolator;

.field private final mZoomOutInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/camera/CaptureAnimManager;->mZoomOutInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/camera/CaptureAnimManager;->mZoomInInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/camera/CaptureAnimManager;->mSlideInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private calculateScale(F)F
    .locals 6
    .param p1    # F

    const/high16 v5, 0x40000000

    const/high16 v3, 0x3f000000

    const v4, 0x3e4ccccd

    const/high16 v0, 0x3f800000

    cmpg-float v1, p1, v3

    if-gtz v1, :cond_0

    const/high16 v1, 0x3f800000

    iget-object v2, p0, Lcom/android/camera/CaptureAnimManager;->mZoomOutInterpolator:Landroid/view/animation/Interpolator;

    mul-float v3, p1, v5

    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v4

    sub-float v0, v1, v2

    :goto_0
    return v0

    :cond_0
    const v1, 0x3f4ccccd

    iget-object v2, p0, Lcom/android/camera/CaptureAnimManager;->mZoomInInterpolator:Landroid/view/animation/Interpolator;

    sub-float v3, p1, v3

    mul-float/2addr v3, v5

    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v4

    add-float v0, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public drawAnimation(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/camera/CameraScreenNail;Lcom/android/gallery3d/ui/RawTexture;)Z
    .locals 21
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # Lcom/android/camera/CameraScreenNail;
    .param p3    # Lcom/android/gallery3d/ui/RawTexture;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/camera/CaptureAnimManager;->mAnimStartTime:J

    sub-long v18, v2, v6

    move-wide/from16 v0, v18

    long-to-float v2, v0

    const/high16 v3, 0x442f0000

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    move-wide/from16 v0, v18

    long-to-float v2, v0

    const/high16 v3, 0x442f0000

    div-float v15, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/camera/CaptureAnimManager;->calculateScale(F)F

    move-result v17

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mAnimOrientation:I

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mAnimOrientation:I

    const/16 v3, 0xb4

    if-ne v2, v3, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/camera/CaptureAnimManager;->mSlideInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v6, v15}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v6

    mul-float/2addr v3, v6

    add-float v13, v2, v3

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mDrawHeight:I

    int-to-float v2, v2

    mul-float v16, v2, v17

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mDrawWidth:I

    int-to-float v2, v2

    mul-float v20, v2, v17

    float-to-int v4, v13

    float-to-int v5, v14

    float-to-int v8, v13

    float-to-int v9, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mAnimOrientation:I

    sparse-switch v2, :sswitch_data_0

    :goto_2
    invoke-interface/range {p1 .. p1}, Lcom/android/gallery3d/ui/GLCanvas;->getAlpha()F

    move-result v12

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/camera/CameraScreenNail;->directDraw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    move/from16 v0, v20

    float-to-int v10, v0

    move/from16 v0, v16

    float-to-int v11, v0

    move-object/from16 v6, p3

    move-object/from16 v7, p1

    invoke-virtual/range {v6 .. v11}, Lcom/android/gallery3d/ui/RawTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/camera/CaptureAnimManager;->mSlideInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v6, v15}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v6

    mul-float/2addr v3, v6

    add-float v14, v2, v3

    goto :goto_1

    :sswitch_0
    sub-float v2, v13, v20

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/high16 v2, 0x40000000

    div-float v2, v16, v2

    sub-float v2, v14, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v2, v2, v17

    add-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v8

    move v9, v5

    goto :goto_2

    :sswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v2, v2, v17

    add-float/2addr v2, v14

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    const/high16 v2, 0x40000000

    div-float v2, v20, v2

    sub-float v2, v13, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-float v2, v14, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v9

    move v8, v4

    goto/16 :goto_2

    :sswitch_2
    add-float v2, v13, v20

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v3, v3, v17

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/high16 v2, 0x40000000

    div-float v2, v16, v2

    sub-float v2, v14, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v2, v2, v17

    sub-float v2, v13, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v8

    move v9, v5

    goto/16 :goto_2

    :sswitch_3
    sub-float v2, v14, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    const/high16 v2, 0x40000000

    div-float v2, v20, v2

    sub-float v2, v13, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    mul-float v2, v2, v17

    add-float/2addr v2, v14

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v9

    move v8, v4

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/camera/CaptureAnimManager;->mAnimOrientation:I

    return-void
.end method

.method public startAnimation(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const v4, 0x3f8ccccd

    const v3, 0x3dcccccd

    const/high16 v2, 0x40000000

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/CaptureAnimManager;->mAnimStartTime:J

    iput p3, p0, Lcom/android/camera/CaptureAnimManager;->mDrawWidth:I

    iput p4, p0, Lcom/android/camera/CaptureAnimManager;->mDrawHeight:I

    iget v0, p0, Lcom/android/camera/CaptureAnimManager;->mAnimOrientation:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    int-to-float v0, p3

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    iget v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    int-to-float v0, p1

    iget v1, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    int-to-float v0, p3

    mul-float/2addr v0, v4

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    int-to-float v0, p2

    int-to-float v1, p4

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    goto :goto_0

    :sswitch_1
    int-to-float v0, p4

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    iget v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    add-int v0, p2, p4

    int-to-float v0, v0

    iget v1, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    neg-int v0, p4

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    goto :goto_0

    :sswitch_2
    int-to-float v0, p3

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    iget v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    int-to-float v0, p1

    iget v1, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    neg-int v0, p3

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    int-to-float v0, p2

    int-to-float v1, p4

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    goto :goto_0

    :sswitch_3
    int-to-float v0, p4

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    iget v0, p0, Lcom/android/camera/CaptureAnimManager;->mGap:F

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    int-to-float v0, p2

    iget v1, p0, Lcom/android/camera/CaptureAnimManager;->mHalfGap:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterY:F

    int-to-float v0, p4

    mul-float/2addr v0, v4

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterDelta:F

    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/camera/CaptureAnimManager;->mCenterX:F

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method
