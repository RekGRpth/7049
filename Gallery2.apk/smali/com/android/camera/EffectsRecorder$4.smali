.class Lcom/android/camera/EffectsRecorder$4;
.super Ljava/lang/Object;
.source "EffectsRecorder.java"

# interfaces
.implements Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/EffectsRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/EffectsRecorder;


# direct methods
.method constructor <init>(Lcom/android/camera/EffectsRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRunnerDone(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x6

    const/4 v7, 0x3

    const/4 v6, 0x2

    iget-object v3, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$600(Lcom/android/camera/EffectsRecorder;)Lcom/android/camera/EffectsRecorder$OnSurfaceStateChangeListener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$600(Lcom/android/camera/EffectsRecorder;)Lcom/android/camera/EffectsRecorder$OnSurfaceStateChangeListener;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lcom/android/camera/EffectsRecorder$OnSurfaceStateChangeListener;->onStateChange(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$000(Lcom/android/camera/EffectsRecorder;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "EffectsRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Graph runner done ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mRunner "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v5}, Lcom/android/camera/EffectsRecorder;->access$700(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mOldRunner "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v5}, Lcom/android/camera/EffectsRecorder;->access$800(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-ne p1, v8, :cond_3

    const-string v2, "EffectsRecorder"

    const-string v4, "Error running filter graph!"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$700(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$700(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/GraphRunner;->getError()Ljava/lang/Exception;

    move-result-object v0

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2, v0}, Lcom/android/camera/EffectsRecorder;->access$900(Lcom/android/camera/EffectsRecorder;Ljava/lang/Exception;)V

    :cond_3
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$800(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$000(Lcom/android/camera/EffectsRecorder;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "EffectsRecorder"

    const-string v4, "Tearing down old graph."

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$1000(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/GraphEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/MffEnvironment;->getContext()Landroid/filterfw/core/FilterContext;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->isActive()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->activate()V

    :cond_5
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$800(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/GraphRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    move-result-object v2

    iget-object v4, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v4}, Lcom/android/camera/EffectsRecorder;->access$1000(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/GraphEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/filterfw/MffEnvironment;->getContext()Landroid/filterfw/core/FilterContext;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/filterfw/core/FilterGraph;->tearDown(Landroid/filterfw/core/FilterContext;)V

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->isActive()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->deactivate()V

    :cond_6
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/camera/EffectsRecorder;->access$802(Lcom/android/camera/EffectsRecorder;Landroid/filterfw/core/GraphRunner;)Landroid/filterfw/core/GraphRunner;

    :cond_7
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$200(Lcom/android/camera/EffectsRecorder;)I

    move-result v2

    if-eq v2, v7, :cond_8

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$200(Lcom/android/camera/EffectsRecorder;)I

    move-result v2

    if-ne v2, v6, :cond_d

    :cond_8
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$000(Lcom/android/camera/EffectsRecorder;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "EffectsRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Previous effect halted. Running graph again. state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v5}, Lcom/android/camera/EffectsRecorder;->access$200(Lcom/android/camera/EffectsRecorder;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/android/camera/EffectsRecorder;->tryEnable3ALocks(Z)Z

    if-ne p1, v8, :cond_a

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$400(Lcom/android/camera/EffectsRecorder;)I

    move-result v2

    if-ne v2, v6, :cond_a

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Lcom/android/camera/EffectsRecorder;->access$500(Lcom/android/camera/EffectsRecorder;II)V

    :cond_a
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$700(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/GraphRunner;->run()V

    :cond_b
    :goto_1
    monitor-exit v3

    return-void

    :cond_c
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$800(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$800(Lcom/android/camera/EffectsRecorder;)Landroid/filterfw/core/GraphRunner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/GraphRunner;->getError()Ljava/lang/Exception;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$200(Lcom/android/camera/EffectsRecorder;)I

    move-result v2

    const/4 v4, 0x5

    if-eq v2, v4, :cond_b

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    invoke-static {v2}, Lcom/android/camera/EffectsRecorder;->access$000(Lcom/android/camera/EffectsRecorder;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "EffectsRecorder"

    const-string v4, "Runner halted, restoring direct preview"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/android/camera/EffectsRecorder;->tryEnable3ALocks(Z)Z

    iget-object v2, p0, Lcom/android/camera/EffectsRecorder$4;->this$0:Lcom/android/camera/EffectsRecorder;

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v2, v4, v5}, Lcom/android/camera/EffectsRecorder;->access$500(Lcom/android/camera/EffectsRecorder;II)V

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
