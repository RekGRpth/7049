.class public Lcom/android/camera/MenuHelper;
.super Ljava/lang/Object;
.source "MenuHelper.java"


# static fields
.field private static final CAMERA_CLASS:Ljava/lang/String; = "com.android.camera.Camera"

.field private static final INCLUDE_IMAGES:I = 0x1

.field private static final INCLUDE_VIDEOS:I = 0x4

.field private static final KEY_SWITCH_INTERNAL:Ljava/lang/String; = "internal"

.field private static final MAV_CLASS:Ljava/lang/String; = "com.mediatek.camera.mav.MavActivity"

.field private static final PANORAMA_CLASS:Ljava/lang/String; = "com.mediatek.camera.panorama.PanoramaActivity"

.field private static final TAG:Ljava/lang/String; = "MenuHelper"

.field private static final VIDEO_CAMERA_CLASS:Ljava/lang/String; = "com.android.camera.VideoCamera"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static gotoMode(ILandroid/app/Activity;)V
    .locals 6
    .param p0    # I
    .param p1    # Landroid/app/Activity;

    packed-switch p0, :pswitch_data_0

    const-string v3, "MenuHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown camera mode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "com.mediatek.camera.panorama.PanoramaActivity"

    const-string v1, "com.mediatek.camera.panorama.PanoramaActivity"

    :goto_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1}, Lcom/android/camera/MenuHelper;->startCameraActivity(Landroid/app/Activity;Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "android.media.action.VIDEO_CAMERA"

    const-string v1, "com.android.camera.VideoCamera"

    goto :goto_1

    :pswitch_2
    const-string v0, "android.media.action.STILL_IMAGE_CAMERA"

    const-string v1, "com.android.camera.Camera"

    goto :goto_1

    :pswitch_3
    const-string v0, "com.mediatek.camera.mav.MavActivity"

    const-string v1, "com.mediatek.camera.mav.MavActivity"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static isCameraModeSwitching(Landroid/content/Intent;)Z
    .locals 1
    .param p0    # Landroid/content/Intent;

    const-string v0, "internal"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static startCameraActivity(Landroid/app/Activity;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    const/high16 v1, 0x4000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x2000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraHolder;->keep()V

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/high16 v1, 0x10a0000

    const v2, 0x10a0001

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
