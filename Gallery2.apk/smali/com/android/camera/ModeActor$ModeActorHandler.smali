.class Lcom/android/camera/ModeActor$ModeActorHandler;
.super Landroid/os/Handler;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ModeActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeActorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ModeActor;


# direct methods
.method public constructor <init>(Lcom/android/camera/ModeActor;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModeActorHandler msg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    new-instance v0, Lcom/android/camera/ModeActor$ModeActorHandler$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ModeActor$ModeActorHandler$1;-><init>(Lcom/android/camera/ModeActor$ModeActorHandler;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->updateThumbnailView()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/ModeActor;->updateSavingHint(Z)V

    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    invoke-virtual {v0}, Lcom/android/camera/ModeActor;->onBurstSaveDone()V

    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->supportStereo3DMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->supportSingle3dSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModeActor$ModeActorHandler;->this$0:Lcom/android/camera/ModeActor;

    iget-object v0, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mRemainPictureView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_1
        0xf -> :sswitch_2
        0x13 -> :sswitch_0
    .end sparse-switch
.end method
