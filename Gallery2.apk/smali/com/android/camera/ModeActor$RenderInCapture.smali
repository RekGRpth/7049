.class public Lcom/android/camera/ModeActor$RenderInCapture;
.super Ljava/lang/Thread;
.source "ModeActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ModeActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "RenderInCapture"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ModeActor;


# direct methods
.method protected constructor <init>(Lcom/android/camera/ModeActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ModeActor$RenderInCapture;->this$0:Lcom/android/camera/ModeActor;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v1, 0x3c

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iget-object v1, p0, Lcom/android/camera/ModeActor$RenderInCapture;->this$0:Lcom/android/camera/ModeActor;

    iget-object v1, v1, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/ActivityBase;->mAppBridge:Lcom/android/camera/ActivityBase$MyAppBridge;

    invoke-virtual {v1}, Lcom/android/camera/ActivityBase$MyAppBridge;->requestRender()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ModeActor"

    const-string v2, "RenderInCapture exit"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
