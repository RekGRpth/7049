.class public abstract Lcom/android/camera/ModeActor;
.super Ljava/lang/Object;
.source "ModeActor.java"

# interfaces
.implements Lcom/android/camera/SelfTimerManager$SelfTimerListener;
.implements Lcom/android/camera/ShutterButton$OnShutterButtonListener;
.implements Lcom/android/camera/ShutterButton$OnShutterButtonLongPressListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ModeActor$ModeActorHandler;,
        Lcom/android/camera/ModeActor$RenderInCapture;
    }
.end annotation


# static fields
.field static final EV_SELECT:I = 0x9

.field protected static final LOG:Z = true

.field static final MSG_BURST_SAVING_DONE:I = 0xf

.field static final MSG_CHANGE_HUD_STATE:I = 0x12

.field static final MSG_EV_SAVING_DONE:I = 0xd

.field static final MSG_EV_SEL_DONE:I = 0x13

.field static final MSG_FIRE_EV_SELECTOR:I = 0xa

.field static final MSG_SHOW_SAVING_HINT:I = 0xe

.field private static final SKIP_FOCUS_ON_CAPTURE:Z = true

.field protected static final TAG:Ljava/lang/String; = "ModeActor"


# instance fields
.field protected mCamera:Lcom/android/camera/Camera;

.field protected mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

.field protected final mHandler:Landroid/os/Handler;

.field protected mLastJpegLoc:Landroid/location/Location;

.field protected mLastUri:Landroid/net/Uri;

.field protected final mModeName:Ljava/lang/String;

.field protected mParameters:Landroid/hardware/Camera$Parameters;

.field protected mPaused:Z

.field protected mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

.field protected mPreferences:Lcom/android/camera/ComboPreferences;

.field protected mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

.field private mSavingDlg:Lcom/android/camera/RotateDialogController;

.field private mTimeSelfTimerStart:J

.field protected mZSDEnabled:Z


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p3    # Landroid/hardware/Camera$Parameters;
    .param p4    # Lcom/android/camera/PreferenceGroup;
    .param p5    # Lcom/android/camera/ComboPreferences;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iput-object p2, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iput-object p3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object p4, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iput-object p5, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    new-instance v0, Lcom/android/camera/ModeActor$ModeActorHandler;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/camera/ModeActor$ModeActorHandler;-><init>(Lcom/android/camera/ModeActor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0, p0}, Lcom/android/camera/SelfTimerManager;->setTimerListener(Lcom/android/camera/SelfTimerManager$SelfTimerListener;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ShutterButton$OnShutterButtonListener;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonLongPressListener(Lcom/android/camera/ShutterButton$OnShutterButtonLongPressListener;)V

    iput-object p6, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    return-void
.end method

.method private setScenemodeSettings()V
    .locals 11

    const v10, 0x7f0c002f

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_exposuremeter_key"

    const v9, 0x7f0c0024

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedExposureMeter()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v3, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v3}, Landroid/hardware/Camera$Parameters;->setExposureMeter(Ljava/lang/String;)V

    :cond_0
    const-string v4, "spot"

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedFocusMeter()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v4, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "ModeActor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setSceneModeSetting, focusMeter: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v4}, Landroid/hardware/Camera$Parameters;->setFocusMeter(Ljava/lang/String;)V

    :cond_1
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_hue_key"

    const v9, 0x7f0c0031

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedHueMode()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v5, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v5}, Landroid/hardware/Camera$Parameters;->setHueMode(Ljava/lang/String;)V

    :cond_2
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_brightness_key"

    const v9, 0x7f0c0035

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedBrightnessMode()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v0, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v0}, Landroid/hardware/Camera$Parameters;->setBrightnessMode(Ljava/lang/String;)V

    :cond_3
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_edge_key"

    invoke-virtual {p0, v10}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedEdgeMode()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v2, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v2}, Landroid/hardware/Camera$Parameters;->setEdgeMode(Ljava/lang/String;)V

    :cond_4
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_saturation_key"

    const v9, 0x7f0c0033

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedSaturationMode()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v6}, Landroid/hardware/Camera$Parameters;->setSaturationMode(Ljava/lang/String;)V

    :cond_5
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_contrast_key"

    invoke-virtual {p0, v10}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v1, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v1}, Landroid/hardware/Camera$Parameters;->setContrastMode(Ljava/lang/String;)V

    :cond_6
    return-void
.end method


# virtual methods
.method public animateByIntentOrZSD()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v0, v0, Lcom/android/camera/Camera;->mIsImageCaptureIntent:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraRotation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    :cond_0
    return-void
.end method

.method public animateCapture()V
    .locals 0

    return-void
.end method

.method public applySpecialCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public breakTimer()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->breakTimer()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    return-void
.end method

.method public canShot()Z
    .locals 4

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getRemainPictures()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkMode(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "can not set Capture mode = null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public checkSelfTimerMode()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->checkSelfTimerMode()Z

    move-result v0

    return v0
.end method

.method public checkStopProcess()V
    .locals 0

    return-void
.end method

.method public computeRemaining(I)I
    .locals 1
    .param p1    # I

    if-lez p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    add-int/lit8 p1, p1, -0x2

    :cond_0
    :goto_0
    return p1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method protected createName(J)Ljava/lang/String;
    .locals 3
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const v2, 0x7f0c010e

    invoke-virtual {p0, v2}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public doCancelCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doSmileShutter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enableFD(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v2

    aget-object v0, v2, p1

    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-eq v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public ensureCaptureTempPath()V
    .locals 2

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->getCaptureTempPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setCapturePath(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getCamOri(I)I
    .locals 1
    .param p1    # I

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    aget-object v0, v0, p1

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    return v0
.end method

.method public getCaptureMode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    return-object v0
.end method

.method public getCaptureTempPath()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLastThumbnail()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->getLastThumbnail()V

    return-void
.end method

.method public getPictureBytes(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/camera/Storage;->PICTURE_SIZE_TABLE:Lcom/android/camera/DefaultHashMap;

    invoke-virtual {v1, p1}, Lcom/android/camera/DefaultHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPictureCallback(Landroid/location/Location;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p1    # Landroid/location/Location;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleSDcardUnmount()V
    .locals 0

    return-void
.end method

.method public interruptRenderThread()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mRenderThread:Lcom/android/camera/ModeActor$RenderInCapture;

    :cond_0
    return-void
.end method

.method public isBurstShotInternal()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isInShutterProgress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSelfTimerCounting()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->isSelfTimerCounting()Z

    move-result v0

    return v0
.end method

.method public isSelfTimerEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->isSelfTimerEnabled()Z

    move-result v0

    return v0
.end method

.method public isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lowStorage()V
    .locals 0

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    return-void
.end method

.method public onBurstSaveDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x5e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onPause()V
    .locals 3

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause() mPausing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    return-void
.end method

.method public onPausePre()V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPausePre mPaused ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v0}, Lcom/android/camera/SelfTimerManager;->breakTimer()V

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->interruptRenderThread()V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pref_camera_self_timer_key"

    aput-object v2, v0, v1

    const-string v1, "0"

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/android/camera/ModeActor;->writePreference([Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    invoke-virtual {v0}, Lcom/android/camera/Camera$ImageNamer;->cleanOldUriAndRequestSync()V

    :cond_1
    return-void
.end method

.method public onPictureTaken(Landroid/location/Location;II)V
    .locals 0
    .param p1    # Landroid/location/Location;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->interruptRenderThread()V

    return-void
.end method

.method public onPreviewStartDone()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "ModeActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() mPausing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    invoke-virtual {p0, v3}, Lcom/android/camera/ModeActor;->updateSavingHint(Z)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    return-void
.end method

.method public onShutter()V
    .locals 0

    return-void
.end method

.method public onShutterButtonClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->onShutterButtonClick()V

    return-void
.end method

.method public onShutterButtonFocus(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onShutterButtonLongPressed()V
    .locals 5

    const v4, 0x7f0c00b3

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_camera_normal_capture_key"

    invoke-virtual {v2, v3}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v2, Lcom/android/camera/ui/RotateTextToast;

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getOrientation()I

    move-result v4

    invoke-direct {v2, v3, v1, v4}, Lcom/android/camera/ui/RotateTextToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    invoke-virtual {v2}, Lcom/android/camera/ui/RotateTextToast;->show()V

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getEntry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public onTimerStart()V
    .locals 0

    return-void
.end method

.method public onTimerStop()V
    .locals 0

    return-void
.end method

.method public onTimerTimeout()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->onShutterButtonClick()V

    :cond_0
    return-void
.end method

.method public overrideSelfTimer(Z)V
    .locals 5
    .param p1    # Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v0, "0"

    :cond_0
    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pref_camera_self_timer_key"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v2, "pref_camera_self_timer_key"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/Camera;->mSelftimerManager:Lcom/android/camera/SelfTimerManager;

    invoke-virtual {v1, v0}, Lcom/android/camera/SelfTimerManager;->setSelfTimerDuration(Ljava/lang/String;)V

    return-void
.end method

.method public pictureSize()J
    .locals 6

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v3, v3, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iget v4, v2, Landroid/hardware/Camera$Size;->height:I

    if-le v3, v4, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const-string v3, "ModeActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "picture size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "superfine"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/Storage;->getSize(Ljava/lang/String;)I

    move-result v3

    int-to-long v3, v3

    return-wide v3

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public prepareUri(Landroid/content/ContentResolver;JIIII)V
    .locals 8
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mImageNamer:Lcom/android/camera/Camera$ImageNamer;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/android/camera/Camera$ImageNamer;->prepareUri(Landroid/content/ContentResolver;JIIII)V

    return-void
.end method

.method public processModeAction()Z
    .locals 2

    const-string v0, "normal"

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public readyToCapture()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public restoreModeUI(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public saveBulkPictures([Ljava/lang/String;Landroid/location/Location;)V
    .locals 32
    .param p1    # [Ljava/lang/String;
    .param p2    # Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v26

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/android/camera/Util;->createJpegName(J)Ljava/lang/String;

    move-result-object v27

    const/4 v13, 0x0

    const/16 v28, 0x0

    const/16 v19, 0x0

    const/4 v8, 0x0

    move-object/from16 v12, p1

    array-length v0, v12

    move/from16 v23, v0

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    move/from16 v1, v23

    if-ge v0, v1, :cond_1

    aget-object v24, v12, v21

    if-eqz v24, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "_"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v13, v13, 0x1

    invoke-static {v4}, Lcom/android/camera/Storage;->generateFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    const/4 v15, 0x0

    :try_start_0
    new-instance v16, Landroid/media/ExifInterface;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v15, v16

    :goto_1
    invoke-static {v15}, Lcom/android/camera/Util;->getExifOrientation(Landroid/media/ExifInterface;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v26

    iget v9, v0, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, v26

    iget v10, v0, Landroid/hardware/Camera$Size;->height:I

    const/4 v11, 0x0

    move-object/from16 v7, p2

    invoke-static/range {v3 .. v11}, Lcom/android/camera/Storage;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;IIII)Landroid/net/Uri;

    move-result-object v28

    :cond_0
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    :catch_0
    move-exception v14

    const-string v3, "ModeActor"

    const-string v7, "cannot read exif"

    invoke-static {v3, v7, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    if-eqz v28, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreviewFrameLayout()Lcom/android/camera/PreviewFrameLayout;

    move-result-object v22

    move-object/from16 v0, v26

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-double v9, v3

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-double v0, v3

    move-wide/from16 v30, v0

    div-double v9, v9, v30

    invoke-static {v9, v10}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v9

    double-to-int v0, v9

    move/from16 v29, v0

    move-object/from16 v0, v26

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-double v9, v3

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-double v0, v3

    move-wide/from16 v30, v0

    div-double v9, v9, v30

    invoke-static {v9, v10}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v9

    double-to-int v0, v9

    move/from16 v20, v0

    move/from16 v0, v29

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v0, v19

    move/from16 v1, v25

    move-object/from16 v2, v28

    invoke-static {v0, v8, v1, v2}, Lcom/android/camera/Thumbnail;->createThumbnail(Ljava/lang/String;IILandroid/net/Uri;)Lcom/android/camera/Thumbnail;

    move-result-object v7

    iput-object v7, v3, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    move-object/from16 v0, v28

    invoke-static {v3, v0}, Lcom/android/camera/Util;->broadcastNewPicture(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_2
    return-void
.end method

.method public saveEVPictures()V
    .locals 0

    return-void
.end method

.method protected setCaptureMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mModeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    return-void
.end method

.method public setCaptureModeSettings()V
    .locals 0

    return-void
.end method

.method public setDisplayOrientation(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method protected setMtkCameraParameters(Ljava/lang/String;Landroid/hardware/Camera$Size;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/hardware/Camera$Size;

    const-string v7, "ModeActor"

    const-string v8, "setMtkCameraParameters"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v7, v7, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/hardware/Camera$Parameters;->setCameraMode(I)V

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->setCaptureMode()V

    const/4 v3, 0x0

    const-string v7, "auto"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ModeActor;->setScenemodeSettings()V

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_iso_key"

    const v9, 0x7f0c001c

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedISOSpeed()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v3, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v3}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    :cond_0
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_coloreffect_key"

    const v9, 0x7f0c003c

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedColorEffects()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v2, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v2}, Landroid/hardware/Camera$Parameters;->setColorEffect(Ljava/lang/String;)V

    :cond_1
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_antibanding_key"

    const v9, 0x7f0c0057

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedAntibanding()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v0, v7}, Lcom/android/camera/ModeActor;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v0}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->setCaptureModeSettings()V

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->isZSDSupported()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v8, "pref_camera_zsd_key"

    const v9, 0x7f0c0084

    invoke-virtual {p0, v9}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v6}, Landroid/hardware/Camera$Parameters;->setZSDMode(Ljava/lang/String;)V

    :cond_3
    const-string v7, "on"

    iget-object v8, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getZSDMode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/camera/ModeActor;->mZSDEnabled:Z

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getISOSpeed()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/android/camera/ModeActor;->getCamOri(I)I

    move-result v1

    if-nez v3, :cond_4

    const v7, 0x7f0c001c

    invoke-virtual {p0, v7}, Lcom/android/camera/ModeActor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_4
    const-string v7, "1600"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "800"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    :cond_5
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    const-string v7, "1280x960"

    iget-object v8, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v9, "0"

    invoke-static {v7, v5, v8, v1, v9}, Lcom/android/camera/CameraSettings;->setCameraPictureSize(Ljava/lang/String;Ljava/util/List;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "pref_camera_picturesize_key"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "1280x960"

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    :goto_0
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    iget v7, p2, Landroid/hardware/Camera$Size;->width:I

    iget v8, v4, Landroid/hardware/Camera$Size;->width:I

    if-eq v7, v8, :cond_6

    iget-object v7, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v7, v7, Lcom/android/camera/Camera;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x60

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    return-void

    :cond_7
    iget-object v7, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "pref_camera_picturesize_key"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public skipFocus()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public updateCaptureModeButton(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v1, Lcom/android/camera/Camera;->mNormalCaptureIndicatorButton:Lcom/android/camera/ui/ControlBarIndicatorButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public updateCaptureModeIndicator()V
    .locals 0

    return-void
.end method

.method public updateCaptureModeUI(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "pref_camera_whitebalance_key"

    aput-object v2, v1, v3

    aput-object v4, v1, v5

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v1, v6

    const/4 v2, 0x3

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/camera/ModeActor;->overrideSelfTimer(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v1, v3

    aput-object v4, v1, v5

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateMembers(Lcom/android/camera/CameraManager$CameraProxy;Landroid/hardware/Camera$Parameters;Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ComboPreferences;)V
    .locals 0
    .param p1    # Lcom/android/camera/CameraManager$CameraProxy;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # Lcom/android/camera/PreferenceGroup;
    .param p4    # Lcom/android/camera/ComboPreferences;

    iput-object p1, p0, Lcom/android/camera/ModeActor;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iput-object p2, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object p3, p0, Lcom/android/camera/ModeActor;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iput-object p4, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {p0}, Lcom/android/camera/ModeActor;->updateShutterListener()V

    return-void
.end method

.method public updateModePreference()V
    .locals 0

    return-void
.end method

.method public updateSavingHint(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/camera/ModeActor;->mPaused:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mSavingDlg:Lcom/android/camera/RotateDialogController;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mSavingDlg:Lcom/android/camera/RotateDialogController;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/RotateDialogController;->showWaitingDialog(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ModeActor;->mSavingDlg:Lcom/android/camera/RotateDialogController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mSavingDlg:Lcom/android/camera/RotateDialogController;

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ModeActor;->mSavingDlg:Lcom/android/camera/RotateDialogController;

    goto :goto_0
.end method

.method public updateSceneModeUI(Z)V
    .locals 9
    .param p1    # Z

    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, v1, Lcom/android/camera/ActivityBase;->mParameters:Landroid/hardware/Camera$Parameters;

    iput-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getISOSpeed()Ljava/lang/String;

    move-result-object v0

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "pref_camera_whitebalance_key"

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getWhiteBalance()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "pref_camera_focusmode_key"

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x4

    const-string v4, "pref_camera_iso_key"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object v0, v2, v3

    const/4 v3, 0x6

    const-string v4, "pref_camera_exposuremeter_key"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getExposureMeter()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pref_camera_edge_key"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getEdgeMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "pref_camera_saturation_key"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSaturationMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "pref_camera_contrast_key"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getContrastMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "pref_camera_hue_key"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getHueMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "pref_camera_brightness_key"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getBrightnessMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    :goto_0
    const-string v1, "1600"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "800"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "pref_camera_picturesize_key"

    aput-object v3, v2, v6

    const-string v3, "1280x960"

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/camera/ModeActor;->updateCaptureModeUI(Z)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v2, 0x14

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "pref_camera_flashmode_key"

    aput-object v3, v2, v6

    aput-object v5, v2, v7

    const-string v3, "pref_camera_whitebalance_key"

    aput-object v3, v2, v8

    aput-object v5, v2, v4

    const/4 v3, 0x4

    const-string v4, "pref_camera_focusmode_key"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object v5, v2, v3

    const/4 v3, 0x6

    const-string v4, "pref_camera_iso_key"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object v5, v2, v3

    const/16 v3, 0x8

    const-string v4, "pref_camera_exposuremeter_key"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    aput-object v5, v2, v3

    const/16 v3, 0xa

    const-string v4, "pref_camera_edge_key"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    aput-object v5, v2, v3

    const/16 v3, 0xc

    const-string v4, "pref_camera_saturation_key"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    aput-object v5, v2, v3

    const/16 v3, 0xe

    const-string v4, "pref_camera_contrast_key"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    aput-object v5, v2, v3

    const/16 v3, 0x10

    const-string v4, "pref_camera_hue_key"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    aput-object v5, v2, v3

    const/16 v3, 0x12

    const-string v4, "pref_camera_brightness_key"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    aput-object v5, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "pref_camera_picturesize_key"

    aput-object v3, v2, v6

    aput-object v5, v2, v7

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->overrideSettings([Ljava/lang/String;)V

    goto :goto_1
.end method

.method public updateShutterListener()V
    .locals 2

    const-string v0, "ModeActor"

    const-string v1, "updateShutterListener in normal mode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonLongPressListener(Lcom/android/camera/ShutterButton$OnShutterButtonLongPressListener;)V

    iget-object v0, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, v0, Lcom/android/camera/Camera;->mShutterButton:Lcom/android/camera/ShutterButton;

    iget-object v1, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ShutterButton$OnShutterButtonListener;)V

    return-void
.end method

.method public updateThumbnailInSaver(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    if-gt p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateViews(Lcom/android/camera/Camera;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;

    return-void
.end method

.method public updateZoomControl(Lcom/android/camera/ui/ZoomControl;Z)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/ZoomControl;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Lcom/android/camera/ui/ZoomControl;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public varargs writePreference([Ljava/lang/String;)V
    .locals 5
    .param p1    # [Ljava/lang/String;

    array-length v4, p1

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    :cond_0
    iget-object v4, p0, Lcom/android/camera/ModeActor;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {v4}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_1

    aget-object v2, p1, v1

    add-int/lit8 v4, v1, 0x1

    aget-object v3, p1, v4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v4, p0, Lcom/android/camera/ModeActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->reloadPreferences()V

    return-void
.end method
