.class public Lcom/android/camera/ModePicker;
.super Landroid/widget/RelativeLayout;
.source "ModePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/camera/ui/PopupManager$OnOtherPopupShowedListener;
.implements Lcom/android/camera/ui/Rotatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ModePicker$OnModeChangeListener;
    }
.end annotation


# static fields
.field public static final MODE_CAMERA:I = 0x0

.field public static final MODE_MAV:I = 0x3

.field private static final MODE_NUM:I = 0x4

.field public static final MODE_PANORAMA:I = 0x2

.field public static final MODE_VIDEO:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ModePicker"


# instance fields
.field private CURRENT_MODE_NUM:I

.field private final DISABLED_COLOR:I

.field private SUPPORTED_MODE_NUM:I

.field private mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mChooseModeLessView:Landroid/widget/RelativeLayout;

.field private mChooseModeView:Landroid/widget/RelativeLayout;

.field private mCurrentMode:I

.field private mCurrentModeBar:Landroid/view/View;

.field private mCurrentModeFrame:Landroid/view/View;

.field private mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

.field private mFadeIn:Landroid/view/animation/Animation;

.field private mFadeOut:Landroid/view/animation/Animation;

.field private mFirstCalculate:Z

.field private mListener:Lcom/android/camera/ModePicker$OnModeChangeListener;

.field private mModeChanged:Z

.field private mModeSelectionFrame:Landroid/view/View;

.field private mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

.field private mNeedCollapse:Z

.field private mSelectionEnabled:Z

.field private mSmallSizePixel:I

.field private res:Landroid/content/res/Resources;

.field private sIsScreenLarge:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    iput v1, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    iput-boolean v1, p0, Lcom/android/camera/ModePicker;->sIsScreenLarge:Z

    new-instance v0, Lcom/android/camera/ModePicker$2;

    invoke-direct {v0, p0}, Lcom/android/camera/ModePicker$2;-><init>(Lcom/android/camera/ModePicker;)V

    iput-object v0, p0, Lcom/android/camera/ModePicker;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    iput-boolean v1, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    iput-boolean v1, p0, Lcom/android/camera/ModePicker;->mFirstCalculate:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/camera/ModePicker;->DISABLED_COLOR:I

    const v0, 0x7f050002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ModePicker;->mFadeIn:Landroid/view/animation/Animation;

    const v0, 0x7f050003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ModePicker;->mFadeOut:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mFadeOut:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {p1}, Lcom/android/camera/ui/PopupManager;->getInstance(Landroid/content/Context;)Lcom/android/camera/ui/PopupManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/PopupManager;->setOnOtherPopupShowedListener(Lcom/android/camera/ui/PopupManager$OnOtherPopupShowedListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ModePicker;->res:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ModePicker;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ModePicker;

    iget-boolean v0, p0, Lcom/android/camera/ModePicker;->mModeChanged:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/ModePicker;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/camera/ModePicker;

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/ModePicker;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/camera/ModePicker;

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/camera/ModePicker;)V
    .locals 0
    .param p0    # Lcom/android/camera/ModePicker;

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->changeToSelectedMode()V

    return-void
.end method

.method private changeToSelectedMode()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mListener:Lcom/android/camera/ModePicker$OnModeChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mListener:Lcom/android/camera/ModePicker$OnModeChangeListener;

    iget v1, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    invoke-interface {v0, v1}, Lcom/android/camera/ModePicker$OnModeChangeListener;->onModeChanged(I)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/ModePicker;->mModeChanged:Z

    return-void
.end method

.method private computeCollapse()Z
    .locals 8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v6, "ModePicker"

    const-string v7, "computeCollapse"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iput-boolean v5, p0, Lcom/android/camera/ModePicker;->mFirstCalculate:Z

    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_1
    iget v6, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    if-ge v1, v6, :cond_2

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v6, v6, v1

    invoke-direct {p0, v6}, Lcom/android/camera/ModePicker;->getHeight(Landroid/view/View;)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    invoke-static {}, Lcom/android/camera/Util;->isMAVSupport()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {}, Lcom/android/camera/Util;->isPANORAMASupport()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v6, v6, v4

    invoke-direct {p0, v6}, Lcom/android/camera/ModePicker;->getHeight(Landroid/view/View;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x8

    sub-int v2, v3, v6

    :goto_2
    invoke-direct {p0, p0}, Lcom/android/camera/ModePicker;->getHeight(Landroid/view/View;)I

    move-result v6

    iget-object v7, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v7, v7, v4

    invoke-direct {p0, v7}, Lcom/android/camera/ModePicker;->getTop(Landroid/view/View;)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    sub-int v0, v6, v7

    if-eqz v2, :cond_0

    if-le v2, v0, :cond_0

    move v4, v5

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v6, v6, v4

    invoke-direct {p0, v6}, Lcom/android/camera/ModePicker;->getHeight(Landroid/view/View;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x4

    sub-int v2, v3, v6

    goto :goto_2
.end method

.method private enableModeSelection(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/android/camera/ModePicker;->mSelectionEnabled:Z

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mFadeIn:Landroid/view/animation/Animation;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/ModePicker;->updateModeState()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModePicker;->mFadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private getDrawableResource(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0200da

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0200e1

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200dc

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200db

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getHeight(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method private getTop(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method private highlightView(Landroid/widget/ImageView;Z)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Z

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->clearColorFilter()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/camera/ModePicker;->DISABLED_COLOR:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method private highlightViewBackGround(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/widget/ImageView;->clearColorFilter()V

    if-eqz p2, :cond_0

    const v0, 0x7f02011f

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f020009

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private initializeLessMode()V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v1, 0xf

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/camera/ModePicker;->sIsScreenLarge:Z

    iget-boolean v1, p0, Lcom/android/camera/ModePicker;->sIsScreenLarge:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    new-array v1, v1, [Lcom/android/camera/ui/RotateImageView;

    iput-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v1, 0x7f0b006b

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v4, v2

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v1, 0x7f0b006c

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v4, v3

    iget v1, p0, Lcom/android/camera/ModePicker;->mSmallSizePixel:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, p0, Lcom/android/camera/ModePicker;->mSmallSizePixel:I

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mChooseModeView:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mChooseModeLessView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private initializeMoreMode()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mChooseModeView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    iget v0, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    new-array v0, v0, [Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v0, 0x7f0b0067

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v2, 0x1

    const v0, 0x7f0b0068

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v2, 0x2

    const v0, 0x7f0b0069

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mChooseModeView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mChooseModeLessView:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private registerOnClickListener()V
    .locals 2

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setModeVisibility(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    const/16 v1, 0x8

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v0, v0, p2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v0, v0, p2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    goto :goto_0
.end method

.method private updateModeState()V
    .locals 12

    const/16 v11, 0x8

    const/4 v10, -0x2

    const/4 v9, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v4, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v9, :cond_1

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v7, v4, v0

    iget v4, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-ne v0, v4, :cond_0

    move v4, v5

    :goto_1
    invoke-direct {p0, v7, v4}, Lcom/android/camera/ModePicker;->highlightViewBackGround(Landroid/widget/ImageView;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v4, v6

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    const/4 v7, 0x3

    if-ge v4, v7, :cond_7

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_2
    iget v4, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    if-ge v0, v4, :cond_3

    :goto_3
    if-ge v1, v9, :cond_2

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-ne v4, v11, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    if-gt v9, v1, :cond_4

    :cond_3
    return-void

    :cond_4
    iget v4, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-ne v1, v4, :cond_5

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v10, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v10, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_4
    const-string v4, "T"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "i="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " j="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mCurrentMode="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-direct {p0, v1}, Lcom/android/camera/ModePicker;->getDrawableResource(I)I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v7, v4, v0

    iget v4, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-ne v1, v4, :cond_6

    move v4, v5

    :goto_5
    invoke-direct {p0, v7, v4}, Lcom/android/camera/ModePicker;->highlightView(Landroid/widget/ImageView;Z)V

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v7, p0, Lcom/android/camera/ModePicker;->mSmallSizePixel:I

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v7, p0, Lcom/android/camera/ModePicker;->mSmallSizePixel:I

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_4

    :cond_6
    move v4, v6

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_6
    iget v4, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    if-ge v0, v4, :cond_3

    if-ne v0, v5, :cond_8

    iget v3, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    :goto_7
    iget v4, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    if-ge v3, v4, :cond_d

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-direct {p0, v3}, Lcom/android/camera/ModePicker;->getDrawableResource(I)I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v7, v4, v0

    iget v4, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-ne v3, v4, :cond_c

    move v4, v5

    :goto_8
    invoke-direct {p0, v7, v4}, Lcom/android/camera/ModePicker;->highlightView(Landroid/widget/ImageView;Z)V

    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    :goto_a
    iget v4, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    if-ge v1, v4, :cond_a

    iget v4, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-eq v1, v4, :cond_9

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-ne v4, v11, :cond_a

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_a
    add-int/lit8 v2, v1, 0x1

    move v3, v1

    iget-boolean v4, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    move v1, v2

    goto :goto_7

    :cond_b
    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    move v1, v2

    goto :goto_7

    :cond_c
    move v4, v6

    goto :goto_8

    :cond_d
    const-string v4, "ModePicker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateModeState target="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9
.end method


# virtual methods
.method public dismissModeSelection()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/camera/ModePicker;->mSelectionEnabled:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/camera/ModePicker;->enableModeSelection(Z)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/ui/PopupManager;->getInstance(Landroid/content/Context;)Lcom/android/camera/ui/PopupManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/camera/ui/PopupManager;->notifyShowPopup(Landroid/view/View;)V

    invoke-direct {p0, v2}, Lcom/android/camera/ModePicker;->enableModeSelection(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_2

    iget v1, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-eq v1, v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/camera/ModePicker;->setCurrentMode(I)V

    iput-boolean v2, p0, Lcom/android/camera/ModePicker;->mModeChanged:Z

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/camera/ModePicker;->enableModeSelection(Z)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/ModePicker;->changeToSelectedMode()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v3, 0xf

    if-eq v2, v8, :cond_0

    if-ne v2, v9, :cond_2

    :cond_0
    move v1, v5

    :goto_0
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/android/camera/ModePicker;->res:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const v6, 0x7f0b006e

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    const v3, 0x7f0b006d

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    new-instance v6, Lcom/android/camera/ModePicker$1;

    invoke-direct {v6, p0}, Lcom/android/camera/ModePicker$1;-><init>(Lcom/android/camera/ModePicker;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-array v3, v9, [Lcom/android/camera/ui/RotateImageView;

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0070

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v6, v7

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0071

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v6, v5

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0072

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v6, v4

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b006f

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v6, v8

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v3, v3, v7

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v3, v3, v5

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v3, v3, v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v3, v3, v8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v3, 0x7f0b0065

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    new-array v3, v3, [Lcom/android/camera/ui/RotateImageView;

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0067

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v6, v4

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0068

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v4, v5

    iget-object v4, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    const v3, 0x7f0b0069

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v4, v7

    const v3, 0x7f0b0065

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    :goto_1
    const v3, 0x7f0b0066

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mChooseModeView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0b006a

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mChooseModeLessView:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->registerOnClickListener()V

    return-void

    :cond_2
    move v1, v4

    goto/16 :goto_0

    :cond_3
    const v3, 0x7f0b0073

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    iget-object v3, p0, Lcom/android/camera/ModePicker;->res:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_4
    invoke-direct {p0, v5}, Lcom/android/camera/ModePicker;->enableModeSelection(Z)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v6, v5, v1

    div-int/lit8 v3, v6, 0x2

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    iget v7, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    mul-int/2addr v7, v5

    add-int v2, v6, v7

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    add-int v7, v2, v3

    sub-int v8, p5, p3

    iget-object v9, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    add-int v9, v2, v3

    add-int/2addr v9, v1

    sub-int v10, p5, p3

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/android/camera/ModePicker;->mFirstCalculate:Z

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->computeCollapse()Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    iget-boolean v6, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    if-eqz v6, :cond_1

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->updateModeState()V

    :cond_1
    return-void

    :cond_2
    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    sub-int v6, v4, v0

    div-int/lit8 v3, v6, 0x2

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    iget v7, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    mul-int/2addr v7, v4

    add-int v2, v6, v7

    iget-object v6, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    const/4 v7, 0x0

    add-int v8, v2, v3

    iget-object v9, p0, Lcom/android/camera/ModePicker;->mCurrentModeBar:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int v10, v2, v3

    add-int/2addr v10, v0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public onModeChanged(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/camera/ModePicker;->setCurrentMode(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOtherPopupShowed()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/camera/ModePicker;->dismissModeSelection()Z

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->computeCollapse()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/camera/ModePicker;->mNeedCollapse:Z

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->updateModeState()V

    :cond_0
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 6
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    const/4 v1, 0x1

    iget v5, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    if-nez v5, :cond_0

    const/4 v1, 0x0

    :cond_0
    iget-object v5, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-virtual {v4, v1}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    invoke-virtual {v4, v1}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/android/camera/ModePicker;->updateModeState()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionFrame:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->updateModeState()V

    :cond_4
    return-void
.end method

.method public setModeSupport()V
    .locals 3

    const/4 v2, 0x3

    invoke-static {}, Lcom/android/camera/Util;->isMAVSupport()Z

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/android/camera/ModePicker;->setModeVisibility(ZI)V

    invoke-static {}, Lcom/android/camera/Util;->isPANORAMASupport()Z

    move-result v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/android/camera/ModePicker;->setModeVisibility(ZI)V

    invoke-static {}, Lcom/android/camera/Util;->isVideoCameraSupport()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/camera/ModePicker;->setModeVisibility(ZI)V

    iget v0, p0, Lcom/android/camera/ModePicker;->SUPPORTED_MODE_NUM:I

    if-ge v0, v2, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ModePicker;->initializeLessMode()V

    :goto_0
    iget v0, p0, Lcom/android/camera/ModePicker;->mCurrentMode:I

    invoke-virtual {p0, v0}, Lcom/android/camera/ModePicker;->setCurrentMode(I)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/ModePicker;->initializeMoreMode()V

    goto :goto_0
.end method

.method public setOnModeChangeListener(Lcom/android/camera/ModePicker$OnModeChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ModePicker$OnModeChangeListener;

    iput-object p1, p0, Lcom/android/camera/ModePicker;->mListener:Lcom/android/camera/ModePicker$OnModeChangeListener;

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mModeSelectionIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/ui/RotateImageView;->setOrientation(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/android/camera/ModePicker;->CURRENT_MODE_NUM:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeFrame:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ModePicker;->mCurrentModeIcon:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/ui/RotateImageView;->setOrientation(IZ)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method
