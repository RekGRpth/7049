.class public Lcom/android/camera/PanoUtil;
.super Ljava/lang/Object;
.source "PanoUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateDifferenceBetweenAngles(DD)D
    .locals 10
    .param p0    # D
    .param p2    # D

    const-wide/16 v8, 0x0

    const-wide v6, 0x4076800000000000L

    sub-double v4, p2, p0

    rem-double v0, v4, v6

    cmpg-double v4, v0, v8

    if-gez v4, :cond_0

    add-double/2addr v0, v6

    :cond_0
    sub-double v4, p0, p2

    rem-double v2, v4, v6

    cmpg-double v4, v2, v8

    if-gez v4, :cond_1

    add-double/2addr v2, v6

    :cond_1
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    return-wide v4
.end method

.method public static createName(Ljava/lang/String;J)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static decodeYUV420SPQuarterRes([I[BII)V
    .locals 17
    .param p0    # [I
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    mul-int v2, p2, p3

    const/4 v5, 0x0

    const/4 v13, 0x0

    :goto_0
    move/from16 v0, p3

    if-ge v5, v0, :cond_8

    shr-int/lit8 v14, v5, 0x1

    mul-int v14, v14, p2

    add-int v8, v2, v14

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v4, 0x0

    move v9, v8

    :goto_1
    move/from16 v0, p2

    if-ge v4, v0, :cond_7

    mul-int v14, v5, p2

    add-int/2addr v14, v4

    aget-byte v14, p1, v14

    and-int/lit16 v14, v14, 0xff

    add-int/lit8 v11, v14, -0x10

    if-gez v11, :cond_0

    const/4 v11, 0x0

    :cond_0
    and-int/lit8 v14, v4, 0x1

    if-nez v14, :cond_9

    add-int/lit8 v8, v9, 0x1

    aget-byte v14, p1, v9

    and-int/lit16 v14, v14, 0xff

    add-int/lit8 v10, v14, -0x80

    add-int/lit8 v9, v8, 0x1

    aget-byte v14, p1, v8

    and-int/lit16 v14, v14, 0xff

    add-int/lit8 v7, v14, -0x80

    add-int/lit8 v8, v9, 0x2

    :goto_2
    mul-int/lit16 v12, v11, 0x4a8

    mul-int/lit16 v14, v10, 0x662

    add-int v6, v12, v14

    mul-int/lit16 v14, v10, 0x341

    sub-int v14, v12, v14

    mul-int/lit16 v15, v7, 0x190

    sub-int v3, v14, v15

    mul-int/lit16 v14, v7, 0x812

    add-int v1, v12, v14

    if-gez v6, :cond_4

    const/4 v6, 0x0

    :cond_1
    :goto_3
    if-gez v3, :cond_5

    const/4 v3, 0x0

    :cond_2
    :goto_4
    if-gez v1, :cond_6

    const/4 v1, 0x0

    :cond_3
    :goto_5
    const/high16 v14, -0x1000000

    shl-int/lit8 v15, v6, 0x6

    const/high16 v16, 0xff0000

    and-int v15, v15, v16

    or-int/2addr v14, v15

    shr-int/lit8 v15, v3, 0x2

    const v16, 0xff00

    and-int v15, v15, v16

    or-int/2addr v14, v15

    shr-int/lit8 v15, v1, 0xa

    and-int/lit16 v15, v15, 0xff

    or-int/2addr v14, v15

    aput v14, p0, v13

    add-int/lit8 v4, v4, 0x4

    add-int/lit8 v13, v13, 0x1

    move v9, v8

    goto :goto_1

    :cond_4
    const v14, 0x3ffff

    if-le v6, v14, :cond_1

    const v6, 0x3ffff

    goto :goto_3

    :cond_5
    const v14, 0x3ffff

    if-le v3, v14, :cond_2

    const v3, 0x3ffff

    goto :goto_4

    :cond_6
    const v14, 0x3ffff

    if-le v1, v14, :cond_3

    const v1, 0x3ffff

    goto :goto_5

    :cond_7
    add-int/lit8 v5, v5, 0x4

    goto/16 :goto_0

    :cond_8
    return-void

    :cond_9
    move v8, v9

    goto :goto_2
.end method
