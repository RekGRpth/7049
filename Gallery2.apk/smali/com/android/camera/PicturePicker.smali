.class public Lcom/android/camera/PicturePicker;
.super Landroid/app/Activity;
.source "PicturePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/PicturePicker$1;,
        Lcom/android/camera/PicturePicker$MainHandler;
    }
.end annotation


# static fields
.field public static final FILE_PATHS:Ljava/lang/String; = "paths"

.field private static final HEIGHT_PADDING:I = 0x28

.field private static final KEY_EV0:Ljava/lang/String; = "key_ev0"

.field private static final KEY_EVM:Ljava/lang/String; = "key_evm"

.field private static final KEY_EVP:Ljava/lang/String; = "key_evp"

.field private static final MSG_FINISH:I = 0x1

.field public static final PICTURES_TO_PICK:Ljava/lang/String; = "pictures-to-pick"

.field public static final PICTURE_COUNT:Ljava/lang/String; = "picCount"

.field private static final TAG:Ljava/lang/String; = "PicturePicker"

.field private static final WIDTH_PADDING:I = 0xc8


# instance fields
.field mEv0:Lcom/android/camera/EVPickerItem;

.field mEvm:Lcom/android/camera/EVPickerItem;

.field mEvp:Lcom/android/camera/EVPickerItem;

.field private mForceFinishing:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHasStorage:Z

.field private mPaths:[Ljava/lang/String;

.field mPictures2Pick:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/PicturePicker;->mPictures2Pick:I

    new-instance v0, Lcom/android/camera/PicturePicker$MainHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/camera/PicturePicker$MainHandler;-><init>(Lcom/android/camera/PicturePicker;Lcom/android/camera/PicturePicker$1;)V

    iput-object v0, p0, Lcom/android/camera/PicturePicker;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private evSelectCancel()V
    .locals 5

    iget-object v4, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private evSelectDone()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v4, 0x0

    new-array v3, v10, [Ljava/lang/String;

    new-array v0, v10, [Lcom/android/camera/EVPickerItem;

    iget-object v7, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    aput-object v7, v0, v11

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    aput-object v8, v0, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    aput-object v8, v0, v7

    const/4 v2, 0x0

    move v5, v4

    :goto_0
    if-ge v2, v10, :cond_2

    aget-object v7, v0, v2

    invoke-virtual {v7}, Landroid/view/View;->isSelected()Z

    move-result v7

    iget-object v8, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-direct {p0, v7, v8}, Lcom/android/camera/PicturePicker;->saveOrDelete(ZLjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v4, v5, 0x1

    iget-object v7, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v7, v7, v2

    aput-object v7, v3, v5

    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    goto :goto_0

    :cond_0
    const-string v7, "PicturePicker"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File is gone! :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v4, v5

    goto :goto_1

    :cond_2
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "picCount"

    invoke-virtual {v6, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v7, "paths"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    if-lez v5, :cond_3

    const/4 v7, -0x1

    invoke-virtual {v2, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :goto_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_3
    invoke-virtual {p0, v11}, Landroid/app/Activity;->setResult(I)V

    goto :goto_2
.end method

.method private isAnyImgSelected()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveOrDelete(ZLjava/lang/String;)Z
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/PicturePicker;->mHasStorage:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private setOnClicks()V
    .locals 3

    const v2, 0x7f0b0036

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0b0038

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setParameters(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 22
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/os/Bundle;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    if-eqz p2, :cond_0

    const-string v3, "key_ev0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v11, 0x1

    :goto_0
    const-string v3, "key_ev0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    const-string v3, "key_evp"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    const-string v3, "key_evm"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    :cond_0
    const-string v3, "paths"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    array-length v0, v3

    move/from16 v16, v0

    const-string v3, "window"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/WindowManager;

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v18, v3, -0x28

    mul-int/lit8 v3, v18, 0x4

    div-int/lit8 v19, v3, 0x3

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    add-int/lit16 v15, v3, -0xc8

    move/from16 v0, v19

    if-le v0, v15, :cond_1

    move/from16 v19, v15

    mul-int/lit8 v3, v19, 0x3

    div-int/lit8 v18, v3, 0x4

    :cond_1
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v20, v0

    fill-array-data v20, :array_0

    const/4 v7, 0x0

    const/4 v10, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v10, v0, :cond_a

    aget v3, v20, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/android/camera/EVPickerItem;

    const/4 v2, 0x0

    const-string v3, "PicturePicker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "thumb: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v3, v3, v10

    mul-int v4, v19, v18

    move/from16 v0, v19

    invoke-static {v3, v0, v4}, Lcom/android/camera/Util;->makeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    const-string v3, "PicturePicker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bmp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_3

    const-string v3, "PicturePicker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File is gone:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/PicturePicker;->mPaths:[Ljava/lang/String;

    aget-object v5, v5, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x7f0b0037

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v3, 0x4

    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/PicturePicker;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_2
    return-void

    :cond_2
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ge v3, v4, :cond_7

    if-nez v7, :cond_4

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    move/from16 v0, v18

    int-to-float v3, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    move/from16 v0, v19

    int-to-float v4, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/high16 v3, 0x43870000

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    :cond_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    aget v3, v20, v10

    const v4, 0x7f0b003b

    if-ne v3, v4, :cond_8

    if-nez v11, :cond_5

    if-eqz v12, :cond_8

    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/android/camera/EVPickerItem;->performClick()Z

    :cond_6
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x1

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v2, v0, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_3

    :cond_8
    if-eqz v13, :cond_9

    aget v3, v20, v10

    const v4, 0x7f0b003c

    if-ne v3, v4, :cond_9

    invoke-virtual/range {v17 .. v17}, Lcom/android/camera/EVPickerItem;->performClick()Z

    goto :goto_4

    :cond_9
    if-eqz v14, :cond_6

    aget v3, v20, v10

    const v4, 0x7f0b003d

    if-ne v3, v4, :cond_6

    invoke-virtual/range {v17 .. v17}, Lcom/android/camera/EVPickerItem;->performClick()Z

    goto :goto_4

    :cond_a
    if-nez v11, :cond_b

    if-nez v12, :cond_b

    if-nez v13, :cond_b

    if-nez v14, :cond_b

    const v3, 0x7f0b0037

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v3, 0x4

    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    const-string v3, "pictures-to-pick"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/camera/PicturePicker;->mPictures2Pick:I

    goto/16 :goto_2

    nop

    :array_0
    .array-data 4
        0x7f0b003c
        0x7f0b003b
        0x7f0b003d
    .end array-data
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/android/camera/PicturePicker;->evSelectDone()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/android/camera/PicturePicker;->isAnyImgSelected()Z

    move-result v2

    const v9, 0x7f0b0037

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v9, p0, Lcom/android/camera/PicturePicker;->mHasStorage:Z

    if-eqz v9, :cond_1

    if-eqz v2, :cond_3

    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget v9, p0, Lcom/android/camera/PicturePicker;->mPictures2Pick:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/android/camera/EVPickerItem;

    invoke-virtual {v9}, Landroid/view/View;->isSelected()Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x3

    new-array v3, v9, [I

    fill-array-data v3, :array_0

    move-object v0, v3

    array-length v7, v0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v7, :cond_0

    aget v4, v0, v5

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/android/camera/EVPickerItem;

    if-eq v4, v6, :cond_2

    invoke-virtual {v8}, Landroid/view/View;->isSelected()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Lcom/android/camera/EVPickerItem;->performClick()Z

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v9, 0x4

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/camera/PicturePicker;->evSelectCancel()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0036
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :array_0
    .array-data 4
        0x7f0b003b
        0x7f0b003d
        0x7f0b003c
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "PicturePicker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() getLastNonConfigurationInstance()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/camera/PicturePicker;->mForceFinishing:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-static {v2}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    const v1, 0x7f040017

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f0b003b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/EVPickerItem;

    iput-object v1, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    const v1, 0x7f0b003d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/EVPickerItem;

    iput-object v1, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    const v1, 0x7f0b003c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/EVPickerItem;

    iput-object v1, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v3

    const-wide/32 v5, 0x2faf080

    cmp-long v1, v3, v5

    if-lez v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/android/camera/PicturePicker;->mHasStorage:Z

    iget-boolean v1, p0, Lcom/android/camera/PicturePicker;->mHasStorage:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/android/camera/PicturePicker;->setParameters(Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/PicturePicker;->setOnClicks()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onResume()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v1, "PicturePicker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onResume() mForceFinishing="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/PicturePicker;->mForceFinishing:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/camera/PicturePicker;->mForceFinishing:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v6}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v1

    const-wide/32 v3, 0x2faf080

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const v1, 0x7f0b0037

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    const-string v0, "PicturePicker"

    const-string v1, "onRetainNonConfigurationInstance()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "key_ev0"

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEv0:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "key_evp"

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEvp:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "key_evm"

    iget-object v1, p0, Lcom/android/camera/PicturePicker;->mEvm:Lcom/android/camera/EVPickerItem;

    invoke-virtual {v1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
