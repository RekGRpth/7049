.class public final Lcom/android/camera/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_translucent:I = 0x7f020000

.field public static final appwidget_photo_border:I = 0x7f020001

.field public static final background:I = 0x7f020002

.field public static final background_portrait:I = 0x7f020003

.field public static final best_shot_hl:I = 0x7f020004

.field public static final best_shot_normal:I = 0x7f020005

.field public static final best_shot_press:I = 0x7f020006

.field public static final bg_camera_pattern:I = 0x7f020007

.field public static final bg_mode_picker:I = 0x7f020008

.field public static final bg_pressed:I = 0x7f020009

.field public static final bg_pressed_exit_fading:I = 0x7f02000a

.field public static final bg_switcher:I = 0x7f02000b

.field public static final bg_text_on_preview:I = 0x7f02000c

.field public static final bg_vidcontrol:I = 0x7f02000d

.field public static final border_last_picture:I = 0x7f02000e

.field public static final border_photo_frame_widget:I = 0x7f02000f

.field public static final border_photo_frame_widget_focused_holo:I = 0x7f020010

.field public static final border_photo_frame_widget_holo:I = 0x7f020011

.field public static final border_photo_frame_widget_pressed_holo:I = 0x7f020012

.field public static final btn_camera_shutter_holo:I = 0x7f020013

.field public static final btn_camera_shutter_pressed_holo:I = 0x7f020014

.field public static final btn_close_settings:I = 0x7f020015

.field public static final btn_conv_normal:I = 0x7f020016

.field public static final btn_conv_pressed:I = 0x7f020017

.field public static final btn_default_normal_holo_dark:I = 0x7f020018

.field public static final btn_default_pressed_holo_dark:I = 0x7f020019

.field public static final btn_dismiss_normal:I = 0x7f02001a

.field public static final btn_dismiss_pressed:I = 0x7f02001b

.field public static final btn_ic_3d_switch_off:I = 0x7f02001c

.field public static final btn_ic_3d_switch_on:I = 0x7f02001d

.field public static final btn_ic_camera_hdr_off:I = 0x7f02001e

.field public static final btn_ic_camera_hdr_on:I = 0x7f02001f

.field public static final btn_ic_camera_photo_asd:I = 0x7f020020

.field public static final btn_ic_camera_photo_bestshot:I = 0x7f020021

.field public static final btn_ic_camera_photo_burstshot16:I = 0x7f020022

.field public static final btn_ic_camera_photo_burstshot4:I = 0x7f020023

.field public static final btn_ic_camera_photo_burstshot8:I = 0x7f020024

.field public static final btn_ic_camera_photo_evbracketshot:I = 0x7f020025

.field public static final btn_ic_camera_photo_facebeautify:I = 0x7f020026

.field public static final btn_ic_camera_photo_normal:I = 0x7f020027

.field public static final btn_ic_camera_photo_smileshot:I = 0x7f020028

.field public static final btn_ic_cancel_normal:I = 0x7f020029

.field public static final btn_ic_cancel_pressed:I = 0x7f02002a

.field public static final btn_ic_panorama:I = 0x7f02002b

.field public static final btn_make_offline_disabled_on_holo_dark:I = 0x7f02002c

.field public static final btn_make_offline_normal_off_holo_dark:I = 0x7f02002d

.field public static final btn_make_offline_normal_on_holo_dark:I = 0x7f02002e

.field public static final btn_pan_shutter_holo:I = 0x7f02002f

.field public static final btn_pan_shutter_holo_disable:I = 0x7f020030

.field public static final btn_pan_shutter_pressed_holo:I = 0x7f020031

.field public static final btn_pan_shutter_recording_holo:I = 0x7f020032

.field public static final btn_pan_shutter_recording_pressed_holo:I = 0x7f020033

.field public static final btn_retake_shutter_holo:I = 0x7f020034

.field public static final btn_retake_shutter_pressed_holo:I = 0x7f020035

.field public static final btn_setting_picker:I = 0x7f020036

.field public static final btn_shutter:I = 0x7f020037

.field public static final btn_shutter_pan:I = 0x7f020038

.field public static final btn_shutter_pan_recording:I = 0x7f020039

.field public static final btn_shutter_retake:I = 0x7f02003a

.field public static final btn_shutter_video:I = 0x7f02003b

.field public static final btn_shutter_video_recording:I = 0x7f02003c

.field public static final btn_video_shutter_holo:I = 0x7f02003d

.field public static final btn_video_shutter_holo_disable:I = 0x7f02003e

.field public static final btn_video_shutter_pressed_holo:I = 0x7f02003f

.field public static final btn_video_shutter_recording_holo:I = 0x7f020040

.field public static final btn_video_shutter_recording_pressed_holo:I = 0x7f020041

.field public static final btn_video_snapshot:I = 0x7f020042

.field public static final btn_video_snapshot_disable:I = 0x7f020043

.field public static final btn_video_snapshot_normal:I = 0x7f020044

.field public static final btn_video_snapshot_pressed:I = 0x7f020045

.field public static final btn_wheel_close_settings:I = 0x7f020046

.field public static final cab_divider_vertical_dark:I = 0x7f020047

.field public static final camera_background:I = 0x7f020048

.field public static final camera_crop_holo:I = 0x7f020049

.field public static final color_effects_aqua:I = 0x7f02004a

.field public static final color_effects_blackboard:I = 0x7f02004b

.field public static final color_effects_mono:I = 0x7f02004c

.field public static final color_effects_negative:I = 0x7f02004d

.field public static final color_effects_none:I = 0x7f02004e

.field public static final color_effects_posterize:I = 0x7f02004f

.field public static final color_effects_sepia:I = 0x7f020050

.field public static final color_effects_solarize:I = 0x7f020051

.field public static final color_effects_spia_blue:I = 0x7f020052

.field public static final color_effects_spia_green:I = 0x7f020053

.field public static final color_effects_whiteboard:I = 0x7f020054

.field public static final dialog_full_holo_dark:I = 0x7f020055

.field public static final dropdown_ic_arrow_normal_holo_dark:I = 0x7f020056

.field public static final ev_selector_overlay:I = 0x7f020057

.field public static final focus_box:I = 0x7f020058

.field public static final frame_overlay_gallery_camera:I = 0x7f020059

.field public static final frame_overlay_gallery_folder:I = 0x7f02005a

.field public static final frame_overlay_gallery_picasa:I = 0x7f02005b

.field public static final frame_overlay_gallery_ptp:I = 0x7f02005c

.field public static final frame_overlay_gallery_stereo:I = 0x7f02005d

.field public static final grid_pressed:I = 0x7f02005e

.field public static final grid_selected:I = 0x7f02005f

.field public static final ic_2d:I = 0x7f020060

.field public static final ic_3d:I = 0x7f020061

.field public static final ic_arrows_0:I = 0x7f020062

.field public static final ic_arrows_1:I = 0x7f020063

.field public static final ic_arrows_2:I = 0x7f020064

.field public static final ic_arrows_3:I = 0x7f020065

.field public static final ic_arrows_line:I = 0x7f020066

.field public static final ic_arrows_line_collimated:I = 0x7f020067

.field public static final ic_camera_asd_auto:I = 0x7f020068

.field public static final ic_camera_asd_backlight:I = 0x7f020069

.field public static final ic_camera_asd_backlight_portrait:I = 0x7f02006a

.field public static final ic_camera_asd_landscape:I = 0x7f02006b

.field public static final ic_camera_asd_night:I = 0x7f02006c

.field public static final ic_camera_asd_night_portrait:I = 0x7f02006d

.field public static final ic_camera_asd_portrait:I = 0x7f02006e

.field public static final ic_camera_panorama_directions_down_normal:I = 0x7f02006f

.field public static final ic_camera_panorama_directions_down_select:I = 0x7f020070

.field public static final ic_camera_panorama_directions_left_normal:I = 0x7f020071

.field public static final ic_camera_panorama_directions_left_select:I = 0x7f020072

.field public static final ic_camera_panorama_directions_right_normal:I = 0x7f020073

.field public static final ic_camera_panorama_directions_right_select:I = 0x7f020074

.field public static final ic_camera_panorama_directions_up_normal:I = 0x7f020075

.field public static final ic_camera_panorama_directions_up_select:I = 0x7f020076

.field public static final ic_camera_remian_pictures:I = 0x7f020077

.field public static final ic_control_play:I = 0x7f020078

.field public static final ic_depth_far:I = 0x7f020079

.field public static final ic_depth_near:I = 0x7f02007a

.field public static final ic_dot_3:I = 0x7f02007b

.field public static final ic_dot_3_vertical:I = 0x7f02007c

.field public static final ic_effects_holo_light:I = 0x7f02007d

.field public static final ic_exposure_holo_light:I = 0x7f02007e

.field public static final ic_facebeautify_frame:I = 0x7f02007f

.field public static final ic_flash_auto_holo_light:I = 0x7f020080

.field public static final ic_flash_off_holo_light:I = 0x7f020081

.field public static final ic_flash_on_holo_light:I = 0x7f020082

.field public static final ic_focus_face_focused:I = 0x7f020083

.field public static final ic_focus_failed:I = 0x7f020084

.field public static final ic_focus_focused:I = 0x7f020085

.field public static final ic_focus_focusing:I = 0x7f020086

.field public static final ic_gallery:I = 0x7f020087

.field public static final ic_gallery_play:I = 0x7f020088

.field public static final ic_gallery_play_big:I = 0x7f020089

.field public static final ic_highlight:I = 0x7f02008a

.field public static final ic_indicators_cloudy:I = 0x7f02008b

.field public static final ic_indicators_fluorescent:I = 0x7f02008c

.field public static final ic_indicators_hdr_on:I = 0x7f02008d

.field public static final ic_indicators_incandescent:I = 0x7f02008e

.field public static final ic_indicators_landscape:I = 0x7f02008f

.field public static final ic_indicators_landscape_flash_auto:I = 0x7f020090

.field public static final ic_indicators_landscape_flash_off:I = 0x7f020091

.field public static final ic_indicators_landscape_flash_on:I = 0x7f020092

.field public static final ic_indicators_macro:I = 0x7f020093

.field public static final ic_indicators_scn:I = 0x7f020094

.field public static final ic_indicators_shade:I = 0x7f020095

.field public static final ic_indicators_sunlight:I = 0x7f020096

.field public static final ic_indicators_tungsten:I = 0x7f020097

.field public static final ic_indicators_twlight:I = 0x7f020098

.field public static final ic_indicators_warmfluorescent:I = 0x7f020099

.field public static final ic_lockscreen_chevron_up:I = 0x7f02009a

.field public static final ic_manage_pin:I = 0x7f02009b

.field public static final ic_mav_gesture_hand:I = 0x7f02009c

.field public static final ic_mav_overlay:I = 0x7f02009d

.field public static final ic_media_bigscreen:I = 0x7f02009e

.field public static final ic_media_cropscreen:I = 0x7f02009f

.field public static final ic_media_fullscreen:I = 0x7f0200a0

.field public static final ic_menu_camera_holo_light:I = 0x7f0200a1

.field public static final ic_menu_camera_video_view:I = 0x7f0200a2

.field public static final ic_menu_cancel_holo_light:I = 0x7f0200a3

.field public static final ic_menu_detail:I = 0x7f0200a4

.field public static final ic_menu_display_bookmark:I = 0x7f0200a5

.field public static final ic_menu_done_holo_light:I = 0x7f0200a6

.field public static final ic_menu_info_details:I = 0x7f0200a7

.field public static final ic_menu_loop:I = 0x7f0200a8

.field public static final ic_menu_make_offline:I = 0x7f0200a9

.field public static final ic_menu_overflow:I = 0x7f0200aa

.field public static final ic_menu_ptp_holo_light:I = 0x7f0200ab

.field public static final ic_menu_revert_holo_dark:I = 0x7f0200ac

.field public static final ic_menu_save_holo_light:I = 0x7f0200ad

.field public static final ic_menu_share_holo_light:I = 0x7f0200ae

.field public static final ic_menu_single_track:I = 0x7f0200af

.field public static final ic_menu_slideshow_holo_light:I = 0x7f0200b0

.field public static final ic_menu_stereo:I = 0x7f0200b1

.field public static final ic_menu_stop:I = 0x7f0200b2

.field public static final ic_menu_trash_holo_light:I = 0x7f0200b3

.field public static final ic_menu_unloop:I = 0x7f0200b4

.field public static final ic_menuselect_camera_facing_back:I = 0x7f0200b5

.field public static final ic_menuselect_camera_facing_front:I = 0x7f0200b6

.field public static final ic_menuselect_gps_off:I = 0x7f0200b7

.field public static final ic_menuselect_gps_on:I = 0x7f0200b8

.field public static final ic_pan_border_fast:I = 0x7f0200b9

.field public static final ic_pan_left_indicator:I = 0x7f0200ba

.field public static final ic_pan_left_indicator_fast:I = 0x7f0200bb

.field public static final ic_pan_progression:I = 0x7f0200bc

.field public static final ic_pan_recording_indicator:I = 0x7f0200bd

.field public static final ic_pan_right_indicator:I = 0x7f0200be

.field public static final ic_pan_right_indicator_fast:I = 0x7f0200bf

.field public static final ic_pan_thumb:I = 0x7f0200c0

.field public static final ic_pano_collimated_window:I = 0x7f0200c1

.field public static final ic_pano_collimated_window_item1:I = 0x7f0200c2

.field public static final ic_pano_collimated_window_item2:I = 0x7f0200c3

.field public static final ic_pano_down:I = 0x7f0200c4

.field public static final ic_pano_down_fixed:I = 0x7f0200c5

.field public static final ic_pano_left:I = 0x7f0200c6

.field public static final ic_pano_left_fixed:I = 0x7f0200c7

.field public static final ic_pano_navi_window:I = 0x7f0200c8

.field public static final ic_pano_normal_window:I = 0x7f0200c9

.field public static final ic_pano_right:I = 0x7f0200ca

.field public static final ic_pano_right_fixed:I = 0x7f0200cb

.field public static final ic_pano_up:I = 0x7f0200cc

.field public static final ic_pano_up_fixed:I = 0x7f0200cd

.field public static final ic_panorama_block:I = 0x7f0200ce

.field public static final ic_panorama_block_highlight:I = 0x7f0200cf

.field public static final ic_quality_1080p:I = 0x7f0200d0

.field public static final ic_quality_480p:I = 0x7f0200d1

.field public static final ic_quality_720p:I = 0x7f0200d2

.field public static final ic_recording_indicator:I = 0x7f0200d3

.field public static final ic_recording_pause_indicator:I = 0x7f0200d4

.field public static final ic_scn_holo_light:I = 0x7f0200d5

.field public static final ic_settings_holo_light:I = 0x7f0200d6

.field public static final ic_shot_border:I = 0x7f0200d7

.field public static final ic_snapshot_border:I = 0x7f0200d8

.field public static final ic_stereo_overlay:I = 0x7f0200d9

.field public static final ic_switch_camera_holo_light:I = 0x7f0200da

.field public static final ic_switch_mav_holo_light:I = 0x7f0200db

.field public static final ic_switch_pan_holo_light:I = 0x7f0200dc

.field public static final ic_switch_photo_facing_holo_light:I = 0x7f0200dd

.field public static final ic_switch_to_2d:I = 0x7f0200de

.field public static final ic_switch_to_3d:I = 0x7f0200df

.field public static final ic_switch_video_facing_holo_light:I = 0x7f0200e0

.field public static final ic_switch_video_holo_light:I = 0x7f0200e1

.field public static final ic_timelapse_1:I = 0x7f0200e2

.field public static final ic_timelapse_10:I = 0x7f0200e3

.field public static final ic_timelapse_1_5:I = 0x7f0200e4

.field public static final ic_timelapse_2:I = 0x7f0200e5

.field public static final ic_timelapse_2_5:I = 0x7f0200e6

.field public static final ic_timelapse_3:I = 0x7f0200e7

.field public static final ic_timelapse_5:I = 0x7f0200e8

.field public static final ic_timelapse_none:I = 0x7f0200e9

.field public static final ic_triangle:I = 0x7f0200ea

.field public static final ic_vidcontrol_pause:I = 0x7f0200eb

.field public static final ic_vidcontrol_play:I = 0x7f0200ec

.field public static final ic_vidcontrol_reload:I = 0x7f0200ed

.field public static final ic_video_effects_background_fields_of_wheat_holo:I = 0x7f0200ee

.field public static final ic_video_effects_background_intergalactic_holo:I = 0x7f0200ef

.field public static final ic_video_effects_background_normal_holo_dark:I = 0x7f0200f0

.field public static final ic_video_effects_faces_big_eyes_holo_dark:I = 0x7f0200f1

.field public static final ic_video_effects_faces_big_mouth_holo_dark:I = 0x7f0200f2

.field public static final ic_video_effects_faces_big_nose_holo_dark:I = 0x7f0200f3

.field public static final ic_video_effects_faces_small_eyes_holo_dark:I = 0x7f0200f4

.field public static final ic_video_effects_faces_small_mouth_holo_dark:I = 0x7f0200f5

.field public static final ic_video_effects_faces_squeeze_holo_dark:I = 0x7f0200f6

.field public static final ic_video_layout_2d:I = 0x7f0200f7

.field public static final ic_video_layout_sbs_swap:I = 0x7f0200f8

.field public static final ic_video_layout_side_by_side:I = 0x7f0200f9

.field public static final ic_video_layout_tab_swap:I = 0x7f0200fa

.field public static final ic_video_layout_top_and_bottom:I = 0x7f0200fb

.field public static final ic_video_quality:I = 0x7f0200fc

.field public static final ic_video_thumb:I = 0x7f0200fd

.field public static final ic_viewfinder_camera_facing_back:I = 0x7f0200fe

.field public static final ic_viewfinder_camera_facing_front:I = 0x7f0200ff

.field public static final ic_viewfinder_empty:I = 0x7f020100

.field public static final ic_viewfinder_gps_no_signal:I = 0x7f020101

.field public static final ic_viewfinder_gps_off:I = 0x7f020102

.field public static final ic_viewfinder_gps_on:I = 0x7f020103

.field public static final ic_white_balance_auto_holo_light:I = 0x7f020104

.field public static final ic_white_balance_cloudy_holo_light:I = 0x7f020105

.field public static final ic_white_balance_fluorescent_holo_light:I = 0x7f020106

.field public static final ic_white_balance_incandescent_holo_light:I = 0x7f020107

.field public static final ic_white_balance_shade_holo_light:I = 0x7f020108

.field public static final ic_white_balance_sunlight_holo_light:I = 0x7f020109

.field public static final ic_white_balance_tungsten_holo_light:I = 0x7f02010a

.field public static final ic_white_balance_twlight_holo_light:I = 0x7f02010b

.field public static final ic_white_balance_warmfluorescent_holo_light:I = 0x7f02010c

.field public static final ic_zoom_big:I = 0x7f02010d

.field public static final ic_zoom_big_dark:I = 0x7f02010e

.field public static final ic_zoom_in:I = 0x7f02010f

.field public static final ic_zoom_in_holo_dark:I = 0x7f020110

.field public static final ic_zoom_in_holo_light:I = 0x7f020111

.field public static final ic_zoom_out:I = 0x7f020112

.field public static final ic_zoom_out_holo_dark:I = 0x7f020113

.field public static final ic_zoom_out_holo_light:I = 0x7f020114

.field public static final ic_zoom_slider:I = 0x7f020115

.field public static final icn_media_pause:I = 0x7f020116

.field public static final icn_media_pause_focused_holo_dark:I = 0x7f020117

.field public static final icn_media_pause_normal_holo_dark:I = 0x7f020118

.field public static final icn_media_pause_pressed_holo_dark:I = 0x7f020119

.field public static final icn_media_play:I = 0x7f02011a

.field public static final icn_media_play_focused_holo_dark:I = 0x7f02011b

.field public static final icn_media_play_normal_holo_dark:I = 0x7f02011c

.field public static final icn_media_play_pressed_holo_dark:I = 0x7f02011d

.field public static final list_divider:I = 0x7f02011e

.field public static final list_pressed_holo_light:I = 0x7f02011f

.field public static final mav_dismiss_button_bg:I = 0x7f020120

.field public static final media_default_bkg:I = 0x7f020121

.field public static final on_screen_hint_frame:I = 0x7f020122

.field public static final overscroll_edge:I = 0x7f020123

.field public static final overscroll_glow:I = 0x7f020124

.field public static final panel_undo_holo:I = 0x7f020125

.field public static final pano_direction_left_indicator:I = 0x7f020126

.field public static final pano_direction_right_indicator:I = 0x7f020127

.field public static final photoeditor_actionbar_translucent:I = 0x7f020128

.field public static final photoeditor_actionbar_translucent_bottom:I = 0x7f020129

.field public static final photoeditor_artistic:I = 0x7f02012a

.field public static final photoeditor_color:I = 0x7f02012b

.field public static final photoeditor_effect_autofix:I = 0x7f02012c

.field public static final photoeditor_effect_crop:I = 0x7f02012d

.field public static final photoeditor_effect_crossprocess:I = 0x7f02012e

.field public static final photoeditor_effect_documentary:I = 0x7f02012f

.field public static final photoeditor_effect_doodle:I = 0x7f020130

.field public static final photoeditor_effect_duotone:I = 0x7f020131

.field public static final photoeditor_effect_facelift:I = 0x7f020132

.field public static final photoeditor_effect_facetan:I = 0x7f020133

.field public static final photoeditor_effect_filllight:I = 0x7f020134

.field public static final photoeditor_effect_fisheye:I = 0x7f020135

.field public static final photoeditor_effect_flip:I = 0x7f020136

.field public static final photoeditor_effect_grain:I = 0x7f020137

.field public static final photoeditor_effect_grayscale:I = 0x7f020138

.field public static final photoeditor_effect_highlight:I = 0x7f020139

.field public static final photoeditor_effect_lomoish:I = 0x7f02013a

.field public static final photoeditor_effect_negative:I = 0x7f02013b

.field public static final photoeditor_effect_posterize:I = 0x7f02013c

.field public static final photoeditor_effect_redeye:I = 0x7f02013d

.field public static final photoeditor_effect_rotate:I = 0x7f02013e

.field public static final photoeditor_effect_saturation:I = 0x7f02013f

.field public static final photoeditor_effect_sepia:I = 0x7f020140

.field public static final photoeditor_effect_shadow:I = 0x7f020141

.field public static final photoeditor_effect_sharpen:I = 0x7f020142

.field public static final photoeditor_effect_straighten:I = 0x7f020143

.field public static final photoeditor_effect_temperature:I = 0x7f020144

.field public static final photoeditor_effect_tint:I = 0x7f020145

.field public static final photoeditor_effect_vignette:I = 0x7f020146

.field public static final photoeditor_exposure:I = 0x7f020147

.field public static final photoeditor_fix:I = 0x7f020148

.field public static final photoeditor_redo:I = 0x7f020149

.field public static final photoeditor_scale_seekbar_color:I = 0x7f02014a

.field public static final photoeditor_scale_seekbar_generic:I = 0x7f02014b

.field public static final photoeditor_scale_seekbar_light:I = 0x7f02014c

.field public static final photoeditor_scale_seekbar_shadow:I = 0x7f02014d

.field public static final photoeditor_seekbar_thumb:I = 0x7f02014e

.field public static final photoeditor_tab_selected_focused_holo:I = 0x7f02014f

.field public static final photoeditor_tab_selected_holo:I = 0x7f020150

.field public static final photoeditor_tab_selected_pressed_holo:I = 0x7f020151

.field public static final photoeditor_tab_unselected_focused_holo:I = 0x7f020152

.field public static final photoeditor_tab_unselected_pressed_holo:I = 0x7f020153

.field public static final photoeditor_toggle_button_background:I = 0x7f020154

.field public static final photoeditor_undo:I = 0x7f020155

.field public static final popup_full_dark:I = 0x7f020156

.field public static final preview:I = 0x7f020157

.field public static final progress_bg_holo_dark:I = 0x7f020158

.field public static final progress_primary_holo_dark:I = 0x7f020159

.field public static final progress_secondary_holo_dark:I = 0x7f02015a

.field public static final s3d_conv_btn_bg:I = 0x7f02015b

.field public static final scrubber_knob:I = 0x7f02015c

.field public static final seekbar_line:I = 0x7f02015d

.field public static final setting_picker:I = 0x7f02015e

.field public static final smile_cancel_btn:I = 0x7f02015f

.field public static final spinner_76_inner_holo:I = 0x7f020160

.field public static final spinner_76_outer_holo:I = 0x7f020161

.field public static final toast_frame_holo:I = 0x7f020162

.field public static final wallpaper_picker_preview:I = 0x7f020163

.field public static final zoom_slider_bar:I = 0x7f020164


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
