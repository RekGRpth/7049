.class public Lcom/android/camera/SelfTimerManager;
.super Ljava/lang/Object;
.source "SelfTimerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/SelfTimerManager$SelfTimerListener;
    }
.end annotation


# static fields
.field private static final MAX_DELEY_TIME:I = 0x2710

.field private static final MSG_SELFTIMER_TIMEOUT:I = 0x9

.field private static final SELF_TIMER_INTERVAL:I = 0xfa

.field private static final SELF_TIMER_SHORT_BOUND:I = 0x7d0

.field private static final SELF_TIMER_VOLUME:I = 0x64

.field private static final STATE_SELF_TIMER_COUNTING:I = 0x1

.field private static final STATE_SELF_TIMER_IDLE:I = 0x0

.field private static final STATE_SELF_TIMER_SNAP:I = 0x2


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mLowStorageTag:Z

.field private mSelfTimerDuration:I

.field private mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

.field private mSelfTimerState:I

.field private mSelfTimerTone:Landroid/media/ToneGenerator;

.field private mTimeSelfTimerStart:J


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1    # Landroid/os/Looper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "SelfTimerManager"

    iput-object v0, p0, Lcom/android/camera/SelfTimerManager;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/SelfTimerManager;->mLowStorageTag:Z

    new-instance v0, Lcom/android/camera/SelfTimerManager$1;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/SelfTimerManager$1;-><init>(Lcom/android/camera/SelfTimerManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/camera/SelfTimerManager;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/camera/SelfTimerManager;->initSelfTimerTone()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/SelfTimerManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/SelfTimerManager;

    invoke-direct {p0}, Lcom/android/camera/SelfTimerManager;->selfTimerTimeout()V

    return-void
.end method

.method private initSelfTimerTone()V
    .locals 4

    :try_start_0
    new-instance v1, Landroid/media/ToneGenerator;

    const/4 v2, 0x1

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SelfTimerManager"

    const-string v2, "Exception caught while creating tone generator: "

    invoke-static {v1, v2, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;

    goto :goto_0
.end method

.method private declared-synchronized selfTimerStart()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/SelfTimerManager;->mLowStorageTag:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/SelfTimerManager;->mTimeSelfTimerStart:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    invoke-direct {p0}, Lcom/android/camera/SelfTimerManager;->selfTimerTimeout()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized selfTimerStop()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "SelfTimerManager"

    const-string v1, "selfTimerStop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    invoke-interface {v0}, Lcom/android/camera/SelfTimerManager$SelfTimerListener;->onTimerStop()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized selfTimerTimeout()V
    .locals 14

    const-wide/16 v12, 0x7d0

    const-wide/16 v6, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/android/camera/SelfTimerManager;->mTimeSelfTimerStart:J

    sub-long v2, v8, v10

    iget v8, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    int-to-long v8, v8

    cmp-long v8, v8, v2

    if-lez v8, :cond_1

    iget v8, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    int-to-long v8, v8

    sub-long v4, v8, v2

    :goto_0
    cmp-long v8, v4, v12

    if-ltz v8, :cond_2

    sub-long v0, v4, v12

    :goto_1
    iget-object v6, p0, Lcom/android/camera/SelfTimerManager;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x9

    invoke-virtual {v6, v7, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;

    const/16 v7, 0x9

    const/16 v8, 0x64

    invoke-virtual {v6, v7, v8}, Landroid/media/ToneGenerator;->startTone(II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move-wide v4, v6

    goto :goto_0

    :cond_2
    cmp-long v6, v4, v6

    if-eqz v6, :cond_3

    const-wide/16 v0, 0xfa

    goto :goto_1

    :cond_3
    const/4 v6, 0x2

    :try_start_1
    iput v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    iget-object v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    invoke-interface {v6}, Lcom/android/camera/SelfTimerManager$SelfTimerListener;->onTimerTimeout()V

    :cond_4
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method


# virtual methods
.method public declared-synchronized breakTimer()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    invoke-interface {v0}, Lcom/android/camera/SelfTimerManager$SelfTimerListener;->onTimerStop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public checkSelfTimerMode()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/SelfTimerManager;->selfTimerStart()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clerSelfTimerState()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-ne v1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/SelfTimerManager;->selfTimerStop()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelfTimerCounting()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelfTimerEnabled()Z
    .locals 1

    iget v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseTone()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerTone:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->release()V

    :cond_0
    return-void
.end method

.method public setLowStorage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/SelfTimerManager;->mLowStorageTag:Z

    invoke-virtual {p0}, Lcom/android/camera/SelfTimerManager;->breakTimer()V

    return-void
.end method

.method public setSelfTimerDuration(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    const/16 v1, 0x2710

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "invalid self timer delay"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iput v0, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerDuration:I

    return-void
.end method

.method public setTimerListener(Lcom/android/camera/SelfTimerManager$SelfTimerListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    iput-object p1, p0, Lcom/android/camera/SelfTimerManager;->mSelfTimerListener:Lcom/android/camera/SelfTimerManager$SelfTimerListener;

    return-void
.end method
