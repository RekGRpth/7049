.class public Lcom/android/camera/SwitchAnimManager;
.super Ljava/lang/Object;
.source "SwitchAnimManager.java"


# static fields
.field private static final ANIMATION_DURATION:F = 400.0f

.field public static final INITIAL_DARKEN_ALPHA:F = 0.8f

.field private static final TAG:Ljava/lang/String; = "SwitchAnimManager"

.field private static final ZOOM_DELTA_PREVIEW:F = 0.2f

.field private static final ZOOM_DELTA_REVIEW:F = 0.5f


# instance fields
.field private mAnimStartTime:J

.field private mPreviewFrameLayoutWidth:I

.field private mReviewDrawingHeight:I

.field private mReviewDrawingWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drawAnimation(Lcom/android/gallery3d/ui/GLCanvas;IIIILcom/android/camera/CameraScreenNail;Lcom/android/gallery3d/ui/RawTexture;)Z
    .locals 25
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/camera/CameraScreenNail;
    .param p7    # Lcom/android/gallery3d/ui/RawTexture;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/camera/SwitchAnimManager;->mAnimStartTime:J

    sub-long v23, v2, v6

    move-wide/from16 v0, v23

    long-to-float v2, v0

    const/high16 v3, 0x43c80000

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    move-wide/from16 v0, v23

    long-to-float v2, v0

    const/high16 v3, 0x43c80000

    div-float v15, v2, v3

    move/from16 v0, p2

    int-to-float v2, v0

    move/from16 v0, p4

    int-to-float v3, v0

    const/high16 v6, 0x40000000

    div-float/2addr v3, v6

    add-float v13, v2, v3

    move/from16 v0, p3

    int-to-float v2, v0

    move/from16 v0, p5

    int-to-float v3, v0

    const/high16 v6, 0x40000000

    div-float/2addr v3, v6

    add-float v14, v2, v3

    const/high16 v2, 0x3f800000

    const v3, 0x3e4ccccd

    const/high16 v6, 0x3f800000

    sub-float/2addr v6, v15

    mul-float/2addr v3, v6

    sub-float v16, v2, v3

    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v18, v2, v16

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v17, v2, v16

    const/high16 v2, 0x40000000

    div-float v2, v18, v2

    sub-float v2, v13, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/high16 v2, 0x40000000

    div-float v2, v17, v2

    sub-float v2, v14, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    const/high16 v2, 0x3f800000

    const/high16 v3, 0x3f000000

    mul-float/2addr v3, v15

    add-float v19, v2, v3

    const/high16 v22, 0x3f800000

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/SwitchAnimManager;->mPreviewFrameLayoutWidth:I

    if-eqz v2, :cond_1

    move/from16 v0, p4

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/SwitchAnimManager;->mPreviewFrameLayoutWidth:I

    int-to-float v3, v3

    div-float v22, v2, v3

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingWidth:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    mul-float v21, v2, v22

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingHeight:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    mul-float v20, v2, v22

    const/high16 v2, 0x40000000

    div-float v2, v21, v2

    sub-float v2, v13, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v8

    const/high16 v2, 0x40000000

    div-float v2, v20, v2

    sub-float v2, v14, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-interface/range {p1 .. p1}, Lcom/android/gallery3d/ui/GLCanvas;->getAlpha()F

    move-result v12

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p6

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/camera/CameraScreenNail;->directDraw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    const/high16 v2, 0x3f800000

    sub-float/2addr v2, v15

    const v3, 0x3f4ccccd

    mul-float/2addr v2, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v11

    move-object/from16 v6, p7

    move-object/from16 v7, p1

    invoke-virtual/range {v6 .. v11}, Lcom/android/gallery3d/ui/RawTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_1
    const-string v2, "SwitchAnimManager"

    const-string v3, "mPreviewFrameLayoutWidth is 0."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public drawDarkPreview(Lcom/android/gallery3d/ui/GLCanvas;IIIILcom/android/gallery3d/ui/RawTexture;)Z
    .locals 13
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/ui/RawTexture;

    int-to-float v1, p2

    move/from16 v0, p4

    int-to-float v2, v0

    const/high16 v5, 0x40000000

    div-float/2addr v2, v5

    add-float v8, v1, v2

    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p5

    int-to-float v2, v0

    const/high16 v5, 0x40000000

    div-float/2addr v2, v5

    add-float v9, v1, v2

    const/high16 v12, 0x3f800000

    iget v1, p0, Lcom/android/camera/SwitchAnimManager;->mPreviewFrameLayoutWidth:I

    if-eqz v1, :cond_0

    move/from16 v0, p4

    int-to-float v1, v0

    iget v2, p0, Lcom/android/camera/SwitchAnimManager;->mPreviewFrameLayoutWidth:I

    int-to-float v2, v2

    div-float v12, v1, v2

    :goto_0
    iget v1, p0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingWidth:I

    int-to-float v1, v1

    mul-float v11, v1, v12

    iget v1, p0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingHeight:I

    int-to-float v1, v1

    mul-float v10, v1, v12

    const/high16 v1, 0x40000000

    div-float v1, v11, v1

    sub-float v1, v8, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/high16 v1, 0x40000000

    div-float v1, v10, v1

    sub-float v1, v9, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->getAlpha()F

    move-result v7

    const v1, 0x3f4ccccd

    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object/from16 v1, p6

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/android/gallery3d/ui/RawTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    invoke-interface {p1, v7}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    const/4 v1, 0x1

    return v1

    :cond_0
    const-string v1, "SwitchAnimManager"

    const-string v2, "mPreviewFrameLayoutWidth is 0."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPreviewFrameLayoutSize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/camera/SwitchAnimManager;->mPreviewFrameLayoutWidth:I

    return-void
.end method

.method public setReviewDrawingSize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingWidth:I

    iput p2, p0, Lcom/android/camera/SwitchAnimManager;->mReviewDrawingHeight:I

    return-void
.end method

.method public startAnimation()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/SwitchAnimManager;->mAnimStartTime:J

    return-void
.end method
