.class Lcom/android/camera/Thumbnail$Media;
.super Ljava/lang/Object;
.source "Thumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/Thumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Media"
.end annotation


# instance fields
.field public final dateTaken:J

.field public final id:J

.field public final orientation:I

.field public final stereo3dType:I

.field public final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(JIJLandroid/net/Uri;I)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # J
    .param p6    # Landroid/net/Uri;
    .param p7    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/android/camera/Thumbnail$Media;->id:J

    iput p3, p0, Lcom/android/camera/Thumbnail$Media;->orientation:I

    iput-wide p4, p0, Lcom/android/camera/Thumbnail$Media;->dateTaken:J

    iput-object p6, p0, Lcom/android/camera/Thumbnail$Media;->uri:Landroid/net/Uri;

    iput p7, p0, Lcom/android/camera/Thumbnail$Media;->stereo3dType:I

    return-void
.end method
