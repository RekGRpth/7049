.class Lcom/android/camera/VideoCamera$5;
.super Ljava/lang/Object;
.source "VideoCamera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/VideoCamera;->stopVideoRecordingAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/VideoCamera;


# direct methods
.method constructor <init>(Lcom/android/camera/VideoCamera;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x0

    const-string v4, "videocamera"

    const-string v5, "mVideoSavingTask Start -->"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$500(Lcom/android/camera/VideoCamera;)Z

    move-result v4

    if-eqz v4, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$2700(Lcom/android/camera/VideoCamera;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$2800(Lcom/android/camera/VideoCamera;)Lcom/android/camera/EffectsRecorder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/EffectsRecorder;->stopRecording()V

    :goto_0
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    iget-object v5, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v5}, Lcom/android/camera/VideoCamera;->access$6500(Lcom/android/camera/VideoCamera;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/VideoCamera;->access$6402(Lcom/android/camera/VideoCamera;Ljava/lang/String;)Ljava/lang/String;

    const-string v4, "videocamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting current video filename: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v6}, Lcom/android/camera/VideoCamera;->access$6400(Lcom/android/camera/VideoCamera;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    iget-object v4, v4, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->normalHandlerCapacity()V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    iget-object v4, v4, Lcom/android/camera/ActivityBase;->mAppBridge:Lcom/android/camera/ActivityBase$MyAppBridge;

    invoke-virtual {v4, v7}, Lcom/android/camera/ActivityBase$MyAppBridge;->renderFullPictureOnly(Z)V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4, v7}, Lcom/android/camera/VideoCamera;->access$502(Lcom/android/camera/VideoCamera;Z)Z

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$2700(Lcom/android/camera/VideoCamera;)Z

    move-result v1

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$2400(Lcom/android/camera/VideoCamera;)Z

    move-result v4

    if-eqz v4, :cond_7

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$5100(Lcom/android/camera/VideoCamera;)I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6700(Lcom/android/camera/VideoCamera;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/android/camera/VideoCamera;->access$5102(Lcom/android/camera/VideoCamera;I)I

    :cond_1
    :goto_2
    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6800(Lcom/android/camera/VideoCamera;)V

    :cond_2
    if-eqz v3, :cond_3

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6900(Lcom/android/camera/VideoCamera;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4, v8}, Lcom/android/camera/VideoCamera;->access$5102(Lcom/android/camera/VideoCamera;I)I

    :cond_3
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$7000(Lcom/android/camera/VideoCamera;)Ljava/lang/Thread;

    move-result-object v5

    monitor-enter v5

    if-nez v1, :cond_4

    :try_start_1
    const-string v4, "videocamera"

    const-string v6, "MediaRecorder.stop() done, notifyAll"

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$7000(Lcom/android/camera/VideoCamera;)Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$1400(Lcom/android/camera/VideoCamera;)Landroid/os/Handler;

    move-result-object v4

    iget-object v6, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v6}, Lcom/android/camera/VideoCamera;->access$7100(Lcom/android/camera/VideoCamera;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$1400(Lcom/android/camera/VideoCamera;)Landroid/os/Handler;

    move-result-object v4

    iget-object v6, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v6}, Lcom/android/camera/VideoCamera;->access$7100(Lcom/android/camera/VideoCamera;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    const-string v4, "videocamera"

    const-string v6, "<-- Quit mVideoSavingTask"

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_5
    :try_start_2
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6300(Lcom/android/camera/VideoCamera;)Landroid/media/MediaRecorder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6300(Lcom/android/camera/VideoCamera;)Landroid/media/MediaRecorder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6300(Lcom/android/camera/VideoCamera;)Landroid/media/MediaRecorder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->stop()V

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6300(Lcom/android/camera/VideoCamera;)Landroid/media/MediaRecorder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOnCameraReleasedListener(Landroid/media/MediaRecorder$OnInfoListener;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v3, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v4, "videocamera"

    const-string v5, "stop fail"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4}, Lcom/android/camera/VideoCamera;->access$6500(Lcom/android/camera/VideoCamera;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    iget-object v5, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v5}, Lcom/android/camera/VideoCamera;->access$6500(Lcom/android/camera/VideoCamera;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/VideoCamera;->access$6600(Lcom/android/camera/VideoCamera;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/android/camera/VideoCamera;->access$5102(Lcom/android/camera/VideoCamera;I)I

    goto/16 :goto_2

    :cond_7
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/android/camera/VideoCamera$5;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v4, v8}, Lcom/android/camera/VideoCamera;->access$5102(Lcom/android/camera/VideoCamera;I)I

    goto/16 :goto_2

    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4
.end method
