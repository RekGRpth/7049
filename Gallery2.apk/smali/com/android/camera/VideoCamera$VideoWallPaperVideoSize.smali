.class public Lcom/android/camera/VideoCamera$VideoWallPaperVideoSize;
.super Ljava/lang/Object;
.source "VideoCamera.java"

# interfaces
.implements Lcom/android/camera/VideoCamera$VideoSizeManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/VideoCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VideoWallPaperVideoSize"
.end annotation


# instance fields
.field private mVideoAspectRatio:F

.field final synthetic this$0:Lcom/android/camera/VideoCamera;


# direct methods
.method public constructor <init>(Lcom/android/camera/VideoCamera;F)V
    .locals 0
    .param p2    # F

    iput-object p1, p0, Lcom/android/camera/VideoCamera$VideoWallPaperVideoSize;->this$0:Lcom/android/camera/VideoCamera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/camera/VideoCamera$VideoWallPaperVideoSize;->mVideoAspectRatio:F

    return-void
.end method


# virtual methods
.method public getVideoSize(Landroid/media/CamcorderProfile;Ljava/util/List;I)V
    .locals 5
    .param p1    # Landroid/media/CamcorderProfile;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/CamcorderProfile;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;I)V"
        }
    .end annotation

    const/16 v4, 0x280

    const/16 v3, 0x170

    const/16 v2, 0x1e0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/VideoCamera$VideoWallPaperVideoSize;->this$0:Lcom/android/camera/VideoCamera;

    invoke-static {v0}, Lcom/android/camera/VideoCamera;->access$900(Lcom/android/camera/VideoCamera;)I

    move-result v0

    if-nez v0, :cond_2

    rem-int/lit16 v0, p3, 0xb4

    if-nez v0, :cond_1

    iput v3, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iput v2, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    :cond_0
    :goto_0
    const-string v0, "videocamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VideoSizeManager videoFrameWidth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " videoFrameHeight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iput v4, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iput v2, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_0

    :cond_2
    rem-int/lit16 v0, p3, 0xb4

    if-nez v0, :cond_3

    iput v4, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iput v2, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_0

    :cond_3
    iput v3, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iput v2, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_0
.end method
