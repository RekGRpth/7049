.class Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;
.super Landroid/widget/SimpleAdapter;
.source "BasicSettingPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ui/BasicSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BasicSettingAdapter"
.end annotation


# instance fields
.field private mItemEnabled:[Z

.field final synthetic this$0:Lcom/android/camera/ui/BasicSettingPopup;


# direct methods
.method public constructor <init>(Lcom/android/camera/ui/BasicSettingPopup;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[II)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .param p5    # [Ljava/lang/String;
    .param p6    # [I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[II)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->this$0:Lcom/android/camera/ui/BasicSettingPopup;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    new-array v0, p7, [Z

    iput-object v0, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    const/4 v6, 0x0

    :goto_0
    if-ge v6, p7, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x3e99999a

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    aget-boolean v0, v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/SimpleAdapter;->setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateItemState(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/camera/ui/BasicSettingPopup$BasicSettingAdapter;->mItemEnabled:[Z

    aput-boolean p2, v0, p1

    return-void
.end method
