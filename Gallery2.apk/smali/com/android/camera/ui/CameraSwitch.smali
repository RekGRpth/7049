.class public Lcom/android/camera/ui/CameraSwitch;
.super Lcom/android/camera/ui/RotateImageView;
.source "CameraSwitch.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraSwitch"


# instance fields
.field private mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

.field private mPreference:Lcom/android/camera/IconListPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/RotateImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public initializeIndicatorPref(Lcom/android/camera/IconListPreference;)V
    .locals 2
    .param p1    # Lcom/android/camera/IconListPreference;

    const-string v0, "CameraSwitch"

    const-string v1, "initializeIndicatorPref"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {p0}, Lcom/android/camera/ui/CameraSwitch;->reloadPreference()V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const-string v1, "CameraSwitch"

    const-string v2, "Fail to find switch camera"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->print()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v2, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    rsub-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    invoke-virtual {p0}, Lcom/android/camera/ui/CameraSwitch;->reloadPreference()V

    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ui/CameraSwitch;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    invoke-interface {v1}, Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;->onCameraSwitchClicked()V

    goto :goto_0
.end method

.method public reloadPreference()V
    .locals 4

    iget-object v2, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getLargeIconIds()[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v3, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v3}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    aget v2, v0, v1

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ui/CameraSwitch;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getSingleIcon()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setSettingChangedListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iput-object p1, p0, Lcom/android/camera/ui/CameraSwitch;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    return-void
.end method
