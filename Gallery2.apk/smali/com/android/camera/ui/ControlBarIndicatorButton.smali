.class public Lcom/android/camera/ui/ControlBarIndicatorButton;
.super Lcom/android/camera/ui/AbstractIndicatorButton;
.source "ControlBarIndicatorButton.java"

# interfaces
.implements Lcom/android/camera/ui/BasicSettingPopup$Listener;


# instance fields
.field private final DISABLED_ALPHA:F

.field private mFilterEnabled:Z

.field private mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

.field private mOverrideValue:Ljava/lang/String;

.field private mPreference:Lcom/android/camera/IconListPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/AbstractIndicatorButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x3ecccccd

    iput v0, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->DISABLED_ALPHA:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mFilterEnabled:Z

    return-void
.end method


# virtual methods
.method public forceReloadPreference()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->reloadValue()V

    invoke-virtual {p0}, Lcom/android/camera/ui/ControlBarIndicatorButton;->reloadPreference()V

    return-void
.end method

.method public initializeIndicatorPref(Lcom/android/camera/IconListPreference;)V
    .locals 1
    .param p1    # Lcom/android/camera/IconListPreference;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    iput-object p1, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {p0}, Lcom/android/camera/ui/ControlBarIndicatorButton;->reloadPreference()V

    return-void
.end method

.method protected initializePopup()V
    .locals 5

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0076

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const v3, 0x7f040005

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/BasicSettingPopup;

    iget-object v3, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v1, v3}, Lcom/android/camera/ui/BasicSettingPopup;->initialize(Lcom/android/camera/IconListPreference;)V

    invoke-virtual {v1, p0}, Lcom/android/camera/ui/BasicSettingPopup;->setSettingChangedListener(Lcom/android/camera/ui/BasicSettingPopup$Listener;)V

    iput-object v1, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    iget-object v3, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public onSettingChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/camera/ui/ControlBarIndicatorButton;->reloadPreference()V

    invoke-virtual {p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->dismissPopupDelayed()V

    iget-object v0, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    invoke-interface {v0}, Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;->onSharedPreferenceChanged()V

    :cond_0
    return-void
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 6
    .param p1    # [Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    array-length v4, p1

    const/4 v5, 0x2

    if-le v4, v5, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "performance defect! please check"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    aget-object v0, p1, v3

    aget-object v1, p1, v2

    invoke-virtual {p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iput-object v1, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mOverrideValue:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/ui/ControlBarIndicatorButton;->reloadPreference()V

    return-void

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public reloadPreference()V
    .locals 5

    const/4 v4, -0x1

    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getLargeIconIds()[I

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mOverrideValue:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v3, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v3}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v4, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->print()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v3, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mOverrideValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v4, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->print()V

    goto :goto_0

    :cond_1
    aget v2, v0, v1

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    invoke-super {p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->reloadPreference()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getSingleIcon()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public setSettingChangedListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iput-object p1, p0, Lcom/android/camera/ui/ControlBarIndicatorButton;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    return-void
.end method
