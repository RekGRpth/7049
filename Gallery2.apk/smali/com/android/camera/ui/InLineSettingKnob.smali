.class public Lcom/android/camera/ui/InLineSettingKnob;
.super Lcom/android/camera/ui/InLineSettingItem;
.source "InLineSettingKnob.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "InLineSettingKnob"


# instance fields
.field private mEntry:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mNext:Z

.field private mNextButton:Landroid/widget/Button;

.field mNextTouchListener:Landroid/view/View$OnTouchListener;

.field private mPrevButton:Landroid/widget/Button;

.field private mPrevious:Z

.field mPreviousTouchListener:Landroid/view/View$OnTouchListener;

.field private final mRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/InLineSettingItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/camera/ui/InLineSettingKnob$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ui/InLineSettingKnob$1;-><init>(Lcom/android/camera/ui/InLineSettingKnob;)V

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/camera/ui/InLineSettingKnob$2;

    invoke-direct {v0, p0}, Lcom/android/camera/ui/InLineSettingKnob$2;-><init>(Lcom/android/camera/ui/InLineSettingKnob;)V

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/android/camera/ui/InLineSettingKnob$3;

    invoke-direct {v0, p0}, Lcom/android/camera/ui/InLineSettingKnob$3;-><init>(Lcom/android/camera/ui/InLineSettingKnob;)V

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPreviousTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/InLineSettingKnob;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;

    iget-boolean v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNext:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/camera/ui/InLineSettingKnob;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNext:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/camera/ui/InLineSettingKnob;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/ui/InLineSettingKnob;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;

    iget-boolean v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevious:Z

    return v0
.end method

.method static synthetic access$202(Lcom/android/camera/ui/InLineSettingKnob;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevious:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/camera/ui/InLineSettingKnob;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/camera/ui/InLineSettingKnob;

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p0, p1}, Lcom/android/camera/ui/InLineSettingKnob;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 v0, 0x1

    return v0
.end method

.method public initialize(Lcom/android/camera/ListPreference;)V
    .locals 7
    .param p1    # Lcom/android/camera/ListPreference;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/android/camera/ui/InLineSettingItem;->initialize(Lcom/android/camera/ListPreference;)V

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextButton:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c013b

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v4}, Lcom/android/camera/CameraPreference;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevButton:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c013a

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v4}, Lcom/android/camera/CameraPreference;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0b0044

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0b0042

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPreviousTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0b0043

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/camera/ui/InLineSettingKnob;->mEntry:Landroid/widget/TextView;

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v2}, Lcom/android/camera/CameraPreference;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->getEntry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected updateView()V
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x4

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->getOverrideValue()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingKnob;->mEntry:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v5}, Lcom/android/camera/ListPreference;->getEntry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextButton:Landroid/widget/Button;

    iget v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mIndex:I

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevButton:Landroid/widget/Button;

    iget v5, p0, Lcom/android/camera/ui/InLineSettingItem;->mIndex:I

    iget-object v6, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v6}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v6

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ne v5, v6, :cond_2

    :goto_2
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v2, v1}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingKnob;->mEntry:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v4}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingKnob;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingKnob;->mPrevButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const-string v2, "InLineSettingKnob"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail to find override value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/camera/ui/InLineSettingItem;->mPreference:Lcom/android/camera/ListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->print()V

    goto :goto_3
.end method
