.class public Lcom/android/camera/ui/IndicatorButton;
.super Lcom/android/camera/ui/AbstractIndicatorButton;
.source "IndicatorButton.java"

# interfaces
.implements Lcom/android/camera/ui/BasicSettingPopup$Listener;
.implements Lcom/android/camera/ui/EffectSettingPopup$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ui/IndicatorButton$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "IndicatorButton"


# instance fields
.field private mListener:Lcom/android/camera/ui/IndicatorButton$Listener;

.field private mOverrideValue:Ljava/lang/String;

.field private mPreference:Lcom/android/camera/IconListPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/camera/IconListPreference;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/IconListPreference;

    invoke-direct {p0, p1}, Lcom/android/camera/ui/AbstractIndicatorButton;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorButton;->reloadPreference()V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initializePopup()V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0b0076

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const-string v4, "pref_video_effect_key"

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorButton;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f040016

    invoke-virtual {v2, v4, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/EffectSettingPopup;

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v1, v4}, Lcom/android/camera/ui/EffectSettingPopup;->initialize(Lcom/android/camera/IconListPreference;)V

    invoke-virtual {v1, p0}, Lcom/android/camera/ui/EffectSettingPopup;->setSettingChangedListener(Lcom/android/camera/ui/EffectSettingPopup$Listener;)V

    iput-object v1, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    :goto_0
    iget-object v4, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    const v4, 0x7f040005

    invoke-virtual {v2, v4, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/BasicSettingPopup;

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v0, v4}, Lcom/android/camera/ui/BasicSettingPopup;->initialize(Lcom/android/camera/IconListPreference;)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/BasicSettingPopup;->setSettingChangedListener(Lcom/android/camera/ui/BasicSettingPopup$Listener;)V

    iput-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    goto :goto_0
.end method

.method public isOverridden()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSettingChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorButton;->reloadPreference()V

    invoke-virtual {p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->dismissPopupDelayed()V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorButton;->mListener:Lcom/android/camera/ui/IndicatorButton$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorButton;->mListener:Lcom/android/camera/ui/IndicatorButton$Listener;

    invoke-interface {v0}, Lcom/android/camera/ui/IndicatorButton$Listener;->onSettingChanged()V

    :cond_0
    return-void
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v3}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v3}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    array-length v3, p3

    if-le v3, v4, :cond_5

    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    :cond_3
    iget-object v3, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    if-nez v3, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0076

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const v3, 0x7f040005

    invoke-virtual {v1, v3, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/BasicSettingPopup;

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/BasicSettingPopup;->initialize(Lcom/android/camera/IconListPreference;)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/BasicSettingPopup;->setSettingChangedListener(Lcom/android/camera/ui/BasicSettingPopup$Listener;)V

    iput-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    iget-object v3, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    iget-object v3, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    check-cast v3, Lcom/android/camera/ui/BasicSettingPopup;

    invoke-virtual {v3, p1, p2, p3}, Lcom/android/camera/ui/BasicSettingPopup;->overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    array-length v3, p3

    if-ne v3, v4, :cond_3

    iput-object p2, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 6
    .param p1    # [Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    array-length v4, p1

    const/4 v5, 0x2

    if-le v4, v5, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "performance defect! please check"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    aget-object v0, p1, v3

    aget-object v1, p1, v2

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorButton;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/camera/ui/AbstractIndicatorButton;->setEnabled(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorButton;->reloadPreference()V

    return-void

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public reloadPreference()V
    .locals 5

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getLargeIconIds()[I

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v3}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    :goto_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const-string v2, "IndicatorButton"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to find override value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->print()V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorButton;->mOverrideValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_1
    aget v2, v0, v1

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_2
    invoke-super {p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->reloadPreference()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/camera/ui/IndicatorButton;->mPreference:Lcom/android/camera/IconListPreference;

    invoke-virtual {v2}, Lcom/android/camera/IconListPreference;->getSingleIcon()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method public setSettingChangedListener(Lcom/android/camera/ui/IndicatorButton$Listener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/IndicatorButton$Listener;

    iput-object p1, p0, Lcom/android/camera/ui/IndicatorButton;->mListener:Lcom/android/camera/ui/IndicatorButton$Listener;

    return-void
.end method
