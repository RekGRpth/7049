.class public abstract Lcom/android/camera/ui/IndicatorControl;
.super Landroid/widget/RelativeLayout;
.source "IndicatorControl.java"

# interfaces
.implements Lcom/android/camera/ui/IndicatorButton$Listener;
.implements Lcom/android/camera/ui/OtherSettingsPopup$Listener;
.implements Lcom/android/camera/ui/Rotatable;


# static fields
.field public static final MODE_CAMERA:I = 0x0

.field public static final MODE_VIDEO:I = 0x1

.field private static final TAG:Ljava/lang/String; = "IndicatorControl"


# instance fields
.field protected mCameraPicker:Lcom/android/camera/ui/CameraPicker;

.field protected mCurrentMode:I

.field mIndicators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/camera/ui/AbstractIndicatorButton;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

.field protected mOnIndicatorEventListener:Lcom/android/camera/ui/OnIndicatorEventListener;

.field private mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

.field protected mZoomControl:Lcom/android/camera/ui/ZoomControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected addControls([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v1

    check-cast v1, Lcom/android/camera/IconListPreference;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/android/camera/ui/IndicatorControl;->addIndicator(Landroid/content/Context;Lcom/android/camera/IconListPreference;)Lcom/android/camera/ui/IndicatorButton;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {p0, v2, v3, p2}, Lcom/android/camera/ui/IndicatorControl;->addOtherSettingIndicator(Landroid/content/Context;I[Ljava/lang/String;)Lcom/android/camera/ui/OtherSettingIndicatorButton;

    :cond_2
    return-void
.end method

.method public addIndicator(Landroid/content/Context;Lcom/android/camera/IconListPreference;)Lcom/android/camera/ui/IndicatorButton;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/IconListPreference;

    new-instance v0, Lcom/android/camera/ui/IndicatorButton;

    invoke-direct {v0, p1, p2}, Lcom/android/camera/ui/IndicatorButton;-><init>(Landroid/content/Context;Lcom/android/camera/IconListPreference;)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/IndicatorButton;->setSettingChangedListener(Lcom/android/camera/ui/IndicatorButton$Listener;)V

    invoke-virtual {p2}, Lcom/android/camera/CameraPreference;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public addOtherSettingIndicator(Landroid/content/Context;I[Ljava/lang/String;)Lcom/android/camera/ui/OtherSettingIndicatorButton;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    new-instance v0, Lcom/android/camera/ui/OtherSettingIndicatorButton;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/android/camera/ui/OtherSettingIndicatorButton;-><init>(Landroid/content/Context;ILcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/OtherSettingIndicatorButton;->setSettingChangedListener(Lcom/android/camera/ui/OtherSettingsPopup$Listener;)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public dismissSettingPopup()Z
    .locals 3

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v1}, Lcom/android/camera/ui/AbstractIndicatorButton;->dismissPopup()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getActiveSettingPopup()Landroid/view/View;
    .locals 4

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v2}, Lcom/android/camera/ui/AbstractIndicatorButton;->getPopupWindow()Lcom/android/camera/ui/AbstractSettingPopup;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected initializeCameraPicker()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v2, "pref_camera_id_key"

    invoke-virtual {v1, v2}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-nez v1, :cond_3

    const v1, 0x7f0200dd

    invoke-static {v1}, Lcom/android/camera/ui/CameraPicker;->setImageResourceId(I)V

    :cond_2
    :goto_1
    new-instance v1, Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/camera/ui/CameraPicker;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {v1, v0}, Lcom/android/camera/ui/CameraPicker;->initialize(Lcom/android/camera/ListPreference;)V

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const v1, 0x7f0200e0

    invoke-static {v1}, Lcom/android/camera/ui/CameraPicker;->setImageResourceId(I)V

    goto :goto_1
.end method

.method protected initializeZoomControl(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/ZoomControl;

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    goto :goto_0
.end method

.method public onRestorePreferencesClicked()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    invoke-interface {v0}, Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;->onRestorePreferencesClicked()V

    :cond_0
    return-void
.end method

.method public onSettingChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    invoke-interface {v0}, Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;->onSharedPreferenceChanged()V

    :cond_0
    return-void
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    instance-of v2, v0, Lcom/android/camera/ui/IndicatorButton;

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/camera/ui/AbstractIndicatorButton;->overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 7
    .param p1    # [Ljava/lang/String;

    array-length v5, p1

    rem-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v5

    :cond_0
    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    instance-of v5, v0, Lcom/android/camera/ui/OtherSettingIndicatorButton;

    if-eqz v5, :cond_2

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/AbstractIndicatorButton;->overrideSettings([Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_1
    array-length v5, p1

    if-ge v1, v5, :cond_1

    aget-object v3, p1, v1

    add-int/lit8 v5, v1, 0x1

    aget-object v4, p1, v5

    invoke-virtual {v0}, Lcom/android/camera/ui/AbstractIndicatorButton;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object v4, v5, v6

    invoke-virtual {v0, v5}, Lcom/android/camera/ui/AbstractIndicatorButton;->overrideSettings([Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_4
    return-void
.end method

.method public reInitializeOtherSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/camera/ui/IndicatorControl;->setPreferenceGroup(Lcom/android/camera/PreferenceGroup;)V

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    instance-of v2, v0, Lcom/android/camera/ui/OtherSettingIndicatorButton;

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/AbstractIndicatorButton;->reInitialSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public reloadPreferences()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    invoke-virtual {v2}, Lcom/android/camera/PreferenceGroup;->reloadValue()V

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/AbstractIndicatorButton;->reloadPreference()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected removeControls(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    move v1, p1

    :goto_0
    add-int v2, p1, p2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/AbstractIndicatorButton;->removePopupWindow()V

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->removeViews(II)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 7
    .param p1    # Z

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/android/camera/ui/AbstractIndicatorButton;

    if-eqz v3, :cond_0

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    iget v3, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-ne v3, v6, :cond_0

    if-eqz p1, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {v3, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget v3, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-ne v3, v6, :cond_3

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    return-void

    :cond_4
    move v4, v5

    goto :goto_2
.end method

.method public setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V
    .locals 1
    .param p1    # Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iput-object p1, p0, Lcom/android/camera/ui/IndicatorControl;->mListener:Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/CameraPicker;->setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V

    :cond_0
    return-void
.end method

.method public setOnIndicatorEventListener(Lcom/android/camera/ui/OnIndicatorEventListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/OnIndicatorEventListener;

    iput-object p1, p0, Lcom/android/camera/ui/IndicatorControl;->mOnIndicatorEventListener:Lcom/android/camera/ui/OnIndicatorEventListener;

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/android/camera/ui/Rotatable;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v2, p1, p2}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setPreferenceGroup(Lcom/android/camera/PreferenceGroup;)V
    .locals 3
    .param p1    # Lcom/android/camera/PreferenceGroup;

    iput-object p1, p0, Lcom/android/camera/ui/IndicatorControl;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    invoke-virtual {p1}, Lcom/android/camera/CameraPreference;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00e5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    :cond_0
    return-void
.end method

.method public setupFilter(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/android/camera/ui/TwoStateImageView;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/android/camera/ui/TwoStateImageView;

    invoke-virtual {v2, p1}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
