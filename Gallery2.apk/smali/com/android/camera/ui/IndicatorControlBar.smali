.class public Lcom/android/camera/ui/IndicatorControlBar;
.super Lcom/android/camera/ui/IndicatorControl;
.source "IndicatorControlBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final ICON_SPACING:I

.field private static final TAG:Ljava/lang/String; = "IndicatorControlBar"


# instance fields
.field private mSecondLevelIcon:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    sput v0, Lcom/android/camera/ui/IndicatorControlBar;->ICON_SPACING:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public enableZoom(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/ZoomControl;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/PreferenceGroup;
    .param p3    # Z

    invoke-virtual {p0, p2}, Lcom/android/camera/ui/IndicatorControl;->setPreferenceGroup(Lcom/android/camera/PreferenceGroup;)V

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->initializeCameraPicker()V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {p0, p3}, Lcom/android/camera/ui/IndicatorControl;->initializeZoomControl(Z)V

    iget v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mOnIndicatorEventListener:Lcom/android/camera/ui/OnIndicatorEventListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/camera/ui/OnIndicatorEventListener;->onIndicatorEvent(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f0b0048

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sub-int v4, p4, p2

    sub-int v1, p5, p3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    move v3, v4

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    add-int v6, v2, v3

    invoke-virtual {v5, v8, v2, v3, v6}, Landroid/view/View;->layout(IIII)V

    :cond_2
    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    add-int v6, v2, v3

    sub-int v7, v1, v2

    sub-int/2addr v7, v3

    invoke-virtual {v5, v8, v6, v3, v7}, Landroid/view/ViewGroup;->layout(IIII)V

    :cond_3
    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    sub-int v6, v1, v2

    sub-int/2addr v6, v3

    sub-int v7, v1, v2

    invoke-virtual {v5, v8, v6, v3, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    move v3, v1

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    add-int v6, v2, v3

    invoke-virtual {v5, v2, v8, v6, v3}, Landroid/view/View;->layout(IIII)V

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    add-int v6, v2, v3

    sub-int v7, v4, v2

    sub-int/2addr v7, v3

    invoke-virtual {v5, v6, v8, v7, v3}, Landroid/view/ViewGroup;->layout(IIII)V

    :cond_5
    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    sub-int v6, v4, v2

    sub-int/2addr v6, v3

    sub-int v7, v4, v2

    invoke-virtual {v5, v6, v8, v7, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/ui/IndicatorControl;->setEnabled(Z)V

    iget v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBar;->mSecondLevelIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/camera/ui/IndicatorControlBar;->enableZoom(Z)V

    goto :goto_1
.end method
