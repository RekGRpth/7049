.class Lcom/android/camera/ui/IndicatorControlBarContainer$1;
.super Ljava/lang/Object;
.source "IndicatorControlBarContainer.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ui/IndicatorControlBarContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;


# direct methods
.method constructor <init>(Lcom/android/camera/ui/IndicatorControlBarContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;->this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;->this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;

    invoke-static {v0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->access$000(Lcom/android/camera/ui/IndicatorControlBarContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;->this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;

    invoke-static {v0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->access$100(Lcom/android/camera/ui/IndicatorControlBarContainer;)Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;->this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;

    invoke-static {v0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->access$200(Lcom/android/camera/ui/IndicatorControlBarContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;->this$0:Lcom/android/camera/ui/IndicatorControlBarContainer;

    invoke-static {v0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->access$300(Lcom/android/camera/ui/IndicatorControlBarContainer;)Lcom/android/camera/ui/IndicatorControlBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
