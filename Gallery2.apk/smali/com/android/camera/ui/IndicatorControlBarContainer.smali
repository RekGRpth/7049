.class public Lcom/android/camera/ui/IndicatorControlBarContainer;
.super Lcom/android/camera/ui/IndicatorControlContainer;
.source "IndicatorControlBarContainer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IndicatorControlBarContainer"


# instance fields
.field private mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFadeIn:Landroid/view/animation/Animation;

.field private mFadeOut:Landroid/view/animation/Animation;

.field private mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

.field private mSecondLevelFadeIn:Landroid/view/animation/Animation;

.field private mSecondLevelFadeOut:Landroid/view/animation/Animation;

.field private mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControlContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/camera/ui/IndicatorControlBarContainer$1;

    invoke-direct {v0, p0}, Lcom/android/camera/ui/IndicatorControlBarContainer$1;-><init>(Lcom/android/camera/ui/IndicatorControlBarContainer;)V

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    const/high16 v0, 0x7f050000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeIn:Landroid/view/animation/Animation;

    const v0, 0x7f050001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeOut:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeOut:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const v0, 0x7f050009

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeIn:Landroid/view/animation/Animation;

    const v0, 0x7f05000a

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeOut:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeOut:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/IndicatorControlBarContainer;)Landroid/view/animation/Animation;
    .locals 1
    .param p0    # Lcom/android/camera/ui/IndicatorControlBarContainer;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeOut:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/camera/ui/IndicatorControlBarContainer;)Lcom/android/camera/ui/SecondLevelIndicatorControlBar;
    .locals 1
    .param p0    # Lcom/android/camera/ui/IndicatorControlBarContainer;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/ui/IndicatorControlBarContainer;)Landroid/view/animation/Animation;
    .locals 1
    .param p0    # Lcom/android/camera/ui/IndicatorControlBarContainer;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeOut:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/camera/ui/IndicatorControlBarContainer;)Lcom/android/camera/ui/IndicatorControlBar;
    .locals 1
    .param p0    # Lcom/android/camera/ui/IndicatorControlBarContainer;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    return-object v0
.end method

.method private leaveSecondLevelIndicator()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public disableCameraPicker()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public dismissSecondLevelIndicator()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->leaveSecondLevelIndicator()V

    :cond_0
    return-void
.end method

.method public dismissSettingPopup()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControlBar;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public enableFilter(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    return-void
.end method

.method public enableZoom(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControlBar;->enableZoom(Z)V

    return-void
.end method

.method public getActiveSettingPopup()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->getActiveSettingPopup()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->getActiveSettingPopup()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/PreferenceGroup;
    .param p3    # Z
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/camera/ui/IndicatorControlBar;->initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1, p2, p4, p5}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f0b0047

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/IndicatorControlBar;

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/IndicatorControl;->setOnIndicatorEventListener(Lcom/android/camera/ui/OnIndicatorEventListener;)V

    const v0, 0x7f0b0049

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/IndicatorControl;->setOnIndicatorEventListener(Lcom/android/camera/ui/OnIndicatorEventListener;)V

    return-void
.end method

.method public onIndicatorEvent(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mFadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelFadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlBarContainer;->leaveSecondLevelIndicator()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/camera/ui/IndicatorControl;->overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->overrideSettings([Ljava/lang/String;)V

    return-void
.end method

.method public reInitializeOtherSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->reInitializeOtherSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V

    return-void
.end method

.method public reloadPreferences()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->reloadPreferences()V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->reloadPreferences()V

    return-void
.end method

.method public setCameraPickerVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControlBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->setEnabled(Z)V

    return-void
.end method

.method public setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V
    .locals 1
    .param p1    # Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mIndicatorControlBar:Lcom/android/camera/ui/IndicatorControlBar;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlBarContainer;->mSecondLevelIndicatorControlBar:Lcom/android/camera/ui/SecondLevelIndicatorControlBar;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->setOrientation(IZ)V

    return-void
.end method
