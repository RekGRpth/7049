.class public Lcom/android/camera/ui/IndicatorControlWheel;
.super Lcom/android/camera/ui/IndicatorControl;
.source "IndicatorControlWheel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final ANIMATION_TIME:I = 0x12c

.field private static final EDGE_STROKE_WIDTH:F = 6.0f

.field private static final HIGHLIGHT_DEGREES:I = 0x1e

.field private static final HIGHLIGHT_RADIANS:D

.field public static final HIGHLIGHT_WIDTH:I = 0x4

.field private static final LANDSCAPE_CLOSE_ICON_DEFAULT_DEGREES:I = 0x13b

.field private static final LANDSCAPE_FIRST_LEVEL_END_DEGREES:I = 0x11e

.field private static final LANDSCAPE_FIRST_LEVEL_START_DEGREES:I = 0x4a

.field private static final LANDSCAPE_MAX_ZOOM_CONTROL_DEGREES:I = 0x108

.field private static final LANDSCAPE_SECOND_LEVEL_END_DEGREES:I = 0x12c

.field private static final LANDSCAPE_SECOND_LEVEL_START_DEGREES:I = 0x3c

.field private static final TAG:Ljava/lang/String; = "IndicatorControlWheel"

.field private static final TIME_LAPSE_ARC_WIDTH:I = 0x6


# instance fields
.field private final CLOSE_ICON_DEFAULT_DEGREES:I

.field private final FIRST_LEVEL_END_DEGREES:I

.field private final FIRST_LEVEL_START_DEGREES:I

.field private final HIGHLIGHT_COLOR:I

.field private final HIGHLIGHT_FAN_COLOR:I

.field private final MAX_ZOOM_CONTROL_DEGREES:I

.field private final SECOND_LEVEL_END_DEGREES:I

.field private final SECOND_LEVEL_START_DEGREES:I

.field private final TIME_LAPSE_ARC_COLOR:I

.field private mAnimationStartTime:J

.field private mBackgroundPaint:Landroid/graphics/Paint;

.field private mBackgroundRect:Landroid/graphics/RectF;

.field private mCenterX:I

.field private mCenterY:I

.field private mChildRadians:[D

.field private mCloseIcon:Landroid/widget/ImageView;

.field private mCurrentLevel:I

.field private mEndVisibleRadians:[D

.field private mFanPath:Landroid/graphics/Path;

.field private mHandler:Landroid/os/Handler;

.field private mInAnimation:Z

.field private mInitialized:Z

.field private mLastMotionEvent:Landroid/view/MotionEvent;

.field private mNumberOfFrames:J

.field private mPressedIndex:I

.field private mRecordingStartTime:J

.field private final mRunnable:Ljava/lang/Runnable;

.field private mSecondLevelIcon:Landroid/widget/ImageView;

.field private mSecondLevelStartIndex:I

.field private mSectorRadians:[D

.field private mShutterButtonRadius:D

.field private mStartVisibleRadians:[D

.field private mStrokeWidth:I

.field private mTimeLapseInterval:I

.field private mTouchSectorRadians:[D

.field private mWheelRadius:D

.field private res:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/high16 v0, 0x403e000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    sput-wide v0, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_RADIANS:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const-wide/16 v5, 0x0

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    iput-wide v5, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mRecordingStartTime:J

    iput-wide v5, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mNumberOfFrames:J

    iput-boolean v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/camera/ui/IndicatorControlWheel$1;

    invoke-direct {v1, p0}, Lcom/android/camera/ui/IndicatorControlWheel$1;-><init>(Lcom/android/camera/ui/IndicatorControlWheel;)V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mRunnable:Ljava/lang/Runnable;

    iput v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    iput v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    new-array v1, v3, [D

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStartVisibleRadians:[D

    new-array v1, v3, [D

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mEndVisibleRadians:[D

    new-array v1, v3, [D

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSectorRadians:[D

    new-array v1, v3, [D

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mTouchSectorRadians:[D

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_COLOR:I

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_FAN_COLOR:I

    const v1, 0x7f090006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->TIME_LAPSE_ARC_COLOR:I

    invoke-virtual {p0, v2}, Landroid/view/View;->setWillNotDraw(Z)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->res:Landroid/content/res/Resources;

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_0

    const/16 v1, 0x4a

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_START_DEGREES:I

    const/16 v1, 0x11e

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_END_DEGREES:I

    const/16 v1, 0x3c

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    const/16 v1, 0x12c

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_END_DEGREES:I

    const/16 v1, 0x108

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->MAX_ZOOM_CONTROL_DEGREES:I

    const/16 v1, 0x13b

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->CLOSE_ICON_DEFAULT_DEGREES:I

    :goto_0
    return-void

    :cond_0
    const/16 v1, -0x10

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_START_DEGREES:I

    const/16 v1, 0xc4

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_END_DEGREES:I

    const/16 v1, -0x1e

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    const/16 v1, 0xd2

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_END_DEGREES:I

    const/16 v1, 0xae

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->MAX_ZOOM_CONTROL_DEGREES:I

    const/16 v1, 0xe1

    iput v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->CLOSE_ICON_DEFAULT_DEGREES:I

    goto :goto_0
.end method

.method private addImageButton(Landroid/content/Context;IZ)Landroid/widget/ImageView;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Z

    if-eqz p3, :cond_0

    new-instance v0, Lcom/android/camera/ui/RotateImageView;

    invoke-direct {v0, p1}, Lcom/android/camera/ui/RotateImageView;-><init>(Landroid/content/Context;)V

    :goto_0
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/camera/ui/TwoStateImageView;

    invoke-direct {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private changeIndicatorsLevel()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mAnimationStartTime:J

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    return-void
.end method

.method private getChildCountByLevel(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    goto :goto_0
.end method

.method private getSelectedIndicatorIndex()I
    .locals 5

    const/4 v3, -0x1

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControl;->mIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/AbstractIndicatorButton;->getPopupWindow()Lcom/android/camera/ui/AbstractSettingPopup;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    :cond_0
    :goto_1
    return v3

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    if-eq v4, v3, :cond_0

    iget v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v4, v2, Lcom/android/camera/ui/AbstractIndicatorButton;

    if-nez v4, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    goto :goto_1
.end method

.method private getTouchIndicatorIndex(D)I
    .locals 20
    .param p1    # D

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    move/from16 v16, v0

    if-eqz v16, :cond_1

    const/4 v12, -0x1

    :cond_0
    :goto_0
    return v12

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/camera/ui/IndicatorControlWheel;->getChildCountByLevel(I)I

    move-result v3

    if-nez v3, :cond_2

    const/4 v12, -0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v11, v3, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    if-nez v16, :cond_5

    const/4 v12, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mTouchSectorRadians:[D

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v17, v0

    aget-wide v7, v16, v17

    const-wide/16 v13, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    if-nez v16, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    move-object/from16 v16, v0

    if-eqz v16, :cond_7

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    move-object/from16 v16, v0

    if-nez v16, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_START_DEGREES:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    add-double v13, v16, v7

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    move/from16 v16, v0

    add-int/lit8 v4, v16, -0x1

    :goto_3
    const-wide/16 v16, 0x0

    cmpg-double v16, v13, v16

    if-gez v16, :cond_3

    const-wide v16, 0x401921fb54442d18L

    add-double v13, v13, v16

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v4

    add-double v5, v16, v7

    cmpl-double v16, v13, v5

    if-lez v16, :cond_a

    cmpl-double v16, p1, v13

    if-gez v16, :cond_4

    cmpg-double v16, p1, v5

    if-gtz v16, :cond_9

    :cond_4
    const/4 v15, 0x1

    :goto_4
    if-eqz v15, :cond_12

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v16, v0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_c

    const/4 v10, 0x0

    :goto_5
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v12

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->rebase(DI)D

    move-result-wide v16

    sub-double v16, p1, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mSectorRadians:[D

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v19, v0

    aget-wide v18, v18, v19

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v9, v0

    if-le v9, v11, :cond_d

    add-int/2addr v12, v11

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v12

    sub-double v13, v16, v7

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v12

    sub-double v13, v16, v7

    goto/16 :goto_2

    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v16

    add-int/lit8 v4, v16, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v12

    sub-double v13, v16, v7

    goto/16 :goto_3

    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_a
    cmpl-double v16, p1, v13

    if-ltz v16, :cond_b

    cmpg-double v16, p1, v5

    if-gtz v16, :cond_b

    const/4 v15, 0x1

    :goto_6
    goto/16 :goto_4

    :cond_b
    const/4 v15, 0x0

    goto :goto_6

    :cond_c
    const/16 v10, 0x5a

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->rebase(DI)D

    move-result-wide p1

    goto/16 :goto_5

    :cond_d
    if-ltz v9, :cond_0

    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    if-nez v16, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    move-object/from16 v16, v0

    if-nez v16, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    aget-wide v16, v16, v12

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->rebase(DI)D

    move-result-wide v16

    sub-double v16, v16, v7

    cmpl-double v16, p1, v16

    if-ltz v16, :cond_11

    add-int/2addr v12, v9

    goto/16 :goto_0

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    add-int v17, v12, v9

    aget-wide v16, v16, v17

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->rebase(DI)D

    move-result-wide v16

    add-double v16, v16, v7

    cmpg-double v16, p1, v16

    if-gtz v16, :cond_10

    add-int/2addr v12, v9

    goto/16 :goto_0

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    move-object/from16 v16, v0

    add-int v17, v12, v9

    add-int/lit8 v17, v17, 0x1

    aget-wide v16, v16, v17

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->rebase(DI)D

    move-result-wide v16

    sub-double v16, v16, v7

    cmpl-double v16, p1, v16

    if-ltz v16, :cond_11

    add-int v16, v12, v9

    add-int/lit8 v12, v16, 0x1

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    move/from16 v16, v0

    if-nez v16, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    move-object/from16 v16, v0

    if-eqz v16, :cond_12

    const/4 v12, 0x0

    goto/16 :goto_0

    :cond_12
    const/4 v12, -0x1

    goto/16 :goto_0
.end method

.method private injectMotionEvent(ILandroid/view/MotionEvent;I)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # I

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-virtual {v0, p2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method private presetFirstLevelChildRadians()V
    .locals 6

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStartVisibleRadians:[D

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_START_DEGREES:I

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    aput-wide v3, v2, v5

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mTouchSectorRadians:[D

    sget-wide v3, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_RADIANS:D

    aput-wide v3, v2, v5

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mEndVisibleRadians:[D

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_END_DEGREES:I

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    aput-wide v3, v2, v5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    add-int/lit8 v1, v0, 0x1

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->MAX_ZOOM_CONTROL_DEGREES:I

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    aput-wide v3, v2, v0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    add-int/lit8 v1, v0, 0x1

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_START_DEGREES:I

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    aput-wide v3, v2, v0

    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    add-int/lit8 v1, v0, 0x1

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->FIRST_LEVEL_END_DEGREES:I

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    aput-wide v3, v2, v0

    return-void
.end method

.method private presetSecondLevelChildRadians()V
    .locals 14

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/android/camera/ui/IndicatorControlWheel;->getChildCountByLevel(I)I

    move-result v0

    if-gt v0, v8, :cond_0

    move v6, v8

    :goto_0
    iget v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_END_DEGREES:I

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    sub-int/2addr v9, v10

    div-int/2addr v9, v6

    int-to-double v4, v9

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSectorRadians:[D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    aput-wide v10, v9, v8

    iget v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->CLOSE_ICON_DEFAULT_DEGREES:I

    int-to-double v1, v9

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStartVisibleRadians:[D

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    aput-wide v10, v9, v8

    iget v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v0, :cond_1

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    add-int v10, v7, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v11

    aput-wide v11, v9, v10

    add-double/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v6, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mTouchSectorRadians:[D

    sget-wide v10, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_RADIANS:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    aput-wide v10, v9, v8

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mEndVisibleRadians:[D

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_END_DEGREES:I

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    aput-wide v10, v9, v8

    return-void
.end method

.method private rebase(DI)D
    .locals 4
    .param p1    # D
    .param p3    # I

    int-to-double v0, p3

    add-double/2addr v0, p1

    const-wide v2, 0x401921fb54442d18L

    rem-double/2addr v0, v2

    return-wide v0
.end method

.method private rotateWheel()V
    .locals 13

    const/4 v8, 0x0

    iget v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->CLOSE_ICON_DEFAULT_DEGREES:I

    iget v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    sub-int v6, v7, v9

    iget v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-nez v7, :cond_2

    iget v5, p0, Lcom/android/camera/ui/IndicatorControlWheel;->CLOSE_ICON_DEFAULT_DEGREES:I

    :goto_0
    iget v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-nez v7, :cond_0

    neg-int v6, v6

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mAnimationStartTime:J

    sub-long/2addr v9, v11

    long-to-int v0, v9

    const/16 v7, 0x12c

    if-lt v0, v7, :cond_1

    const/16 v0, 0x12c

    iget v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-nez v7, :cond_3

    const/4 v7, 0x1

    :goto_1
    iput v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    iput-boolean v8, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    :cond_1
    mul-int v7, v6, v0

    div-int/lit16 v7, v7, 0x12c

    add-int v1, v5, v7

    int-to-double v9, v1

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v9

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    iget v11, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    aget-wide v11, v7, v11

    sub-double v3, v9, v11

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_4

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    aget-wide v9, v7, v2

    add-double/2addr v9, v3

    aput-wide v9, v7, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    iget v5, p0, Lcom/android/camera/ui/IndicatorControlWheel;->SECOND_LEVEL_START_DEGREES:I

    goto :goto_0

    :cond_3
    move v7, v8

    goto :goto_1

    :cond_4
    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    aget-wide v8, v9, v8

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->MAX_ZOOM_CONTROL_DEGREES:I

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    sub-double/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/ui/ZoomControl;->rotate(D)V

    :cond_5
    return-void
.end method


# virtual methods
.method public dismissSecondLevelIndicator()V
    .locals 2

    iget v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->changeIndicatorsLevel()V

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0, p1}, Landroid/view/View;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v10

    if-nez v10, :cond_0

    const/4 v10, 0x0

    :goto_0
    return v10

    :cond_0
    iput-object p1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mLastMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    iget v11, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    float-to-double v5, v10

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-float v10, v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    sub-float/2addr v10, v11

    float-to-double v7, v10

    mul-double v10, v5, v5

    mul-double v12, v7, v7

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iget-wide v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mWheelRadius:D

    iget v12, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStrokeWidth:I

    int-to-double v12, v12

    add-double/2addr v10, v12

    cmpg-double v10, v1, v10

    if-gtz v10, :cond_a

    iget-wide v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    cmpl-double v10, v1, v10

    if-lez v10, :cond_a

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    const-wide/16 v10, 0x0

    cmpg-double v10, v3, v10

    if-gez v10, :cond_1

    const-wide v10, 0x401921fb54442d18L

    add-double/2addr v3, v10

    :cond_1
    invoke-direct {p0, v3, v4}, Lcom/android/camera/ui/IndicatorControlWheel;->getTouchIndicatorIndex(D)I

    move-result v9

    iget-object v10, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v10, :cond_2

    if-nez v9, :cond_2

    iget-object v10, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v10, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    if-ne v9, v10, :cond_3

    if-nez v0, :cond_5

    :cond_3
    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_7

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    const/4 v11, 0x3

    invoke-direct {p0, v10, p1, v11}, Lcom/android/camera/ui/IndicatorControlWheel;->injectMotionEvent(ILandroid/view/MotionEvent;I)V

    :cond_4
    :goto_1
    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    const/4 v10, 0x2

    if-ne v0, v10, :cond_5

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-eqz v10, :cond_5

    const/4 v10, 0x0

    invoke-direct {p0, v9, p1, v10}, Lcom/android/camera/ui/IndicatorControlWheel;->injectMotionEvent(ILandroid/view/MotionEvent;I)V

    :cond_5
    const/4 v10, -0x1

    if-eq v9, v10, :cond_6

    const/4 v10, 0x2

    if-eq v0, v10, :cond_6

    invoke-virtual {p0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_6
    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-nez v10, :cond_8

    if-eqz v9, :cond_8

    const/4 v10, 0x2

    if-ne v0, v10, :cond_8

    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->getSelectedIndicatorIndex()I

    move-result v10

    if-eq v10, v9, :cond_4

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    goto :goto_1

    :cond_8
    const/4 v10, 0x1

    if-ne v0, v10, :cond_9

    const/4 v9, -0x1

    :cond_9
    iput v9, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->onTouchOutBound()V

    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method public enableZoom(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/ZoomControl;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/PreferenceGroup;
    .param p3    # Z
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0x4a

    invoke-static {v4}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v4

    int-to-double v4, v4

    iput-wide v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    const/16 v4, 0x57

    invoke-static {v4}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v4

    iput v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStrokeWidth:I

    iget-wide v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    iget v6, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mStrokeWidth:I

    int-to-double v6, v6

    const-wide/high16 v8, 0x3fe0000000000000L

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iput-wide v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mWheelRadius:D

    invoke-virtual {p0, p2}, Lcom/android/camera/ui/IndicatorControl;->setPreferenceGroup(Lcom/android/camera/PreferenceGroup;)V

    invoke-virtual {p0, p3}, Lcom/android/camera/ui/IndicatorControl;->initializeZoomControl(Z)V

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->initializeCameraPicker()V

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelIcon:Landroid/widget/ImageView;

    if-nez v4, :cond_0

    const v4, 0x7f0200d6

    invoke-direct {p0, p1, v4, v3}, Lcom/android/camera/ui/IndicatorControlWheel;->addImageButton(Landroid/content/Context;IZ)Landroid/widget/ImageView;

    move-result-object v4

    iput-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelIcon:Landroid/widget/ImageView;

    const v5, 0x7f0b0048

    invoke-virtual {v4, v5}, Landroid/view/View;->setId(I)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    iput v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelStartIndex:I

    :cond_0
    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCloseIcon:Landroid/widget/ImageView;

    if-nez v4, :cond_1

    const v4, 0x7f020046

    invoke-direct {p0, p1, v4, v2}, Lcom/android/camera/ui/IndicatorControlWheel;->addImageButton(Landroid/content/Context;IZ)Landroid/widget/ImageView;

    move-result-object v4

    iput-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCloseIcon:Landroid/widget/ImageView;

    :cond_1
    iget-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCloseIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    add-int/lit8 v1, v4, 0x1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    sub-int v0, v4, v1

    if-lez v0, :cond_2

    invoke-virtual {p0, v1, v0}, Lcom/android/camera/ui/IndicatorControl;->removeControls(II)V

    :cond_2
    invoke-virtual {p0, p4, p5}, Lcom/android/camera/ui/IndicatorControl;->addControls([Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    new-array v4, v4, [D

    iput-object v4, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->presetFirstLevelChildRadians()V

    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->presetSecondLevelChildRadians()V

    iget v4, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-eqz v4, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p0, v2}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    iput-boolean v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mInitialized:Z

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/android/camera/ui/IndicatorControlWheel;->changeIndicatorsLevel()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1    # Landroid/graphics/Canvas;

    invoke-direct/range {p0 .. p0}, Lcom/android/camera/ui/IndicatorControlWheel;->getSelectedIndicatorIndex()I

    move-result v14

    const/4 v2, 0x1

    if-lt v14, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    aget-wide v2, v2, v14

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-int v9, v2

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    double-to-float v10, v2

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mStrokeWidth:I

    int-to-double v6, v4

    add-double/2addr v2, v6

    const-wide/high16 v6, 0x4008000000000000L

    add-double/2addr v2, v6

    double-to-float v13, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-float v3, v3

    sub-float/2addr v3, v10

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-float v4, v4

    sub-float/2addr v4, v10

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-float v6, v6

    add-float/2addr v6, v10

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-float v7, v7

    add-float/2addr v7, v10

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    neg-int v4, v9

    add-int/lit8 v4, v4, 0xf

    int-to-float v4, v4

    const/high16 v6, -0x3e100000

    invoke-virtual {v2, v3, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-float v3, v3

    sub-float/2addr v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-float v4, v4

    sub-float/2addr v4, v13

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-float v6, v6

    add-float/2addr v6, v13

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-float v7, v7

    add-float/2addr v7, v13

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    neg-int v4, v9

    add-int/lit8 v4, v4, -0xf

    int-to-float v4, v4

    const/high16 v6, 0x41f00000

    invoke-virtual {v2, v3, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40800000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_FAN_COLOR:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->res:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v8

    if-eqz v8, :cond_0

    const v2, 0x3fffffff

    and-int/2addr v8, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mFanPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_COLOR:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->res:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    neg-int v2, v9

    add-int/lit8 v2, v2, -0xf

    int-to-float v4, v2

    const/high16 v5, 0x41f00000

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-double v3, v3

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    sub-double/2addr v3, v6

    double-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-double v6, v4

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    move-wide/from16 v17, v0

    sub-double v6, v6, v17

    double-to-float v4, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    int-to-double v6, v6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    move-wide/from16 v17, v0

    add-double v6, v6, v17

    double-to-float v6, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    int-to-double v0, v7

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mShutterButtonRadius:D

    move-wide/from16 v19, v0

    add-double v17, v17, v19

    move-wide/from16 v0, v17

    double-to-float v7, v0

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    const/high16 v3, 0x40400000

    const/high16 v4, 0x40400000

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->inset(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40c00000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->TIME_LAPSE_ARC_COLOR:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mRecordingStartTime:J

    sub-long v15, v2, v6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    int-to-long v2, v2

    div-long v11, v15, v2

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mNumberOfFrames:J

    cmp-long v2, v11, v2

    if-lez v2, :cond_4

    const/high16 v5, 0x43b40000

    move-object/from16 v0, p0

    iput-wide v11, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mNumberOfFrames:J

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    :cond_3
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    int-to-long v2, v2

    rem-long v2, v15, v2

    long-to-float v2, v2

    const/high16 v3, 0x43b40000

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    int-to-float v3, v3

    div-float v5, v2, v3

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 18
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mInitialized:Z

    if-nez v13, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    if-eqz v13, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/android/camera/ui/IndicatorControlWheel;->rotateWheel()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_5

    sub-int v13, p4, p2

    const/16 v14, 0x5d

    invoke-static {v14}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v14

    sub-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    sub-int v13, p5, p3

    div-int/lit8 v13, v13, 0x2

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    :goto_0
    const/4 v4, 0x0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v13

    if-ge v4, v13, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mChildRadians:[D

    aget-wide v5, v13, v4

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mStartVisibleRadians:[D

    const/4 v14, 0x1

    aget-wide v7, v13, v14

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mInAnimation:Z

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mEndVisibleRadians:[D

    const/4 v14, 0x1

    aget-wide v1, v13, v14

    :goto_3
    invoke-virtual {v9}, Landroid/view/View;->isEnabled()Z

    move-result v13

    if-nez v13, :cond_3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    if-nez v13, :cond_3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    const/4 v14, 0x1

    if-eq v13, v14, :cond_4

    :cond_3
    sget-wide v13, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_RADIANS:D

    const-wide/high16 v15, 0x4000000000000000L

    div-double/2addr v13, v15

    sub-double v13, v7, v13

    cmpg-double v13, v5, v13

    if-ltz v13, :cond_4

    sget-wide v13, Lcom/android/camera/ui/IndicatorControlWheel;->HIGHLIGHT_RADIANS:D

    const-wide/high16 v15, 0x4000000000000000L

    div-double/2addr v13, v15

    add-double/2addr v13, v1

    cmpl-double v13, v5, v13

    if-lez v13, :cond_8

    :cond_4
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    sub-int v13, p4, p2

    div-int/lit8 v13, v13, 0x2

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    sub-int v13, p5, p3

    const/16 v14, 0x5d

    invoke-static {v14}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v14

    sub-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mStartVisibleRadians:[D

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    aget-wide v7, v13, v14

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mEndVisibleRadians:[D

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCurrentLevel:I

    aget-wide v1, v13, v14

    goto :goto_3

    :cond_8
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterX:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mWheelRadius:D

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-int v14, v14

    add-int v11, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mCenterY:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/camera/ui/IndicatorControlWheel;->mWheelRadius:D

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-int v14, v14

    sub-int v12, v13, v14

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/ui/IndicatorControl;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-ne v9, v13, :cond_9

    const/4 v13, 0x0

    const/4 v14, 0x0

    sub-int v15, p4, p2

    sub-int v16, p5, p3

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    :cond_9
    div-int/lit8 v13, v10, 0x2

    sub-int v13, v11, v13

    div-int/lit8 v14, v3, 0x2

    sub-int v14, v12, v14

    div-int/lit8 v15, v10, 0x2

    add-int/2addr v15, v11

    div-int/lit8 v16, v3, 0x2

    add-int v16, v16, v12

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_4
.end method

.method public onTouchOutBound()V
    .locals 4

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    iget v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    iget-object v1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mLastMotionEvent:Landroid/view/MotionEvent;

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/ui/IndicatorControlWheel;->injectMotionEvent(ILandroid/view/MotionEvent;I)V

    iput v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mPressedIndex:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4
    .param p1    # Z

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/camera/ui/IndicatorControl;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mInitialized:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCloseIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mSecondLevelIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mCloseIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/camera/ui/IndicatorControlWheel;->enableZoom(Z)V

    goto :goto_3
.end method

.method public startTimeLapseAnimation(IJ)V
    .locals 2
    .param p1    # I
    .param p2    # J

    iput p1, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    iput-wide p2, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mRecordingStartTime:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mNumberOfFrames:J

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public stopTimeLapseAnimation()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ui/IndicatorControlWheel;->mTimeLapseInterval:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method
