.class public Lcom/android/camera/ui/IndicatorControlWheelContainer;
.super Lcom/android/camera/ui/IndicatorControlContainer;
.source "IndicatorControlWheelContainer.java"


# static fields
.field public static final SHUTTER_BUTTON_RADIUS:I = 0x4a

.field public static final STROKE_WIDTH:I = 0x57

.field private static final TAG:Ljava/lang/String; = "IndicatorControlWheelContainer"

.field public static final WHEEL_CENTER_TO_SECANT:I = 0x5d


# instance fields
.field private mCenterX:I

.field private mCenterY:I

.field private mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

.field private mShutterButton:Landroid/view/View;

.field private mShutterButtonRadius:D


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControlContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public disableCameraPicker()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    return-void
.end method

.method public dismissSecondLevelIndicator()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControlWheel;->dismissSecondLevelIndicator()V

    return-void
.end method

.method public dismissSettingPopup()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    move-result v0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    iget v10, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterX:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    float-to-double v1, v9

    iget v9, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterY:I

    int-to-float v9, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    sub-float/2addr v9, v10

    float-to-double v3, v9

    mul-double v9, v1, v1

    mul-double v11, v3, v3

    add-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    iget-wide v9, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButtonRadius:D

    cmpg-double v9, v5, v9

    if-gtz v9, :cond_4

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v9}, Lcom/android/camera/ui/IndicatorControlWheel;->onTouchOutBound()V

    :cond_2
    if-eqz v0, :cond_3

    if-ne v0, v8, :cond_0

    :cond_3
    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->isPressed()Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x3

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move v7, v8

    goto :goto_0

    :cond_5
    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v7, p1}, Lcom/android/camera/ui/IndicatorControlWheel;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_0
.end method

.method public enableFilter(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    return-void
.end method

.method public enableZoom(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControlWheel;->enableZoom(Z)V

    return-void
.end method

.method public getActiveSettingPopup()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->getActiveSettingPopup()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/PreferenceGroup;
    .param p3    # Z
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/camera/ui/IndicatorControlWheel;->initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;Z[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    const/16 v0, 0x4a

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButtonRadius:D

    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/IndicatorControlWheel;

    iput-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    return-void
.end method

.method public onIndicatorEvent(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/16 v4, 0x5d

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    sub-int v2, p4, p2

    invoke-static {v4}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterX:I

    sub-int v2, p5, p3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterY:I

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    sub-int v3, p4, p2

    sub-int/2addr v3, v1

    iget v4, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterY:I

    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v4, v5

    sub-int v5, p4, p2

    iget v6, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterY:I

    div-int/lit8 v7, v0, 0x2

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    :goto_0
    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    sub-int v3, p4, p2

    sub-int v4, p5, p3

    invoke-virtual {v2, v8, v8, v3, v4}, Landroid/view/ViewGroup;->layout(IIII)V

    return-void

    :cond_0
    sub-int v2, p4, p2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterX:I

    sub-int v2, p5, p3

    invoke-static {v4}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterY:I

    iget-object v2, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    iget v3, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterX:I

    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    sub-int v4, p5, p3

    sub-int/2addr v4, v0

    iget v5, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mCenterX:I

    div-int/lit8 v6, v1, 0x2

    add-int/2addr v5, v6

    sub-int v6, p5, p3

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x0

    const/high16 v8, -0x80000000

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7, v2, v2}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v7, v2, v2}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/lit8 v1, v7, 0x10

    iget-object v7, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mShutterButton:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/lit8 v0, v7, 0x10

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-nez v6, :cond_0

    move v5, v1

    :goto_0
    if-nez v3, :cond_2

    move v4, v0

    :goto_1
    invoke-virtual {p0, v5, v4}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void

    :cond_0
    if-ne v6, v8, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    goto :goto_0

    :cond_2
    if-ne v3, v8, :cond_3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_1

    :cond_3
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    goto :goto_1
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->overrideSettings([Ljava/lang/String;)V

    return-void
.end method

.method public reInitializeOtherSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->reInitializeOtherSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V

    return-void
.end method

.method public reloadPreferences()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControl;->reloadPreferences()V

    return-void
.end method

.method public setCameraPickerVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    iget-object v0, v0, Lcom/android/camera/ui/IndicatorControl;->mCameraPicker:Lcom/android/camera/ui/CameraPicker;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControlWheel;->setEnabled(Z)V

    return-void
.end method

.method public setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V
    .locals 1
    .param p1    # Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/IndicatorControl;->setListener(Lcom/android/camera/CameraPreference$OnPreferenceChangedListener;)V

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->setOrientation(IZ)V

    return-void
.end method

.method public startTimeLapseAnimation(IJ)V
    .locals 1
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/camera/ui/IndicatorControlWheel;->startTimeLapseAnimation(IJ)V

    return-void
.end method

.method public stopTimeLapseAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControlWheelContainer;->mIndicatorControlWheel:Lcom/android/camera/ui/IndicatorControlWheel;

    invoke-virtual {v0}, Lcom/android/camera/ui/IndicatorControlWheel;->stopTimeLapseAnimation()V

    return-void
.end method
