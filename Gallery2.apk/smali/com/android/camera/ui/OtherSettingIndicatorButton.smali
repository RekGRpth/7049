.class public Lcom/android/camera/ui/OtherSettingIndicatorButton;
.super Lcom/android/camera/ui/AbstractIndicatorButton;
.source "OtherSettingIndicatorButton.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OtherSettingIndicatorButton"


# instance fields
.field private mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

.field private mPrefKeys:[Ljava/lang/String;

.field private mPreferenceGroup:Lcom/android/camera/PreferenceGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/android/camera/PreferenceGroup;
    .param p4    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/camera/ui/AbstractIndicatorButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-object p3, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iput-object p4, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPrefKeys:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected initializePopup()V
    .locals 5

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0076

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const v3, 0x7f04002e

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/OtherSettingsPopup;

    iget-object v3, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    invoke-virtual {v1, v3}, Lcom/android/camera/ui/OtherSettingsPopup;->setSettingChangedListener(Lcom/android/camera/ui/OtherSettingsPopup$Listener;)V

    iget-object v3, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iget-object v4, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPrefKeys:[Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/android/camera/ui/OtherSettingsPopup;->initialize(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    return-void
.end method

.method public overrideSettings(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ui/OtherSettingIndicatorButton;->initializePopup()V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    check-cast v0, Lcom/android/camera/ui/OtherSettingsPopup;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/OtherSettingsPopup;->overrideSettings([Ljava/lang/String;)V

    return-void
.end method

.method public reInitialSetting(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iput-object p2, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mPrefKeys:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ui/AbstractIndicatorButton;->mPopup:Lcom/android/camera/ui/AbstractSettingPopup;

    return-void
.end method

.method public setSettingChangedListener(Lcom/android/camera/ui/OtherSettingsPopup$Listener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    iput-object p1, p0, Lcom/android/camera/ui/OtherSettingIndicatorButton;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    return-void
.end method
