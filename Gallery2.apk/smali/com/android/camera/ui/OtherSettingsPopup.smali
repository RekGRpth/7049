.class public Lcom/android/camera/ui/OtherSettingsPopup;
.super Lcom/android/camera/ui/AbstractSettingPopup;
.source "OtherSettingsPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/camera/ui/InLineSettingItem$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ui/OtherSettingsPopup$OtherSettingsAdapter;,
        Lcom/android/camera/ui/OtherSettingsPopup$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "OtherSettingsPopup"


# instance fields
.field private mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/camera/ListPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mListItemAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/camera/ListPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/AbstractSettingPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/OtherSettingsPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/camera/ui/OtherSettingsPopup;

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;)V
    .locals 7
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # [Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b00ab

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    const/4 v1, 0x0

    :goto_0
    array-length v5, p2

    if-ge v1, v5, :cond_2

    aget-object v5, p2, v1

    invoke-virtual {p1, v5}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v5, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/android/camera/ui/OtherSettingsPopup$OtherSettingsAdapter;

    invoke-direct {v5, p0}, Lcom/android/camera/ui/OtherSettingsPopup$OtherSettingsAdapter;-><init>(Lcom/android/camera/ui/OtherSettingsPopup;)V

    iput-object v5, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItemAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mSettingList:Landroid/view/ViewGroup;

    check-cast v5, Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItemAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v5, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mSettingList:Landroid/view/ViewGroup;

    check-cast v5, Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mSettingList:Landroid/view/ViewGroup;

    check-cast v5, Landroid/widget/ListView;

    const v6, 0x106000d

    invoke-virtual {v5, v6}, Landroid/widget/AbsListView;->setSelector(I)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    invoke-interface {v0}, Lcom/android/camera/ui/OtherSettingsPopup$Listener;->onRestorePreferencesClicked()V

    :cond_0
    return-void
.end method

.method public onSettingChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    invoke-interface {v0}, Lcom/android/camera/ui/OtherSettingsPopup$Listener;->onSettingChanged()V

    :cond_0
    return-void
.end method

.method public varargs overrideSettings([Ljava/lang/String;)V
    .locals 7
    .param p1    # [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    array-length v6, p1

    if-ge v1, v6, :cond_2

    aget-object v3, p1, v1

    add-int/lit8 v6, v1, 0x1

    aget-object v5, p1, v6

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    iget-object v6, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/camera/ListPreference;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4, v5}, Lcom/android/camera/ListPreference;->setOverrideValue(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItemAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_2
    return-void
.end method

.method public reloadPreference()V
    .locals 5

    iget-object v4, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mSettingList:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v4, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ListPreference;

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/camera/ui/AbstractSettingPopup;->mSettingList:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v3}, Lcom/android/camera/ui/InLineSettingItem;->reloadPreference()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setSettingChangedListener(Lcom/android/camera/ui/OtherSettingsPopup$Listener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    iput-object p1, p0, Lcom/android/camera/ui/OtherSettingsPopup;->mListener:Lcom/android/camera/ui/OtherSettingsPopup$Listener;

    return-void
.end method
