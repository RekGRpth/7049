.class public Lcom/android/camera/ui/SecondLevelIndicatorControlBar;
.super Lcom/android/camera/ui/IndicatorControl;
.source "SecondLevelIndicatorControlBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/camera/ui/AbstractIndicatorButton$IndicatorChangeListener;


# static fields
.field private static ICON_SPACING:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SecondLevelIndicatorControlBar"


# instance fields
.field private mCloseIcon:Landroid/view/View;

.field private mDivider:Landroid/view/View;

.field mNonIndicatorButtonCount:I

.field mOrientation:I

.field private mPopupedIndicator:Landroid/view/View;

.field mSelectedIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    sput v0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mOrientation:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    return-void
.end method

.method private dispatchRelativeTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    invoke-virtual {p1, p2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    return-void
.end method

.method private getTouchViewIndex(IZ)I
    .locals 9
    .param p1    # I
    .param p2    # Z

    const/4 v6, -0x1

    if-eqz p2, :cond_1

    iget-object v7, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v7

    if-ge p1, v7, :cond_2

    iget-object v6, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v6

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v7, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v7

    if-le p1, v7, :cond_2

    iget-object v6, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v6

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    iget v7, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    if-eq v2, v7, :cond_0

    iget v7, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz p2, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v7

    sget v8, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    div-int/lit8 v8, v8, 0x2

    sub-int v0, v7, v8

    if-lt p1, v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sget v6, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    add-int v1, v4, v6

    iget v6, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    sub-int v7, p1, v0

    div-int/2addr v7, v1

    add-int/2addr v6, v7

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v7

    sget v8, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    div-int/lit8 v8, v8, 0x2

    add-int v0, v7, v8

    if-gt p1, v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sget v6, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    add-int v1, v5, v6

    iget v6, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    sub-int v7, v0, p1

    div-int/2addr v7, v1

    add-int/2addr v6, v7

    goto :goto_0
.end method


# virtual methods
.method public addIndicator(Landroid/content/Context;Lcom/android/camera/IconListPreference;)Lcom/android/camera/ui/IndicatorButton;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/IconListPreference;

    invoke-super {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->addIndicator(Landroid/content/Context;Lcom/android/camera/IconListPreference;)Lcom/android/camera/ui/IndicatorButton;

    move-result-object v0

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->setIndicatorChangeListener(Lcom/android/camera/ui/AbstractIndicatorButton$IndicatorChangeListener;)V

    return-object v0
.end method

.method public addOtherSettingIndicator(Landroid/content/Context;I[Ljava/lang/String;)Lcom/android/camera/ui/OtherSettingIndicatorButton;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3}, Lcom/android/camera/ui/IndicatorControl;->addOtherSettingIndicator(Landroid/content/Context;I[Ljava/lang/String;)Lcom/android/camera/ui/OtherSettingIndicatorButton;

    move-result-object v0

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/AbstractIndicatorButton;->setIndicatorChangeListener(Lcom/android/camera/ui/AbstractIndicatorButton$IndicatorChangeListener;)V

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1    # Landroid/view/MotionEvent;

    const/4 v13, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v11

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return v9

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    if-ne v11, v13, :cond_4

    move v4, v10

    :goto_1
    if-eqz v4, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    :goto_2
    float-to-int v7, v11

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    :goto_3
    if-eqz v2, :cond_0

    if-lt v7, v2, :cond_2

    add-int/lit8 v7, v2, -0x1

    :cond_2
    invoke-direct {p0, v7, v4}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->getTouchViewIndex(IZ)I

    move-result v3

    iget v11, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_3

    iget v11, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    if-eq v3, v11, :cond_3

    iget v11, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    invoke-virtual {p0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v11, 0x3

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-direct {p0, v6, p1}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->dispatchRelativeTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    instance-of v11, v6, Lcom/android/camera/ui/AbstractIndicatorButton;

    if-eqz v11, :cond_3

    move-object v1, v6

    check-cast v1, Lcom/android/camera/ui/AbstractIndicatorButton;

    invoke-virtual {v1}, Lcom/android/camera/ui/AbstractIndicatorButton;->dismissPopup()Z

    :cond_3
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    if-nez v8, :cond_7

    move v9, v10

    goto :goto_0

    :cond_4
    move v4, v9

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    goto :goto_3

    :cond_7
    iget v11, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    if-eq v11, v3, :cond_8

    if-ne v0, v13, :cond_8

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_8
    invoke-direct {p0, v8, p1}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->dispatchRelativeTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)V

    iput v3, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mSelectedIndex:I

    move v9, v10

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;Lcom/android/camera/PreferenceGroup;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/camera/PreferenceGroup;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p2}, Lcom/android/camera/ui/IndicatorControl;->setPreferenceGroup(Lcom/android/camera/PreferenceGroup;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    sub-int v0, v2, v3

    if-lez v0, :cond_0

    iget v2, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    invoke-virtual {p0, v2, v0}, Lcom/android/camera/ui/IndicatorControl;->removeControls(II)V

    :cond_0
    invoke-virtual {p0, p3, p4}, Lcom/android/camera/ui/IndicatorControl;->addControls([Ljava/lang/String;[Ljava/lang/String;)V

    iget v2, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mOrientation:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mOrientation:I

    invoke-virtual {p0, v2, v1}, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->setOrientation(IZ)V

    :cond_1
    iget v2, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/camera/ui/IndicatorControl;->setupFilter(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/camera/ui/IndicatorControl;->dismissSettingPopup()Z

    iget-object v0, p0, Lcom/android/camera/ui/IndicatorControl;->mOnIndicatorEventListener:Lcom/android/camera/ui/OnIndicatorEventListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/camera/ui/OnIndicatorEventListener;->onIndicatorEvent(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f0b004a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mDivider:Landroid/view/View;

    const v0, 0x7f0b004b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sub-int v9, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    move v6, v7

    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    const/4 v11, 0x0

    add-int v12, v6, v4

    invoke-virtual {v10, v11, v6, v9, v12}, Landroid/view/View;->layout(IIII)V

    add-int v10, v4, v7

    add-int/2addr v6, v10

    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mDivider:Landroid/view/View;

    sub-int v11, v9, v7

    iget-object v12, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mDivider:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v6

    invoke-virtual {v10, v7, v6, v11, v12}, Landroid/view/View;->layout(IIII)V

    sub-int v10, v2, v4

    sub-int v8, v10, v7

    sget v10, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    add-int v1, v4, v10

    add-int/lit8 v3, v0, -0x1

    :goto_1
    iget v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    if-lt v3, v10, :cond_0

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    add-int v12, v8, v4

    invoke-virtual {v10, v11, v8, v9, v12}, Landroid/view/View;->layout(IIII)V

    sub-int/2addr v8, v1

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_2
    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v7

    move v6, v7

    sget v10, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->ICON_SPACING:I

    add-int v5, v4, v10

    add-int/lit8 v3, v0, -0x1

    :goto_2
    iget v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mNonIndicatorButtonCount:I

    if-lt v3, v10, :cond_3

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    add-int v12, v6, v4

    invoke-virtual {v10, v6, v11, v12, v2}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v6, v5

    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_3
    sub-int v10, v9, v4

    mul-int/lit8 v11, v7, 0x2

    sub-int v6, v10, v11

    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mDivider:Landroid/view/View;

    iget-object v11, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mDivider:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v11, v6

    sub-int v12, v2, v7

    invoke-virtual {v10, v6, v7, v11, v12}, Landroid/view/View;->layout(IIII)V

    sub-int v10, v9, v4

    sub-int v6, v10, v7

    iget-object v10, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    const/4 v11, 0x0

    add-int v12, v6, v4

    invoke-virtual {v10, v6, v11, v12, v2}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0
.end method

.method public onShowIndicator(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mPopupedIndicator:Landroid/view/View;

    if-eq v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    :goto_1
    iput-object p1, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mPopupedIndicator:Landroid/view/View;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/ui/IndicatorControl;->setEnabled(Z)V

    iget v0, p0, Lcom/android/camera/ui/IndicatorControl;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mCloseIcon:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setOrientation(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    iput p1, p0, Lcom/android/camera/ui/SecondLevelIndicatorControlBar;->mOrientation:I

    invoke-super {p0, p1, p2}, Lcom/android/camera/ui/IndicatorControl;->setOrientation(IZ)V

    return-void
.end method
