.class public Lcom/android/camera/ui/ZoomControlBar;
.super Lcom/android/camera/ui/ZoomControl;
.source "ZoomControlBar.java"


# static fields
.field private static final ICON_SPACING:I

.field private static final TAG:Ljava/lang/String; = "ZoomControlBar"

.field private static final THRESHOLD_FIRST_MOVE:I


# instance fields
.field private mBar:Landroid/view/View;

.field private mIconSize:I

.field private mSize:I

.field private mSliderLength:I

.field private mSliderPosition:I

.field private mStartChanging:Z

.field private mTotalIconSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    sput v0, Lcom/android/camera/ui/ZoomControlBar;->THRESHOLD_FIRST_MOVE:I

    const/16 v0, 0xc

    invoke-static {v0}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v0

    sput v0, Lcom/android/camera/ui/ZoomControlBar;->ICON_SPACING:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/ZoomControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    const v1, 0x7f020164

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private getSliderPosition(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/android/camera/ui/ZoomControl;->mOrientation:I

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int v0, p1, v1

    :goto_0
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    :cond_1
    return v0

    :cond_2
    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v2, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int/2addr v1, v2

    sub-int v0, v1, p1

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/camera/ui/ZoomControl;->mOrientation:I

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v2, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int/2addr v1, v2

    sub-int v0, v1, p1

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int v0, p1, v1

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    move v4, v5

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/android/camera/ui/ZoomControlBar;->setActivated(Z)V

    invoke-virtual {p0}, Lcom/android/camera/ui/ZoomControl;->closeZoomControl()V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/android/camera/ui/ZoomControlBar;->setActivated(Z)V

    iput-boolean v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mStartChanging:Z

    :pswitch_2
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_5

    move v2, v5

    :goto_2
    if-eqz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    :goto_3
    float-to-int v4, v4

    invoke-direct {p0, v4}, Lcom/android/camera/ui/ZoomControlBar;->getSliderPosition(I)I

    move-result v3

    iget-boolean v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mStartChanging:Z

    if-nez v4, :cond_3

    iget v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    sub-int v1, v4, v3

    sget v4, Lcom/android/camera/ui/ZoomControlBar;->THRESHOLD_FIRST_MOVE:I

    if-gt v1, v4, :cond_2

    sget v4, Lcom/android/camera/ui/ZoomControlBar;->THRESHOLD_FIRST_MOVE:I

    neg-int v4, v4

    if-ge v1, v4, :cond_3

    :cond_2
    iput-boolean v5, p0, Lcom/android/camera/ui/ZoomControlBar;->mStartChanging:Z

    :cond_3
    iget-boolean v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mStartChanging:Z

    if-eqz v4, :cond_4

    const-wide/high16 v6, 0x3ff0000000000000L

    int-to-double v8, v3

    mul-double/2addr v6, v8

    iget v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    int-to-double v8, v4

    div-double/2addr v6, v8

    invoke-virtual {p0, v6, v7}, Lcom/android/camera/ui/ZoomControl;->performZoom(D)V

    iput v3, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    :cond_4
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_1

    :cond_5
    move v2, v4

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomMax:I

    if-nez v6, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    if-eqz v0, :cond_2

    sub-int v2, p4, p2

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v10, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    :goto_2
    iget v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    iget v4, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    :goto_3
    if-eqz v0, :cond_5

    iget v6, p0, Lcom/android/camera/ui/ZoomControl;->mOrientation:I

    const/16 v7, 0xb4

    if-ne v6, v7, :cond_4

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomOut:Landroid/widget/ImageView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    add-int v1, v6, v4

    :goto_4
    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomSlider:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomSlider:Landroid/widget/ImageView;

    const/4 v7, 0x0

    div-int/lit8 v8, v3, 0x2

    sub-int v8, v1, v8

    div-int/lit8 v9, v3, 0x2

    add-int/2addr v9, v1

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    :cond_2
    sub-int v2, p5, p3

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    iget v7, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v10, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    :cond_3
    iget v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    int-to-double v6, v6

    iget v8, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIndex:I

    int-to-double v8, v8

    mul-double/2addr v6, v8

    iget v8, p0, Lcom/android/camera/ui/ZoomControl;->mZoomMax:I

    int-to-double v8, v8

    div-double/2addr v6, v8

    double-to-int v4, v6

    goto :goto_3

    :cond_4
    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomOut:Landroid/widget/ImageView;

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    invoke-virtual {v6, v7, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v6

    sub-int v1, v6, v4

    goto :goto_4

    :cond_5
    iget v6, p0, Lcom/android/camera/ui/ZoomControl;->mOrientation:I

    const/16 v7, 0x5a

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomOut:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v8, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    sub-int/2addr v7, v8

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    sub-int v1, v6, v4

    :goto_5
    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomSlider:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomSlider:Landroid/widget/ImageView;

    div-int/lit8 v7, v5, 0x2

    sub-int v7, v1, v7

    const/4 v8, 0x0

    div-int/lit8 v9, v5, 0x2

    add-int/2addr v9, v1

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_1

    :cond_6
    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomOut:Landroid/widget/ImageView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v8, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    sub-int/2addr v7, v8

    const/4 v8, 0x0

    iget v9, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    invoke-virtual {v6, v7, v8, v9, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    add-int v1, v6, v4

    goto :goto_5
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "ZoomControlBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "w="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " oldw="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " oldh="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iput p2, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    :goto_0
    iget v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    sget v1, Lcom/android/camera/ui/ZoomControlBar;->ICON_SPACING:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    iget v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget v1, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    const-string v0, "ZoomControlBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTotalIconSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/ui/ZoomControlBar;->mTotalIconSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSliderLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iput p1, p0, Lcom/android/camera/ui/ZoomControlBar;->mSize:I

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mIconSize:I

    goto :goto_0
.end method

.method public setActivated(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/ui/ZoomControl;->setActivated(Z)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setActivated(Z)V

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/16 v1, 0xb4

    if-eq p1, v1, :cond_0

    iget v0, p0, Lcom/android/camera/ui/ZoomControl;->mOrientation:I

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/camera/ui/ZoomControl;->setOrientation(IZ)V

    return-void
.end method

.method public setZoomIndex(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/camera/ui/ZoomControl;->setZoomIndex(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/ui/ZoomControlBar;->mSliderPosition:I

    return-void
.end method
