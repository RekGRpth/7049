.class public Lcom/android/camera/ui/ZoomControlWheel;
.super Lcom/android/camera/ui/ZoomControl;
.source "ZoomControlWheel.java"


# static fields
.field private static final HIGHLIGHT_DEGREES:I = 0x1e

.field private static final LANDSCAPE_ZOOM_IN_ICON_DEGREES:I = 0x60

.field private static final LANDSCAPE_ZOOM_OUT_ICON_DEGREES:I = 0x108

.field private static final TAG:Ljava/lang/String; = "ZoomControlWheel"

.field private static final TRAIL_WIDTH:I = 0x2


# instance fields
.field private final DEFAULT_SLIDER_POSITION:I

.field private final MAX_SLIDER_ANGLE:I

.field private final MIN_SLIDER_ANGLE:I

.field private final SLIDER_RANGE:D

.field private final TRAIL_COLOR:I

.field private final ZOOM_IN_ICON_DEGREES:I

.field private final ZOOM_OUT_ICON_DEGREES:I

.field private mBackgroundPaint:Landroid/graphics/Paint;

.field private mBackgroundRect:Landroid/graphics/RectF;

.field private mCenterX:I

.field private mCenterY:I

.field private mRotateAngle:D

.field private mShutterButtonRadius:D

.field private mSliderRadians:D

.field private mStrokeWidth:I

.field private mWheelRadius:D


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/ZoomControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->TRAIL_COLOR:I

    const/16 v1, 0x4a

    invoke-static {v1}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v1

    int-to-double v1, v1

    iput-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mShutterButtonRadius:D

    const/16 v1, 0x57

    invoke-static {v1}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v1

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mStrokeWidth:I

    iget-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mShutterButtonRadius:D

    iget v3, p0, Lcom/android/camera/ui/ZoomControlWheel;->mStrokeWidth:I

    int-to-double v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    iput-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mWheelRadius:D

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/16 v1, 0x60

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_IN_ICON_DEGREES:I

    const/16 v1, 0x108

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_OUT_ICON_DEGREES:I

    :goto_0
    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_OUT_ICON_DEGREES:I

    add-int/lit8 v1, v1, -0xf

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_IN_ICON_DEGREES:I

    add-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->DEFAULT_SLIDER_POSITION:I

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    iget v2, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    sub-int/2addr v1, v2

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->SLIDER_RANGE:D

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->DEFAULT_SLIDER_POSITION:I

    int-to-double v1, v1

    iput-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    return-void

    :cond_0
    const/4 v1, 0x6

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_IN_ICON_DEGREES:I

    const/16 v1, 0xae

    iput v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_OUT_ICON_DEGREES:I

    goto :goto_0
.end method

.method private drawArc(Landroid/graphics/Canvas;IIDII)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # D
    .param p6    # I
    .param p7    # I

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    int-to-double v1, v1

    sub-double/2addr v1, p4

    double-to-float v1, v1

    iget v2, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    int-to-double v2, v2

    sub-double/2addr v2, p4

    double-to-float v2, v2

    iget v3, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    int-to-double v3, v3

    add-double/2addr v3, p4

    double-to-float v3, v3

    iget v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    int-to-double v4, v4

    add-double/2addr v4, p4

    double-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    int-to-float v1, p7

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundRect:Landroid/graphics/RectF;

    int-to-float v2, p2

    int-to-float v3, p3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/camera/ui/ZoomControlWheel;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    return-void
.end method

.method private getSliderDrawAngle(D)D
    .locals 2
    .param p1    # D

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p1

    :cond_0
    :goto_0
    return-wide p1

    :cond_1
    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p1

    goto :goto_0
.end method

.method private layoutIcon(Landroid/view/View;D)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # D

    iget-wide v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mRotateAngle:D

    add-double/2addr p2, v4

    iget v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    iget-wide v5, p0, Lcom/android/camera/ui/ZoomControlWheel;->mWheelRadius:D

    invoke-static {p2, p3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    add-int v2, v4, v5

    iget v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    iget-wide v5, p0, Lcom/android/camera/ui/ZoomControlWheel;->mWheelRadius:D

    invoke-static {p2, p3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    sub-int v3, v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v4, v1, 0x2

    sub-int v4, v2, v4

    div-int/lit8 v5, v0, 0x2

    sub-int v5, v3, v5

    div-int/lit8 v6, v1, 0x2

    add-int/2addr v6, v2

    div-int/lit8 v7, v0, 0x2

    add-int/2addr v7, v3

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0, p1}, Landroid/view/View;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    iget v10, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    float-to-double v3, v9

    iget v9, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    int-to-float v9, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    sub-float/2addr v9, v10

    float-to-double v5, v9

    mul-double v9, v3, v3

    mul-double v11, v5, v5

    add-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    const-wide/16 v9, 0x0

    cmpg-double v9, v1, v9

    if-gez v9, :cond_2

    const-wide v9, 0x401921fb54442d18L

    add-double/2addr v1, v9

    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/android/camera/ui/ZoomControlWheel;->getSliderDrawAngle(D)D

    move-result-wide v9

    iput-wide v9, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    packed-switch v0, :pswitch_data_0

    :goto_1
    const/4 v9, 0x1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/camera/ui/ZoomControl;->closeZoomControl()V

    goto :goto_1

    :pswitch_1
    iget v9, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    int-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v9

    iget-wide v11, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    sub-double/2addr v9, v11

    iget-wide v11, p0, Lcom/android/camera/ui/ZoomControlWheel;->SLIDER_RANGE:D

    div-double/2addr v9, v11

    invoke-virtual {p0, v9, v10}, Lcom/android/camera/ui/ZoomControl;->performZoom(D)V

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    neg-int v0, v0

    iget-wide v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mRotateAngle:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-int v1, v4

    sub-int v2, v0, v1

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    sub-int v3, v0, v1

    add-int v0, v2, v3

    if-lez v0, :cond_0

    neg-int v3, v2

    :cond_0
    iget-wide v4, p0, Lcom/android/camera/ui/ZoomControlWheel;->mWheelRadius:D

    iget v6, p0, Lcom/android/camera/ui/ZoomControlWheel;->TRAIL_COLOR:I

    const/4 v7, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/camera/ui/ZoomControlWheel;->drawArc(Landroid/graphics/Canvas;IIDII)V

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/16 v2, 0x5d

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sub-int v0, p4, p2

    invoke-static {v2}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    sub-int v0, p5, p3

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    :goto_0
    iget-object v0, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIn:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_IN_ICON_DEGREES:I

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/ui/ZoomControlWheel;->layoutIcon(Landroid/view/View;D)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControl;->mZoomOut:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->ZOOM_OUT_ICON_DEGREES:I

    int-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/ui/ZoomControlWheel;->layoutIcon(Landroid/view/View;D)V

    iget-object v0, p0, Lcom/android/camera/ui/ZoomControl;->mZoomSlider:Landroid/widget/ImageView;

    iget-wide v1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    invoke-direct {p0, v1, v2}, Lcom/android/camera/ui/ZoomControlWheel;->getSliderDrawAngle(D)D

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/ui/ZoomControlWheel;->layoutIcon(Landroid/view/View;D)V

    return-void

    :cond_0
    sub-int v0, p4, p2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterX:I

    sub-int v0, p5, p3

    invoke-static {v2}, Lcom/android/camera/Util;->dpToPixel(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mCenterY:I

    goto :goto_0
.end method

.method public rotate(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/android/camera/ui/ZoomControlWheel;->mRotateAngle:D

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    return-void
.end method

.method public setZoomIndex(I)V
    .locals 6
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/camera/ui/ZoomControl;->setZoomIndex(I)V

    iget v2, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    iget v3, p0, Lcom/android/camera/ui/ZoomControlWheel;->MIN_SLIDER_ANGLE:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    iget v4, p0, Lcom/android/camera/ui/ZoomControl;->mZoomMax:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    iget v4, p0, Lcom/android/camera/ui/ZoomControl;->mZoomIndex:I

    int-to-double v4, v4

    mul-double v0, v2, v4

    iget v2, p0, Lcom/android/camera/ui/ZoomControlWheel;->MAX_SLIDER_ANGLE:I

    int-to-double v2, v2

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    return-void
.end method

.method public startZoomControl()V
    .locals 2

    invoke-super {p0}, Lcom/android/camera/ui/ZoomControl;->startZoomControl()V

    iget v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->DEFAULT_SLIDER_POSITION:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/ui/ZoomControlWheel;->mSliderRadians:D

    return-void
.end method
