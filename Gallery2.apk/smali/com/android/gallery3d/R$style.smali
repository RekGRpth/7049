.class public final Lcom/android/gallery3d/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ActionBarArrow:I = 0x7f0d002a

.field public static final ActionBarBackLinearLayout:I = 0x7f0d0028

.field public static final ActionBarIcon:I = 0x7f0d002b

.field public static final ActionBarImageView:I = 0x7f0d0029

.field public static final ActionBarInner:I = 0x7f0d0025

.field public static final ActionBarLinearLayout:I = 0x7f0d0027

.field public static final ActionBarOuter:I = 0x7f0d0026

.field public static final ActionBarTitle:I = 0x7f0d002c

.field public static final Animation_OnScreenHint:I = 0x7f0d0003

.field public static final DialogPickerTheme:I = 0x7f0d0036

.field public static final Effect:I = 0x7f0d0020

.field public static final EffectIcon:I = 0x7f0d001e

.field public static final EffectLabel:I = 0x7f0d001f

.field public static final EffectLabelInToolPanel:I = 0x7f0d0023

.field public static final EffectSettingGrid:I = 0x7f0d0010

.field public static final EffectSettingItem:I = 0x7f0d0011

.field public static final EffectSettingItemTitle:I = 0x7f0d0012

.field public static final EffectSettingTypeTitle:I = 0x7f0d0013

.field public static final EffectTitleSeparator:I = 0x7f0d0015

.field public static final EffectTypeSeparator:I = 0x7f0d0014

.field public static final EffectsBar:I = 0x7f0d0022

.field public static final EffectsContainer:I = 0x7f0d0021

.field public static final EffectsMenuActionButton:I = 0x7f0d0030

.field public static final EffectsMenuContainer:I = 0x7f0d002f

.field public static final FirstRunButtonStyle:I = 0x7f0d0039

.field public static final FirstRunTextStyle:I = 0x7f0d0038

.field public static final FirstRunTitleStyle:I = 0x7f0d0037

.field public static final FullscreenToolView:I = 0x7f0d0031

.field public static final Holo_ActionBar:I = 0x7f0d0034

.field public static final ImageActionButton:I = 0x7f0d002d

.field public static final MediaButton_Play:I = 0x7f0d0035

.field public static final OnScreenHintTextAppearance:I = 0x7f0d0001

.field public static final OnScreenHintTextAppearance_Small:I = 0x7f0d0002

.field public static final OnScreenIndicator:I = 0x7f0d000c

.field public static final OnScreenIndicators:I = 0x7f0d0019

.field public static final OnViewfinderLabel:I = 0x7f0d000e

.field public static final PanoCustomDialogText:I = 0x7f0d000f

.field public static final PanoViewHorizontalBar:I = 0x7f0d001b

.field public static final PopupTitleSeparator:I = 0x7f0d0006

.field public static final PopupTitleText:I = 0x7f0d0018

.field public static final ReviewControlGroup:I = 0x7f0d0016

.field public static final ReviewControlIcon:I = 0x7f0d0004

.field public static final ReviewControlText:I = 0x7f0d001d

.field public static final ReviewPlayIcon:I = 0x7f0d0005

.field public static final ReviewThumbnail:I = 0x7f0d001c

.field public static final SeekBar:I = 0x7f0d0024

.field public static final SettingItemList:I = 0x7f0d0007

.field public static final SettingItemText:I = 0x7f0d0009

.field public static final SettingItemTitle:I = 0x7f0d0008

.field public static final SettingKnob:I = 0x7f0d000b

.field public static final SettingPopupWindow:I = 0x7f0d0017

.field public static final SettingRow:I = 0x7f0d000a

.field public static final SpinnerProgressDialog:I = 0x7f0d0032

.field public static final TextActionButton:I = 0x7f0d002e

.field public static final ThemeCamera:I = 0x7f0d0000

.field public static final Theme_Gallery:I = 0x7f0d0033

.field public static final ViewfinderLabelLayout:I = 0x7f0d001a

.field public static final ViewfinderLableLayoutTimeRect:I = 0x7f0d000d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
