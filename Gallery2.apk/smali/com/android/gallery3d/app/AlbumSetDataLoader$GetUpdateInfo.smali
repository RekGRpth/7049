.class Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;
.super Ljava/lang/Object;
.source "AlbumSetDataLoader.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/AlbumSetDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUpdateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mVersion:J

.field final synthetic this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/app/AlbumSetDataLoader;J)V
    .locals 0
    .param p2    # J

    iput-object p1, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->mVersion:J

    return-void
.end method

.method private getInvalidIndex(J)I
    .locals 7
    .param p1    # J

    iget-object v5, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v5}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$400(Lcom/android/gallery3d/app/AlbumSetDataLoader;)[J

    move-result-object v4

    array-length v2, v4

    iget-object v5, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v5}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$500(Lcom/android/gallery3d/app/AlbumSetDataLoader;)I

    move-result v0

    iget-object v5, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v5}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$600(Lcom/android/gallery3d/app/AlbumSetDataLoader;)I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_1

    rem-int v1, v0, v2

    rem-int v5, v0, v2

    aget-wide v5, v4, v5

    cmp-long v5, v5, p1

    if-eqz v5, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public call()Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->mVersion:J

    invoke-direct {p0, v3, v4}, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->getInvalidIndex(J)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v3}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$700(Lcom/android/gallery3d/app/AlbumSetDataLoader;)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->mVersion:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    move-object v1, v2

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;

    invoke-direct {v1, v2}, Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;-><init>(Lcom/android/gallery3d/app/AlbumSetDataLoader$1;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v2}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$700(Lcom/android/gallery3d/app/AlbumSetDataLoader;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->version:J

    iput v0, v1, Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->index:I

    iget-object v2, p0, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->this$0:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-static {v2}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->access$900(Lcom/android/gallery3d/app/AlbumSetDataLoader;)I

    move-result v2

    iput v2, v1, Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->size:I

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;->call()Lcom/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;

    move-result-object v0

    return-object v0
.end method
