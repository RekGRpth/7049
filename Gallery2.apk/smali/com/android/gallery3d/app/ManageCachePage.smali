.class public Lcom/android/gallery3d/app/ManageCachePage;
.super Lcom/android/gallery3d/app/ActivityState;
.source "ManageCachePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/gallery3d/app/EyePosition$EyePositionListener;
.implements Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;
.implements Lcom/android/gallery3d/ui/SelectionManager$SelectionListener;


# static fields
.field private static final DATA_CACHE_SIZE:I = 0x100

.field public static final KEY_MEDIA_PATH:Ljava/lang/String; = "media-path"

.field private static final MSG_REFRESH_STORAGE_INFO:I = 0x1

.field private static final MSG_REQUEST_LAYOUT:I = 0x2

.field private static final PROGRESS_BAR_MAX:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "ManageCachePage"


# instance fields
.field private mAlbumCountToMakeAvailableOffline:I

.field private mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

.field private mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

.field private mEyePosition:Lcom/android/gallery3d/app/EyePosition;

.field private mFooterContent:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mLayoutReady:Z

.field private mMediaSet:Lcom/android/gallery3d/data/MediaSet;

.field private mRootPane:Lcom/android/gallery3d/ui/GLView;

.field protected mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

.field protected mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

.field private mSlotView:Lcom/android/gallery3d/ui/SlotView;

.field private mUpdateStorageInfo:Lcom/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateStorageInfoJob:Lcom/android/gallery3d/util/ThreadPool$Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mX:F

.field private mY:F

.field private mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/app/ActivityState;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mLayoutReady:Z

    new-instance v0, Lcom/android/gallery3d/app/ManageCachePage$1;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/ManageCachePage$1;-><init>(Lcom/android/gallery3d/app/ManageCachePage;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    new-instance v0, Lcom/android/gallery3d/app/ManageCachePage$3;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/ManageCachePage$3;-><init>(Lcom/android/gallery3d/app/ManageCachePage;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfoJob:Lcom/android/gallery3d/util/ThreadPool$Job;

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/app/ManageCachePage;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mLayoutReady:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/gallery3d/app/ManageCachePage;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mLayoutReady:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/ManageCachePage;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/ManageCachePage;I)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/ManageCachePage;->onDown(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/app/ManageCachePage;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->onUp()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/gallery3d/app/ManageCachePage;)Lcom/android/gallery3d/app/EyePosition;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mEyePosition:Lcom/android/gallery3d/app/EyePosition;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/gallery3d/app/ManageCachePage;)Lcom/android/gallery3d/ui/SlotView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/app/ManageCachePage;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mX:F

    return v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/app/ManageCachePage;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mY:F

    return v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/app/ManageCachePage;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mZ:F

    return v0
.end method

.method static synthetic access$700(Lcom/android/gallery3d/app/ManageCachePage;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->refreshCacheStorageInfo()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/ManageCachePage;)Lcom/android/gallery3d/ui/GLView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/ManageCachePage;)Lcom/android/gallery3d/ui/CacheStorageUsageInfo;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/ManageCachePage;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    return-object v0
.end method

.method private initializeData(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const-string v1, "media-path"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-interface {v1}, Lcom/android/gallery3d/app/GalleryActivity;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/android/gallery3d/data/MediaSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mMediaSet:Lcom/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    iget-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mMediaSet:Lcom/android/gallery3d/data/MediaSet;

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/SelectionManager;->setSourceMediaSet(Lcom/android/gallery3d/data/MediaSet;)V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/SelectionManager;->setAutoLeaveSelectionMode(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/SelectionManager;->enterSelectionMode()V

    new-instance v1, Lcom/android/gallery3d/app/AlbumSetDataLoader;

    iget-object v2, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    iget-object v3, p0, Lcom/android/gallery3d/app/ManageCachePage;->mMediaSet:Lcom/android/gallery3d/data/MediaSet;

    const/16 v4, 0x100

    invoke-direct {v1, v2, v3, v4}, Lcom/android/gallery3d/app/AlbumSetDataLoader;-><init>(Lcom/android/gallery3d/app/GalleryActivity;Lcom/android/gallery3d/data/MediaSet;I)V

    iput-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    iget-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->setModel(Lcom/android/gallery3d/app/AlbumSetDataLoader;)V

    return-void
.end method

.method private initializeFooterViews()V
    .locals 4

    iget-object v0, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040020

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    iget-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    const v3, 0x7f0b0052

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->refreshCacheStorageInfo()V

    return-void
.end method

.method private initializeViews()V
    .locals 9

    iget-object v7, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v7, Landroid/app/Activity;

    new-instance v0, Lcom/android/gallery3d/ui/SelectionManager;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/gallery3d/ui/SelectionManager;-><init>(Lcom/android/gallery3d/app/GalleryContext;Z)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/ui/SelectionManager;->setSelectionListener(Lcom/android/gallery3d/ui/SelectionManager$SelectionListener;)V

    invoke-static {v7}, Lcom/android/gallery3d/app/Config$ManageCachePage;->get(Landroid/content/Context;)Lcom/android/gallery3d/app/Config$ManageCachePage;

    move-result-object v8

    new-instance v0, Lcom/android/gallery3d/ui/SlotView;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    iget-object v2, v8, Lcom/android/gallery3d/app/Config$AlbumSetPage;->slotViewSpec:Lcom/android/gallery3d/ui/SlotView$Spec;

    invoke-direct {v0, v1, v2}, Lcom/android/gallery3d/ui/SlotView;-><init>(Lcom/android/gallery3d/app/GalleryActivity;Lcom/android/gallery3d/ui/SlotView$Spec;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    new-instance v0, Lcom/android/gallery3d/ui/ManageCacheDrawer;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    iget-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    iget-object v3, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    iget-object v4, v8, Lcom/android/gallery3d/app/Config$AlbumSetPage;->labelSpec:Lcom/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;

    iget v5, v8, Lcom/android/gallery3d/app/Config$ManageCachePage;->cachePinSize:I

    iget v6, v8, Lcom/android/gallery3d/app/Config$ManageCachePage;->cachePinMargin:I

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/ui/ManageCacheDrawer;-><init>(Lcom/android/gallery3d/app/GalleryActivity;Lcom/android/gallery3d/ui/SelectionManager;Lcom/android/gallery3d/ui/SlotView;Lcom/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;II)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/SlotView;->setSlotRenderer(Lcom/android/gallery3d/ui/SlotView$SlotRenderer;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    new-instance v1, Lcom/android/gallery3d/app/ManageCachePage$4;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/ManageCachePage$4;-><init>(Lcom/android/gallery3d/app/ManageCachePage;)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/SlotView;->setListener(Lcom/android/gallery3d/ui/SlotView$Listener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->initializeFooterViews()V

    return-void
.end method

.method private onDown(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->setPressedIndex(I)V

    return-void
.end method

.method private onUp()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->setPressedIndex(I)V

    return-void
.end method

.method private refreshCacheStorageInfo()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    const v14, 0x7f0b0051

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    const v14, 0x7f0b0050

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const/16 v13, 0x2710

    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setMax(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    invoke-virtual {v13}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;->getTotalBytes()J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    invoke-virtual {v13}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;->getUsedBytes()J

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    invoke-virtual {v13}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;->getExpectedUsedBytes()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    invoke-virtual {v13}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;->getFreeBytes()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v1, Landroid/app/Activity;

    const-wide/16 v13, 0x0

    cmp-long v13, v9, v13

    if-nez v13, :cond_0

    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    const v13, 0x7f0c0236

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "-"

    aput-object v16, v14, v15

    invoke-virtual {v1, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const-wide/16 v13, 0x2710

    mul-long/2addr v13, v11

    div-long/2addr v13, v9

    long-to-int v13, v13

    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setProgress(I)V

    const-wide/16 v13, 0x2710

    mul-long/2addr v13, v2

    div-long/2addr v13, v9

    long-to-int v13, v13

    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    const v13, 0x7f0c0236

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v1, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v1, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showToast()V
    .locals 4

    iget v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100004

    iget v3, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private showToastForLocalAlbum()V
    .locals 3

    iget-object v0, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0231

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0b0052

    if-ne v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v3, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-interface {v3}, Lcom/android/gallery3d/app/GalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    :try_start_0
    iget-object v3, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/ui/SelectionManager;->getSelected(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/ActivityState;->onBackPressed()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v2}, Lcom/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->showToast()V

    new-instance v1, Lcom/android/gallery3d/ui/MenuExecutor;

    iget-object v3, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    iget-object v4, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    invoke-direct {v1, v3, v4}, Lcom/android/gallery3d/ui/MenuExecutor;-><init>(Lcom/android/gallery3d/app/GalleryActivity;Lcom/android/gallery3d/ui/SelectionManager;)V

    const v3, 0x7f0b0003

    const v4, 0x7f0c01fd

    invoke-virtual {v1, v3, v4, p0}, Lcom/android/gallery3d/ui/MenuExecutor;->startAction(IILcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v2}, Lcom/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {v2}, Lcom/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->initializeFooterViews()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0b004f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onConfirmDialogDismissed(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onConfirmDialogShown()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/os/Bundle;

    new-instance v0, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;-><init>(Lcom/android/gallery3d/app/GalleryActivity;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->initializeViews()V

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/ManageCachePage;->initializeData(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/gallery3d/app/EyePosition;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-interface {v1}, Lcom/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/gallery3d/app/EyePosition;-><init>(Landroid/content/Context;Lcom/android/gallery3d/app/EyePosition$EyePositionListener;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mEyePosition:Lcom/android/gallery3d/app/EyePosition;

    new-instance v0, Lcom/android/gallery3d/app/ManageCachePage$2;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-interface {v1}, Lcom/android/gallery3d/app/GalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/ManageCachePage$2;-><init>(Lcom/android/gallery3d/app/ManageCachePage;Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onEyePositionChanged(FFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/GLView;->lockRendering()V

    iput p1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mX:F

    iput p2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mY:F

    iput p3, p0, Lcom/android/gallery3d/app/ManageCachePage;->mZ:F

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/GLView;->unlockRendering()V

    iget-object v0, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/android/gallery3d/app/ActivityState;->onPause()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->pause()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->pause()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mEyePosition:Lcom/android/gallery3d/app/EyePosition;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/EyePosition;->pause()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfo:Lcom/android/gallery3d/util/Future;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfo:Lcom/android/gallery3d/util/Future;

    invoke-interface {v1}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfo:Lcom/android/gallery3d/util/Future;

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0b004f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onProgressComplete(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/app/ActivityState;->onBackPressed()V

    return-void
.end method

.method public onProgressUpdate(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/gallery3d/app/ActivityState;->onResume()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mRootPane:Lcom/android/gallery3d/ui/GLView;

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/app/ActivityState;->setContentPane(Lcom/android/gallery3d/ui/GLView;)V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->resume()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionDrawer:Lcom/android/gallery3d/ui/ManageCacheDrawer;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->resume()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mEyePosition:Lcom/android/gallery3d/app/EyePosition;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/EyePosition;->resume()V

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    invoke-interface {v1}, Lcom/android/gallery3d/app/GalleryActivity;->getThreadPool()Lcom/android/gallery3d/util/ThreadPool;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfoJob:Lcom/android/gallery3d/util/ThreadPool$Job;

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;)Lcom/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mUpdateStorageInfo:Lcom/android/gallery3d/util/Future;

    iget-object v1, p0, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0b004f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/gallery3d/app/ManageCachePage;->mFooterContent:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onSelectionChange(Lcom/android/gallery3d/data/Path;Z)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Z

    return-void
.end method

.method public onSelectionModeChange(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onSingleTapUp(I)V
    .locals 8
    .param p1    # I

    iget-object v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumSetDataAdapter:Lcom/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v6, p1}, Lcom/android/gallery3d/app/AlbumSetDataLoader;->getMediaSet(I)Lcom/android/gallery3d/data/MediaSet;

    move-result-object v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    and-int/lit16 v6, v6, 0x100

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->showToastForLocalAlbum()V

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getCacheFlag()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-object v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/android/gallery3d/ui/SelectionManager;->isItemSelected(Lcom/android/gallery3d/data/Path;)Z

    move-result v1

    if-nez v0, :cond_2

    if-eqz v1, :cond_5

    iget v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    :cond_2
    :goto_2
    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getCacheSize()J

    move-result-wide v3

    iget-object v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mCacheStorageInfo:Lcom/android/gallery3d/ui/CacheStorageUsageInfo;

    xor-int v7, v0, v1

    if-eqz v7, :cond_3

    neg-long v3, v3

    :cond_3
    invoke-virtual {v6, v3, v4}, Lcom/android/gallery3d/ui/CacheStorageUsageInfo;->increaseTargetCacheSize(J)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/ManageCachePage;->refreshCacheStorageInfo()V

    iget-object v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/android/gallery3d/ui/SelectionManager;->toggle(Lcom/android/gallery3d/data/Path;)V

    iget-object v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mSlotView:Lcom/android/gallery3d/ui/SlotView;

    invoke-virtual {v6}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/gallery3d/app/ManageCachePage;->mAlbumCountToMakeAvailableOffline:I

    goto :goto_2
.end method
