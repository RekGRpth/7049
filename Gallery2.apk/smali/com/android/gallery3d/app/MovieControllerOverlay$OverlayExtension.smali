.class Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;
.super Ljava/lang/Object;
.source "MovieControllerOverlay.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MovieControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverlayExtension"
.end annotation


# instance fields
.field private mAlwaysShowBottom:Z

.field private mCanPause:Z

.field private mEnableScrubbing:Z

.field private mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

.field private mPlayingInfo:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/android/gallery3d/app/MovieControllerOverlay;)V
    .locals 1

    iput-object p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mCanPause:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mEnableScrubbing:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$1;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/MovieControllerOverlay;
    .param p2    # Lcom/android/gallery3d/app/MovieControllerOverlay$1;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;-><init>(Lcom/android/gallery3d/app/MovieControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public canHidePanel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mAlwaysShowBottom:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearBuffering()V
    .locals 3

    const/4 v2, -0x1

    const-string v0, "Gallery3D/MovieControllerOverlay"

    const-string v1, "clearBuffering()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/app/TimeBar;->setSecondaryProgress(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->showBuffering(ZI)V

    return-void
.end method

.method public handleHide()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mAlwaysShowBottom:Z

    return v0
.end method

.method public handleShowPaused()Z
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->BUFFERING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PAUSED:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iput-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleShowPlaying()Z
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->BUFFERING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PLAYING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iput-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleUpdateViews()Z
    .locals 3

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$600(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->LOADING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->ERROR:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->BUFFERING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->RETRY_CONNECTING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->ENDED:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v2, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->RETRY_CONNECTING_ERROR:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mCanPause:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mPlayingInfo:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PLAYING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mPlayingInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public isPlayingEnd()Z
    .locals 4

    const-string v1, "Gallery3D/MovieControllerOverlay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPlayingEnd() state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->ENDED:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    if-eq v1, v2, :cond_0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->ERROR:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    if-eq v1, v2, :cond_0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->RETRY_CONNECTING_ERROR:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public onShowEnded()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->clearBuffering()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    return-void
.end method

.method public onShowErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->clearBuffering()V

    return-void
.end method

.method public onShowLoading()V
    .locals 3

    const v0, 0x2050055

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    return-void
.end method

.method public onShowMainView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const-string v0, "Gallery3D/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showMainView("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") errorView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$700(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", loadingView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$400(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", playPauseReplayView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$600(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery3D/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showMainView() enableScrubbing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mEnableScrubbing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mEnableScrubbing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PAUSED:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PLAYING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/TimeBar;->setScrubbing(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/TimeBar;->setScrubbing(Z)V

    goto :goto_0
.end method

.method public setBottomPanel(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mAlwaysShowBottom:Z

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    :goto_0
    const-string v0, "Gallery3D/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBottomPanel("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    const v1, 0x7f020121

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->show()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->hide()V

    goto :goto_0
.end method

.method public setCanPause(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mCanPause:Z

    const-string v0, "Gallery3D/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCanPause("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setCanScrubbing(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mEnableScrubbing:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/TimeBar;->setScrubbing(Z)V

    const-string v0, "Gallery3D/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCanScrubbing("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setPlayingInfo(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    const v0, 0x2050053

    :goto_0
    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mPlayingInfo:Ljava/lang/String;

    const-string v1, "Gallery3D/MovieControllerOverlay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPlayingInfo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") playingInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mPlayingInfo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const v0, 0x2050054

    goto :goto_0
.end method

.method public showBuffering(ZI)V
    .locals 7
    .param p1    # Z
    .param p2    # I

    const/4 v6, 0x0

    const/16 v5, 0x64

    const-string v2, "Gallery3D/MovieControllerOverlay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showBuffering("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lastState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v4}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/android/gallery3d/app/TimeBar;->setSecondaryProgress(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    sget-object v3, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PAUSED:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    sget-object v3, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->PLAYING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    if-ne v2, v3, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$200(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    :cond_2
    if-ltz p2, :cond_3

    if-ge p2, v5, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    sget-object v3, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->BUFFERING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$202(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$State;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    const v0, 0x205004f

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$400(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$500(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    goto :goto_0

    :cond_3
    if-ne p2, v5, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$202(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$State;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$600(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$500(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->mLastState:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$202(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$State;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public showReconnecting(I)V
    .locals 6
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->clearBuffering()V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    sget-object v3, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->RETRY_CONNECTING:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$202(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$State;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    const v0, 0x7f0c015d

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$400(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$500(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    const-string v2, "Gallery3D/MovieControllerOverlay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showReconnecting("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public showReconnectingError()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->clearBuffering()V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    sget-object v3, Lcom/android/gallery3d/app/MovieControllerOverlay$State;->RETRY_CONNECTING_ERROR:Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$202(Lcom/android/gallery3d/app/MovieControllerOverlay;Lcom/android/gallery3d/app/MovieControllerOverlay$State;)Lcom/android/gallery3d/app/MovieControllerOverlay$State;

    const v0, 0x2050058

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/app/TimeBar;->setInfo(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$OverlayExtension;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$600(Lcom/android/gallery3d/app/MovieControllerOverlay;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$500(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    const-string v2, "Gallery3D/MovieControllerOverlay"

    const-string v3, "showReconnectingError()"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
