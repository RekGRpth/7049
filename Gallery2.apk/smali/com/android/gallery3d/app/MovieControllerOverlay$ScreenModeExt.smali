.class Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;
.super Ljava/lang/Object;
.source "MovieControllerOverlay.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mediatek/gallery3d/video/ScreenModeManager$ScreenModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MovieControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScreenModeExt"
.end annotation


# static fields
.field private static final MARGIN:I = 0xa


# instance fields
.field private mParent:Landroid/view/ViewGroup;

.field private mScreenPadding:I

.field private mScreenView:Landroid/widget/ImageView;

.field private mScreenWidth:I

.field final synthetic this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;


# direct methods
.method constructor <init>(Lcom/android/gallery3d/app/MovieControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateScreenModeDrawable()V
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$800(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/mediatek/gallery3d/video/ScreenModeManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/ScreenModeManager;->getNextScreenMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const v2, 0x7f02009e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const v2, 0x7f0200a0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const v2, 0x7f02009f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAddedRightPadding()I
    .locals 2

    iget v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenPadding:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenWidth:I

    add-int/2addr v0, v1

    return v0
.end method

.method init(Landroid/content/Context;Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x2

    const/high16 v6, 0x41200000

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v6

    float-to-int v1, v4

    invoke-virtual {p2, v1, v5, v1, v5}, Landroid/view/View;->setPadding(IIII)V

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const v5, 0x7f0200a0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02009e

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenWidth:I

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenPadding:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    return-void
.end method

.method public onCancelHiding()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$800(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/mediatek/gallery3d/video/ScreenModeManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$800(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/mediatek/gallery3d/video/ScreenModeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$800(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/mediatek/gallery3d/video/ScreenModeManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/ScreenModeManager;->getNextScreenMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/ScreenModeManager;->setScreenMode(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->show()V

    :cond_0
    return-void
.end method

.method public onHide()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onLayout(III)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->getAddedRightPadding()I

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    sub-int v2, p1, p2

    sub-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$300(Lcom/android/gallery3d/app/MovieControllerOverlay;)Lcom/android/gallery3d/app/TimeBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/app/TimeBar;->getPreferredHeight()I

    move-result v3

    sub-int v3, p3, v3

    sub-int v4, p1, p2

    invoke-virtual {v1, v2, v3, v4, p3}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public onScreenModeChanged(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->updateScreenModeDrawable()V

    return-void
.end method

.method public onShow()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-static {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$1000(Lcom/android/gallery3d/app/MovieControllerOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->onHide()V

    goto :goto_0
.end method

.method public onStartHiding()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ScreenModeExt;->mScreenView:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$900(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    return-void
.end method
