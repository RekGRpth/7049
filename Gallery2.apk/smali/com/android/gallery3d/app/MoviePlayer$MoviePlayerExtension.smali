.class Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Lcom/android/gallery3d/app/MoviePlayer$Restorable;
.implements Lcom/mediatek/gallery3d/ext/IMoviePlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoviePlayerExtension"
.end annotation


# static fields
.field private static final KEY_VIDEO_IS_LOOP:Ljava/lang/String; = "video_is_loop"


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mBookmark:Lcom/mediatek/gallery3d/video/BookmarkEnhance;

.field private mCopyRight:Ljava/lang/String;

.field private mIsLoop:Z

.field private mLastCanPaused:Z

.field private mLastPlaying:Z

.field private mPauseBuffering:Z

.field private mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/gallery3d/app/MoviePlayer;


# direct methods
.method private constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p2    # Lcom/android/gallery3d/app/MoviePlayer$1;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->pauseIfNeed()V

    return-void
.end method

.method static synthetic access$3200(Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->resumeIfNeed()V

    return-void
.end method

.method private pauseIfNeed()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->canStop()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastCanPaused:Z

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastCanPaused:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$2500(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MoviePlayer$TState;

    move-result-object v0

    sget-object v3, Lcom/android/gallery3d/app/MoviePlayer$TState;->PLAYING:Lcom/android/gallery3d/app/MoviePlayer$TState;

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$2600(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->clearBuffering()V

    iput-boolean v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mPauseBuffering:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$1700(Lcom/android/gallery3d/app/MoviePlayer;)V

    :cond_1
    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseIfNeed() mLastPlaying="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mLastCanPaused="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastCanPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPauseBuffering="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mPauseBuffering:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private resumeIfNeed()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastCanPaused:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mPauseBuffering:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3500(Lcom/android/gallery3d/app/MoviePlayer;)V

    :cond_0
    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeIfNeed() mLastPlaying="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mLastCanPaused="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastCanPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPauseBuffering="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mPauseBuffering:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public addBookmark()V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mBookmark:Lcom/mediatek/gallery3d/video/BookmarkEnhance;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/gallery3d/video/BookmarkEnhance;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/gallery3d/video/BookmarkEnhance;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mBookmark:Lcom/mediatek/gallery3d/video/BookmarkEnhance;

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mBookmark:Lcom/mediatek/gallery3d/video/BookmarkEnhance;

    invoke-virtual {v0, v2}, Lcom/mediatek/gallery3d/video/BookmarkEnhance;->exists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v0

    const v1, 0x7f0c0164

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addBookmark() mTitle="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", mUri="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mBookmark:Lcom/mediatek/gallery3d/video/BookmarkEnhance;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/gallery3d/video/BookmarkEnhance;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v0

    const v1, 0x7f0c0165

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public canStop()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$2600(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->isPlayingEnd()Z

    move-result v0

    :cond_0
    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "canStop() stopped="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLoop()Z
    .locals 3

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLoop() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    return v0
.end method

.method public onFirstShow(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->pauseIfNeed()V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFirstShow() mLastPlaying="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onLastDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->resumeIfNeed()V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLastDismiss() mLastPlaying="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mLastPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_is_loop"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setCanReplay(Z)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_is_loop"

    iget-boolean v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public pauseBuffering()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mPauseBuffering:Z

    return v0
.end method

.method public setLoop(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLoop("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mIsLoop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer;->isLocalFile()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mIsLoop:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setCanReplay(Z)V

    :cond_0
    return-void
.end method

.method public setParameter(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setParameter(II)Z

    move-result v0

    return v0
.end method

.method public setVideoInfo(Landroid/media/Metadata;)V
    .locals 4
    .param p1    # Landroid/media/Metadata;

    const/16 v3, 0xa

    const/4 v2, 0x7

    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mTitle:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1, v3}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v3}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mAuthor:Ljava/lang/String;

    :cond_1
    invoke-virtual {p1, v2}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v2}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mCopyRight:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public showDetail()V
    .locals 5

    new-instance v0, Lcom/mediatek/gallery3d/video/DetailDialog;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mAuthor:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->mCopyRight:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mediatek/gallery3d/video/DetailDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0c0170

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    new-instance v1, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension$1;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension$1;-><init>(Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    new-instance v1, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension$2;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension$2;-><init>(Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public startNextVideo(Lcom/mediatek/gallery3d/ext/IMovieItem;)V
    .locals 6
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-object v1, p1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v2

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v0

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$3300(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/Bookmarker;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v4}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v4

    invoke-interface {v4}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v0}, Lcom/android/gallery3d/app/Bookmarker;->setBookmark(Landroid/net/Uri;II)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/VideoView;->stopPlayback()V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$2900(Lcom/android/gallery3d/app/MoviePlayer;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3, v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$802(Lcom/android/gallery3d/app/MoviePlayer;Lcom/mediatek/gallery3d/ext/IMovieItem;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v4}, Lcom/android/gallery3d/app/MoviePlayer;->access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/app/MovieActivity;->refreshMovieInfo(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$3400(Lcom/android/gallery3d/app/MoviePlayer;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/SurfaceView;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->closeOptionsMenu()V

    return-void

    :cond_0
    const-string v3, "MoviePlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot play the next video! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/ext/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopVideo()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MoviePlayer"

    const-string v1, "stopVideo()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    sget-object v1, Lcom/android/gallery3d/app/MoviePlayer$TState;->STOPED:Lcom/android/gallery3d/app/MoviePlayer$TState;

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$2502(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$TState;)Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->clearSeek()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->clearDuration()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setResumed(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$2900(Lcom/android/gallery3d/app/MoviePlayer;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0, v2}, Lcom/android/gallery3d/app/MoviePlayer;->access$3002(Lcom/android/gallery3d/app/MoviePlayer;Z)Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setCanReplay(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showEnded()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$500(Lcom/android/gallery3d/app/MoviePlayer;)I

    return-void
.end method

.method public updateUI()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$900(Lcom/android/gallery3d/app/MoviePlayer;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->hide()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->show()V

    :cond_3
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3700(Lcom/android/gallery3d/app/MoviePlayer;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0
.end method
