.class Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Lcom/android/gallery3d/app/MoviePlayer$Restorable;
.implements Lcom/mediatek/gallery3d/video/ScreenModeManager$ScreenModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenModeExt"
.end annotation


# static fields
.field private static final KEY_VIDEO_SCREEN_MODE:Ljava/lang/String; = "video_screen_mode"


# instance fields
.field private mScreenMode:I

.field private mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

.field final synthetic this$0:Lcom/android/gallery3d/app/MoviePlayer;


# direct methods
.method private constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 1

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    new-instance v0, Lcom/mediatek/gallery3d/video/ScreenModeManager;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/video/ScreenModeManager;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p2    # Lcom/android/gallery3d/app/MoviePlayer$1;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    return-void
.end method


# virtual methods
.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_screen_mode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_screen_mode"

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onScreenModeChanged(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnScreenModeClicked("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setScreenMode()V
    .locals 3

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setScreenModeManager(Lcom/mediatek/gallery3d/video/ScreenModeManager;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setScreenModeManager(Lcom/mediatek/gallery3d/video/ScreenModeManager;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

    invoke-virtual {v0, p0}, Lcom/mediatek/gallery3d/video/ScreenModeManager;->addListener(Lcom/mediatek/gallery3d/video/ScreenModeManager$ScreenModeListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenModeManager:Lcom/mediatek/gallery3d/video/ScreenModeManager;

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/ScreenModeManager;->setScreenMode(I)V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setScreenMode() mScreenMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->mScreenMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
