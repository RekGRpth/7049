.class public Lcom/android/gallery3d/app/MoviePlayer;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Lcom/android/gallery3d/app/ControllerOverlay$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/MoviePlayer$14;,
        Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;,
        Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;,
        Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;,
        Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;,
        Lcom/android/gallery3d/app/MoviePlayer$Restorable;,
        Lcom/android/gallery3d/app/MoviePlayer$TState;,
        Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;
    }
.end annotation


# static fields
.field private static final BLACK_TIMEOUT:J = 0x1f4L

.field private static final CMDNAME:Ljava/lang/String; = "command"

.field private static final CMDPAUSE:Ljava/lang/String; = "pause"

.field private static final DELAY_REMOVE_MS:I = 0x2710

.field private static final KEY_CONSUMED_DRM_RIGHT:Ljava/lang/String; = "consumed_drm_right"

.field private static final KEY_POSITION_WHEN_PAUSED:Ljava/lang/String; = "video_position_when_paused"

.field private static final KEY_RESUMEABLE_TIME:Ljava/lang/String; = "resumeable-timeout"

.field private static final KEY_VIDEO_CAN_PAUSE:Ljava/lang/String; = "video_can_pause"

.field private static final KEY_VIDEO_CAN_SEEK:Ljava/lang/String; = "video_can_seek"

.field private static final KEY_VIDEO_LAST_DISCONNECT_TIME:Ljava/lang/String; = "last_disconnect_time"

.field private static final KEY_VIDEO_LAST_DURATION:Ljava/lang/String; = "video_last_duration"

.field private static final KEY_VIDEO_POSITION:Ljava/lang/String; = "video-position"

.field private static final KEY_VIDEO_STATE:Ljava/lang/String; = "video_state"

.field private static final KEY_VIDEO_STREAMING_TYPE:Ljava/lang/String; = "video_streaming_type"

.field private static final LOG:Z = true

.field private static final RESUMEABLE_TIMEOUT:J = 0x2bf20L

.field private static final SERVICECMD:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final STREAMING_HTTP:I = 0x1

.field public static final STREAMING_LOCAL:I = 0x0

.field public static final STREAMING_RTSP:I = 0x2

.field public static final STREAMING_SDP:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MoviePlayer"


# instance fields
.field private final mActionBar:Landroid/app/ActionBar;

.field private mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

.field private final mAudioBecomingNoisyReceiver:Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;

.field private final mBookmarker:Lcom/android/gallery3d/app/Bookmarker;

.field private mCanReplay:Z

.field private mConsumedDrmRight:Z

.field private mContext:Landroid/content/Context;

.field private final mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

.field private mDelayVideoRunnable:Ljava/lang/Runnable;

.field private mDragging:Z

.field private mDrmExt:Lcom/mediatek/gallery3d/ext/IMovieDrmExtension;

.field private mFirstBePlayed:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHasPaused:Z

.field private mIsOnlyAudio:Z

.field private mLastSystemUiVis:I

.field private mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

.field private mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

.field private mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

.field private final mPlayingChecker:Ljava/lang/Runnable;

.field private final mProgressChecker:Ljava/lang/Runnable;

.field private final mRemoveBackground:Ljava/lang/Runnable;

.field private mResumeableTime:J

.field private mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

.field private final mRootView:Landroid/view/View;

.field private mScreenModeExt:Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

.field private mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

.field private mShowing:Z

.field private mStreamingType:I

.field private mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

.field private mVideoCanPause:Z

.field private mVideoCanSeek:Z

.field private mVideoLastDuration:I

.field private mVideoPosition:I

.field private final mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

.field private mWaitMetaData:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/android/gallery3d/app/MovieActivity;Lcom/mediatek/gallery3d/ext/IMovieItem;Landroid/os/Bundle;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/android/gallery3d/app/MovieActivity;
    .param p3    # Lcom/mediatek/gallery3d/ext/IMovieItem;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mLastSystemUiVis:I

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$1;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/app/MoviePlayer$1;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayingChecker:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$2;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/app/MoviePlayer$2;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRemoveBackground:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$3;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/app/MoviePlayer$3;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mProgressChecker:Ljava/lang/Runnable;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanPause:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/ExtensionHelper;->getMovieDrmExtension(Landroid/content/Context;)Lcom/mediatek/gallery3d/ext/IMovieDrmExtension;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDrmExt:Lcom/mediatek/gallery3d/ext/IMovieDrmExtension;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    sget-object v2, Lcom/android/gallery3d/app/MoviePlayer$TState;->PLAYING:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mScreenModeExt:Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$13;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/app/MoviePlayer$13;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDelayVideoRunnable:Ljava/lang/Runnable;

    invoke-virtual {p2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {p2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRootView:Landroid/view/View;

    const v2, 0x7f0b0075

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/video/MTKVideoView;

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v2, Lcom/android/gallery3d/app/Bookmarker;

    invoke-direct {v2, p2}, Lcom/android/gallery3d/app/Bookmarker;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mBookmarker:Lcom/android/gallery3d/app/Bookmarker;

    new-instance v2, Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    check-cast p1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2}, Lcom/android/gallery3d/app/MovieControllerOverlay;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2, p0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setListener(Lcom/android/gallery3d/app/ControllerOverlay$Listener;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2, p5}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setCanReplay(Z)V

    invoke-direct {p0, p2, p3, p5}, Lcom/android/gallery3d/app/MoviePlayer;->init(Lcom/android/gallery3d/app/MovieActivity;Lcom/mediatek/gallery3d/ext/IMovieItem;Z)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v3, Lcom/android/gallery3d/app/MoviePlayer$4;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/MoviePlayer$4;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    invoke-virtual {v2, v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setSurfaceListener(Lcom/mediatek/gallery3d/video/MTKVideoView$SurfaceListener;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v3, Lcom/android/gallery3d/app/MoviePlayer$5;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/MoviePlayer$5;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v3, Lcom/android/gallery3d/app/MoviePlayer$6;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/MoviePlayer$6;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v3, Lcom/android/gallery3d/app/MoviePlayer$7;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/MoviePlayer$7;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/MoviePlayer;->showSystemUi(Z)V

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mAudioBecomingNoisyReceiver:Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mAudioBecomingNoisyReceiver:Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;

    invoke-virtual {v2}, Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;->register()V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p2, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    if-eqz p4, :cond_0

    const-string v2, "video-position"

    const/4 v3, 0x0

    invoke-virtual {p4, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    const-string v2, "resumeable-timeout"

    const-wide v3, 0x7fffffffffffffffL

    invoke-virtual {p4, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    invoke-direct {p0, p4}, Lcom/android/gallery3d/app/MoviePlayer;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mScreenModeExt:Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

    invoke-virtual {v2}, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->setScreenMode()V

    return-void

    :cond_0
    sget-object v2, Lcom/android/gallery3d/app/MoviePlayer$TState;->PLAYING:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mBookmarker:Lcom/android/gallery3d/app/Bookmarker;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/app/Bookmarker;->getBookmark(Landroid/net/Uri;)Lcom/android/gallery3d/app/BookmarkerInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, v0}, Lcom/android/gallery3d/app/MoviePlayer;->showResumeDialog(Landroid/content/Context;Lcom/android/gallery3d/app/BookmarkerInfo;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->startVideoCareDrm()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/MoviePlayer;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mLastSystemUiVis:I

    return v0
.end method

.method static synthetic access$1002(Lcom/android/gallery3d/app/MoviePlayer;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mLastSystemUiVis:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/app/MoviePlayer;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/android/gallery3d/app/MoviePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/app/MoviePlayer;ZII)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideoCareDrm(ZII)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/gallery3d/app/MoviePlayer;ZII)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideo(ZII)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/gallery3d/app/MoviePlayer;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayingChecker:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/gallery3d/app/MoviePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MoviePlayer$TState;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$TState;)Lcom/android/gallery3d/app/MoviePlayer$TState;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/gallery3d/app/MoviePlayer;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    return v0
.end method

.method static synthetic access$2800(Lcom/android/gallery3d/app/MoviePlayer;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    return v0
.end method

.method static synthetic access$2900(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->clearVideoInfo()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/gallery3d/app/MoviePlayer;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/Bookmarker;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mBookmarker:Lcom/android/gallery3d/app/Bookmarker;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->startVideoCareDrm()V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->playVideo()V

    return-void
.end method

.method static synthetic access$3600(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/app/ActionBar;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/gallery3d/app/MoviePlayer;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isMoviePartialVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/app/MoviePlayer;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/app/MoviePlayer;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/app/MoviePlayer;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mProgressChecker:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieActivity;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IMovieItem;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/gallery3d/app/MoviePlayer;Lcom/mediatek/gallery3d/ext/IMovieItem;)Lcom/mediatek/gallery3d/ext/IMovieItem;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    return-object p1
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->updateDisplayElement()V

    return-void
.end method

.method private addBackground()V
    .locals 2

    const-string v0, "MoviePlayer"

    const-string v1, "addBackground()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRemoveBackground:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRootView:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private clearVideoInfo()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->access$1800(Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;)V

    :cond_0
    return-void
.end method

.method private doOnPause()V
    .locals 12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->addBackground()V

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v8}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    iput v5, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v8}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mBookmarker:Lcom/android/gallery3d/app/Bookmarker;

    iget-object v9, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v9}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v9

    iget v10, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iget v11, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-virtual {v8, v9, v10, v11}, Lcom/android/gallery3d/app/Bookmarker;->setBookmark(Landroid/net/Uri;II)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v8}, Landroid/widget/VideoView;->stopPlayback()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/32 v10, 0x2bf20

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setResumed(Z)V

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/SurfaceView;->setVisibility(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-interface {v8}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->clearBuffering()V

    iget-object v8, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-static {v8}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->access$1400(Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;)V

    const-string v8, "MoviePlayer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doOnPause() save video info consume:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v1, v6

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "MoviePlayer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doOnPause() suspend video consume:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v3, v1

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "MoviePlayer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doOnPause() mVideoPosition="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mResumeableTime="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mVideoLastDuration="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v5, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    goto/16 :goto_0

    :cond_1
    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    goto/16 :goto_1
.end method

.method private doStartVideo(ZII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideo(ZIIZ)V

    return-void
.end method

.method private doStartVideo(ZIIZ)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    const-string v3, "MoviePlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doStartVideo("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "http"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "rtsp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "https"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showLoading()V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLiveStreaming()Z

    move-result v4

    invoke-interface {v3, v4}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->setPlayingInfo(Z)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayingChecker:Ljava/lang/Runnable;

    const-wide/16 v5, 0xfa

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    iget-boolean v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mWaitMetaData:Z

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4, v5, v6, v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;Z)V

    if-eqz p4, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->start()V

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->getLoop()Z

    move-result v1

    if-eqz v1, :cond_7

    move v0, v1

    :goto_2
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v3, v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setCanReplay(Z)V

    if-lez p2, :cond_3

    iget-boolean v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->canSeekForward()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v3, p2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_3
    if-eqz p1, :cond_4

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v3, p3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setDuration(I)V

    :cond_4
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    return-void

    :cond_5
    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showPlaying()V

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->hide()V

    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    :cond_7
    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mCanReplay:Z

    goto :goto_2
.end method

.method private doStartVideoCareDrm(ZII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doStartVideoCareDrm("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/gallery3d/app/MoviePlayer$TState;->PLAYING:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDrmExt:Lcom/mediatek/gallery3d/ext/IMovieDrmExtension;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    new-instance v3, Lcom/android/gallery3d/app/MoviePlayer$12;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer$12;-><init>(Lcom/android/gallery3d/app/MoviePlayer;ZII)V

    invoke-interface {v0, v1, v2, v3}, Lcom/mediatek/gallery3d/ext/IMovieDrmExtension;->handleDrmFile(Landroid/content/Context;Lcom/mediatek/gallery3d/ext/IMovieItem;Lcom/mediatek/gallery3d/ext/IMovieDrmExtension$IMovieDrmCallback;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideo(ZII)V

    :cond_0
    return-void
.end method

.method private dump()V
    .locals 4

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dump() mHasPaused="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoPosition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mResumeableTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoLastDuration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mDragging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mConsumedDrmRight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoCanSeek="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoCanPause="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanPause:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getVideoInfo(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v5, 0x3

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v3}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MovieUtils;->isLocalFile(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1, v4, v4}, Landroid/media/MediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-virtual {v2, v0}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->setVideoInfo(Landroid/media/Metadata;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v2, v0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->setVideoInfo(Landroid/media/Metadata;)V

    :goto_0
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-gtz v1, :cond_3

    iput v5, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    :cond_0
    :goto_1
    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoInfo() duration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mStreamingType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    const-string v2, "MoviePlayer"

    const-string v3, "Metadata is null!"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    if-ne v2, v5, :cond_0

    const/4 v2, 0x2

    iput v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    goto :goto_1
.end method

.method private init(Lcom/android/gallery3d/app/MovieActivity;Lcom/mediatek/gallery3d/ext/IMovieItem;Z)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/app/MovieActivity;
    .param p2    # Lcom/mediatek/gallery3d/ext/IMovieItem;
    .param p3    # Z

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    iput-boolean p3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mCanReplay:Z

    iput-object p2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {p2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/app/MoviePlayer;->judgeStreamingType(Landroid/net/Uri;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRootView:Landroid/view/View;

    new-instance v1, Lcom/android/gallery3d/app/MoviePlayer$11;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/MoviePlayer$11;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->getOverlayExt()Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    return-void
.end method

.method private static isMediaKey(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMoviePartialVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieActivity;->isPartialVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private judgeStreamingType(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "judgeStreamingType("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    iput-boolean v5, p0, Lcom/android/gallery3d/app/MoviePlayer;->mWaitMetaData:Z

    invoke-static {p1, p2}, Lcom/mediatek/gallery3d/ext/MovieUtils;->isSdpStreaming(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    :goto_1
    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mStreamingType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mCanGetMetaData="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mWaitMetaData:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {p1, p2}, Lcom/mediatek/gallery3d/ext/MovieUtils;->isRtspStreaming(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    goto :goto_1

    :cond_2
    invoke-static {p1, p2}, Lcom/mediatek/gallery3d/ext/MovieUtils;->isHttpStreaming(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput v5, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    iput-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mWaitMetaData:Z

    goto :goto_1

    :cond_3
    iput v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    iput-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mWaitMetaData:Z

    goto :goto_1
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_last_duration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    const-string v0, "video_can_pause"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanPause:Z

    const-string v0, "video_can_seek"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    const-string v0, "consumed_drm_right"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    const-string v0, "video_streaming_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    const-string v0, "video_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer$TState;->valueOf(Ljava/lang/String;)Lcom/android/gallery3d/app/MoviePlayer$TState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mScreenModeExt:Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestoreInstanceState("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onSaveInstanceStateMore(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "video_last_duration"

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "video_can_pause"

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->canPause()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "video_can_seek"

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->canSeekForward()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "consumed_drm_right"

    iget-boolean v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "video_streaming_type"

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "video_state"

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mScreenModeExt:Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$ScreenModeExt;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private pauseVideo()V
    .locals 2

    const-string v0, "MoviePlayer"

    const-string v1, "pauseVideo()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/gallery3d/app/MoviePlayer$TState;->PAUSED:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showPaused()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    return-void
.end method

.method private pauseVideoMoreThanThreeMinutes()V
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLiveStreaming()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/ExtensionHelper;->getMovieStrategy(Landroid/content/Context;)Lcom/mediatek/gallery3d/ext/IMovieStrategy;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/gallery3d/ext/IMovieStrategy;->shouldEnableCheckLongSleep()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanPause:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canPause()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    :cond_1
    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pauseVideoMoreThanThreeMinutes() now="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private playVideo()V
    .locals 2

    const-string v0, "MoviePlayer"

    const-string v1, "playVideo()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/gallery3d/app/MoviePlayer$TState;->PLAYING:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showPlaying()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    return-void
.end method

.method private removeBackground()V
    .locals 4

    const-string v0, "MoviePlayer"

    const-string v1, "removeBackground()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRemoveBackground:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRemoveBackground:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setProgress()I
    .locals 5

    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setProgress() mDragging="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mShowing="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mShowing:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mIsOnlyAudio="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mShowing:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    if-nez v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v0

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v2, v1, v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setTimes(II)V

    goto :goto_0
.end method

.method private showResumeDialog(Landroid/content/Context;Lcom/android/gallery3d/app/BookmarkerInfo;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/gallery3d/app/BookmarkerInfo;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c01d4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c01d5

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lcom/android/gallery3d/app/BookmarkerInfo;->mBookmark:I

    div-int/lit16 v4, v4, 0x3e8

    invoke-static {p1, v4}, Lcom/android/gallery3d/util/GalleryUtils;->formatDuration(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/gallery3d/app/MoviePlayer$8;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/MoviePlayer$8;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c01d6

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$9;

    invoke-direct {v2, p0, p2}, Lcom/android/gallery3d/app/MoviePlayer$9;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/BookmarkerInfo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c01db

    new-instance v2, Lcom/android/gallery3d/app/MoviePlayer$10;

    invoke-direct {v2, p0, p2}, Lcom/android/gallery3d/app/MoviePlayer$10;-><init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/BookmarkerInfo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showSystemUi(Z)V
    .locals 2
    .param p1    # Z

    const/16 v0, 0x700

    if-nez p1, :cond_0

    or-int/lit8 v0, v0, 0x7

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method

.method private startVideoCareDrm()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideoCareDrm(ZII)V

    return-void
.end method

.method private updateDisplayElement()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isMoviePartialVisible()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->displayTimeBar(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getMoviePlayerExt()Lcom/mediatek/gallery3d/ext/IMoviePlayer;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    return-object v0
.end method

.method public getVideoSurface()Landroid/view/SurfaceView;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    return-object v0
.end method

.method public isFullBuffer()Z
    .locals 2

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isLiveStreaming()Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLiveStreaming() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public isLocalFile()Z
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mStreamingType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->pauseBuffering()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isFullBuffer()Z

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-interface {v1, v0, p2}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->showBuffering(ZI)V

    :cond_0
    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBufferingUpdate("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") pauseBuffering="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->pauseBuffering()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCompletion()V
    .locals 0

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCompletion() mCanReplay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mCanReplay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getError()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MoviePlayer"

    const-string v1, "error occured, exit the video player!"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->getLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->onReplay()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/gallery3d/app/MoviePlayer$TState;->COMPELTED:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iput-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mCanReplay:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showEnded()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->onCompletion()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mAudioBecomingNoisyReceiver:Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$AudioBecomingNoisyReceiver;->unregister()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->access$1500(Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onError("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setError()V

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mProgressChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showErrorMessage(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHidden()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MoviePlayer"

    const-string v1, "onHidden"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mShowing:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isMoviePartialVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/MoviePlayer;->showSystemUi(Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->addBackground()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/MoviePlayer;->showSystemUi(Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->removeBackground()V

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onInfo() what:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " extra:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v2, p1, p2, p3}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->onInfo(Landroid/media/MediaPlayer;II)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x35c

    if-ne p2, v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    const v3, 0x7f0c015c

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iput-boolean v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-static {p1}, Lcom/android/gallery3d/app/MoviePlayer;->isMediaKey(I)Z

    move-result v0

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :cond_1
    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->canPause()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->playVideo()V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->canPause()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->playVideo()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x57 -> :sswitch_0
        0x58 -> :sswitch_0
        0x7e -> :sswitch_3
        0x7f -> :sswitch_2
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-static {p1}, Lcom/android/gallery3d/app/MoviePlayer;->isMediaKey(I)Z

    move-result v0

    return v0
.end method

.method public onPause()Z
    .locals 4

    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause() isLiveStreaming()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLiveStreaming()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLiveStreaming()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const-string v1, "MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause() , return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->doOnPause()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPlayPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->playVideo()V

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;

    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPrepared("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MoviePlayer;->getVideoInfo(Landroid/media/MediaPlayer;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLocalFile()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isLiveStreaming()Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->setPlayingInfo(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canSeekBackward()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canSeekForward()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-interface {v2, v0}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->setCanPause(Z)V

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    invoke-interface {v2, v1}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->setCanScrubbing(Z)V

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->isTargetPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->start()V

    :cond_1
    const-string v2, "MoviePlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPrepared() canPause="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", canSeek="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActivityContext:Lcom/android/gallery3d/app/MovieActivity;

    iget-object v3, p0, Lcom/android/gallery3d/app/MoviePlayer;->mMovieItem:Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/app/MovieActivity;->refreshMovieInfo(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onReplay()V
    .locals 2

    const-string v0, "MoviePlayer"

    const-string v1, "onReplay()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mFirstBePlayed:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->handleOnReplay()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->startVideoCareDrm()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->dump()V

    iput-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDelayVideoRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDelayVideoRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mServerTimeoutExt:Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$ServerTimeoutExtension;->handleOnResume()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/gallery3d/app/MoviePlayer$14;->$SwitchMap$com$android$gallery3d$app$MoviePlayer$TState:[I

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mTState:Lcom/android/gallery3d/app/MoviePlayer$TState;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mConsumedDrmRight:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-direct {p0, v5, v0, v1}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideo(ZII)V

    :goto_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideoMoreThanThreeMinutes()V

    :goto_2
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->dump()V

    iput-boolean v4, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mProgressChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mRetryExt:Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->showRetry()V

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mPlayerExt:Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer$MoviePlayerExtension;->stopVideo()V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mController:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->showEnded()V

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoCanSeek:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canSeekForward()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setDuration(I)V

    goto :goto_2

    :pswitch_3
    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-direct {p0, v5, v0, v1, v4}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideo(ZIIZ)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->pauseVideo()V

    goto :goto_2

    :cond_4
    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoLastDuration:I

    invoke-direct {p0, v5, v0, v1}, Lcom/android/gallery3d/app/MoviePlayer;->doStartVideoCareDrm(ZII)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video-position"

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "resumeable-timeout"

    iget-wide v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mResumeableTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MoviePlayer;->onSaveInstanceStateMore(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSeekEnd(I)V
    .locals 3
    .param p1    # I

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekEnd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mDragging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    return-void
.end method

.method public onSeekMove(I)V
    .locals 3
    .param p1    # I

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekMove("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mDragging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public onSeekStart()V
    .locals 3

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekStart() mDragging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mDragging:Z

    return-void
.end method

.method public onShown()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mShowing:Z

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->isMoviePartialVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer;->onHidden()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "MoviePlayer"

    const-string v1, "onShown"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->addBackground()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->setProgress()I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/MoviePlayer;->showSystemUi(Z)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop() mHasPaused="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mHasPaused:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/MoviePlayer;->doOnPause()V

    :cond_0
    return-void
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    :goto_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer;->mOverlayExt:Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    iget-boolean v1, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    invoke-interface {v0, v1, v2}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->setBottomPanel(ZZ)V

    const-string v0, "MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVideoSizeChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mIsOnlyAudio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/android/gallery3d/app/MoviePlayer;->mIsOnlyAudio:Z

    goto :goto_0
.end method
