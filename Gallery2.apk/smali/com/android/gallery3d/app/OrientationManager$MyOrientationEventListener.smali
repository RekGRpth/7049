.class Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "OrientationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/OrientationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOrientationEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/gallery3d/app/OrientationManager;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/app/OrientationManager;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;->this$0:Lcom/android/gallery3d/app/OrientationManager;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;->this$0:Lcom/android/gallery3d/app/OrientationManager;

    iget-object v1, p0, Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;->this$0:Lcom/android/gallery3d/app/OrientationManager;

    invoke-static {v1}, Lcom/android/gallery3d/app/OrientationManager;->access$000(Lcom/android/gallery3d/app/OrientationManager;)I

    move-result v1

    invoke-static {p1, v1}, Lcom/android/gallery3d/app/OrientationManager;->access$100(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/OrientationManager;->access$002(Lcom/android/gallery3d/app/OrientationManager;I)I

    iget-object v0, p0, Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;->this$0:Lcom/android/gallery3d/app/OrientationManager;

    invoke-static {v0}, Lcom/android/gallery3d/app/OrientationManager;->access$200(Lcom/android/gallery3d/app/OrientationManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/OrientationManager$MyOrientationEventListener;->this$0:Lcom/android/gallery3d/app/OrientationManager;

    invoke-static {v0}, Lcom/android/gallery3d/app/OrientationManager;->access$300(Lcom/android/gallery3d/app/OrientationManager;)V

    goto :goto_0
.end method
