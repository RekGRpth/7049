.class public Lcom/android/gallery3d/app/PhotoDataAdapter;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/android/gallery3d/app/PhotoPage$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$UpdateContent;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;
    }
.end annotation


# static fields
.field private static final BIT_FULL_IMAGE:I = 0x2

.field private static final BIT_GIF_ANIMATION:I = -0x80000000

.field private static final BIT_SCREEN_NAIL:I = 0x1

.field private static final BIT_SECOND_SCREEN_NAIL:I = 0x20000000

.field private static final BIT_STEREO_FULL_IMAGE:I = 0x40000000

.field private static final DATA_CACHE_SIZE:I = 0x20

.field private static final IMAGE_CACHE_SIZE:I = 0x7

.field private static final IS_STEREO_CONVERGENCE_SUPPORTED:Z

.field private static final IS_STEREO_DISPLAY_SUPPORTED:Z

.field private static final MIN_LOAD_COUNT:I = 0x8

.field private static final MSG_LOAD_FINISH:I = 0x2

.field private static final MSG_LOAD_START:I = 0x1

.field private static final MSG_RUN_OBJECT:I = 0x3

.field private static final MSG_UPDATE_IMAGE_REQUESTS:I = 0x4

.field private static final SCREEN_NAIL_MAX:I = 0x3

.field private static final STEREO_FULL_SHIFT:I = 0x0

.field private static final STEREO_THUMB_SHIFT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PhotoDataAdapter"

.field private static final mIsDrmSupported:Z

.field private static final mIsGifAnimationSupported:Z

.field private static sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;


# instance fields
.field private mActiveEnd:I

.field private mActiveStart:I

.field private final mActivity:Lcom/android/gallery3d/app/GalleryActivity;

.field private mCameraIndex:I

.field private final mChanges:[J

.field private mConsumedItemPath:Lcom/android/gallery3d/data/Path;

.field private mContentEnd:I

.field private mContentStart:I

.field private mCurrentIndex:I

.field private final mData:[Lcom/android/gallery3d/data/MediaItem;

.field private mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

.field private mFocusHintDirection:I

.field private mFocusHintPath:Lcom/android/gallery3d/data/Path;

.field private mImageCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mIsActive:Z

.field private mIsPanorama:Z

.field private mItemPath:Lcom/android/gallery3d/data/Path;

.field private final mMainHandler:Landroid/os/Handler;

.field private mNeedFullImage:Z

.field private final mPaths:[Lcom/android/gallery3d/data/Path;

.field private final mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

.field private final mReDecodeToImproveImageQuality:Z

.field private mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

.field private mSize:I

.field private final mSource:Lcom/android/gallery3d/data/MediaSet;

.field private final mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

.field private mSourceVersion:J

.field private final mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

.field private final mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

.field private mTimeIntervalDRM:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/high16 v12, 0x40000000

    const/high16 v11, 0x20000000

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifAnimationSupported()Z

    move-result v8

    sput-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isDrmSupported()Z

    move-result v8

    sput-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v8

    sput-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoConvergenceSupported()Z

    move-result v8

    sput-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_CONVERGENCE_SUPPORTED:Z

    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v8, :cond_0

    move v1, v6

    :goto_0
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-eqz v8, :cond_1

    move v0, v6

    :goto_1
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_2

    const/4 v5, 0x4

    :goto_2
    const/4 v3, 0x0

    add-int/lit8 v8, v1, 0x10

    add-int/lit8 v8, v8, -0x2

    add-int/2addr v8, v5

    new-array v8, v8, [Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    sput-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v9, v7, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v3

    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_8

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v9, v7, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v4

    :goto_3
    const/4 v2, 0x1

    move v4, v3

    :goto_4
    const/4 v8, 0x7

    if-ge v2, v8, :cond_3

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v9, v2, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v4

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v10, v2

    invoke-direct {v9, v10, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_0
    move v1, v7

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_1

    :cond_2
    move v5, v7

    goto :goto_2

    :cond_3
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_4

    const/4 v2, 0x1

    :goto_5
    if-gt v2, v6, :cond_4

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v9, v2, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v4

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v10, v2

    invoke-direct {v9, v10, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v8, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_4
    move v3, v4

    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v6, :cond_6

    sget-object v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v8, v7, v12}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v6, v3

    const/4 v2, 0x1

    :goto_6
    if-gtz v2, :cond_5

    sget-object v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v8, v2, v12}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v6, v4

    sget-object v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v9, v2

    invoke-direct {v8, v9, v12}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v6, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_5
    move v3, v4

    :cond_6
    sget-object v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/4 v9, 0x2

    invoke-direct {v8, v7, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v6, v3

    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v6, :cond_7

    sget-object v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/high16 v9, -0x80000000

    invoke-direct {v8, v7, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v6, v4

    :goto_7
    return-void

    :cond_7
    move v3, v4

    goto :goto_7

    :cond_8
    move v3, v4

    goto/16 :goto_3
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/GalleryActivity;Lcom/android/gallery3d/ui/PhotoView;Lcom/android/gallery3d/data/MediaSet;Lcom/android/gallery3d/data/Path;IIZ)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/app/GalleryActivity;
    .param p2    # Lcom/android/gallery3d/ui/PhotoView;
    .param p3    # Lcom/android/gallery3d/data/MediaSet;
    .param p4    # Lcom/android/gallery3d/data/Path;
    .param p5    # I
    .param p6    # I
    .param p7    # Z

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x7

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReDecodeToImproveImageQuality:Z

    new-instance v0, Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-direct {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    const/16 v0, 0x20

    new-array v0, v0, [Lcom/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    new-array v0, v3, [J

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    new-array v0, v3, [Lcom/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    iput-wide v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    iput v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    iput-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-direct {v0, p0, v4}, Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-static {p4}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iput p5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iput p6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    iput-boolean p7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    invoke-interface {p1}, Lcom/android/gallery3d/app/GalleryActivity;->getThreadPool()Lcom/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    invoke-static {v0, v5, v6}, Ljava/util/Arrays;->fill([JJ)V

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$1;

    invoke-interface {p1}, Lcom/android/gallery3d/app/GalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$1;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return v0
.end method

.method static synthetic access$1302(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return p1
.end method

.method static synthetic access$1400(Lcom/android/gallery3d/app/PhotoDataAdapter;)[Lcom/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$1600(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/Path;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/gallery3d/app/PhotoDataAdapter;)J
    .locals 2
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-wide v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/android/gallery3d/app/PhotoDataAdapter;J)J
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$1900(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return v0
.end method

.method static synthetic access$1902(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return v0
.end method

.method static synthetic access$2002(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$2100(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    return-void
.end method

.method static synthetic access$2200()Z
    .locals 1

    sget-boolean v0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->restoreDrmConsumeStatus()V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    return-void
.end method

.method static synthetic access$2800(Lcom/android/gallery3d/app/PhotoDataAdapter;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Ljava/util/concurrent/Callable;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/Path;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    return v0
.end method

.method static synthetic access$3200(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateOriginScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3300(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSecondScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateStereoFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateGifDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;
    .param p2    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->recordScreenNailForStereo(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method private executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private fireDataChange()V
    .locals 15

    const v10, 0x7fffffff

    const/4 v14, 0x0

    const/4 v13, 0x7

    const/4 v1, 0x0

    const/4 v3, -0x3

    :goto_0
    const/4 v9, 0x3

    if-gt v3, v9, :cond_1

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v9, v3

    invoke-direct {p0, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getVersion(I)J

    move-result-wide v5

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aget-wide v11, v9, v11

    cmp-long v9, v11, v5

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aput-wide v5, v9, v11

    const/4 v1, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-nez v1, :cond_3

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x7

    new-array v2, v13, [I

    new-array v7, v13, [Lcom/android/gallery3d/data/Path;

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    invoke-static {v9, v14, v7, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v13, :cond_4

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    iget v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v11, v3

    add-int/lit8 v11, v11, -0x3

    invoke-direct {p0, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v11

    aput-object v11, v9, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v13, :cond_9

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    aget-object v8, v9, v3

    if-nez v8, :cond_5

    aput v10, v2, v3

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v13, :cond_6

    aget-object v9, v7, v4

    if-ne v9, v8, :cond_7

    :cond_6
    if-ge v4, v13, :cond_8

    add-int/lit8 v9, v4, -0x3

    :goto_6
    aput v9, v2, v3

    goto :goto_4

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    move v9, v10

    goto :goto_6

    :cond_9
    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget v10, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    neg-int v10, v10

    iget v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int/2addr v11, v12

    invoke-virtual {v9, v2, v10, v11}, Lcom/android/gallery3d/ui/PhotoView;->notifyDataChange([III)V

    invoke-virtual {p0, v14}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "PhotoDataAdapter"

    const-string v10, "fireDataChange: enterCameraPreview"

    invoke-static {v9, v10}, Lcom/android/gallery3d/app/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v9}, Lcom/android/gallery3d/ui/PhotoView;->enterCameraPreview()V

    goto :goto_1
.end method

.method private getItem(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v0, :cond_2

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v1, p1, 0x20

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-lt p1, v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v1, p1, 0x20

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private getPath(I)Lcom/android/gallery3d/data/Path;
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method private getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0
.end method

.method private getVersion(I)J
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v1

    goto :goto_0
.end method

.method private isTemporaryItem(Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v2, p1, Lcom/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    sget v3, Lcom/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getSize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 3
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-static {p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    new-instance v1, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v1, v2, v0}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(II)V

    goto :goto_0
.end method

.method private recordScreenNailForStereo(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/MediaItem;
    .param p2    # Landroid/graphics/Bitmap;

    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v1

    const/high16 v2, 0x10000

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    const/high16 v2, 0x80000

    and-int/2addr v2, v1

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-nez v2, :cond_2

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;-><init>()V

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    :cond_2
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iput-object p2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private resetDrmConsumeStatus()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method private restoreDrmConsumeStatus()V
    .locals 4

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTimeIntervalDRM:Z

    if-eqz v2, :cond_2

    const-string v2, "PhotoDataAdapter"

    const-string v3, "restoreDrmConsumeStatus:for time interval media..."

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v3, v3, 0x20

    aget-object v1, v2, v3

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->hasDrmRights()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "PhotoDataAdapter"

    const-string v3, "restoreDrmConsumeStatus:we have no rights "

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v2, v3, :cond_3

    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->enterConsumeMode()V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    goto :goto_0
.end method

.method private saveDrmConsumeStatus()V
    .locals 4

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v3, v3, 0x20

    aget-object v0, v2, v3

    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_0
.end method

.method private startGifAnimation(Lcom/android/gallery3d/data/Path;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/data/Path;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getTotalFrameCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getWidth()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getHeight()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getWidth()I

    move-result v3

    int-to-long v3, v3

    iget-object v5, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v5}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getHeight()I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v3, v5

    const-wide/16 v5, 0x4

    mul-long/2addr v3, v5

    const-wide v5, 0x100000000L

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3

    :cond_2
    const-string v3, "PhotoDataAdapter"

    const-string v4, "startGifAnimation:illegal gif frame dimension"

    invoke-static {v3, v4}, Lcom/android/gallery3d/app/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v3, :cond_0

    new-instance v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v3, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput v0, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->animatedIndex:I

    iput-object v1, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;

    invoke-direct {v6, p0, p1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private startTaskIfNeeded(II)Lcom/android/gallery3d/util/Future;
    .locals 13
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation

    const/high16 v12, 0x10000

    const/high16 v11, -0x80000000

    const/4 v7, 0x0

    const/4 v10, 0x2

    const/4 v6, 0x1

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v5, :cond_0

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p1, v5, :cond_1

    :cond_0
    move-object v5, v7

    :goto_0
    return-object v5

    :cond_1
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_2

    move-object v5, v7

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v8, p1, 0x20

    aget-object v1, v5, v8

    if-eqz v1, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v3

    if-ne p2, v6, :cond_4

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_4

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v5, v8, v3

    if-nez v5, :cond_4

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    if-ne p2, v10, :cond_5

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_5

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v5, v8, v3

    if-nez v5, :cond_5

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_5
    if-ne p2, v11, :cond_6

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_6

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifSupported()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    cmp-long v5, v8, v3

    if-nez v5, :cond_6

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_6
    const/high16 v5, 0x20000000

    if-ne p2, v5, :cond_7

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_7

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    cmp-long v5, v8, v3

    if-nez v5, :cond_7

    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v5, :cond_7

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_7
    const/high16 v5, 0x40000000

    if-ne p2, v5, :cond_8

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_8

    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v5, :cond_8

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_8
    if-ne p2, v10, :cond_9

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_9

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    cmp-long v5, v8, v3

    if-nez v5, :cond_9

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_9
    if-ne p2, v6, :cond_a

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_a

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;

    invoke-direct {v6, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_a
    if-ne p2, v10, :cond_b

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_b

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    and-int/lit8 v5, v5, 0x40

    if-eqz v5, :cond_b

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;

    invoke-direct {v6, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_b
    if-ne p2, v10, :cond_c

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_c

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    and-int/lit8 v5, v5, 0x40

    if-nez v5, :cond_c

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    const/16 v5, 0x3c0

    iput v5, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v2, v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;

    invoke-direct {v6, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;-><init>(Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_c
    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v5, :cond_d

    if-ne p2, v11, :cond_d

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_d

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    and-int/lit16 v5, v5, 0x1000

    if-eqz v5, :cond_d

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inGifDecoder:Z

    const-string v5, "PhotoDataAdapter"

    const-string v7, "startTaskIfNeeded:start GifDecoder task"

    invoke-static {v5, v7}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {v1, v6, v2}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v6

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_d
    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v5, :cond_10

    const/high16 v5, 0x20000000

    if-ne p2, v5, :cond_f

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_f

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    and-int/2addr v5, v12

    if-eqz v5, :cond_f

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v5

    iput v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    const/high16 v7, 0x80000

    and-int/2addr v5, v7

    if-nez v5, :cond_e

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inStereoConvergence:Z

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iput-object v5, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inInputDataBundle:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    :cond_e
    invoke-static {v2, v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;

    invoke-direct {v6, p0, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_f
    const/high16 v5, 0x40000000

    if-ne p2, v5, :cond_10

    iget-wide v8, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    cmp-long v5, v8, v3

    if-eqz v5, :cond_10

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    and-int/2addr v5, v12

    if-eqz v5, :cond_10

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    const/high16 v8, 0x80000

    and-int/2addr v5, v8

    if-nez v5, :cond_10

    const-string v5, "PhotoDataAdapter"

    const-string v7, "create stereo full image task..."

    invoke-static {v5, v7}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v5

    iput v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFullFrame:Z

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    iput v5, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    invoke-static {v2, v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageJob;

    invoke-direct {v6, p0, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_10
    move-object v5, v7

    goto/16 :goto_0
.end method

.method private updateCurrentIndex(I)V
    .locals 5
    .param p1    # I

    const-string v2, "PhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCurrentIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-ne v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v3, p1, 0x20

    aget-object v0, v2, v3

    if-nez v0, :cond_3

    const/4 v2, 0x0

    :goto_1
    iput-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    invoke-interface {v2, p1, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoChanged(ILcom/android/gallery3d/data/Path;)V

    :cond_2
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/PhotoView;->enterCameraPreview()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_1

    :cond_4
    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v2, v1, v2

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/PhotoView;->leaveCameraPreview()V

    goto :goto_0
.end method

.method private updateDrmScreenNail()V
    .locals 9

    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    :goto_0
    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v5, :cond_0

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-nez v3, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v4

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v2, v1, v5

    const/4 v5, -0x3

    if-lt v2, v5, :cond_2

    const/4 v5, 0x3

    if-gt v2, v5, :cond_2

    if-eqz v2, :cond_4

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    :cond_4
    invoke-direct {p0, v4, v3, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v6, v2, 0x3

    const-wide/16 v7, -0x1

    aput-wide v7, v5, v6

    goto :goto_1
.end method

.method private updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;I)Z
    .locals 4
    .param p1    # Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-direct {p0, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v3

    goto :goto_0
.end method

.method private updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z
    .locals 6
    .param p1    # Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
    .param p2    # Lcom/android/gallery3d/data/MediaItem;
    .param p3    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-virtual {p2}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v1

    const/16 v0, 0xc

    invoke-virtual {p1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v3, :cond_0

    and-int v3, v2, v0

    if-nez v3, :cond_1

    :cond_0
    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-nez v3, :cond_2

    and-int v3, v2, v0

    if-nez v3, :cond_2

    :cond_1
    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v3, :cond_4

    and-int/lit8 v2, v2, -0xd

    invoke-virtual {p1, v2}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    :goto_1
    move v5, v4

    :cond_2
    return v5

    :cond_3
    move v3, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    goto :goto_1
.end method

.method private updateFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/BitmapRegionDecoder;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/BitmapRegionDecoder;

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-ne p1, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateGifDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const-string v2, "PhotoDataAdapter"

    const-string v3, ">> updateGifDecoder"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_1

    :cond_0
    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateGifDecoder:invalid entry or future"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/drm/DrmHelper;->showDrmMicroThumb(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateGifDecoder:no animation for non-consume drm"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    const-string v2, "PhotoDataAdapter"

    const-string v3, "<< updateGifDecoder"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startGifAnimation(Lcom/android/gallery3d/data/Path;)V

    goto :goto_1
.end method

.method private updateImageCache()V
    .locals 15

    const/4 v14, 0x1

    const-wide/16 v12, -0x1

    const/4 v11, 0x0

    new-instance v6, Ljava/util/HashSet;

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    :goto_0
    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v7, :cond_12

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v8, v1, 0x20

    aget-object v3, v7, v8

    if-nez v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v4

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_11

    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v7, v14, :cond_3

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_2

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    :cond_2
    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    iput-wide v12, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    :cond_3
    iget-wide v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    instance-of v7, v7, Lcom/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v7, :cond_4

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    check-cast v5, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Lcom/android/gallery3d/ui/BitmapScreenNail;->updatePlaceholderSize(II)V

    :cond_4
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v7, :cond_6

    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-lez v7, :cond_6

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_5
    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-wide v12, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_6

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_6
    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-lez v7, :cond_9

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_7

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    :cond_7
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_8

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_8
    iput-wide v12, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    :cond_9
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v7, :cond_e

    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v7, v14, :cond_e

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_a

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    :cond_a
    iput-wide v12, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_b

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_b
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_c

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_c
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    if-eqz v7, :cond_d

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    :cond_d
    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    :cond_e
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-lez v7, :cond_0

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_f

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    :cond_f
    iput-wide v12, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v7, :cond_10

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    :cond_10
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    goto/16 :goto_1

    :cond_11
    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-direct {v0, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_12
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/gallery3d/data/Path;

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_14

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_14
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_15

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_15
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_16

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_16
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v7, :cond_18

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_17

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_17
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_18

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_18
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_19

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_19

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_19
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v7, :cond_13

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_1a

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_1a
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_1b

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1b
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_1c

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1c
    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v7, :cond_1d

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    :cond_1d
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v7, :cond_1e

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    :cond_1e
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v7, :cond_1f

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    :cond_1f
    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    if-eqz v7, :cond_13

    iput-object v11, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    goto/16 :goto_2

    :cond_20
    return-void
.end method

.method private updateImageRequests()V
    .locals 13

    const-wide/16 v11, -0x1

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v8, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v9, v1, 0x20

    aget-object v5, v8, v9

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v8

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v8, v9, :cond_0

    const/4 v7, 0x0

    const/4 v3, 0x0

    :goto_0
    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    array-length v8, v8

    if-ge v3, v8, :cond_4

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v6, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->indexOffset:I

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v0, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->imageBit:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_3

    iget-boolean v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    if-nez v8, :cond_3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    add-int v8, v1, v6

    invoke-direct {p0, v8, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startTaskIfNeeded(II)Lcom/android/gallery3d/util/Future;

    move-result-object v7

    if-eqz v7, :cond_2

    :cond_4
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_6

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_6

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    :cond_6
    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_7

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_7

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    :cond_7
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v8, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    :cond_8
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    :cond_9
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_a

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_a

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_a

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    :cond_a
    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_5

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_5

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    goto :goto_1
.end method

.method private updateOriginScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v1, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/ui/ScreenNail;

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-ne p1, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v5, p2, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v5, 0x0

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-nez v4, :cond_6

    const/4 v2, 0x1

    :goto_1
    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    instance-of v5, v5, Lcom/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v5, :cond_3

    iget-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    check-cast v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;->combine(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v4

    :cond_3
    if-nez v4, :cond_7

    iput-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    :goto_2
    const/4 v1, -0x3

    :goto_3
    const/4 v5, 0x3

    if-gt v1, v5, :cond_5

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v5, v1

    invoke-direct {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v5

    if-ne p1, v5, :cond_8

    if-nez v1, :cond_4

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    :cond_4
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5, v1}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_5
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :cond_7
    iput-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private updateScreenNailSize(Lcom/android/gallery3d/ui/ScreenNail;Lcom/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;
    .param p2    # Lcom/android/gallery3d/data/MediaItem;

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getImageOptions()Lcom/mediatek/gallery3d/ext/IImageOptions;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IImageOptions;->shouldUseOriginalSize()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method private updateSecondScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v4, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v0, :cond_4

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_5

    :goto_1
    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    new-instance v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    sget-boolean v3, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_CONVERGENCE_SUPPORTED:Z

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    :cond_3
    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v3, :cond_4

    const-string v3, "PhotoDataAdapter"

    const-string v4, "updateSecondScreenNail:update tileProvider"

    invoke-static {v3, v4}, Lcom/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v5}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v4, v4, 0x20

    aget-object v2, v3, v4

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/ui/PhotoView;->setStereoLayout(I)V

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v3

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v6, v6}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(ZZ)V

    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0

    :cond_5
    new-instance v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_6
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v5, v5}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(ZZ)V

    goto :goto_2
.end method

.method private updateSlidingWindow()V
    .locals 6

    const/4 v5, 0x0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v4, -0x7

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v2, 0x7

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-ne v3, v2, :cond_1

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ne v3, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    iput v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x10

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v4, -0x20

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v2, 0x20

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-gt v3, v4, :cond_2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt v3, v4, :cond_2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x8

    if-le v3, v4, :cond_0

    :cond_2
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    :goto_1
    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v1, v3, :cond_5

    if-lt v1, v2, :cond_3

    if-lt v1, v0, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v4, v1, 0x20

    const/4 v5, 0x0

    aput-object v5, v3, v4

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iput v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method private updateStereoFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 5
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateStereoFullImage()"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_2

    :cond_0
    const-string v2, "PhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateStereoFullImage:wrong task:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-nez v0, :cond_3

    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateStereoFullImage:got null bundle()"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0

    :cond_3
    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    :cond_4
    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/data/RegionDecoder;->release()V

    :cond_5
    iget-object v2, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    iput-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    iget-object v2, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    iput-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    const-string v2, "PhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateStereoFullImage:entry.secondFullImage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v2, :cond_6

    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateStereoFullImage:got second full image"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-ne p1, v2, :cond_6

    const-string v2, "PhotoDataAdapter"

    const-string v3, "updateStereoFullImage:update tileProvider"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_6
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateTileProvider()V
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    goto :goto_0
.end method

.method private updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v2, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProviderEx(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "PhotoDataAdapter"

    const-string v5, "updateTileProvider:we return!"

    invoke-static {v4, v5}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v6

    invoke-virtual {v4, v2, v5, v6}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;)V

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v1

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4, v2, v3, v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    goto :goto_0
.end method

.method private updateTileProviderEx(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z
    .locals 14
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    const/4 v11, 0x0

    const/4 v13, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    if-eqz p1, :cond_1

    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v8, :cond_1

    move v8, v9

    :goto_0
    invoke-static {v8}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v8}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getScreenNailSubType(Lcom/android/gallery3d/ui/ScreenNail;)I

    move-result v5

    invoke-static {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->showDrmMicroThumb(I)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    :goto_1
    return v10

    :cond_1
    move v8, v10

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v6, v8}, Lcom/mediatek/gallery3d/util/MediatekFeature;->syncSubType(Lcom/android/gallery3d/ui/ScreenNail;Lcom/android/gallery3d/ui/ScreenNail;)Z

    invoke-static {v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getSizeForSubtype(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/PhotoView$Size;

    move-result-object v4

    if-eqz v4, :cond_6

    iget v7, v4, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iget v3, v4, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_2
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v8, v6, v7, v3}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v12, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v8, v9, v12}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILcom/android/gallery3d/ui/ScreenNail;)V

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v12, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v8, v13, v12}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILcom/android/gallery3d/ui/ScreenNail;)V

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v12, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    invoke-virtual {v8, v12}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoConvergence(Lcom/mediatek/gallery3d/stereo/StereoConvergence;)V

    :cond_3
    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v8, :cond_4

    iget v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iget-object v12, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v12}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v12

    invoke-static {v9, v8, v12}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v2

    iget v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iget-object v12, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v12}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v12

    invoke-static {v10, v8, v12}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v1

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v10, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v8, v10, v6, v2, v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;Lcom/android/gallery3d/ui/ScreenNail;II)V

    :cond_4
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v8, :cond_5

    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-nez v8, :cond_7

    move-object v0, v11

    :goto_3
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v8, v9, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoRegionDecoder(ILandroid/graphics/BitmapRegionDecoder;)V

    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-nez v8, :cond_8

    move-object v0, v11

    :goto_4
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v8, v13, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoRegionDecoder(ILandroid/graphics/BitmapRegionDecoder;)V

    :cond_5
    move v10, v9

    goto :goto_1

    :cond_6
    invoke-interface {v6}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v7

    invoke-interface {v6}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    goto :goto_2

    :cond_7
    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    iget-object v0, v8, Lcom/mediatek/gallery3d/data/RegionDecoder;->regionDecoder:Landroid/graphics/BitmapRegionDecoder;

    goto :goto_3

    :cond_8
    iget-object v8, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    iget-object v0, v8, Lcom/mediatek/gallery3d/data/RegionDecoder;->regionDecoder:Landroid/graphics/BitmapRegionDecoder;

    goto :goto_4
.end method


# virtual methods
.method public enterConsumeMode()V
    .locals 5

    const/4 v4, 0x1

    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_1

    const-string v2, "PhotoDataAdapter"

    const-string v3, "enter is null"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v2, :cond_2

    const-string v2, "PhotoDataAdapter"

    const-string v3, "enterConsumeMode:we already in consumed mode!"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iput-boolean v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->isTimeInterval()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTimeIntervalDRM:Z

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startGifAnimation(Lcom/android/gallery3d/data/Path;)V

    goto :goto_0
.end method

.method public enteredConsumeMode()Z
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    goto :goto_0
.end method

.method public getCurrentIndex()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method public getImageHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getImageHeight()I

    move-result v0

    return v0
.end method

.method public getImageRotation(I)I
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v1

    goto :goto_0
.end method

.method public getImageSize(ILcom/android/gallery3d/ui/PhotoView$Size;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/gallery3d/ui/PhotoView$Size;

    const/4 v2, 0x0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    iput v2, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iput v2, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v1

    iput v1, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v1

    iput v1, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public getImageWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getImageWidth()I

    move-result v0

    return v0
.end method

.method public getLevelCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getLevelCount()I

    move-result v0

    return v0
.end method

.method public getLoadingState(I)I
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v3, p1

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 3
    .param p1    # I

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v0, v1, p1

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit8 v2, v0, 0x20

    aget-object v1, v1, v2

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getScreenNail()Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method public getScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 6
    .param p1    # I

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v1, v4, p1

    if-ltz v1, :cond_0

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge v1, v4, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt v1, v4, :cond_3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-nez v4, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v4

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-nez p1, :cond_2

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    :cond_2
    iget-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getStereoConvergence()Lcom/mediatek/gallery3d/stereo/StereoConvergence;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoConvergence()Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    move-result-object v0

    return-object v0
.end method

.method public getStereoImageHeight(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoImageHeight(I)I

    move-result v0

    return v0
.end method

.method public getStereoImageWidth(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoImageWidth(I)I

    move-result v0

    return v0
.end method

.method public getStereoLevelCount(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoLevelCount(I)I

    move-result v0

    return v0
.end method

.method public getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method public getTile(IIIIILcom/android/gallery3d/data/BitmapPool;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/data/BitmapPool;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getTile(IIIIILcom/android/gallery3d/data/BitmapPool;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getTile(IIIIILcom/android/gallery3d/data/BitmapPool;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/data/BitmapPool;
    .param p7    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getTile(IIIIILcom/android/gallery3d/data/BitmapPool;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public isCamera(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeletable(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMav(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    goto :goto_0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideo(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateCurrentIndex(I)V

    return-void
.end method

.method public pause()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->terminate()V

    iput-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/android/gallery3d/data/ContentListener;)V

    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->saveDrmConsumeStatus()V

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_2
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_3
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_4
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifSupported()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_5
    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_6
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_7
    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->backupData:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    return-void
.end method

.method public resume()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/data/MediaSet;->addContentListener(Lcom/android/gallery3d/data/ContentListener;)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    return-void
.end method

.method public setCurrentPhoto(Lcom/android/gallery3d/data/Path;I)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # I

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iput p2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v1

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public setDataListener(Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    return-void
.end method

.method public setFocusHintPath(Lcom/android/gallery3d/data/Path;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method public setNeedFullImage(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public triggerStereoFullImage()V
    .locals 8

    const/4 v7, 0x1

    const-string v5, "PhotoDataAdapter"

    const-string v6, "trigger stereo full image()"

    invoke-static {v5, v6}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_2

    const-string v5, "PhotoDataAdapter"

    const-string v6, "entry is null"

    invoke-static {v5, v6}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v5, :cond_3

    const-string v5, "PhotoDataAdapter"

    const-string v6, "trigger stereo full image :already started"

    invoke-static {v5, v6}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondFullImage:Lcom/mediatek/gallery3d/data/RegionDecoder;

    if-eqz v5, :cond_4

    const-string v5, "PhotoDataAdapter"

    const-string v6, "trigger stereo full image :already done"

    invoke-static {v5, v6}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v6, v6, 0x20

    aget-object v1, v5, v6

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v3

    iget-wide v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    cmp-long v5, v5, v3

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    const/high16 v6, 0x80000

    and-int/2addr v5, v6

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v5

    const/high16 v6, 0x10000

    and-int/2addr v5, v6

    if-eqz v5, :cond_0

    const-string v5, "PhotoDataAdapter"

    const-string v6, "create stereo full image task for 2d image..."

    invoke-static {v5, v6}, Lcom/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v5

    iput v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iput-wide v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedStereoFullImage:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v7, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    iput-boolean v7, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFullFrame:Z

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    iput v5, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    invoke-static {v2, v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageJob;

    invoke-direct {v6, p0, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageListener;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$StereoFullImageListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoFullImageTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0
.end method
