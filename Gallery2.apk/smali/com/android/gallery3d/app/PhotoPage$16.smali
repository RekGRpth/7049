.class Lcom/android/gallery3d/app/PhotoPage$16;
.super Ljava/lang/Object;
.source "PhotoPage.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/PhotoPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/gallery3d/app/PhotoPage;


# direct methods
.method constructor <init>(Lcom/android/gallery3d/app/PhotoPage;)V
    .locals 0

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConvValueChanged(I)V
    .locals 3
    .param p1    # I

    const-string v0, "PhotoPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "convergence value changed to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/PhotoView;->setConvergenceProgress(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    :cond_0
    return-void
.end method

.method public onEnterConvTuningMode()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0, v3}, Lcom/android/gallery3d/app/PhotoPage;->access$3302(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$1900(Lcom/android/gallery3d/app/PhotoPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$1300(Lcom/android/gallery3d/app/PhotoPage;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/PhotoPage;->disableBarChanges()V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/gallery3d/ui/PhotoView;->enterConvMode(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$3400(Lcom/android/gallery3d/app/PhotoPage;)I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v1

    iget v1, v1, Lcom/android/gallery3d/ui/PhotoView;->mConvergenceProgress:I

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/PhotoPage;->access$3402(Lcom/android/gallery3d/app/PhotoPage;I)I

    const-string v0, "PhotoPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enter conv mode:mStoredProgress="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$3400(Lcom/android/gallery3d/app/PhotoPage;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/PhotoView;->onZoomToSuggestedScale()Z

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0, v3}, Lcom/android/gallery3d/app/PhotoPage;->access$3502(Lcom/android/gallery3d/app/PhotoPage;I)I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v0}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(ZZ)V

    goto :goto_0
.end method

.method public onFirstRunHintDismissed()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/PhotoPage;->access$3302(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/PhotoPage;->access$3602(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1}, Lcom/android/gallery3d/app/PhotoPage;->access$3700(Lcom/android/gallery3d/app/PhotoPage;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    iget-object v1, v1, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1, v0}, Lcom/android/gallery3d/app/PhotoPage;->access$3702(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v1}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/PhotoView;->enterConvMode(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/PhotoPage;->enableBarChanges()V

    return-void
.end method

.method public onFirstRunHintShown()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/PhotoPage$16;->onEnterConvTuningMode()V

    return-void
.end method

.method public onLeaveConvTuningMode(ZI)V
    .locals 6
    .param p1    # Z
    .param p2    # I

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2, v5}, Lcom/android/gallery3d/app/PhotoPage;->access$3302(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2, v5}, Lcom/android/gallery3d/app/PhotoPage;->access$3602(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$3700(Lcom/android/gallery3d/app/PhotoPage;)Z

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    iget-object v2, v2, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2, v1}, Lcom/android/gallery3d/app/PhotoPage;->access$3702(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/gallery3d/ui/PhotoView;->enterConvMode(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-virtual {v2}, Lcom/android/gallery3d/app/PhotoPage;->enableBarChanges()V

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    iget v0, v2, Lcom/android/gallery3d/ui/PhotoView;->mConvergenceProgress:I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/gallery3d/ui/PhotoView;->setStoredProgress(I)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    const-string v2, "PhotoPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLeaveConvTuningMode:saving progress "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    iget-object v2, v2, Lcom/android/gallery3d/app/ActivityState;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v3}, Lcom/android/gallery3d/app/PhotoPage;->access$700(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/app/PhotoPage$Model;

    move-result-object v3

    invoke-interface {v3, v5}, Lcom/android/gallery3d/app/PhotoPage$Model;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->updateConvergence(Landroid/content/Context;ILcom/android/gallery3d/data/MediaItem;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2, v0}, Lcom/android/gallery3d/app/PhotoPage;->access$3402(Lcom/android/gallery3d/app/PhotoPage;I)I

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2, v5}, Lcom/android/gallery3d/app/PhotoPage;->access$3602(Lcom/android/gallery3d/app/PhotoPage;Z)Z

    return-void

    :cond_2
    const-string v2, "PhotoPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLeaveConvTuningMode:reset progress "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v4}, Lcom/android/gallery3d/app/PhotoPage;->access$3400(Lcom/android/gallery3d/app/PhotoPage;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v3}, Lcom/android/gallery3d/app/PhotoPage;->access$3400(Lcom/android/gallery3d/app/PhotoPage;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->setConvergenceProgress(I)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoPage$16;->this$0:Lcom/android/gallery3d/app/PhotoPage;

    invoke-static {v2}, Lcom/android/gallery3d/app/PhotoPage;->access$200(Lcom/android/gallery3d/app/PhotoPage;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0
.end method
