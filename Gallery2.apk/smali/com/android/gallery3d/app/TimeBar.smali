.class public Lcom/android/gallery3d/app/TimeBar;
.super Landroid/view/View;
.source "TimeBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/TimeBar$Listener;
    }
.end annotation


# static fields
.field private static final LOG:Z = true

.field private static final SCRUBBER_PADDING_IN_DP:I = 0xa

.field private static final TAG:Ljava/lang/String; = "Gallery3D/TimeBar"

.field private static final TEXT_SIZE_IN_DP:I = 0xe

.field public static final UNKNOWN:I = -0x1

.field private static final V_PADDING_IN_DP:I = 0x1e


# instance fields
.field private currentTime:I

.field private final listener:Lcom/android/gallery3d/app/TimeBar$Listener;

.field private mEnableScrubbing:Z

.field private mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

.field private mLastShowTime:I

.field private mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

.field private mSecondaryProgressExt:Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;

.field private final playedBar:Landroid/graphics/Rect;

.field private final playedPaint:Landroid/graphics/Paint;

.field private final progressBar:Landroid/graphics/Rect;

.field private final progressPaint:Landroid/graphics/Paint;

.field private final scrubber:Landroid/graphics/Bitmap;

.field private scrubberCorrection:I

.field private scrubberLeft:I

.field private final scrubberPadding:I

.field private scrubberTop:I

.field private scrubbing:Z

.field private showScrubber:Z

.field private showTimes:Z

.field private final timeBounds:Landroid/graphics/Rect;

.field private final timeTextPaint:Landroid/graphics/Paint;

.field private totalTime:I

.field private vPaddingInPx:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/gallery3d/app/TimeBar$Listener;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/gallery3d/app/TimeBar$Listener;

    const/4 v5, -0x1

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v5, p0, Lcom/android/gallery3d/app/TimeBar;->mLastShowTime:I

    new-instance v2, Lcom/android/gallery3d/app/TimeBarSecondaryProgressExtImpl;

    invoke-direct {v2}, Lcom/android/gallery3d/app/TimeBarSecondaryProgressExtImpl;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mSecondaryProgressExt:Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;

    new-instance v2, Lcom/android/gallery3d/app/TimeBarInfoExtImpl;

    invoke-direct {v2}, Lcom/android/gallery3d/app/TimeBarInfoExtImpl;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

    new-instance v2, Lcom/android/gallery3d/app/TimeBarLayoutExtImpl;

    invoke-direct {v2}, Lcom/android/gallery3d/app/TimeBarLayoutExtImpl;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/app/TimeBar$Listener;

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    iput-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showTimes:Z

    iput-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->progressPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->progressPaint:Landroid/graphics/Paint;

    const v3, -0x7f7f80

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->playedPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->playedPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41600000

    mul-float v1, v2, v3

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    const v3, -0x313132

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02015c

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41200000

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41f00000

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    invoke-interface {v2, v3, v4}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->init(II)V

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

    invoke-interface {v2, v1}, Lcom/android/gallery3d/app/ITimeBarInfoExt;->init(F)V

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->mSecondaryProgressExt:Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;

    invoke-interface {v2}, Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;->init()V

    return-void
.end method

.method private clampScrubber()V
    .locals 4

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v0, v3, 0x2

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v0

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v2, v3, v0

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    return-void
.end method

.method private getScrubberTime()I
    .locals 4

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private inScrubber(FF)Z
    .locals 4
    .param p1    # F
    .param p2    # F

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int v1, v2, v3

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberTop:I

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int v0, v2, v3

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v2, p1

    if-gez v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int/2addr v2, v1

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberTop:I

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v2, p2

    if-gez v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 10
    .param p1    # J

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    long-to-int v4, p1

    div-int/lit16 v3, v4, 0x3e8

    rem-int/lit8 v2, v3, 0x3c

    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    div-int/lit16 v0, v3, 0xe10

    if-lez v0, :cond_0

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    const-string v4, "%02d:%02d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private update()V
    .locals 6

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->updateBounds()V

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-interface {v0, p0, v1, v2}, Lcom/android/gallery3d/app/ITimeBarInfoExt;->updateVisibleText(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method private updateBounds()V
    .locals 6

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    if-le v3, v4, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    :goto_0
    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->mLastShowTime:I

    if-ne v3, v2, :cond_1

    :goto_1
    return-void

    :cond_0
    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    goto :goto_0

    :cond_1
    int-to-long v3, v2

    invoke-direct {p0, v3, v4}, Lcom/android/gallery3d/app/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v4, v1, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iput v2, p0, Lcom/android/gallery3d/app/TimeBar;->mLastShowTime:I

    const-string v3, "Gallery3D/TimeBar"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateBounds() durationText="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeBounds="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->mSecondaryProgressExt:Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-interface {v0, p1, v1}, Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->playedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberTop:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->showTimes:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/app/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    invoke-interface {v3}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getTimeOffset()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/app/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    invoke-interface {v3}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getTimeOffset()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-interface {v1, p0, v2}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getInfoBounds(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/android/gallery3d/app/ITimeBarInfoExt;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    return-void
.end method

.method public getBarHeight()I
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    add-int v0, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-interface {v1, v0, v2}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getBarHeight(ILandroid/graphics/Rect;)I

    move-result v1

    return v1
.end method

.method public getPreferredHeight()I
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->vPaddingInPx:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int v0, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    iget-object v2, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-interface {v1, v0, v2}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getPreferredHeight(ILandroid/graphics/Rect;)I

    move-result v1

    return v1
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v5, 0x0

    sub-int v3, p4, p2

    sub-int v0, p5, p3

    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showTimes:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v4, v5, v5, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->update()V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v1, v4, 0x3

    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showTimes:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v1, v4

    :cond_1
    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    invoke-interface {v4, v1}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getProgressMargin(I)I

    move-result v1

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberPadding:I

    add-int/2addr v4, v0

    div-int/lit8 v4, v4, 0x2

    iget-object v5, p0, Lcom/android/gallery3d/app/TimeBar;->mLayoutExt:Lcom/android/gallery3d/app/ITimeBarLayoutExt;

    iget-object v6, p0, Lcom/android/gallery3d/app/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-interface {v5, v6}, Lcom/android/gallery3d/app/ITimeBarLayoutExt;->getProgressOffset(Landroid/graphics/Rect;)I

    move-result v5

    add-int v2, v4, v5

    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubber:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v2, v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberTop:I

    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    sub-int v6, v3, v6

    sub-int/2addr v6, v1

    add-int/lit8 v7, v2, 0x4

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v4, "Gallery3D/TimeBar"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onTouchEvent() showScrubber="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", enableScrubbing="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/gallery3d/app/TimeBar;->mEnableScrubbing:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", totalTime="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", scrubbing="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", event="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->mEnableScrubbing:Z

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v0, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    move v2, v3

    :goto_0
    return v2

    :pswitch_0
    int-to-float v4, v0

    int-to-float v5, v1

    invoke-direct {p0, v4, v5}, Lcom/android/gallery3d/app/TimeBar;->inScrubber(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    iput-boolean v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    sub-int v3, v0, v3

    iput v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberCorrection:I

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    invoke-interface {v3}, Lcom/android/gallery3d/app/TimeBar$Listener;->onScrubbingStart()V

    goto :goto_0

    :pswitch_1
    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    if-eqz v4, :cond_0

    iget v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberCorrection:I

    sub-int v3, v0, v3

    iput v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubberLeft:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->clampScrubber()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->getScrubberTime()I

    move-result v3

    iput v3, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    iget-object v3, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    iget v4, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    invoke-interface {v3, v4}, Lcom/android/gallery3d/app/TimeBar$Listener;->onScrubbingMove(I)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->update()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :pswitch_2
    iget-boolean v4, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->getScrubberTime()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/android/gallery3d/app/TimeBar$Listener;->onScrubbingEnd(I)V

    iput-boolean v3, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->update()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public resetTime()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/android/gallery3d/app/TimeBar;->setTime(II)V

    return-void
.end method

.method public setInfo(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Gallery3D/TimeBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->mInfoExt:Lcom/android/gallery3d/app/ITimeBarInfoExt;

    invoke-interface {v0, p1}, Lcom/android/gallery3d/app/ITimeBarInfoExt;->setInfo(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "Gallery3D/TimeBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setScrubbing("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") scrubbing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/gallery3d/app/TimeBar;->mEnableScrubbing:Z

    iget-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->getScrubberTime()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/gallery3d/app/TimeBar$Listener;->onScrubbingEnd(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    :cond_0
    return-void
.end method

.method public setSecondaryProgress(I)V
    .locals 3
    .param p1    # I

    const-string v0, "Gallery3D/TimeBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSecondaryProgress("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->mSecondaryProgressExt:Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;

    iget-object v1, p0, Lcom/android/gallery3d/app/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-interface {v0, v1, p1}, Lcom/android/gallery3d/app/ITimeBarSecondaryProgressExt;->setSecondaryProgress(Landroid/graphics/Rect;I)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setShowScrubber(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "Gallery3D/TimeBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShowScrubber("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") showScrubber="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/gallery3d/app/TimeBar;->showScrubber:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TimeBar;->listener:Lcom/android/gallery3d/app/TimeBar$Listener;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->getScrubberTime()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/gallery3d/app/TimeBar$Listener;->onScrubbingEnd(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/TimeBar;->scrubbing:Z

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setShowTimes(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/TimeBar;->showTimes:Z

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setTime(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "Gallery3D/TimeBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTime("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    if-ne v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/gallery3d/app/TimeBar;->currentTime:I

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/TimeBar;->totalTime:I

    if-gtz p2, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/app/TimeBar;->setScrubbing(Z)V

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/TimeBar;->update()V

    goto :goto_0
.end method
