.class public Lcom/android/gallery3d/data/Face;
.super Ljava/lang/Object;
.source "Face.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/gallery3d/data/Face;",
        ">;"
    }
.end annotation


# instance fields
.field private mName:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field private mPosition:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/gallery3d/data/Face;)I
    .locals 2
    .param p1    # Lcom/android/gallery3d/data/Face;

    iget-object v0, p0, Lcom/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/gallery3d/data/Face;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/data/Face;->compareTo(Lcom/android/gallery3d/data/Face;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/android/gallery3d/data/Face;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/data/Face;

    iget-object v1, p0, Lcom/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    return-object v0
.end method
