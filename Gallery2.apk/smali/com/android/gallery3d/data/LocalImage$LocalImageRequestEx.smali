.class public Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;
.super Ljava/lang/Object;
.source "LocalImage.java"

# interfaces
.implements Lcom/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/data/LocalImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalImageRequestEx"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
        ">;"
    }
.end annotation


# instance fields
.field private mApplication:Lcom/android/gallery3d/app/GalleryApp;

.field private mLocalFilePath:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mParams:Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

.field private mPath:Lcom/android/gallery3d/data/Path;

.field private mTargetSize:I

.field private mType:I


# direct methods
.method constructor <init>(Lcom/android/gallery3d/app/GalleryApp;Lcom/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/app/GalleryApp;
    .param p2    # Lcom/android/gallery3d/data/Path;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mApplication:Lcom/android/gallery3d/app/GalleryApp;

    iput-object p2, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mPath:Lcom/android/gallery3d/data/Path;

    iput p3, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mType:I

    invoke-static {p3}, Lcom/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mTargetSize:I

    iput-object p4, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mMimeType:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mLocalFilePath:Ljava/lang/String;

    iput-object p6, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mParams:Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    return-void
.end method


# virtual methods
.method public run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 3
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;

    iget-object v0, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mLocalFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mParams:Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "LocalImage"

    const-string v1, "LocalImageRequestEx:got null mLocalFilePath or mParams"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mParams:Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    iget-object v1, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mLocalFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->mMimeType:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/mediatek/gallery3d/data/RequestHelper;->requestDataBundle(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Ljava/lang/String;Ljava/lang/String;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/data/LocalImage$LocalImageRequestEx;->run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    move-result-object v0

    return-object v0
.end method
