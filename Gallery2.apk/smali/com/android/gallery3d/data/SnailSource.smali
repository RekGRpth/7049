.class public Lcom/android/gallery3d/data/SnailSource;
.super Lcom/android/gallery3d/data/MediaSource;
.source "SnailSource.java"


# static fields
.field private static final IS_DRM_SUPPORTED:Z

.field private static final IS_STEREO_DISPLAY_SUPPORTED:Z

.field private static final SNAIL_ALBUM:I = 0x0

.field private static final SNAIL_ITEM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SnailSource"

.field private static sNextId:I


# instance fields
.field private mApplication:Lcom/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/android/gallery3d/data/PathMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isDrmSupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/data/SnailSource;->IS_DRM_SUPPORTED:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/data/SnailSource;->IS_STEREO_DISPLAY_SUPPORTED:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/app/GalleryApp;

    const-string v0, "snail"

    invoke-direct {p0, v0}, Lcom/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/gallery3d/data/SnailSource;->mApplication:Lcom/android/gallery3d/app/GalleryApp;

    new-instance v0, Lcom/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    iget-object v0, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    const-string v1, "/snail/set/*"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    const-string v1, "/snail/item/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    return-void
.end method

.method public static getItemPath(I)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # I

    const-string v0, "/snail/item"

    invoke-static {v0}, Lcom/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/data/Path;->getChild(I)Lcom/android/gallery3d/data/Path;

    move-result-object v0

    return-object v0
.end method

.method public static getSetPath(I)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # I

    const-string v0, "/snail/set"

    invoke-static {v0}, Lcom/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/data/Path;->getChild(I)Lcom/android/gallery3d/data/Path;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized newId()I
    .locals 3

    const-class v1, Lcom/android/gallery3d/data/SnailSource;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/android/gallery3d/data/SnailSource;->sNextId:I

    add-int/lit8 v2, v0, 0x1

    sput v2, Lcom/android/gallery3d/data/SnailSource;->sNextId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public createMediaObject(Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/MediaObject;
    .locals 8
    .param p1    # Lcom/android/gallery3d/data/Path;

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/gallery3d/data/SnailSource;->mApplication:Lcom/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/gallery3d/data/Path;->getMtkInclusion()I

    move-result v4

    iget-object v5, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    invoke-virtual {v5, p1}, Lcom/android/gallery3d/data/PathMatcher;->match(Lcom/android/gallery3d/data/Path;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :pswitch_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/snail/item/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    sget-boolean v5, Lcom/android/gallery3d/data/SnailSource;->IS_DRM_SUPPORTED:Z

    if-nez v5, :cond_0

    sget-boolean v5, Lcom/android/gallery3d/data/SnailSource;->IS_STEREO_DISPLAY_SUPPORTED:Z

    if-eqz v5, :cond_1

    :cond_0
    invoke-virtual {v0, v3, v4}, Lcom/android/gallery3d/data/DataManager;->getMediaObject(Ljava/lang/String;I)Lcom/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/data/MediaItem;

    :goto_1
    new-instance v5, Lcom/android/gallery3d/data/SnailAlbum;

    invoke-direct {v5, p1, v2}, Lcom/android/gallery3d/data/SnailAlbum;-><init>(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, Lcom/android/gallery3d/data/DataManager;->getMediaObject(Ljava/lang/String;)Lcom/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/data/MediaItem;

    goto :goto_1

    :pswitch_1
    iget-object v5, p0, Lcom/android/gallery3d/data/SnailSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v1

    new-instance v5, Lcom/android/gallery3d/data/SnailItem;

    invoke-direct {v5, p1}, Lcom/android/gallery3d/data/SnailItem;-><init>(Lcom/android/gallery3d/data/Path;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
