.class public Lcom/android/gallery3d/gadget/WidgetTypeChooser;
.super Landroid/app/Activity;
.source "WidgetTypeChooser.java"


# instance fields
.field private mListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;-><init>(Lcom/android/gallery3d/gadget/WidgetTypeChooser;)V

    iput-object v0, p0, Lcom/android/gallery3d/gadget/WidgetTypeChooser;->mListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f0c0243

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setTitle(I)V

    const v2, 0x7f04000e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b001b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/android/gallery3d/gadget/WidgetTypeChooser;->mListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    const v2, 0x7f0b001f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/android/gallery3d/gadget/WidgetTypeChooser$2;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/gadget/WidgetTypeChooser$2;-><init>(Lcom/android/gallery3d/gadget/WidgetTypeChooser;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
