.class public Lcom/android/gallery3d/photoeditor/ActionBar;
.super Lcom/android/gallery3d/photoeditor/RestorableView;
.source "ActionBar.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/RestorableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private showSaveOrShare()V
    .locals 5

    const v4, 0x7f0b00b8

    const v3, 0x7f0b00b5

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v1, 0x1

    :goto_0
    const v3, 0x7f0b00b7

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz v1, :cond_0

    const v3, 0x7f0b00b9

    if-eq v0, v3, :cond_1

    :cond_0
    if-nez v1, :cond_2

    if-ne v0, v4, :cond_2

    :cond_1
    invoke-virtual {v2}, Landroid/widget/ViewAnimator;->showNext()V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canSave()Z
    .locals 1

    const v0, 0x7f0b00b8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected childLayoutId()I
    .locals 1

    const v0, 0x7f040033

    return v0
.end method

.method public clickBack()V
    .locals 1

    const v0, 0x7f0b00b2

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/android/gallery3d/photoeditor/RestorableView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/ActionBar;->showSaveOrShare()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/android/gallery3d/photoeditor/RestorableView;->onFinishInflate()V

    invoke-virtual {p0, v0, v0}, Lcom/android/gallery3d/photoeditor/ActionBar;->updateButtons(ZZ)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    sub-int v2, p4, p2

    if-le v1, v2, :cond_1

    const/4 v2, 0x4

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public updateButtons(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    const v0, 0x7f0b00b5

    invoke-virtual {p0, v0, p1}, Lcom/android/gallery3d/photoeditor/RestorableView;->setViewEnabled(IZ)V

    const v0, 0x7f0b00b6

    invoke-virtual {p0, v0, p2}, Lcom/android/gallery3d/photoeditor/RestorableView;->setViewEnabled(IZ)V

    const v0, 0x7f0b00b8

    invoke-virtual {p0, v0, p1}, Lcom/android/gallery3d/photoeditor/RestorableView;->setViewEnabled(IZ)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/ActionBar;->showSaveOrShare()V

    return-void
.end method

.method public updateSave(Z)V
    .locals 1
    .param p1    # Z

    const v0, 0x7f0b00b8

    invoke-virtual {p0, v0, p1}, Lcom/android/gallery3d/photoeditor/RestorableView;->setViewEnabled(IZ)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/ActionBar;->showSaveOrShare()V

    return-void
.end method
