.class public Lcom/android/gallery3d/photoeditor/EffectsMenu;
.super Lcom/android/gallery3d/photoeditor/RestorableView;
.source "EffectsMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/RestorableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private setToggleRunnable(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;II)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;
    .param p2    # I
    .param p3    # I

    new-instance v0, Lcom/android/gallery3d/photoeditor/EffectsMenu$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/android/gallery3d/photoeditor/EffectsMenu$1;-><init>(Lcom/android/gallery3d/photoeditor/EffectsMenu;ILcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;I)V

    invoke-virtual {p0, p2, v0}, Lcom/android/gallery3d/photoeditor/RestorableView;->setClickRunnable(ILjava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected childLayoutId()I
    .locals 1

    const v0, 0x7f04003e

    return v0
.end method

.method public clearSelected()V
    .locals 5

    const v3, 0x7f0b00bc

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/gallery3d/photoeditor/RestorableView;->setViewSelected(IZ)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setOnToggleListener(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;

    const v0, 0x7f0b00bd

    const v1, 0x7f04003b

    invoke-direct {p0, p1, v0, v1}, Lcom/android/gallery3d/photoeditor/EffectsMenu;->setToggleRunnable(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;II)V

    const v0, 0x7f0b00be

    const v1, 0x7f040039

    invoke-direct {p0, p1, v0, v1}, Lcom/android/gallery3d/photoeditor/EffectsMenu;->setToggleRunnable(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;II)V

    const v0, 0x7f0b00bf

    const v1, 0x7f04003a

    invoke-direct {p0, p1, v0, v1}, Lcom/android/gallery3d/photoeditor/EffectsMenu;->setToggleRunnable(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;II)V

    const v0, 0x7f0b00c0

    const v1, 0x7f04003c

    invoke-direct {p0, p1, v0, v1}, Lcom/android/gallery3d/photoeditor/EffectsMenu;->setToggleRunnable(Lcom/android/gallery3d/photoeditor/EffectsMenu$OnToggleListener;II)V

    return-void
.end method
