.class Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/photoeditor/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhotoRenderer"
.end annotation


# instance fields
.field flippedHorizontalDegrees:F

.field flippedVerticalDegrees:F

.field photo:Lcom/android/gallery3d/photoeditor/Photo;

.field final photoBounds:Landroid/graphics/RectF;

.field final queue:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

.field rotatedDegrees:F

.field final synthetic this$0:Lcom/android/gallery3d/photoeditor/PhotoView;

.field viewHeight:I

.field viewWidth:I


# direct methods
.method private constructor <init>(Lcom/android/gallery3d/photoeditor/PhotoView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->this$0:Lcom/android/gallery3d/photoeditor/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->queue:Ljava/util/Vector;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photoBounds:Landroid/graphics/RectF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/gallery3d/photoeditor/PhotoView;Lcom/android/gallery3d/photoeditor/PhotoView$1;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/photoeditor/PhotoView;
    .param p2    # Lcom/android/gallery3d/photoeditor/PhotoView$1;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;-><init>(Lcom/android/gallery3d/photoeditor/PhotoView;)V

    return-void
.end method


# virtual methods
.method flipPhoto(FF)V
    .locals 7
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v1}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v2}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewWidth:I

    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewHeight:I

    move v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->setRenderToFlip(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIIIFF)V

    iput p1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedHorizontalDegrees:F

    iput p2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedVerticalDegrees:F

    :cond_0
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->queue:Ljava/util/Vector;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->queue:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->queue:Ljava/util/Vector;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Runnable;

    move-object v1, v0

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->queue:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->this$0:Lcom/android/gallery3d/photoeditor/PhotoView;

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    :cond_2
    invoke-static {}, Lcom/android/gallery3d/photoeditor/RendererUtils;->renderBackground()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v3}, Lcom/android/gallery3d/photoeditor/Photo;->texture()I

    move-result v3

    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewWidth:I

    iget v5, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewHeight:I

    invoke-static {v2, v3, v4, v5}, Lcom/android/gallery3d/photoeditor/RendererUtils;->renderTexture(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;III)V

    :cond_3
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 2
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I
    .param p3    # I

    iput p2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewWidth:I

    iput p3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewHeight:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->updateSurface(ZZ)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-static {}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createProgram()Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    return-void
.end method

.method rotatePhoto(F)V
    .locals 6
    .param p1    # F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v1}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v2}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewWidth:I

    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewHeight:I

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/android/gallery3d/photoeditor/RendererUtils;->setRenderToRotate(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIIIF)V

    iput p1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatedDegrees:F

    :cond_0
    return-void
.end method

.method setPhoto(Lcom/android/gallery3d/photoeditor/Photo;Z)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/photoeditor/Photo;
    .param p2    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v2

    :goto_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v1

    :goto_1
    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photoBounds:Landroid/graphics/RectF;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photoBounds:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    int-to-float v6, v2

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photoBounds:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    int-to-float v6, v1

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photoBounds:Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v7, v2

    int-to-float v8, v1

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {p0, p2, v0}, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->updateSurface(ZZ)V

    return-void

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method updateSurface(ZZ)V
    .locals 8
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedHorizontalDegrees:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_0

    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedVerticalDegrees:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_5

    :cond_0
    move v0, v3

    :goto_0
    iget v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatedDegrees:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_1

    if-eqz v0, :cond_6

    :cond_1
    move v1, v3

    :goto_1
    if-eqz p1, :cond_2

    if-nez v1, :cond_3

    :cond_2
    if-eqz p2, :cond_7

    if-nez v1, :cond_7

    :cond_3
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->renderContext:Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v3}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v3

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->photo:Lcom/android/gallery3d/photoeditor/Photo;

    invoke-virtual {v4}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v4

    iget v5, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewWidth:I

    iget v6, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->viewHeight:I

    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->setRenderToFit(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIII)V

    iput v7, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatedDegrees:F

    iput v7, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedHorizontalDegrees:F

    iput v7, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedVerticalDegrees:F

    :cond_4
    :goto_2
    return-void

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    iget v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatedDegrees:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatedDegrees:F

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->rotatePhoto(F)V

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_4

    iget v2, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedHorizontalDegrees:F

    iget v3, p0, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flippedVerticalDegrees:F

    invoke-virtual {p0, v2, v3}, Lcom/android/gallery3d/photoeditor/PhotoView$PhotoRenderer;->flipPhoto(FF)V

    goto :goto_2
.end method
