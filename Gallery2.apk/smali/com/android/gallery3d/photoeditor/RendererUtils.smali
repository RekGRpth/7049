.class public Lcom/android/gallery3d/photoeditor/RendererUtils;
.super Ljava/lang/Object;
.source "RendererUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    }
.end annotation


# static fields
.field private static final DEGREE_TO_RADIAN:F = 0.017453292f

.field private static final FLOAT_SIZE_BYTES:I = 0x4

.field private static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler, v_texcoord);\n}\n"

.field private static final POS_VERTICES:[F

.field private static final TEX_VERTICES:[F

.field private static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec4 a_position;\nattribute vec2 a_texcoord;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_Position = a_position;\n  v_texcoord = a_texcoord;\n}\n"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x8

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/gallery3d/photoeditor/RendererUtils;->TEX_VERTICES:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/gallery3d/photoeditor/RendererUtils;->POS_VERTICES:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
    .end array-data

    :array_1
    .array-data 4
        -0x40800000
        -0x40800000
        0x3f800000
        -0x40800000
        -0x40800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkGlError(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method public static clearTexture(I)V
    .locals 3
    .param p0    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v0, v1, [I

    aput p0, v0, v2

    array-length v1, v0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    const-string v1, "glDeleteTextures"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    return-void
.end method

.method public static createProgram()Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    .locals 10

    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v6, 0x8b31

    const-string v7, "attribute vec4 a_position;\nattribute vec2 a_texcoord;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_Position = a_position;\n  v_texcoord = a_texcoord;\n}\n"

    invoke-static {v6, v7}, Lcom/android/gallery3d/photoeditor/RendererUtils;->loadShader(ILjava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const v6, 0x8b30

    const-string v7, "precision mediump float;\nuniform sampler2D tex_sampler;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler, v_texcoord);\n}\n"

    invoke-static {v6, v7}, Lcom/android/gallery3d/photoeditor/RendererUtils;->loadShader(ILjava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v6, "glAttachShader"

    invoke-static {v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v6, "glAttachShader"

    invoke-static {v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v2, v9, [I

    const v6, 0x8b82

    invoke-static {v4, v6, v2, v8}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v6, v2, v8

    if-eq v6, v9, :cond_2

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    const/4 v4, 0x0

    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not link program: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    new-instance v0, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;-><init>()V

    const-string v6, "tex_sampler"

    invoke-static {v4, v6}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v6

    invoke-static {v0, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$502(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;I)I

    const-string v6, "a_texcoord"

    invoke-static {v4, v6}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v6

    invoke-static {v0, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$202(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;I)I

    const-string v6, "a_position"

    invoke-static {v4, v6}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v6

    invoke-static {v0, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$402(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;I)I

    sget-object v6, Lcom/android/gallery3d/photoeditor/RendererUtils;->TEX_VERTICES:[F

    invoke-static {v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createVerticesBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$302(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    sget-object v6, Lcom/android/gallery3d/photoeditor/RendererUtils;->POS_VERTICES:[F

    invoke-static {v6}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createVerticesBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$002(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    invoke-static {v0, v4}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$102(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;I)I

    goto/16 :goto_0
.end method

.method public static createTexture()I
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v0, v1, [I

    array-length v1, v0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const-string v1, "glGenTextures"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    aget v1, v0, v2

    return v1
.end method

.method public static createTexture(Landroid/graphics/Bitmap;)I
    .locals 5
    .param p0    # Landroid/graphics/Bitmap;

    const v4, 0x812f

    const/16 v3, 0x2601

    const/4 v1, 0x0

    const/16 v2, 0xde1

    invoke-static {}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createTexture()I

    move-result v0

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    invoke-static {v2, v1, p0, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    const/16 v1, 0x2800

    invoke-static {v2, v1, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2801

    invoke-static {v2, v1, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2802

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const-string v1, "texImage2D"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    return v0
.end method

.method private static createVerticesBuffer([F)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0    # [F

    array-length v1, p0

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Number of vertices should be four."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    array-length v1, p0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    return-object v0
.end method

.method private static getFitVertices(IIII)[F
    .locals 7
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    int-to-float v4, p0

    int-to-float v5, p1

    div-float v2, v4, v5

    int-to-float v4, p2

    int-to-float v5, p3

    div-float v0, v4, v5

    div-float v1, v0, v2

    const/16 v4, 0x8

    new-array v3, v4, [F

    sget-object v4, Lcom/android/gallery3d/photoeditor/RendererUtils;->POS_VERTICES:[F

    array-length v5, v3

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/high16 v4, 0x3f800000

    cmpl-float v4, v1, v4

    if-lez v4, :cond_0

    aget v4, v3, v6

    div-float/2addr v4, v1

    aput v4, v3, v6

    const/4 v4, 0x2

    aget v5, v3, v4

    div-float/2addr v5, v1

    aput v5, v3, v4

    const/4 v4, 0x4

    aget v5, v3, v4

    div-float/2addr v5, v1

    aput v5, v3, v4

    const/4 v4, 0x6

    aget v5, v3, v4

    div-float/2addr v5, v1

    aput v5, v3, v4

    :goto_0
    return-object v3

    :cond_0
    const/4 v4, 0x1

    aget v5, v3, v4

    mul-float/2addr v5, v1

    aput v5, v3, v4

    const/4 v4, 0x3

    aget v5, v3, v4

    mul-float/2addr v5, v1

    aput v5, v3, v4

    const/4 v4, 0x5

    aget v5, v3, v4

    mul-float/2addr v5, v1

    aput v5, v3, v4

    const/4 v4, 0x7

    aget v5, v3, v4

    mul-float/2addr v5, v1

    aput v5, v3, v4

    goto :goto_0
.end method

.method private static loadShader(ILjava/lang/String;)I
    .locals 6
    .param p0    # I
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v2, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    invoke-static {v2}, Landroid/opengl/GLES20;->glCompileShader(I)V

    const/4 v3, 0x1

    new-array v0, v3, [I

    const v3, 0x8b81

    invoke-static {v2, v3, v0, v4}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    aget v3, v0, v4

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not compile shader "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    return v2
.end method

.method public static renderBackground()V
    .locals 2

    const/4 v1, 0x0

    const/high16 v0, 0x3f800000

    invoke-static {v1, v1, v1, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    return-void
.end method

.method public static renderTexture(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;III)V
    .locals 6
    .param p0    # Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v2, 0x1406

    const/4 v1, 0x2

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$100(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string v0, "glUseProgram"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    invoke-static {v3, v3, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    const-string v0, "glViewport"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$200(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$300(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)Ljava/nio/FloatBuffer;

    move-result-object v5

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$200(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$400(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$000(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)Ljava/nio/FloatBuffer;

    move-result-object v5

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$400(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    const-string v0, "vertex attribute setup"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const-string v0, "glActiveTexture"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    const/16 v0, 0xde1

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const-string v0, "glBindTexture"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$500(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;)I

    move-result v0

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    return-void
.end method

.method public static saveTexture(III)Landroid/graphics/Bitmap;
    .locals 11
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const v10, 0x8d40

    const/4 v9, 0x1

    const/4 v0, 0x0

    new-array v8, v9, [I

    invoke-static {v9, v8, v0}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    const-string v1, "glGenFramebuffers"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    aget v1, v8, v0

    invoke-static {v10, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    const-string v1, "glBindFramebuffer"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    const v1, 0x8ce0

    const/16 v2, 0xde1

    invoke-static {v10, v1, v2, p0, v0}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    const-string v1, "glFramebufferTexture2D"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    mul-int v1, p1, p2

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v1, v0

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    const-string v1, "glReadPixels"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    invoke-static {v10, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    const-string v1, "glBindFramebuffer"

    invoke-static {v1}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    invoke-static {v9, v8, v0}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    const-string v0, "glDeleteFramebuffer"

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->checkGlError(Ljava/lang/String;)V

    return-object v7
.end method

.method public static setRenderToFit(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIII)V
    .locals 1
    .param p0    # Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-static {p1, p2, p3, p4}, Lcom/android/gallery3d/photoeditor/RendererUtils;->getFitVertices(IIII)[F

    move-result-object v0

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createVerticesBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$002(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    return-void
.end method

.method public static setRenderToFlip(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIIIFF)V
    .locals 13
    .param p0    # Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # F

    invoke-static/range {p1 .. p4}, Lcom/android/gallery3d/photoeditor/RendererUtils;->getFitVertices(IIII)[F

    move-result-object v1

    move/from16 v0, p5

    float-to-int v10, v0

    div-int/lit16 v3, v10, 0xb4

    rem-int/lit8 v10, v3, 0x2

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget v11, v1, v11

    neg-float v11, v11

    aput v11, v1, v10

    const/4 v10, 0x4

    const/4 v11, 0x0

    aget v11, v1, v11

    aput v11, v1, v10

    const/4 v10, 0x2

    const/4 v11, 0x2

    aget v11, v1, v11

    neg-float v11, v11

    aput v11, v1, v10

    const/4 v10, 0x6

    const/4 v11, 0x2

    aget v11, v1, v11

    aput v11, v1, v10

    :cond_0
    move/from16 v0, p6

    float-to-int v10, v0

    div-int/lit16 v8, v10, 0xb4

    rem-int/lit8 v10, v8, 0x2

    if-eqz v10, :cond_1

    const/4 v10, 0x1

    const/4 v11, 0x1

    aget v11, v1, v11

    neg-float v11, v11

    aput v11, v1, v10

    const/4 v10, 0x3

    const/4 v11, 0x1

    aget v11, v1, v11

    aput v11, v1, v10

    const/4 v10, 0x5

    const/4 v11, 0x5

    aget v11, v1, v11

    neg-float v11, v11

    aput v11, v1, v10

    const/4 v10, 0x7

    const/4 v11, 0x5

    aget v11, v1, v11

    aput v11, v1, v10

    :cond_1
    const/high16 v4, 0x40a00000

    const/16 v10, 0x8

    new-array v9, v10, [F

    const/4 v10, 0x0

    const/4 v11, 0x0

    array-length v12, v9

    invoke-static {v1, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/high16 v10, 0x43340000

    rem-float v10, p5, v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-eqz v10, :cond_2

    mul-int/lit16 v10, v3, 0xb4

    int-to-float v10, v10

    sub-float v10, p5, v10

    const v11, 0x3c8efa35

    mul-float v5, v10, v11

    invoke-static {v5}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    invoke-static {v5}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    const/4 v10, 0x0

    aget v10, v1, v10

    mul-float/2addr v10, v7

    add-float/2addr v10, v4

    div-float v6, v4, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget v11, v1, v11

    mul-float/2addr v11, v2

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x1

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x4

    const/4 v11, 0x0

    aget v11, v9, v11

    aput v11, v9, v10

    const/4 v10, 0x5

    const/4 v11, 0x5

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x2

    aget v10, v1, v10

    mul-float/2addr v10, v7

    add-float/2addr v10, v4

    div-float v6, v4, v10

    const/4 v10, 0x2

    const/4 v11, 0x2

    aget v11, v1, v11

    mul-float/2addr v11, v2

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x3

    const/4 v11, 0x3

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x6

    const/4 v11, 0x2

    aget v11, v9, v11

    aput v11, v9, v10

    const/4 v10, 0x7

    const/4 v11, 0x7

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    :cond_2
    const/high16 v10, 0x43340000

    rem-float v10, p6, v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-eqz v10, :cond_3

    mul-int/lit16 v10, v8, 0xb4

    int-to-float v10, v10

    sub-float v10, p6, v10

    const v11, 0x3c8efa35

    mul-float v5, v10, v11

    invoke-static {v5}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    invoke-static {v5}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    const/4 v10, 0x1

    aget v10, v1, v10

    mul-float/2addr v10, v7

    add-float/2addr v10, v4

    div-float v6, v4, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x1

    aget v11, v1, v11

    mul-float/2addr v11, v2

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x2

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x3

    const/4 v11, 0x1

    aget v11, v9, v11

    aput v11, v9, v10

    const/4 v10, 0x5

    aget v10, v1, v10

    mul-float/2addr v10, v7

    add-float/2addr v10, v4

    div-float v6, v4, v10

    const/4 v10, 0x4

    const/4 v11, 0x4

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x5

    const/4 v11, 0x5

    aget v11, v1, v11

    mul-float/2addr v11, v2

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x6

    const/4 v11, 0x6

    aget v11, v1, v11

    mul-float/2addr v11, v6

    aput v11, v9, v10

    const/4 v10, 0x7

    const/4 v11, 0x5

    aget v11, v9, v11

    aput v11, v9, v10

    :cond_3
    invoke-static {v9}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createVerticesBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$002(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    return-void
.end method

.method public static setRenderToRotate(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;IIIIF)V
    .locals 16
    .param p0    # Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F

    move/from16 v0, p5

    neg-float v13, v0

    const v14, 0x3c8efa35

    mul-float v7, v13, v14

    invoke-static {v7}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    invoke-static {v7}, Landroid/util/FloatMath;->sin(F)F

    move-result v10

    move/from16 v0, p1

    int-to-float v13, v0

    mul-float v3, v2, v13

    move/from16 v0, p1

    int-to-float v13, v0

    mul-float v11, v10, v13

    move/from16 v0, p2

    int-to-float v13, v0

    mul-float v1, v2, v13

    move/from16 v0, p2

    int-to-float v13, v0

    mul-float v9, v10, v13

    const/16 v13, 0x8

    new-array v12, v13, [F

    const/4 v13, 0x0

    neg-float v14, v3

    add-float/2addr v14, v9

    aput v14, v12, v13

    const/4 v13, 0x1

    neg-float v14, v11

    sub-float/2addr v14, v1

    aput v14, v12, v13

    const/4 v13, 0x2

    add-float v14, v3, v9

    aput v14, v12, v13

    const/4 v13, 0x3

    sub-float v14, v11, v1

    aput v14, v12, v13

    const/4 v13, 0x4

    const/4 v14, 0x2

    aget v14, v12, v14

    neg-float v14, v14

    aput v14, v12, v13

    const/4 v13, 0x5

    const/4 v14, 0x3

    aget v14, v12, v14

    neg-float v14, v14

    aput v14, v12, v13

    const/4 v13, 0x6

    const/4 v14, 0x0

    aget v14, v12, v14

    neg-float v14, v14

    aput v14, v12, v13

    const/4 v13, 0x7

    const/4 v14, 0x1

    aget v14, v12, v14

    neg-float v14, v14

    aput v14, v12, v13

    const/4 v13, 0x0

    aget v13, v12, v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const/4 v14, 0x2

    aget v14, v12, v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    move-result v6

    const/4 v13, 0x1

    aget v13, v12, v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const/4 v14, 0x3

    aget v14, v12, v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    move-result v5

    move/from16 v0, p3

    int-to-float v13, v0

    div-float/2addr v13, v6

    move/from16 v0, p4

    int-to-float v14, v0

    div-float/2addr v14, v5

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v8

    const/4 v4, 0x0

    :goto_0
    const/16 v13, 0x8

    if-ge v4, v13, :cond_0

    aget v13, v12, v4

    move/from16 v0, p3

    int-to-float v14, v0

    div-float v14, v8, v14

    mul-float/2addr v13, v14

    aput v13, v12, v4

    add-int/lit8 v13, v4, 0x1

    aget v14, v12, v13

    move/from16 v0, p4

    int-to-float v15, v0

    div-float v15, v8, v15

    mul-float/2addr v14, v15

    aput v14, v12, v13

    add-int/lit8 v4, v4, 0x2

    goto :goto_0

    :cond_0
    invoke-static {v12}, Lcom/android/gallery3d/photoeditor/RendererUtils;->createVerticesBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;->access$002(Lcom/android/gallery3d/photoeditor/RendererUtils$RenderContext;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    return-void
.end method
