.class public Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder;
.super Landroid/app/AlertDialog$Builder;
.source "YesCancelDialogBuilder.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Runnable;
    .param p3    # I

    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0c01a6

    new-instance v1, Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder$2;

    invoke-direct {v1, p0, p2}, Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder$2;-><init>(Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01ed

    new-instance v2, Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder$1;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder$1;-><init>(Lcom/android/gallery3d/photoeditor/YesCancelDialogBuilder;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    return-void
.end method
