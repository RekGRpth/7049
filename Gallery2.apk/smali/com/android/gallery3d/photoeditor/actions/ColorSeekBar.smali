.class Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;
.super Lcom/android/gallery3d/photoeditor/actions/AbstractSeekBar;
.source "ColorSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$OnColorChangeListener;
    }
.end annotation


# instance fields
.field private final colors:[I

.field private progressDrawable:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/AbstractSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080056

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->setMax(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;)[I
    .locals 1
    .param p0    # Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    return-object v0
.end method


# virtual methods
.method public getColor()I
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super/range {p0 .. p4}, Lcom/android/gallery3d/photoeditor/actions/AbstractSeekBar;->onSizeChanged(IIII)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->progressDrawable:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->progressDrawable:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int v11, v2, v4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    sub-int v6, v2, v4

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->progressDrawable:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->progressDrawable:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v8, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v8, v2}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    div-int/lit8 v9, v6, 0x2

    int-to-float v1, v9

    sub-int v2, v11, v9

    int-to-float v3, v2

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    const/4 v2, 0x0

    int-to-float v4, v6

    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    const/4 v4, 0x0

    aget v2, v2, v4

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v2, v9

    int-to-float v4, v9

    invoke-virtual {v0, v1, v2, v4, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v2, v2, v4

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v2, v9

    int-to-float v4, v9

    invoke-virtual {v0, v3, v2, v4, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    sub-float v2, v3, v1

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    div-float v10, v2, v4

    const/high16 v2, 0x40000000

    div-float v2, v10, v2

    add-float v3, v1, v2

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    const/4 v4, 0x0

    aget v2, v2, v4

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    int-to-float v4, v6

    move-object v5, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move v1, v3

    const/4 v7, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v7, v2, :cond_1

    add-float v3, v1, v10

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    aget v2, v2, v7

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    int-to-float v4, v6

    move-object v5, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move v1, v3

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    const/high16 v2, 0x40000000

    div-float v2, v10, v2

    add-float v3, v1, v2

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->colors:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v2, v2, v4

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    int-to-float v4, v6

    move-object v5, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->progressDrawable:Landroid/graphics/Bitmap;

    invoke-direct {v2, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setColorIndex(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method public setOnColorChangeListener(Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$OnColorChangeListener;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$OnColorChangeListener;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void

    :cond_0
    new-instance v0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$1;

    invoke-direct {v0, p0, p1}, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$OnColorChangeListener;)V

    goto :goto_0
.end method
