.class public Lcom/android/gallery3d/photoeditor/actions/CropAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "CropAction.java"


# static fields
.field private static final DEFAULT_CROP:F = 0.2f


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 6

    const v5, 0x3f4ccccd

    const v4, 0x3e4ccccd

    new-instance v2, Lcom/android/gallery3d/photoeditor/filters/CropFilter;

    invoke-direct {v2}, Lcom/android/gallery3d/photoeditor/filters/CropFilter;-><init>()V

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->disableFilterOutput()V

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->toolKit:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;

    invoke-virtual {v3}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addCropView()Lcom/android/gallery3d/photoeditor/actions/CropView;

    move-result-object v1

    new-instance v3, Lcom/android/gallery3d/photoeditor/actions/CropAction$1;

    invoke-direct {v3, p0, v2}, Lcom/android/gallery3d/photoeditor/actions/CropAction$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/CropAction;Lcom/android/gallery3d/photoeditor/filters/CropFilter;)V

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/photoeditor/actions/CropView;->setOnCropChangeListener(Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v4, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/photoeditor/actions/CropView;->setCropBounds(Landroid/graphics/RectF;)V

    invoke-virtual {v2, v0}, Lcom/android/gallery3d/photoeditor/filters/CropFilter;->setCropBounds(Landroid/graphics/RectF;)V

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyChanged(Lcom/android/gallery3d/photoeditor/filters/Filter;)V

    return-void
.end method
