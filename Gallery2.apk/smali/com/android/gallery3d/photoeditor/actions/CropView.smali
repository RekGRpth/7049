.class Lcom/android/gallery3d/photoeditor/actions/CropView;
.super Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;
    }
.end annotation


# static fields
.field private static final MIN_CROP_WIDTH_HEIGHT:I = 0x2

.field private static final MOVE_BLOCK:I = 0x10

.field private static final MOVE_BOTTOM:I = 0x8

.field private static final MOVE_LEFT:I = 0x1

.field private static final MOVE_RIGHT:I = 0x4

.field private static final MOVE_TOP:I = 0x2

.field private static final SHADOW_ALPHA:I = 0xa0

.field private static final TOUCH_TOLERANCE:I = 0x19


# instance fields
.field private final borderPaint:Landroid/graphics/Paint;

.field private final cropBounds:Landroid/graphics/RectF;

.field private final cropIndicator:Landroid/graphics/drawable/Drawable;

.field private final indicatorSize:I

.field private lastX:F

.field private lastY:F

.field private listener:Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;

.field private movingEdges:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v3, v3, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropIndicator:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f0a0047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->indicatorSize:I

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->borderPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->borderPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->borderPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->borderPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method private detectMovingEdges(FF)V
    .locals 10
    .param p1    # F
    .param p2    # F

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/high16 v9, 0x41c80000

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/CropView;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v1

    iput v6, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    iget v7, v1, Landroid/graphics/RectF;->left:F

    sub-float v7, p1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float v7, p1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v7, v2, v9

    if-gtz v7, :cond_3

    cmpg-float v7, v2, v3

    if-gez v7, :cond_3

    iget v7, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    :cond_0
    :goto_0
    iget v7, v1, Landroid/graphics/RectF;->top:F

    sub-float v7, p2, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v7, p2, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v7, v4, v9

    if-gtz v7, :cond_4

    move v7, v5

    :goto_1
    cmpg-float v8, v4, v0

    if-gez v8, :cond_5

    :goto_2
    and-int/2addr v5, v7

    if-eqz v5, :cond_6

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    :cond_1
    :goto_3
    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_2

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    if-nez v5, :cond_2

    const/16 v5, 0x10

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_3
    cmpg-float v7, v3, v9

    if-gtz v7, :cond_0

    iget v7, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    or-int/lit8 v7, v7, 0x4

    iput v7, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    goto :goto_0

    :cond_4
    move v7, v6

    goto :goto_1

    :cond_5
    move v5, v6

    goto :goto_2

    :cond_6
    cmpg-float v5, v0, v9

    if-gtz v5, :cond_1

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    goto :goto_3
.end method

.method private drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # F
    .param p4    # F

    float-to-int v2, p3

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->indicatorSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    float-to-int v2, p4

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->indicatorSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->indicatorSize:I

    add-int/2addr v2, v0

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->indicatorSize:I

    add-int/2addr v3, v1

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawShadow(Landroid/graphics/Canvas;FFFF)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    const/16 v0, 0xa0

    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private getCropBoundsDisplayed()Landroid/graphics/RectF;
    .locals 7

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    new-instance v0, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    mul-float/2addr v3, v2

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    mul-float/2addr v4, v1

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    mul-float/2addr v5, v2

    iget-object v6, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v6, v1

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v3, v4}, Landroid/graphics/RectF;->offset(FF)V

    return-object v0
.end method

.method private moveEdges(FF)V
    .locals 7
    .param p1    # F
    .param p2    # F

    const/high16 v6, 0x40000000

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/CropView;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v0

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_2

    cmpl-float v3, p1, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v4

    invoke-static {v3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    :goto_0
    cmpl-float v3, p2, v5

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    invoke-static {v3, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    :goto_1
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->offset(FF)V

    :goto_2
    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v3}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->mapPhotoRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/gallery3d/photoeditor/actions/CropView;->refreshByCropChange(Z)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    invoke-static {v3, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-static {v3, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v6

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoWidth()F

    move-result v4

    div-float v2, v3, v4

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, v6

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoHeight()F

    move-result v4

    div-float v1, v3, v4

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    iget v3, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p1

    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->left:F

    :cond_3
    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_4

    iget v3, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, p2

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->top:F

    :cond_4
    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_5

    iget v3, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, p1

    iget v4, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->right:F

    :cond_5
    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_6

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, p2

    iget v4, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    :cond_6
    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    goto/16 :goto_2
.end method

.method private refreshByCropChange(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->listener:Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->listener:Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;

    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-interface {v0, v1, p1}, Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;->onCropChanged(Landroid/graphics/RectF;Z)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/CropView;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v6

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, v6, Landroid/graphics/RectF;->top:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawShadow(Landroid/graphics/Canvas;FFFF)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    iget v4, v6, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawShadow(Landroid/graphics/Canvas;FFFF)V

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawShadow(Landroid/graphics/Canvas;FFFF)V

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->bottom:F

    iget v4, v6, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawShadow(Landroid/graphics/Canvas;FFFF)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->borderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    if-nez v0, :cond_8

    const/4 v7, 0x1

    :goto_0
    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    if-eqz v7, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget v2, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_1
    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    if-eqz v7, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_3
    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_4

    if-eqz v7, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropIndicator:Landroid/graphics/drawable/Drawable;

    iget v1, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_5
    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_6

    if-eqz v7, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropIndicator:Landroid/graphics/drawable/Drawable;

    iget v1, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/CropView;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_7
    return-void

    :cond_8
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/photoeditor/actions/CropView;->detectMovingEdges(FF)V

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastX:F

    iput v1, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastY:F

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastX:F

    sub-float v2, v0, v2

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastY:F

    sub-float v3, v1, v3

    invoke-direct {p0, v2, v3}, Lcom/android/gallery3d/photoeditor/actions/CropView;->moveEdges(FF)V

    :cond_1
    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastX:F

    iput v1, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->lastY:F

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->movingEdges:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setCropBounds(Landroid/graphics/RectF;)V
    .locals 5
    .param p1    # Landroid/graphics/RectF;

    const/high16 v4, 0x40000000

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoWidth()F

    move-result v1

    mul-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoHeight()F

    move-result v1

    mul-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->cropBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/CropView;->refreshByCropChange(Z)V

    return-void
.end method

.method public setOnCropChangeListener(Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/actions/CropView;->listener:Lcom/android/gallery3d/photoeditor/actions/CropView$OnCropChangeListener;

    return-void
.end method
