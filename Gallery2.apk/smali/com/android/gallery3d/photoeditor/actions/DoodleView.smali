.class Lcom/android/gallery3d/photoeditor/actions/DoodleView;
.super Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;
.source "DoodleView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;
    }
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private bitmapCanvas:Landroid/graphics/Canvas;

.field private final bitmapPaint:Landroid/graphics/Paint;

.field private color:I

.field private final displayMatrix:Landroid/graphics/Matrix;

.field private doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

.field private final doodlePaint:Landroid/graphics/Paint;

.field private final drawingMatrix:Landroid/graphics/Matrix;

.field private final drawingPath:Landroid/graphics/Path;

.field private final lastPoint:Landroid/graphics/PointF;

.field private listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmapPaint:Landroid/graphics/Paint;

    invoke-static {}, Lcom/android/gallery3d/photoeditor/actions/Doodle;->createPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodlePaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->displayMatrix:Landroid/graphics/Matrix;

    return-void
.end method

.method private addLastPointIntoDoodle()V
    .locals 4

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/photoeditor/actions/Doodle;->addControlPoint(Landroid/graphics/PointF;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    invoke-interface {v0, v1}, Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;->onDoodleChanged(Lcom/android/gallery3d/photoeditor/actions/Doodle;)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    return-void
.end method

.method private drawDoodle(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodlePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    invoke-virtual {v1}, Lcom/android/gallery3d/photoeditor/actions/Doodle;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingPath:Landroid/graphics/Path;

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/Doodle;->getDrawingPath(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private finishDoodle()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    invoke-virtual {v0}, Lcom/android/gallery3d/photoeditor/actions/Doodle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmapCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawDoodle(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    invoke-interface {v0, v1}, Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;->onDoodleFinished(Lcom/android/gallery3d/photoeditor/actions/Doodle;)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    return-void
.end method

.method private startDoodle()V
    .locals 5

    new-instance v0, Lcom/android/gallery3d/photoeditor/actions/Doodle;

    iget v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->color:I

    new-instance v2, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/Doodle;-><init>(ILandroid/graphics/PointF;)V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->doodle:Lcom/android/gallery3d/photoeditor/actions/Doodle;

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->displayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawDoodle(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/high16 v5, 0x3f800000

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->onSizeChanged(IIII)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoWidth()F

    move-result v1

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->getPhotoHeight()F

    move-result v2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->bitmapCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->drawingMatrix:Landroid/graphics/Matrix;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v4, v4, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v2, v0, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->displayMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/high16 v3, 0x3f800000

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->mapPhotoPoint(FFLandroid/graphics/PointF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->startDoodle()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->mapPhotoPoint(FFLandroid/graphics/PointF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->addLastPointIntoDoodle()V

    goto :goto_0

    :pswitch_2
    add-float v2, v0, v3

    add-float/2addr v3, v1

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->lastPoint:Landroid/graphics/PointF;

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->mapPhotoPoint(FFLandroid/graphics/PointF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->addLastPointIntoDoodle()V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->finishDoodle()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->color:I

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->finishDoodle()V

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->startDoodle()V

    return-void
.end method

.method public setOnDoodleChangeListener(Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;->listener:Lcom/android/gallery3d/photoeditor/actions/DoodleView$OnDoodleChangeListener;

    return-void
.end method
