.class public Lcom/android/gallery3d/photoeditor/actions/DuotoneAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "DuotoneAction.java"


# static fields
.field private static final DEFAULT_FIRST_COLOR:I = 0x4488

.field private static final DEFAULT_SECOND_COLOR:I = 0xffff00


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 3

    new-instance v0, Lcom/android/gallery3d/photoeditor/filters/DuotoneFilter;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/filters/DuotoneFilter;-><init>()V

    const/16 v1, 0x4488

    const v2, 0xffff00

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/photoeditor/filters/DuotoneFilter;->setDuotone(II)V

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyChanged(Lcom/android/gallery3d/photoeditor/filters/Filter;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyOk()V

    return-void
.end method
