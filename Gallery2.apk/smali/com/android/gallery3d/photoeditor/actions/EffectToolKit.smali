.class public Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;
.super Ljava/lang/Object;
.source "EffectToolKit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$2;,
        Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;
    }
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field private final photoView:Lcom/android/gallery3d/photoeditor/PhotoView;

.field private final toolFullscreen:Landroid/view/ViewGroup;

.field private final toolPanel:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0b00c3

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040038

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    const v3, 0x7f0b00ba

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const v2, 0x7f0b00c2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/photoeditor/PhotoView;

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->photoView:Lcom/android/gallery3d/photoeditor/PhotoView;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->photoView:Lcom/android/gallery3d/photoeditor/PhotoView;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040037

    invoke-virtual {v2, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->photoView:Lcom/android/gallery3d/photoeditor/PhotoView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method private addFullscreenTool(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->inflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->getPhotoView()Lcom/android/gallery3d/photoeditor/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/photoeditor/PhotoView;->getPhotoBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->setPhotoBounds(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private addPanelTool(I)Landroid/view/View;
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->inflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    const v4, 0x7f0b00ba

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-object v0
.end method

.method private getScalePickerProgressDrawable(Landroid/content/res/Resources;Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;

    sget-object v0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$2;->$SwitchMap$com$android$gallery3d$photoeditor$actions$EffectToolKit$ScaleType:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f02014b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f02014c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f02014d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02014a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addColorPicker()Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;
    .locals 1

    const v0, 0x7f040034

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addPanelTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;

    return-object v0
.end method

.method public addCropView()Lcom/android/gallery3d/photoeditor/actions/CropView;
    .locals 1

    const v0, 0x7f040035

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFullscreenTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/CropView;

    return-object v0
.end method

.method public addDoodleView()Lcom/android/gallery3d/photoeditor/actions/DoodleView;
    .locals 1

    const v0, 0x7f040036

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFullscreenTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/DoodleView;

    return-object v0
.end method

.method public addFlipView()Lcom/android/gallery3d/photoeditor/actions/FlipView;
    .locals 1

    const v0, 0x7f04003f

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFullscreenTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/FlipView;

    return-object v0
.end method

.method public addRotateView()Lcom/android/gallery3d/photoeditor/actions/RotateView;
    .locals 1

    const v0, 0x7f040041

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFullscreenTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/RotateView;

    return-object v0
.end method

.method public addScalePicker(Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;)Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar;
    .locals 2
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;

    const v1, 0x7f040042

    invoke-direct {p0, v1}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addPanelTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->getScalePickerProgressDrawable(Landroid/content/res/Resources;Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public addTouchView()Lcom/android/gallery3d/photoeditor/actions/TouchView;
    .locals 1

    const v0, 0x7f040043

    invoke-direct {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFullscreenTool(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/photoeditor/actions/TouchView;

    return-object v0
.end method

.method public cancel()V
    .locals 10

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    new-instance v9, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$1;

    invoke-direct {v9, p0}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    invoke-virtual {v2, v9}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v9}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolFullscreen:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->toolPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getPhotoView()Lcom/android/gallery3d/photoeditor/PhotoView;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->photoView:Lcom/android/gallery3d/photoeditor/PhotoView;

    return-object v0
.end method
