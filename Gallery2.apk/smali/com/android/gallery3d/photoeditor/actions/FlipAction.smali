.class public Lcom/android/gallery3d/photoeditor/actions/FlipAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "FlipAction.java"


# static fields
.field private static final DEFAULT_ANGLE:F = 0.0f

.field private static final DEFAULT_FLIP_SPAN:F = 180.0f


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/android/gallery3d/photoeditor/filters/FlipFilter;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/filters/FlipFilter;-><init>()V

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->disableFilterOutput()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->toolKit:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;

    invoke-virtual {v2}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addFlipView()Lcom/android/gallery3d/photoeditor/actions/FlipView;

    move-result-object v1

    new-instance v2, Lcom/android/gallery3d/photoeditor/actions/FlipAction$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/gallery3d/photoeditor/actions/FlipAction$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/FlipAction;Lcom/android/gallery3d/photoeditor/filters/FlipFilter;Lcom/android/gallery3d/photoeditor/actions/FlipView;)V

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->setOnFlipChangeListener(Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;)V

    invoke-virtual {v1, v3, v3}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->setFlippedAngles(FF)V

    const/high16 v2, 0x43340000

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->setFlipSpan(F)V

    return-void
.end method
