.class Lcom/android/gallery3d/photoeditor/actions/FlipView;
.super Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;
.source "FlipView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;
    }
.end annotation


# static fields
.field private static final FIXED_DIRECTION_THRESHOLD:F = 20.0f


# instance fields
.field private currentHorizontalDegrees:F

.field private currentVerticalDegrees:F

.field private fixedDirection:Z

.field private fixedDirectionHorizontal:Z

.field private lastHorizontalDegrees:F

.field private lastVerticalDegrees:F

.field private listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

.field private maxFlipSpan:F

.field private touchStartX:F

.field private touchStartY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private calculateAngle(ZFF)F
    .locals 4
    .param p1    # Z
    .param p2    # F
    .param p3    # F

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    :goto_0
    int-to-float v2, v2

    const v3, 0x3eb33333

    mul-float v0, v2, v3

    if-eqz p1, :cond_2

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartX:F

    sub-float v1, p2, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_3

    move v1, v0

    :goto_2
    if-eqz p1, :cond_4

    sub-float v2, p2, v1

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartX:F

    :cond_0
    :goto_3
    div-float v2, v1, v0

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->maxFlipSpan:F

    mul-float/2addr v2, v3

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartY:F

    sub-float v1, v2, p3

    goto :goto_1

    :cond_3
    neg-float v1, v0

    goto :goto_2

    :cond_4
    add-float v2, v1, p3

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartY:F

    goto :goto_3
.end method

.method private refreshAngle(FFZ)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # Z

    iput p1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->currentHorizontalDegrees:F

    iput p2, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->currentVerticalDegrees:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;->onAngleChanged(FFZ)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    return v4

    :pswitch_0
    iput-boolean v1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirection:Z

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->currentHorizontalDegrees:F

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastHorizontalDegrees:F

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->currentVerticalDegrees:F

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastVerticalDegrees:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iput v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartY:F

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    invoke-interface {v5}, Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;->onStartTrackingTouch()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-boolean v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirection:Z

    if-eqz v5, :cond_3

    iget-boolean v1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirectionHorizontal:Z

    :cond_1
    :goto_1
    invoke-direct {p0, v1, v2, v3}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->calculateAngle(ZFF)F

    move-result v0

    iget-boolean v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirection:Z

    if-nez v5, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x41a00000

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    iput-boolean v4, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirection:Z

    iput-boolean v1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->fixedDirectionHorizontal:Z

    :cond_2
    if-eqz v1, :cond_4

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastHorizontalDegrees:F

    add-float/2addr v5, v0

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastVerticalDegrees:F

    invoke-direct {p0, v5, v6, v4}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->refreshAngle(FFZ)V

    goto :goto_0

    :cond_3
    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartX:F

    sub-float v5, v2, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->touchStartY:F

    sub-float v6, v3, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_1

    move v1, v4

    goto :goto_1

    :cond_4
    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastHorizontalDegrees:F

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->lastVerticalDegrees:F

    add-float/2addr v6, v0

    invoke-direct {p0, v5, v6, v4}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->refreshAngle(FFZ)V

    goto :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    invoke-interface {v5}, Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;->onStopTrackingTouch()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setFlipSpan(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->maxFlipSpan:F

    return-void
.end method

.method public setFlippedAngles(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/gallery3d/photoeditor/actions/FlipView;->refreshAngle(FFZ)V

    return-void
.end method

.method public setOnFlipChangeListener(Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/actions/FlipView;->listener:Lcom/android/gallery3d/photoeditor/actions/FlipView$OnFlipChangeListener;

    return-void
.end method
