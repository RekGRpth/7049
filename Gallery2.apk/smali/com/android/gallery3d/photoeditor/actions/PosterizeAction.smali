.class public Lcom/android/gallery3d/photoeditor/actions/PosterizeAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "PosterizeAction.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 1

    new-instance v0, Lcom/android/gallery3d/photoeditor/filters/PosterizeFilter;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/filters/PosterizeFilter;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyChanged(Lcom/android/gallery3d/photoeditor/filters/Filter;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyOk()V

    return-void
.end method
