.class Lcom/android/gallery3d/photoeditor/actions/RotateView;
.super Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;
.source "RotateView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;
    }
.end annotation


# static fields
.field private static final MATH_HALF_PI:F = 1.5707964f

.field private static final MATH_PI:F = 3.1415927f

.field private static final RADIAN_TO_DEGREE:F = 57.295776f


# instance fields
.field private centerX:I

.field private centerY:I

.field private currentRotatedAngle:F

.field private final dashStrokePaint:Landroid/graphics/Paint;

.field private drawGrids:Z

.field private final grids:Landroid/graphics/Path;

.field private final gridsColor:I

.field private lastRotatedAngle:F

.field private listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

.field private maxRotatedAngle:F

.field private minRotatedAngle:F

.field private final referenceColor:I

.field private final referenceLine:Landroid/graphics/Path;

.field private touchStartAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x3f800000

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->gridsColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceColor:I

    return-void

    nop

    :array_0
    .array-data 4
        0x41700000
        0x40a00000
    .end array-data
.end method

.method private calculateAngle(Landroid/view/MotionEvent;)F
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const v6, 0x40490fdb

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-float v4, v4

    sub-float v1, v3, v4

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float v2, v3, v4

    cmpl-float v3, v1, v5

    if-nez v3, :cond_2

    cmpl-float v3, v2, v5

    if-ltz v3, :cond_1

    const v0, 0x3fc90fdb

    :goto_0
    cmpl-float v3, v0, v5

    if-ltz v3, :cond_3

    cmpg-float v3, v1, v5

    if-gez v3, :cond_3

    sub-float/2addr v0, v6

    :cond_0
    :goto_1
    return v0

    :cond_1
    const v0, -0x4036f025

    goto :goto_0

    :cond_2
    div-float v3, v2, v1

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->atan(D)D

    move-result-wide v3

    double-to-float v0, v3

    goto :goto_0

    :cond_3
    cmpg-float v3, v0, v5

    if-gez v3, :cond_0

    cmpg-float v3, v1, v5

    if-gez v3, :cond_0

    add-float/2addr v0, v6

    goto :goto_1
.end method

.method private refreshAngle(FZ)V
    .locals 2
    .param p1    # F
    .param p2    # Z

    neg-float v0, p1

    const v1, 0x42652ee0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->currentRotatedAngle:F

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;->onAngleChanged(FZ)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->drawGrids:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->gridsColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->currentRotatedAngle:F

    neg-float v0, v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->dashStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/high16 v8, 0x40800000

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->onSizeChanged(IIII)V

    div-int/lit8 v4, p1, 0x2

    iput v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    div-int/lit8 v4, p2, 0x2

    iput v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    iget v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-double v4, v4

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iget v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-float v4, v4

    sub-float v0, v1, v4

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    neg-float v5, v0

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, v0

    iget v6, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerY:I

    int-to-float v4, v4

    sub-float v0, v1, v4

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-float v5, v5

    neg-float v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->referenceLine:Landroid/graphics/Path;

    iget v5, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->centerX:I

    int-to-float v5, v5

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v0, v4, v8

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float v2, v4, v0

    :goto_0
    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v2, v5}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    add-float/2addr v2, v0

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float v0, v4, v8

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float v3, v4, v0

    :goto_1
    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v3, v4

    if-gez v4, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v4, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->grids:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/android/gallery3d/photoeditor/actions/FullscreenToolView;->displayBounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Path;->lineTo(FF)V

    add-float/2addr v3, v0

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return v4

    :pswitch_0
    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->currentRotatedAngle:F

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->lastRotatedAngle:F

    invoke-direct {p0, p1}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->calculateAngle(Landroid/view/MotionEvent;)F

    move-result v2

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->touchStartAngle:F

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    invoke-interface {v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;->onStartTrackingTouch()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->calculateAngle(Landroid/view/MotionEvent;)F

    move-result v1

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->touchStartAngle:F

    sub-float v2, v1, v2

    iget v3, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->lastRotatedAngle:F

    add-float v0, v2, v3

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->maxRotatedAngle:F

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->minRotatedAngle:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_2

    :cond_1
    iget v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->currentRotatedAngle:F

    iput v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->lastRotatedAngle:F

    iput v1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->touchStartAngle:F

    goto :goto_0

    :cond_2
    neg-float v2, v0

    const v3, 0x42652ee0

    mul-float/2addr v2, v3

    invoke-direct {p0, v2, v4}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->refreshAngle(FZ)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    invoke-interface {v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;->onStopTrackingTouch()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDrawGrids(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->drawGrids:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setOnRotateChangeListener(Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    iput-object p1, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->listener:Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;

    return-void
.end method

.method public setRotateSpan(F)V
    .locals 2
    .param p1    # F

    const/high16 v0, 0x43b40000

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x7f800000

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->maxRotatedAngle:F

    :goto_0
    iget v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->maxRotatedAngle:F

    neg-float v0, v0

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->minRotatedAngle:F

    return-void

    :cond_0
    const v0, 0x42652ee0

    div-float v0, p1, v0

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/photoeditor/actions/RotateView;->maxRotatedAngle:F

    goto :goto_0
.end method

.method public setRotatedAngle(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->refreshAngle(FZ)V

    return-void
.end method
