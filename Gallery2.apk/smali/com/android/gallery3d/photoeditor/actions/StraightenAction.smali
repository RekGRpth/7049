.class public Lcom/android/gallery3d/photoeditor/actions/StraightenAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "StraightenAction.java"


# static fields
.field private static final DEFAULT_ANGLE:F = 0.0f

.field private static final DEFAULT_ROTATE_SPAN:F = 90.0f


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 3

    new-instance v0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;-><init>()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->toolKit:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;

    invoke-virtual {v2}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addRotateView()Lcom/android/gallery3d/photoeditor/actions/RotateView;

    move-result-object v1

    new-instance v2, Lcom/android/gallery3d/photoeditor/actions/StraightenAction$1;

    invoke-direct {v2, p0, v0}, Lcom/android/gallery3d/photoeditor/actions/StraightenAction$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/StraightenAction;Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;)V

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->setOnRotateChangeListener(Lcom/android/gallery3d/photoeditor/actions/RotateView$OnRotateChangeListener;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->setDrawGrids(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->setRotatedAngle(F)V

    const/high16 v2, 0x42b40000

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/RotateView;->setRotateSpan(F)V

    return-void
.end method
