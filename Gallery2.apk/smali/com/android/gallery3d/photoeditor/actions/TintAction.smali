.class public Lcom/android/gallery3d/photoeditor/actions/TintAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "TintAction.java"


# static fields
.field private static final DEFAULT_COLOR_INDEX:I = 0xd


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 3

    new-instance v1, Lcom/android/gallery3d/photoeditor/filters/TintFilter;

    invoke-direct {v1}, Lcom/android/gallery3d/photoeditor/filters/TintFilter;-><init>()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->toolKit:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;

    invoke-virtual {v2}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addColorPicker()Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;

    move-result-object v0

    new-instance v2, Lcom/android/gallery3d/photoeditor/actions/TintAction$1;

    invoke-direct {v2, p0, v1}, Lcom/android/gallery3d/photoeditor/actions/TintAction$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/TintAction;Lcom/android/gallery3d/photoeditor/filters/TintFilter;)V

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->setOnColorChangeListener(Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar$OnColorChangeListener;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->setColorIndex(I)V

    invoke-virtual {v0}, Lcom/android/gallery3d/photoeditor/actions/ColorSeekBar;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/filters/TintFilter;->setTint(I)V

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyChanged(Lcom/android/gallery3d/photoeditor/filters/Filter;)V

    return-void
.end method
