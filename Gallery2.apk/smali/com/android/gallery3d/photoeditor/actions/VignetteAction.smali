.class public Lcom/android/gallery3d/photoeditor/actions/VignetteAction;
.super Lcom/android/gallery3d/photoeditor/actions/EffectAction;
.source "VignetteAction.java"


# static fields
.field private static final DEFAULT_SCALE:F = 0.5f


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public prepare()V
    .locals 5

    const/high16 v4, 0x3f000000

    new-instance v0, Lcom/android/gallery3d/photoeditor/filters/VignetteFilter;

    invoke-direct {v0}, Lcom/android/gallery3d/photoeditor/filters/VignetteFilter;-><init>()V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->toolKit:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;

    sget-object v3, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;->GENERIC:Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/photoeditor/actions/EffectToolKit;->addScalePicker(Lcom/android/gallery3d/photoeditor/actions/EffectToolKit$ScaleType;)Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar;

    move-result-object v1

    new-instance v2, Lcom/android/gallery3d/photoeditor/actions/VignetteAction$1;

    invoke-direct {v2, p0, v0}, Lcom/android/gallery3d/photoeditor/actions/VignetteAction$1;-><init>(Lcom/android/gallery3d/photoeditor/actions/VignetteAction;Lcom/android/gallery3d/photoeditor/filters/VignetteFilter;)V

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar;->setOnScaleChangeListener(Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar$OnScaleChangeListener;)V

    invoke-virtual {v1, v4}, Lcom/android/gallery3d/photoeditor/actions/ScaleSeekBar;->setProgress(F)V

    invoke-virtual {v0, v4}, Lcom/android/gallery3d/photoeditor/filters/AbstractScaleFilter;->setScale(F)V

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/photoeditor/actions/EffectAction;->notifyChanged(Lcom/android/gallery3d/photoeditor/filters/Filter;)V

    return-void
.end method
