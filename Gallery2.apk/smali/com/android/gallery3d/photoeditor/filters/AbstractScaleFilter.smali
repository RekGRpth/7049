.class public abstract Lcom/android/gallery3d/photoeditor/filters/AbstractScaleFilter;
.super Lcom/android/gallery3d/photoeditor/filters/Filter;
.source "AbstractScaleFilter.java"


# instance fields
.field protected scale:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/filters/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/photoeditor/filters/AbstractScaleFilter;->scale:F

    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/photoeditor/filters/AbstractScaleFilter;->scale:F

    return-void
.end method

.method protected writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    iget v0, p0, Lcom/android/gallery3d/photoeditor/filters/AbstractScaleFilter;->scale:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method
