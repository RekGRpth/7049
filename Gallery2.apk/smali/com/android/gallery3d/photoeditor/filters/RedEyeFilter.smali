.class public Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;
.super Lcom/android/gallery3d/photoeditor/filters/Filter;
.source "RedEyeFilter.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final redeyes:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->creatorOf(Ljava/lang/Class;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/filters/Filter;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addRedEyePosition(Landroid/graphics/PointF;)V
    .locals 1
    .param p1    # Landroid/graphics/PointF;

    iget-object v0, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public process(Lcom/android/gallery3d/photoeditor/Photo;Lcom/android/gallery3d/photoeditor/Photo;)V
    .locals 10
    .param p1    # Lcom/android/gallery3d/photoeditor/Photo;
    .param p2    # Lcom/android/gallery3d/photoeditor/Photo;

    const-string v6, "android.media.effect.effects.RedEyeEffect"

    invoke-virtual {p0, v6}, Lcom/android/gallery3d/photoeditor/filters/Filter;->getEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v1

    iget-object v6, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    new-array v0, v6, [F

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    add-int/lit8 v4, v3, 0x1

    iget v6, v2, Landroid/graphics/PointF;->x:F

    aput v6, v0, v3

    add-int/lit8 v3, v4, 0x1

    iget v6, v2, Landroid/graphics/PointF;->y:F

    aput v6, v0, v4

    goto :goto_0

    :cond_0
    const-string v6, "centers"

    invoke-virtual {v1, v6, v0}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->texture()I

    move-result v6

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v7

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v8

    invoke-virtual {p2}, Lcom/android/gallery3d/photoeditor/Photo;->texture()I

    move-result v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/media/effect/Effect;->apply(IIII)V

    return-void
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v2, p0, Lcom/android/gallery3d/photoeditor/filters/RedEyeFilter;->redeyes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
