.class public Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;
.super Lcom/android/gallery3d/photoeditor/filters/Filter;
.source "StraightenFilter.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_DEGREES:F = 45.0f


# instance fields
.field private degrees:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;

    invoke-static {v0}, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->creatorOf(Ljava/lang/Class;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/photoeditor/filters/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public process(Lcom/android/gallery3d/photoeditor/Photo;Lcom/android/gallery3d/photoeditor/Photo;)V
    .locals 5
    .param p1    # Lcom/android/gallery3d/photoeditor/Photo;
    .param p2    # Lcom/android/gallery3d/photoeditor/Photo;

    const-string v1, "android.media.effect.effects.StraightenEffect"

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/photoeditor/filters/Filter;->getEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v0

    const-string v1, "maxAngle"

    const/high16 v2, 0x42340000

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "angle"

    iget v2, p0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->degrees:F

    neg-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->texture()I

    move-result v1

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->width()I

    move-result v2

    invoke-virtual {p1}, Lcom/android/gallery3d/photoeditor/Photo;->height()I

    move-result v3

    invoke-virtual {p2}, Lcom/android/gallery3d/photoeditor/Photo;->texture()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/effect/Effect;->apply(IIII)V

    return-void
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->degrees:F

    return-void
.end method

.method public setAngle(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->degrees:F

    return-void
.end method

.method protected writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    iget v0, p0, Lcom/android/gallery3d/photoeditor/filters/StraightenFilter;->degrees:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method
