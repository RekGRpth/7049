.class public Lcom/android/gallery3d/ui/BitmapTileProvider;
.super Ljava/lang/Object;
.source "BitmapTileProvider.java"

# interfaces
.implements Lcom/android/gallery3d/ui/TileImageView$Model;


# instance fields
.field private final mConfig:Landroid/graphics/Bitmap$Config;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mMipmaps:[Landroid/graphics/Bitmap;

.field private mRecycled:Z

.field private final mScreenNail:Lcom/android/gallery3d/ui/ScreenNail;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mRecycled:Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mImageWidth:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mImageHeight:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-gt v1, p2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-le v1, p2, :cond_1

    :cond_0
    const/high16 v1, 0x3f000000

    invoke-static {p1, v1, v2}, Lcom/android/gallery3d/common/BitmapUtils;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {v2, v1}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mMipmaps:[Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method


# virtual methods
.method public getImageHeight()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mImageHeight:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mImageWidth:I

    return v0
.end method

.method public getLevelCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mMipmaps:[Landroid/graphics/Bitmap;

    array-length v0, v0

    return v0
.end method

.method public getScreenNail()Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    return-object v0
.end method

.method public getStereoConvergence()Lcom/mediatek/gallery3d/stereo/StereoConvergence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStereoImageHeight(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/BitmapTileProvider;->getImageHeight()I

    move-result v0

    return v0
.end method

.method public getStereoImageWidth(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/BitmapTileProvider;->getImageWidth()I

    move-result v0

    return v0
.end method

.method public getStereoLevelCount(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/BitmapTileProvider;->getLevelCount()I

    move-result v0

    return v0
.end method

.method public getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTile(IIIIILcom/android/gallery3d/data/BitmapPool;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/data/BitmapPool;

    const/4 v6, 0x0

    shr-int/2addr p2, p1

    shr-int/2addr p3, p1

    mul-int/lit8 v7, p5, 0x2

    add-int v5, p4, v7

    if-nez p6, :cond_0

    move-object v4, v6

    :goto_0
    if-nez v4, :cond_1

    iget-object v7, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    :goto_1
    iget-object v7, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mMipmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v7, p1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    neg-int v7, p2

    add-int v2, v7, p5

    neg-int v7, p3

    add-int v3, v7, p5

    int-to-float v7, v2

    int-to-float v8, v3

    invoke-virtual {v0, v1, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-object v4

    :cond_0
    invoke-virtual {p6}, Lcom/android/gallery3d/data/BitmapPool;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_1
.end method

.method public getTile(IIIIILcom/android/gallery3d/data/BitmapPool;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/data/BitmapPool;
    .param p7    # I

    const-string v0, "BitmapTileProvider"

    const-string v1, "getTile:un-implemented!!!"

    invoke-static {v0, v1}, Lcom/android/gallery3d/ui/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public recycle()V
    .locals 5

    iget-boolean v4, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mRecycled:Z

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mRecycled:Z

    iget-object v0, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mMipmaps:[Landroid/graphics/Bitmap;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    invoke-static {v1}, Lcom/android/gallery3d/common/BitmapUtils;->recycleSilently(Landroid/graphics/Bitmap;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/ui/BitmapTileProvider;->mScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v4}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    goto :goto_0
.end method
