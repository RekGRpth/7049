.class Lcom/android/gallery3d/ui/CropView$DetectFaceTask;
.super Ljava/lang/Thread;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/ui/CropView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetectFaceTask"
.end annotation


# instance fields
.field private final mFaceBitmap:Landroid/graphics/Bitmap;

.field private mFaceCount:I

.field private final mFaces:[Landroid/media/FaceDetector$Face;

.field final synthetic this$0:Lcom/android/gallery3d/ui/CropView;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/media/FaceDetector$Face;

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    iput-object p2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    const-string v0, "face-detect"

    invoke-virtual {p0, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;
    .locals 13
    .param p1    # Landroid/media/FaceDetector$Face;

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p1, v3}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    iget-object v9, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v9

    const/high16 v10, 0x40000000

    mul-float v5, v9, v10

    move v6, v5

    iget-object v9, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v9}, Lcom/android/gallery3d/ui/CropView;->access$900(Lcom/android/gallery3d/ui/CropView;)F

    move-result v0

    const/high16 v9, -0x40800000

    cmpl-float v9, v0, v9

    if-eqz v9, :cond_0

    const/high16 v9, 0x3f800000

    cmpl-float v9, v0, v9

    if-lez v9, :cond_2

    mul-float v5, v6, v0

    :cond_0
    :goto_0
    new-instance v4, Landroid/graphics/RectF;

    iget v9, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v5

    iget v10, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v6

    iget v11, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v5

    iget v12, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v12, v6

    invoke-direct {v4, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    int-to-float v11, v8

    int-to-float v12, v2

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/graphics/RectF;->intersect(FFFF)Z

    const/high16 v9, -0x40800000

    cmpl-float v9, v0, v9

    if-eqz v9, :cond_1

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v9

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    div-float/2addr v9, v10

    cmpl-float v9, v9, v0

    if-lez v9, :cond_3

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v9

    mul-float v7, v9, v0

    iget v9, v4, Landroid/graphics/RectF;->left:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v9, v10

    sub-float/2addr v9, v7

    const/high16 v10, 0x3f000000

    mul-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v7

    iput v9, v4, Landroid/graphics/RectF;->right:F

    :cond_1
    :goto_1
    iget v9, v4, Landroid/graphics/RectF;->left:F

    int-to-float v10, v8

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->right:F

    int-to-float v10, v8

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->right:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    int-to-float v10, v2

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->top:F

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v2

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    return-object v4

    :cond_2
    div-float v6, v5, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v9

    div-float v1, v9, v0

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v9, v10

    sub-float/2addr v9, v1

    const/high16 v10, 0x3f000000

    mul-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->top:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v1

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/media/FaceDetector;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const/4 v4, 0x3

    invoke-direct {v1, v2, v3, v4}, Landroid/media/FaceDetector;-><init>(III)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    invoke-virtual {v1, v0, v2}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v2

    iput v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$1300(Lcom/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v3}, Lcom/android/gallery3d/ui/CropView;->access$1300(Lcom/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public updateFaces()V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    if-le v2, v3, :cond_1

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$1400(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    aget-object v3, v3, v0

    invoke-direct {p0, v3}, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/CropView$FaceHighlightView;->addFace(Landroid/graphics/RectF;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$1400(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$800(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/app/GalleryActivity;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/gallery3d/app/GalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c01de

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :cond_1
    iget v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$1400(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$500(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    aget-object v3, v3, v4

    invoke-direct {p0, v3}, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;->setRectangle(Landroid/graphics/RectF;)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$500(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$500(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;->setInitRectangle()V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/android/gallery3d/ui/CropView;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView;->access$500(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    goto :goto_1
.end method
