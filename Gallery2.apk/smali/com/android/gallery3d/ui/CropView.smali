.class public Lcom/android/gallery3d/ui/CropView;
.super Lcom/android/gallery3d/ui/GLView;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/ui/CropView$DetectFaceTask;,
        Lcom/android/gallery3d/ui/CropView$HighlightRectangle;,
        Lcom/android/gallery3d/ui/CropView$AnimationController;,
        Lcom/android/gallery3d/ui/CropView$FaceHighlightView;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION:I = 0x4e2

.field private static final ANIMATION_TRIGGER:I = 0x40

.field private static final COLOR_FACE_OUTLINE:I = -0x1000000

.field private static final COLOR_OUTLINE:I = -0xff7501

.field private static final FACE_EYE_RATIO:F = 2.0f

.field private static final FACE_PIXEL_COUNT:I = 0x1d4c0

.field private static final MAX_FACE_COUNT:I = 0x3

.field private static final MAX_SELECTION_RATIO:F = 0.8f

.field private static final MIN_SELECTION_LENGTH:F = 16.0f

.field private static final MIN_SELECTION_RATIO:F = 0.4f

.field private static final MOVE_BLOCK:I = 0x10

.field private static final MOVE_BOTTOM:I = 0x8

.field private static final MOVE_LEFT:I = 0x1

.field private static final MOVE_RIGHT:I = 0x4

.field private static final MOVE_TOP:I = 0x2

.field private static final MSG_UPDATE_FACES:I = 0x1

.field private static final OUTLINE_WIDTH:F = 3.0f

.field private static final SELECTION_RATIO:F = 0.6f

.field private static final SIZE_UNKNOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "CropView"

.field private static final TOUCH_TOLERANCE:I = 0x1e

.field public static final UNSPECIFIED:F = -1.0f

.field private static final mIsStereoDisplaySupported:Z


# instance fields
.field private mActivity:Lcom/android/gallery3d/app/GalleryActivity;

.field private mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

.field private mAspectRatio:F

.field private mFaceDetectionView:Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

.field private mFacePaint:Lcom/android/gallery3d/ui/GLPaint;

.field private mFirstImageHeight:I

.field private mFirstImageWidth:I

.field private mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

.field private mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

.field private mImageHeight:I

.field private mImageRotation:I

.field private mImageView:Lcom/android/gallery3d/ui/TileImageView;

.field private mImageWidth:I

.field private mMainHandler:Landroid/os/Handler;

.field private mPaint:Lcom/android/gallery3d/ui/GLPaint;

.field private mSecondImageHeight:I

.field private mSecondImageWidth:I

.field private mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

.field private mSpotlightRatioX:F

.field private mSpotlightRatioY:F

.field private mStereoMode:Z

.field private mStereoWallpaperMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/GalleryActivity;)V
    .locals 5
    .param p1    # Lcom/android/gallery3d/app/GalleryActivity;

    const/4 v4, 0x1

    const/high16 v3, 0x40400000

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/android/gallery3d/ui/GLView;-><init>()V

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mAspectRatio:F

    iput v2, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    iput v2, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    new-instance v0, Lcom/android/gallery3d/ui/CropView$AnimationController;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/ui/CropView$AnimationController;-><init>(Lcom/android/gallery3d/ui/CropView;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    new-instance v0, Lcom/android/gallery3d/ui/GLPaint;

    invoke-direct {v0}, Lcom/android/gallery3d/ui/GLPaint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mPaint:Lcom/android/gallery3d/ui/GLPaint;

    new-instance v0, Lcom/android/gallery3d/ui/GLPaint;

    invoke-direct {v0}, Lcom/android/gallery3d/ui/GLPaint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFacePaint:Lcom/android/gallery3d/ui/GLPaint;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/ui/CropView;->mStereoWallpaperMode:Z

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageWidth:I

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageHeight:I

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageWidth:I

    iput v1, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageHeight:I

    iput-object p1, p0, Lcom/android/gallery3d/ui/CropView;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    new-instance v0, Lcom/android/gallery3d/ui/TileImageView;

    invoke-direct {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;-><init>(Lcom/android/gallery3d/app/GalleryContext;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    sget-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/gallery3d/ui/TileImageView;

    invoke-direct {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;-><init>(Lcom/android/gallery3d/app/GalleryContext;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0, v4}, Lcom/android/gallery3d/ui/TileImageView;->setStereoIndex(I)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    new-instance v0, Lcom/android/gallery3d/ui/TileImageView;

    invoke-direct {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;-><init>(Lcom/android/gallery3d/app/GalleryContext;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/TileImageView;->setStereoIndex(I)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    :cond_0
    new-instance v0, Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/ui/CropView$FaceHighlightView;-><init>(Lcom/android/gallery3d/ui/CropView;Lcom/android/gallery3d/ui/CropView$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    new-instance v0, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;-><init>(Lcom/android/gallery3d/ui/CropView;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/GLView;->addComponent(Lcom/android/gallery3d/ui/GLView;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v0, v4}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mPaint:Lcom/android/gallery3d/ui/GLPaint;

    const v1, -0xff7501

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/GLPaint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mPaint:Lcom/android/gallery3d/ui/GLPaint;

    invoke-virtual {v0, v3}, Lcom/android/gallery3d/ui/GLPaint;->setLineWidth(F)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFacePaint:Lcom/android/gallery3d/ui/GLPaint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/GLPaint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFacePaint:Lcom/android/gallery3d/ui/GLPaint;

    invoke-virtual {v0, v3}, Lcom/android/gallery3d/ui/GLPaint;->setLineWidth(F)V

    new-instance v0, Lcom/android/gallery3d/ui/CropView$1;

    invoke-interface {p1}, Lcom/android/gallery3d/app/GalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/ui/CropView$1;-><init>(Lcom/android/gallery3d/ui/CropView;Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    return v0
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    return v0
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/GLPaint;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mPaint:Lcom/android/gallery3d/ui/GLPaint;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/ui/CropView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$FaceHighlightView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$AnimationController;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/GLPaint;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFacePaint:Lcom/android/gallery3d/ui/GLPaint;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/CropView;->mStereoWallpaperMode:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/ui/CropView$HighlightRectangle;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/ui/CropView;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/gallery3d/ui/CropView;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/gallery3d/ui/CropView;)Lcom/android/gallery3d/app/GalleryActivity;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mActivity:Lcom/android/gallery3d/app/GalleryActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/CropView;

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mAspectRatio:F

    return v0
.end method

.method private setImageViewPosition(IIF)Z
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # F

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    iget v2, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    iget v3, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/ui/CropView;->setTileViewPosition(Lcom/android/gallery3d/ui/TileImageView;IIIIF)Z

    move-result v7

    sget-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    iget v2, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageWidth:I

    iget v3, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageHeight:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/ui/CropView;->setTileViewPosition(Lcom/android/gallery3d/ui/TileImageView;IIIIF)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v7, :cond_3

    :cond_0
    move v7, v9

    :goto_0
    iget-object v1, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    iget v2, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageWidth:I

    iget v3, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageHeight:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/ui/CropView;->setTileViewPosition(Lcom/android/gallery3d/ui/TileImageView;IIIIF)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v7, :cond_4

    :cond_1
    move v7, v9

    :cond_2
    :goto_1
    return v7

    :cond_3
    move v7, v8

    goto :goto_0

    :cond_4
    move v7, v8

    goto :goto_1
.end method

.method private setTileViewPosition(Lcom/android/gallery3d/ui/TileImageView;IIIIF)Z
    .locals 5
    .param p1    # Lcom/android/gallery3d/ui/TileImageView;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F

    sub-int v0, p2, p4

    sub-int v1, p3, p5

    iget v2, p0, Lcom/android/gallery3d/ui/CropView;->mImageRotation:I

    sparse-switch v2, :sswitch_data_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :sswitch_0
    const/4 v3, 0x0

    invoke-virtual {p1, p4, p5, p6, v3}, Lcom/android/gallery3d/ui/TileImageView;->setPosition(IIFI)Z

    move-result v3

    :goto_0
    return v3

    :sswitch_1
    const/16 v3, 0x5a

    invoke-virtual {p1, p5, v0, p6, v3}, Lcom/android/gallery3d/ui/TileImageView;->setPosition(IIFI)Z

    move-result v3

    goto :goto_0

    :sswitch_2
    const/16 v3, 0xb4

    invoke-virtual {p1, v0, v1, p6, v3}, Lcom/android/gallery3d/ui/TileImageView;->setPosition(IIFI)Z

    move-result v3

    goto :goto_0

    :sswitch_3
    const/16 v3, 0x10e

    invoke-virtual {p1, v1, p4, p6, v3}, Lcom/android/gallery3d/ui/TileImageView;->setPosition(IIFI)Z

    move-result v3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public detectFaces(Landroid/graphics/Bitmap;)V
    .locals 13
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v12, 0x2

    const/4 v11, 0x0

    iget v4, p0, Lcom/android/gallery3d/ui/CropView;->mImageRotation:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const v8, 0x47ea6000

    mul-int v9, v7, v3

    int-to-float v9, v9

    div-float/2addr v8, v9

    invoke-static {v8}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v5

    div-int/lit8 v8, v4, 0x5a

    and-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_0

    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-float v8, v4

    div-int/lit8 v9, v6, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v2, 0x2

    int-to-float v10, v10

    invoke-virtual {v0, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    int-to-float v8, v6

    int-to-float v9, v7

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v3

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8, v12}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v11, v11, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_0
    new-instance v8, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;

    invoke-direct {v8, p0, v1}, Lcom/android/gallery3d/ui/CropView$DetectFaceTask;-><init>(Lcom/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    div-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    div-int/lit8 v9, v2, 0x2

    int-to-float v9, v9

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    int-to-float v8, v4

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v8, v2

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    neg-int v9, v6

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    int-to-float v8, v6

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v7

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8, v12}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v11, v11, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public getCropRectangle()Landroid/graphics/RectF;
    .locals 7

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-static {v2}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;->access$100(Lcom/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method public getImageHeight()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    return v0
.end method

.method public getStereoMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/CropView;->mStereoMode:Z

    return v0
.end method

.method public initializeHighlightRectangle()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;->setInitRectangle()V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/android/gallery3d/ui/CropView$FaceHighlightView;

    invoke-virtual {v2, v3, v3, v1, v0}, Lcom/android/gallery3d/ui/GLView;->layout(IIII)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2, v3, v3, v1, v0}, Lcom/android/gallery3d/ui/GLView;->layout(IIII)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v2, v3, v3, v1, v0}, Lcom/android/gallery3d/ui/GLView;->layout(IIII)V

    sget-boolean v2, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v2, v3, v3, v1, v0}, Lcom/android/gallery3d/ui/GLView;->layout(IIII)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v2, v3, v3, v1, v0}, Lcom/android/gallery3d/ui/GLView;->layout(IIII)V

    :cond_0
    iget v2, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/CropView$AnimationController;->initialize()V

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/GLView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/gallery3d/ui/CropView;->mStereoWallpaperMode:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    iget-object v3, p0, Lcom/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-static {v3}, Lcom/android/gallery3d/ui/CropView$HighlightRectangle;->access$100(Lcom/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/CropView$AnimationController;->parkNow(Landroid/graphics/RectF;)V

    :cond_1
    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->freeTextures()V

    sget-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->freeTextures()V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->freeTextures()V

    :cond_0
    return-void
.end method

.method public render(Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    invoke-static {}, Lcom/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/anim/Animation;->calculate(J)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/ui/CropView$AnimationController;->getCenterX()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/CropView$AnimationController;->getCenterY()I

    move-result v2

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/CropView$AnimationController;->getScale()F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/gallery3d/ui/CropView;->setImageViewPosition(IIF)Z

    invoke-super {p0, p1}, Lcom/android/gallery3d/ui/GLView;->render(Lcom/android/gallery3d/ui/GLCanvas;)V

    return-void
.end method

.method public renderBackground(Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->clearBuffer()V

    return-void
.end method

.method public resume()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->prepareTextures()V

    sget-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->prepareTextures()V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageView;->prepareTextures()V

    :cond_0
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/ui/CropView;->mAspectRatio:F

    return-void
.end method

.method public setDataModel(Lcom/android/gallery3d/ui/TileImageView$Model;I)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/ui/TileImageView$Model;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v1, 0x1

    div-int/lit8 v0, p2, 0x5a

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    invoke-interface {p1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    :goto_0
    iput p2, p0, Lcom/android/gallery3d/ui/CropView;->mImageRotation:I

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;->setModel(Lcom/android/gallery3d/ui/TileImageView$Model;)V

    sget-boolean v0, Lcom/android/gallery3d/ui/CropView;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;->setModel(Lcom/android/gallery3d/ui/TileImageView$Model;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondTileView:Lcom/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageView;->setModel(Lcom/android/gallery3d/ui/TileImageView$Model;)V

    div-int/lit8 v0, p2, 0x5a

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageHeight(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageWidth:I

    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageWidth(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageHeight:I

    invoke-interface {p1, v2}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageHeight(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageWidth:I

    invoke-interface {p1, v2}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageWidth(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageHeight:I

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/gallery3d/ui/CropView;->mAnimation:Lcom/android/gallery3d/ui/CropView$AnimationController;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/CropView$AnimationController;->initialize()V

    return-void

    :cond_1
    invoke-interface {p1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageWidth:I

    invoke-interface {p1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mImageHeight:I

    goto :goto_0

    :cond_2
    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageWidth(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageWidth:I

    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageHeight(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mFirstImageHeight:I

    invoke-interface {p1, v2}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageWidth(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageWidth:I

    invoke-interface {p1, v2}, Lcom/android/gallery3d/ui/TileImageView$Model;->getStereoImageHeight(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/CropView;->mSecondImageHeight:I

    goto :goto_1
.end method

.method public setSpotlightRatio(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    iput p1, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    iput p2, p0, Lcom/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    return-void
.end method

.method public setStereoMode(Z)V
    .locals 4
    .param p1    # Z

    iget-boolean v1, p0, Lcom/android/gallery3d/ui/CropView;->mStereoMode:Z

    if-ne v1, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "CropView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setStereoMode(stereoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/ui/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/gallery3d/ui/CropView;->mStereoMode:Z

    iget-boolean v1, p0, Lcom/android/gallery3d/ui/CropView;->mStereoMode:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    :goto_1
    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/gallery3d/ui/GLRoot;->setStereoLayout(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setStereoWallpaperMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/ui/CropView;->mStereoWallpaperMode:Z

    return-void
.end method
