.class public Lcom/android/gallery3d/ui/NinePatchTexture;
.super Lcom/android/gallery3d/ui/ResourceTexture;
.source "NinePatchTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/ui/NinePatchTexture$1;,
        Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NinePatchTexture"


# instance fields
.field private mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

.field private mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache",
            "<",
            "Lcom/android/gallery3d/ui/NinePatchInstance;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;-><init>(Lcom/android/gallery3d/ui/NinePatchTexture$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    return-void
.end method

.method private findInstance(Lcom/android/gallery3d/ui/GLCanvas;II)Lcom/android/gallery3d/ui/NinePatchInstance;
    .locals 4
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I

    move v1, p2

    shl-int/lit8 v3, v1, 0x10

    or-int v1, v3, p3

    iget-object v3, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v3, v1}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/NinePatchInstance;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/gallery3d/ui/NinePatchInstance;

    invoke-direct {v0, p0, p2, p3}, Lcom/android/gallery3d/ui/NinePatchInstance;-><init>(Lcom/android/gallery3d/ui/NinePatchTexture;II)V

    iget-object v3, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v3, v1, v0}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->put(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/ui/NinePatchInstance;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/ui/NinePatchInstance;->recycle(Lcom/android/gallery3d/ui/GLCanvas;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/ResourceTexture;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->clear()V

    :cond_0
    if-eqz p4, :cond_1

    if-eqz p5, :cond_1

    invoke-direct {p0, p1, p4, p5}, Lcom/android/gallery3d/ui/NinePatchTexture;->findInstance(Lcom/android/gallery3d/ui/GLCanvas;II)Lcom/android/gallery3d/ui/NinePatchInstance;

    move-result-object v0

    invoke-virtual {v0, p1, p0, p2, p3}, Lcom/android/gallery3d/ui/NinePatchInstance;->draw(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/ui/NinePatchTexture;II)V

    :cond_1
    return-void
.end method

.method public getNinePatchChunk()Lcom/android/gallery3d/ui/NinePatchChunk;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/NinePatchTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    return-object v0
.end method

.method public getPaddings()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/NinePatchTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    iget-object v0, v0, Lcom/android/gallery3d/ui/NinePatchChunk;->mPaddings:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 6

    iget-object v3, p0, Lcom/android/gallery3d/ui/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/ui/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iget-object v3, p0, Lcom/android/gallery3d/ui/ResourceTexture;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/android/gallery3d/ui/ResourceTexture;->mResId:I

    invoke-static {v3, v4, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/ui/UploadedTexture;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/android/gallery3d/ui/BasicTexture;->setSize(II)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v3, 0x0

    :goto_0
    iput-object v3, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    iget-object v3, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mChunk:Lcom/android/gallery3d/ui/NinePatchChunk;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid nine-patch image: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/gallery3d/ui/ResourceTexture;->mResId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/gallery3d/ui/NinePatchChunk;->deserialize([B)Lcom/android/gallery3d/ui/NinePatchChunk;

    move-result-object v3

    goto :goto_0
.end method

.method public recycle()V
    .locals 5

    invoke-super {p0}, Lcom/android/gallery3d/ui/ResourceTexture;->recycle()V

    iget-object v0, p0, Lcom/android/gallery3d/ui/BasicTexture;->mCanvasRef:Lcom/android/gallery3d/ui/GLCanvas;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v4, v1}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/ui/NinePatchInstance;

    invoke-virtual {v2, v0}, Lcom/android/gallery3d/ui/NinePatchInstance;->recycle(Lcom/android/gallery3d/ui/GLCanvas;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/android/gallery3d/ui/NinePatchTexture;->mInstanceCache:Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/NinePatchTexture$SmallCache;->clear()V

    goto :goto_0
.end method
