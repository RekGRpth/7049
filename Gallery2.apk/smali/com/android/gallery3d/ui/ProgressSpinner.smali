.class public Lcom/android/gallery3d/ui/ProgressSpinner;
.super Ljava/lang/Object;
.source "ProgressSpinner.java"


# static fields
.field private static ROTATE_SPEED_INNER:F

.field private static ROTATE_SPEED_OUTER:F


# instance fields
.field private mAnimationTimestamp:J

.field private final mHeight:I

.field private final mInner:Lcom/android/gallery3d/ui/ResourceTexture;

.field private mInnerDegree:F

.field private final mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

.field private mOuterDegree:F

.field private final mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x3e9dfd13

    sput v0, Lcom/android/gallery3d/ui/ProgressSpinner;->ROTATE_SPEED_OUTER:F

    const v0, -0x41ad593c

    sput v0, Lcom/android/gallery3d/ui/ProgressSpinner;->ROTATE_SPEED_INNER:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    iput v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f020161

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f020160

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    iget-object v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/ResourceTexture;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/ResourceTexture;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mWidth:I

    iget-object v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/ResourceTexture;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/ResourceTexture;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mHeight:I

    return-void
.end method


# virtual methods
.method public draw(Lcom/android/gallery3d/ui/GLCanvas;II)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I

    const/high16 v8, 0x3f800000

    const/high16 v7, 0x43b40000

    const/4 v6, 0x0

    invoke-static {}, Lcom/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iput-wide v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    :cond_0
    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    iget-wide v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    sub-long v3, v0, v3

    long-to-float v3, v3

    sget v4, Lcom/android/gallery3d/ui/ProgressSpinner;->ROTATE_SPEED_OUTER:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    iget-wide v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    sub-long v3, v0, v3

    long-to-float v3, v3

    sget v4, Lcom/android/gallery3d/ui/ProgressSpinner;->ROTATE_SPEED_INNER:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    iput-wide v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    cmpl-float v2, v2, v7

    if-lez v2, :cond_1

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    sub-float/2addr v2, v7

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    :cond_1
    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    add-float/2addr v2, v7

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    :cond_2
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->save(I)V

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p2

    int-to-float v2, v2

    iget v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mHeight:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p3

    int-to-float v3, v3

    invoke-interface {p1, v2, v3}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    invoke-interface {p1, v2, v6, v6, v8}, Lcom/android/gallery3d/ui/GLCanvas;->rotate(FFFF)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    iget-object v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/ResourceTexture;->getWidth()I

    move-result v3

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuter:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/ResourceTexture;->getHeight()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, p1, v3, v4}, Lcom/android/gallery3d/ui/ResourceTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    iget v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    iget v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    sub-float/2addr v2, v3

    invoke-interface {p1, v2, v6, v6, v8}, Lcom/android/gallery3d/ui/GLCanvas;->rotate(FFFF)V

    iget-object v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    iget-object v3, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/ResourceTexture;->getWidth()I

    move-result v3

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInner:Lcom/android/gallery3d/ui/ResourceTexture;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/ResourceTexture;->getHeight()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, p1, v3, v4}, Lcom/android/gallery3d/ui/ResourceTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    return-void
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mWidth:I

    return v0
.end method

.method public startAnimation()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mAnimationTimestamp:J

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mOuterDegree:F

    iput v2, p0, Lcom/android/gallery3d/ui/ProgressSpinner;->mInnerDegree:F

    return-void
.end method
