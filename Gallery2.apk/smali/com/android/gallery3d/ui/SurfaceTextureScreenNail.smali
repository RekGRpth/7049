.class public abstract Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;
.super Ljava/lang/Object;
.source "SurfaceTextureScreenNail.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Lcom/android/gallery3d/ui/ScreenNail;


# static fields
.field protected static final INTERVALS:I = 0x3c

.field private static final TAG:Ljava/lang/String; = "SurfaceTextureScreenNail"

.field private static sFrameListener:Landroid/os/HandlerThread;


# instance fields
.field protected mDebug:Z

.field protected mDebugFlag:I

.field protected mDebugLevel2:Z

.field protected mDebugLevel3:Z

.field protected mDrawFrameCount:I

.field protected mDrawStartTime:J

.field protected mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

.field private mHasTexture:Z

.field private mHeight:I

.field protected mRequestCount:I

.field protected mRequestStartTime:J

.field private final mSourceRect:Landroid/graphics/RectF;

.field private final mSrcTextureMatrixValues:[F

.field protected mStereoMode:I

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private final mTargetRect:Landroid/graphics/RectF;

.field private final mTempMatrix:[F

.field private mTransform:[F

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FrameListener"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTransform:[F

    iput-boolean v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHasTexture:Z

    const-string v0, "cam.debug"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugFlag:I

    iput-boolean v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebug:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel2:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel3:Z

    iput v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    iput v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mRequestCount:I

    iput-wide v4, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mRequestStartTime:J

    iput-wide v4, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawStartTime:J

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSourceRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTargetRect:Landroid/graphics/RectF;

    const/16 v0, 0x20

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTempMatrix:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    iput v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugFlag:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebug:Z

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugFlag:I

    if-le v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel2:Z

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugFlag:I

    const/4 v3, 0x2

    if-le v0, v3, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel3:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private getMatrixString([F)Ljava/lang/String;
    .locals 6
    .param p1    # [F

    if-nez p1, :cond_0

    const-string v5, "empty matrix"

    :goto_0
    return-object v5

    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget v1, v0, v2

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    const-string v5, "__"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private multiplyMatrix([FI)V
    .locals 6
    .param p1    # [F
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTempMatrix:[F

    iget-object v4, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    move-object v2, p1

    move v3, p2

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    const/16 v2, 0x10

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public acquireSurfaceTexture()V
    .locals 3

    new-instance v0, Lcom/android/gallery3d/ui/ExtTexture;

    const v1, 0x8d65

    invoke-direct {v0, v1}, Lcom/android/gallery3d/ui/ExtTexture;-><init>(I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/ui/BasicTexture;->setSize(II)V

    sget-object v0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/ExtTexture;->getId()I

    move-result v1

    sget-object v2, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/SurfaceTexture;-><init>(ILandroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHasTexture:Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V
    .locals 13
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel3:Z

    if-eqz v0, :cond_0

    const-string v0, "SurfaceTextureScreenNail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Preview] SurfaceTextureScreenNail draw, time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHasTexture:Z

    if-nez v0, :cond_2

    monitor-exit p0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTransform:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/android/gallery3d/ui/GLCanvas;->save(I)V

    div-int/lit8 v0, p4, 0x2

    add-int v9, p2, v0

    div-int/lit8 v0, p5, 0x2

    add-int v10, p3, v0

    int-to-float v0, v9

    int-to-float v1, v10

    invoke-interface {p1, v0, v1}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    const/high16 v0, 0x3f800000

    const/high16 v1, -0x40800000

    const/high16 v2, 0x3f800000

    invoke-interface {p1, v0, v1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->scale(FFF)V

    neg-int v0, v9

    int-to-float v0, v0

    neg-int v1, v10

    int-to-float v1, v1

    invoke-interface {p1, v0, v1}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ActivityBase;

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->getStereo3DType()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoLayout()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    if-eqz v0, :cond_3

    if-eqz v12, :cond_3

    const-string v0, "off"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-boolean v0, Lcom/android/camera/Util;->STEREO3D_MODE:Z

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTransform:[F

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->setStereo3DMatrix([F)V

    :cond_5
    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    iget-object v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mTransform:[F

    move-object v0, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/android/gallery3d/ui/GLCanvas;->drawTexture(Lcom/android/gallery3d/ui/BasicTexture;[FIIII)V

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebug:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel2:Z

    if-eqz v0, :cond_6

    const-string v0, "SurfaceTextureScreenNail"

    const-string v1, "[Preview] GLCanvas drawing Frame"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    rem-int/lit8 v0, v0, 0x3c

    if-nez v0, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawStartTime:J

    sub-long v0, v7, v0

    long-to-int v11, v0

    const-string v0, "SurfaceTextureScreenNail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Preview] Drawing frame, fps = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    int-to-float v2, v2

    const/high16 v3, 0x447a0000

    mul-float/2addr v2, v3

    int-to-float v3, v11

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in last "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millisecond."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v7, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawStartTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDrawFrameCount:I

    :cond_7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel3:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoPassId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const-string v0, "SurfaceTextureScreenNail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Preview] SurfaceTextureScreenNail draw done, time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public draw(Lcom/android/gallery3d/ui/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # Landroid/graphics/RectF;
    .param p3    # Landroid/graphics/RectF;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public fullHandlerCapacity()V
    .locals 2

    sget-object v0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    const/16 v1, -0x13

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadPriority(II)V

    return-void
.end method

.method public abstract getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    return v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    return v0
.end method

.method public abstract noDraw()V
.end method

.method public normalHandlerCapacity()V
    .locals 2

    sget-object v0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->sFrameListener:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadPriority(II)V

    return-void
.end method

.method public abstract onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
.end method

.method public abstract recycle()V
.end method

.method public releaseSurfaceTexture()V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHasTexture:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/ExtTexture;->recycle()V

    iput-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mExtTexture:Lcom/android/gallery3d/ui/ExtTexture;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    iput-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setSize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    iput p2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    return-void
.end method

.method protected setStereo3DMatrix([F)V
    .locals 7
    .param p1    # [F

    const/4 v3, 0x3

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000

    const/4 v2, 0x2

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSourceRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/gallery3d/ui/GLRoot;->getStereoPassId()I

    move-result v1

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    if-ne v2, v1, :cond_2

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    int-to-float v3, v3

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_0
    :goto_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    aput v2, v1, v6

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    const/4 v2, 0x5

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    aput v3, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    const/16 v2, 0xa

    aput v5, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    const/16 v2, 0xc

    iget v3, v0, Landroid/graphics/RectF;->left:F

    aput v3, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    const/16 v2, 0xd

    iget v3, v0, Landroid/graphics/RectF;->top:F

    aput v3, v1, v2

    iget-object v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    const/16 v2, 0xf

    aput v5, v1, v2

    iget-boolean v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mDebugLevel3:Z

    if-eqz v1, :cond_1

    const-string v1, "SurfaceTextureScreenNail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mStereoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PassId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getContext()Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/gallery3d/ui/GLRoot;->getStereoPassId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mSrcTextureMatrixValues="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mSrcTextureMatrixValues:[F

    invoke-direct {p0, v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getMatrixString([F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, p1, v6}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->multiplyMatrix([FI)V

    return-void

    :cond_2
    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    if-ne v3, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0

    :cond_3
    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    if-ne v2, v1, :cond_4

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0

    :cond_4
    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mStereoMode:I

    if-ne v3, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->mHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0
.end method
