.class public Lcom/android/gallery3d/ui/TextureUploader;
.super Ljava/lang/Object;
.source "TextureUploader.java"

# interfaces
.implements Lcom/android/gallery3d/ui/GLRoot$OnGLIdleListener;


# static fields
.field private static final INIT_CAPACITY:I = 0x40

.field private static final QUOTA_PER_FRAME:I = 0x1


# instance fields
.field private final mBgTextures:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/android/gallery3d/ui/UploadedTexture;",
            ">;"
        }
    .end annotation
.end field

.field private final mFgTextures:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/android/gallery3d/ui/UploadedTexture;",
            ">;"
        }
    .end annotation
.end field

.field private final mGLRoot:Lcom/android/gallery3d/ui/GLRoot;

.field private transient mIsQueued:Z


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/ui/GLRoot;)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/ui/GLRoot;

    const/16 v1, 0x40

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mIsQueued:Z

    iput-object p1, p0, Lcom/android/gallery3d/ui/TextureUploader;->mGLRoot:Lcom/android/gallery3d/ui/GLRoot;

    return-void
.end method

.method private queueSelfIfNeed()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mIsQueued:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mIsQueued:Z

    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mGLRoot:Lcom/android/gallery3d/ui/GLRoot;

    invoke-interface {v0, p0}, Lcom/android/gallery3d/ui/GLRoot;->addOnGLIdleListener(Lcom/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    goto :goto_0
.end method

.method private upload(Lcom/android/gallery3d/ui/GLCanvas;Ljava/util/ArrayDeque;IZ)I
    .locals 3
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/ui/GLCanvas;",
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/android/gallery3d/ui/UploadedTexture;",
            ">;IZ)I"
        }
    .end annotation

    const/4 v2, 0x0

    :goto_0
    if-lez p3, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    monitor-exit p0

    :cond_0
    return p3

    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/UploadedTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/UploadedTexture;->setIsUploading(Z)V

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/UploadedTexture;->isContentValid()Z

    move-result v1

    if-eqz v1, :cond_2

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/UploadedTexture;->updateContent(Lcom/android/gallery3d/ui/GLCanvas;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p4, :cond_3

    invoke-virtual {v0, p1, v2, v2}, Lcom/android/gallery3d/ui/BasicTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    :cond_3
    add-int/lit8 p3, p3, -0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addBgTexture(Lcom/android/gallery3d/ui/UploadedTexture;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/UploadedTexture;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/android/gallery3d/ui/UploadedTexture;->isContentValid()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/gallery3d/ui/UploadedTexture;->setIsUploading(Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/ui/TextureUploader;->queueSelfIfNeed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addFgTexture(Lcom/android/gallery3d/ui/UploadedTexture;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/UploadedTexture;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/android/gallery3d/ui/UploadedTexture;->isContentValid()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/gallery3d/ui/UploadedTexture;->setIsUploading(Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/ui/TextureUploader;->queueSelfIfNeed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 2

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/UploadedTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/UploadedTexture;->setIsUploading(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/UploadedTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/UploadedTexture;->setIsUploading(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public onGLIdle(Lcom/android/gallery3d/ui/GLCanvas;Z)Z
    .locals 4
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    invoke-direct {p0, p1, v3, v0, v1}, Lcom/android/gallery3d/ui/TextureUploader;->upload(Lcom/android/gallery3d/ui/GLCanvas;Ljava/util/ArrayDeque;IZ)I

    move-result v0

    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/ui/TextureUploader;->mGLRoot:Lcom/android/gallery3d/ui/GLRoot;

    invoke-interface {v3}, Lcom/android/gallery3d/ui/GLRoot;->requestRender()V

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    invoke-direct {p0, p1, v3, v0, v2}, Lcom/android/gallery3d/ui/TextureUploader;->upload(Lcom/android/gallery3d/ui/GLCanvas;Ljava/util/ArrayDeque;IZ)I

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/gallery3d/ui/TextureUploader;->mFgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/ui/TextureUploader;->mBgTextures:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/android/gallery3d/ui/TextureUploader;->mIsQueued:Z

    iget-boolean v1, p0, Lcom/android/gallery3d/ui/TextureUploader;->mIsQueued:Z

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
