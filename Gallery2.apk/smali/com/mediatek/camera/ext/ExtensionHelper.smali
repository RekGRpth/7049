.class public Lcom/mediatek/camera/ext/ExtensionHelper;
.super Ljava/lang/Object;
.source "ExtensionHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtensionHelper"

.field private static sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ensureCameraExtension(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v0, "ExtensionHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter ensureCameraExtension sCameraExtension = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/mediatek/camera/ext/ICameraExtension;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {v0, v1, v2}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/camera/ext/ICameraExtension;

    sput-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/camera/ext/CameraExtension;

    invoke-direct {v0}, Lcom/mediatek/camera/ext/CameraExtension;-><init>()V

    sput-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    :cond_1
    :goto_1
    const-string v0, "ExtensionHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exit ensureCameraExtension sCameraExtension = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/camera/ext/CameraExtension;

    invoke-direct {v0}, Lcom/mediatek/camera/ext/CameraExtension;-><init>()V

    sput-object v0, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    goto :goto_1

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-nez v1, :cond_2

    new-instance v1, Lcom/mediatek/camera/ext/CameraExtension;

    invoke-direct {v1}, Lcom/mediatek/camera/ext/CameraExtension;-><init>()V

    sput-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    :cond_2
    throw v0
.end method

.method public static getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;
    .locals 3

    sget-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Please call ensureCameraExtension before getFeatureExtension"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    sget-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    invoke-interface {v1}, Lcom/mediatek/camera/ext/ICameraExtension;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v0

    return-object v0
.end method

.method public static getPathPicker()Lcom/mediatek/camera/ext/ISavingPath;
    .locals 3

    sget-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Please call ensureCameraExtension before getPathPicker"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    sget-object v1, Lcom/mediatek/camera/ext/ExtensionHelper;->sCameraExtension:Lcom/mediatek/camera/ext/ICameraExtension;

    invoke-interface {v1}, Lcom/mediatek/camera/ext/ICameraExtension;->getSavingPath()Lcom/mediatek/camera/ext/ISavingPath;

    move-result-object v0

    return-object v0
.end method
