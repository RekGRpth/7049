.class Lcom/mediatek/camera/mav/MavActivity$3;
.super Landroid/os/Handler;
.source "MavActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/mav/MavActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/mav/MavActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/camera/mav/MavActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    const-string v0, "MavActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage what= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$300(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$400(Lcom/mediatek/camera/mav/MavActivity;)Lcom/android/camera/RotateDialogController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$500(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/mav/MavActivity;->hidePostControlAlert()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$600(Lcom/mediatek/camera/mav/MavActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$800(Lcom/mediatek/camera/mav/MavActivity;)Lcom/android/camera/CameraScreenNail;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$700(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0, v3}, Lcom/mediatek/camera/mav/MavActivity;->access$902(Lcom/mediatek/camera/mav/MavActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/camera/Thumbnail;

    invoke-static {v1, v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1002(Lcom/mediatek/camera/mav/MavActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1100(Lcom/mediatek/camera/mav/MavActivity;)V

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1200(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1300(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$900(Lcom/mediatek/camera/mav/MavActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0, v3}, Lcom/mediatek/camera/mav/MavActivity;->access$902(Lcom/mediatek/camera/mav/MavActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/camera/Thumbnail;

    invoke-static {v1, v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1402(Lcom/mediatek/camera/mav/MavActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1500(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$3;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1200(Lcom/mediatek/camera/mav/MavActivity;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
