.class Lcom/mediatek/camera/mav/MavActivity$4;
.super Ljava/lang/Thread;
.source "MavActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/camera/mav/MavActivity;->onCaptureDone(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/mav/MavActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/camera/mav/MavActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$1900(Lcom/mediatek/camera/mav/MavActivity;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v4}, Lcom/mediatek/camera/mav/MavActivity;->access$2000(Lcom/mediatek/camera/mav/MavActivity;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/mediatek/camera/mav/MavActivity;->access$2100(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/camera/Storage;->generateMpoFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/4 v12, 0x0

    :try_start_0
    new-instance v13, Landroid/media/ExifInterface;

    invoke-direct {v13, v14}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v12, v13

    :goto_0
    invoke-static {v12}, Lcom/mediatek/camera/mav/MavActivity;->getExifOrientation(Landroid/media/ExifInterface;)I

    move-result v7

    const-string v2, "ImageWidth"

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v8

    const-string v2, "ImageLength"

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v9

    const-string v2, "MavActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCaptureDone.run orientation "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " w * h:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v4}, Lcom/mediatek/camera/mav/MavActivity;->access$2000(Lcom/mediatek/camera/mav/MavActivity;)J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v10, 0x1

    invoke-static/range {v2 .. v10}, Lcom/android/camera/Storage;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;IIII)Landroid/net/Uri;

    move-result-object v18

    const/16 v17, 0x0

    if-eqz v18, :cond_0

    int-to-double v4, v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$2200(Lcom/mediatek/camera/mav/MavActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-double v0, v2

    move-wide/from16 v20, v0

    div-double v4, v4, v20

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v19, v0

    int-to-double v4, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$2200(Lcom/mediatek/camera/mav/MavActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-double v0, v2

    move-wide/from16 v20, v0

    div-double v4, v4, v20

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v15, v4

    move/from16 v0, v19

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v14, v7, v0, v1}, Lcom/android/camera/Thumbnail;->createThumbnail(Ljava/lang/String;IILandroid/net/Uri;)Lcom/android/camera/Thumbnail;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/mediatek/camera/mav/MavActivity;->access$2300(Lcom/mediatek/camera/mav/MavActivity;Lcom/android/camera/Thumbnail;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/android/camera/Util;->broadcastNewPicture(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$2400(Lcom/mediatek/camera/mav/MavActivity;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/camera/mav/MavActivity$4;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v4}, Lcom/mediatek/camera/mav/MavActivity;->access$2400(Lcom/mediatek/camera/mav/MavActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v4, v5, v6, v10, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catch_0
    move-exception v11

    const-string v2, "MavActivity"

    const-string v4, "cannot read exif"

    invoke-static {v2, v4, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method
