.class Lcom/mediatek/camera/mav/MavActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "MavActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/mav/MavActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/mav/MavActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/camera/mav/MavActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MavActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received intent action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-static {v4}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$1200(Lcom/mediatek/camera/mav/MavActivity;)V

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$2600(Lcom/mediatek/camera/mav/MavActivity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/android/camera/Storage;->MOUNT_POINT:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$500(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/camera/mav/MavActivity;->access$2700(Lcom/mediatek/camera/mav/MavActivity;Z)V

    :cond_3
    invoke-static {v4}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$1200(Lcom/mediatek/camera/mav/MavActivity;)V

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/camera/mav/MavActivity;->access$2802(Lcom/mediatek/camera/mav/MavActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$2900(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$5;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$1200(Lcom/mediatek/camera/mav/MavActivity;)V

    goto :goto_0
.end method
