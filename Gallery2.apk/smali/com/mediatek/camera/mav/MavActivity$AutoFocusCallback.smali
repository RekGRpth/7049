.class final Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;
.super Ljava/lang/Object;
.source "MavActivity.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/mav/MavActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/mav/MavActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/camera/mav/MavActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/camera/mav/MavActivity;Lcom/mediatek/camera/mav/MavActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/camera/mav/MavActivity;
    .param p2    # Lcom/mediatek/camera/mav/MavActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;-><init>(Lcom/mediatek/camera/mav/MavActivity;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 6
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$3200(Lcom/mediatek/camera/mav/MavActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "MavActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAutoFocusTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v4}, Lcom/mediatek/camera/mav/MavActivity;->access$3300(Lcom/mediatek/camera/mav/MavActivity;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavActivity$AutoFocusCallback;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v0}, Lcom/mediatek/camera/mav/MavActivity;->access$000(Lcom/mediatek/camera/mav/MavActivity;)Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/camera/FocusManager;->onAutoFocus(Z)V

    goto :goto_0
.end method
