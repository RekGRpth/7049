.class Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "MavActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/mav/MavActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOrientationEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/mav/MavActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/camera/mav/MavActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 3
    .param p1    # I

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$1600(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v2

    invoke-static {p1, v2}, Lcom/android/camera/Util;->roundOrientation(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/camera/mav/MavActivity;->access$1602(Lcom/mediatek/camera/mav/MavActivity;I)I

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$1600(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v2

    add-int/2addr v1, v2

    rem-int/lit16 v0, v1, 0x168

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1}, Lcom/mediatek/camera/mav/MavActivity;->access$1700(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v1, v0}, Lcom/mediatek/camera/mav/MavActivity;->access$1702(Lcom/mediatek/camera/mav/MavActivity;I)I

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/mav/MavActivity;

    invoke-static {v2}, Lcom/mediatek/camera/mav/MavActivity;->access$1700(Lcom/mediatek/camera/mav/MavActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/camera/mav/MavActivity;->access$1800(Lcom/mediatek/camera/mav/MavActivity;I)V

    goto :goto_0
.end method
