.class public Lcom/mediatek/camera/mav/MavController;
.super Ljava/lang/Object;
.source "MavController.java"

# interfaces
.implements Landroid/hardware/Camera$MAVCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/camera/mav/MavController$CaptureEventListener;
    }
.end annotation


# static fields
.field static ANIMATION:Z = false

.field private static final IDLE:I = 0x0

.field private static final MAV_CAPTURE_NUM:I = 0x19

.field private static final MERGING:I = 0x2

.field private static final STARTED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MavController"


# instance fields
.field private lock:Ljava/lang/Object;

.field private mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

.field public mCancelOnClickListener:Landroid/view/View$OnClickListener;

.field private mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

.field private mCurrentNum:I

.field private mHandler:Landroid/os/Handler;

.field private mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

.field private mState:I

.field private mStopProcess:Z

.field private mStopping:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/camera/mav/MavController;->ANIMATION:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/mediatek/camera/mav/MavController$CaptureEventListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    iput-boolean v0, p0, Lcom/mediatek/camera/mav/MavController;->mStopProcess:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/camera/mav/MavController;->lock:Ljava/lang/Object;

    new-instance v0, Lcom/mediatek/camera/mav/MavController$1;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/mav/MavController$1;-><init>(Lcom/mediatek/camera/mav/MavController;)V

    iput-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCancelOnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1}, Lcom/mediatek/camera/mav/MavController;->initializeViews(Landroid/app/Activity;)V

    new-instance v0, Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iput-object p2, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/camera/mav/MavController;)Lcom/mediatek/camera/mav/MavController$CaptureEventListener;
    .locals 1
    .param p0    # Lcom/mediatek/camera/mav/MavController;

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/camera/mav/MavController;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/mav/MavController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/camera/mav/MavController;->doStop(Z)V

    return-void
.end method

.method static synthetic access$202(Lcom/mediatek/camera/mav/MavController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/mav/MavController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/mav/MavController;->mStopping:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/camera/mav/MavController;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/mav/MavController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/camera/mav/MavController;->onHardwareStopped(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/camera/mav/MavController;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/camera/mav/MavController;

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/camera/mav/MavController;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/camera/mav/MavController;

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/camera/mav/MavController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/mav/MavController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/mav/MavController;->mStopProcess:Z

    return p1
.end method

.method private doStart()V
    .locals 2

    const-string v0, "MavController"

    const-string v1, "doStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->startMAV(I)V

    return-void
.end method

.method private doStop(Z)V
    .locals 4
    .param p1    # Z

    const-string v1, "MavController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doStop isMerge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraHolder;->isSameCameraDevice(Lcom/android/camera/CameraManager$CameraProxy;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/android/camera/CameraManager$CameraProxy;->stopMAV(I)V

    :goto_1
    monitor-exit v0

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "MavController"

    const-string v2, "doStop device is release? "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initializeViews(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    return-void
.end method

.method private onHardwareStopped(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "MavController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHardwareStopped isMerge: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    invoke-interface {v0, p1}, Lcom/mediatek/camera/mav/MavController$CaptureEventListener;->onCaptureDone(Z)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    return-void
.end method

.method private showGuideText(Landroid/content/Context;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I

    return-void
.end method

.method private stopAsync(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const-string v1, "MavController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopAsync mStopping: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/camera/mav/MavController;->mStopping:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/camera/mav/MavController;->mStopping:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/mediatek/camera/mav/MavController;->mStopping:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/camera/mav/MavController$2;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/camera/mav/MavController$2;-><init>(Lcom/mediatek/camera/mav/MavController;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavController;->lock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/mediatek/camera/mav/MavController;->mStopProcess:Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private waitLock()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/camera/mav/MavController;->lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public checkStopProcess()V
    .locals 1

    :goto_0
    iget-boolean v0, p0, Lcom/mediatek/camera/mav/MavController;->mStopProcess:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/camera/mav/MavController;->waitLock()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onFrame()V
    .locals 5

    const/4 v4, 0x1

    const/16 v3, 0x19

    const-string v0, "MavController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFrame: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    const-string v0, "MavController"

    const-string v1, "mav done"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    invoke-direct {p0, v4}, Lcom/mediatek/camera/mav/MavController;->onHardwareStopped(Z)V

    :goto_1
    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    if-ne v0, v3, :cond_0

    invoke-virtual {p0, v4}, Lcom/mediatek/camera/mav/MavController;->stop(Z)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    if-ltz v0, :cond_4

    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    if-ge v0, v3, :cond_4

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v1, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x9

    div-int/lit8 v1, v1, 0x19

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    goto :goto_1

    :cond_4
    const-string v0, "MavController"

    const-string v1, "onFrame is called in abnormal state"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public resetController()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    return-void
.end method

.method public setCamera(Lcom/android/camera/CameraManager$CameraProxy;)V
    .locals 3
    .param p1    # Lcom/android/camera/CameraManager$CameraProxy;

    const-string v0, "MavController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCamera mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    return-void
.end method

.method public setOrientation(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    sget-boolean v1, Lcom/mediatek/camera/mav/MavController;->ANIMATION:Z

    invoke-virtual {v0, p1, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    return-void
.end method

.method public start()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/camera/mav/MavController;->mStopping:Z

    if-nez v2, :cond_0

    iput v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    iput v1, p0, Lcom/mediatek/camera/mav/MavController;->mCurrentNum:I

    invoke-direct {p0}, Lcom/mediatek/camera/mav/MavController;->doStart()V

    iget-object v2, p0, Lcom/mediatek/camera/mav/MavController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v2, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "MavController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "MavController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/mediatek/camera/mav/MavController;->mState:I

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    :cond_0
    :goto_1
    invoke-direct {p0, p1}, Lcom/mediatek/camera/mav/MavController;->stopAsync(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/mav/MavController;->mCaptureEventListener:Lcom/mediatek/camera/mav/MavController$CaptureEventListener;

    invoke-interface {v0}, Lcom/mediatek/camera/mav/MavController$CaptureEventListener;->onMergeStarted()V

    goto :goto_1
.end method
