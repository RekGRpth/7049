.class Lcom/mediatek/camera/panorama/AnimationController$1;
.super Ljava/lang/Object;
.source "AnimationController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/panorama/AnimationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDotCount:I

.field final synthetic this$0:Lcom/mediatek/camera/panorama/AnimationController;


# direct methods
.method constructor <init>(Lcom/mediatek/camera/panorama/AnimationController;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->mDotCount:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/AnimationController;->access$000(Lcom/mediatek/camera/panorama/AnimationController;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iput v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->mDotCount:I

    :cond_0
    iget-object v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/AnimationController;->access$100(Lcom/mediatek/camera/panorama/AnimationController;)I

    move-result v1

    iget v2, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->mDotCount:I

    if-lt v1, v2, :cond_1

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x5a0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const-string v1, "camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start Arrow animation of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v3}, Lcom/mediatek/camera/panorama/AnimationController;->access$100(Lcom/mediatek/camera/panorama/AnimationController;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/AnimationController;->access$000(Lcom/mediatek/camera/panorama/AnimationController;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v2}, Lcom/mediatek/camera/panorama/AnimationController;->access$100(Lcom/mediatek/camera/panorama/AnimationController;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/AnimationController;->access$108(Lcom/mediatek/camera/panorama/AnimationController;)I

    iget-object v1, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->this$0:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/AnimationController;->access$200(Lcom/mediatek/camera/panorama/AnimationController;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x168

    iget v3, p0, Lcom/mediatek/camera/panorama/AnimationController$1;->mDotCount:I

    div-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
