.class Lcom/mediatek/camera/panorama/PanoramaActivity$3;
.super Landroid/os/Handler;
.source "PanoramaActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/panorama/PanoramaActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage what= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$300(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$400(Lcom/mediatek/camera/panorama/PanoramaActivity;)Lcom/android/camera/RotateDialogController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$500(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->hidePost3DAutoRamaControlAlert()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$600(Lcom/mediatek/camera/panorama/PanoramaActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$800(Lcom/mediatek/camera/panorama/PanoramaActivity;)Lcom/android/camera/CameraScreenNail;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$700(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$902(Lcom/mediatek/camera/panorama/PanoramaActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/camera/Thumbnail;

    invoke-static {v1, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1002(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateThumbnailView()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1100(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1200(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$900(Lcom/mediatek/camera/panorama/PanoramaActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$902(Lcom/mediatek/camera/panorama/PanoramaActivity;Z)Z

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/camera/Thumbnail;

    invoke-static {v1, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1302(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateThumbnailView()V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->showPost3DAutoRamControlAlert()V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1100(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_6
        0x64 -> :sswitch_5
    .end sparse-switch
.end method
