.class Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "PanoramaActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/camera/panorama/PanoramaActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOrientationEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/camera/panorama/PanoramaActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 4
    .param p1    # I

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v2}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1400(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v2

    invoke-static {p1, v2}, Lcom/android/camera/Util;->roundOrientation(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1402(Lcom/mediatek/camera/panorama/PanoramaActivity;I)I

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1400(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v2}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v2

    add-int/2addr v1, v2

    rem-int/lit16 v0, v1, 0x168

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1500(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v1, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1502(Lcom/mediatek/camera/panorama/PanoramaActivity;I)I

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;->this$0:Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-static {v2}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1500(Lcom/mediatek/camera/panorama/PanoramaActivity;)I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->access$1600(Lcom/mediatek/camera/panorama/PanoramaActivity;IZ)V

    goto :goto_0
.end method
