.class public Lcom/mediatek/camera/panorama/PanoramaActivity;
.super Lcom/android/camera/ActivityBase;
.source "PanoramaActivity.java"

# interfaces
.implements Landroid/hardware/Camera$ErrorCallback;
.implements Lcom/android/camera/FocusManager$Listener;
.implements Lcom/android/camera/LocationManager$Listener;
.implements Lcom/android/camera/ModePicker$OnModeChangeListener;
.implements Lcom/android/camera/PreviewFrameLayout$OnSizeChangedListener;
.implements Lcom/android/camera/ShutterButton$OnShutterButtonListener;
.implements Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;,
        Lcom/mediatek/camera/panorama/PanoramaActivity$ZoomChangeListener;,
        Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;
    }
.end annotation


# static fields
.field private static final FIRST_TIME_INIT:I = 0x2

.field private static final FOCUSING:I = 0x2

.field private static final GUIDE_CAPTURE:I = 0x2

.field private static final GUIDE_MOVE:I = 0x1

.field private static final GUIDE_SHUTTER:I = 0x0

.field private static final IDLE:I = 0x1

.field private static final MSG_3D_SWITCH_TEST:I = 0x64

.field private static final MSG_CLEAR_SCREEN_DELAY:I = 0x5

.field private static final MSG_FINAL_IMAGE_READY:I = 0x1

.field private static final MSG_GENERATE_FINAL_IMAGE_ERROR:I = 0x3

.field private static final MSG_GET_THUMBNAIL_DONE:I = 0x6

.field private static final PREVIEW_STOPPED:I = 0x0

.field private static final REVIEW_DURATION:I = 0x7d0

.field private static final SCREEN_DELAY:I = 0x1d4c0

.field private static final SNAPSHOT_IN_PROGRESS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PanoramaActivity"

.field private static final UPDATE_STORAGE:I = 0x7

.field private static final ZOOM_START:I = 0x1

.field private static final ZOOM_STOPPED:I = 0x0

.field private static final ZOOM_STOPPING:I = 0x2


# instance fields
.field private mAeLockSupported:Z

.field private mAlertControlBar:Landroid/view/ViewGroup;

.field private final mAutoFocusCallback:Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;

.field private mAwbLockSupported:Z

.field private mCameraDisplayOrientation:I

.field private final mCameraId:I

.field private mCameraSound:Landroid/media/MediaActionSound;

.field private mCameraState:I

.field private mCancelButton:Lcom/android/camera/ui/RotateImageView;

.field private mCancelGroup:Lcom/android/camera/ui/RotateLayout;

.field private mDisplayOrientation:I

.field private mDisplayRotation:I

.field mDoneButton:Lcom/android/camera/ui/RotateImageView;

.field private mDoneGroup:Lcom/android/camera/ui/RotateLayout;

.field private mFalseShutterCallback:Ljava/lang/Runnable;

.field private mFirstTimeInitialized:Z

.field private mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

.field private mFocusAreaSupported:Z

.field private mFocusManager:Lcom/android/camera/FocusManager;

.field private mFocusStartTime:J

.field private mFullLayout:Landroid/view/View;

.field private mGpsIndicator:Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mLocationManager:Lcom/android/camera/LocationManager;

.field private mMeteringAreaSupported:Z

.field private mModePicker:Lcom/android/camera/ModePicker;

.field private mNameFormat:Ljava/lang/String;

.field private mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

.field private mOnScreenProgress:Lcom/android/camera/ui/RotateLayout;

.field private mOrientation:I

.field private mOrientationCompensation:I

.field private mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

.field private mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

.field private mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPictureSize:Landroid/hardware/Camera$Size;

.field private mPicturesRemaining:J

.field mPreferences:Lcom/android/camera/ComboPreferences;

.field private mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRemainPictureView:Landroid/widget/TextView;

.field private mRotateDialog:Lcom/android/camera/RotateDialogController;

.field private mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

.field private mShutterButton:Lcom/android/camera/ShutterButton;

.field private mStorageHint:Lcom/android/camera/OnScreenHint;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTargetZoomValue:I

.field private mThumbnailUpdated:Z

.field private mTimeTaken:J

.field private mUpdateHintRunnable:Ljava/lang/Runnable;

.field private mWhiteBalanceIndicator:Landroid/widget/ImageView;

.field private mZoomControl:Lcom/android/camera/ui/ZoomControl;

.field private mZoomMax:I

.field private mZoomState:I

.field private mZoomValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/camera/ActivityBase;-><init>()V

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->getBackCameraId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomState:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientation:I

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$1;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFalseShutterCallback:Ljava/lang/Runnable;

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$2;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mUpdateHintRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$3;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$3;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$5;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$5;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/mediatek/camera/panorama/PanoramaActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAutoFocusCallback:Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/camera/panorama/PanoramaActivity;)Lcom/android/camera/FocusManager;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/camera/panorama/PanoramaActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Lcom/android/camera/Thumbnail;

    iput-object p1, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/camera/panorama/PanoramaActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->checkStorage()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/camera/panorama/PanoramaActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->showCaptureError()V

    return-void
.end method

.method static synthetic access$1302(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Lcom/android/camera/Thumbnail;

    iput-object p1, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/mediatek/camera/panorama/PanoramaActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientation:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/camera/panorama/PanoramaActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientation:I

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/camera/panorama/PanoramaActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/camera/panorama/PanoramaActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/camera/panorama/PanoramaActivity;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setOrientationIndicator(IZ)V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/camera/panorama/PanoramaActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mNameFormat:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/camera/panorama/PanoramaActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mTimeTaken:J

    return-wide v0
.end method

.method static synthetic access$1900(Ljava/lang/String;J)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # J

    invoke-static {p0, p1, p2}, Lcom/mediatek/camera/panorama/PanoramaActivity;->createName(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/camera/panorama/PanoramaActivity;J)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # J

    invoke-virtual {p0, p1, p2}, Lcom/android/camera/ActivityBase;->updateStorageHint(J)V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/camera/panorama/PanoramaActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFullLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Lcom/android/camera/Thumbnail;

    invoke-virtual {p0, p1}, Lcom/android/camera/ActivityBase;->saveThumbnailToFile(Lcom/android/camera/Thumbnail;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/camera/panorama/PanoramaActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/mediatek/camera/panorama/PanoramaActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->getLastThumbnailUncached()V

    return-void
.end method

.method static synthetic access$2500(Lcom/mediatek/camera/panorama/PanoramaActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopCapture(Z)V

    return-void
.end method

.method static synthetic access$2602(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Lcom/android/camera/Thumbnail;

    iput-object p1, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/mediatek/camera/panorama/PanoramaActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->onZoomValueChanged(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/mediatek/camera/panorama/PanoramaActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/camera/panorama/PanoramaActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeFirstTime()V

    return-void
.end method

.method static synthetic access$3000(Lcom/mediatek/camera/panorama/PanoramaActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusStartTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/mediatek/camera/panorama/PanoramaActivity;)Lcom/android/camera/RotateDialogController;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/camera/panorama/PanoramaActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/camera/panorama/PanoramaActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/camera/panorama/PanoramaActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getCameraRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/camera/panorama/PanoramaActivity;)Lcom/android/camera/CameraScreenNail;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/camera/panorama/PanoramaActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mThumbnailUpdated:Z

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/camera/panorama/PanoramaActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mThumbnailUpdated:Z

    return p1
.end method

.method private canTakePicture()Z
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isCameraIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkStorage()V
    .locals 6

    const-wide/32 v2, 0x2faf080

    const-wide/16 v4, 0x0

    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    sub-long/2addr v0, v2

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->pictureSize()J

    move-result-wide v2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRemainPictureView:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    invoke-virtual {p0, v0, v1}, Lcom/android/camera/ActivityBase;->updateStorageHint(J)V

    return-void

    :cond_1
    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    iput-wide v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRemainPictureView:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private closeCamera()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "PanoramaActivity"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v2}, Lcom/android/camera/CameraManager$CameraProxy;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->release()V

    iput-object v2, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCameraState(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onCameraReleased()V

    :cond_0
    return-void
.end method

.method private collapseCameraControls()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/ModePicker;->dismissModeSelection()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private configureCamera(Landroid/hardware/Camera$Parameters;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method private createContentView()V
    .locals 4

    const v3, 0x7f0b0077

    const/4 v2, 0x0

    const v0, 0x7f0b00a6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFullLayout:Landroid/view/View;

    const v0, 0x7f0b010d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v2}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailViewWidth:I

    const v0, 0x7f0b0064

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ModePicker;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0, p0}, Lcom/android/camera/ModePicker;->setOnModeChangeListener(Lcom/android/camera/ModePicker$OnModeChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/camera/ModePicker;->setCurrentMode(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/ModePicker;->setModeSupport()V

    :cond_0
    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ShutterButton;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ShutterButton$OnShutterButtonListener;)V

    const v0, 0x7f0b0079

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateLayout;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

    const v0, 0x7f0b007e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateLayout;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenProgress:Lcom/android/camera/ui/RotateLayout;

    const v0, 0x7f0b007c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRemainPictureView:Landroid/widget/TextView;

    const v0, 0x7f0b007d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v2}, Lcom/android/camera/ui/TwoStateImageView;->enableFilter(Z)V

    const v0, 0x7f0b0109

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneButton:Lcom/android/camera/ui/RotateImageView;

    const v0, 0x7f0b0107

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelButton:Lcom/android/camera/ui/RotateImageView;

    const v0, 0x7f0b003e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateLayout;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    new-instance v1, Lcom/android/camera/RotateDialogController;

    const v2, 0x7f04004f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2, v0}, Lcom/android/camera/RotateDialogController;-><init>(Landroid/app/Activity;ILandroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/ZoomControl;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/PreviewFrameLayout;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setSingleTapUpListener(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

    invoke-virtual {v0, p0}, Lcom/android/camera/PreviewFrameLayout;->setOnSizeChangedListener(Lcom/android/camera/PreviewFrameLayout$OnSizeChangedListener;)V

    return-void
.end method

.method private static createName(Ljava/lang/String;J)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private enableCameraControls(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0, p1}, Lcom/android/camera/ModePicker;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/ZoomControl;->setEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_3
    return-void
.end method

.method private getCameraRotation()I
    .locals 2

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayRotation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method public static getExifOrientation(Landroid/media/ExifInterface;)I
    .locals 4
    .param p0    # Landroid/media/ExifInterface;

    const/4 v3, -0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v2, "Orientation"

    invoke-virtual {p0, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v3, :cond_0

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static getExifOrientation(I)Ljava/lang/String;
    .locals 3
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :sswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_1
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private getGuideString(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0c0097

    :goto_1
    goto :goto_0

    :cond_0
    const v0, 0x7f0c009b

    goto :goto_1

    :pswitch_1
    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0c0099

    :goto_2
    goto :goto_0

    :cond_1
    const v0, 0x7f0c0098

    goto :goto_2

    :pswitch_2
    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f0c009a

    :goto_3
    goto :goto_0

    :cond_2
    const v0, 0x7f0c009c

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initOnScreenIndicator()V
    .locals 2

    const v0, 0x7f0b007b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    const v0, 0x7f0b007a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->supportPanorama3dSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCaptureMode()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updatePanorama3DSwitch()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private initializeCapabilities()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v0}, Lcom/android/camera/Util;->setModeSupport(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/ModePicker;->setModeSupport()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "auto"

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaSupported:Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mMeteringAreaSupported:Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAeLockSupported:Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAwbLockSupported:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private initializeFirstTime()V
    .locals 3

    const-string v1, "PanoramaActivity"

    const-string v2, "initializeFirstTime"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mNameFormat:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeGuideView()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initOnScreenIndicator()V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getWhiteBalance()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateWhiteBalanceOnScreenIndicator(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeZoom()V

    new-instance v1, Lcom/android/camera/LocationManager;

    invoke-direct {v1, p0, p0}, Lcom/android/camera/LocationManager;-><init>(Landroid/content/Context;Lcom/android/camera/LocationManager$Listener;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/RecordLocationPreference;->get(Landroid/content/SharedPreferences;Landroid/content/ContentResolver;)Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/LocationManager;->recordLocation(Z)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->enable()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->checkStorage()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->installIntentFilter()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    goto :goto_0
.end method

.method private initializeFocusManager()V
    .locals 9

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    aget-object v8, v0, v1

    iget v0, v8, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v6, :cond_1

    :goto_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/android/camera/FocusManager;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/camera/FocusManager;-><init>(Lcom/android/camera/ComboPreferences;[Ljava/lang/String;Landroid/view/View;Landroid/hardware/Camera$Parameters;Lcom/android/camera/FocusManager$Listener;ZLandroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private initializeGuideView()V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/android/camera/ui/RotateTextToast;

    invoke-direct {p0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getGuideString(I)I

    move-result v2

    const v0, 0x7f0b0078

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/android/camera/ui/RotateTextToast;-><init>(Landroid/app/Activity;IILandroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    invoke-virtual {v0}, Lcom/android/camera/ui/RotateTextToast;->showTransparent()V

    return-void
.end method

.method private initializeSecondTime()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->enable()V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/RecordLocationPreference;->get(Landroid/content/SharedPreferences;Landroid/content/ContentResolver;)Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/LocationManager;->recordLocation(Z)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->checkStorage()V

    return-void
.end method

.method private initializeZoom()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeZoomMax(Landroid/hardware/Camera$Parameters;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ZoomControl;->setZoomIndex(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    new-instance v1, Lcom/mediatek/camera/panorama/PanoramaActivity$ZoomChangeListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/camera/panorama/PanoramaActivity$ZoomChangeListener;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;Lcom/mediatek/camera/panorama/PanoramaActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ZoomControl;->setOnZoomChangeListener(Lcom/android/camera/ui/ZoomControl$OnZoomChangedListener;)V

    goto :goto_0
.end method

.method private initializeZoomMax(Landroid/hardware/Camera$Parameters;)Z
    .locals 3
    .param p1    # Landroid/hardware/Camera$Parameters;

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomMax:I

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeZoomMax zoom max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomMax:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomMax:I

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ZoomControl;->setZoomMax(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private installIntentFilter()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private isCameraIdle()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->isFocusCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private keepScreenOnAwhile()V
    .locals 4

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private onZoomValueChanged(I)V
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "PanoramaActivity"

    const-string v1, "Set zoom value to Camera Device"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomValue:I

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateCameraParametersZoom()V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0
.end method

.method private openCamera()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/camera/CameraHardwareException;,
            Lcom/android/camera/CameraDisabledException;
        }
    .end annotation

    const-string v0, "PanoramaActivity"

    const-string v1, "openCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    invoke-static {p0, v0}, Lcom/android/camera/Util;->openCamera(Landroid/app/Activity;I)Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/panorama/PanoramaController;->setCamera(Lcom/android/camera/CameraManager$CameraProxy;)V

    return-void
.end method

.method private pictureSize()J
    .locals 2

    const-string v0, "autorama"

    invoke-static {v0}, Lcom/android/camera/Storage;->getSize(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private resetCapture(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resetCapture finish = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->unlockAeAwb()V

    invoke-direct {p0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCameraState(I)V

    :cond_0
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    invoke-direct {p0, v0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setOrientationIndicator(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->keepScreenOnAwhile()V

    return-void
.end method

.method private resetScreenOn()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method private safeStop()V
    .locals 3

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    const-string v1, "PanoramaActivity"

    const-string v2, "check stopAsync thread state, if running,we must wait"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v1}, Lcom/mediatek/camera/panorama/PanoramaController;->checkStopProcess()V

    monitor-enter v0

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopPreview()V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->closeCamera()V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v1}, Lcom/android/camera/CameraScreenNail;->releaseSurfaceTexture()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopCapture(Z)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private setCameraState(I)V
    .locals 4
    .param p1    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    return-void

    :pswitch_0
    invoke-direct {p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->enableCameraControls(Z)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->enableCameraControls(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setCaptureMode()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "pano_3d"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    goto :goto_0

    :cond_1
    const-string v0, "normal"

    goto :goto_1
.end method

.method private setDisplayOrientation()V
    .locals 3

    invoke-static {p0}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayRotation:I

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    invoke-static {v0, v1}, Lcom/android/camera/Util;->getDisplayOrientation(II)I

    move-result v0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraDisplayOrientation:I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayRotation:I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    invoke-static {v0, v1}, Lcom/android/camera/Util;->getDisplayOrientation(II)I

    move-result v0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayOrientation:I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayOrientation:I

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayRotation:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/camera/panorama/PanoramaController;->setDisplayOrientation(II)V

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDisplayOrientation orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDisplayOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setOrientationIndicator(IZ)V
    .locals 8
    .param p1    # I
    .param p2    # Z

    const/4 v7, 0x3

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-ne v5, v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x4

    new-array v4, v5, [Lcom/android/camera/ui/Rotatable;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    aput-object v5, v4, v7

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-interface {v3, p1, p2}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v5

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientation:I

    const/16 v6, 0x10e

    if-ne v5, v6, :cond_0

    :cond_3
    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v5, p1, p2}, Lcom/mediatek/camera/panorama/PanoramaController;->setOrientation(IZ)V

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/ZoomControl;->setOrientation(IZ)V

    :cond_4
    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    :cond_5
    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/RotateTextToast;->setOrientation(IZ)V

    :cond_6
    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenProgress:Lcom/android/camera/ui/RotateLayout;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenProgress:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    :cond_7
    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneGroup:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelGroup:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v5, p1, p2}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    goto :goto_0
.end method

.method private setPreviewDisplay()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraDisplayOrientation:I

    rem-int/lit16 v1, v1, 0xb4

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->setSize(II)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->notifyScreenNailChanged()V

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v1}, Lcom/android/camera/CameraScreenNail;->acquireSurfaceTexture()V

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    :cond_0
    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, v2}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewTextureAsync(Landroid/graphics/SurfaceTexture;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->setSize(II)V

    goto :goto_0
.end method

.method private setupCamera()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/camera/CameraHardwareException;,
            Lcom/android/camera/CameraDisabledException;
        }
    .end annotation

    const-string v0, "PanoramaActivity"

    const-string v1, "setupCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->openCamera()V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeCapabilities()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeFocusManager()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setupCaptureParams(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->configureCamera(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method private setupCaptureParams(Landroid/hardware/Camera$Parameters;)V
    .locals 12
    .param p1    # Landroid/hardware/Camera$Parameters;

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPictureSize:Landroid/hardware/Camera$Size;

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreviewFrameLayout:Lcom/android/camera/PreviewFrameLayout;

    iget-object v8, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPictureSize:Landroid/hardware/Camera$Size;

    iget v8, v8, Landroid/hardware/Camera$Size;->width:I

    int-to-double v8, v8

    iget-object v10, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPictureSize:Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Lcom/android/camera/PreviewFrameLayout;->setAspectRatio(D)V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v7

    iget v8, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    aget-object v7, v7, v8

    iget v0, v7, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPictureSize:Landroid/hardware/Camera$Size;

    iget v7, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v7, v7

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPictureSize:Landroid/hardware/Camera$Size;

    iget v9, v9, Landroid/hardware/Camera$Size;->height:I

    int-to-double v9, v9

    div-double/2addr v7, v9

    invoke-static {p0, v5, v7, v8}, Lcom/android/camera/Util;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    const-string v7, "PanoramaActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Sensor["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\'s orientation is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4, v3}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v0, :cond_0

    const/16 v7, 0xb4

    if-ne v0, v7, :cond_4

    :cond_0
    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v8, v3, Landroid/hardware/Camera$Size;->height:I

    iget v9, v3, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v7, v8, v9}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :goto_0
    iget-object v7, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v8, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7, v8}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v7, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v7}, Lcom/android/camera/CameraManager$CameraProxy;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    iput-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    :cond_1
    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v7}, Lcom/android/camera/Util;->setPreviewProperty(Landroid/hardware/Camera$Parameters;)V

    const-string v7, "PanoramaActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Preview size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    :cond_2
    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v8, "video-stabilization-supported"

    invoke-virtual {v7, v8}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "true"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v8, "video-stabilization"

    const-string v9, "false"

    invoke-virtual {v7, v8, v9}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateCameraParametersZoom()V

    return-void

    :cond_4
    iget-object v7, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v8, v3, Landroid/hardware/Camera$Size;->width:I

    iget v9, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v7, v8, v9}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_0
.end method

.method private showCaptureError()V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0c011b

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0c010c

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0c011a

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/android/camera/RotateDialogController;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private startPreview()V
    .locals 3

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startPreview mPaused = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->resetTouchFocus()V

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopPreview()V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setDisplayOrientation()V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setDisplayOrientation(I)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setPreviewDisplay()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/FocusManager;->setAeAwbLock(Z)V

    const-string v0, "PanoramaActivity"

    const-string v1, "startPreviewAsync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->startPreviewAsync()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStarted()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCameraState(I)V

    goto :goto_0
.end method

.method private stopCapture(Z)V
    .locals 4
    .param p1    # Z

    const/4 v0, 0x1

    const-string v1, "PanoramaActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopCapture isMerge = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v1}, Lcom/mediatek/camera/panorama/PanoramaController;->hasCaptured()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    if-nez p1, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->resetCapture(Z)V

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaController;->resetController()V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->stop(Z)V

    goto :goto_1
.end method

.method private stopPreview()V
    .locals 3

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopPreview mCameraState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-eqz v0, :cond_0

    const-string v0, "PanoramaActivity"

    const-string v1, "stopPreview"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->stopPreview()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStopped()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCameraState(I)V

    return-void
.end method

.method private switchToOtherMode(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-static {v0}, Lcom/android/camera/ThumbnailHolder;->keep(Lcom/android/camera/Thumbnail;)V

    :cond_1
    invoke-static {p1, p0}, Lcom/android/camera/MenuHelper;->gotoMode(ILandroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private unlockAeAwb()V
    .locals 2

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/FocusManager;->setAeAwbLock(Z)V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setFocusParameters()V

    :cond_0
    return-void
.end method

.method private updateCameraParametersPreference()V
    .locals 13

    const v12, 0x7f0c002f

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_whitebalance_key"

    const v11, 0x7f0c00f9

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedWhiteBalance()Ljava/util/List;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v8}, Landroid/hardware/Camera$Parameters;->setWhiteBalance(Ljava/lang/String;)V

    :cond_0
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_exposuremeter_key"

    const v11, 0x7f0c0024

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedExposureMeter()Ljava/util/List;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v4}, Landroid/hardware/Camera$Parameters;->setExposureMeter(Ljava/lang/String;)V

    :cond_1
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_hue_key"

    const v11, 0x7f0c0031

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedHueMode()Ljava/util/List;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v5}, Landroid/hardware/Camera$Parameters;->setHueMode(Ljava/lang/String;)V

    :cond_2
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_brightness_key"

    const v11, 0x7f0c0035

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedBrightnessMode()Ljava/util/List;

    move-result-object v9

    invoke-static {v1, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v1}, Landroid/hardware/Camera$Parameters;->setBrightnessMode(Ljava/lang/String;)V

    :cond_3
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_edge_key"

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedEdgeMode()Ljava/util/List;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v3}, Landroid/hardware/Camera$Parameters;->setEdgeMode(Ljava/lang/String;)V

    :cond_4
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_saturation_key"

    const v11, 0x7f0c0033

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedSaturationMode()Ljava/util/List;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v7}, Landroid/hardware/Camera$Parameters;->setSaturationMode(Ljava/lang/String;)V

    :cond_5
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_contrast_key"

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v2}, Landroid/hardware/Camera$Parameters;->setContrastMode(Ljava/lang/String;)V

    :cond_6
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_iso_key"

    const v11, 0x7f0c001c

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedISOSpeed()Ljava/util/List;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v6}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    :cond_7
    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    const-string v10, "pref_camera_antibanding_key"

    const v11, 0x7f0c0057

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedAntibanding()Ljava/util/List;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v0}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    :cond_8
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateCameraParametersZoom()V

    return-void
.end method

.method private updateCameraParametersZoom()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomValue:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    :cond_0
    return-void
.end method

.method private updateIndicatorVisibility()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRemainPictureView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->supportPanorama3dSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private updatePanorama3DSwitch()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v1}, Lcom/android/camera/Util;->setModeSupport(Landroid/hardware/Camera$Parameters;)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v1}, Lcom/android/camera/ModePicker;->setModeSupport()V

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v1

    add-int/lit16 v1, v1, 0x10e

    rem-int/lit16 v0, v1, 0x168

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    const v2, 0x7f02001c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    invoke-virtual {v1, v0, v3}, Lcom/android/camera/ui/ZoomControl;->setOrientation(IZ)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOnScreenDisplayLayout:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v1, v0, v3}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    invoke-virtual {v1, v0, v3}, Lcom/android/camera/ui/RotateTextToast;->setOrientation(IZ)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneGroup:Lcom/android/camera/ui/RotateLayout;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneGroup:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v1, v0, v3}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    :cond_3
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelGroup:Lcom/android/camera/ui/RotateLayout;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelGroup:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v1, v0, v3}, Lcom/android/camera/ui/RotateLayout;->setOrientation(IZ)V

    :cond_4
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v1, v0, v3}, Lcom/mediatek/camera/panorama/PanoramaController;->setOrientation(IZ)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_5
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    const v2, 0x7f02001d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    invoke-direct {p0, v1, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setOrientationIndicator(IZ)V

    goto :goto_0
.end method

.method private updateWhiteBalanceOnScreenIndicator(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "auto"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const-string v0, "fluorescent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02008c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const-string v0, "incandescent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02008e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    const-string v0, "daylight"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020096

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    const-string v0, "cloudy-daylight"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f02008b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_6
    const-string v0, "shade"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020095

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    const-string v0, "twilight"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020098

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_8
    const-string v0, "tungsten"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020097

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_9
    const-string v0, "warm-fluorescent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mWhiteBalanceIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020099

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method


# virtual methods
.method public autoFocus()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusStartTime:J

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAutoFocusCallback:Lcom/mediatek/camera/panorama/PanoramaActivity$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    return-void
.end method

.method public cancelAutoFocus()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setFocusParameters()V

    return-void
.end method

.method public capture()Z
    .locals 7

    const/4 v6, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v4, "PanoramaActivity"

    const-string v5, "capture"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-eq v4, v6, :cond_0

    iget-object v4, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientation:I

    invoke-static {v4, v5}, Lcom/android/camera/Util;->getJpegRotation(II)I

    move-result v0

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v0}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    invoke-virtual {v4}, Lcom/android/camera/LocationManager;->getCurrentLocation()Landroid/location/Location;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v4, v1}, Lcom/android/camera/Util;->setGpsParameters(Landroid/hardware/Camera$Parameters;Landroid/location/Location;)V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCapturePath()V

    iget-object v4, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v4}, Lcom/mediatek/camera/panorama/PanoramaController;->start()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->showPost3DAutoRamControlAlert()V

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    invoke-direct {p0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getGuideString(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/camera/ui/RotateTextToast;->changeTextContent(I)V

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v4, v2}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->keepScreenOnAwhile()V

    invoke-direct {p0, v6}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCameraState(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFalseShutterCallback:Ljava/lang/Runnable;

    const-wide/16 v5, 0x12c

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v2, v3

    goto :goto_0
.end method

.method public doSmileShutter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hideGpsOnScreenIndicator()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public hidePost3DAutoRamaControlAlert()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/android/camera/Util;->fadeOut(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneButton:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelButton:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/android/camera/Util;->fadeIn([Landroid/view/View;)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateIndicatorVisibility()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    invoke-direct {p0, v4}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getGuideString(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateTextToast;->changeTextContent(I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public isIgnoreCheckDefaultPath()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public movingTips()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getGuideString(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateTextToast;->changeTextContent(I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isCameraIdle()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCaptureDone(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCaptureDone isMerge "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->resetCapture(Z)V

    :cond_0
    if-eqz p1, :cond_1

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$4;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1    # Landroid/content/res/Configuration;

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v2, "PanoramaActivity"

    const-string v4, "onConfigurationChanged"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    iput-object v5, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    const v2, 0x7f0b0012

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040027

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v2, 0x7f04000c

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->createContentView()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeFocusManager()V

    new-instance v2, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    invoke-direct {v2, p0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    new-instance v2, Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-direct {v2, p0, p0}, Lcom/mediatek/camera/panorama/PanoramaController;-><init>(Landroid/app/Activity;Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;)V

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v4, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v2, v4}, Lcom/mediatek/camera/panorama/PanoramaController;->setCamera(Lcom/android/camera/CameraManager$CameraProxy;)V

    const v2, 0x7f0b0105

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    const v2, 0x7f0b0108

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/RotateLayout;

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneGroup:Lcom/android/camera/ui/RotateLayout;

    const v2, 0x7f0b0106

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/RotateLayout;

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelGroup:Lcom/android/camera/ui/RotateLayout;

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setDisplayOrientation()V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updateThumbnailView()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeZoom()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeGuideView()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initOnScreenIndicator()V

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationCompensation:I

    invoke-direct {p0, v2, v3}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setOrientationIndicator(IZ)V

    return-void

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const-string v1, "PanoramaActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate Bundle = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/camera/ComboPreferences;

    invoke-direct {v1, p0}, Lcom/android/camera/ComboPreferences;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraId:I

    invoke-virtual {v1, p0, v2}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    invoke-super {p0, p1}, Lcom/android/camera/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/mediatek/camera/ext/ExtensionHelper;->ensureCameraExtension(Landroid/content/Context;)V

    const v1, 0x7f04002a

    invoke-virtual {p0, v1}, Lcom/android/camera/ActivityBase;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->createContentView()V

    invoke-virtual {p0, v4, v4}, Lcom/android/camera/ActivityBase;->createCameraScreenNail(ZZ)V

    new-instance v1, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;-><init>(Lcom/mediatek/camera/panorama/PanoramaActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    new-instance v1, Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/camera/panorama/PanoramaController;-><init>(Landroid/app/Activity;Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;)V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    const v1, 0x7f0b0105

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    const v1, 0x7f0b0108

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateLayout;

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneGroup:Lcom/android/camera/ui/RotateLayout;

    const v1, 0x7f0b0106

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateLayout;

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelGroup:Lcom/android/camera/ui/RotateLayout;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onDestroy()V

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public onError(ILandroid/hardware/Camera;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/hardware/Camera;

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Media server died."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    const v0, 0x7f0c0079

    invoke-static {p0, v0}, Lcom/android/camera/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->showCaptureError()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopCapture(Z)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/android/camera/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->onShutterButtonFocus(Z)V

    goto :goto_0

    :sswitch_1
    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->onShutterButtonClick()V

    goto :goto_0

    :sswitch_2
    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->onShutterButtonFocus(Z)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v1}, Landroid/view/View;->requestFocusFromTouch()Z

    :goto_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_2
        0x1b -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyPressed(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyPressed ok = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopCapture(Z)V

    :cond_0
    return-void
.end method

.method public onMergeStarted()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/RotateDialogController;->showWaitingDialog(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    const v1, 0x7f0c0140

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateTextToast;->changeTextContent(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneButton:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCancelButton:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onModeChanged(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->switchToOtherMode(I)V

    :cond_0
    return-void
.end method

.method public onPanorama3DClicked(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .annotation runtime Lcom/android/camera/OnClickAttr;
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->isCameraIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/camera/Util;->switch3DMode()Z

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setCaptureMode()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->updatePanorama3DSwitch()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaController;->updateMatrix()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateTextToast:Lcom/android/camera/ui/RotateTextToast;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->getGuideString(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateTextToast;->changeTextContent(I)V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "PanoramaActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->isForceFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->removeMessages()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->safeStop()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRotateDialog:Lcom/android/camera/RotateDialogController;

    invoke-virtual {v0}, Lcom/android/camera/RotateDialogController;->dismissDialog()V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->hidePost3DAutoRamaControlAlert()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mUpdateHintRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mStorageHint:Lcom/android/camera/OnScreenHint;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mStorageHint:Lcom/android/camera/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/camera/OnScreenHint;->cancel()V

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mStorageHint:Lcom/android/camera/OnScreenHint;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mLocationManager:Lcom/android/camera/LocationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/LocationManager;->recordLocation(Z)V

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->resetScreenOn()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mOrientationEventListener:Lcom/mediatek/camera/panorama/PanoramaActivity$MyOrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    invoke-virtual {v0}, Landroid/media/MediaActionSound;->release()V

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x1

    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onResume()V

    const-string v1, "PanoramaActivity"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->isForceFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v5}, Lcom/android/camera/Storage;->updateDefaultDirectory(Z)Z

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-nez v1, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->setupCamera()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->startPreview()V

    const-string v1, "PanoramaActivity"

    const-string v2, "StartPreview done in onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/android/camera/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/camera/CameraDisabledException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mUpdateHintRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->keepScreenOnAwhile()V

    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->getLastThumbnail()V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    if-nez v1, :cond_2

    new-instance v1, Landroid/media/MediaActionSound;

    invoke-direct {v1}, Landroid/media/MediaActionSound;-><init>()V

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    invoke-virtual {v1, v5}, Landroid/media/MediaActionSound;->load(I)V

    :cond_2
    invoke-static {p0}, Lcom/android/camera/ui/PopupManager;->getInstance(Landroid/content/Context;)Lcom/android/camera/ui/PopupManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/ui/PopupManager;->notifyShowPopup(Landroid/view/View;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const v1, 0x7f0c00bc

    invoke-static {p0, v1}, Lcom/android/camera/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    const v1, 0x7f0c00bd

    invoke-static {p0, v1}, Lcom/android/camera/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->initializeSecondTime()V

    goto :goto_1
.end method

.method public onReviewCancelClicked(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .annotation runtime Lcom/android/camera/OnClickAttr;
    .end annotation

    const-string v0, "PanoramaActivity"

    const-string v1, "onReviewCancelClicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, v0, Lcom/mediatek/camera/panorama/PanoramaController;->mCancelOnClickListener:Landroid/view/View$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->hidePost3DAutoRamaControlAlert()V

    return-void
.end method

.method public onReviewDoneClicked(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .annotation runtime Lcom/android/camera/OnClickAttr;
    .end annotation

    const-string v0, "PanoramaActivity"

    const-string v1, "onReviewDoneClicked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, v0, Lcom/mediatek/camera/panorama/PanoramaController;->mOkOnClickListener:Landroid/view/View$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method

.method public onShutterButtonClick()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/ModePicker;->onOtherPopupShowed()V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPicturesRemaining:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->isFocusingSnapOnFinish()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraAppView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->doSnap()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->stopCapture(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onShutterButtonFocus(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->canTakePicture()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterDown()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterUp()V

    goto :goto_0
.end method

.method protected onSingleTapUp(Landroid/view/View;II)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFirstTimeInitialized:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->collapseCameraControls()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaSupported:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mMeteringAreaSupported:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0, p2, p3}, Lcom/android/camera/FocusManager;->onSingleTapUp(II)V

    goto :goto_0
.end method

.method public onSizeChanged(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "PanoramaActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSize Change width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/FocusManager;->setPreviewSize(II)V

    :cond_0
    if-le p1, p2, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/camera/panorama/PanoramaController;->setPreviewSize(II)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanoramaController:Lcom/mediatek/camera/panorama/PanoramaController;

    invoke-virtual {v0, p2, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->setPreviewSize(II)V

    goto :goto_0
.end method

.method public onThumbnailClicked(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .annotation runtime Lcom/android/camera/OnClickAttr;
    .end annotation

    iget-boolean v0, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnail:Lcom/android/camera/Thumbnail;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->gotoGallery()V

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->keepScreenOnAwhile()V

    :cond_0
    return-void
.end method

.method public playSound(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraSound:Landroid/media/MediaActionSound;

    invoke-virtual {v0, p1}, Landroid/media/MediaActionSound;->play(I)V

    return-void
.end method

.method public readyToCapture()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected saveThumbnailToFile()V
    .locals 0

    return-void
.end method

.method public setCapturePath()V
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mTimeTaken:J

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mNameFormat:Ljava/lang/String;

    iget-wide v3, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mTimeTaken:J

    invoke-static {v2, v3, v4}, Lcom/mediatek/camera/panorama/PanoramaActivity;->createName(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    :goto_0
    invoke-static {v1, v2}, Lcom/android/camera/Storage;->generateFilepath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setCapturePath(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setFocusParameters()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAeLockSupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getAeAwbLock()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setAutoExposureLock(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAwbLockSupported:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getAeAwbLock()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setAutoWhiteBalanceLock(Z)V

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusAreaSupported:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getFocusAreas()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    :cond_2
    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mMeteringAreaSupported:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getMeteringAreas()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->configureCamera(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method public showGpsOnScreenIndicator(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020103

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mGpsIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020101

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public showPost3DAutoRamControlAlert()V
    .locals 6

    const/16 v5, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    aput-object v3, v2, v1

    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    aput-object v3, v2, v0

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mZoomControl:Lcom/android/camera/ui/ZoomControl;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mRemainPictureView:Landroid/widget/TextView;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/android/camera/Util;->fadeOut([Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mPanorama3DSwitch:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mModePicker:Lcom/android/camera/ModePicker;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mShutterButton:Lcom/android/camera/ShutterButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    invoke-static {v2}, Lcom/android/camera/Util;->fadeIn(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mAlertControlBar:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneButton:Lcom/android/camera/ui/RotateImageView;

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public startFaceDetection()V
    .locals 0

    return-void
.end method

.method public stopFaceDetection()V
    .locals 0

    return-void
.end method

.method public supportPanorama3dSwitch()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    if-nez v1, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedCaptureMode()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public updateRemainSpace()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected updateThumbnailView()V
    .locals 2

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/android/camera/ActivityBase;->updateThumbnailView()V

    goto :goto_0
.end method
