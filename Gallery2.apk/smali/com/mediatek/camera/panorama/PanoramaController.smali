.class public Lcom/mediatek/camera/panorama/PanoramaController;
.super Ljava/lang/Object;
.source "PanoramaController.java"

# interfaces
.implements Landroid/hardware/Camera$AUTORAMACallback;
.implements Landroid/hardware/Camera$AUTORAMAMVCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;
    }
.end annotation


# static fields
.field static final ANIMATION:Z = true

.field private static final COMPUTE_METHOD2:Z = true

.field private static final DEBUG:Z = true

.field private static final DIRECTION_DOWN:I = 0x3

.field private static final DIRECTION_LEFT:I = 0x1

.field private static final DIRECTION_RIGHT:I = 0x0

.field private static final DIRECTION_UNKNOWN:I = 0x4

.field private static final DIRECTION_UP:I = 0x2

.field private static final IDLE:I = 0x0

.field private static final MERGING:I = 0x2

.field private static final NUM_3D_AUTORAMA_CAPTURE:I = 0x12

.field private static final NUM_AUTORAMA_CAPTURE:I = 0x9

.field private static final PANO_3D_DISTANCE_VERTICAL:I = 0xf0

.field private static final PANO_3D_OVERLAP_DISTANCE:I = 0x20

.field private static final STARTED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PanoramaController"

.field private static final TARGET_DISTANCE_HORIZONTAL:I = 0xa0

.field private static final TARGET_DISTANCE_VERTICAL:I = 0x78


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

.field private mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

.field public mCancelOnClickListener:Landroid/view/View$OnClickListener;

.field private mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

.field private mCenterIndicator:Landroid/view/ViewGroup;

.field private mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

.field private mCurrentNum:I

.field private mDirectionSigns:[Landroid/view/ViewGroup;

.field private mDisplayDirection:I

.field private mDisplayMatrix:Landroid/graphics/Matrix;

.field private mDisplayOrientaion:I

.field private mDisplayRotation:I

.field private mDistanceHorizontal:I

.field private mDistanceVertical:I

.field private mHalfArrowHeight:I

.field private mHalfArrowLength:I

.field private mHandler:Landroid/os/Handler;

.field private mLock:Ljava/lang/Object;

.field private mNaviWindow:Landroid/view/View;

.field private mNormalWindowDrawable:Landroid/graphics/drawable/Drawable;

.field public mOkOnClickListener:Landroid/view/View$OnClickListener;

.field private mPanoView:Landroid/view/View;

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

.field private mSensorDirection:I

.field private mSensorMatrix:[Landroid/graphics/Matrix;

.field private mShowingCollimatedDrawable:Z

.field private mState:I

.field private mStopProcess:Z

.field private mStopping:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v2, [Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayDirection:I

    iput-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopProcess:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mLock:Ljava/lang/Object;

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaController$1;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaController$1;-><init>(Lcom/mediatek/camera/panorama/PanoramaController;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mOkOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/camera/panorama/PanoramaController$2;

    invoke-direct {v0, p0}, Lcom/mediatek/camera/panorama/PanoramaController$2;-><init>(Lcom/mediatek/camera/panorama/PanoramaController;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCancelOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    :goto_0
    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xf0

    :goto_1
    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->initializeViews(Landroid/app/Activity;)V

    new-instance v0, Lcom/mediatek/camera/ui/ProgressIndicator;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iput-object p2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    const/16 v0, 0xa0

    goto :goto_0

    :cond_1
    const/16 v0, 0x78

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/mediatek/camera/panorama/PanoramaController;)Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/camera/panorama/PanoramaController;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->doStop(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/camera/panorama/PanoramaController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/camera/panorama/PanoramaController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopping:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/camera/panorama/PanoramaController;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->onHardwareStopped(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/camera/panorama/PanoramaController;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/camera/panorama/PanoramaController;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/camera/panorama/PanoramaController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopProcess:Z

    return p1
.end method

.method static synthetic access$702(Lcom/mediatek/camera/panorama/PanoramaController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mShowingCollimatedDrawable:Z

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/camera/panorama/PanoramaController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/camera/panorama/PanoramaController;)Lcom/mediatek/camera/panorama/AnimationController;
    .locals 1
    .param p0    # Lcom/mediatek/camera/panorama/PanoramaController;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    return-object v0
.end method

.method private doStart()V
    .locals 3

    const/16 v2, 0x9

    const-string v0, "PanoramaController"

    const-string v1, "doStart"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v2}, Lcom/android/camera/CameraManager$CameraProxy;->start3DSHOT(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v2}, Lcom/android/camera/CameraManager$CameraProxy;->startAUTORAMA(I)V

    goto :goto_0
.end method

.method private doStop(Z)V
    .locals 6
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "PanoramaController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doStop isMerge "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraHolder;->isSameCameraDevice(Lcom/android/camera/CameraManager$CameraProxy;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/camera/CameraManager$CameraProxy;->stop3DSHOT(I)V

    :goto_1
    monitor-exit v0

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lcom/android/camera/CameraManager$CameraProxy;->stopAUTORAMA(I)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    :try_start_1
    const-string v1, "PanoramaController"

    const-string v2, "doStop device is release? "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private initializeViews(Landroid/app/Activity;)V
    .locals 5
    .param p1    # Landroid/app/Activity;

    const/4 v4, 0x0

    const v1, 0x7f0b0085

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPanoView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    const v1, 0x7f0b0098

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    aput-object v1, v2, v4

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    const/4 v3, 0x1

    const v1, 0x7f0b0093

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    const/4 v3, 0x2

    const v1, 0x7f0b009d

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    const/4 v3, 0x3

    const v1, 0x7f0b00a0

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    aput-object v1, v2, v3

    const v1, 0x7f0b005b

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    const v1, 0x7f0b00a5

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    new-instance v2, Lcom/mediatek/camera/panorama/AnimationController;

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v1}, Lcom/mediatek/camera/panorama/AnimationController;-><init>([Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNormalWindowDrawable:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0b0092

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->prepareSensorMatrix()V

    return-void
.end method

.method private onHardwareStopped(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const-string v0, "PanoramaController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHardwareStopped isMerge: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v3}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    invoke-interface {v0, p1}, Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;->onCaptureDone(Z)V

    :cond_1
    return-void
.end method

.method private resetDirectionIcons()V
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v4

    if-ne v2, v4, :cond_0

    const/4 v2, 0x3

    :goto_0
    invoke-virtual {p0, v2}, Lcom/mediatek/camera/panorama/PanoramaController;->setOrientationIndicator(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v2}, Lcom/mediatek/camera/panorama/AnimationController;->startCenterAnimation()V

    const/16 v1, 0x8

    :goto_1
    const/4 v0, 0x0

    :goto_2
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private stopAsync(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const-string v1, "PanoramaController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopAsync mStopping: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopping:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopping:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopping:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/camera/panorama/PanoramaController$3;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController$3;-><init>(Lcom/mediatek/camera/panorama/PanoramaController;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopProcess:Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private update3DDirection(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    if-eq p1, v3, :cond_0

    rsub-int/lit8 p1, p1, 0x3

    :cond_0
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/PanoramaActivity;->movingTips()V

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/mediatek/camera/panorama/PanoramaController;->setOrientationIndicator(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/AnimationController;->startCenterAnimation()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/AnimationController;->startDirectionAnimation()V

    goto :goto_0
.end method

.method private updateDirection(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x4

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    if-nez p1, :cond_2

    :cond_0
    rsub-int/lit8 p1, p1, 0x3

    :cond_1
    :goto_0
    const-string v1, "PanoramaController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateDirection mDirection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " direction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    if-eq v1, p1, :cond_4

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    if-eq v1, v4, :cond_3

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/mediatek/camera/panorama/PanoramaActivity;

    invoke-virtual {v1}, Lcom/mediatek/camera/panorama/PanoramaActivity;->movingTips()V

    invoke-virtual {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->setOrientationIndicator(I)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v1}, Lcom/mediatek/camera/panorama/AnimationController;->startCenterAnimation()V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_4

    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 p1, p1, -0x2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void
.end method

.method private waitLock()V
    .locals 3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "PanoramaController"

    const-string v2, "InterruptedException in waitLock"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public checkStopProcess()V
    .locals 1

    :goto_0
    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopProcess:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->waitLock()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public clampCenterXY([F)V
    .locals 4
    .param p1    # [F

    const/4 v3, 0x1

    const/4 v2, 0x0

    aget v0, p1, v2

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    int-to-float v0, v0

    aput v0, p1, v2

    :cond_0
    aget v0, p1, v2

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    aput v0, p1, v2

    :cond_1
    aget v0, p1, v3

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v0, v0

    aput v0, p1, v2

    :cond_2
    aget v0, p1, v3

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    aput v0, p1, v2

    :cond_3
    return-void
.end method

.method public getArrowHL()V
    .locals 3

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    if-le v1, v0, :cond_1

    shr-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    shr-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    shr-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    shr-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    goto :goto_0
.end method

.method public hasCaptured()Z
    .locals 3

    const-string v0, "PanoramaController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasCaptured mCurrentNum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCapture()V
    .locals 6

    const/4 v3, 0x0

    const/16 v5, 0x9

    const/4 v4, 0x1

    const-string v0, "PanoramaController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCapture: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-eq v0, v5, :cond_2

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_2
    const-string v0, "PanoramaController"

    const-string v1, "autorama done"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    invoke-direct {p0, v4}, Lcom/mediatek/camera/panorama/PanoramaController;->onHardwareStopped(Z)V

    :cond_3
    :goto_1
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-ne v0, v5, :cond_0

    invoke-virtual {p0, v4}, Lcom/mediatek/camera/panorama/PanoramaController;->stop(Z)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-ltz v0, :cond_8

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-ge v0, v5, :cond_8

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-nez v0, :cond_5

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/AnimationController;->startDirectionAnimation()V

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-ne v0, v4, :cond_6

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/mediatek/camera/panorama/PanoramaActivity;

    iget-object v0, v0, Lcom/mediatek/camera/panorama/PanoramaActivity;->mDoneButton:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v4}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    :cond_6
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/AnimationController;->stopCenterAnimation()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mShowingCollimatedDrawable:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_7
    iput-boolean v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mShowingCollimatedDrawable:Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/camera/panorama/PanoramaController$4;

    invoke-direct {v1, p0}, Lcom/mediatek/camera/panorama/PanoramaController$4;-><init>(Lcom/mediatek/camera/panorama/PanoramaController;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_8
    const-string v0, "PanoramaController"

    const-string v1, "onCapture is called in abnormal state"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onFrame(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x4

    const/4 v3, 0x1

    if-eq p2, v4, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mShowingCollimatedDrawable:Z

    if-nez v2, :cond_0

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    if-ge v2, v3, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const/high16 v2, -0x10000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    int-to-short v0, v2

    const v2, 0xffff

    and-int/2addr v2, p1

    int-to-short v1, v2

    invoke-virtual {p0, v0, v1, p2}, Lcom/mediatek/camera/panorama/PanoramaController;->updateUIShowingMatrix(III)V

    goto :goto_0
.end method

.method public prepareSensorMatrix()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/high16 v3, -0x40800000

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v2

    const/4 v1, 0x0

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v4

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v4

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v5

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v6

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v6

    iget v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    int-to-float v1, v1

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method public prepareTransformMatrix(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x0

    const/high16 v7, 0x40000000

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    shr-int/lit8 v1, v4, 0x1

    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    shr-int/lit8 v0, v4, 0x1

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->getArrowHL()V

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v4

    if-eqz v4, :cond_0

    const/high16 v3, 0x43000000

    :goto_0
    int-to-float v4, v0

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v5, v5

    sub-float v2, v4, v5

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    int-to-float v5, v5

    div-float v5, v3, v5

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    int-to-float v6, v6

    div-float v6, v2, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    sparse-switch v4, :sswitch_data_0

    :goto_1
    :sswitch_0
    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v5, v5

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void

    :cond_0
    int-to-float v4, v1

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    int-to-float v5, v5

    sub-float v3, v4, v5

    goto :goto_0

    :sswitch_1
    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v2

    mul-float/2addr v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x42b40000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    :sswitch_2
    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v3

    mul-float/2addr v5, v7

    neg-float v6, v2

    mul-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x43340000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    :sswitch_3
    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v2

    mul-float/2addr v5, v7

    invoke-virtual {v4, v5, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, -0x3d4c0000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public resetController()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x4

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPanoView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setCamera(Lcom/android/camera/CameraManager$CameraProxy;)V
    .locals 0
    .param p1    # Lcom/android/camera/CameraManager$CameraProxy;

    iput-object p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    return-void
.end method

.method public setDisplayOrientation(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    iput p2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayRotation:I

    return-void
.end method

.method public setOrientation(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    return-void
.end method

.method public setOrientationIndicator(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x10e

    const/16 v4, 0xb4

    const/16 v3, 0x5a

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v2, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v2, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    const/high16 v1, -0x3d4c0000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v4, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v4, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    const/high16 v1, 0x42b40000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v3, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v3, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    const/high16 v1, 0x43340000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v5, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v5, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0
.end method

.method public setPreviewSize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    iput p2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    return-void
.end method

.method public start()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mStopping:Z

    if-nez v2, :cond_0

    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    iput v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCurrentNum:I

    iput-boolean v1, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mShowingCollimatedDrawable:Z

    const/4 v2, 0x4

    iput v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->doStart()V

    invoke-direct {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->resetDirectionIcons()V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPanoView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v2, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "PanoramaController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x4

    const-string v0, "PanoramaController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mState:I

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v5}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v5}, Lcom/android/camera/CameraManager$CameraProxy;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/mediatek/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mProgressIndicator:Lcom/mediatek/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v4}, Lcom/mediatek/camera/ui/ProgressIndicator;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/mediatek/camera/panorama/PanoramaController;->stopAsync(Z)V

    iput v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorDirection:I

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mAnimation:Lcom/mediatek/camera/panorama/AnimationController;

    invoke-virtual {v0}, Lcom/mediatek/camera/panorama/AnimationController;->stopCenterAnimation()V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPanoView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mCaptureEventListener:Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;

    invoke-interface {v0}, Lcom/mediatek/camera/panorama/PanoramaController$CaptureEventListener;->onMergeStarted()V

    goto :goto_1
.end method

.method public updateMatrix()V
    .locals 1

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    :goto_0
    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceHorizontal:I

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xf0

    :goto_1
    iput v0, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDistanceVertical:I

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->prepareSensorMatrix()V

    return-void

    :cond_0
    const/16 v0, 0xa0

    goto :goto_0

    :cond_1
    const/16 v0, 0x78

    goto :goto_1
.end method

.method public updateUIShowing(III)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v7, 0xb4

    const/4 v9, 0x4

    const/4 v11, 0x0

    invoke-static {}, Lcom/android/camera/Util;->getS3DMode()Z

    move-result v6

    if-eqz v6, :cond_5

    if-ne p3, v9, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    if-ne v6, v7, :cond_2

    rsub-int/lit8 p3, p3, 0x1

    neg-int v6, p1

    int-to-short p1, v6

    neg-int v6, p2

    int-to-short p2, v6

    :cond_2
    const-string v6, "PanoramaController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "direction "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " y: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    shr-int/lit8 v1, v6, 0x1

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    shr-int/lit8 v0, v6, 0x1

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    shr-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    shr-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    :cond_3
    const/4 v5, 0x0

    const/4 v4, 0x0

    if-eq p3, v9, :cond_4

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    iget v7, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int/2addr v6, v7

    mul-int/lit8 v7, p1, 0x2

    sub-int v5, v6, v7

    mul-int v6, v0, p2

    div-int/lit8 v6, v6, 0x78

    add-int v4, v0, v6

    :cond_4
    const-string v6, "PanoramaController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Navi Window, CenterX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " CenterY "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    iget v7, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    sub-int v7, v4, v7

    iget v8, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int v8, v5, v8

    iget v9, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    add-int/2addr v9, v4

    iget v10, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    add-int/2addr v10, v5

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, p3}, Lcom/mediatek/camera/panorama/PanoramaController;->update3DDirection(I)V

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayOrientaion:I

    if-ne v6, v7, :cond_7

    const/4 v6, 0x1

    if-eq p3, v6, :cond_6

    if-nez p3, :cond_8

    :cond_6
    rsub-int/lit8 p3, p3, 0x1

    :goto_1
    neg-int v6, p1

    int-to-short p1, v6

    neg-int v6, p2

    int-to-short p2, v6

    :cond_7
    const-string v6, "PanoramaController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "direction "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " y: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    shr-int/lit8 v1, v6, 0x1

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewHeight:I

    shr-int/lit8 v0, v6, 0x1

    invoke-virtual {p0}, Lcom/mediatek/camera/panorama/PanoramaController;->getArrowHL()V

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int v2, v1, v6

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int v3, v0, v6

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayRotation:I

    if-nez v6, :cond_9

    packed-switch p3, :pswitch_data_0

    :goto_2
    const-string v6, "PanoramaController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Navi Window, CenterX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " CenterY "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    iget v7, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    sub-int v7, v4, v7

    iget v8, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int v8, v5, v8

    iget v9, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    add-int/2addr v9, v4

    iget v10, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    add-int/2addr v10, v5

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, p3}, Lcom/mediatek/camera/panorama/PanoramaController;->updateDirection(I)V

    iget-object v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    rsub-int/lit8 p3, p3, 0x5

    goto/16 :goto_1

    :pswitch_0
    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mPreviewWidth:I

    iget v7, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int/2addr v6, v7

    mul-int v7, v2, p1

    div-int/lit16 v7, v7, 0xa0

    sub-int v5, v6, v7

    mul-int v6, v3, p2

    div-int/lit8 v6, v6, 0x78

    add-int v4, v0, v6

    goto :goto_2

    :pswitch_1
    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    mul-int v7, v2, p1

    div-int/lit16 v7, v7, 0xa0

    sub-int v5, v6, v7

    mul-int v6, v3, p2

    div-int/lit8 v6, v6, 0x78

    add-int v4, v0, v6

    goto :goto_2

    :pswitch_2
    mul-int v6, v2, p1

    div-int/lit16 v6, v6, 0xa0

    sub-int v5, v1, v6

    add-int v6, v0, v3

    mul-int v7, v3, p2

    div-int/lit8 v7, v7, 0x78

    add-int v4, v6, v7

    goto :goto_2

    :pswitch_3
    mul-int v6, v2, p1

    div-int/lit16 v6, v6, 0xa0

    sub-int v5, v1, v6

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    mul-int v7, v3, p2

    div-int/lit8 v7, v7, 0x78

    add-int v4, v6, v7

    goto/16 :goto_2

    :cond_9
    packed-switch p3, :pswitch_data_1

    goto/16 :goto_2

    :pswitch_4
    add-int v6, v1, v2

    mul-int v7, v2, p1

    div-int/lit16 v7, v7, 0xa0

    sub-int v4, v6, v7

    mul-int v6, v3, p2

    div-int/lit8 v6, v6, 0x78

    sub-int v5, v0, v6

    goto/16 :goto_2

    :pswitch_5
    sub-int v6, v1, v2

    mul-int v7, v2, p1

    div-int/lit16 v7, v7, 0xa0

    sub-int v4, v6, v7

    mul-int v6, v3, p2

    div-int/lit8 v6, v6, 0x78

    sub-int v5, v0, v6

    goto/16 :goto_2

    :pswitch_6
    mul-int v6, v2, p1

    div-int/lit16 v6, v6, 0xa0

    sub-int v4, v1, v6

    sub-int v6, v0, v3

    mul-int v7, v3, p2

    div-int/lit8 v7, v7, 0x78

    sub-int v5, v6, v7

    goto/16 :goto_2

    :pswitch_7
    mul-int v6, v2, p1

    div-int/lit16 v6, v6, 0xa0

    sub-int v4, v1, v6

    add-int v6, v0, v3

    mul-int v7, v3, p2

    div-int/lit8 v7, v7, 0x78

    sub-int v5, v6, v7

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public updateUIShowingMatrix(III)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x2

    new-array v2, v3, [F

    int-to-float v3, p1

    aput v3, v2, v8

    int-to-float v3, p2

    aput v3, v2, v6

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v3, v3, p3

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    const-string v3, "PanoramaController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Matrix x = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p3}, Lcom/mediatek/camera/panorama/PanoramaController;->prepareTransformMatrix(I)V

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    const-string v3, "PanoramaController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DisplayMatrix x = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget v3, v2, v8

    float-to-int v0, v3

    aget v3, v2, v6

    float-to-int v1, v3

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    iget v4, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    sub-int v5, v1, v5

    iget v6, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowHeight:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mHalfArrowLength:I

    add-int/2addr v7, v1

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, p3}, Lcom/mediatek/camera/panorama/PanoramaController;->updateDirection(I)V

    iget-object v3, p0, Lcom/mediatek/camera/panorama/PanoramaController;->mNaviWindow:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
