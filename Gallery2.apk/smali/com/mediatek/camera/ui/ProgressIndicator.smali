.class public Lcom/mediatek/camera/ui/ProgressIndicator;
.super Ljava/lang/Object;
.source "ProgressIndicator.java"


# static fields
.field public static final BLOCK_NUMBERS:I = 0x9

.field public static final BLOCK_NUMBERS_SINGLE3D:I = 0x2

.field private static final HEIGHT_WITHOUT_ZOOMBAR:I = 0x0

.field private static final HEIGHT_WITH_ZOOMBAR:I = 0x28

.field public static final MAV_CAPTURE_NUM:I = 0xf

.field public static final PANORAMA_CAPTURE_NUM:I = 0x9

.field private static final TAG:Ljava/lang/String; = "ProgressIndicator"

.field public static final TYPE_MAV:I = 0x1

.field public static final TYPE_PANO:I = 0x2

.field public static final TYPE_SINGLE3D:I = 0x3


# instance fields
.field private mAnimation:Z

.field private mBlockPadding:I

.field private final mMavBlockSizes:[I

.field private final mOrientation:I

.field private final mPanoBlockSizes:[I

.field private mProgressBars:Landroid/widget/ImageView;

.field private mProgressView:Landroid/view/View;

.field private final mSingle3DBlockSizes:[I


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 9
    .param p1    # Landroid/app/Activity;
    .param p2    # I

    const/4 v8, 0x0

    const/4 v5, 0x2

    const/high16 v4, 0x3f800000

    const/16 v7, 0x9

    const/high16 v6, 0x3f000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v3, v7, [I

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mPanoBlockSizes:[I

    new-array v3, v7, [I

    fill-array-data v3, :array_1

    iput-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mMavBlockSizes:[I

    new-array v3, v5, [I

    fill-array-data v3, :array_2

    iput-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mSingle3DBlockSizes:[I

    const/4 v3, 0x4

    iput v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    const v3, 0x7f0b00a7

    invoke-virtual {p1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressView:Landroid/view/View;

    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0b00a8

    invoke-virtual {p1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    iput v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mOrientation:I

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->density:F

    const/4 v3, 0x1

    if-ne p2, v3, :cond_2

    cmpl-float v3, v2, v4

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    add-float/2addr v3, v6

    float-to-int v3, v3

    iput v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mMavBlockSizes:[I

    iget-object v4, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mMavBlockSizes:[I

    aget v4, v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, v2

    add-float/2addr v4, v6

    float-to-int v4, v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    new-instance v4, Lcom/mediatek/camera/ui/ProgressBarDrawable;

    iget-object v5, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mMavBlockSizes:[I

    iget v7, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    invoke-direct {v4, p1, v5, v6, v7}, Lcom/mediatek/camera/ui/ProgressBarDrawable;-><init>(Landroid/content/Context;Landroid/view/View;[II)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_1
    iget-boolean v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mAnimation:Z

    invoke-virtual {p0, v8, v3}, Lcom/mediatek/camera/ui/ProgressIndicator;->setOrientation(IZ)V

    return-void

    :cond_2
    if-ne p2, v5, :cond_4

    cmpl-float v3, v2, v4

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    add-float/2addr v3, v6

    float-to-int v3, v3

    iput v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v7, :cond_3

    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mPanoBlockSizes:[I

    iget-object v4, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mPanoBlockSizes:[I

    aget v4, v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, v2

    add-float/2addr v4, v6

    float-to-int v4, v4

    aput v4, v3, v0

    const-string v3, "ProgressIndicator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mPanoBlockSizes[i]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mPanoBlockSizes:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    new-instance v4, Lcom/mediatek/camera/ui/ProgressBarDrawable;

    iget-object v5, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mPanoBlockSizes:[I

    iget v7, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    invoke-direct {v4, p1, v5, v6, v7}, Lcom/mediatek/camera/ui/ProgressBarDrawable;-><init>(Landroid/content/Context;Landroid/view/View;[II)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_4
    const/4 v3, 0x3

    if-ne p2, v3, :cond_1

    cmpl-float v3, v2, v4

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    add-float/2addr v3, v6

    float-to-int v3, v3

    iput v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v5, :cond_5

    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mSingle3DBlockSizes:[I

    iget-object v4, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mSingle3DBlockSizes:[I

    aget v4, v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, v2

    add-float/2addr v4, v6

    float-to-int v4, v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    new-instance v4, Lcom/mediatek/camera/ui/ProgressBarDrawable;

    iget-object v5, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mSingle3DBlockSizes:[I

    iget v7, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mBlockPadding:I

    invoke-direct {v4, p1, v5, v6, v7}, Lcom/mediatek/camera/ui/ProgressBarDrawable;-><init>(Landroid/content/Context;Landroid/view/View;[II)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    nop

    :array_0
    .array-data 4
        0x11
        0xf
        0xd
        0xc
        0xb
        0xc
        0xd
        0xf
        0x11
    .end array-data

    :array_1
    .array-data 4
        0xb
        0xc
        0xd
        0xf
        0x11
        0xf
        0xd
        0xc
        0xb
    .end array-data

    :array_2
    .array-data 4
        0xb
        0xb
    .end array-data
.end method


# virtual methods
.method public setOrientation(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    iput-boolean p2, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mAnimation:Z

    iget-object v1, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressView:Landroid/view/View;

    check-cast v1, Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0012

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v4

    if-ne v3, v4, :cond_0

    if-eqz p1, :cond_1

    const/16 v3, 0xb4

    if-eq p1, v3, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x5a

    if-eq p1, v3, :cond_1

    const/16 v3, 0x10e

    if-ne p1, v3, :cond_2

    :cond_1
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    const/16 v6, 0x28

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_2
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 3
    .param p1    # I

    const-string v0, "ProgressIndicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProgress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressBars:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageLevel(I)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/ui/ProgressIndicator;->mProgressView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
