.class public Lcom/mediatek/gallery3d/ext/MovieListLoader;
.super Ljava/lang/Object;
.source "MovieListLoader.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ext/IMovieListLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;
    }
.end annotation


# static fields
.field private static final LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "MovieListLoader"


# instance fields
.field private mListTask:Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelList()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/MovieListLoader;->mListTask:Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/MovieListLoader;->mListTask:Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public fillVideoList(Landroid/content/Context;Landroid/content/Intent;Lcom/mediatek/gallery3d/ext/IMovieListLoader$LoaderListener;Lcom/mediatek/gallery3d/ext/IMovieItem;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/mediatek/gallery3d/ext/IMovieListLoader$LoaderListener;
    .param p4    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    const/4 v6, 0x0

    const/4 v3, 0x0

    const-string v0, "mediatek.intent.extra.ALL_VIDEO_FOLDER"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mediatek.intent.extra.ALL_VIDEO_FOLDER"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    :cond_0
    const-string v5, "datetaken DESC, _id DESC "

    const-string v0, "mediatek.intent.extra.VIDEO_LIST_ORDERBY"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "mediatek.intent.extra.VIDEO_LIST_ORDERBY"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/MovieListLoader;->cancelList()V

    new-instance v0, Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;-><init>(Lcom/mediatek/gallery3d/ext/MovieListLoader;Landroid/content/Context;ZLcom/mediatek/gallery3d/ext/IMovieListLoader$LoaderListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ext/MovieListLoader;->mListTask:Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/MovieListLoader;->mListTask:Lcom/mediatek/gallery3d/ext/MovieListLoader$MovieListFetcherTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/mediatek/gallery3d/ext/IMovieItem;

    aput-object p4, v1, v6

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const-string v0, "MovieListLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fillVideoList() fetechAll="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", orderBy="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public isEnabledVideoList(Landroid/content/Intent;)Z
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const-string v1, "mediatek.intent.extra.ENABLE_VIDEO_LIST"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mediatek.intent.extra.ENABLE_VIDEO_LIST"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    const-string v1, "MovieListLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isEnabledVideoList() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method
