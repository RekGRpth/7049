.class public Lcom/mediatek/gallery3d/jps/JpsHelper;
.super Ljava/lang/Object;
.source "JpsHelper.java"


# static fields
.field public static final FILE_EXTENSION:Ljava/lang/String; = "jps"

.field public static final MIME_TYPE:Ljava/lang/String; = "image/x-jps"

.field private static final TAG:Ljava/lang/String; = "JpsHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adjustRect(IZLandroid/graphics/Rect;)V
    .locals 5
    .param p0    # I
    .param p1    # Z
    .param p2    # Landroid/graphics/Rect;

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eq v3, p0, :cond_0

    if-ne v4, p0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    if-eq v4, p0, :cond_1

    const/16 v3, 0x10

    if-ne v3, p0, :cond_2

    :cond_1
    if-nez p1, :cond_4

    move p1, v1

    :cond_2
    :goto_1
    invoke-static {v0, p1, p2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustRect(ZZLandroid/graphics/Rect;)V

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move p1, v2

    goto :goto_1
.end method
