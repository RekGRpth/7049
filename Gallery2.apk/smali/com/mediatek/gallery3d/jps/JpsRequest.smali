.class public Lcom/mediatek/gallery3d/jps/JpsRequest;
.super Ljava/lang/Object;
.source "JpsRequest.java"

# interfaces
.implements Lcom/mediatek/gallery3d/data/IMediaRequest;


# static fields
.field private static final TAG:Ljava/lang/String; = "JpsRequest"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private retrieveLargeData(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # I
    .param p3    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p4    # Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .param p5    # Landroid/graphics/BitmapRegionDecoder;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "JpsRequest"

    const-string v5, "retrieveLargeData:job cancelled"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    if-nez p5, :cond_3

    :cond_2
    const-string v4, "JpsRequest"

    const-string v5, "retrieveLargeData:invalid parameters"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v6

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v7

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x1

    invoke-static {p2, v4, v1}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/high16 v4, 0xa00000

    const/4 v5, -0x1

    iget v6, v1, Landroid/graphics/Rect;->right:I

    iget v7, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    iget v8, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/mediatek/gallery3d/data/DecodeHelper;->calculateSampleSize(IIII)I

    move-result v4

    iput v4, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFullFrame:Z

    if-nez v4, :cond_4

    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    if-eqz v4, :cond_5

    :cond_4
    invoke-static {p1, p5, v1, v2}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_6

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "JpsRequest"

    const-string v5, "retrieveLargeData:first:job cancelled"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    :cond_5
    :goto_1
    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFullFrame:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v6

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v7

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v4, 0x0

    invoke-static {p2, v4, v1}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    invoke-static {p1, p5, v1, v2}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_8

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "JpsRequest"

    const-string v5, "retrieveLargeData:second:job cancelled"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v4, 0x1

    invoke-static {p1, v0, v4}, Lcom/mediatek/gallery3d/data/DecodeHelper;->getRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)Lcom/mediatek/gallery3d/data/RegionDecoder;

    move-result-object v3

    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFullFrame:Z

    if-eqz v4, :cond_7

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    :cond_7
    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    if-eqz v4, :cond_5

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    goto :goto_1

    :cond_8
    const/4 v4, 0x1

    invoke-static {p1, v0, v4}, Lcom/mediatek/gallery3d/data/DecodeHelper;->getRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)Lcom/mediatek/gallery3d/data/RegionDecoder;

    move-result-object v4

    iput-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    goto/16 :goto_0
.end method

.method private retrieveStereoConvergence(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # I
    .param p3    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p4    # Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .param p5    # Landroid/graphics/BitmapRegionDecoder;

    const/4 v8, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "JpsRequest"

    const-string v5, "retrieveStereoConvergence:job cancelled"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    if-nez p5, :cond_3

    :cond_2
    const-string v4, "JpsRequest"

    const-string v5, "retrieveStereoConvergence:invalid parameters"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v4

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v5

    invoke-direct {v1, v8, v8, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x1

    invoke-static {p2, v4, v1}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iget v4, v1, Landroid/graphics/Rect;->right:I

    iget v5, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    iget v6, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    iget v6, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v7, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v4, v5, v6, v7}, Lcom/mediatek/gallery3d/data/DecodeHelper;->calculateSampleSizeByType(IIII)I

    move-result v4

    iput v4, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-boolean v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_5

    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_5

    invoke-static {p1, p5, v1, v2}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v5, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v0, v4, v5}, Lcom/mediatek/gallery3d/data/DecodeHelper;->postScaleDown(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_7

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v4

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v5

    invoke-virtual {v1, v8, v8, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {p2, v8, v1}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    invoke-static {p1, p5, v1, v2}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iget v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v5, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v3, v4, v5}, Lcom/mediatek/gallery3d/data/DecodeHelper;->postScaleDown(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    :goto_2
    invoke-static {v0, v3}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->getInstance(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    move-result-object v4

    iput-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->stereoConvergence:Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_4

    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_6

    iget-object v0, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_6
    iget-object v0, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_7
    iget-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    goto :goto_2
.end method

.method private retrieveThumbData(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V
    .locals 8
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # I
    .param p3    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p4    # Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .param p5    # Landroid/graphics/BitmapRegionDecoder;

    const/4 v7, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "JpsRequest"

    const-string v4, "retrieveThumbData:job cancelled"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    if-nez p5, :cond_3

    :cond_2
    const-string v3, "JpsRequest"

    const-string v4, "retrieveThumbData:invalid parameters"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v3

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v4

    invoke-direct {v0, v7, v7, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v3, 0x1

    invoke-static {p2, v3, v0}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    iget v5, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v6, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v3, v4, v5, v6}, Lcom/mediatek/gallery3d/data/DecodeHelper;->calculateSampleSizeByType(IIII)I

    move-result v3

    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-boolean v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    iget-boolean v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    if-eqz v3, :cond_4

    invoke-static {p1, p5, v0, v1}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    iget-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    iget v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v5, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v3, v4, v5}, Lcom/mediatek/gallery3d/data/DecodeHelper;->postScaleDown(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    :cond_4
    iget-boolean v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    if-eqz v3, :cond_6

    const/4 v2, 0x0

    iget-object v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inInputDataBundle:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v3, :cond_5

    iget-object v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inInputDataBundle:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v3, v3, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_5

    iget-object v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inInputDataBundle:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v3, v3, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inInputDataBundle:Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v2, v3, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->originalFrame:Landroid/graphics/Bitmap;

    :cond_5
    if-nez v2, :cond_7

    invoke-static {p1, p5, v0, v1}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v2, v3, v4}, Lcom/mediatek/gallery3d/data/DecodeHelper;->postScaleDown(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    :goto_1
    iput-object v2, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    :cond_6
    iget-boolean v3, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    if-eqz v3, :cond_0

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v3

    invoke-virtual {p5}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v4

    invoke-virtual {v0, v7, v7, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {p2, v7, v0}, Lcom/mediatek/gallery3d/jps/JpsHelper;->adjustRect(IZLandroid/graphics/Rect;)V

    invoke-static {p1, p5, v0, v1}, Lcom/mediatek/gallery3d/data/DecodeHelper;->safeDecodeImageRegion(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    iget-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    iget v4, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inType:I

    iget v5, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    invoke-static {v3, v4, v5}, Lcom/mediatek/gallery3d/data/DecodeHelper;->postScaleDown(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public request(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/graphics/BitmapRegionDecoder;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 6
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # I
    .param p3    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p4    # Landroid/graphics/BitmapRegionDecoder;

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "JpsRequest"

    const-string v1, "request:job cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v4

    :cond_0
    if-eqz p3, :cond_1

    if-nez p4, :cond_2

    :cond_1
    const-string v0, "JpsRequest"

    const-string v1, "request:got null params or decoder!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "JpsRequest"

    invoke-virtual {p3, v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->info(Ljava/lang/String;)V

    new-instance v4, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    invoke-direct {v4}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;-><init>()V

    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    if-nez v0, :cond_3

    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    if-nez v0, :cond_3

    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    if-eqz v0, :cond_4

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/jps/JpsRequest;->retrieveThumbData(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V

    :cond_4
    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inStereoConvergence:Z

    if-eqz v0, :cond_5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/jps/JpsRequest;->retrieveStereoConvergence(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V

    :cond_5
    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFullFrame:Z

    if-nez v0, :cond_6

    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    if-nez v0, :cond_6

    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFullFrame:Z

    if-eqz v0, :cond_7

    :cond_6
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/jps/JpsRequest;->retrieveLargeData(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;Landroid/graphics/BitmapRegionDecoder;)V

    :cond_7
    iget-boolean v0, p3, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inGifDecoder:Z

    if-eqz v0, :cond_8

    const-string v0, "JpsRequest"

    const-string v1, "request: no GifDecoder can be generated from jps"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const-string v0, "JpsRequest"

    invoke-virtual {v4, v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->info(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public request(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 9
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p3    # Landroid/content/ContentResolver;
    .param p4    # Landroid/net/Uri;

    const/4 v5, 0x0

    const-string v6, "JpsRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "request(jc, parmas, cr, uri="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const-string v6, "JpsRequest"

    const-string v7, "request:invalid parameters"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v5

    :cond_1
    const/4 v3, 0x0

    const/4 v1, 0x0

    :try_start_0
    const-string v6, "r"

    invoke-virtual {p3, p4, v6}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v1, v7}, Lcom/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v4

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2, p2, v4}, Lcom/mediatek/gallery3d/jps/JpsRequest;->request(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/graphics/BitmapRegionDecoder;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    throw v5
.end method

.method public request(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Ljava/lang/String;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 10
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p3    # Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "JpsRequest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "request(jc, parmas, filePath="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const-string v7, "JpsRequest"

    const-string v8, "request:invalid parameters"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v6

    :cond_1
    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v1, v8}, Lcom/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v5

    const/4 v4, 0x2

    invoke-virtual {p0, p1, v4, p2, v5}, Lcom/mediatek/gallery3d/jps/JpsRequest;->request(Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/graphics/BitmapRegionDecoder;)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_2
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v2, v3

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method public request(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;[BII)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 2
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p2    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p3    # [B
    .param p4    # I
    .param p5    # I

    const-string v0, "JpsRequest"

    const-string v1, "request:no support for buffer!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method
