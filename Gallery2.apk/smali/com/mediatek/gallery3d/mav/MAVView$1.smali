.class Lcom/mediatek/gallery3d/mav/MAVView$1;
.super Ljava/lang/Thread;
.source "MAVView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/mav/MAVView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/mav/MAVView;

.field final synthetic val$startX:I


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/mav/MAVView;I)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    iput p2, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->val$startX:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView;->access$102(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z

    :goto_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$100(Lcom/mediatek/gallery3d/mav/MAVView;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v3

    iget v4, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->val$startX:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/mav/MAVView;->access$300(Lcom/mediatek/gallery3d/mav/MAVView;)I

    move-result v4

    div-int v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    new-instance v4, Lcom/mediatek/gallery3d/mav/MAVView$1$1;

    invoke-direct {v4, p0, v2}, Lcom/mediatek/gallery3d/mav/MAVView$1$1;-><init>(Lcom/mediatek/gallery3d/mav/MAVView$1;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->isOverScrolled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$500(Lcom/mediatek/gallery3d/mav/MAVView;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v3

    float-to-int v0, v3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$600(Lcom/mediatek/gallery3d/mav/MAVView;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$700(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$700(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView;->access$502(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$500(Lcom/mediatek/gallery3d/mav/MAVView;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-virtual {v3}, Landroid/view/View;->postInvalidate()V

    :cond_1
    const-wide/16 v3, 0x14

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/mav/MAVView;->access$102(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z

    return-void

    :cond_3
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$600(Lcom/mediatek/gallery3d/mav/MAVView;)I

    move-result v3

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$800(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView;->access$800(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView;->access$502(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z

    goto :goto_1
.end method
