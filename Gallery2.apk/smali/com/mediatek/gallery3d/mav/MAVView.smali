.class public Lcom/mediatek/gallery3d/mav/MAVView;
.super Landroid/widget/ImageView;
.source "MAVView.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;
    }
.end annotation


# static fields
.field private static BASE_ANGLE:I = 0x0

.field private static final FLING_DIRECTION_INVALID:I = -0x1

.field private static final FLING_DIRECTION_LEFT:I = 0x1

.field private static final FLING_DIRECTION_RIGHT:I = 0x2

.field private static final NS2S:F = 1.0E-9f

.field private static final OFFSET:F = 0.0f

.field private static final TAG:Ljava/lang/String; = "MAVView"

.field private static final TH:F = 0.001f


# instance fields
.field private angle:[F

.field private mAbsorbingFling:Z

.field private mBaseMatrix:Landroid/graphics/Matrix;

.field private mBitmapArr:[Landroid/graphics/Bitmap;

.field private mDisplay:Landroid/view/Display;

.field private mDistDivider:I

.field private mEffectLeft:Landroid/widget/EdgeEffect;

.field private mEffectRight:Landroid/widget/EdgeEffect;

.field private mEnableTouchMode:Z

.field private mFirstShowBitmap:Landroid/graphics/Bitmap;

.field private mFirstTime:Z

.field private mFlingDirection:I

.field private mFlingOnGoing:Z

.field private mFlingThread:Ljava/lang/Thread;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHasDown:Z

.field private mImageRotation:I

.field private mLastIndex:I

.field private mLastTmpIndex:I

.field private mOrientation:I

.field private mOverScrollPoint:F

.field public mRectifySensorListener:Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;

.field private mRectifyValue:[F

.field private mResponsibility:Z

.field private mScrollDistance:F

.field private mScroller:Landroid/widget/OverScroller;

.field private mTouchThreshold:I

.field private mValue:F

.field private mWidth:I

.field private timestamp:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xf

    sput v0, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x3

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mRectifyValue:[F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstTime:Z

    const v0, 0xffff

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    new-instance v0, Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;-><init>(Lcom/mediatek/gallery3d/mav/MAVView;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mRectifySensorListener:Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->timestamp:F

    new-array v0, v4, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingDirection:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->initView(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x3

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mRectifyValue:[F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstTime:Z

    const v0, 0xffff

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    new-instance v0, Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;-><init>(Lcom/mediatek/gallery3d/mav/MAVView;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mRectifySensorListener:Lcom/mediatek/gallery3d/mav/MAVView$RectifySensorListener;

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->timestamp:F

    new-array v0, v4, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingDirection:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->initView(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/mav/MAVView;)[F
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mRectifyValue:[F

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/mav/MAVView;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/OverScroller;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScroller:Landroid/widget/OverScroller;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/mav/MAVView;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/mav/MAVView;I)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->scrollDeltaDistance(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/mav/MAVView;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/gallery3d/mav/MAVView;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/mav/MAVView;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingDirection:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/gallery3d/mav/MAVView;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method private checkBitmap(Landroid/graphics/Bitmap;)Z
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v4, 0x0

    if-nez p1, :cond_0

    const-string v3, "MAVView"

    const-string v4, "checkBitmap:in passed Bitmap is null!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v1, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v0, v3

    cmpg-float v3, v1, v4

    if-lez v3, :cond_1

    cmpg-float v3, v0, v4

    if-gtz v3, :cond_2

    :cond_1
    const-string v3, "MAVView"

    const-string v4, "checkBitmap:invalid dimension of Bitmap!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkSelf()Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v0, v2

    cmpg-float v2, v1, v3

    if-lez v2, :cond_0

    cmpg-float v2, v0, v3

    if-gtz v2, :cond_1

    :cond_0
    const-string v2, "MAVView"

    const-string v3, "checkSelf:invalid dimension of ImageView!"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private initView(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v0, Landroid/widget/OverScroller;

    invoke-direct {v0, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScroller:Landroid/widget/OverScroller;

    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-direct {v0, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-direct {v0, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView;->updateDimensionParams()V

    const-string v0, "MAVView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initView: initial rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private scrollDeltaDistance(I)V
    .locals 3
    .param p1    # I

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    div-int v0, p1, v2

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    add-int v1, v2, v0

    if-gez v1, :cond_2

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v1

    invoke-virtual {p0, v2}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v2, v2

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method private setBitmapMatrix(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1    # Landroid/graphics/Bitmap;

    const/high16 v13, 0x40000000

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView;->checkSelf()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->checkBitmap(Landroid/graphics/Bitmap;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    const-string v10, "MAVView"

    const-string v11, "setBitmapMatrix:either Bitmap or ImageView\'s dimen invalid"

    invoke-static {v10, v11}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v10}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v10

    int-to-float v7, v10

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    int-to-float v6, v10

    const/4 v8, 0x0

    const/4 v2, 0x0

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    div-int/lit8 v10, v10, 0x5a

    rem-int/lit8 v10, v10, 0x2

    if-eqz v10, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v8, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v2, v10

    :goto_1
    div-float v9, v7, v8

    div-float v3, v6, v2

    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    if-eqz v10, :cond_2

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v0, v10, 0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v1, v10, 0x2

    neg-int v10, v0

    int-to-float v10, v10

    neg-int v11, v1

    int-to-float v11, v11

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    int-to-float v10, v10

    invoke-virtual {v4, v10}, Landroid/graphics/Matrix;->postRotate(F)Z

    div-float v10, v8, v13

    div-float v11, v2, v13

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    :cond_2
    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    mul-float v11, v8, v5

    sub-float v11, v7, v11

    div-float/2addr v11, v13

    mul-float v12, v2, v5

    sub-float v12, v6, v12

    div-float/2addr v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v10}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v8, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_1
.end method

.method private updateDimensionParams()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    goto :goto_0
.end method


# virtual methods
.method public configChanged(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView;->updateDimensionParams()V

    const-string v0, "MAVView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "configChanged: new width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingDirection:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    const/high16 v3, 0x42b40000

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    const/high16 v3, -0x3d4c0000

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "MAVView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFling: vX="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", vY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    const/4 v1, 0x0

    cmpg-float v1, p3, v1

    if-gez v1, :cond_3

    const/4 v1, 0x2

    :goto_1
    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingDirection:I

    move/from16 v0, p3

    float-to-int v12, v0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingOnGoing:Z

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->finish()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->finish()V

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mAbsorbingFling:Z

    :cond_2
    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    mul-int/2addr v1, v3

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    mul-int v2, v1, v3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScroller:Landroid/widget/OverScroller;

    const/4 v3, 0x0

    neg-int v4, v12

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    new-instance v1, Lcom/mediatek/gallery3d/mav/MAVView$1;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/gallery3d/mav/MAVView$1;-><init>(Lcom/mediatek/gallery3d/mav/MAVView;I)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    const-string v3, "scroller-fling"

    invoke-virtual {v1, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lez v0, :cond_3

    const v0, 0xffff

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v1, :cond_0

    :goto_0
    return v4

    :cond_0
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    cmpg-float v1, p3, v3

    if-gez v1, :cond_1

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_2

    :cond_1
    cmpl-float v1, p3, v3

    if-lez v1, :cond_5

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_5

    :cond_2
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    iput v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    iput p3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    cmpg-float v1, p3, v3

    if-gez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_3
    :goto_1
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/mav/MAVView;->scrollDeltaDistance(I)V

    goto :goto_0

    :cond_4
    cmpl-float v1, p3, v3

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    add-float/2addr v1, p3

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    if-nez v1, :cond_8

    cmpg-float v1, p3, v3

    if-gez v1, :cond_8

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_8

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    :cond_6
    :goto_2
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mTouchThreshold:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDistDivider:I

    mul-int v0, v1, v2

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    if-nez v1, :cond_9

    cmpg-float v1, p3, v3

    if-gez v1, :cond_9

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_7
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/EdgeEffect;->onPull(F)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_1

    :cond_8
    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_6

    cmpl-float v1, p3, v3

    if-lez v1, :cond_6

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_6

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    goto :goto_2

    :cond_9
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_3

    cmpl-float v1, p3, v3

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_a
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mScrollDistance:F

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOverScrollPoint:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/EdgeEffect;->onPull(F)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13
    .param p1    # Landroid/hardware/SensorEvent;

    iget-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x3cf5c28f

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v2

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    if-eq v5, v2, :cond_2

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    const/4 v5, 0x0

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstTime:Z

    :cond_2
    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mOrientation:I

    packed-switch v5, :pswitch_data_0

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v3, v5, v6

    :goto_1
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3cf5c28f

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_0

    const/4 v5, 0x0

    add-float/2addr v5, v3

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->timestamp:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3a83126f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v5, v5

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->timestamp:F

    sub-float/2addr v5, v6

    const v6, 0x3089705f

    mul-float v0, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x1

    aget v7, v5, v6

    float-to-double v7, v7

    iget v9, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mValue:F

    mul-float/2addr v9, v0

    const/high16 v10, 0x43340000

    mul-float/2addr v9, v10

    float-to-double v9, v9

    const-wide v11, 0x400921fb54442d18L

    div-double/2addr v9, v11

    add-double/2addr v7, v9

    double-to-float v7, v7

    aput v7, v5, v6

    iget-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstTime:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v6

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstTime:Z

    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v6, v6

    mul-int/2addr v5, v6

    sget v6, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    mul-int/lit8 v6, v6, 0x2

    div-int v1, v5, v6

    if-ltz v1, :cond_5

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    const v6, 0xffff

    if-eq v5, v6, :cond_4

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    if-eq v5, v1, :cond_5

    :cond_4
    const-string v5, "MAVView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setImageBitmap: bitmap["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v7, v7

    sub-int/2addr v7, v1

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v6, v6

    sub-int/2addr v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    invoke-virtual {p0, v5}, Lcom/mediatek/gallery3d/mav/MAVView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    :cond_5
    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v5, v5

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->timestamp:F

    goto/16 :goto_0

    :pswitch_0
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x1

    aget v3, v5, v6

    goto/16 :goto_1

    :pswitch_1
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v3, v5, v6

    goto/16 :goto_1

    :pswitch_2
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    neg-float v3, v5

    goto/16 :goto_1

    :pswitch_3
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    neg-float v3, v5

    goto/16 :goto_1

    :cond_6
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v6

    goto/16 :goto_2

    :cond_7
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView;->angle:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView;->BASE_ANGLE:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_0
    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    goto :goto_1

    :pswitch_1
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_2
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    if-nez v1, :cond_3

    const/4 v0, 0x1

    iput-boolean v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    goto :goto_1

    :cond_3
    iput-boolean v3, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mHasDown:Z

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_4
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public recycleBitmapArr()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const-string v1, "MAVView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is recycled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setBitmapArr([Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # [Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    return-void
.end method

.method public setFirstShowBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView;->checkSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->checkBitmap(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "MAVView"

    const-string v1, "setImageBitmap:either Bitmap or ImageView\'s dimen invalid"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView;->setBitmapMatrix(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setImageRotation(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mImageRotation:I

    const-string v0, "MAVView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setImageRotation(rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setResponsibility(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mResponsibility:Z

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastIndex:I

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mLastTmpIndex:I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView;->updateDimensionParams()V

    :cond_0
    return-void
.end method

.method public setTouchModeEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView;->mEnableTouchMode:Z

    return-void
.end method
