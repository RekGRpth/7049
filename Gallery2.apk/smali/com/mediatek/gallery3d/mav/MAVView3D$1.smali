.class Lcom/mediatek/gallery3d/mav/MAVView3D$1;
.super Ljava/lang/Thread;
.source "MAVView3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/mav/MAVView3D;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

.field final synthetic val$startX:I


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/mav/MAVView3D;I)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    iput p2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->val$startX:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$402(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z

    :goto_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$400(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v3

    iget v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->val$startX:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v4}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$600(Lcom/mediatek/gallery3d/mav/MAVView3D;)I

    move-result v4

    div-int v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    new-instance v4, Lcom/mediatek/gallery3d/mav/MAVView3D$1$1;

    invoke-direct {v4, p0, v2}, Lcom/mediatek/gallery3d/mav/MAVView3D$1$1;-><init>(Lcom/mediatek/gallery3d/mav/MAVView3D$1;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->isOverScrolled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$800(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v3

    float-to-int v0, v3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$900(Lcom/mediatek/gallery3d/mav/MAVView3D;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$1000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$1000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$802(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$800(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$1200(Lcom/mediatek/gallery3d/mav/MAVView3D;)V

    :cond_1
    const-wide/16 v3, 0x14

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$402(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z

    return-void

    :cond_3
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$900(Lcom/mediatek/gallery3d/mav/MAVView3D;)I

    move-result v3

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$1100(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$1100(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v3, v5}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$802(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z

    goto :goto_1
.end method
