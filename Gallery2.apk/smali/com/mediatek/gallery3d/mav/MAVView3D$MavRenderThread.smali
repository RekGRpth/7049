.class Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;
.super Ljava/lang/Thread;
.source "MAVView3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/mav/MAVView3D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MavRenderThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;


# direct methods
.method private constructor <init>(Lcom/mediatek/gallery3d/mav/MAVView3D;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/gallery3d/mav/MAVView3D;Lcom/mediatek/gallery3d/mav/MAVView3D$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/mav/MAVView3D;
    .param p2    # Lcom/mediatek/gallery3d/mav/MAVView3D$1;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;-><init>(Lcom/mediatek/gallery3d/mav/MAVView3D;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :goto_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-boolean v2, v2, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderRequested:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderRequested:Z

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$100(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$200(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v2, v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->drawOnCanvas(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$200(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;->this$0:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->access$000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v1

    :try_start_3
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :cond_2
    :try_start_4
    const-string v2, "MAVView3D"

    const-string v3, "render is called before surface is ready!"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method
