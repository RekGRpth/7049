.class public Lcom/mediatek/gallery3d/mav/MAVView3D;
.super Landroid/view/SurfaceView;
.source "MAVView3D.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;
    }
.end annotation


# static fields
.field private static BASE_ANGLE:I = 0x0

.field private static final FLING_DIRECTION_INVALID:I = -0x1

.field private static final FLING_DIRECTION_LEFT:I = 0x1

.field private static final FLING_DIRECTION_RIGHT:I = 0x2

.field private static final NS2S:F = 1.0E-9f

.field private static final OFFSET:F = 0.0f

.field private static final TAG:Ljava/lang/String; = "MAVView3D"

.field private static final TARGET_FRAME_TIME:J = 0xaL

.field private static final TH:F = 0.001f


# instance fields
.field private angle:[F

.field private mAbsorbingFling:Z

.field private mBaseMatrix:Landroid/graphics/Matrix;

.field private mBitmapArr:[Landroid/graphics/Bitmap;

.field private mCurrentBitmap:Landroid/graphics/Bitmap;

.field private mDisplay:Landroid/view/Display;

.field private mDistDivider:I

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mEffectLeft:Landroid/widget/EdgeEffect;

.field private mEffectRight:Landroid/widget/EdgeEffect;

.field private mEnableTouchMode:Z

.field private mFirstShowBitmap:Landroid/graphics/Bitmap;

.field private mFirstTime:Z

.field private mFlingDirection:I

.field private mFlingOnGoing:Z

.field private mFlingThread:Ljava/lang/Thread;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mImageRotation:I

.field private mLastFrameTime:J

.field private mLastIndex:I

.field private mLastTmpIndex:I

.field private mLeftDrawMatrix:Landroid/graphics/Matrix;

.field private mMaxIndex:I

.field private mNextBitmap:Landroid/graphics/Bitmap;

.field private mOrientation:I

.field private mOverScrollPoint:F

.field private mPaint:Landroid/graphics/Paint;

.field private mRenderLock:Ljava/lang/Object;

.field public mRenderRequested:Z

.field private mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

.field private mResponsibility:Z

.field private mRightDrawMatrix:Landroid/graphics/Matrix;

.field private mScrollDistance:F

.field private mScroller:Landroid/widget/OverScroller;

.field private mStereoMode:Z

.field private mSurfaceReady:Z

.field private mTouchThreshold:I

.field private mValue:F

.field private mWidth:I

.field private timestamp:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xf

    sput v0, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstTime:Z

    const v0, 0xffff

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->timestamp:F

    new-array v0, v6, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLeftDrawMatrix:Landroid/graphics/Matrix;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderLock:Ljava/lang/Object;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderRequested:Z

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mHolder:Landroid/view/SurfaceHolder;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mSurfaceReady:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastFrameTime:J

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mMaxIndex:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingDirection:I

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    iput v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->initView(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstTime:Z

    const v0, 0xffff

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->timestamp:F

    new-array v0, v6, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLeftDrawMatrix:Landroid/graphics/Matrix;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderLock:Ljava/lang/Object;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderRequested:Z

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mHolder:Landroid/view/SurfaceHolder;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mSurfaceReady:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastFrameTime:J

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mMaxIndex:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingDirection:I

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    iput v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->initView(Landroid/content/Context;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mSurfaceReady:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/EdgeEffect;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/gallery3d/mav/MAVView3D;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/mav/MAVView3D;)Landroid/widget/OverScroller;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScroller:Landroid/widget/OverScroller;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/mav/MAVView3D;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/mav/MAVView3D;I)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->scrollDeltaDistance(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/gallery3d/mav/MAVView3D;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/gallery3d/mav/MAVView3D;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/gallery3d/mav/MAVView3D;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingDirection:I

    return v0
.end method

.method private checkBitmap(Landroid/graphics/Bitmap;)Z
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v4, 0x0

    if-nez p1, :cond_0

    const-string v3, "MAVView3D"

    const-string v4, "checkBitmap: Bitmap is null!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v1, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v0, v3

    cmpg-float v3, v1, v4

    if-lez v3, :cond_1

    cmpg-float v3, v0, v4

    if-gtz v3, :cond_2

    :cond_1
    const-string v3, "MAVView3D"

    const-string v4, "checkBitmap:invalid dimension of Bitmap!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkSelf()Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v0, v2

    cmpg-float v2, v1, v3

    if-lez v2, :cond_0

    cmpg-float v2, v0, v3

    if-gtz v2, :cond_1

    :cond_0
    const-string v2, "MAVView3D"

    const-string v3, "checkSelf:invalid dimension of current view!"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private initView(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v0, Landroid/widget/OverScroller;

    invoke-direct {v0, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScroller:Landroid/widget/OverScroller;

    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-direct {v0, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-direct {v0, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->updateDimensionParams()V

    const-string v0, "MAVView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initView: initial rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {p0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    new-instance v0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;-><init>(Lcom/mediatek/gallery3d/mav/MAVView3D;Lcom/mediatek/gallery3d/mav/MAVView3D$1;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    const-string v1, "MAVView3D-render"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private requestRender()V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderRequested:Z

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-string v2, "MAVView3D"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request render consumed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private scrollDeltaDistance(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v3, v3

    if-nez v3, :cond_2

    :cond_0
    const-string v3, "MAVView3D"

    const-string v4, "scrollDeltaDistance: bitmap array is NOT ready!!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v3

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    div-int v1, p1, v3

    if-eqz v1, :cond_1

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    add-int v2, v3, v1

    iget-boolean v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v3, :cond_3

    add-int/lit8 v3, v0, -0x1

    invoke-static {v2, v4, v3}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    :goto_1
    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    if-eq v2, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v2

    invoke-virtual {p0, v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v0, -0x2

    invoke-static {v2, v4, v3}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    goto :goto_1
.end method

.method private setBitmapMatrix(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->checkSelf()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->checkBitmap(Landroid/graphics/Bitmap;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    const-string v10, "MAVView3D"

    const-string v11, "setBitmapMatrix:either Bitmap or ImageView\'s dimen invalid"

    invoke-static {v10, v11}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v10

    int-to-float v7, v10

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    int-to-float v6, v10

    const/4 v8, 0x0

    const/4 v2, 0x0

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    div-int/lit8 v10, v10, 0x5a

    rem-int/lit8 v10, v10, 0x2

    if-eqz v10, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v8, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v2, v10

    :goto_1
    div-float v9, v7, v8

    div-float v3, v6, v2

    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    if-eqz v10, :cond_2

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v0, v10, 0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v1, v10, 0x2

    neg-int v10, v0

    int-to-float v10, v10

    neg-int v11, v1

    int-to-float v11, v11

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    int-to-float v10, v10

    invoke-virtual {v4, v10}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/high16 v10, 0x40000000

    div-float v10, v8, v10

    const/high16 v11, 0x40000000

    div-float v11, v2, v11

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    :cond_2
    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    mul-float v11, v8, v5

    sub-float v11, v7, v11

    const/high16 v12, 0x40000000

    div-float/2addr v11, v12

    mul-float v12, v2, v5

    sub-float v12, v6, v12

    const/high16 v13, 0x40000000

    div-float/2addr v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBaseMatrix:Landroid/graphics/Matrix;

    iput-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    new-instance v10, Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-direct {v10, v11}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLeftDrawMatrix:Landroid/graphics/Matrix;

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLeftDrawMatrix:Landroid/graphics/Matrix;

    const/high16 v11, 0x3f000000

    const/high16 v12, 0x3f800000

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postScale(FF)Z

    new-instance v10, Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-direct {v10, v11}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    const/high16 v11, 0x3f000000

    const/high16 v12, 0x3f800000

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v10, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    const/high16 v11, 0x40000000

    div-float v11, v7, v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    const-string v10, "MAVView3D"

    const-string v11, " setImageBitmap => requestRender"

    invoke-static {v10, v11}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    const-string v10, "MAVView3D"

    const-string v11, " setImageBitmap <= requestRender"

    invoke-static {v10, v11}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "MAVView3D"

    const-string v11, "<< setImageBitmap"

    invoke-static {v10, v11}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v8, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto/16 :goto_1
.end method

.method private updateDimensionParams()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    goto :goto_0
.end method


# virtual methods
.method public configChanged(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->updateDimensionParams()V

    const-string v0, "MAVView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "configChanged: new width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public drawOnCanvas(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v13, 0x42b40000

    const/high16 v12, 0x3f800000

    const/high16 v11, 0x3f000000

    const/high16 v10, -0x3d4c0000

    const/4 v9, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-string v6, "MAVView3D"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">> onDraw: canvas="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    const-string v6, "MAVView3D"

    const-string v7, " onDraw: => draw black"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v6, -0x1000000

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    const-string v6, "MAVView3D"

    const-string v7, " onDraw: <= draw black"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v6, :cond_3

    const-string v6, "MAVView3D"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " onDraw: bitmap w="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", h="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "MAVView3D"

    const-string v7, " onDraw => drawBitmap"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDrawMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    const-string v6, "MAVView3D"

    const-string v7, " onDraw <= drawBitmap"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    const-string v6, "MAVView3D"

    const-string v7, " onDraw: => draw effect right"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v6, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v6, v1

    int-to-float v6, v6

    invoke-virtual {p1, v9, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_0
    :goto_1
    const-string v6, "MAVView3D"

    const-string v7, " onDraw: <= draw effect right"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "MAVView3D"

    const-string v7, " onDraw: => draw effect left"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v6, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v6, v0

    int-to-float v6, v6

    invoke-virtual {p1, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    :goto_2
    const-string v6, "MAVView3D"

    const-string v7, " onDraw: <= draw effect left"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-string v6, "MAVView3D"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<< onDraw: consumed "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLeftDrawMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRightDrawMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v6, v1

    int-to-float v6, v6

    invoke-virtual {p1, v9, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    neg-int v6, v1

    int-to-float v6, v6

    invoke-virtual {p1, v9, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v6, v0

    int-to-float v6, v6

    invoke-virtual {p1, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    int-to-float v6, v1

    invoke-virtual {p1, v9, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v6

    or-int/2addr v2, v6

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_2
.end method

.method public getStereoMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "MAVView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDown: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-nez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0, v3}, Landroid/widget/OverScroller;->forceFinished(Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingDirection:I

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iput v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const-string v1, "MAVView3D"

    const-string v3, "onFling"

    invoke-static {v1, v3}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "MAVView3D"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFling: vX="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", vY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    const/4 v1, 0x0

    cmpg-float v1, p3, v1

    if-gez v1, :cond_3

    const/4 v1, 0x2

    :goto_1
    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingDirection:I

    move/from16 v0, p3

    float-to-int v12, v0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingOnGoing:Z

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->finish()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->finish()V

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mAbsorbingFling:Z

    :cond_2
    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    mul-int/2addr v1, v3

    iget v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    mul-int v2, v1, v3

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScroller:Landroid/widget/OverScroller;

    const/4 v3, 0x0

    neg-int v4, v12

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    new-instance v1, Lcom/mediatek/gallery3d/mav/MAVView3D$1;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/gallery3d/mav/MAVView3D$1;-><init>(Lcom/mediatek/gallery3d/mav/MAVView3D;I)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    const-string v3, "scroller-fling"

    invoke-virtual {v1, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFlingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const-string v0, "MAVView3D"

    const-string v1, "onLayout"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lez v0, :cond_3

    const v0, 0xffff

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-nez v6, :cond_0

    :goto_0
    return v4

    :cond_0
    const-string v6, "MAVView3D"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onScroll: distX="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    cmpl-float v6, v6, v9

    if-eqz v6, :cond_2

    cmpg-float v6, p3, v9

    if-gez v6, :cond_1

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    cmpl-float v6, v6, v9

    if-gtz v6, :cond_2

    :cond_1
    cmpl-float v6, p3, v9

    if-lez v6, :cond_5

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_5

    :cond_2
    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    iput v9, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    iput p3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    cmpg-float v5, p3, v9

    if-gez v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_3
    :goto_1
    const-string v5, "MAVView3D"

    const-string v6, "onScroll: => scrollDeltaDistance"

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    float-to-int v5, v5

    invoke-direct {p0, v5}, Lcom/mediatek/gallery3d/mav/MAVView3D;->scrollDeltaDistance(I)V

    const-string v5, "MAVView3D"

    const-string v6, "onScroll: <= scrollDeltaDistance"

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    const-string v5, "MAVView3D"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onScroll consumed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    cmpl-float v5, p3, v9

    if-lez v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_1

    :cond_5
    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    add-float/2addr v6, p3

    iput v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    if-nez v6, :cond_a

    cmpg-float v6, p3, v9

    if-gez v6, :cond_a

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_a

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iput v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    :cond_6
    :goto_2
    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    add-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mTouchThreshold:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDistDivider:I

    mul-int v1, v6, v7

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v6, :cond_8

    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v6, :cond_b

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_b

    move v0, v4

    :goto_3
    iget-boolean v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-eqz v6, :cond_c

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x2

    if-ne v6, v7, :cond_c

    move v6, v4

    :goto_4
    or-int/2addr v0, v6

    cmpl-float v6, p3, v9

    if-lez v6, :cond_7

    move v5, v4

    :cond_7
    and-int/2addr v0, v5

    :cond_8
    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    if-nez v5, :cond_d

    cmpg-float v5, p3, v9

    if-gez v5, :cond_d

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_9
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/EdgeEffect;->onPull(F)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    goto/16 :goto_1

    :cond_a
    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_6

    cmpl-float v6, p3, v9

    if-lez v6, :cond_6

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_6

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iput v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    goto :goto_2

    :cond_b
    move v0, v5

    goto :goto_3

    :cond_c
    move v6, v5

    goto :goto_4

    :cond_d
    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v5

    if-nez v5, :cond_e

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v5}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_e
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mScrollDistance:F

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOverScrollPoint:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mWidth:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/EdgeEffect;->onPull(F)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    goto/16 :goto_1
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13
    .param p1    # Landroid/hardware/SensorEvent;

    iget-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x3cf5c28f

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v2

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    if-eq v5, v2, :cond_2

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    const/4 v5, 0x0

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstTime:Z

    :cond_2
    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mOrientation:I

    packed-switch v5, :pswitch_data_0

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v3, v5, v6

    :goto_1
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3cf5c28f

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_0

    const/4 v5, 0x0

    add-float/2addr v5, v3

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->timestamp:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3a83126f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v5, v5

    iget v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->timestamp:F

    sub-float/2addr v5, v6

    const v6, 0x3089705f

    mul-float v0, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x1

    aget v7, v5, v6

    float-to-double v7, v7

    iget v9, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mValue:F

    mul-float/2addr v9, v0

    const/high16 v10, 0x43340000

    mul-float/2addr v9, v10

    float-to-double v9, v9

    const-wide v11, 0x400921fb54442d18L

    div-double/2addr v9, v11

    add-double/2addr v7, v9

    double-to-float v7, v7

    aput v7, v5, v6

    iget-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstTime:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v6

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstTime:Z

    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v6, v6

    mul-int/2addr v5, v6

    sget v6, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    mul-int/lit8 v6, v6, 0x2

    div-int v1, v5, v6

    if-ltz v1, :cond_5

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    const v6, 0xffff

    if-eq v5, v6, :cond_4

    iget v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    if-eq v5, v1, :cond_5

    :cond_4
    const-string v5, "MAVView3D"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setImageBitmap: bitmap["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v7, v7

    sub-int/2addr v7, v1

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v6, v6

    sub-int/2addr v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    invoke-virtual {p0, v5}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    :cond_5
    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    long-to-float v5, v5

    iput v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->timestamp:F

    goto/16 :goto_0

    :pswitch_0
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x1

    aget v3, v5, v6

    goto/16 :goto_1

    :pswitch_1
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v3, v5, v6

    goto/16 :goto_1

    :pswitch_2
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    neg-float v3, v5

    goto/16 :goto_1

    :pswitch_3
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    neg-float v3, v5

    goto/16 :goto_1

    :cond_6
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v6

    goto/16 :goto_2

    :cond_7
    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->angle:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    sget v8, Lcom/mediatek/gallery3d/mav/MAVView3D;->BASE_ANGLE:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    iget-boolean v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    if-nez v2, :cond_0

    :goto_0
    return v5

    :cond_0
    const-string v2, "MAVView3D"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTouchEvent: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    const-string v2, "MAVView3D"

    const-string v3, " onTouchEvent: => gesture detector"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const-string v2, "MAVView3D"

    const-string v3, " onTouchEvent: <= gesture detector"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-string v2, "MAVView3D"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTouchEvent consumed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    iput v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectRight:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEffectLeft:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public recycleBitmapArr()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const-string v1, "MAVView3D"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is recycled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setBitmapArr([Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # [Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mMaxIndex:I

    :cond_0
    return-void
.end method

.method public setFirstShowBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->checkSelf()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->checkBitmap(Landroid/graphics/Bitmap;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "MAVView3D"

    const-string v4, "setImageBitmap:either Bitmap or ImageView\'s dimen invalid"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v3, "MAVView3D"

    const-string v4, ">> setImageBitmap"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iget-boolean v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_7

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v1

    if-ne v3, v4, :cond_4

    move v2, v1

    :cond_2
    if-gez v2, :cond_5

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    :goto_2
    if-ltz v2, :cond_3

    if-ge v2, v0, :cond_3

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v2

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    :cond_3
    :goto_3
    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setBitmapMatrix(Landroid/graphics/Bitmap;)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v3, v0, -0x1

    if-le v2, v3, :cond_6

    add-int/lit8 v2, v0, -0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    goto :goto_3

    :cond_8
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mNextBitmap:Landroid/graphics/Bitmap;

    goto :goto_3
.end method

.method public setImageRotation(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mImageRotation:I

    const-string v0, "MAVView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setImageRotation(rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setResponsibility(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mResponsibility:Z

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mBitmapArr:[Landroid/graphics/Bitmap;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastIndex:I

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mLastTmpIndex:I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->updateDimensionParams()V

    :cond_0
    return-void
.end method

.method public setTouchModeEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mEnableTouchMode:Z

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mSurfaceReady:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;-><init>(Lcom/mediatek/gallery3d/mav/MAVView3D;Lcom/mediatek/gallery3d/mav/MAVView3D$1;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    const-string v1, "MAVView3D-render"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mSurfaceReady:Z

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mRenderThread:Lcom/mediatek/gallery3d/mav/MAVView3D$MavRenderThread;

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public toggleStereoMode()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :goto_1
    invoke-static {p0, v0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->setSfStereoLayout(Landroid/view/SurfaceView;I)V

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mStereoMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MAVView3D;->mCurrentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->requestRender()V

    return-void

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
