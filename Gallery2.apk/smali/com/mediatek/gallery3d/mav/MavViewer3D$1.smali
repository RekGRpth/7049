.class Lcom/mediatek/gallery3d/mav/MavViewer3D$1;
.super Ljava/lang/Object;
.source "MavViewer3D.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/mav/MavViewer3D;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/mav/MavViewer3D;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$1;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$1;-><init>(Lcom/mediatek/gallery3d/mav/MavViewer3D$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decode filepath:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/mpo/MpoDecoder;->decodeFile(Ljava/lang/String;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$402(Lcom/mediatek/gallery3d/mav/MavViewer3D;Lcom/mediatek/mpo/MpoDecoder;)Lcom/mediatek/mpo/MpoDecoder;

    :goto_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "MavViewer3D"

    const-string v1, "failed to decode MpoDecoder, finish()"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$2;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$2;-><init>(Lcom/mediatek/gallery3d/mav/MavViewer3D$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decode Uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$600(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mpo/MpoDecoder;->decodeUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$402(Lcom/mediatek/gallery3d/mav/MavViewer3D;Lcom/mediatek/mpo/MpoDecoder;)Lcom/mediatek/mpo/MpoDecoder;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$700(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v6, "orientation"

    aput-object v6, v2, v9

    const-string v5, "bucket_display_name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-interface {v10, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0, v7}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z

    :cond_3
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$700(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-nez v0, :cond_4

    :try_start_0
    new-instance v13, Landroid/media/ExifInterface;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v13, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string v0, "Orientation"

    const/4 v1, 0x1

    invoke-virtual {v13, v0, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v14

    packed-switch v14, :pswitch_data_0

    :pswitch_0
    const-string v0, "MavViewer3D"

    const-string v1, "rotation in exif is not available!"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$700(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "final rotation is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$800(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$900(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/gallery3d/mav/MAVView3D;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$800(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setImageRotation(I)V

    :cond_5
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mpo/MpoDecoder;->frameCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1002(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mpo/MpoDecoder;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mpo/MpoDecoder;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, v0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mpo/MpoDecoder;->width()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/mpo/MpoDecoder;->height()I

    move-result v2

    const/16 v6, 0x280

    invoke-static {v1, v2, v6}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(III)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SampleSize:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v2, v2, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget v2, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1000(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1102(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, v0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Landroid/graphics/BitmapFactory$Options;Z)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I

    move-result v2

    iget-object v6, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v6, v6, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v1, v2, v6}, Lcom/mediatek/mpo/MpoDecoder;->frameBitmap(ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1202(Lcom/mediatek/gallery3d/mav/MavViewer3D;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$900(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/gallery3d/mav/MAVView3D;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setFirstShowBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$3;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$3;-><init>(Lcom/mediatek/gallery3d/mav/MavViewer3D$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    new-instance v5, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v7, v5, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inMpoFrames:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, v5, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayWidth:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, v5, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayHeight:I

    invoke-static {v5, v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getImages:run:decode filepath:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v0, v1, v9}, Lcom/mediatek/gallery3d/data/RequestHelper;->requestDataBundle(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/content/Context;Ljava/lang/String;Z)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    move-result-object v11

    :goto_3
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    if-eqz v11, :cond_6

    iget-object v4, v11, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->mpoFrames:[Landroid/graphics/Bitmap;

    :cond_6
    invoke-static {v0, v4}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1502(Lcom/mediatek/gallery3d/mav/MavViewer3D;[Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)[Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v0, "MavViewer3D"

    const-string v1, "onCreate: get null MPO frames"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1

    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v12

    const-string v0, "MavViewer3D"

    const-string v1, "Exception when trying to fetch orientation from exif"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/16 v1, 0x5a

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/16 v1, 0xb4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/16 v1, 0x10e

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :cond_7
    const-string v0, "MavViewer3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getImages:run:decode Uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v2}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/content/Context;

    move-result-object v6

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "image/mpo"

    invoke-static/range {v4 .. v9}, Lcom/mediatek/gallery3d/data/RequestHelper;->requestDataBundle(Lcom/android/gallery3d/util/ThreadPool$JobContext;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Z)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    move-result-object v11

    goto/16 :goto_3

    :cond_8
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$900(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/gallery3d/mav/MAVView3D;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v1}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$1500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)[Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setBitmapArr([Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;->this$0:Lcom/mediatek/gallery3d/mav/MavViewer3D;

    invoke-static {v0}, Lcom/mediatek/gallery3d/mav/MavViewer3D;->access$100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$4;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/mav/MavViewer3D$1$4;-><init>(Lcom/mediatek/gallery3d/mav/MavViewer3D$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
