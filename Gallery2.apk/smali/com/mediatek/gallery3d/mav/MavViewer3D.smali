.class public Lcom/mediatek/gallery3d/mav/MavViewer3D;
.super Landroid/app/Activity;
.source "MavViewer3D.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final CAMERA_MAV_IMAGE_BUCKET_NAME:Ljava/lang/String;

.field public static final MSG_UPDATE_MAVVIEW:I = 0x1

.field private static final PREFTAG_FIRSTHINTSHOWED:Ljava/lang/String; = "firstshow"

.field private static final SHARED_PREF_NAME:Ljava/lang/String; = "mav_viewer"

.field private static final TAG:Ljava/lang/String; = "MavViewer3D"

.field private static final TARGET_SIZE:I = 0x280


# instance fields
.field private mBtnToggleStereoMode:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mCr:Landroid/content/ContentResolver;

.field private mCurrentFrame:I

.field private mDecodeUri:Z

.field private mDefaultDisplay:Landroid/view/Display;

.field private mFirstShowBitmap:Landroid/graphics/Bitmap;

.field private mGyroSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field private mHasGyroSensor:Z

.field private mMavBitmapArr:[Landroid/graphics/Bitmap;

.field private mMiddleFrame:I

.field private mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

.field private mMpoFilePath:Ljava/lang/String;

.field private mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

.field public mOptions:Landroid/graphics/BitmapFactory$Options;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mRotation:I

.field private mRotationFetched:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTotalCount:I

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/Camera/Mav"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->CAMERA_MAV_IMAGE_BUCKET_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mUri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mCr:Landroid/content/ContentResolver;

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mTotalCount:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mCurrentFrame:I

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMiddleFrame:I

    iput-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDefaultDisplay:Landroid/view/Display;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHandler:Landroid/os/Handler;

    iput v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotation:I

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotationFetched:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mTotalCount:I

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mTotalCount:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMiddleFrame:I

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMiddleFrame:I

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/mediatek/gallery3d/mav/MavViewer3D;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mFirstShowBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/view/Display;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDefaultDisplay:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)[Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMavBitmapArr:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/mediatek/gallery3d/mav/MavViewer3D;[Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # [Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMavBitmapArr:[Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/mpo/MpoDecoder;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/gallery3d/mav/MavViewer3D;Lcom/mediatek/mpo/MpoDecoder;)Lcom/mediatek/mpo/MpoDecoder;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # Lcom/mediatek/mpo/MpoDecoder;

    iput-object p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Landroid/content/ContentResolver;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mCr:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotationFetched:Z

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/gallery3d/mav/MavViewer3D;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotationFetched:Z

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/gallery3d/mav/MavViewer3D;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotation:I

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/gallery3d/mav/MavViewer3D;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotation:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/gallery3d/mav/MavViewer3D;)Lcom/mediatek/gallery3d/mav/MAVView3D;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/mav/MavViewer3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    return-object v0
.end method

.method private calcuSampleSize(I)I
    .locals 5
    .param p1    # I

    if-gtz p1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-lez p1, :cond_1

    add-int/lit8 v0, v0, 0x1

    ushr-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_1
    const-wide/high16 v1, 0x4000000000000000L

    int-to-double v3, v0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-int v1, v1

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->toggleStereoMode()V

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/mav/MAVView3D;->getStereoMode()Z

    move-result v2

    if-eqz v2, :cond_0

    const v1, 0x7f020060

    :goto_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast p1, Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    const v1, 0x7f020061

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f0b0063
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1    # Landroid/content/res/Configuration;

    const v7, 0x7f0b0061

    const/4 v6, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v4, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v4, p0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->configChanged(Landroid/content/Context;)V

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    const/4 v3, 0x0

    move-object v3, v2

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const v4, 0x7f040021

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const-string v0, "MavViewer3D"

    const-string v2, "onCreate"

    invoke-static {v0, v2}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040024

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0063

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mBtnToggleStereoMode:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mBtnToggleStereoMode:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mGyroSensor:Landroid/hardware/Sensor;

    const-string v0, "MavViewer3D"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate: gyro sensor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mGyroSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mGyroSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    const v0, 0x7f0b0062

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/mav/MAVView3D;

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setFormat(I)V

    const v0, 0x7f0b0060

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDefaultDisplay:Landroid/view/Display;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setTouchModeEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v0, "mpoFilePath"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "mpoFilePath"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoFilePath:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoFilePath:Ljava/lang/String;

    if-nez v0, :cond_7

    invoke-virtual {v10}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "MavViewer3D"

    const-string v2, "onCreate: get null uri"

    invoke-static {v0, v2}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "orientation"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "bucket_display_name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoFilePath:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotation:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mRotationFetched:Z

    :cond_4
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    :goto_2
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoFilePath:Ljava/lang/String;

    if-nez v0, :cond_8

    const-string v0, "MavViewer3D"

    const-string v2, "onCreate: get null MPO file path in extra of intent"

    invoke-static {v0, v2}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mCr:Landroid/content/ContentResolver;

    invoke-virtual {v10}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mUri:Landroid/net/Uri;

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mDecodeUri:Z

    :cond_8
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setTouchModeEnabled(Z)V

    const-string v0, "mav_viewer"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    const-string v0, "firstshow"

    const/4 v2, 0x0

    invoke-interface {v13, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v11, 0x1

    :goto_3
    const v0, 0x7f0b0061

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-eqz v11, :cond_b

    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v0, "firstshow"

    const/4 v2, 0x1

    invoke-interface {v7, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_9
    :goto_4
    iput-object p0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mContext:Landroid/content/Context;

    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/mav/MavViewer3D$1;-><init>(Lcom/mediatek/gallery3d/mav/MavViewer3D;)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :cond_a
    const/4 v11, 0x0

    goto :goto_3

    :cond_b
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    invoke-virtual {v12, v8}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "MavViewer3D"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/mav/MAVView3D;->setResponsibility(Z)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/mav/MAVView3D;->recycleBitmapArr()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMpoDecoder:Lcom/mediatek/mpo/MpoDecoder;

    invoke-virtual {v0}, Lcom/mediatek/mpo/MpoDecoder;->close()V

    :cond_0
    return-void
.end method

.method public onFirstRunDismiss(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f0b0061

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "MavViewer3D"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "MavViewer3D"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mHasGyroSensor:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mMultiAngleView:Lcom/mediatek/gallery3d/mav/MAVView3D;

    iget-object v2, p0, Lcom/mediatek/gallery3d/mav/MavViewer3D;->mGyroSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    return-void
.end method
