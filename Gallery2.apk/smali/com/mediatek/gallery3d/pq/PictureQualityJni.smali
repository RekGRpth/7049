.class public Lcom/mediatek/gallery3d/pq/PictureQualityJni;
.super Ljava/lang/Object;
.source "PictureQualityJni.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PictureQualityJni"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PQjni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native nativeGetColorIndex()I
.end method

.method public static native nativeGetColorRange()I
.end method

.method public static native nativeGetGrassToneIndex()I
.end method

.method public static native nativeGetGrassToneRange()I
.end method

.method public static native nativeGetSharpnessIndex()I
.end method

.method public static native nativeGetSharpnessRange()I
.end method

.method public static native nativeGetSkinToneIndex()I
.end method

.method public static native nativeGetSkinToneRange()I
.end method

.method public static native nativeGetSkyToneIndex()I
.end method

.method public static native nativeGetSkyToneRange()I
.end method

.method public static native nativeSetColorIndex(I)Z
.end method

.method public static native nativeSetGrassToneIndex(I)Z
.end method

.method public static native nativeSetSharpnessIndex(I)Z
.end method

.method public static native nativeSetSkinToneIndex(I)Z
.end method

.method public static native nativeSetSkyToneIndex(I)Z
.end method
