.class public Lcom/mediatek/gallery3d/pq/PictureQualityOptions;
.super Ljava/lang/Object;
.source "PictureQualityOptions.java"


# static fields
.field private static mColorOption:I

.field private static mGrassToneOption:I

.field private static mSharpnessOption:I

.field private static mSkinToneOption:I

.field private static mSkyToneOption:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkyToneOption:I

    sput v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkinToneOption:I

    sput v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mGrassToneOption:I

    const/4 v0, 0x1

    sput v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mColorOption:I

    const/4 v0, 0x5

    sput v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSharpnessOption:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getColorOptin()I
    .locals 1

    sget v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mColorOption:I

    return v0
.end method

.method public static getGrassToneOptin()I
    .locals 1

    sget v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mGrassToneOption:I

    return v0
.end method

.method public static getSharpnessOptin()I
    .locals 1

    sget v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSharpnessOption:I

    return v0
.end method

.method public static getSkinToneOptin()I
    .locals 1

    sget v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkinToneOption:I

    return v0
.end method

.method public static getSkyToneOption()I
    .locals 1

    sget v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkyToneOption:I

    return v0
.end method

.method public static declared-synchronized setColorOptin(I)V
    .locals 2
    .param p0    # I

    const-class v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mColorOption:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setGrassToneOptin(I)V
    .locals 2
    .param p0    # I

    const-class v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mGrassToneOption:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSharpnessOptin(I)V
    .locals 2
    .param p0    # I

    const-class v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSharpnessOption:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSkinToneOptin(I)V
    .locals 2
    .param p0    # I

    const-class v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkinToneOption:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSkyToneOption(I)V
    .locals 2
    .param p0    # I

    const-class v0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/mediatek/gallery3d/pq/PictureQualityOptions;->mSkyToneOption:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
