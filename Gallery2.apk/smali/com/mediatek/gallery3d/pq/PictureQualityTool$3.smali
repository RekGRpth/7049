.class Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;
.super Ljava/lang/Object;
.source "PictureQualityTool.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v1}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p2

    div-int/lit8 v1, v1, 0x64

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$802(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$1000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SkinTone:   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v2}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSkinToneIndex(I)Z

    const-string v0, "PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SkinTone Index is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v2}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    return-void
.end method
