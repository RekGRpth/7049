.class public Lcom/mediatek/gallery3d/pq/PictureQualityTool;
.super Landroid/app/Activity;
.source "PictureQualityTool.java"


# static fields
.field public static final ACTION_PQ:Ljava/lang/String; = "com.android.camera.action.PQ"

.field private static final BACKUP_PIXEL_COUNT:I = 0x75300

.field private static final TAG:Ljava/lang/String; = "PictureQualityTool"


# instance fields
.field private PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mColorOption:I

.field private mColorRange:I

.field private mGrassToneOption:I

.field private mGrassToneRange:I

.field private mImageView:Landroid/widget/ImageView;

.field private mSeekBarColor:Landroid/widget/SeekBar;

.field private mSeekBarGrassTone:Landroid/widget/SeekBar;

.field private mSeekBarSharpness:Landroid/widget/SeekBar;

.field private mSeekBarSkinTone:Landroid/widget/SeekBar;

.field private mSeekBarSkyTone:Landroid/widget/SeekBar;

.field private mSharpnessOption:I

.field private mSharpnessRange:I

.field private mSkinToneOption:I

.field private mSkinToneRange:I

.field private mSkyToneOption:I

.field private mSkyToneRange:I

.field private mTextViewColor:Landroid/widget/TextView;

.field private mTextViewColorRange:Landroid/widget/TextView;

.field private mTextViewGrassTone:Landroid/widget/TextView;

.field private mTextViewGrassToneMin:Landroid/widget/TextView;

.field private mTextViewGrassToneRange:Landroid/widget/TextView;

.field private mTextViewSharpness:Landroid/widget/TextView;

.field private mTextViewSharpnessRange:Landroid/widget/TextView;

.field private mTextViewSkinTone:Landroid/widget/TextView;

.field private mTextViewSkinToneMin:Landroid/widget/TextView;

.field private mTextViewSkinToneRange:Landroid/widget/TextView;

.field private mTextViewSkyTone:Landroid/widget/TextView;

.field private mTextViewSkyToneMin:Landroid/widget/TextView;

.field private mTextViewSkyToneRange:Landroid/widget/TextView;

.field public options:Landroid/graphics/BitmapFactory$Options;

.field public pqUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessOption:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessOption:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Lcom/mediatek/gallery3d/pq/PictureQualityJni;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneOption:I

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneOption:I

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    return v0
.end method

.method static synthetic access$1300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneOption:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneOption:I

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onReDisplayPQImage()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorOption:I

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorOption:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorRange:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewColor:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneOption:I

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneOption:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    return v0
.end method

.method private initPQToolView()V
    .locals 7

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneRange()I

    move-result v3

    iput v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneRange()I

    move-result v3

    iput v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneRange()I

    move-result v3

    iput v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetColorRange()I

    move-result v3

    iput v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorRange:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpnessRange()I

    move-result v3

    iput v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    const-string v3, "PictureQualityTool"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SkyToneRange "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SkinToneRange "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " GrassToneRange "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mColorRange "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorRange:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSharpnessRange "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/util/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x7f0b0053

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneMin:Landroid/widget/TextView;

    const v3, 0x7f0b00d1

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneMin:Landroid/widget/TextView;

    const v3, 0x7f0b00d5

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneMin:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneMin:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneMin:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneMin:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00c6

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageView:Landroid/widget/ImageView;

    const v3, 0x7f0b00cb

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpnessRange:Landroid/widget/TextView;

    const v3, 0x7f0b00cf

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewColorRange:Landroid/widget/TextView;

    const v3, 0x7f0b00da

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneRange:Landroid/widget/TextView;

    const v3, 0x7f0b00d3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneRange:Landroid/widget/TextView;

    const v3, 0x7f0b00d7

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneRange:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpnessRange:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewColorRange:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorRange:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneRange:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneRange:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneRange:Landroid/widget/TextView;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00cc

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sharpness:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpnessIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00ca

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpnessIndex()I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    add-int/lit8 v5, v5, -0x1

    div-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    const v3, 0x7f0b00db

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SkyTone:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneIndex()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneIndex()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00d9

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneIndex()I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    add-int/lit8 v5, v5, -0x1

    div-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    const v3, 0x7f0b00d8

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GrassTone:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneIndex()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneIndex()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00d6

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneIndex()I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    add-int/lit8 v5, v5, -0x1

    div-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    const v3, 0x7f0b00d4

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SkinTone:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneIndex()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneIndex()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00d2

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneIndex()I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    add-int/lit8 v5, v5, -0x1

    div-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    const v3, 0x7f0b00d0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewColor:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewColor:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Color:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetColorIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0b00ce

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarColor:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarColor:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetColorIndex()I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mColorRange:I

    add-int/lit8 v5, v5, -0x1

    div-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->pqUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    iget v2, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    iget v1, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, -0x1

    const v5, 0x75300

    invoke-static {v2, v1, v4, v5}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSize(IIII)I

    move-result v4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x0

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onReDisplayPQImage()V

    return-void

    :catch_0
    move-exception v0

    const-string v3, "PictureQualityTool"

    const-string v4, "bitmapfactory decodestream fail"

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/util/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onReDisplayPQImage()V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->pqUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "PictureQualityTool"

    const-string v2, "bitmapfactory decodestream fail"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onSaveClicked()V
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "color"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetColorIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "sharpness"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpnessIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skyTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skinTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "grassTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQToolJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f040044

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PQUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->pqUri:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isPictureQualityEnhanceSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    :cond_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->initPQToolView()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    new-instance v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool$1;

    invoke-direct {v2, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$1;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarColor:Landroid/widget/SeekBar;

    new-instance v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool$2;

    invoke-direct {v2, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$2;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    new-instance v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;

    invoke-direct {v2, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$3;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    new-instance v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool$4;

    invoke-direct {v2, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$4;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    new-instance v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool$5;

    invoke-direct {v2, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$5;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :sswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onSaveClicked()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0b001f -> :sswitch_1
        0x7f0b012a -> :sswitch_2
    .end sparse-switch
.end method
