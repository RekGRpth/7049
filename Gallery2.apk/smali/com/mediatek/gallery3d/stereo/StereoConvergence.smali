.class public Lcom/mediatek/gallery3d/stereo/StereoConvergence;
.super Ljava/lang/Object;
.source "StereoConvergence.java"


# static fields
.field public static final CONVERGENCE_INDEX:[I

.field private static final SUB_INDEX_NUM:I = 0xa

.field private static final TAG:Ljava/lang/String; = "StereoConvergence"

.field public static final TOTAL_INDEX_NUM:I = 0x51


# instance fields
.field private mActiveFlags:[I

.field private mDefaultPosition:I

.field private mHeightRate:F

.field private mInputHeight:I

.field private mInputWidth:I

.field private mLeftOffsetXRate:[F

.field private mLeftOffsetYRate:F

.field private mRightOffsetXRate:[F

.field private mRightOffsetYRate:F

.field private mWidthRate:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->CONVERGENCE_INDEX:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0xa
        0x14
        0x1e
        0x28
        0x32
        0x3c
        0x46
        0x50
    .end array-data
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mDefaultPosition:I

    return-void
.end method

.method private static LinearDifference([FII)V
    .locals 5
    .param p0    # [F
    .param p1    # I
    .param p2    # I

    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    if-ge p2, p1, :cond_2

    :cond_0
    const-string v3, "StereoConvergence"

    const-string v4, "LinearDifferece:invalid input params"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    aget v2, p0, p1

    aget v0, p0, p2

    add-int/lit8 v1, p1, 0x1

    :goto_0
    if-ge v1, p2, :cond_1

    sub-float v3, v0, v2

    sub-int v4, v1, p1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-int v4, p2, p1

    int-to-float v4, v4

    div-float/2addr v3, v4

    aget v4, p0, p1

    add-float/2addr v3, v4

    aput v3, p0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static executeConvergence(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/mediatek/gallery3d/stereo/StereoConvergence;)Lcom/mediatek/stereo3d/Stereo3DConvergence;
    .locals 5
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const-string v2, "StereoConvergence"

    const-string v3, "executeConvergence:got null stereo frame"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_2

    move-object v0, p0

    move-object v1, p1

    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, p2, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputWidth:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, p2, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputHeight:I

    invoke-static {v0, v1}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->execute(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/stereo3d/Stereo3DConvergence;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    if-le v2, v3, :cond_3

    const-string v2, "StereoConvergence"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInstance:left: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "StereoConvergence"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInstance:right:["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3, p0}, Lcom/mediatek/gallery3d/data/DecodeHelper;->resizeBitmap(IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, p1

    goto/16 :goto_1

    :cond_3
    const-string v2, "StereoConvergence"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInstance:left: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "StereoConvergence"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInstance:right:["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3, p1}, Lcom/mediatek/gallery3d/data/DecodeHelper;->resizeBitmap(IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public static getInstance(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/gallery3d/stereo/StereoConvergence;
    .locals 3
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Bitmap;

    new-instance v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->executeConvergence(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/mediatek/gallery3d/stereo/StereoConvergence;)Lcom/mediatek/stereo3d/Stereo3DConvergence;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->retrieveInfo(Lcom/mediatek/gallery3d/stereo/StereoConvergence;Lcom/mediatek/stereo3d/Stereo3DConvergence;)Lcom/mediatek/gallery3d/stereo/StereoConvergence;

    move-result-object v2

    goto :goto_0
.end method

.method private static retrieveInfo(Lcom/mediatek/gallery3d/stereo/StereoConvergence;Lcom/mediatek/stereo3d/Stereo3DConvergence;)Lcom/mediatek/gallery3d/stereo/StereoConvergence;
    .locals 16
    .param p0    # Lcom/mediatek/gallery3d/stereo/StereoConvergence;
    .param p1    # Lcom/mediatek/stereo3d/Stereo3DConvergence;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v13, "StereoConvergence"

    const-string v14, "retrieveInfo: why got invalid parameters"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p0, 0x0

    :goto_0
    return-object p0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getDefaultPosition()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mDefaultPosition:I

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getActiveFlags()[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    if-nez v13, :cond_2

    const-string v13, "StereoConvergence"

    const-string v14, "getInstance:null active flags!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->CONVERGENCE_INDEX:[I

    array-length v13, v13

    new-array v13, v13, [I

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    :cond_2
    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    array-length v13, v13

    if-ge v1, v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    const/4 v14, 0x0

    aput v14, v13, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mDefaultPosition:I

    const/4 v15, 0x1

    aput v15, v13, v14

    const/4 v5, 0x0

    const/4 v11, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/16 v13, 0x51

    new-array v13, v13, [F

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    const/16 v13, 0x51

    new-array v13, v13, [F

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    const/16 v13, 0x51

    new-array v13, v13, [F

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getCroppingIntervals(Z)[I

    move-result-object v2

    const/4 v1, 0x0

    :goto_2
    array-length v13, v2

    if-ge v1, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    mul-int/lit8 v14, v1, 0xa

    aget v15, v2, v1

    int-to-float v15, v15

    aput v15, v13, v14

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_3
    array-length v13, v2

    add-int/lit8 v13, v13, -0x1

    if-ge v1, v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    mul-int/lit8 v14, v1, 0xa

    add-int/lit8 v15, v1, 0x1

    mul-int/lit8 v15, v15, 0xa

    invoke-static {v13, v14, v15}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->LinearDifference([FII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getCroppingIntervals(Z)[I

    move-result-object v2

    const/4 v1, 0x0

    :goto_4
    array-length v13, v2

    if-ge v1, v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    mul-int/lit8 v14, v1, 0xa

    aget v15, v2, v1

    int-to-float v15, v15

    aput v15, v13, v14

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    :goto_5
    array-length v13, v2

    add-int/lit8 v13, v13, -0x1

    if-ge v1, v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    mul-int/lit8 v14, v1, 0xa

    add-int/lit8 v15, v1, 0x1

    mul-int/lit8 v15, v15, 0xa

    invoke-static {v13, v14, v15}, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->LinearDifference([FII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    array-length v13, v13

    if-ge v1, v13, :cond_8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    aget v5, v13, v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    aget v11, v13, v1

    invoke-static {v5, v11}, Ljava/lang/Math;->min(FF)F

    move-result v13

    sub-float v3, v5, v13

    invoke-static {v5, v11}, Ljava/lang/Math;->min(FF)F

    move-result v13

    sub-float v9, v11, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputWidth:I

    int-to-float v13, v13

    invoke-static {v3, v9}, Ljava/lang/Math;->max(FF)F

    move-result v14

    sub-float v8, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputWidth:I

    int-to-float v14, v14

    div-float v14, v8, v14

    aput v14, v13, v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputWidth:I

    int-to-float v14, v14

    div-float v14, v3, v14

    aput v14, v13, v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputWidth:I

    int-to-float v14, v14

    div-float v14, v9, v14

    aput v14, v13, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getOffsetY(Z)I

    move-result v13

    int-to-float v6, v13

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->getOffsetY(Z)I

    move-result v13

    int-to-float v12, v13

    invoke-static {v6, v12}, Ljava/lang/Math;->min(FF)F

    move-result v13

    sub-float v4, v6, v13

    invoke-static {v6, v12}, Ljava/lang/Math;->min(FF)F

    move-result v13

    sub-float v10, v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputHeight:I

    int-to-float v13, v13

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v14

    sub-float v7, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputHeight:I

    int-to-float v13, v13

    div-float v13, v7, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mHeightRate:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputHeight:I

    int-to-float v13, v13

    div-float v13, v4, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetYRate:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mInputHeight:I

    int-to-float v13, v13

    div-float v13, v10, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetYRate:F

    goto/16 :goto_0
.end method


# virtual methods
.method public getActiveFlags()[I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mActiveFlags:[I

    return-object v0
.end method

.method public getDefaultPosition()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mDefaultPosition:I

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mDefaultPosition:I

    mul-int/lit8 v0, v0, 0xa

    goto :goto_0
.end method

.method public getHeightRate()F
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mHeightRate:F

    return v0
.end method

.method public getLeftOffsetXRate(I)F
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    if-nez v1, :cond_0

    const-string v1, "StereoConvergence"

    const-string v2, "getLeftOffsetXRate:mLeftOffsetXRate=null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    array-length v1, v1

    if-le p1, v1, :cond_2

    :cond_1
    const-string v1, "StereoConvergence"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getLeftOffsetXRate:invalid position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetXRate:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getLeftOffsetYRate()F
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mLeftOffsetYRate:F

    return v0
.end method

.method public getRightOffsetXRate(I)F
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    if-nez v1, :cond_0

    const-string v1, "StereoConvergence"

    const-string v2, "getRightOffsetXRate:mRightOffsetXRate=null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    array-length v1, v1

    if-le p1, v1, :cond_2

    :cond_1
    const-string v1, "StereoConvergence"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRightOffsetXRate:invalid position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetXRate:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getRightOffsetYRate()F
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mRightOffsetYRate:F

    return v0
.end method

.method public getWidthRate(I)F
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    if-nez v1, :cond_0

    const-string v1, "StereoConvergence"

    const-string v2, "getWidthRate:mWidthRate=null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    array-length v1, v1

    if-le p1, v1, :cond_2

    :cond_1
    const-string v1, "StereoConvergence"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWidthRate:invalid position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvergence;->mWidthRate:[F

    aget v0, v0, p1

    goto :goto_0
.end method
