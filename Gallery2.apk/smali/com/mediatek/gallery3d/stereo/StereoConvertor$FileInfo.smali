.class Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;
.super Ljava/lang/Object;
.source "StereoConvertor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/stereo/StereoConvertor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileInfo"
.end annotation


# instance fields
.field private mFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo:got null file"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "StereoConvertor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FileInfo:got file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    goto :goto_0
.end method


# virtual methods
.method public equalByName(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo: equalByName: invalid params"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo: getName: null mFile!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo: getPath: null mFile!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public lastModified()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo: lastModified: null mFile!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    goto :goto_0
.end method

.method public setLastModified(J)Z
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "StereoConvertor"

    const-string v1, "FileInfo: setLastModified: null mFile!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/stereo/StereoConvertor$FileInfo;->mFile:Ljava/io/File;

    invoke-virtual {v0, p1, p2}, Ljava/io/File;->setLastModified(J)Z

    move-result v0

    goto :goto_0
.end method
