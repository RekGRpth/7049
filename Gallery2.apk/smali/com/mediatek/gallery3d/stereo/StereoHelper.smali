.class public Lcom/mediatek/gallery3d/stereo/StereoHelper;
.super Ljava/lang/Object;
.source "StereoHelper.java"


# static fields
.field public static final ATTATCH_WITHOUT_CONVERSION:Ljava/lang/String; = "attachWithoutConversion"

.field public static final FLAG_EX_S3D_2D:I = 0x0

.field public static final FLAG_EX_S3D_3D:I = 0x80000

.field public static final FLAG_EX_S3D_LR_SWAPPED:I = 0x800000

.field public static final FLAG_EX_S3D_MASK:I = 0xff0000

.field public static final FLAG_EX_S3D_SIDE_BY_SIDE:I = 0x200000

.field public static final FLAG_EX_S3D_TOP_AND_BOTTOM:I = 0x400000

.field public static final FLAG_EX_S3D_UNKNOWN:I = 0x100000

.field public static final INCLUDED_STEREO_IMAGE:Ljava/lang/String; = "includedSteroImage"

.field public static final INVALID_BUCKET_ID:I = 0x0

.field public static final INVALID_LOCAL_PATH_END:Ljava/lang/String; = "/0"

.field public static final JPS_EXTENSION:Ljava/lang/String; = "jps"

.field public static final JPS_MIME_TYPE:Ljava/lang/String; = "image/x-jps"

.field public static final JPS_MIME_TYPE2:Ljava/lang/String; = "image/jps"

.field public static final KEY_GET_NO_STEREO_IMAGE:Ljava/lang/String; = "get_no_stereo_image"

.field public static final KEY_PARAMETER_3D_INFO:I = 0x5dd

.field public static final KEY_PARAMETER_3D_OFFSET:I = 0x5de

.field public static final MEDIA_INFO_3D:I = 0x35f

.field public static final MIN_STEREO_INPUT_HEIGHT:I = 0x3c

.field public static final MIN_STEREO_INPUT_WIDTH:I = 0x28

.field public static final PNS_MIME_TYPE:Ljava/lang/String; = "image/pns"

.field private static final PREF_TAG_AUTO_TO_2D:Ljava/lang/String; = "auto_to_2d_tag"

.field private static final PREF_TAG_IMAGE_AC:Ljava/lang/String; = "image_ac_switch"

.field private static final PREF_TAG_VIDEO_AC:Ljava/lang/String; = "video_ac_switch"

.field static final PROJECTION_DATA:[Ljava/lang/String;

.field private static final SHARED_PREF_AC_SWITCH:Ljava/lang/String; = "ac_switch"

.field private static final SHARED_PREF_AUTO_TO_2D_TOAST:Ljava/lang/String; = "auto_to_2d"

.field public static final STEREO_DISPLAY_FIRST_PASS:I = 0x1

.field public static final STEREO_DISPLAY_NORMAL_PASS:I = 0x0

.field public static final STEREO_DISPLAY_SECOND_PASS:I = 0x2

.field public static final STEREO_EXTRA:Ljava/lang/String; = "onlyStereoMedia"

.field public static final STEREO_INDEX_FIRST:I = 0x1

.field public static final STEREO_INDEX_NONE:I = 0x0

.field public static final STEREO_INDEX_SECOND:I = 0x2

.field public static final STEREO_LAYOUT_FULL_FRAME:I = 0x1

.field public static final STEREO_LAYOUT_LEFT_AND_RIGHT:I = 0x2

.field public static final STEREO_LAYOUT_NONE:I = 0x0

.field public static final STEREO_LAYOUT_SWAP_LEFT_AND_RIGHT:I = 0x8

.field public static final STEREO_LAYOUT_SWAP_TOP_AND_BOTTOM:I = 0x10

.field public static final STEREO_LAYOUT_TOP_AND_BOTTOM:I = 0x4

.field public static final STEREO_TYPE:Ljava/lang/String; = "stereo_type"

.field public static final STEREO_TYPE_2D:I = 0x0

.field public static final STEREO_TYPE_FRAME_SEQUENCE:I = 0x1

.field public static final STEREO_TYPE_SIDE_BY_SIDE:I = 0x2

.field public static final STEREO_TYPE_SWAP_LEFT_RIGHT:I = 0x4

.field public static final STEREO_TYPE_SWAP_TOP_BOTTOM:I = 0x5

.field public static final STEREO_TYPE_TOP_BOTTOM:I = 0x3

.field public static final STEREO_TYPE_UNKNOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "StereoHelper"

.field public static final VALUE_PARAMETER_3D_AC_OFF:I = 0x7fffffff

.field private static final mIsMpoSupported:Z

.field private static final mIsStereoDisplaySupported:Z

.field private static final sStereoFalse:Ljava/lang/String; = "(stereo_type=0 OR stereo_type=-1 OR stereo_type IS NULL)"

.field public static sStereoIcon:Lcom/android/gallery3d/ui/Texture; = null

.field private static sStereoOverlay:Landroid/graphics/drawable/Drawable; = null

.field private static final sStereoTrue:Ljava/lang/String; = "(stereo_type!=0 AND stereo_type!=-1 AND stereo_type IS NOT NULL)"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->PROJECTION_DATA:[Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isMpoSupported()Z

    move-result v0

    sput-boolean v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsMpoSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v0

    sput-boolean v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsStereoDisplaySupported:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adjustDim(ZII)I
    .locals 1
    .param p0    # Z
    .param p1    # I
    .param p2    # I

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    div-int/lit8 p2, p2, 0x2

    :cond_0
    :goto_0
    return p2

    :cond_1
    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    div-int/lit8 p2, p2, 0x2

    goto :goto_0
.end method

.method public static adjustRect(ZZLandroid/graphics/Rect;)V
    .locals 6
    .param p0    # Z
    .param p1    # Z
    .param p2    # Landroid/graphics/Rect;

    if-nez p2, :cond_0

    const-string v0, "StereoHelper"

    const-string v1, "adjustRect:got null image rect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_1

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_1
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    iget v5, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_3
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    iget v5, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public static convertToLocalLayout(I)I
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x10

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static drawImageTypeOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/graphics/Bitmap;

    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    if-nez v13, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0200d9

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sput-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    :cond_0
    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    const-string v13, "StereoHelper"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "original stereo overlay w="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", h="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-float v13, v12

    int-to-float v14, v5

    div-float v1, v13, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ge v2, v3, :cond_2

    const/4 v6, 0x1

    :goto_0
    if-eqz v6, :cond_1

    move v3, v2

    :cond_1
    div-int/lit8 v9, v3, 0x5

    if-eqz v6, :cond_3

    move v5, v9

    int-to-float v13, v9

    mul-float/2addr v13, v1

    float-to-int v12, v13

    :goto_1
    const-string v13, "StereoHelper"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "scaled stereo overlay w="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", h="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x2

    add-int/lit8 v4, v2, -0x2

    sub-int v11, v4, v5

    add-int v8, v12, v7

    const-string v13, "StereoHelper"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "stereo overlay drawing dimension=("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v7, v11, v8, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v10, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v13, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v10}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_3
    move v12, v9

    int-to-float v13, v12

    div-float/2addr v13, v1

    float-to-int v5, v13

    goto :goto_1
.end method

.method public static drawLeftBottom(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/ui/Texture;IIII)V
    .locals 6
    .param p0    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p1    # Lcom/android/gallery3d/ui/Texture;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/android/gallery3d/ui/Texture;->getWidth()I

    move-result v4

    invoke-interface {p1}, Lcom/android/gallery3d/ui/Texture;->getHeight()I

    move-result v5

    add-int v0, p3, p5

    sub-int v3, v0, v5

    move-object v0, p1

    move-object v1, p0

    move v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/android/gallery3d/ui/Texture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    goto :goto_0
.end method

.method public static dumpBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 5
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Ljava/lang/String;

    const-string v2, "StereoHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dumpBitmap("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p0, :cond_1

    const-string v2, "StereoHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dumpBitmap:["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "StereoHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dumpBitmap:config:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/DCIM/Bitmap["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "].png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v2

    :goto_1
    if-eqz v0, :cond_1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_3
    throw v2

    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_3

    :catchall_1
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method public static generateSecondImage(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 16
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p3    # Z

    if-nez p1, :cond_0

    const/4 v8, 0x0

    :goto_0
    return-object v8

    :cond_0
    const/16 v1, 0x28

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt v1, v3, :cond_1

    const/16 v1, 0x3c

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v1, v3, :cond_2

    :cond_1
    const-string v1, "StereoHelper"

    const-string v3, "generateSecondImage:image dimension too small"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    const/16 v1, 0x400

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v1, v3}, Lcom/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    rem-int/lit8 v6, v4, 0x2

    rem-int/lit8 v7, v5, 0x2

    const-string v1, "StereoHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "generateSecondImage:params.inRotation="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v15, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    add-int v1, v6, v7

    if-gtz v1, :cond_3

    move-object/from16 v0, p2

    iget v1, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    if-eqz v1, :cond_9

    :cond_3
    const-string v1, "StereoHelper"

    const-string v3, "generateSecondImage:resize or rotate before convert"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p2

    iget v1, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    rem-int/lit16 v1, v1, 0xb4

    if-nez v1, :cond_6

    const/4 v13, 0x1

    :goto_1
    if-eqz v13, :cond_7

    add-int v12, v4, v6

    :goto_2
    if-eqz v13, :cond_8

    add-int v11, v5, v7

    :goto_3
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v11, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p2

    iget v1, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    invoke-static {v1, v4, v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getDrawBitmapMatrix(III)Landroid/graphics/Matrix;

    move-result-object v10

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :goto_4
    if-eqz p3, :cond_4

    move-object/from16 v0, p1

    if-eq v0, v9, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    invoke-static {v9}, Lcom/mediatek/gallery3d/stereo/StereoConvertor;->convert2Dto3D(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz p3, :cond_5

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    invoke-interface/range {p0 .. p0}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v13, 0x0

    goto :goto_1

    :cond_7
    add-int v12, v5, v7

    goto :goto_2

    :cond_8
    add-int v11, v4, v6

    goto :goto_3

    :cond_9
    move-object/from16 v9, p1

    goto :goto_4

    :cond_a
    if-nez v2, :cond_b

    const-string v1, "StereoHelper"

    const-string v3, "generateSecondImage:failed to create stereo pair"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_b
    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-static/range {v1 .. v7}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->retrieveDataBundle(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;IIII)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    move-result-object v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public static getACEnabled(Landroid/content/Context;Z)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const-string v2, "StereoHelper"

    const-string v3, "getACEnabled:why we got a null context"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    const-string v2, "ac_switch"

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getAcPrefTAG(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method private static getAcPrefTAG(Z)Ljava/lang/String;
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const-string v0, "image_ac_switch"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "video_ac_switch"

    goto :goto_0
.end method

.method private static getDrawBitmapMatrix(III)Landroid/graphics/Matrix;
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sparse-switch p0, :sswitch_data_0

    :goto_0
    :sswitch_0
    int-to-float v1, p0

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    return-object v0

    :sswitch_1
    neg-int v1, p2

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :sswitch_2
    neg-int v1, p1

    int-to-float v1, v1

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :sswitch_3
    neg-int v1, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private static getExternalUri(Z)Landroid/net/Uri;
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "StereoHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFilePathFromUri:got file path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "StereoHelper"

    const-string v2, "getFilePathFromUri:got unknown uri scheme"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-static {p0, p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->queryFilePathFromDB(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInclusionFromData(Landroid/os/Bundle;)I
    .locals 5
    .param p0    # Landroid/os/Bundle;

    const/4 v3, 0x0

    sget-boolean v4, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsStereoDisplaySupported:Z

    if-eqz v4, :cond_0

    if-nez p0, :cond_3

    :cond_0
    sget-boolean v4, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsMpoSupported:Z

    if-eqz v4, :cond_2

    const/16 v2, 0x40

    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    const/16 v2, 0x1000

    or-int/lit16 v2, v2, 0x4000

    const-string v4, "onlyStereoMedia"

    invoke-virtual {p0, v4, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string v4, "get-album"

    invoke-virtual {p0, v4, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sget-boolean v3, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsMpoSupported:Z

    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    or-int/lit16 v2, v2, 0x80

    or-int/lit16 v2, v2, 0x100

    or-int/lit8 v2, v2, 0x1

    :cond_4
    :goto_1
    if-nez v1, :cond_1

    const v3, 0x8000

    or-int/2addr v2, v3

    goto :goto_0

    :cond_5
    or-int/lit16 v2, v2, 0x3c0

    goto :goto_1
.end method

.method public static getManualConvergencePointNum()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public static getMpoFrameIndex(ZI)I
    .locals 2
    .param p0    # Z
    .param p1    # I

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez p0, :cond_0

    const/4 v1, 0x4

    if-ne v1, p1, :cond_0

    const/4 v0, 0x2

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getOverlay(I)Lcom/android/gallery3d/ui/Texture;
    .locals 3
    .param p0    # I

    and-int/lit8 v0, p0, 0x10

    if-nez v0, :cond_0

    and-int/lit8 v0, p0, 0x40

    if-nez v0, :cond_0

    and-int/lit16 v0, p0, 0x80

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoIcon:Lcom/android/gallery3d/ui/Texture;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->sContext:Landroid/content/Context;

    const v2, 0x7f0200d9

    invoke-direct {v0, v1, v2}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoIcon:Lcom/android/gallery3d/ui/Texture;

    :cond_1
    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->sStereoIcon:Lcom/android/gallery3d/ui/Texture;

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getSplitBitmapBackpMatrix(IIIIIZ)Landroid/graphics/Matrix;
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    const/4 v5, 0x0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return-object v0

    :sswitch_0
    if-eqz p5, :cond_0

    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x0

    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    rsub-int v3, p0, 0x168

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    :cond_0
    div-int/lit8 v1, p1, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p5, :cond_1

    div-int/lit8 v1, p1, 0x2

    :goto_2
    const/4 v2, 0x0

    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    rsub-int v3, p0, 0x168

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    neg-int v3, p4

    int-to-float v3, v3

    invoke-virtual {v0, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :cond_1
    move v1, p1

    goto :goto_2

    :sswitch_2
    if-eqz p5, :cond_2

    div-int/lit8 v1, p1, 0x2

    :goto_3
    move v2, p2

    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    rsub-int v3, p0, 0x168

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    neg-int v3, p3

    int-to-float v3, v3

    neg-int v4, p4

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :cond_2
    move v1, p1

    goto :goto_3

    :sswitch_3
    if-eqz p5, :cond_3

    const/4 v1, 0x0

    :goto_4
    move v2, p2

    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    rsub-int v3, p0, 0x168

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    neg-int v3, p3

    int-to-float v3, v3

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :cond_3
    div-int/lit8 v1, p1, 0x2

    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private static getStereoClause(Z)Ljava/lang/String;
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const-string v0, "(stereo_type!=0 AND stereo_type!=-1 AND stereo_type IS NOT NULL)"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "(stereo_type=0 OR stereo_type=-1 OR stereo_type IS NULL)"

    goto :goto_0
.end method

.method public static getStereoVideoImage(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;ZI)Landroid/graphics/Bitmap;
    .locals 12
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z
    .param p3    # I

    const/16 v11, 0x8

    const/4 v10, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-lez v9, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-gtz v9, :cond_2

    :cond_0
    const-string v6, "StereoHelper"

    const-string v7, "getStereoVideoImage:got invalid original frame"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-static {p3}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->convertToLocalLayout(I)I

    move-result v4

    if-eqz v4, :cond_3

    if-ne v6, v4, :cond_4

    if-nez p2, :cond_4

    :cond_3
    const-string v6, "StereoHelper"

    const-string v7, "getStereoVideoImage:can not retrieve second image!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    goto :goto_0

    :cond_4
    const/4 v9, 0x2

    if-eq v9, v4, :cond_5

    if-ne v11, v4, :cond_8

    :cond_5
    move v3, v6

    :goto_1
    if-eq v11, v4, :cond_6

    const/16 v9, 0x10

    if-ne v9, v4, :cond_7

    :cond_6
    if-nez p2, :cond_9

    move p2, v6

    :cond_7
    :goto_2
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-direct {v5, v7, v7, v6, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v6, v5, Landroid/graphics/Rect;->right:I

    iget v7, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    iget v9, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v9

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v3, p2, v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustRect(ZZLandroid/graphics/Rect;)V

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {v2, v10, v10, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, p1, v5, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    if-eqz v0, :cond_1

    const-string v6, "StereoHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getStereoVideoImage:["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    move v3, v7

    goto :goto_1

    :cond_9
    move p2, v7

    goto :goto_2
.end method

.method public static getSurfaceLayout(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/high16 v0, 0x100000

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x200000

    goto :goto_0

    :pswitch_3
    const/high16 v0, 0x400000

    goto :goto_0

    :pswitch_4
    const/high16 v0, 0xa00000

    goto :goto_0

    :pswitch_5
    const/high16 v0, 0xc00000

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getSurfaceStereoMode(Z)I
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const/high16 v0, 0x80000

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getWhereClause(I)Ljava/lang/String;
    .locals 6
    .param p0    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    and-int/lit8 v2, p0, 0x1

    if-eqz v2, :cond_4

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    move-object v1, v0

    :cond_0
    :goto_0
    and-int/lit16 v2, p0, 0x4000

    if-eqz v2, :cond_1

    if-nez v1, :cond_3

    invoke-static {v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_1
    if-nez v1, :cond_8

    invoke-static {}, Lcom/mediatek/gallery3d/stereo/StereoConvertor;->getHideFolderWhereClause()Ljava/lang/String;

    move-result-object v1

    :goto_2
    return-object v1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    if-nez v1, :cond_6

    move-object v1, v0

    :cond_5
    :goto_3
    and-int/lit16 v2, p0, 0x4000

    if-nez v2, :cond_1

    if-nez v1, :cond_7

    invoke-static {v4}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v1

    :goto_4
    goto :goto_1

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/gallery3d/stereo/StereoConvertor;->getHideFolderWhereClause()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public static getWhereClause(IZ)Ljava/lang/String;
    .locals 6
    .param p0    # I
    .param p1    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/mediatek/gallery3d/mpo/MpoHelper;->getWhereClause(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    and-int/lit8 v2, p0, 0x1

    if-eqz v2, :cond_6

    if-nez p1, :cond_4

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    move-object v1, v0

    :cond_0
    :goto_0
    and-int/lit16 v2, p0, 0x1000

    if-eqz v2, :cond_1

    if-nez v1, :cond_3

    const-string v1, "mime_type=\'image/x-jps\'"

    :cond_1
    :goto_1
    if-nez v1, :cond_c

    invoke-static {}, Lcom/mediatek/gallery3d/stereo/StereoConvertor;->getHideFolderWhereClause()Ljava/lang/String;

    move-result-object v1

    :goto_2
    return-object v1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mime_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "image/x-jps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    and-int/lit16 v2, p0, 0x4000

    if-eqz v2, :cond_1

    if-nez v1, :cond_5

    invoke-static {v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v1

    :goto_3
    goto :goto_1

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_6
    if-nez p1, :cond_a

    if-eqz v0, :cond_7

    if-nez v1, :cond_8

    move-object v1, v0

    :cond_7
    :goto_4
    and-int/lit16 v2, p0, 0x1000

    if-nez v2, :cond_1

    if-nez v1, :cond_9

    const-string v1, "mime_type!=\'image/x-jps\'"

    :goto_5
    goto/16 :goto_1

    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mime_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "image/x-jps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_a
    and-int/lit16 v2, p0, 0x4000

    if-eqz v2, :cond_1

    if-nez v1, :cond_b

    invoke-static {v4}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v1

    :goto_6
    goto/16 :goto_1

    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getStereoClause(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/gallery3d/stereo/StereoConvertor;->getHideFolderWhereClause()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public static isStereo(I)Z
    .locals 1
    .param p0    # I

    if-eqz p0, :cond_0

    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isStereoFirstPath(Lcom/android/gallery3d/ui/GLRoot;)Z
    .locals 3
    .param p0    # Lcom/android/gallery3d/ui/GLRoot;

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-boolean v2, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsStereoDisplaySupported:Z

    if-eqz v2, :cond_1

    if-eqz p0, :cond_1

    invoke-interface {p0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoLayout()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoPassId()I

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static isStereoImage(Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0    # Lcom/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    const/high16 v2, 0x10000

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    const/high16 v2, 0x80000

    and-int/2addr v2, v0

    if-nez v2, :cond_0

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isStereoMediaFolder(Lcom/android/gallery3d/data/MediaSet;)Z
    .locals 4
    .param p0    # Lcom/android/gallery3d/data/MediaSet;

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    sget-boolean v2, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsStereoDisplaySupported:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v0

    instance-of v2, p0, Lcom/android/gallery3d/data/LocalAlbum;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v2, :cond_0

    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isStereoMode(Lcom/android/gallery3d/ui/GLRoot;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/GLRoot;

    sget-boolean v0, Lcom/mediatek/gallery3d/stereo/StereoHelper;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoLayout()I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTileViewVisible(II)Z
    .locals 1
    .param p0    # I
    .param p1    # I

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeShareProviderIgnorAction(Landroid/content/Intent;)V
    .locals 2
    .param p0    # Landroid/content/Intent;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "StereoHelper"

    const-string v1, "make share provider ignor action!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "share_selection_perform_action"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static needAutoFormatDection(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static queryFilePathFromDB(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const/4 v7, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mediatek/gallery3d/stereo/StereoHelper;->PROJECTION_DATA:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v1, "StereoHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queryFilePathFromDB:got file path "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_3
    throw v1
.end method

.method public static renderSubTypeOverlay(Lcom/android/gallery3d/ui/GLCanvas;IIIII)V
    .locals 6
    .param p0    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-static {p5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getOverlay(I)Lcom/android/gallery3d/ui/Texture;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->drawLeftBottom(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/ui/Texture;IIII)V

    goto :goto_0
.end method

.method private static retrieveDataBundle(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;IIII)Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;
    .locals 10
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const-string v0, "StereoHelper"

    const-string v1, "retrieveDataBundle: got null stereo or params"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    :cond_1
    :goto_0
    return-object v7

    :cond_2
    new-instance v7, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    invoke-direct {v7}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;-><init>()V

    const/4 v1, 0x1

    iget v6, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    move-object v0, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move/from16 v5, p6

    invoke-static/range {v0 .. v6}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->retrieveStereoImage(Landroid/graphics/Bitmap;ZIIIII)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-interface {p0}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v7, 0x0

    goto :goto_0

    :cond_3
    iget-boolean v0, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    if-eqz v0, :cond_5

    iput-object v8, v7, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    :cond_4
    :goto_1
    invoke-interface {p0}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->recycle()V

    const/4 v7, 0x0

    goto :goto_0

    :cond_5
    iget-boolean v0, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFullFrame:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    invoke-static {p0, v8, v0}, Lcom/mediatek/gallery3d/data/DecodeHelper;->getRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)Lcom/mediatek/gallery3d/data/RegionDecoder;

    move-result-object v0

    iput-object v0, v7, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    iget v6, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inRotation:I

    move-object v0, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move/from16 v5, p6

    invoke-static/range {v0 .. v6}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->retrieveStereoImage(Landroid/graphics/Bitmap;ZIIIII)Landroid/graphics/Bitmap;

    move-result-object v9

    iget-boolean v0, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    if-eqz v0, :cond_8

    iput-object v9, v7, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    :cond_7
    :goto_2
    invoke-interface {p0}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->recycle()V

    const/4 v7, 0x0

    goto :goto_0

    :cond_8
    iget-boolean v0, p2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFullFrame:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-static {p0, v9, v0}, Lcom/mediatek/gallery3d/data/DecodeHelper;->getRegionDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;Z)Lcom/mediatek/gallery3d/data/RegionDecoder;

    move-result-object v0

    iput-object v0, v7, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFullFrame:Lcom/mediatek/gallery3d/data/RegionDecoder;

    goto :goto_2
.end method

.method private static retrieveStereoImage(Landroid/graphics/Bitmap;ZIIIII)Landroid/graphics/Bitmap;
    .locals 9
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move v0, p6

    move v3, p4

    move v4, p5

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getSplitBitmapBackpMatrix(IIIIIZ)Landroid/graphics/Matrix;

    move-result-object v7

    const/4 v0, 0x0

    invoke-virtual {v6, p0, v7, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    return-object v8
.end method

.method public static setACEnabled(Landroid/content/Context;ZZ)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Z

    const/4 v2, 0x0

    if-nez p0, :cond_0

    const-string v3, "StereoHelper"

    const-string v4, "setACEnabled:why we got a null context"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    const-string v3, "ac_switch"

    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getAcPrefTAG(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static setSfStereoLayout(Landroid/view/SurfaceView;I)V
    .locals 1
    .param p0    # Landroid/view/SurfaceView;
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->setSfStereoLayout(Landroid/view/SurfaceView;IZ)V

    return-void
.end method

.method public static setSfStereoLayout(Landroid/view/SurfaceView;IZ)V
    .locals 5
    .param p0    # Landroid/view/SurfaceView;
    .param p1    # I
    .param p2    # Z

    const/high16 v4, 0xff0000

    if-nez p0, :cond_0

    const-string v1, "StereoHelper"

    const-string v2, "setSFStereoLayout:why we got null SurfaceView!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getSurfaceStereoMode(Z)I

    move-result v1

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getSurfaceLayout(I)I

    move-result v2

    or-int v0, v1, v2

    const-string v1, "StereoHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSfStereoLayout:call SurfaceView.setFlagsEx(0x00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "0x00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0, v4}, Landroid/view/SurfaceView;->setFlagsEx(II)V

    goto :goto_0
.end method

.method public static tryToShowConvertTo2DMode(Landroid/content/Context;)V
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v4, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "auto_to_2d"

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "auto_to_2d_tag"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const v3, 0x7f0c0194

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "auto_to_2d_tag"

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static updateConvergence(Landroid/content/Context;ILcom/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Lcom/android/gallery3d/data/MediaItem;

    if-eqz p2, :cond_0

    instance-of v4, p2, Lcom/android/gallery3d/data/LocalImage;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p2

    check-cast v1, Lcom/android/gallery3d/data/LocalImage;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "convergence"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/android/gallery3d/data/LocalMediaItem;->id:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v2, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updateConvergence(Landroid/content/Context;ZII)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    if-eqz p0, :cond_0

    if-gez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "convergence"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getExternalUri(Z)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static updateDatabaseForFilePath(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/ContentValues;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x1

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p2}, Landroid/media/MediaFile;->getFileTypeBySuffix(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    :goto_1
    const-string v4, "StereoHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateDatabaseForFilePath:got target Uri:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "_data= ?"

    new-array v5, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v0, v2, p1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-eq v7, v3, :cond_0

    const-string v4, "StereoHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateDatabaseForFilePath: why we updated "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " row(s)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-static {v1}, Landroid/media/MediaFile;->isImageFileType(I)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    :cond_3
    const-string v4, "StereoHelper"

    const-string v5, "updateDatabaseForFilePath:un-intended file type"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updateStereoLayout(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->updateStereoLayout(Landroid/content/Context;Landroid/net/Uri;IJ)V

    return-void
.end method

.method public static updateStereoLayout(Landroid/content/Context;Landroid/net/Uri;IJ)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # J

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "StereoHelper"

    const-string v4, "updateStereoLayout:got a Uri that is not file or from media DB"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-static {p0, p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v3, "StereoHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateStereoLayout:got null file path for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    const-string v3, "StereoHelper"

    const-string v4, "updateStereoLayout: why no file exist!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    invoke-virtual {v0, p3, p4}, Ljava/io/File;->setLastModified(J)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "StereoHelper"

    const-string v4, "updateStereoLayout: why setLastModified returns false!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "stereo_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "date_modified"

    const-wide/16 v4, 0x3e8

    div-long v4, p3, v4

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0, v2, v1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->updateDatabaseForFilePath(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static updateStereoLayout(Landroid/content/Context;ZII)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    if-eqz p0, :cond_0

    if-gez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "stereo_type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getExternalUri(Z)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method
