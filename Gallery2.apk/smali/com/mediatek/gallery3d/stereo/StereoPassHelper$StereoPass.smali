.class public Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;
.super Ljava/lang/Object;
.source "StereoPassHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/stereo/StereoPassHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StereoPass"
.end annotation


# instance fields
.field public rateH:F

.field public rateW:F

.field public rateX:F

.field public rateY:F

.field public scaleX:F

.field public scaleY:F

.field public transfX:F

.field public transfY:F


# direct methods
.method public constructor <init>()V
    .locals 9

    const/high16 v3, 0x3f800000

    const/4 v1, 0x0

    move-object v0, p0

    move v2, v1

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v1

    move v8, v1

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>(FFFFFFFF)V

    return-void
.end method

.method public constructor <init>(FFFFFFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F
    .param p8    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateX:F

    iput p2, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateY:F

    iput p3, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateW:F

    iput p4, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateH:F

    iput p5, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleX:F

    iput p6, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleY:F

    iput p7, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->transfX:F

    iput p8, p0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->transfY:F

    return-void
.end method
