.class public Lcom/mediatek/gallery3d/stereo/StereoPassHelper;
.super Ljava/lang/Object;
.source "StereoPassHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;
    }
.end annotation


# static fields
.field private static final STEREO_DRAW_FIRST_PASS:I = 0x0

.field private static final STEREO_DRAW_SECOND_PASS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "StereoPassHelper"

.field private static mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

.field private static mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v15, 0x1

    const/4 v14, 0x0

    const/high16 v4, 0x3f800000

    const/high16 v3, 0x3f000000

    const/4 v1, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    new-instance v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>()V

    aput-object v2, v0, v14

    sget-object v9, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    new-instance v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move v2, v1

    move v5, v3

    move v6, v4

    move v7, v1

    move v8, v1

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>(FFFFFFFF)V

    aput-object v0, v9, v15

    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    const/4 v2, 0x2

    new-instance v5, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move v6, v3

    move v7, v1

    move v8, v3

    move v9, v4

    move v10, v3

    move v11, v4

    move v12, v3

    move v13, v1

    invoke-direct/range {v5 .. v13}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>(FFFFFFFF)V

    aput-object v5, v0, v2

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    sput-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    new-instance v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>()V

    aput-object v2, v0, v14

    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    new-instance v5, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move v6, v1

    move v7, v1

    move v8, v4

    move v9, v3

    move v10, v4

    move v11, v3

    move v12, v1

    move v13, v1

    invoke-direct/range {v5 .. v13}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>(FFFFFFFF)V

    aput-object v5, v0, v15

    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    const/4 v2, 0x2

    new-instance v5, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move v6, v1

    move v7, v3

    move v8, v4

    move v9, v3

    move v10, v4

    move v11, v3

    move v12, v1

    move v13, v3

    invoke-direct/range {v5 .. v13}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;-><init>(FFFFFFFF)V

    aput-object v5, v0, v2

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStereoPass(II)Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;
    .locals 2
    .param p0    # I
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->getStereoPassForLayout(I)[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    aget-object v1, v0, p0

    goto :goto_0
.end method

.method public static getStereoPassForLayout(I)[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassSbs:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->mStereoPassTab:[Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static prepareForStereoPass(Lcom/android/gallery3d/ui/GLCanvas;IIII)V
    .locals 8
    .param p0    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    const/4 v0, 0x5

    if-le p2, v0, :cond_2

    :cond_0
    const-string v0, "StereoPassHelper"

    const-string v1, "prepareForStereoPass:got invalid params"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p1, p2}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->getStereoPass(II)Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move-result-object v7

    if-eqz v7, :cond_1

    int-to-float v0, p3

    iget v1, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateX:F

    mul-float/2addr v0, v1

    float-to-int v1, v0

    int-to-float v0, p4

    iget v2, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateY:F

    mul-float/2addr v0, v2

    float-to-int v2, v0

    int-to-float v0, p3

    iget v3, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateW:F

    mul-float/2addr v0, v3

    float-to-int v3, v0

    int-to-float v0, p4

    iget v4, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateH:F

    mul-float/2addr v0, v4

    float-to-int v4, v0

    move-object v0, p0

    move v5, p3

    move v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/android/gallery3d/ui/GLCanvas;->setSize(IIIIII)V

    int-to-float v0, p3

    iget v1, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->transfX:F

    mul-float/2addr v0, v1

    int-to-float v1, p4

    iget v2, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->transfY:F

    mul-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-interface {p0, v0, v1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FFF)V

    iget v0, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleX:F

    iget v1, v7, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleY:F

    const/high16 v2, 0x3f800000

    invoke-interface {p0, v0, v1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->scale(FFF)V

    goto :goto_0
.end method

.method public static setScissorBox(Lcom/android/gallery3d/ui/GLRoot;Ljavax/microedition/khronos/opengles/GL11;Landroid/graphics/Rect;II)V
    .locals 8
    .param p0    # Lcom/android/gallery3d/ui/GLRoot;
    .param p1    # Ljavax/microedition/khronos/opengles/GL11;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # I
    .param p4    # I

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_1

    :cond_0
    const-string v3, "StereoPassHelper"

    const-string v4, "setScissorBox:got invalid params"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-interface {p0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoLayout()I

    move-result v1

    invoke-interface {p0}, Lcom/android/gallery3d/ui/GLRoot;->getStereoPassId()I

    move-result v0

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->getStereoPass(II)Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v3, "StereoPassHelper"

    const-string v4, "setScissorBox:why we got null stereo pass"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    int-to-float v3, p3

    iget v4, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateX:F

    mul-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v5, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleX:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v4, p4

    iget v5, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->rateY:F

    mul-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget v6, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleY:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v4, p4, v4

    iget v5, p2, Landroid/graphics/Rect;->right:I

    iget v6, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleX:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    iget v7, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, v2, Lcom/mediatek/gallery3d/stereo/StereoPassHelper$StereoPass;->scaleY:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Lcom/mediatek/gallery3d/stereo/StereoPassHelper;->setScissorBox(Ljavax/microedition/khronos/opengles/GL11;IIII)V

    goto :goto_0
.end method

.method private static setScissorBox(Ljavax/microedition/khronos/opengles/GL11;IIII)V
    .locals 0
    .param p0    # Ljavax/microedition/khronos/opengles/GL11;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-eqz p0, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p0, p1, p2, p3, p4}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    goto :goto_0
.end method
