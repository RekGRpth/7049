.class Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;
.super Ljava/lang/Object;
.source "ConvergenceBarManager.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    const/4 v3, 0x0

    const-string v0, "ConvBarManager"

    const-string v1, "onLayoutChange"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-static {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$000(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    sub-int v1, p5, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$102(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$100(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$202(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-static {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$200(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Landroid/view/animation/TranslateAnimation;

    move-result-object v0

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-static {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$300(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-static {v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$200(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Landroid/view/animation/TranslateAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;->this$0:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->access$002(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Z)Z

    return-void
.end method
