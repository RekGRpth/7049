.class public interface abstract Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;
.super Ljava/lang/Object;
.source "ConvergenceBarManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConvergenceChangeListener"
.end annotation


# virtual methods
.method public abstract onConvValueChanged(I)V
.end method

.method public abstract onEnterConvTuningMode()V
.end method

.method public abstract onFirstRunHintDismissed()V
.end method

.method public abstract onFirstRunHintShown()V
.end method

.method public abstract onLeaveConvTuningMode(ZI)V
.end method
