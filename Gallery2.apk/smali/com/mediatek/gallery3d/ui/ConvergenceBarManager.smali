.class public Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
.super Ljava/lang/Object;
.source "ConvergenceBarManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;
    }
.end annotation


# static fields
.field private static final ANIM_SHOWUP_DURATION:I = 0x2bc

.field private static final PREFTAG_FIRSTHINT_SHOWED_IMAGE:Ljava/lang/String; = "first_hint_showed_image"

.field private static final PREFTAG_FIRSTHINT_SHOWED_VIDEO:Ljava/lang/String; = "first_hint_showed_video"

.field private static final SHARED_PREF_NAME:Ljava/lang/String; = "convergence_tuning"

.field private static final TAG:Ljava/lang/String; = "ConvBarManager"


# instance fields
.field private mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

.field private mAnimShowUp:Landroid/view/animation/TranslateAnimation;

.field private mBtnConvCancel:Landroid/widget/Button;

.field private mBtnConvOK:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

.field private mConvBarHeight:I

.field private mConvBarLayoutChangeFired:Z

.field private mConvBarLayoutListener:Landroid/view/View$OnLayoutChangeListener;

.field private mConvValues:[I

.field private mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

.field private mFirstRunLayoutChangeFired:Z

.field private mFirstRunLayoutListener:Landroid/view/View$OnLayoutChangeListener;

.field private mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

.field private mParent:Landroid/view/ViewGroup;

.field private mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

.field private mValue:I

.field private mValueMasks:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$1;-><init>(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutChangeFired:Z

    new-instance v0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$3;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$3;-><init>(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mParent:Landroid/view/ViewGroup;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarLayoutChangeFired:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarLayoutChangeFired:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarHeight:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarHeight:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Landroid/view/animation/TranslateAnimation;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
    .param p1    # Landroid/view/animation/TranslateAnimation;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutChangeFired:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutChangeFired:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Landroid/view/animation/TranslateAnimation;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;
    .param p1    # Landroid/view/animation/TranslateAnimation;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    return-object v0
.end method

.method private initConvergenceViews()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, -0x2

    const-string v3, "ConvBarManager"

    const-string v4, "initConvergenceViews"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mParent:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput v7, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarHeight:I

    iput-object v8, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    :cond_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mContext:Landroid/content/Context;

    const v4, 0x7f04000f

    invoke-static {v3, v4, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iput-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iput-boolean v7, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBarLayoutChangeFired:Z

    instance-of v3, v1, Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_2

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1, v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    const-string v3, "ConvBarManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " convergence bar="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const v4, 0x7f0b0022

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iput-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    const-string v3, "ConvBarManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " seek bar="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const v4, 0x7f0b0026

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvOK:Landroid/widget/Button;

    const-string v3, "ConvBarManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mBtnConvOK="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvOK:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const v4, 0x7f0b0025

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvCancel:Landroid/widget/Button;

    const-string v3, "ConvBarManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mBtnConvCancel="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvCancel:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-virtual {v3, p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvOK:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mBtnConvCancel:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_2
    instance-of v3, v1, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_1

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x50

    invoke-direct {v0, v6, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method private initFirstRunViews()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, -0x2

    const-string v4, "ConvBarManager"

    const-string v5, "initFirstRunViews"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mParent:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v8, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    :cond_1
    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mContext:Landroid/content/Context;

    const v5, 0x7f040052

    invoke-static {v4, v5, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iput-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayoutChangeFired:Z

    instance-of v4, v2, Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_3

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v2, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const v5, 0x7f0b011b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v4, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$2;

    invoke-direct {v4, p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$2;-><init>(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_3
    instance-of v4, v2, Landroid/widget/FrameLayout;

    if-eqz v4, :cond_2

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x50

    invoke-direct {v1, v7, v6, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v2, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private showConvergenceBar()V
    .locals 0

    return-void
.end method

.method private showStereoFirstRunHint()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->initFirstRunViews()V

    return-void
.end method


# virtual methods
.method public dismissFirstRun()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;->onFirstRunHintDismissed()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimFirstRunShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public enterConvTuningMode(Landroid/view/ViewGroup;[II)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # [I
    .param p3    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->enterConvTuningMode(Landroid/view/ViewGroup;[I[II)V

    return-void
.end method

.method public enterConvTuningMode(Landroid/view/ViewGroup;[I[II)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # [I
    .param p3    # [I
    .param p4    # I

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mParent:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvValues:[I

    iput-object p3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValueMasks:[I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->initConvergenceViews()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-virtual {v0, p2, p3, p4}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->setConvergenceValues([I[II)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;->onEnterConvTuningMode()V

    :cond_0
    return-void
.end method

.method public leaveConvTuningMode(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->leaveConvTuningMode(ZZ)V

    return-void
.end method

.method public leaveConvTuningMode(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    iget v2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValue:I

    invoke-interface {v1, p1, v2}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;->onLeaveConvTuningMode(ZI)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v1, 0x2bc

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->leaveConvTuningMode(Z)V

    goto :goto_0

    :pswitch_1
    iput v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValue:I

    invoke-virtual {p0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->leaveConvTuningMode(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0025
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iput p2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValue:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    invoke-interface {v0, p2}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;->onConvValueChanged(I)V

    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStereoMediaOpened(Z)V
    .locals 7
    .param p1    # Z

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mContext:Landroid/content/Context;

    const-string v6, "convergence_tuning"

    invoke-virtual {v5, v6, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    if-eqz p1, :cond_3

    const-string v3, "first_hint_showed_image"

    :goto_0
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_0

    move v1, v4

    :cond_0
    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->showStereoFirstRunHint()V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    invoke-interface {v5}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;->onFirstRunHintShown()V

    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    return-void

    :cond_3
    const-string v3, "first_hint_showed_video"

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public overrideAutoRectify(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-virtual {v0, p1}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->overrideAutoRectify(I)V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->pause()V

    :cond_0
    return-void
.end method

.method public reloadConvergenceBar()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->initConvergenceViews()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mSeekBar:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvValues:[I

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValueMasks:[I

    iget v3, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mValue:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->setConvergenceValues([I[II)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mConvBar:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public reloadFirstRun()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mFirstRunLayout:Lcom/mediatek/gallery3d/ui/RelativeLayoutIgnoreTouch;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->initFirstRunViews()V

    goto :goto_0
.end method

.method public setConvergenceListener(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->mListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    return-void
.end method
