.class public Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
.super Lcom/android/gallery3d/ui/BitmapScreenNail;
.source "MtkBitmapScreenNail.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MtkBitmapScreenNail"


# instance fields
.field protected mOriginHeight:I

.field protected mOriginWidth:I

.field protected mSubType:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(II)V

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    iput p1, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iput p2, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    iget v0, p0, Lcom/android/gallery3d/ui/BitmapScreenNail;->mWidth:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iget v0, p0, Lcom/android/gallery3d/ui/BitmapScreenNail;->mHeight:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    iput p2, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    iput p3, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    return-void
.end method


# virtual methods
.method public draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->permitShowThumb(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super/range {p0 .. p5}, Lcom/android/gallery3d/ui/BitmapScreenNail;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    :goto_0
    iget v5, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, Lcom/mediatek/gallery3d/drm/DrmHelper;->renderSubTypeOverlay(Lcom/android/gallery3d/ui/GLCanvas;IIIII)V

    return-void

    :cond_0
    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, p5

    const v5, -0xddddde

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/android/gallery3d/ui/GLCanvas;->fillRect(FFFFI)V

    goto :goto_0
.end method

.method public getOriginHeight()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginHeight:I

    return v0
.end method

.method public getOriginWidth()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mOriginWidth:I

    return v0
.end method

.method public getSubType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    return v0
.end method

.method public setSubType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->mSubType:I

    return-void
.end method
