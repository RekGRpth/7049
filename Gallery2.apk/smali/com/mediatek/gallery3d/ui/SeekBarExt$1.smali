.class Lcom/mediatek/gallery3d/ui/SeekBarExt$1;
.super Ljava/lang/Object;
.source "SeekBarExt.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/ui/SeekBarExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/ui/SeekBarExt;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    const-string v2, "Seekbarext"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onProgressChanged(progress="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",fromUser="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$100(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$100(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$100(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    array-length v2, v0

    if-lez v2, :cond_2

    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_2

    aget v2, v0, v1

    if-ne p2, v2, :cond_0

    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$300(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/os/Vibrator;

    move-result-object v2

    const-wide/16 v3, 0x32

    invoke-virtual {v2, v3, v4}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$200(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$000(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v3}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$400(Lcom/mediatek/gallery3d/ui/SeekBarExt;)I

    move-result v3

    add-int/2addr v3, p2

    invoke-interface {v2, p1, v3, p3}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$000(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onStartTrackingTouch(Landroid/widget/SeekBar;)V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;->this$0:Lcom/mediatek/gallery3d/ui/SeekBarExt;

    invoke-static {v0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->access$000(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onStopTrackingTouch(Landroid/widget/SeekBar;)V

    return-void
.end method
