.class public Lcom/mediatek/gallery3d/ui/SeekBarExt;
.super Landroid/widget/SeekBar;
.source "SeekBarExt.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Seekbarext"


# instance fields
.field private mAlwaysInTapRegion:Z

.field private mAutoRectifyRange:I

.field private mClientListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mContext:Landroid/content/Context;

.field private mConvArray:[I

.field private mConvOffset:I

.field private mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mIsDown:Z

.field private mMaskedArray:[I

.field private mMaxConvergence:I

.field private mSelfListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSqueezed:Z

.field private mTouchSlopSquare:I

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSqueezed:Z

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mTouchSlopSquare:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    new-instance v0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;-><init>(Lcom/mediatek/gallery3d/ui/SeekBarExt;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSelfListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mContext:Landroid/content/Context;

    const-string v0, "Seekbarext"

    const-string v1, "constructor #1 called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initializeDrawables()V

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initViewConfigurations(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSqueezed:Z

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mTouchSlopSquare:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    new-instance v0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;-><init>(Lcom/mediatek/gallery3d/ui/SeekBarExt;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSelfListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iput v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mContext:Landroid/content/Context;

    const-string v0, "Seekbarext"

    const-string v1, "constructor #2 called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initializeDrawables()V

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initViewConfigurations(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    iput-boolean v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSqueezed:Z

    iput v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    iput v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mTouchSlopSquare:I

    iput-object v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iput-boolean v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    new-instance v0, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt$1;-><init>(Lcom/mediatek/gallery3d/ui/SeekBarExt;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSelfListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iput v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    iput-boolean v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mContext:Landroid/content/Context;

    const-string v0, "Seekbarext"

    const-string v1, "constructor #3 called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Seekbarext"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "original thumb offset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getThumbOffset()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initializeDrawables()V

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->initViewConfigurations(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mClientListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/ui/SeekBarExt;)[I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/ui/SeekBarExt;)Landroid/os/Vibrator;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mVibrator:Landroid/os/Vibrator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/ui/SeekBarExt;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/SeekBarExt;

    iget v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    return v0
.end method

.method private initViewConfigurations(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int v1, v0, v0

    iput v1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mTouchSlopSquare:I

    return-void
.end method

.method private initializeDrawables()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mVibrator:Landroid/os/Vibrator;

    return-void
.end method

.method private onSingleTapUp(I)V
    .locals 5
    .param p1    # I

    const-string v2, "Seekbarext"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSingleTapUp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    array-length v2, v2

    if-lez v2, :cond_0

    iget v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    if-gtz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    array-length v2, v2

    if-lez v2, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_0

    const-string v2, "Seekbarext"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onSingleTapUp: array["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget v2, v0, v1

    sub-int/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    if-gt v2, v3, :cond_2

    aget v2, v0, v1

    invoke-virtual {p0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v3, 0x32

    invoke-virtual {v2, v3, v4}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 30
    .param p1    # Landroid/graphics/Canvas;

    monitor-enter p0

    :try_start_0
    const-string v25, "Seekbarext"

    const-string v26, "onDraw"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    const-string v25, "Seekbarext"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "  orig drawable height="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->height()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v25, "Seekbarext"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "  intrinsic drawable height="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v23

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v13, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingTop()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    move/from16 v25, v0

    if-lez v25, :cond_0

    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    if-lez v25, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    :goto_1
    move-object v5, v6

    array-length v15, v5

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v15, :cond_3

    aget v10, v5, v11

    int-to-float v0, v10

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v24, v25, v26

    const/high16 v25, 0x3f800000

    cmpl-float v25, v24, v25

    if-lez v25, :cond_2

    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    goto :goto_1

    :cond_2
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v24

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v17, v0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    add-int v25, v18, v17

    div-int/lit8 v26, v13, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-int v26, v16, v14

    div-int/lit8 v26, v26, 0x2

    sub-int v26, v26, v12

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvergenceIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v25

    monitor-exit p0

    throw v25

    :cond_3
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v21

    if-ne v0, v14, :cond_5

    const/16 v25, 0x1

    :goto_4
    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSqueezed:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSqueezed:Z

    move/from16 v25, v0

    if-nez v25, :cond_4

    if-lez v21, :cond_4

    sub-int v9, v21, v14

    if-lez v9, :cond_4

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    div-int/lit8 v27, v9, 0x2

    add-int v26, v26, v27

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    div-int/lit8 v29, v9, 0x2

    sub-int v28, v28, v29

    move-object/from16 v0, v22

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    const-string v25, "Seekbarext"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "  after squeeze bounds="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v25, "Seekbarext"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "  after squeeze height="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->height()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v25, "Seekbarext"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, " final drawing progress h="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->height()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p1}, Landroid/widget/AbsSeekBar;->onDraw(Landroid/graphics/Canvas;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_5
    const/16 v25, 0x0

    goto/16 :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_1
    :goto_1
    if-eqz v3, :cond_3

    move v4, v5

    goto :goto_0

    :pswitch_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    goto :goto_1

    :pswitch_1
    iget-boolean v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    if-nez v6, :cond_2

    move v4, v5

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget-object v7, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    sub-float/2addr v6, v7

    float-to-int v0, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    sub-float/2addr v6, v7

    float-to-int v1, v6

    mul-int v6, v0, v0

    mul-int v7, v1, v1

    add-int v2, v6, v7

    iget v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mTouchSlopSquare:I

    if-le v2, v6, :cond_1

    iput-boolean v4, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    goto :goto_1

    :pswitch_2
    iput-boolean v4, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    iget-boolean v4, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAlwaysInTapRegion:Z

    if-eqz v4, :cond_1

    const-string v4, "Seekbarext"

    const-string v6, "onSingleTapUp!!!"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/mediatek/gallery3d/ui/SeekBarExt;->onSingleTapUp(I)V

    const/4 v3, 0x1

    goto :goto_1

    :pswitch_3
    iput-boolean v4, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    goto :goto_1

    :cond_3
    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public overrideAutoRectify(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    return-void
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mIsDown:Z

    return-void
.end method

.method public setConvergenceValues([I[II)V
    .locals 11
    .param p1    # [I
    .param p2    # [I
    .param p3    # I

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz p1, :cond_0

    array-length v6, p1

    if-gtz v6, :cond_2

    :cond_0
    const-string v6, "Seekbarext"

    const-string v7, "convergence array is not valid"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    array-length v6, p1

    invoke-static {p1, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    if-eqz v0, :cond_1

    array-length v6, v0

    if-lez v6, :cond_1

    aget v6, v0, v8

    iput v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    const/4 v1, 0x0

    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_3

    aget v6, v0, v1

    iget v9, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    sub-int/2addr v6, v9

    aput v6, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    aget v6, v0, v6

    iput v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    iget v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    invoke-virtual {p0, v6}, Landroid/widget/AbsSeekBar;->setMax(I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvArray:[I

    if-eqz p2, :cond_5

    array-length v6, p2

    array-length v9, v0

    if-ne v6, v9, :cond_5

    move v2, v7

    :goto_2
    const-string v9, "Seekbarext"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mask is "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v2, :cond_6

    const-string v6, "valid"

    :goto_3
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_8

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_4
    array-length v6, p2

    if-ge v1, v6, :cond_7

    aget v3, p2, v1

    if-eqz v3, :cond_4

    aget v6, v0, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    move v2, v8

    goto :goto_2

    :cond_6
    const-string v6, "invalid"

    goto :goto_3

    :cond_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_8

    new-array v6, v5, [I

    iput-object v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v5, :cond_8

    iget-object v9, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaskedArray:[I

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v9, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_8
    aget v6, v0, v8

    if-lt p3, v6, :cond_9

    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    aget v6, v0, v6

    if-gt p3, v6, :cond_9

    iget v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mConvOffset:I

    sub-int v6, p3, v6

    invoke-virtual {p0, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_9
    iget v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mMaxConvergence:I

    div-int/lit8 v6, v6, 0x8

    div-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    const-string v6, "Seekbarext"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setConvergenceValues: auto rectify radius="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mAutoRectifyRange:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v7}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mSelfListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-super {p0, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/SeekBarExt;->mClientListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method
