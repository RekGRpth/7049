.class Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;
.super Ljava/lang/Object;
.source "StereoVideoLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/ui/StereoVideoLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    const/4 v4, 0x0

    const-string v1, "StereoVideoLayout"

    const-string v2, "onLayoutChange"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-static {v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$000(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Z

    move-result v1

    if-nez v1, :cond_0

    sub-int v1, p5, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    new-instance v2, Landroid/view/animation/TranslateAnimation;

    int-to-float v3, v0

    invoke-direct {v2, v4, v4, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$102(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-static {v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$100(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Landroid/view/animation/TranslateAnimation;

    move-result-object v1

    const-wide/16 v2, 0x2bc

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-static {v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$200(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$100(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Landroid/view/animation/TranslateAnimation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;->this$0:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->access$002(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;Z)Z

    return-void
.end method
