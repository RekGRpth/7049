.class public interface abstract Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;
.super Ljava/lang/Object;
.source "StereoVideoLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/ui/StereoVideoLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoLayoutListener"
.end annotation


# virtual methods
.method public abstract onEnterVideoLayoutMode()V
.end method

.method public abstract onLeaveVideoLayoutMode(ZI)V
.end method

.method public abstract onVideoLayoutChanged(I)V
.end method
