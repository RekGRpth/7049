.class public Lcom/mediatek/gallery3d/ui/StereoVideoLayout;
.super Ljava/lang/Object;
.source "StereoVideoLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;,
        Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;
    }
.end annotation


# static fields
.field private static final ANIM_SHOWUP_DURATION:I = 0x2bc

.field private static final LAYOUT_TYPE_COUNT:I = 0x5

.field private static final TAG:Ljava/lang/String; = "StereoVideoLayout"

.field private static sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;


# instance fields
.field private mAnimShowUp:Landroid/view/animation/TranslateAnimation;

.field private mBtnCancel:Landroid/widget/Button;

.field private mBtnOK:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private final mLayoutButtons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

.field private mParent:Landroid/view/ViewGroup;

.field private mSelectedViewId:I

.field private mVideoLayout:Landroid/widget/RelativeLayout;

.field private mVideoLayoutChangeFired:Z

.field private mVideoLayoutListener:Landroid/view/View$OnLayoutChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x5

    new-array v2, v7, [Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    sput-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const/4 v0, 0x0

    sget-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    add-int/lit8 v1, v0, 0x1

    new-instance v3, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const/4 v4, 0x0

    const v5, 0x7f0b011d

    const v6, 0x7f0c018e

    invoke-direct {v3, v4, v5, v6}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;-><init>(III)V

    aput-object v3, v2, v0

    sget-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    add-int/lit8 v0, v1, 0x1

    new-instance v3, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const/4 v4, 0x2

    const v5, 0x7f0b011e

    const v6, 0x7f0c018f

    invoke-direct {v3, v4, v5, v6}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;-><init>(III)V

    aput-object v3, v2, v1

    sget-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    add-int/lit8 v1, v0, 0x1

    new-instance v3, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const/4 v4, 0x3

    const v5, 0x7f0b011f

    const v6, 0x7f0c0190

    invoke-direct {v3, v4, v5, v6}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;-><init>(III)V

    aput-object v3, v2, v0

    sget-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    add-int/lit8 v0, v1, 0x1

    new-instance v3, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const/4 v4, 0x4

    const v5, 0x7f0b0120

    const v6, 0x7f0c0191

    invoke-direct {v3, v4, v5, v6}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;-><init>(III)V

    aput-object v3, v2, v1

    sget-object v2, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    add-int/lit8 v1, v0, 0x1

    new-instance v3, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    const v4, 0x7f0b0121

    const v5, 0x7f0c0192

    invoke-direct {v3, v7, v4, v5}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;-><init>(III)V

    aput-object v3, v2, v0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    new-instance v0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$1;-><init>(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mParent:Landroid/view/ViewGroup;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayoutChangeFired:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayoutChangeFired:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Landroid/view/animation/TranslateAnimation;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout;
    .param p1    # Landroid/view/animation/TranslateAnimation;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/ui/StereoVideoLayout;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private static getLayoutForView(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->viewId:I

    if-ne p0, v1, :cond_0

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->stereoLayout:I

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static getStringIdForView(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->viewId:I

    if-ne p0, v1, :cond_0

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->stringId:I

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static getViewForLayout(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->stereoLayout:I

    if-ne p0, v1, :cond_0

    sget-object v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->viewId:I

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private initVideoLayout()V
    .locals 8

    const/4 v7, 0x0

    const-string v5, "StereoVideoLayout"

    const-string v6, "initVideoLayout()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mParent:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v7, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    :cond_0
    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mContext:Landroid/content/Context;

    const v6, 0x7f040053

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayoutChangeFired:Z

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xc

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    const v6, 0x7f0b0122

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mBtnOK:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    const v6, 0x7f0b0102

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mBtnCancel:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mBtnOK:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mBtnCancel:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    :goto_0
    sget-object v5, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    sget-object v5, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->sLayoutViewString:[Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;

    aget-object v5, v5, v0

    iget v4, v5, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$LayoutViewString;->viewId:I

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    if-ne v5, v4, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private onClickVideoLayout(I)V
    .locals 7
    .param p1    # I

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    if-nez v2, :cond_0

    const-string v4, "StereoVideoLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClickVideoLayout: we why got null for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    if-ne p1, v1, :cond_1

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    iput p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->getLayoutForView(I)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    invoke-interface {v4, v3}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;->onVideoLayoutChanged(I)V

    return-void
.end method

.method private onLongClickVideoLayout(I)V
    .locals 5
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->getStringIdForView(I)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->getGravity()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/Toast;->getXOffset()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public enterVideoLayoutMode(Landroid/view/ViewGroup;I)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mParent:Landroid/view/ViewGroup;

    invoke-static {p2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->getViewForLayout(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->initVideoLayout()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;->onEnterVideoLayoutMode()V

    :cond_0
    return-void
.end method

.method public leaveVideoLayoutMode(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mSelectedViewId:I

    invoke-static {v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->getLayoutForView(I)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    invoke-interface {v2, p1, v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;->onLeaveVideoLayoutMode(ZI)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mAnimShowUp:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v3, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->onClickVideoLayout(I)V

    :goto_0
    return-void

    :cond_0
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->leaveVideoLayoutMode(Z)V

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->leaveVideoLayoutMode(Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0b0102 -> :sswitch_0
        0x7f0b0122 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mLayoutButtons:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->onLongClickVideoLayout(I)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public reloadVideoLayout()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->initVideoLayout()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mVideoLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVideoLayoutListener(Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->mListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    return-void
.end method
