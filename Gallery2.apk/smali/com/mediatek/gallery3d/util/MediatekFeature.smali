.class public Lcom/mediatek/gallery3d/util/MediatekFeature;
.super Ljava/lang/Object;
.source "MediatekFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;,
        Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    }
.end annotation


# static fields
.field public static final ALL_DRM_MEDIA:I = 0x1e

.field public static final ALL_MPO_MEDIA:I = 0x3c0

.field private static final CURSOR_MIMETYPE_IMAGE:Ljava/lang/String; = "vnd.android.cursor.dir/image"

.field private static final CURSOR_MIMETYPE_VIDEO:Ljava/lang/String; = "vnd.android.cursor.dir/video"

.field public static final EXCLUDE_DEFAULT_MEDIA:I = 0x1

.field public static final EXTRA_ENABLE_VIDEO_LIST:Ljava/lang/String; = "mediatek.intent.extra.ENABLE_VIDEO_LIST"

.field public static final INCLUDE_ALL_STEREO_MEDIA:I = 0x7180

.field public static final INCLUDE_CD_DRM_MEDIA:I = 0x4

.field public static final INCLUDE_FLDCF_DRM_MEDIA:I = 0x10

.field public static final INCLUDE_FL_DRM_MEDIA:I = 0x2

.field public static final INCLUDE_MPO_3D:I = 0x80

.field public static final INCLUDE_MPO_3D_PAN:I = 0x100

.field public static final INCLUDE_MPO_MAV:I = 0x40

.field public static final INCLUDE_MPO_UNKNOWN:I = 0x200

.field public static final INCLUDE_SD_DRM_MEDIA:I = 0x8

.field public static final INCLUDE_STEREO_FOLDER:I = 0x8000

.field public static final INCLUDE_STEREO_JPS:I = 0x1000

.field public static final INCLUDE_STEREO_PNS:I = 0x2000

.field public static final INCLUDE_STEREO_VIDEO:I = 0x4000

.field private static final MAV_VIEW_ACTION:Ljava/lang/String; = "com.mediatek.action.VIEW_MAV"

.field public static final MIMETYPE_GIF:Ljava/lang/String; = "image/gif"

.field private static final MIMETYPE_IMAGE_ALL:Ljava/lang/String; = "image/*"

.field public static final MIMETYPE_MPO:Ljava/lang/String; = "image/mpo"

.field private static final MIMETYPE_VIDEO_ALL:Ljava/lang/String; = "video/*"

.field private static final SUPPORT_STEREO_CONVERGENCE:Z

.field private static final TAG:Ljava/lang/String; = "MediatekFeature"

.field private static final cpuCoreNum:I = 0x2

.field private static final customizedForMedia3D:Z = true

.field private static final customizedForMyFavorite:Z = true

.field private static final customizedForVLW:Z = true

.field private static final gifBackGroundColor:I = -0x1

.field private static mMavOverlay:Lcom/android/gallery3d/ui/ResourceTexture; = null

.field public static sContext:Landroid/content/Context; = null

.field public static sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions; = null

.field public static sIsImageOptionsPrepared:Z = false

.field private static final supportBluetoothPrint:Z = true

.field private static final supportDisplay2dAs3d:Z

.field private static final supportDrm:Z = true

.field private static final supportGifAnimation:Z = true

.field private static final supportMAV:Z = true

.field private static final supportMpo:Z = true

.field private static final supportPictureQualityEnhance:Z = true

.field private static final supportStereoDisplay:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MtkUtils;->isSupport3d()Z

    move-result v0

    sput-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportStereoDisplay:Z

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportStereoDisplay:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportDisplay2dAs3d:Z

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportStereoDisplay:Z

    if-eqz v0, :cond_1

    :goto_1
    sput-boolean v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->SUPPORT_STEREO_CONVERGENCE:Z

    sput-boolean v2, Lcom/mediatek/gallery3d/util/MediatekFeature;->sIsImageOptionsPrepared:Z

    sput-object v3, Lcom/mediatek/gallery3d/util/MediatekFeature;->sContext:Landroid/content/Context;

    sput-object v3, Lcom/mediatek/gallery3d/util/MediatekFeature;->mMavOverlay:Lcom/android/gallery3d/ui/ResourceTexture;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addMtkInclusion(Landroid/net/Uri;Lcom/android/gallery3d/data/Path;)Landroid/net/Uri;
    .locals 3
    .param p0    # Landroid/net/Uri;
    .param p1    # Lcom/android/gallery3d/data/Path;

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v0, "MediatekFeature"

    const-string v1, "addMtkInclusion:invalid parameter"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    invoke-virtual {p1}, Lcom/android/gallery3d/data/Path;->getMtkInclusion()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "mtkInclusion"

    invoke-virtual {p1}, Lcom/android/gallery3d/data/Path;->getMtkInclusion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    const-string v0, "MediatekFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMtkInclusion:uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static allStereoSubType()I
    .locals 1

    const/16 v0, 0xf0

    return v0
.end method

.method public static checkForOtherPickActions(Lcom/android/gallery3d/app/PickerActivity;Landroid/os/Bundle;Landroid/content/Intent;)Z
    .locals 8
    .param p0    # Lcom/android/gallery3d/app/PickerActivity;
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v5, "video/*"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "vnd.android.cursor.dir/video"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_0
    move v2, v4

    :goto_0
    const-string v5, "image/*"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "vnd.android.cursor.dir/image"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_1
    move v1, v4

    :goto_1
    if-eqz v2, :cond_7

    const-string v5, "media-path"

    invoke-virtual {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_2
    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    move v3, v4

    :cond_4
    return v3

    :cond_5
    move v2, v3

    goto :goto_0

    :cond_6
    move v1, v3

    goto :goto_1

    :cond_7
    if-eqz v1, :cond_2

    const-string v5, "media-path"

    invoke-virtual {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static createGifDecoder(Ljava/io/FileDescriptor;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 1
    .param p0    # Ljava/io/FileDescriptor;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->createGifDecoderWrapper(Ljava/io/FileDescriptor;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method public static createGifDecoder(Ljava/io/InputStream;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 1
    .param p0    # Ljava/io/InputStream;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->createGifDecoderWrapper(Ljava/io/InputStream;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method public static createGifDecoder(Ljava/lang/String;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->createGifDecoderWrapper(Ljava/lang/String;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method public static cropToFitAspectRatio(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 16
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const-string v12, "MediatekFeature"

    const-string v13, "cropToRetainAspectRatio"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const/16 p0, 0x0

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v12, v11

    int-to-float v13, v8

    div-float v9, v12, v13

    move/from16 v0, p1

    int-to-float v12, v0

    move/from16 v0, p2

    int-to-float v13, v0

    div-float v3, v12, v13

    const-string v12, "MediatekFeature"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " srcRatio="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", destRatio="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-float v12, v9, v3

    if-eqz v12, :cond_1

    move v5, v11

    move v2, v8

    move-object/from16 v6, p0

    cmpl-float v12, v9, v3

    if-lez v12, :cond_4

    const/4 v7, 0x1

    :goto_1
    const/4 v10, 0x0

    const/4 v4, 0x0

    if-eqz v7, :cond_5

    int-to-float v12, v8

    mul-float/2addr v12, v3

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v5

    move v2, v8

    new-instance v10, Landroid/graphics/Rect;

    sub-int v12, v11, v5

    div-int/lit8 v12, v12, 0x2

    const/4 v13, 0x0

    add-int v14, v11, v5

    div-int/lit8 v14, v14, 0x2

    invoke-direct {v10, v12, v13, v14, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_2
    new-instance v4, Landroid/graphics/Rect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v4, v12, v13, v5, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v2, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v0, v10, v4, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    if-eqz p3, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    move-object/from16 p0, v6

    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    :cond_5
    int-to-float v12, v11

    div-float/2addr v12, v3

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v2

    move v5, v11

    new-instance v10, Landroid/graphics/Rect;

    const/4 v12, 0x0

    sub-int v13, v8, v2

    div-int/lit8 v13, v13, 0x2

    div-int/lit8 v14, v8, 0x2

    div-int/lit8 v15, v2, 0x2

    add-int/2addr v14, v15

    invoke-direct {v10, v12, v13, v11, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_2
.end method

.method public static doesMaxEqualMin(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->showDrmMicroThumb(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static drawImageTypeOverlay(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    :cond_0
    const-string v2, "MediatekFeature"

    const-string v3, "drawImageTypeOverlay:invalid params"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "image/pns"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "image/x-jps"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "image/mpo"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    if-eqz v0, :cond_5

    invoke-static {p0, p3}, Lcom/mediatek/gallery3d/mpo/MpoHelper;->drawImageTypeOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    :cond_5
    if-eqz v1, :cond_1

    invoke-static {p0, p3}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->drawImageTypeOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public static drawWidgetImageTypeOverlay(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "mime_type"

    aput-object v0, v2, v12

    const-string v0, "stereo_type"

    aput-object v0, v2, v11

    const-string v9, ""

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    if-eqz v10, :cond_5

    move v8, v11

    :goto_1
    const-string v0, "image/mpo"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-nez v8, :cond_6

    move v7, v11

    :goto_2
    if-eqz v7, :cond_4

    invoke-static {p0, p2}, Lcom/mediatek/gallery3d/mpo/MpoHelper;->drawImageTypeOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    :cond_4
    if-eqz v8, :cond_0

    invoke-static {p0, p2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->drawImageTypeOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_5
    move v8, v12

    goto :goto_1

    :cond_6
    move v7, v12

    goto :goto_2
.end method

.method public static enablePictureQualityEnhance(Landroid/graphics/BitmapFactory$Options;Z)V
    .locals 1
    .param p0    # Landroid/graphics/BitmapFactory$Options;
    .param p1    # Z

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    goto :goto_0
.end method

.method public static enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/util/MediatekFeature$Params;
    .param p1    # Z

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    goto :goto_0
.end method

.method public static getAddedMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "mpo"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "image/mpo"

    goto :goto_0

    :cond_2
    const-string v1, "jps"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "image/x-jps"

    goto :goto_0
.end method

.method public static getCpuCoreNum()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static getGifBackgroundColor()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public static getImageOptions()Lcom/mediatek/gallery3d/ext/IImageOptions;
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sIsImageOptionsPrepared:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->prepareImageOptions(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/mediatek/gallery3d/ext/ImageOptions;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/ext/ImageOptions;-><init>()V

    goto :goto_0
.end method

.method public static getInclusionFromData(Landroid/os/Bundle;)I
    .locals 2
    .param p0    # Landroid/os/Bundle;

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->getDrmInclusionFromData(Landroid/os/Bundle;)I

    move-result v0

    invoke-static {p0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getInclusionFromData(Landroid/os/Bundle;)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public static getMinimalScale(FFIII)F
    .locals 6
    .param p0    # F
    .param p1    # F
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-static {p4}, Lcom/mediatek/gallery3d/drm/DrmHelper;->showDrmMicroThumb(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide v2, 0x40e3880000000000L

    int-to-double v4, p2

    div-double/2addr v2, v4

    int-to-double v4, p3

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v2, v0

    :goto_0
    return v2

    :cond_0
    int-to-float v2, p2

    div-float v2, p0, v2

    int-to-float v3, p3

    div-float v3, p1, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto :goto_0
.end method

.method public static getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 5
    .param p0    # Lcom/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature;->needMtkScreenNail(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    new-instance v1, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    invoke-direct {v1, v3, v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    goto :goto_0
.end method

.method public static getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 5
    .param p0    # Lcom/android/gallery3d/data/MediaItem;
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature;->needMtkScreenNail(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v3

    :goto_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    :goto_2
    new-instance v1, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    invoke-direct {v1, p1, v3, v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;-><init>(Landroid/graphics/Bitmap;II)V

    invoke-virtual {v1, v2}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_2
.end method

.method public static getOnlyStereoWhereClause(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    or-int/lit16 v0, v0, 0x4000

    or-int/lit16 v0, v0, 0x80

    or-int/lit16 v0, v0, 0x100

    or-int/lit16 v0, v0, 0x1000

    or-int/lit8 v0, v0, 0x1

    or-int/lit16 v1, p0, 0x5181

    invoke-static {v1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getWhereClause(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getOnlyStereoWhereClause(IZ)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    or-int/lit16 v0, v0, 0x4000

    :goto_0
    or-int/lit8 v0, v0, 0x1

    or-int v1, v0, p0

    invoke-static {v1, p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getWhereClause(IZ)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    or-int/lit16 v0, v0, 0x80

    or-int/lit16 v0, v0, 0x100

    or-int/lit16 v0, v0, 0x1000

    goto :goto_0
.end method

.method public static getPhotoWidgetInclusion()I
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isDrmSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isMpoSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x40

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit16 v0, v0, 0x80

    or-int/lit16 v0, v0, 0x100

    or-int/lit16 v0, v0, 0x1000

    or-int/lit16 v0, v0, 0x2000

    or-int/lit16 v0, v0, 0x4000

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit16 v0, v0, 0x2000

    or-int/lit16 v0, v0, 0x4000

    goto :goto_0
.end method

.method public static getScreenNailSubType(Lcom/android/gallery3d/ui/ScreenNail;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSizeForSubtype(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/PhotoView$Size;
    .locals 4
    .param p0    # Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {p0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    and-int/lit16 v3, v2, 0x200

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginWidth()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginHeight()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v1, Lcom/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v1}, Lcom/android/gallery3d/ui/PhotoView$Size;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginWidth()I

    move-result v3

    iput v3, v1, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginHeight()I

    move-result v3

    iput v3, v1, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public static getWhereClause(I)Ljava/lang/String;
    .locals 5
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getWhereClause(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->getDrmWhereClause(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    if-nez v2, :cond_2

    move-object v2, v0

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    if-nez v2, :cond_3

    move-object v2, v1

    :cond_1
    :goto_1
    return-object v2

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static getWhereClause(IZ)Ljava/lang/String;
    .locals 5
    .param p0    # I
    .param p1    # Z

    invoke-static {p0, p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getWhereClause(IZ)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->getDrmWhereClause(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    if-nez v2, :cond_2

    move-object v2, v0

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    if-nez v2, :cond_3

    move-object v2, v1

    :cond_1
    :goto_1
    return-object v2

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static handleMavPlayback(Landroid/content/Context;Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->playMpo(Landroid/content/Context;Landroid/net/Uri;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasCustomizedForMedia3D()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static hasCustomizedForMyFavorite()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static hasCustomizedForVLW()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    sput-object p0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sContext:Landroid/content/Context;

    return-void
.end method

.method public static insertBucketIdForPickActions(Lcom/android/gallery3d/data/MediaSet;Landroid/content/Intent;)V
    .locals 2
    .param p0    # Lcom/android/gallery3d/data/MediaSet;
    .param p1    # Landroid/content/Intent;

    instance-of v1, p0, Lcom/android/gallery3d/data/LocalAlbum;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bucketId"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public static isBluetoothPrintSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isDisplay2dAs3dSupported()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportDisplay2dAs3d:Z

    return v0
.end method

.method public static isDrmSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isGifAnimationSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isGifSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isMAVSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isMpoSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isPictureQualityEnhanceSupported()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isStereoConvergenceSupported()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->SUPPORT_STEREO_CONVERGENCE:Z

    return v0
.end method

.method public static isStereoDisplaySupported()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportStereoDisplay:Z

    return v0
.end method

.method public static isStereoImage(Lcom/android/gallery3d/data/MediaObject;)Z
    .locals 3
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    const/4 v1, 0x0

    sget-boolean v2, Lcom/mediatek/gallery3d/util/MediatekFeature;->supportStereoDisplay:Z

    if-eqz v2, :cond_0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    const/high16 v2, 0x10000

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    const/high16 v2, 0x80000

    and-int/2addr v2, v0

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isSupportedByGifDecoder(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "image/gif"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static minScaleLimit(I)F
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->preferDisplayOriginSize(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method public static needMtkScreenNail(I)Z
    .locals 1
    .param p0    # I

    and-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_0

    and-int/lit8 v0, p0, 0x8

    if-nez v0, :cond_0

    and-int/lit16 v0, p0, 0x200

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static permitShowThumb(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->permitShowThumb(I)Z

    move-result v0

    return v0
.end method

.method public static playMpo(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mediatek.action.VIEW_MAV"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "image/mpo"

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MediatekFeature"

    const-string v3, "Unable to open mpo file: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static preferDisplayOriginSize(I)Z
    .locals 1
    .param p0    # I

    and-int/lit16 v0, p0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static preferDisplayOriginalSize()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sIsImageOptionsPrepared:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->prepareImageOptions(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IImageOptions;->shouldUseOriginalSize()Z

    move-result v0

    goto :goto_0
.end method

.method public static prepareImageOptions(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mediatek/gallery3d/ext/IImageOptions;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/content/pm/Signature;

    invoke-static {v1, v2, v3}, Lcom/mediatek/pluginmanager/PluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/ext/IImageOptions;

    sput-object v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    const-string v1, "MediatekFeature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareImageOptions:sImageOptions="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->sIsImageOptionsPrepared:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v1, "MediatekFeature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareImageOptions:JE happened"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mediatek/gallery3d/ext/ImageOptions;

    invoke-direct {v1}, Lcom/mediatek/gallery3d/ext/ImageOptions;-><init>()V

    sput-object v1, Lcom/mediatek/gallery3d/util/MediatekFeature;->sImageOptions:Lcom/mediatek/gallery3d/ext/IImageOptions;

    goto :goto_1
.end method

.method public static renderSubTypeOverlay(Landroid/content/Context;Lcom/android/gallery3d/ui/GLCanvas;III)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-nez p4, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v11, 0x1

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    and-int/2addr v11, v0

    if-eqz v11, :cond_2

    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->mMavOverlay:Lcom/android/gallery3d/ui/ResourceTexture;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f02009d

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->mMavOverlay:Lcom/android/gallery3d/ui/ResourceTexture;

    :cond_1
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v4, v0, 0x5

    sget-object v0, Lcom/mediatek/gallery3d/util/MediatekFeature;->mMavOverlay:Lcom/android/gallery3d/ui/ResourceTexture;

    sub-int v1, p2, v4

    div-int/lit8 v2, v1, 0x2

    sub-int v1, p3, v4

    div-int/lit8 v3, v1, 0x2

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/ui/ResourceTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p1

    move v8, p2

    move v9, p3

    move/from16 v10, p4

    invoke-static/range {v5 .. v10}, Lcom/mediatek/gallery3d/drm/DrmHelper;->renderSubTypeOverlay(Lcom/android/gallery3d/ui/GLCanvas;IIIII)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p1

    move v8, p2

    move v9, p3

    move/from16 v10, p4

    invoke-static/range {v5 .. v10}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->renderSubTypeOverlay(Lcom/android/gallery3d/ui/GLCanvas;IIIII)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static replaceBitmapBgColor(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # I
    .param p2    # Z

    const/4 v3, 0x0

    if-nez p0, :cond_0

    move-object p0, v3

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v4, v5, :cond_1

    const-string v3, "MediatekFeature"

    const-string v4, "replaceBitmapBgColor:Bitmap has no alpha, no bother"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-gtz v4, :cond_3

    :cond_2
    const-string v3, "MediatekFeature"

    const-string v4, "replaceBitmapBgColor:invalid Bitmap dimension"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v1, p0, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 p0, 0x0

    :cond_4
    move-object p0, v0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "MediatekFeature"

    const-string v4, "failed to create new bitmap for replacing gif background: "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static replaceGifBackGround(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->replaceGifBackground(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static replaceGifBackground(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0    # Landroid/graphics/Bitmap;

    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->replaceBitmapBgColor(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static showDrmMicroThumb(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/gallery3d/drm/DrmHelper;->showDrmMicroThumb(I)Z

    move-result v0

    return v0
.end method

.method public static syncSubType(Lcom/android/gallery3d/ui/ScreenNail;Lcom/android/gallery3d/ui/ScreenNail;)Z
    .locals 5
    .param p0    # Lcom/android/gallery3d/ui/ScreenNail;
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v3, "MediatekFeature"

    const-string v4, "syncSubType:why we got null target or source"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-static {p0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v1

    invoke-static {p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static updateSizeForSubtype(Lcom/android/gallery3d/ui/PhotoView$Size;Lcom/android/gallery3d/ui/ScreenNail;)V
    .locals 3
    .param p0    # Lcom/android/gallery3d/ui/PhotoView$Size;
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v1

    and-int/lit16 v2, v1, 0x200

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginWidth()I

    move-result v2

    iput v2, p0, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getOriginHeight()I

    move-result v2

    iput v2, p0, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method
