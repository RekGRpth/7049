.class Lcom/mediatek/gallery3d/video/BookmarkActivity$BookmarkAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "BookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/video/BookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookmarkAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/BookmarkActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/gallery3d/video/BookmarkActivity;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # Landroid/database/Cursor;
    .param p5    # [Ljava/lang/String;
    .param p6    # [I

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/BookmarkActivity$BookmarkAdapter;->this$0:Lcom/mediatek/gallery3d/video/BookmarkActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;

    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mId:J

    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mTitle:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mData:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    iget-object v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mTitleView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mDataView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;

    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/BookmarkActivity$BookmarkAdapter;->this$0:Lcom/mediatek/gallery3d/video/BookmarkActivity;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;-><init>(Lcom/mediatek/gallery3d/video/BookmarkActivity;Lcom/mediatek/gallery3d/video/BookmarkActivity$1;)V

    const v2, 0x7f0b000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mTitleView:Landroid/widget/TextView;

    const v2, 0x7f0b0010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/gallery3d/video/BookmarkActivity$ViewHolder;->mDataView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v1
.end method
