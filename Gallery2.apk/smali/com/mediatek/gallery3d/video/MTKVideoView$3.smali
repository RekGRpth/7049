.class Lcom/mediatek/gallery3d/video/MTKVideoView$3;
.super Ljava/lang/Object;
.source "MTKVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/video/MTKVideoView;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/video/MTKVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, -0x1

    const-string v2, "MTKVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1300(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v2

    if-ne v2, v5, :cond_1

    const-string v2, "MTKVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Duplicate error message. error message has been sent! error=("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/ui/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1402(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    const-string v2, "MTKVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onError() mSeekWhenPrepared="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1500(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDuration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1600(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1800(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1702(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2, v5}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1902(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2, v5}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2002(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2100(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2200(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/MediaController;->hide()V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2300(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2500(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2400(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-interface {v2, v3, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2600(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/16 v2, 0x104

    if-ne p2, v2, :cond_4

    const v0, 0x2050057

    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$3000(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1040010

    new-instance v4, Lcom/mediatek/gallery3d/video/MTKVideoView$3$1;

    invoke-direct {v4, p0}, Lcom/mediatek/gallery3d/video/MTKVideoView$3$1;-><init>(Lcom/mediatek/gallery3d/video/MTKVideoView$3;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0x105

    if-ne p2, v2, :cond_5

    const v0, 0x2050058

    goto :goto_1

    :cond_5
    const/16 v2, 0x106

    if-ne p2, v2, :cond_6

    const v0, 0x2050059

    goto :goto_1

    :cond_6
    const/16 v2, 0x107

    if-ne p2, v2, :cond_7

    const v0, 0x205005a

    goto :goto_1

    :cond_7
    const/16 v2, 0x108

    if-ne p2, v2, :cond_8

    const v0, 0x2050074

    goto :goto_1

    :cond_8
    const/16 v2, 0xc8

    if-ne p2, v2, :cond_9

    const v0, 0x1040015

    goto :goto_1

    :cond_9
    const v0, 0x1040011

    goto :goto_1
.end method
