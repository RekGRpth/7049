.class public Lcom/mediatek/gallery3d/video/MovieListHooker;
.super Lcom/mediatek/gallery3d/video/MovieHooker;
.source "MovieListHooker.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ext/IMovieListLoader$LoaderListener;


# static fields
.field private static final LOG:Z = true

.field private static final MENU_NEXT:I = 0x1

.field private static final MENU_PREVIOUS:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MovieListHooker"


# instance fields
.field private mMenuNext:Landroid/view/MenuItem;

.field private mMenuPrevious:Landroid/view/MenuItem;

.field private mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

.field private mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;-><init>()V

    return-void
.end method

.method private updatePrevNext()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "MovieListHooker"

    const-string v1, "updatePrevNext()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/gallery3d/ext/IMovieList;->isFirst(Lcom/mediatek/gallery3d/ext/IMovieItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/gallery3d/ext/IMovieList;->isLast(Lcom/mediatek/gallery3d/ext/IMovieItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/gallery3d/ext/IMovieList;->isFirst(Lcom/mediatek/gallery3d/ext/IMovieItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/gallery3d/ext/IMovieList;->isLast(Lcom/mediatek/gallery3d/ext/IMovieItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/mediatek/gallery3d/ext/MovieListLoader;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/ext/MovieListLoader;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getContext()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    invoke-interface {v0, v1, v2, p0, v3}, Lcom/mediatek/gallery3d/ext/IMovieListLoader;->fillVideoList(Landroid/content/Context;Landroid/content/Intent;Lcom/mediatek/gallery3d/ext/IMovieListLoader$LoaderListener;Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mediatek/gallery3d/ext/IMovieListLoader;->isEnabledVideoList(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c0179

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuPrevious:Landroid/view/MenuItem;

    invoke-virtual {p0, v3}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c0178

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMenuNext:Landroid/view/MenuItem;

    :cond_0
    return v3
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieLoader:Lcom/mediatek/gallery3d/ext/IMovieListLoader;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieListLoader;->cancelList()V

    return-void
.end method

.method public onListLoaded(Lcom/mediatek/gallery3d/ext/IMovieList;)V
    .locals 3
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieList;

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getContext()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v1, "MovieListHooker"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onListLoaded() "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public onMovieItemChanged(Lcom/mediatek/gallery3d/ext/IMovieItem;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/video/MovieHooker;->onMovieItemChanged(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/MovieListHooker;->updatePrevNext()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuOriginalId(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getPlayer()Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mediatek/gallery3d/ext/IMovieList;->getPrevious(Lcom/mediatek/gallery3d/ext/IMovieItem;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->startNextVideo(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getPlayer()Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MovieListHooker;->mMovieList:Lcom/mediatek/gallery3d/ext/IMovieList;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/mediatek/gallery3d/ext/IMovieList;->getNext(Lcom/mediatek/gallery3d/ext/IMovieItem;)Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->startNextVideo(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/MovieListHooker;->updatePrevNext()V

    const/4 v0, 0x1

    return v0
.end method
