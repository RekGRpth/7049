.class public Lcom/mediatek/gallery3d/video/ProxyDialog;
.super Landroid/app/AlertDialog;
.source "ProxyDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final BTN_CANCEL:I = -0x2

.field private static final BTN_OK:I = -0x1

.field private static final ERROR_HOST_EMPTY:I = 0x0

.field private static final ERROR_HOST_INVALID:I = 0x2

.field private static final ERROR_NONE:I = -0x1

.field private static final ERROR_PORT_EMPTY:I = 0x1

.field private static final ERROR_PORT_INVALID:I = 0x3

.field private static final LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "Gallery3D/ProxyDialog"

.field public static final TYPE_HTTP:I = 0x2

.field public static final TYPE_RTSP:I = 0x1

.field private static final UNKNOWN_PORT:I = -0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCr:Landroid/content/ContentResolver;

.field private mHost:Ljava/lang/String;

.field private mHostErrMsg:Landroid/widget/TextView;

.field private mHostField:Landroid/widget/EditText;

.field private mPort:Ljava/lang/String;

.field private mPortErrMsg:Landroid/widget/TextView;

.field private mPortField:Landroid/widget/EditText;

.field private mSettingKeyProxyHost:Ljava/lang/String;

.field private mSettingKeyProxyPort:Ljava/lang/String;

.field private final mType:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iput p2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    iget v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "mtk_rtsp_proxy_host"

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    const-string v0, "mtk_rtsp_proxy_port"

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    :goto_0
    const-string v0, "Gallery3D/ProxyDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ProxyDialog("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery3D/ProxyDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SETTING_KEY_PROXY_HOST="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery3D/ProxyDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SETTING_KEY_PROXY_PORT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const-string v0, "mtk_http_proxy_host"

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    const-string v0, "mtk_http_proxy_port"

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". type should be one of ProxyDialog.TYPE_RTSP and ProxyDialog.TYPE_HTTP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private enableProxy()V
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private showError(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostErrMsg:Landroid/widget/TextView;

    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortErrMsg:Landroid/widget/TextView;

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private validate()V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostErrMsg:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortErrMsg:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v6}, Lcom/mediatek/gallery3d/video/ProxyDialog;->showError(I)V

    const/4 v1, 0x0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v7}, Lcom/mediatek/gallery3d/video/ProxyDialog;->showError(I)V

    const/4 v1, 0x0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    const v3, 0xffff

    if-le v2, v3, :cond_3

    :cond_2
    const/4 v3, 0x3

    invoke-direct {p0, v3}, Lcom/mediatek/gallery3d/video/ProxyDialog;->showError(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :cond_3
    :goto_0
    invoke-virtual {p0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    invoke-virtual {p0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_4
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v3, "Gallery3D/ProxyDialog"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/ext/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v8}, Lcom/mediatek/gallery3d/video/ProxyDialog;->showError(I)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/ProxyDialog;->validate()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/ProxyDialog;->enableProxy()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, -0x1

    iget v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    const v2, 0x7f0c014a

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->setTitle(I)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04004b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    const v3, 0x7f0b00fc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    const v3, 0x7f0b00fe

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyHost:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHost:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostField:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mCr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mSettingKeyProxyPort:Ljava/lang/String;

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_6

    const-string v2, ""

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    :goto_2
    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortField:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    const v3, 0x7f0b00fb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostErrMsg:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostErrMsg:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mHostErrMsg:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mView:Landroid/view/View;

    const v3, 0x7f0b00fd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortErrMsg:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortErrMsg:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPortErrMsg:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mContext:Landroid/content/Context;

    const v3, 0x104000a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v5, v2, p0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v2, -0x2

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mContext:Landroid/content/Context;

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3, p0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/ProxyDialog;->validate()V

    return-void

    :cond_4
    const v2, 0x7f0c014e

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->setTitle(I)V

    goto/16 :goto_0

    :cond_5
    const-string v2, ""

    goto :goto_1

    :cond_6
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "Gallery3D/ProxyDialog"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/ext/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, ""

    iput-object v2, p0, Lcom/mediatek/gallery3d/video/ProxyDialog;->mPort:Ljava/lang/String;

    goto :goto_2

    :cond_7
    const-string v2, ""

    goto :goto_3
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
