.class Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;
.super Ljava/lang/Object;
.source "StereoVideoHooker.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/video/StereoVideoHooker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConvValueChanged(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$300(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$300(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    const/16 v1, 0x5de

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v2, p1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$400(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->setParameter(II)Z

    goto :goto_0
.end method

.method public onEnterConvTuningMode()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$700(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    return-void
.end method

.method public onFirstRunHintDismissed()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$600(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    return-void
.end method

.method public onFirstRunHintShown()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->onEnterConvTuningMode()V

    return-void
.end method

.method public onLeaveConvTuningMode(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$100(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$202(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setConvergence(I)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$600(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$300(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$300(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    move-result-object v0

    const/16 v1, 0x5de

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v3}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$200(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$400(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->setParameter(II)Z

    goto :goto_0
.end method
