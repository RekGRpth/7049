.class Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;
.super Ljava/lang/Object;
.source "StereoVideoHooker.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/video/StereoVideoHooker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterVideoLayoutMode()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$802(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$700(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    return-void
.end method

.method public onLeaveVideoLayoutMode(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const/4 v3, 0x0

    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLeaveVideoLayoutMode(saveValue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0, v3}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$802(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$900(Lcom/mediatek/gallery3d/video/StereoVideoHooker;IZ)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$1002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I

    :goto_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$600(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-static {v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$1000(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$900(Lcom/mediatek/gallery3d/video/StereoVideoHooker;IZ)V

    goto :goto_0
.end method

.method public onVideoLayoutChanged(I)V
    .locals 3
    .param p1    # I

    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVideoLayoutChanged(stereoType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;->this$0:Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->access$900(Lcom/mediatek/gallery3d/video/StereoVideoHooker;IZ)V

    return-void
.end method
