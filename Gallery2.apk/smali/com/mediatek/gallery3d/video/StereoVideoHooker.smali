.class public Lcom/mediatek/gallery3d/video/StereoVideoHooker;
.super Lcom/mediatek/gallery3d/video/MovieHooker;
.source "StereoVideoHooker.java"


# static fields
.field private static final COLUMN_CONVERGENCE:Ljava/lang/String; = "convergence"

.field private static final COLUMN_ID:Ljava/lang/String; = "_id"

.field private static final COLUMN_STEREO_TYPE:Ljava/lang/String; = "stereo_type"

.field private static final EXTRA_STEREO_TYPE:Ljava/lang/String; = "mediatek.intent.extra.STEREO_TYPE"

.field private static INDEX_CONVERGENCE:I = 0x0

.field private static INDEX_ID:I = 0x0

.field private static INDEX_STEREO_TYPE:I = 0x0

.field private static final LOG:Z = true

.field private static final MENU_AC:I = 0x3

.field private static final MENU_MC:I = 0x2

.field private static final MENU_STEREO_LAYOUTS:I = 0x4

.field private static final MENU_STEREO_VIDEO:I = 0x1

.field private static final PROJECTION:[Ljava/lang/String;

.field public static final STEREO_TYPE_2D:I = 0x0

.field public static final STEREO_TYPE_3D:I = 0x2

.field private static final TAG:Ljava/lang/String; = "StereoVideoHooker"

.field private static final UNKNOWN:I = -0x1


# instance fields
.field private final CENTER:I

.field private activeFlags:[I

.field private convergenceValues:[I

.field private mContext:Landroid/content/Context;

.field private mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

.field private mConvChangeListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

.field private mCurrentStereoMode:Z

.field private mCurrentStereoType:I

.field private mInConvergenceTuningMode:Z

.field private mInVideoLayoutMode:Z

.field private mMenuAC:Landroid/view/MenuItem;

.field private mMenuMC:Landroid/view/MenuItem;

.field private mMenuStereoVideo:Landroid/view/MenuItem;

.field private mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

.field private mRootView:Landroid/view/ViewGroup;

.field private mStereoLayouts:Landroid/view/MenuItem;

.field private mStoredProgress:I

.field private mStoredStereoType:I

.field private mTempProgressWhenPause:I

.field private mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

.field private mVideoLayoutListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

.field private mVideoSurface:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "stereo_type"

    aput-object v1, v0, v3

    const-string v1, "convergence"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->PROJECTION:[Ljava/lang/String;

    sput v2, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_ID:I

    sput v3, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_STEREO_TYPE:I

    sput v4, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_CONVERGENCE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x9

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;-><init>()V

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->convergenceValues:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->activeFlags:[I

    const/16 v0, 0x28

    iput v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->CENTER:I

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    new-instance v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker$1;-><init>(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvChangeListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    new-instance v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker$2;-><init>(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayoutListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0xa
        0x14
        0x1e
        0x28
        0x32
        0x3c
        0x46
        0x50
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0xa
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateConvergence(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredStereoType:I

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredStereoType:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Lcom/mediatek/gallery3d/ext/IMoviePlayer;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/video/StereoVideoHooker;I)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->getOffsetForSF(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateMediaPlayerUI()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/video/StereoVideoHooker;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->resetStereoMode()V

    return-void
.end method

.method static synthetic access$802(Lcom/mediatek/gallery3d/video/StereoVideoHooker;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/gallery3d/video/StereoVideoHooker;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/video/StereoVideoHooker;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoType(IZ)V

    return-void
.end method

.method private enhanceStereoActionBar(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V
    .locals 4
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;
    .param p2    # I

    const/4 v3, -0x1

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-ne p2, v3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setInfoFromMediaData(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V

    :cond_0
    :goto_0
    const-string v1, "StereoVideoHooker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enhanceStereoActionBar() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "media"

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-ne p2, v3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setInfoFromMediaUri(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V

    goto :goto_0
.end method

.method private enterDepthTuningMode()V
    .locals 5

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getConvergence()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enterDepthTuningMode:mStoredProgress="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    if-gez v0, :cond_0

    const/16 v0, 0x28

    iput v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mRootView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->convergenceValues:[I

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->activeFlags:[I

    iget v4, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->enterConvTuningMode(Landroid/view/ViewGroup;[I[II)V

    return-void
.end method

.method private enterVideoLayoutMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredStereoType:I

    const-string v0, "StereoVideoHooker"

    const-string v1, "enterVideoLayoutMode()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mRootView:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredStereoType:I

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->enterVideoLayoutMode(Landroid/view/ViewGroup;I)V

    return-void
.end method

.method private getOffsetForSF(I)I
    .locals 1
    .param p1    # I

    add-int/lit8 v0, p1, -0x28

    return v0
.end method

.method private initStereoDepthTuningBar()V
    .locals 3

    new-instance v0, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mRootView:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvChangeListener:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->setConvergenceListener(Lcom/mediatek/gallery3d/ui/ConvergenceBarManager$ConvergenceChangeListener;)V

    return-void
.end method

.method private initStereoVideoLayout()V
    .locals 3

    new-instance v0, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mRootView:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayoutListener:Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->setVideoLayoutListener(Lcom/mediatek/gallery3d/ui/StereoVideoLayout$VideoLayoutListener;)V

    return-void
.end method

.method private initialStereoVideoIcon(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoVideoIcon()V

    :cond_0
    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialStereoVideoIcon("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mSupport3DIcon="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private resetStereoMode()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(I)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoVideoIcon()V

    return-void
.end method

.method private setInfoFromMediaData(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V
    .locals 9
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;
    .param p2    # I

    const/4 v6, 0x0

    :try_start_0
    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data LIKE \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "file:///"

    const-string v2, ""

    invoke-virtual {v7, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v1, "StereoVideoHooker"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInfoFromMediaData() cursor="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_ID:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setId(I)V

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_STEREO_TYPE:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setStereoType(I)V

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_CONVERGENCE:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setConvergence(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInfoFromMediaData() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    :try_start_1
    const-string v0, "null"
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v8

    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private setInfoFromMediaUri(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V
    .locals 8
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;
    .param p2    # I

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_ID:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setId(I)V

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_STEREO_TYPE:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setStereoType(I)V

    sget v0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->INDEX_CONVERGENCE:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setConvergence(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInfoFromMediaUri() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v7

    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private updateConvergence(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getId()I

    move-result v2

    invoke-static {v0, v1, v2, p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->updateConvergence(Landroid/content/Context;ZII)V

    return-void
.end method

.method private updateMediaPlayerUI()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->updateUI()V

    :cond_0
    return-void
.end method

.method private updateStereoType(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateStereoLayout:stereoType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setStereoType(I)V

    invoke-virtual {p0, p1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(I)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->updateStereoLayout(Landroid/content/Context;Landroid/net/Uri;I)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoVideoIcon()V

    return-void
.end method

.method private updateStereoVideoIcon()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mCurrentStereoMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    const v1, 0x7f0200de

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    const v1, 0x7f0c017d

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateStereoVideoIcon() mMenuStereoVideoIcon="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    const v1, 0x7f0200df

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    const v1, 0x7f0c017e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public changeStereoMode()V
    .locals 2

    iget v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mCurrentStereoType:I

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mCurrentStereoMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(IZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    return-void
.end method

.method public isPartialVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->dismissFirstRun()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-virtual {v2, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->leaveConvTuningMode(Z)V

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-virtual {v2, v1}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->leaveVideoLayoutMode(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v0, "StereoVideoHooker"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->reloadFirstRun()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->reloadConvergenceBar()V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoLayout:Lcom/mediatek/gallery3d/ui/StereoVideoLayout;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/StereoVideoLayout;->reloadVideoLayout()V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v1, -0x1

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "mediatek.intent.extra.STEREO_TYPE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->enhanceStereoActionBar(Lcom/mediatek/gallery3d/ext/IMovieItem;I)V

    :cond_0
    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->setStereoType(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->initialStereoVideoIcon(I)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->initStereoDepthTuningBar()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->initStereoVideoLayout()V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(I)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0, v3}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c017d

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuStereoVideo:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->initialStereoVideoIcon(I)V

    invoke-virtual {p0, v4}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c018a

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuMC:Landroid/view/MenuItem;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c018c

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuAC:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuAC:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuActivityId(I)I

    move-result v0

    const v1, 0x7f0c018d

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStereoLayouts:Landroid/view/MenuItem;

    return v3
.end method

.method public onMovieItemChanged(Lcom/mediatek/gallery3d/ext/IMovieItem;)V
    .locals 2
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/video/MovieHooker;->onMovieItemChanged(Lcom/mediatek/gallery3d/ext/IMovieItem;)V

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(I)V

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->initialStereoVideoIcon(I)V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateConvergenceOffset()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->onStereoMediaOpened(Z)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->getMenuOriginalId(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v1, v2

    :goto_0
    return v1

    :sswitch_0
    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->changeStereoMode()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoVideoIcon()V

    goto :goto_0

    :sswitch_1
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->setACEnabled(Landroid/content/Context;ZZ)Z

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateConvergenceOffset()V

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->enterDepthTuningMode()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->enterVideoLayoutMode()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_3
        0x7f0b0142 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mConvBarManager:Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/ui/ConvergenceBarManager;->pause()V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    invoke-direct {p0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->updateStereoVideoIcon()V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuAC:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuAC:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getACEnabled(Landroid/content/Context;Z)Z

    move-result v0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuAC:Landroid/view/MenuItem;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuMC:Landroid/view/MenuItem;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    move v2, v3

    :goto_0
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mMenuMC:Landroid/view/MenuItem;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    move v2, v3

    :goto_1
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInConvergenceTuningMode:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mInVideoLayoutMode:Z

    if-eqz v2, :cond_1

    :cond_0
    move v3, v4

    :cond_1
    return v3

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_1
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Lcom/mediatek/gallery3d/video/MovieHooker;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    instance-of v0, p2, Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    check-cast p2, Landroid/view/SurfaceView;

    iput-object p2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoSurface:Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mRootView:Landroid/view/ViewGroup;

    goto :goto_0

    :cond_2
    instance-of v0, p2, Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    iput-object p2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    goto :goto_0
.end method

.method public setStereoLayout(I)V
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->setStereoLayout(IZ)V

    return-void
.end method

.method public setStereoLayout(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const-string v0, "StereoVideoHooker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setStereoLayout("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",displayStereo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mCurrentStereoType:I

    iput-boolean p2, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mCurrentStereoMode:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mVideoSurface:Landroid/view/SurfaceView;

    invoke-static {v0, p1, p2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->setSfStereoLayout(Landroid/view/SurfaceView;IZ)V

    return-void
.end method

.method public updateConvergenceOffset()V
    .locals 5

    const/16 v4, 0x5de

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getStereoType()I

    move-result v1

    invoke-static {v1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->isStereo(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->getACEnabled(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    const v2, 0x7fffffff

    invoke-interface {v1, v4, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->setParameter(II)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/gallery3d/video/MovieHooker;->getMovieItem()Lcom/mediatek/gallery3d/ext/IMovieItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getConvergence()I

    move-result v1

    iput v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    const-string v1, "StereoVideoHooker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateConvergenceOffset:mStoredProgress="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    if-gez v1, :cond_2

    const/16 v0, 0x28

    :goto_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mPlayer:Lcom/mediatek/gallery3d/ext/IMoviePlayer;

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->getOffsetForSF(I)I

    move-result v2

    invoke-interface {v1, v4, v2}, Lcom/mediatek/gallery3d/ext/IMoviePlayer;->setParameter(II)Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/gallery3d/video/StereoVideoHooker;->mStoredProgress:I

    goto :goto_1
.end method
