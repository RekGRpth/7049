.class public abstract Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;
.super Landroid/content/BroadcastReceiver;
.source "BaseWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public getWidgetRetryAction()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getWidgetUpdateAction()Ljava/lang/String;
.end method

.method public onDisable(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method

.method public onEnable(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method

.method public abstract onManualReload(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "appWidgetIds"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    if-lez v3, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.appwidget.action.APPWIDGET_ENABLED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onEnable(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-string v3, "android.appwidget.action.APPWIDGET_DISABLED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onDisable(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->getWidgetUpdateAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->getWidgetUpdateAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->getWidgetRetryAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->getWidgetRetryAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onManualReload(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->onDeviceStartup()V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->onWidgetUpdate()V

    return-void
.end method
