.class public final enum Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;
.super Ljava/lang/Enum;
.source "GConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkImplementation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum FAKE:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum FAKE_ALWAYS_FAIL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum FAKE_ALWAYS_FAIL_NEWS:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum FAKE_ALWAYS_FAIL_WEATHER:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum NO_CONTENT:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

.field public static final enum REAL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "REAL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->REAL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "FAKE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "FAKE_ALWAYS_FAIL"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "FAKE_ALWAYS_FAIL_NEWS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL_NEWS:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "FAKE_ALWAYS_FAIL_WEATHER"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL_WEATHER:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const-string v1, "NO_CONTENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->NO_CONTENT:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->REAL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL_NEWS:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->FAKE_ALWAYS_FAIL_WEATHER:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->NO_CONTENT:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    return-object v0
.end method
