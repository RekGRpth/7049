.class public Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;
.super Ljava/lang/Object;
.source "GenieApplication.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/GenieContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GenieApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DefaultGenieContext"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private calcQuickRefreshDelay(JJ)J
    .locals 5
    .param p1    # J
    .param p3    # J

    const-wide/32 v0, 0x1d4c0

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    cmp-long v2, p3, p1

    if-gez v2, :cond_0

    long-to-float v2, p1

    const v3, 0x3dcccccd

    mul-float/2addr v2, v3

    long-to-float v3, p3

    const v4, 0x3f666666

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-long v0, v2

    sub-long v2, p1, v0

    long-to-float v2, v2

    const v3, 0x476a6000

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    move-wide v0, p1

    goto :goto_0
.end method

.method private earliestAlarmTimeForDelay(J)J
    .locals 4
    .param p1    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3

    div-long v2, p1, v2

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public cancelScheduledModelUpdate()V
    .locals 4

    const-string v0, "Genie"

    const-string v1, "Canceling auto-refresh of News & Weather"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    const-class v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->alarm:Landroid/app/AlarmManager;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$300(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/app/AlarmManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public formatMillis(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->formatMillis(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBooleanPreference(IZ)Z
    .locals 2
    .param p1    # I
    .param p2    # Z

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public getCustomTopicPreference()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    const v7, 0x7f0b0008

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz v4, :cond_1

    const-string v7, "&"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    :try_start_0
    const-string v7, "UTF-8"

    invoke-static {v5, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v7, v3, v3}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v6

    :catch_0
    move-exception v7

    goto :goto_1
.end method

.method public getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v0

    return-object v0
.end method

.method public getLongPreference(IJ)J
    .locals 2
    .param p1    # I
    .param p2    # J

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNewsTopicPreference()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    const v6, 0x7f0b0007

    invoke-virtual {p0, v6, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-object v5

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-string v6, "&"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    :try_start_0
    new-instance v6, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    const-string v7, "UTF-8"

    invoke-static {v4, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringPreference(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v0, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object p2, v1

    goto :goto_0
.end method

.method public getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I

    move-result v0

    return v0
.end method

.method public logClick([BII)V
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;-><init>([BII)V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getNetwork()Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;->logClick(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)V

    return-void
.end method

.method public recordRefreshTime()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$100(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->setLongPreference(IJ)V

    return-void
.end method

.method public redrawWidget(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->requestRedraw(Ljava/lang/String;)V

    return-void
.end method

.method public refreshDataModel()V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move v2, v1

    move v4, v3

    move v5, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZ)V

    return-void
.end method

.method public registerConnectivityIntent(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->systemServicesMonitor:Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$500(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->registerConnectivityIntent(I)V

    return-void
.end method

.method public requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V
    .locals 1
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V

    return-void
.end method

.method public requestWidgetRefresh()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->miniWidget:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$200(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->requestRedraw()V

    return-void
.end method

.method public resetScheduleFalloff()V
    .locals 3

    const v0, 0x7f0b000c

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->setLongPreference(IJ)V

    return-void
.end method

.method public runOnUIThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->postToUI:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$400(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public scheduleModelUpdate(Z)V
    .locals 13

    const v12, 0x7f0b000c

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x3e8

    invoke-virtual {p0, v12, v10, v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getLongPreference(IJ)J

    move-result-wide v2

    const v4, 0x7f0b0006

    const-string v5, "10800"

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    mul-long/2addr v4, v8

    const v6, 0x7f0b000b

    invoke-virtual {p0, v6, v10, v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getLongPreference(IJ)J

    move-result-wide v6

    cmp-long v6, v6, v10

    if-nez v6, :cond_0

    move p1, v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->calcQuickRefreshDelay(JJ)J

    move-result-wide v4

    :cond_1
    cmp-long v2, v4, v2

    if-nez v2, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    const-class v6, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;

    invoke-direct {v2, v3, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-static {v3, v0, v2, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->alarm:Landroid/app/AlarmManager;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$300(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/app/AlarmManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->earliestAlarmTimeForDelay(J)J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->alarm:Landroid/app/AlarmManager;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$300(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Landroid/app/AlarmManager;

    move-result-object v0

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    invoke-virtual {p0, v12, v4, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->setLongPreference(IJ)V

    if-eqz p1, :cond_3

    const-string v0, "Genie"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Quick refresh set to "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-long/2addr v4, v8

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "s to wake up in "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    div-long/2addr v2, v8

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "s at the earliest."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v0, "Genie"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Refresh period set to "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-long/2addr v4, v8

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "s to wake up in "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    div-long/2addr v2, v8

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "s at the earliest."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public setCustomTopicPreference(Ljava/lang/Iterable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    :try_start_0
    iget-object v5, v3, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const v5, 0x7f0b0008

    invoke-virtual {p0, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setLongPreference(IJ)V
    .locals 2
    .param p1    # I
    .param p2    # J

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setNewsTopicPreference(Ljava/lang/Iterable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    :try_start_0
    iget-object v5, v3, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const v5, 0x7f0b0007

    invoke-virtual {p0, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public stopLocationMonitor()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->locationProvider:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$600(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->stopMonitor()V

    return-void
.end method

.method public unregisterConnectivityIntent()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->systemServicesMonitor:Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$500(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->unregisterConnectivityIntent()V

    return-void
.end method

.method public updateContentProvider(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .param p3    # J

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$DefaultGenieContext;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->updateContentProvider(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V

    return-void
.end method
