.class public Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;
.super Ljava/lang/Object;
.source "GeniePreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GeniePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PreferencesSnapshot"
.end annotation


# instance fields
.field private preferLocation:Ljava/lang/String;

.field private preferredCustomTopics:Ljava/lang/String;

.field private preferredTopics:Ljava/lang/String;

.field private prefetchPages:Z

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

.field private useCelsius:Z

.field private useMyLocation:Z

.field private weatherHasBeenUpdated:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GeniePreferences;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->weatherHasBeenUpdated:Z

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getPreferLocation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferLocation:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getPreferredTopics()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredTopics:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getCustomTopics()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredCustomTopics:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->isPrefetchPagesEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->prefetchPages:Z

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->useMyLocation()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useMyLocation:Z

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->useCelsius()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useCelsius:Z

    return-void
.end method


# virtual methods
.method public customTopicsChanged()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getCustomTopics()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredCustomTopics:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredCustomTopics:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public preferredTopicsChanged()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getPreferredTopics()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredTopics:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->preferredTopics:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public prefetchPagesChanged()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->prefetchPages:Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->isPrefetchPagesEnabled()Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public setWeatherHasBeenUpdated(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->weatherHasBeenUpdated:Z

    return-void
.end method

.method public useCelsiusChanged()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useCelsius:Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->useCelsius()Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public useMyLocationChanged()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->useMyLocation:Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->this$0:Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->useMyLocation()Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public weatherHasBeenUpdated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->weatherHasBeenUpdated:Z

    return v0
.end method
