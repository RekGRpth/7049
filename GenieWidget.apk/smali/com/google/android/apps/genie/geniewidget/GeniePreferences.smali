.class public Lcom/google/android/apps/genie/geniewidget/GeniePreferences;
.super Ljava/lang/Object;
.source "GeniePreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;
    }
.end annotation


# instance fields
.field private final genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

.field private snapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    return-void
.end method


# virtual methods
.method public getCustomTopics()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v1, 0x7f0b0008

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferLocation()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferencesSnapshot()Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->snapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    return-object v0
.end method

.method public getPreferredTopics()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v1, 0x7f0b0007

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPrefetchImagesEnabled()Z
    .locals 3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isPrefetchPagesEnabled()Z
    .locals 3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public newPreferencesSnapshot()Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;
    .locals 1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;-><init>(Lcom/google/android/apps/genie/geniewidget/GeniePreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->snapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->snapshot:Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    return-object v0
.end method

.method public useCelsius()Z
    .locals 3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0009

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public useMyLocation()Z
    .locals 3

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
