.class public Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;
.super Ljava/lang/Object;
.source "GenieProtoCodec.java"


# static fields
.field private static final LOG:Lcom/google/android/apps/genie/geniewidget/utils/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->LOG:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private buildCondition(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    .locals 16
    .param p1    # Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    :goto_0
    const/4 v10, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v10

    :goto_1
    const/16 v11, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v11, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    :goto_2
    const/16 v12, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setProbPrecipitation(I)V

    :cond_0
    return-object v1

    :cond_1
    const/16 v9, -0x3e7

    goto :goto_0

    :cond_2
    const/16 v10, -0x3e7

    goto :goto_1

    :cond_3
    const/16 v11, -0x3e7

    goto :goto_2
.end method

.method private buildNewsConfig(Ljava/util/List;Ljava/util/List;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)",
            "Lcom/google/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->NEWS_CONFIG:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v3}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x3

    const/16 v4, 0xa

    invoke-virtual {v1, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    const/4 v3, 0x1

    iget-object v4, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    const/4 v3, 0x2

    iget-object v4, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method private parseNews(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
    .locals 12
    .param p1    # Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v11, 0x7

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v11, 0x8

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v11, 0x2

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v11, 0xa

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v11, 0x6

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v7

    const/16 v11, 0xb

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v11, 0x4

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0x9

    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private parseWeather(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;Ljava/lang/String;Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
    .locals 12
    .param p1    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v11, 0x3

    const/4 v10, 0x4

    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getGeocodedCity()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object v0, v5

    :cond_1
    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    new-instance v8, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    invoke-direct {v8, v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-virtual {p3, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->buildCondition(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->setCurrentCondition(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V

    :cond_3
    invoke-virtual {p3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v4, :cond_4

    invoke-virtual {p3, v11, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->buildCondition(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->addForecast(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p3, v10}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {p3, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_5

    invoke-virtual {p3, v10, v7}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->buildCondition(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->addHourlyDatapoint(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_5
    return-object v8
.end method


# virtual methods
.method public createGenieRequestProto(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 15
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    new-instance v8, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->GENIE_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v12}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/16 v12, 0x2d

    const-wide/16 v13, 0x7530

    invoke-virtual {v8, v12, v13, v14}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "_"

    const-string v14, "-"

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v12, 0x21

    invoke-virtual {v8, v12, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v12, 0x20

    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->CONFIG:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v12}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getTypeFilter()Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_9

    :cond_1
    const/16 v12, 0xe

    const/4 v13, 0x1

    invoke-virtual {v0, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->addBool(IZ)V

    const/4 v12, 0x4

    const/4 v13, 0x1

    invoke-virtual {v0, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->addBool(IZ)V

    const/16 v12, 0x8

    const/4 v13, 0x1

    invoke-virtual {v0, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->addBool(IZ)V

    :cond_2
    const/4 v12, 0x6

    invoke-virtual {v8, v12, v0}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getCustomTopics()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getCustomTopics()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_a

    const/4 v2, 0x1

    :goto_0
    const/16 v12, 0xf

    invoke-virtual {v0, v12}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getNewsTopics()Ljava/util/List;

    move-result-object v12

    if-nez v12, :cond_3

    if-eqz v2, :cond_4

    :cond_3
    const/16 v12, 0x2c

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getNewsTopics()Ljava/util/List;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getCustomTopics()Ljava/util/List;

    move-result-object v14

    invoke-direct {p0, v13, v14}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->buildNewsConfig(Ljava/util/List;Ljava/util/List;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v5

    if-eqz v5, :cond_5

    new-instance v7, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->LOCATION:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v12}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x1

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLat()D

    move-result-wide v13

    invoke-virtual {v7, v12, v13, v14}, Lcom/google/common/io/protocol/ProtoBuf;->addDouble(ID)V

    const/4 v12, 0x2

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLng()D

    move-result-wide v13

    invoke-virtual {v7, v12, v13, v14}, Lcom/google/common/io/protocol/ProtoBuf;->addDouble(ID)V

    const/4 v12, 0x1

    invoke-virtual {v8, v12, v7}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    new-instance v11, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->WEATHER_CONFIG:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x6

    invoke-virtual {v11, v12, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v12, 0x2b

    invoke-virtual {v8, v12, v11}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getCategoryRestrict()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_7

    const/4 v12, 0x7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getCategoryRestrict()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getSessionCookie()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_8

    const/4 v12, 0x4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getSessionCookie()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_8
    return-object v8

    :cond_9
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const/4 v12, 0x1

    invoke-virtual {v0, v9, v12}, Lcom/google/common/io/protocol/ProtoBuf;->addBool(IZ)V

    goto :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public encodeLogRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 8
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;

    const/4 v3, 0x0

    new-instance v2, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->GENIE_LOG_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v4}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->EVENT_ID:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v4}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->getEventId()[B

    move-result-object v4

    if-nez v4, :cond_0

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->getEventId()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v3, 0x5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->getIdx()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v3, 0x4

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->getClickType()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->getSearchAction()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->LOG:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    const-string v5, "Genie"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception parsing event id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/genie/geniewidget/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_0
.end method

.method public parseGenieResponse(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 10
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {p2, v6}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v2}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p2, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    if-ge v3, v6, :cond_2

    invoke-virtual {p2, v9, v3}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_2
    if-eqz v5, :cond_1

    invoke-virtual {v5, v1, v3}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->setLogInfo([BI)V

    :cond_1
    const/4 v5, 0x0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_0
    const/16 v6, 0xf

    invoke-virtual {v0, v6}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->parseNews(Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x10

    invoke-virtual {v0, v8}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->parseWeather(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;Ljava/lang/String;Lcom/google/common/io/protocol/ProtoBuf;)Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :catch_0
    move-exception v6

    goto :goto_0

    :cond_2
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
