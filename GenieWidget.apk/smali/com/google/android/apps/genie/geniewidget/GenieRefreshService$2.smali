.class Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;
.super Ljava/lang/Thread;
.source "GenieRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->onStart(Landroid/content/Intent;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

.field final synthetic val$storedIntent:Landroid/content/Intent;

.field final synthetic val$storedShouldUpdateScheduledRefresh:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedIntent:Landroid/content/Intent;

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedShouldUpdateScheduledRefresh:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->refreshThread:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$000(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedIntent:Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedShouldUpdateScheduledRefresh:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startRefreshThread(Landroid/content/Intent;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$100(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedIntent:Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedShouldUpdateScheduledRefresh:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startRefreshThread(Landroid/content/Intent;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$100(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedIntent:Landroid/content/Intent;

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$2;->val$storedShouldUpdateScheduledRefresh:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startRefreshThread(Landroid/content/Intent;Z)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$100(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Landroid/content/Intent;Z)V

    throw v0
.end method
