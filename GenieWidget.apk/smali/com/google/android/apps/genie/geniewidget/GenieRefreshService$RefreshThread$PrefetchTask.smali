.class Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;
.super Ljava/lang/Thread;
.source "GenieRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrefetchTask"
.end annotation


# instance fields
.field private final onComplete:Landroid/os/ConditionVariable;

.field private final requestImages:Z

.field private final stories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;Ljava/util/ArrayList;Landroid/os/ConditionVariable;Z)V
    .locals 0
    .param p3    # Landroid/os/ConditionVariable;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;",
            "Landroid/os/ConditionVariable;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->stories:Ljava/util/ArrayList;

    iput-boolean p4, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->requestImages:Z

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->onComplete:Landroid/os/ConditionVariable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->stories:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->access$500(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getLinkUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->requestImages:Z

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->prefetch(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->access$600(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->access$600(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->this$1:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->access$500(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->stories:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cleanup(Ljava/util/ArrayList;)I

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->onComplete:Landroid/os/ConditionVariable;

    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
