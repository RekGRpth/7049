.class Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;
.super Ljava/lang/Thread;
.source "GenieRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;
    }
.end annotation


# instance fields
.field private final forceLocationFix:Z

.field private final ignoreMASFCache:Z

.field private isCancelled:Z

.field private isComplete:Z

.field private isPrefetching:Z

.field private isWaitingForWifi:Z

.field private final prefetchOnly:Z

.field private prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

.field private final req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

.field private final retryNumber:I

.field private final shouldWaitForWifi:Z

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

.field private final updateScheduledRefresh:Z

.field private final waitForPrefetch:Landroid/os/ConditionVariable;

.field private final waitForWifi:Landroid/os/ConditionVariable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZZZZI)V
    .locals 2
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isWaitingForWifi:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isPrefetching:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete:Z

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForWifi:Landroid/os/ConditionVariable;

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForPrefetch:Landroid/os/ConditionVariable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->shouldWaitForWifi:Z

    iput-boolean p4, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->forceLocationFix:Z

    iput-boolean p5, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->ignoreMASFCache:Z

    iput-boolean p6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->updateScheduledRefresh:Z

    iput-boolean p7, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetchOnly:Z

    iput p8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->retryNumber:I

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z

    return v0
.end method

.method private releaseLocks()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$800(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$800(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$800(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$700(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$700(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$700(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    return-void
.end method

.method private waitForLocks()V
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$700(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$800(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForWifi:Landroid/os/ConditionVariable;

    invoke-virtual {v3}, Landroid/os/ConditionVariable;->close()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForWifi:Landroid/os/ConditionVariable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v3, v4, v5}, Landroid/os/ConditionVariable;->block(J)Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    move-object v2, v1

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public cancelPrefetch()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForPrefetch:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->cancel()V

    :cond_0
    return-void
.end method

.method public cancelRequest()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForWifi:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public isComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete:Z

    return v0
.end method

.method public isPrefetching()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isPrefetching:Z

    return v0
.end method

.method public isWaitingForWifi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isWaitingForWifi:Z

    return v0
.end method

.method public run()V
    .locals 9

    const/4 v8, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetchOnly:Z

    if-nez v0, :cond_2

    new-instance v5, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v5, v8}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->shouldWaitForWifi:Z

    if-eqz v0, :cond_0

    iput-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isWaitingForWifi:Z

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForLocks()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isWaitingForWifi:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isCancelled:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->releaseLocks()V

    iput-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->stopSelf()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$200(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->forceLocationFix:Z

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->ignoreMASFCache:Z

    iget-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->updateScheduledRefresh:Z

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->retryNumber:I

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZZLjava/util/concurrent/CountDownLatch;II)V

    :try_start_0
    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$200(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->loadNewsImages()V

    :cond_2
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->isPrefetchPagesEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->prefetcher:Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/utils/PrefetchHelper;->isReady()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->releaseLocks()V

    iput-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isComplete:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$400(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$400(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->stopService(Landroid/content/Intent;)Z

    goto :goto_0

    :cond_4
    iput-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->isPrefetching:Z

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->isPrefetchImagesEnabled()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->access$300(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForPrefetch:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForPrefetch:Landroid/os/ConditionVariable;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;Ljava/util/ArrayList;Landroid/os/ConditionVariable;Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread$PrefetchTask;->start()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->waitForPrefetch:Landroid/os/ConditionVariable;

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v1, v2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->cancelPrefetch()V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public shouldUpdateScheduledRefresh()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService$RefreshThread;->updateScheduledRefresh:Z

    return v0
.end method
