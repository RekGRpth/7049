.class Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;
.super Ljava/lang/Object;
.source "LocationPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationFailed(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

.field final synthetic val$location:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->val$location:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$100(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v4, 0x7f0b002e

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->val$location:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->val$location:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/preference/EditTextPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->val$location:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->resetScheduleFalloff()V

    invoke-interface {v0, v7}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    return-void
.end method
