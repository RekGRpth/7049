.class Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;
.super Ljava/lang/Object;
.source "LocationPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomLocationChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 11
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v3, 0x7f0b002f

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;
    invoke-static {v0, v10}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$302(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v5, 0x7f0b001a

    invoke-virtual {v4, v5}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v6, 0x7f0b002a

    invoke-virtual {v5, v6}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v3

    # setter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;
    invoke-static {v0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$102(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$300(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Ljava/lang/String;

    move-result-object v8

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v9, v1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZZILjava/lang/String;I)V

    goto :goto_0
.end method
