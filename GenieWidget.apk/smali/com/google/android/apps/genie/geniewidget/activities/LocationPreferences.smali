.class public Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
.super Landroid/preference/PreferenceActivity;
.source "LocationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;
    }
.end annotation


# instance fields
.field private ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field private customLocation:Landroid/preference/EditTextPreference;

.field private final customLocationListener:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;

.field private final postToUI:Landroid/os/Handler;

.field private final progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

.field private requestLocation:Ljava/lang/String;

.field private searching:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationListener:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->postToUI:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationFailed(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationSuccess(Ljava/lang/String;)V

    return-void
.end method

.method private customLocationFailed(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->postToUI:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$2;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private customLocationSuccess(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGeniePrefs()Lcom/google/android/apps/genie/geniewidget/GeniePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences;->getPreferencesSnapshot()Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GeniePreferences$PreferencesSnapshot;->setWeatherHasBeenUpdated(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;

    if-nez v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->postToUI:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected networkFailure()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->postToUI:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$3;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f050000

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    const-string v1, "pref_key_prefer_location"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/EditTextPreference;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationListener:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$CustomLocationChangeListener;

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method protected onResume()V
    .locals 5

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    const v3, 0x7f0b0057

    invoke-virtual {v2, v3}, Landroid/preference/EditTextPreference;->setTitle(I)V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    const v3, 0x7f0b0004

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    invoke-virtual {v2, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    const v2, 0x7f0b0009

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "temperature_unit_celsius"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/utils/LocaleUtils;->prefersCelsius(Ljava/util/Locale;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;

    const v3, 0x7f0b0058

    invoke-virtual {v2, v3}, Landroid/preference/EditTextPreference;->setSummary(I)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->registerProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->progressObserver:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->unregisterProgressObserver(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;)V

    return-void
.end method
