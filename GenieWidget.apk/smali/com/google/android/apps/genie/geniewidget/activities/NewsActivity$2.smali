.class Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;
.super Ljava/lang/Object;
.source "NewsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getNewsItemData()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->ctrl:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getEventId()[B

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getIndex()I

    move-result v4

    const/16 v5, 0xa

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->logClick([BII)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$2;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getNewsItemData()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getLinkUrl()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->launchNewsContentActivity(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$300(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Ljava/lang/String;)V

    return-void
.end method
