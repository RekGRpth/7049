.class Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;
.super Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$AnimationAdapter;
.source "NewsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->refreshData(ZZZZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

.field final synthetic val$forceLocationFix:Z

.field final synthetic val$ignoreMASFCache:Z

.field final synthetic val$requestNews:Z

.field final synthetic val$requestWeather:Z

.field final synthetic val$shouldUpdateScheduledRefresh:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;ZZZZZ)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iput-boolean p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$requestNews:Z

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$requestWeather:Z

    iput-boolean p4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$forceLocationFix:Z

    iput-boolean p5, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$ignoreMASFCache:Z

    iput-boolean p6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$shouldUpdateScheduledRefresh:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$AnimationAdapter;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$1;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressPanel:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$000(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$requestNews:Z

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$requestWeather:Z

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$forceLocationFix:Z

    iget-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$ignoreMASFCache:Z

    iget-boolean v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$5;->val$shouldUpdateScheduledRefresh:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->startRefresh(ZZZZZ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$700(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;ZZZZZ)V

    return-void
.end method
