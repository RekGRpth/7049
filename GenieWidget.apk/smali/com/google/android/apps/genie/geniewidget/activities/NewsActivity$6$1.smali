.class Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;
.super Ljava/lang/Object;
.source "NewsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

.field final synthetic val$percentage:I

.field final synthetic val$task:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;ILcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->val$percentage:I

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->val$task:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1000(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->val$percentage:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->updatingNewsTopics:Z
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$1100(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$7;->$SwitchMap$com$google$android$apps$genie$geniewidget$miniwidget$ProgressObserver$Task:[I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->val$task:Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$900(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v1, v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    const v2, 0x7f0b0038

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->progressMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->access$900(Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;

    iget-object v1, v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    const v2, 0x7f0b0014

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
