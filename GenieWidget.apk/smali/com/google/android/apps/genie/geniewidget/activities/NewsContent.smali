.class public Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;
.super Landroid/app/Activity;
.source "NewsContent.java"


# instance fields
.field private newsTitle:Ljava/lang/String;

.field private newsUrl:Ljava/lang/String;

.field private viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030009

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v4, "url-string"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->newsUrl:Ljava/lang/String;

    const-string v4, "title"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->newsTitle:Ljava/lang/String;

    const-string v4, "guid-string"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    invoke-direct {v4, p0, v5, v5, v5}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/widget/FrameLayout;)V

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    const v4, 0x7f0a0021

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    const v4, 0x7f0a0022

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->viewMediator:Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->newsUrl:Ljava/lang/String;

    invoke-virtual {v4, v3, v1, v2, v5}, Lcom/google/android/apps/genie/geniewidget/view/ViewMediator;->showNewsView(Landroid/widget/FrameLayout;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a0073

    if-ne v1, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->newsUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->newsTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsContent;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
