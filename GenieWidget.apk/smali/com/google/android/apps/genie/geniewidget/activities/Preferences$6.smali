.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

.field final synthetic val$autoRefreshEnabled:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iput-boolean p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;->val$autoRefreshEnabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;->val$autoRefreshEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->showDialog(I)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
