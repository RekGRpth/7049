.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x1

    move-object v2, p2

    check-cast v2, Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const v4, 0x7f0b0005

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->resetScheduleFalloff()V

    invoke-interface {v1, v5}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshStatus()V
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$500(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    return v5

    :cond_0
    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->cancelScheduledModelUpdate()V

    goto :goto_0
.end method
