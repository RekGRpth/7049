.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

.field final synthetic val$refreshInterval:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->val$refreshInterval:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const v4, 0x7f0b0006

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->val$refreshInterval:Landroid/preference/Preference;

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setRefreshIntervalSummary(Landroid/preference/Preference;Ljava/lang/String;)V
    invoke-static {v4, v5, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$600(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/Preference;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->resetScheduleFalloff()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->scheduleModelUpdate(Z)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshStatus()V
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$500(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "10800"

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->showDialog(I)V

    :cond_0
    const/4 v3, 0x1

    return v3
.end method
