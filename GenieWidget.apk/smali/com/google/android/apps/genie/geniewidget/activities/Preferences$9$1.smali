.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->success(Ljava/util/LinkedHashSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

.field final synthetic val$allSections:Ljava/util/LinkedHashSet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;Ljava/util/LinkedHashSet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->val$allSections:Ljava/util/LinkedHashSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getNewsTopicPreference()Ljava/util/List;

    move-result-object v6

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getCustomTopicPreference()Ljava/util/List;

    move-result-object v2

    if-eqz v6, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const-string v12, "pref_news_custom_topics"

    invoke-virtual {v11, v12}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/EditTextPreference;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->customTopicListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$900(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const-string v12, "screen_standard_topics"

    invoke-virtual {v11, v12}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/PreferenceCategory;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->val$allSections:Ljava/util/LinkedHashSet;

    invoke-virtual {v11}, Ljava/util/LinkedHashSet;->size()I

    move-result v11

    if-lez v11, :cond_2

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->val$allSections:Ljava/util/LinkedHashSet;

    invoke-virtual {v11}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    new-instance v9, Landroid/preference/CheckBoxPreference;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$context:Landroid/app/Activity;

    invoke-direct {v9, v11}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v11}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pref_news_category-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v7, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    iget-object v11, v7, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicName:Ljava/lang/String;

    invoke-virtual {v9, v11}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->topicClickListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$1000(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getCurrentNewsTopics()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v4, Landroid/preference/Preference;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$context:Landroid/app/Activity;

    invoke-direct {v4, v11}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v11, 0x7f0b004b

    invoke-virtual {v4, v11}, Landroid/preference/Preference;->setTitle(I)V

    const v11, 0x7f0b004c

    invoke-virtual {v4, v11}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v11, 0x0

    invoke-virtual {v4, v11}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {v8, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const-string v12, "screen_custom_topics"

    invoke-virtual {v11, v12}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v11, v11, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v12, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    iget-object v12, v12, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;->val$context:Landroid/app/Activity;

    invoke-direct {v5, v11, v12, v0, v10}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/content/Context;Landroid/preference/PreferenceCategory;Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->randomGen:Ljava/util/Random;
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$1100()Ljava/util/Random;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Random;->nextInt()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "pref_news_category-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v10, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->setKey(Ljava/lang/String;)V

    iget-object v11, v10, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v5, v11}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2

    :cond_4
    return-void
.end method
