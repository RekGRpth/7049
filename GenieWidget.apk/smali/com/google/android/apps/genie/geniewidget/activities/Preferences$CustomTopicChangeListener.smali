.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomTopicChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x0

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0020

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return v8

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getCustomTopicPreference()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v2, v0}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const-string v6, "screen_custom_topics"

    invoke-virtual {v5, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceCategory;

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v5, v6, v3, v2}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/content/Context;Landroid/preference/PreferenceCategory;Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->randomGen:Ljava/util/Random;
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$1100()Ljava/util/Random;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_news_category-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v5

    invoke-interface {v5, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->setCustomTopicPreference(Ljava/lang/Iterable;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->setNewsTopicPreference(Ljava/lang/Iterable;)V

    goto :goto_0
.end method
