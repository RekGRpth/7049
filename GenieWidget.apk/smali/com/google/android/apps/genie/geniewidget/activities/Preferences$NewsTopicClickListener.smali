.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewsTopicClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;

    const/4 v5, 0x1

    move-object v0, p1

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "pref_news_category-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v1, v3, v5

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v4, v1}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->allNewsSections:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$700(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->setNewsTopicPreference(Ljava/lang/Iterable;)V

    return v5

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v4, v1}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
