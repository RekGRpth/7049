.class public Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
.super Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;
.source "Preferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;,
        Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;,
        Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;
    }
.end annotation


# static fields
.field private static final randomGen:Ljava/util/Random;


# instance fields
.field private allNewsSections:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field

.field private final customTopicListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;

.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private final selectedSections:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field

.field private final topicClickListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->randomGen:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;-><init>()V

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->topicClickListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->customTopicListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->topicClickListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$NewsTopicClickListener;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/util/Random;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->randomGen:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshStatus()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setRefreshIntervalSummary(Landroid/preference/Preference;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->allNewsSections:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Ljava/util/Set;)Ljava/util/Set;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;
    .param p1    # Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->allNewsSections:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->selectedSections:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->customTopicListener:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$CustomTopicChangeListener;

    return-object v0
.end method

.method private setRefreshIntervalSummary(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setUpDownloadingPreferences()V
    .locals 4

    const/high16 v3, 0x7f0b0000

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    return-void
.end method

.method private setUpNewsTopics(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetDataService()Lcom/google/android/apps/genie/geniewidget/services/DataService;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;

    invoke-direct {v3, p0, p1, v2}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$9;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/app/Activity;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->requestTopicList(Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    return-void
.end method

.method private setUpRefreshPreferences()V
    .locals 9

    const v7, 0x7f0b0006

    const-string v6, "screen_refresh"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceScreen;

    const v6, 0x7f0b0005

    invoke-virtual {p0, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0, v7}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-virtual {p0, v7}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setRefreshIntervalSummary(Landroid/preference/Preference;Ljava/lang/String;)V

    :cond_0
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$6;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Z)V

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;

    invoke-direct {v6, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$7;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;

    invoke-direct {v6, p0, v4}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$8;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/Preference;)V

    invoke-virtual {v4, v6}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setUpRefreshStatus()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "screen_refreshStatus"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->formatMillis(J)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0b006a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    const-string v0, "screen_root"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v2, "screen_weather"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    const v2, 0x7f0b004d

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->setOrder(I)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpDownloadingPreferences()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshPreferences()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpRefreshStatus()V

    invoke-direct {p0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpNewsTopics(Landroid/app/Activity;)V

    const-string v0, "screen_legal"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "screen_privacy"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$2;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.apps.genie.geniewidget"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    const-string v1, "screen_about"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    const v2, 0x7f0b000f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/GConfig;->BACKEND:Lcom/google/android/apps/genie/geniewidget/GConfig$Server;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GConfig$Server;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1    # I

    const v6, 0x1080027

    const v5, 0x104000a

    packed-switch p1, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :pswitch_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b006c

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0b006d

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$3;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b0070

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0b0071

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$4;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b006e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0b006f

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$5;

    invoke-direct {v4, p0}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$5;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
