.class public abstract Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;
.super Landroid/app/Activity;
.source "ActionBarActivity.java"


# instance fields
.field final mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->createInstance(Landroid/app/Activity;)Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    return-void
.end method


# virtual methods
.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-super {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method
