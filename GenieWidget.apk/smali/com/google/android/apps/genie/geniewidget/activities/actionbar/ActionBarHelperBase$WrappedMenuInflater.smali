.class Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;
.super Landroid/view/MenuInflater;
.source "ActionBarHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrappedMenuInflater"
.end annotation


# instance fields
.field mInflater:Landroid/view/MenuInflater;

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;Landroid/content/Context;Landroid/view/MenuInflater;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/view/MenuInflater;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;

    invoke-direct {p0, p2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->mInflater:Landroid/view/MenuInflater;

    return-void
.end method

.method private loadActionBarMetadata(I)V
    .locals 9
    .param p1    # I

    const/4 v4, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;

    iget-object v6, v6, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v2

    goto :goto_0

    :pswitch_0
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "item"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "http://schemas.android.com/apk/res/android"

    const-string v7, "id"

    const/4 v8, 0x0

    invoke-interface {v4, v6, v7, v8}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    const-string v6, "http://schemas.android.com/apk/res/android"

    const-string v7, "showAsAction"

    const/4 v8, -0x1

    invoke-interface {v4, v6, v7, v8}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;

    iget-object v6, v6, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v6, Landroid/view/InflateException;

    const-string v7, "Error inflating menu XML"

    invoke-direct {v6, v7, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v6

    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v6

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_4

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_4
    return-void

    :catch_1
    move-exception v0

    :try_start_2
    new-instance v6, Landroid/view/InflateException;

    const-string v7, "Error inflating menu XML"

    invoke-direct {v6, v7, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public inflate(ILandroid/view/Menu;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/Menu;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->loadActionBarMetadata(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;->mInflater:Landroid/view/MenuInflater;

    invoke-virtual {v0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method
