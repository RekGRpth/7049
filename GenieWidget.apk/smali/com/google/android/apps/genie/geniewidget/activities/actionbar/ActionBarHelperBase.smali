.class public Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;
.super Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;
.source "ActionBarHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;
    }
.end annotation


# instance fields
.field protected mActionItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;-><init>(Landroid/app/Activity;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    return-void
.end method

.method private addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->getActionBarCompat()Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/4 v10, 0x0

    const v8, 0x102002c

    if-ne v6, v8, :cond_3

    const v8, 0x7f010002

    :goto_1
    invoke-direct {v1, v9, v10, v8}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v9, Landroid/view/ViewGroup$LayoutParams;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v8, 0x102002c

    if-ne v6, v8, :cond_4

    const v8, 0x7f090010

    :goto_2
    invoke-virtual {v10, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    const/4 v10, -0x1

    invoke-direct {v9, v8, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v8, 0x7f0a0004

    if-ne v6, v8, :cond_2

    const v8, 0x7f0a0003

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setId(I)V

    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v8, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$1;

    invoke-direct {v8, p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;Landroid/view/MenuItem;)V

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v8

    const v9, 0x7f0a0004

    if-ne v8, v9, :cond_0

    new-instance v4, Landroid/widget/ProgressBar;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/4 v9, 0x0

    const v10, 0x7f010003

    invoke-direct {v4, v8, v9, v10}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09000f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09000e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v7, v3, 0x2

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sub-int v8, v3, v7

    div-int/lit8 v8, v8, 0x2

    sub-int v9, v2, v7

    div-int/lit8 v9, v9, 0x2

    sub-int v10, v3, v7

    div-int/lit8 v10, v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const v8, 0x7f0a0002

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setId(I)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    const v8, 0x7f010001

    goto/16 :goto_1

    :cond_4
    const v8, 0x7f09000f

    goto/16 :goto_2
.end method

.method private getActionBarCompat()Landroid/view/ViewGroup;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private setupActionBar()V
    .locals 9

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->getActionBarCompat()Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v2, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x3f800000

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-direct {v3, v5}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;

    const v5, 0x102002c

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const v7, 0x7f0b000d

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v3, v5, v8, v6}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;IILjava/lang/CharSequence;)V

    const v5, 0x7f020018

    invoke-virtual {v1, v5}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;

    new-instance v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/4 v6, 0x0

    const/high16 v7, 0x7f010000

    invoke-direct {v4, v5, v6, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 2
    .param p1    # Landroid/view/MenuInflater;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase$WrappedMenuInflater;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;Landroid/content/Context;Landroid/view/MenuInflater;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    instance-of v0, v0, Landroid/preference/PreferenceActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x7

    const/high16 v5, 0x7f030000

    invoke-virtual {v3, v4, v5}, Landroid/view/Window;->setFeatureInt(II)V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->setupActionBar()V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/SimpleMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;->addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
