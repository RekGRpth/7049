.class public abstract Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "ActionBarPreferenceActivity.java"


# instance fields
.field final mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->createInstance(Landroid/app/Activity;)Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    return-void
.end method


# virtual methods
.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    const/4 v2, 0x7

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->requestWindowFeature(I)Z

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->onBackPressed()V

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarPreferenceActivity;->mActionBarHelper:Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method
