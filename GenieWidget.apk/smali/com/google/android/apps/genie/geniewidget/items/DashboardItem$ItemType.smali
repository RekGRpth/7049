.class public final enum Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;
.super Ljava/lang/Enum;
.source "DashboardItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

.field public static final enum NEWS:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

.field public static final enum WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    const-string v1, "NEWS"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    const-string v1, "WEATHER_FORECAST"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    return-object v0
.end method
