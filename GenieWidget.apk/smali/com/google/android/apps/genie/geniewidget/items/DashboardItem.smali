.class public abstract Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
.super Ljava/lang/Object;
.source "DashboardItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;
    }
.end annotation


# instance fields
.field private eventId:[B

.field private index:I

.field private final type:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->type:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    return-void
.end method


# virtual methods
.method public getEventId()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->eventId:[B

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->index:I

    return v0
.end method

.method public getType()Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->type:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    return-object v0
.end method

.method public setLogInfo([BI)V
    .locals 0
    .param p1    # [B
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->eventId:[B

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->index:I

    return-void
.end method
