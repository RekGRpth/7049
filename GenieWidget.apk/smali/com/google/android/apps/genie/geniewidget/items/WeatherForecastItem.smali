.class public Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
.super Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;
.source "WeatherForecastItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    }
.end annotation


# instance fields
.field private current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

.field private forecasts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;",
            ">;"
        }
    .end annotation
.end field

.field private hourlyData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;",
            ">;"
        }
    .end annotation
.end field

.field private final location:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;-><init>(Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->forecasts:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->hourlyData:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->location:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addForecast(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addHourlyDatapoint(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->hourlyData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    .locals 5
    .param p1    # J

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->hourlyData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gtz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getEndTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunrise()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setSunrise(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunset()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setSunset(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHighTemp()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setHighTemp(I)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getLowTemp()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setLowTemp(I)V

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->hourlyData:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    if-eqz v1, :cond_5

    # getter for: Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->access$000(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    # getter for: Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->access$000(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setDescription(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->setImageUrl(Ljava/lang/String;)V

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gtz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getEndTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-ltz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gtz v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getEndTimestamp()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-ltz v3, :cond_7

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    goto :goto_0
.end method

.method public getForecasts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->forecasts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHourlyData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->hourlyData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->location:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrentCondition(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WEATHER: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->current:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
