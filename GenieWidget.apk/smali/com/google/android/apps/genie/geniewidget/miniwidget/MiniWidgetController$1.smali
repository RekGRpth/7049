.class Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;
.super Ljava/lang/Object;
.source "MiniWidgetController.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/utils/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->acquireLocationAndRefresh(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

.field final synthetic val$ignoreMASFCache:Z

.field final synthetic val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$ignoreMASFCache:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$ignoreMASFCache:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V

    return-void
.end method

.method public success(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->val$ignoreMASFCache:Z

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->internalRefreshData(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$000(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Z)V

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$1;->success(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    return-void
.end method
