.class Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;
.super Ljava/lang/Object;
.source "MiniWidgetController.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
        "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V

    const-string v0, "Genie"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request failed at, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$300(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$200(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->formatMillis(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " next auto-refresh time ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getRefreshIntervalMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyFailure(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$400(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->setRequestInFlight(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$500(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Z)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$600(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->maybeRetry()V
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$700(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V

    return-void
.end method

.method public progress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyProgress(I)V
    invoke-static {v0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$800(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;I)V

    return-void
.end method

.method public success(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;)V
    .locals 16
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->getPayload()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->getPayload()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/GenieException;

    const-string v6, "Server returned no data."

    invoke-direct {v3, v6}, Lcom/google/android/apps/genie/geniewidget/GenieException;-><init>(Ljava/lang/String;)V

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifyFailure(Ljava/lang/Exception;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$400(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Ljava/lang/Exception;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->setRequestInFlight(Z)V
    invoke-static {v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$500(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$600(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->getPayload()Ljava/util/List;

    move-result-object v5

    const/4 v9, 0x0

    const/4 v8, 0x0

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;

    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->getType()Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    if-ne v2, v3, :cond_6

    const/4 v9, 0x1

    if-eqz v8, :cond_2

    :cond_3
    :goto_1
    if-nez v9, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v2

    if-nez v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    :cond_4
    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$200(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->READY:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$100(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getTypeFilter()Ljava/util/ArrayList;

    move-result-object v3

    const/16 v6, 0xf

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->getTimeStampMillis()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setData(ZZLjava/util/List;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->notifySuccess()V
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$900(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$300(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v2, v0, v1, v14, v15}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->updateContentProvider(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->setRequestInFlight(Z)V
    invoke-static {v2, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$500(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->requestLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$600(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getTypeFilter()Ljava/util/ArrayList;

    move-result-object v2

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez v8, :cond_8

    const/4 v13, 0x1

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getTypeFilter()Ljava/util/ArrayList;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-nez v9, :cond_9

    const/4 v12, 0x1

    :goto_4
    if-nez v13, :cond_5

    if-eqz v12, :cond_a

    :cond_5
    const-string v2, "Genie"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Payload incomplete.  missingWeather="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " missingNews="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # invokes: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->maybeRetry()V
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$700(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v11}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->getType()Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    if-ne v2, v3, :cond_2

    const/4 v8, 0x1

    if-eqz v9, :cond_2

    goto/16 :goto_1

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v13, 0x0

    goto :goto_3

    :cond_9
    const/4 v12, 0x0

    goto :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->this$0:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    # getter for: Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->access$300(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;)Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->recordRefreshTime()V

    goto/16 :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    check-cast p2, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$2;->success(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;)V

    return-void
.end method
