.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;
.super Ljava/lang/Object;
.source "MiniWidgetManager.java"


# static fields
.field private static final log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;


# instance fields
.field private final genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

.field private final model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private final running:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->running:Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-void
.end method


# virtual methods
.method public getModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-object v0
.end method

.method public onRedrawComplete(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getState()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->refreshDataModel()V

    goto :goto_0
.end method

.method public requestRedraw()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const-string v1, "com.google.android.apps.genie.MINIWIDGET_UPDATE"

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->redrawWidget(Ljava/lang/String;)V

    return-void
.end method

.method public requestReload()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->reset()V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->requestRedraw()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->refreshDataModel()V

    return-void
.end method
