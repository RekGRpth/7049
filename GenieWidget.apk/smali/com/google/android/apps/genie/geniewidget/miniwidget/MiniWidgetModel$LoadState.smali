.class public final enum Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;
.super Ljava/lang/Enum;
.source "MiniWidgetModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field public static final enum ACQUIRING_LOCATION:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field public static final enum FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field public static final enum LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field public static final enum NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field public static final enum READY:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const-string v1, "ACQUIRING_LOCATION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->ACQUIRING_LOCATION:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const-string v1, "READY"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->READY:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->ACQUIRING_LOCATION:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->LOADING:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->READY:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    return-object v0
.end method
