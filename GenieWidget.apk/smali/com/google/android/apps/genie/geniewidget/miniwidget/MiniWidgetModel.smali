.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
.super Ljava/lang/Object;
.source "MiniWidgetModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$1;,
        Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;
    }
.end annotation


# instance fields
.field private categorizedNews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private currentAppLocale:Ljava/util/Locale;

.field private currentDataLocale:Ljava/util/Locale;

.field private currentNewsStory:I

.field private currentTopicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field

.field private final genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

.field private lastUpdateTime:J

.field private news:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;"
        }
    .end annotation
.end field

.field private state:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

.field private weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->lastUpdateTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentNewsStory:I

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->state:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentDataLocale:Ljava/util/Locale;

    return-void
.end method

.method private categorizeNews(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTopicId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getCategoryTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTopicId()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTopicId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private resetWeather()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    return-void
.end method


# virtual methods
.method public getCurrentNewsTopics()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-interface {v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getCustomTopicPreference()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    return-object v3
.end method

.method public getLastUpdateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->lastUpdateTime:J

    return-wide v0
.end method

.method public getNewsStories()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNewsStories(I)Ljava/util/ArrayList;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;"
        }
    .end annotation

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt p1, v8, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    :cond_0
    return-object v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getCurrentNewsTopics()Ljava/util/List;

    move-result-object v7

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, p1}, Ljava/util/ArrayList;-><init>(I)V

    :cond_4
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v8, p1, :cond_0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v8, p1, :cond_5

    goto :goto_1
.end method

.method public declared-synchronized getNewsStories(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/NewsItem;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v1, p1}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getRefreshIntervalMillis()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v1, 0x7f0b0006

    const-string v2, "10800"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getStringPreference(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getState()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->state:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    return-object v0
.end method

.method public getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    return-object v0
.end method

.method public hasAppLocaleChanged()Z
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized hasData()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->lastUpdateTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hasDataLocaleChanged()Z
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentDataLocale:Ljava/util/Locale;

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized reset()V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->NEW:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetNews()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetWeather()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->lastUpdateTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetAppLocale()V
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentDataLocale:Ljava/util/Locale;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentAppLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentDataLocale:Ljava/util/Locale;

    :cond_0
    return-void
.end method

.method public resetDataLocale()V
    .locals 1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentDataLocale:Ljava/util/Locale;

    return-void
.end method

.method public declared-synchronized resetNews()V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizedNews:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentTopicList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentNewsStory:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setData(ZZLjava/util/List;J)V
    .locals 8
    .param p1    # Z
    .param p2    # Z
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;",
            ">;J)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput-wide p4, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->lastUpdateTime:J

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetWeather()V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->resetNews()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    if-nez p3, :cond_3

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    const/4 v5, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$items$DashboardItem$ItemType:[I

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->getType()Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-object v3, v0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->categorizeNews(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :pswitch_1
    check-cast v2, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v5, 0x1

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setState(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->state:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    return-void
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->state:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    const-string v1, "[no news stories]"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    if-nez v1, :cond_1

    const-string v1, " [no weather]"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showing news "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->currentNewsStory:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->news:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " weather="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->weather:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
