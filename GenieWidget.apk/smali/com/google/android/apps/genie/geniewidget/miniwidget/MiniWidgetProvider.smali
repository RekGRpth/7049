.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;
.super Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;
.source "MiniWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;-><init>()V

    return-void
.end method

.method public static buildStoryIntent(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getEventId()[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getIndex()I

    move-result v2

    xor-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "com.google.android.apps.genie.news#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.genie.EVENT_ID"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getEventId()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.genie.INDEX"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getIndex()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.google.android.apps.genie.news#no-news"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private renderWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[ILcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;[Landroid/widget/RemoteViews;Z)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # [I
    .param p4    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;
    .param p5    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;
    .param p6    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p7    # Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;
    .param p8    # [Landroid/widget/RemoteViews;
    .param p9    # Z

    const/4 v11, 0x0

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasDataLocaleChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetManager()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->requestReload()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    const/4 v8, 0x0

    :goto_1
    move-object/from16 v0, p3

    array-length v1, v0

    if-ge v8, v1, :cond_7

    aget v5, p3, v8

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->showNews(I)Z

    move-result v6

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->showWeather(I)Z

    move-result v7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore_unconfigured_widget_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v10, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_2

    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    if-nez v6, :cond_3

    if-nez v7, :cond_3

    const/4 v6, 0x1

    const/4 v7, 0x1

    :cond_3
    const/16 p9, 0x1

    if-eqz v6, :cond_4

    const/4 v11, 0x1

    :cond_4
    if-eqz v7, :cond_5

    if-nez v6, :cond_5

    const v9, 0x7f030008

    :goto_3
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    aput-object v1, p8, v8

    aget-object v3, p8, v8

    move-object/from16 v1, p4

    move-object v2, p1

    move-object/from16 v4, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->render(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;IZZ)V

    aget-object v1, p8, v8

    move-object/from16 v0, p6

    invoke-direct {p0, v1, p1, v9, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;->wireupClickIntents(Landroid/widget/RemoteViews;Landroid/content/Context;ILcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V

    aget-object v1, p8, v8

    invoke-virtual {p2, v5, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_2

    :cond_5
    if-eqz v6, :cond_6

    if-nez v7, :cond_6

    const v9, 0x7f030005

    goto :goto_3

    :cond_6
    const v9, 0x7f030004

    goto :goto_3

    :cond_7
    if-eqz p9, :cond_0

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->onRedrawComplete(Z)V

    goto/16 :goto_0
.end method

.method private wireupClickIntents(Landroid/widget/RemoteViews;Landroid/content/Context;ILcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/genie/geniewidget/activities/NewsActivity;

    invoke-direct {v1, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getEventId()[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getIndex()I

    move-result v3

    xor-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.google.android.apps.genie.weather#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v2, "com.google.android.apps.genie.EVENT_ID"

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getEventId()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v2, "com.google.android.apps.genie.INDEX"

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getIndex()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    invoke-static {p2, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f030004

    if-ne p3, v1, :cond_2

    const v1, 0x7f0a0011

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v1, 0x7f0a000b

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v1, 0x7f0a000e

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_0
    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.genie.MINIWIDGET_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v1, 0x7f0a0070

    const/4 v2, 0x1

    invoke-static {p2, v2, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void

    :cond_1
    const-string v0, "com.google.android.apps.genie.weather#no-weather"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    const v1, 0x7f030008

    if-ne p3, v1, :cond_0

    const v1, 0x7f0a001a

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method


# virtual methods
.method public getWidgetRetryAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.apps.genie.MINIWIDGET_RETRY"

    return-object v0
.end method

.method public getWidgetUpdateAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.apps.genie.MINIWIDGET_UPDATE"

    return-object v0
.end method

.method public onManualReload(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetManager()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->requestReload()V

    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/BaseWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/content/Intent;)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v0, v3

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetView()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetManager()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->getModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->removeStaleNews()V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getWidgetConfig()Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    move-result-object v7

    array-length v0, v3

    new-array v8, v0, [Landroid/widget/RemoteViews;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;->renderWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[ILcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;[Landroid/widget/RemoteViews;Z)V

    goto :goto_0
.end method
