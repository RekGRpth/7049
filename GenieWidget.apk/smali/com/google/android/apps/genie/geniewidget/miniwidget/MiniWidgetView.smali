.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;
.super Ljava/lang/Object;
.source "MiniWidgetView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView$1;
    }
.end annotation


# instance fields
.field private defaultLocale:Ljava/util/Locale;

.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieContext;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    return-void
.end method

.method private buildStory(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Z)Landroid/widget/RemoteViews;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .param p3    # Z

    const/4 v8, 0x0

    const v7, 0x7f0a0017

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030006

    invoke-direct {v1, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    const v5, 0x7f0a0015

    invoke-direct {p0, v1, v5, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->setIfNotNull(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const v5, 0x7f0a0016

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getSource()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v5, v6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->setIfNotNull(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    invoke-static {p1, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;->buildStoryIntent(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)Landroid/content/Intent;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static {p1, v5, v3, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    const v5, 0x7f0a0014

    invoke-virtual {v1, v5, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getThumbnailUri()Landroid/net/Uri;

    move-result-object v2

    if-nez p3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1, v7, v2}, Landroid/widget/RemoteViews;->setImageViewUri(ILandroid/net/Uri;)V

    invoke-virtual {v1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_0
    return-object v1

    :cond_0
    const/16 v5, 0x8

    invoke-virtual {v1, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private buildStoryEmpty(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 5
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030007

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v3, 0x0

    invoke-static {p1, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetProvider;->buildStoryIntent(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)Landroid/content/Intent;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p1, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v3, 0x7f0a0018

    invoke-virtual {v0, v3, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-object v0
.end method

.method private getLocale()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->defaultLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->defaultLocale:Ljava/util/Locale;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method private isVisible(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    if-ne p2, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private localizeTemp(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v2, 0x7f0b0009

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/utils/LocaleUtils;->prefersCelsius(Ljava/util/Locale;)Z

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getBooleanPreference(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&deg;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/utils/TemperatureConverter;->celsiusToFahrenheit(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&deg;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private renderConditionWeatherIcon(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;Z)V
    .locals 5
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    .param p3    # Z

    const v4, 0x7f0a000f

    if-eqz p3, :cond_0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->L:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->M:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    goto :goto_0

    :cond_1
    const/4 v2, 0x4

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private renderNews(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RemoteViews;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p4    # Z

    const v7, 0x7f0a0012

    invoke-virtual {p2, v7}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    const/4 v1, 0x0

    invoke-virtual {p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v3

    const/16 v6, 0x14

    invoke-virtual {p3, v6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories(I)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    if-eqz v4, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v4, p4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->buildStory(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;Z)Landroid/widget/RemoteViews;

    move-result-object v5

    invoke-virtual {p2, v7, v5}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->buildStoryEmpty(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    :cond_2
    return-void
.end method

.method private renderViewState(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V
    .locals 4
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    const v1, 0x7f0a000d

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getState()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$miniwidget$MiniWidgetModel$LoadState:[I

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->showSection(Landroid/widget/RemoteViews;I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->showSection(Landroid/widget/RemoteViews;I)V

    const v2, 0x7f0a006e

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->ACQUIRING_LOCATION:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v3, 0x7f0b0013

    invoke-interface {v1, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {p1, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const v1, 0x7f0a000b

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v3, 0x7f0b0014

    invoke-interface {v1, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_1
    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_3
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->showSection(Landroid/widget/RemoteViews;I)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0a000c

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private renderWeather(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;Z)V
    .locals 2
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    .param p3    # Z

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasTemperature()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0010

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderConditionWeatherIcon(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;Z)V

    return-void
.end method

.method private renderWeather(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Z)V
    .locals 9
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p3    # Z

    const v8, 0x7f0a0011

    const v7, 0x7f0a0010

    const v6, 0x7f0a000f

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p1, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p1, v8, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderWeather(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;Z)V

    :goto_1
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p1, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private renderWeatherExtra(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V
    .locals 8
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    const v7, 0x7f0a0020

    const v6, 0x7f0a001f

    const v5, 0x7f0a001b

    invoke-virtual {p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    const/4 v3, 0x0

    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f0a001d

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f0a001e

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->setIfNotNull(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const-string v3, "--&deg;"

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHighTemp()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {p1, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getLowTemp()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v7, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    const/16 v3, 0x8

    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2
.end method

.method private setIfNotNull(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 1
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p1, p2, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private showSection(Landroid/widget/RemoteViews;I)V
    .locals 4
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # I

    const v3, 0x7f0a000d

    const v2, 0x7f0a000c

    const v1, 0x7f0a000b

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->isVisible(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, v2, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->isVisible(II)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, v3, p2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->isVisible(II)I

    move-result v0

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method


# virtual methods
.method public render(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;IZZ)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RemoteViews;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p4    # I
    .param p5    # Z
    .param p6    # Z

    const v2, 0x7f0a006f

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderViewState(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V

    invoke-virtual {p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p5, :cond_0

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderNews(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Z)V

    :cond_0
    if-eqz p6, :cond_2

    const v1, 0x7f0a000e

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-nez p5, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderWeather(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Z)V

    if-nez p5, :cond_2

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->renderWeatherExtra(Landroid/widget/RemoteViews;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getState()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->FAILED:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v1, 0x7f0b0015

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p3}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getState()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;->READY:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel$LoadState;

    if-ne v0, v1, :cond_2

    const v0, 0x7f0a000c

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->showSection(Landroid/widget/RemoteViews;I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieContext;

    const v1, 0x7f0b001b

    invoke-interface {v0, v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method
