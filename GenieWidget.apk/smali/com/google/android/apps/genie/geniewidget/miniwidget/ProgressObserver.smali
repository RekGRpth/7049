.class public interface abstract Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;
.super Ljava/lang/Object;
.source "ProgressObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;
    }
.end annotation


# virtual methods
.method public abstract failure(Ljava/lang/Exception;)V
.end method

.method public abstract invalidate()V
.end method

.method public abstract progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
.end method

.method public abstract success()V
.end method
