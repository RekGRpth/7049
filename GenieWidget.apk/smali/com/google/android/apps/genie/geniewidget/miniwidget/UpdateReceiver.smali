.class public Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UpdateReceiver.java"


# instance fields
.field private final scheduled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;->scheduled:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;->scheduled:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v1, 0x1

    const-string v0, "Genie"

    const-string v2, "waking up to refresh News & Weather"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;->scheduled:Z

    iget-boolean v6, p0, Lcom/google/android/apps/genie/geniewidget/miniwidget/UpdateReceiver;->scheduled:Z

    move-object v0, p1

    move v2, v1

    move v3, v1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/GenieRefreshService;->startService(Landroid/content/Context;ZZZZZZ)V

    return-void
.end method
