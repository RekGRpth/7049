.class public Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
.super Ljava/lang/Object;
.source "EditionNewsTopics.java"


# instance fields
.field private editionLocale:Ljava/util/Locale;

.field private topics:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->topics:Ljava/util/LinkedHashSet;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->editionLocale:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public addTopic(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->topics:Ljava/util/LinkedHashSet;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearTopics()V
    .locals 1

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->topics:Ljava/util/LinkedHashSet;

    return-void
.end method

.method public getAllTopics()Ljava/util/LinkedHashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->topics:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method public getEditionLocale()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->editionLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public isPopulated()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->topics:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEditionLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1    # Ljava/util/Locale;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->editionLocale:Ljava/util/Locale;

    return-void
.end method
