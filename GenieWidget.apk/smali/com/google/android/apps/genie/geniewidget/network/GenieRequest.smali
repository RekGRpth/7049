.class public Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
.super Ljava/lang/Object;
.source "GenieRequest.java"


# instance fields
.field private categoryRestrict:Ljava/lang/String;

.field private customTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field

.field private fakeLocation:Ljava/lang/String;

.field private location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

.field private newsTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation
.end field

.field private sessionCookie:Ljava/lang/String;

.field private typeFilter:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->categoryRestrict:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->categoryRestrict:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->sessionCookie:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->sessionCookie:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addTypeFilter(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCategoryRestrict()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->categoryRestrict:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomTopics()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    return-object v0
.end method

.method public getFakeLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-object v0
.end method

.method public getNewsTopics()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    return-object v0
.end method

.method public getSessionCookie()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->sessionCookie:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeFilter()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    return-object v0
.end method

.method public removeTypeFilter(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setCustomTopics(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    return-void
.end method

.method public setFakeLocation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    return-void
.end method

.method public setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    return-void
.end method

.method public setNewsTopics(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GenieRequest "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLat()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->location:Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->getLng()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fakeLocation: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->fakeLocation:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->categoryRestrict:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " catId:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->categoryRestrict:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->sessionCookie:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, " (authenticated)"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->typeFilter:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "news topics: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->newsTopics:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "custom topics: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->customTopics:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[no location]"

    goto/16 :goto_0

    :cond_1
    const-string v0, "[no fake location]"

    goto/16 :goto_1

    :cond_2
    const-string v0, "[no category restrict]"

    goto/16 :goto_2

    :cond_3
    const-string v0, "[no session cookie]"

    goto/16 :goto_3

    :cond_4
    const-string v0, "[no type filter]"

    goto :goto_4

    :cond_5
    const-string v0, "[no news topics]"

    goto :goto_5

    :cond_6
    const-string v0, "[no custom]"

    goto :goto_6
.end method
