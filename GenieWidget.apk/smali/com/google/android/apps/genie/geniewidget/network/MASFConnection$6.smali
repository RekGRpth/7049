.class Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;
.super Ljava/lang/Object;
.source "MASFConnection.java"

# interfaces
.implements Lcom/google/android/news/masf/protocol/Request$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->getUrl(Ljava/lang/String;JZLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

.field final synthetic val$url:Ljava/lang/String;

.field final synthetic val$useCache:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$url:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$useCache:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestCompleted(Lcom/google/android/news/masf/protocol/Request;Lcom/google/android/news/masf/protocol/Response;)V
    .locals 11
    .param p1    # Lcom/google/android/news/masf/protocol/Request;
    .param p2    # Lcom/google/android/news/masf/protocol/Response;

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getStatusCode()I

    move-result v7

    const/16 v8, 0xc8

    if-ne v7, v8, :cond_4

    const/4 v5, 0x0

    :try_start_0
    new-instance v4, Lcom/google/android/news/masf/protocol/HttpResponse;

    invoke-direct {v4, p2}, Lcom/google/android/news/masf/protocol/HttpResponse;-><init>(Lcom/google/android/news/masf/protocol/Response;)V

    invoke-virtual {v4}, Lcom/google/android/news/masf/protocol/HttpResponse;->getDataInputStream()Ljava/io/DataInputStream;

    move-result-object v5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v7, 0x1000

    new-array v1, v7, [B

    const/4 v6, -0x1

    :goto_0
    invoke-virtual {v5, v1}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v3

    :try_start_1
    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v7, v3, v8}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_0

    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$url:Ljava/lang/String;

    invoke-interface {v7, v8, v2}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->success(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-boolean v7, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$useCache:Z

    if-eqz v7, :cond_2

    # getter for: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$500()Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$url:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->put([B[B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    if-eqz v5, :cond_0

    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v7

    goto :goto_1

    :catchall_0
    move-exception v7

    if-eqz v5, :cond_3

    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_2
    throw v7

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Masf error: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getStatusCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    goto :goto_1

    :catch_2
    move-exception v7

    goto :goto_1

    :catch_3
    move-exception v8

    goto :goto_2
.end method

.method public requestFailed(Lcom/google/android/news/masf/protocol/Request;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/news/masf/protocol/Request;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    return-void
.end method
