.class public Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;
.super Ljava/lang/Object;
.source "MASFConnection.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;


# static fields
.field private static final GENIE_LOG_URI:Ljava/lang/String;

.field private static final GENIE_URI_FEED:Ljava/lang/String;

.field private static final GENIE_URI_RAW:Ljava/lang/String;

.field private static genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

.field private static iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;


# instance fields
.field private clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

.field private final masfService:Lcom/google/android/news/masf/MobileServiceMux;

.field private final postToUI:Landroid/os/Handler;

.field private final protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->GENIE_URI_RAW:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->GENIE_URI_FEED:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_SERVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/l"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->GENIE_LOG_URI:Ljava/lang/String;

    sput-object v2, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    sput-object v2, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/Clock$SystemClock;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/utils/Clock$SystemClock;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->postToUI:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/news/common/android/AndroidConfig;

    invoke-direct {v0, p1}, Lcom/google/android/news/common/android/AndroidConfig;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/news/common/Config;->setConfig(Lcom/google/android/news/common/Config;)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/GConfig;->MASF_URL:Ljava/lang/String;

    const-string v1, "newswidget"

    const-string v2, "0.0.0"

    const-string v3, "android"

    const-string v4, "web"

    const/16 v5, 0x7530

    invoke-static/range {v0 .. v5}, Lcom/google/android/news/masf/MobileServiceMux;->initialize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/news/masf/MobileServiceMux;->getSingleton()Lcom/google/android/news/masf/MobileServiceMux;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->masfService:Lcom/google/android/news/masf/MobileServiceMux;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->masfService:Lcom/google/android/news/masf/MobileServiceMux;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/news/masf/MobileServiceMux;->setLoggingEnabled(Z)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    const-class v1, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    const/4 v2, 0x3

    const-string v3, "genie_masfcache"

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-direct {v0, v2, v3, p1, v4}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;-><init>(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/utils/Clock;)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    :cond_0
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    const/16 v2, 0x14

    const-string v3, "genie_iconcache"

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-direct {v0, v2, v3, p1, v4}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;-><init>(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/utils/Clock;)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # [B
    .param p3    # J
    .param p5    # Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->tryGenieCache(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->parseGenieResponse(Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;)Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;)Lcom/google/android/apps/genie/geniewidget/utils/Clock;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    return-object v0
.end method

.method static synthetic access$500()Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    return-object v0
.end method

.method private makeNetworkRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/news/masf/protocol/PlainRequest;[B)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p3    # Lcom/google/android/news/masf/protocol/PlainRequest;
    .param p4    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;",
            "Lcom/google/android/news/masf/protocol/PlainRequest;",
            "[B)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[B)V

    invoke-virtual {p3, v0}, Lcom/google/android/news/masf/protocol/PlainRequest;->setListener(Lcom/google/android/news/masf/protocol/Request$Listener;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->masfService:Lcom/google/android/news/masf/MobileServiceMux;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Lcom/google/android/news/masf/MobileServiceMux;->submitRequest(Lcom/google/android/news/masf/protocol/Request;Z)V

    return-void
.end method

.method private makeRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/String;)V
    .locals 20
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # J
    .param p4    # J
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "JJ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v14, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;-><init>(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)V

    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->setLocation(Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;)V

    new-instance v16, Lcom/google/android/news/masf/protocol/PlainRequest;

    move-object/from16 v0, v16

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/google/android/news/masf/protocol/PlainRequest;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->progress(I)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->createGenieRequestProto(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    invoke-virtual {v4, v14}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->createGenieRequestProto(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v6

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/news/masf/protocol/PlainRequest;->setPayload([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v7, p2

    move-object/from16 v9, p6

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->tryGenieCache(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    move-result-object v17

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, p6

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->successInUiThread(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    :goto_0
    return-void

    :catch_0
    move-exception v15

    const-string v4, "Genie"

    const-string v5, "Couldn\'t build request."

    invoke-static {v4, v5, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v6

    move-wide/from16 v11, p4

    move-object/from16 v13, p6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$1;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v7, v2, v6}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->makeNetworkRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/news/masf/protocol/PlainRequest;[B)V

    goto :goto_0
.end method

.method private parseGenieResponse(Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "I",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)",
            "Lcom/google/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/io/InputStream;)V

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/apps/genie/proto/GenieServiceMessageTypes;->GENIE_RESPONSE:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private tryGenieCache(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[BJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .locals 10
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # [B
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "[BJ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;"
        }
    .end annotation

    const/4 v6, 0x0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    invoke-virtual {v5, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->get([BJ)Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v5, v1, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-direct {v0, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v5, v1, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    array-length v5, v5

    invoke-direct {p0, v0, v5, p5}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->parseGenieResponse(Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    invoke-virtual {v5, p1, v3}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->parseGenieResponse(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    const/4 v7, 0x1

    iget-wide v8, v1, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->timeStamp:J

    invoke-direct {v5, v4, v7, v8, v9}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;-><init>(Ljava/util/List;ZJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v5

    :catch_0
    move-exception v2

    const-string v5, "Genie"

    const-string v7, "IOException accessing cache."

    invoke-static {v5, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v5, v6

    goto :goto_0

    :cond_0
    move-object v5, v6

    goto :goto_0
.end method


# virtual methods
.method protected failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<TT;TS;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->postToUI:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$3;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public getRawFeed(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # J
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "JJ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)V"
        }
    .end annotation

    sget-object v7, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->GENIE_URI_RAW:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->makeRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/String;)V

    return-void
.end method

.method public getUrl(Ljava/lang/String;JZLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JZ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Null url"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, p5}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->failureInUiThread(Ljava/lang/Exception;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p4, :cond_1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->iconCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->get([BJ)Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-interface {p5, p1, v2}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->success(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/news/masf/protocol/HttpRequest;

    invoke-direct {v1, p1}, Lcom/google/android/news/masf/protocol/HttpRequest;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;

    invoke-direct {v2, p0, p5, p1, p4}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$6;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/news/masf/protocol/Request;->setListener(Lcom/google/android/news/masf/protocol/Request$Listener;)V

    invoke-static {}, Lcom/google/android/news/masf/MobileServiceMux;->getSingleton()Lcom/google/android/news/masf/MobileServiceMux;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/news/masf/MobileServiceMux;->submitRequest(Lcom/google/android/news/masf/protocol/Request;)V

    goto :goto_0
.end method

.method public logClick(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->encodeLogRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/news/masf/protocol/PlainRequest;

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->GENIE_LOG_URI:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/google/android/news/masf/protocol/PlainRequest;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/news/masf/protocol/PlainRequest;->setPayload([B)V

    invoke-static {}, Lcom/google/android/news/masf/MobileServiceMux;->getSingleton()Lcom/google/android/news/masf/MobileServiceMux;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/news/masf/MobileServiceMux;->submitRequest(Lcom/google/android/news/masf/protocol/Request;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Genie"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException when encoding click entry: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected successInUiThread(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->postToUI:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$4;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$4;-><init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
