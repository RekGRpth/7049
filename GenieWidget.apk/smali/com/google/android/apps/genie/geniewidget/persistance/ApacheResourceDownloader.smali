.class public Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;
.super Ljava/lang/Object;
.source "ApacheResourceDownloader.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;


# static fields
.field private static mimeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/andrew-inset"

    const-string v2, "ez"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/annodex"

    const-string v2, "anx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/atom+xml"

    const-string v2, "atom"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/bluefish-project"

    const-string v2, "bfproject"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/dicom"

    const-string v2, "dcm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/docbook+xml"

    const-string v2, "docbook"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/ecmascript"

    const-string v2, "es"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/epub+zip"

    const-string v2, "epub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/gnunet-directory"

    const-string v2, "gnd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/illustrator"

    const-string v2, "ai"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/java-archive"

    const-string v2, "jar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/javascript"

    const-string v2, "js"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/mathematica"

    const-string v2, "nb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/mbox"

    const-string v2, "mbox"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/metalink+xml"

    const-string v2, "metalink"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/msword-template"

    const-string v2, "dot"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/msword"

    const-string v2, "doc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/octet-stream"

    const-string v2, "bin"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/oda"

    const-string v2, "oda"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/ogg"

    const-string v2, "ogx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pdf"

    const-string v2, "pdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pgp-encrypted"

    const-string v2, "pgp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pgp-keys"

    const-string v2, "pkr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pgp-signature"

    const-string v2, "sig"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pkcs10"

    const-string v2, "p10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pkcs7-signature"

    const-string v2, "p7s"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/pkix-pkipath"

    const-string v2, "pkipath"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/postscript"

    const-string v2, "ps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/ram"

    const-string v2, "ram"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/rdf+xml"

    const-string v2, "rdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/relaxng"

    const-string v2, "rng"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/rss+xml"

    const-string v2, "rss"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/rtf"

    const-string v2, "rtf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/sdp"

    const-string v2, "sdp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/sieve"

    const-string v2, "siv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/smil"

    const-string v2, "smil"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-7z-compressed"

    const-string v2, "7z"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-abiword"

    const-string v2, "abw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ace"

    const-string v2, "ace"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-alz"

    const-string v2, "alz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-amipro"

    const-string v2, "sam"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-anjuta"

    const-string v2, "prj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-aportisdoc"

    const-string v2, "pdb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-applix-spreadsheet"

    const-string v2, "as"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-applix-word"

    const-string v2, "aw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-archive"

    const-string v2, "a"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-arj"

    const-string v2, "arj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-asp"

    const-string v2, "asp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-awk"

    const-string v2, "awk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bcpio"

    const-string v2, "bcpio"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bittorrent"

    const-string v2, "torrent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-blender"

    const-string v2, "blend"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-btsnoop"

    const-string v2, "btsnoop"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bzdvi"

    const-string v2, "dvi.bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bzip-compressed-tar"

    const-string v2, "tar.bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bzip"

    const-string v2, "bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bzpdf"

    const-string v2, "pdf.bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-bzpostscript"

    const-string v2, "ps.bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cabri"

    const-string v2, "fig"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cb7"

    const-string v2, "cb7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cbr"

    const-string v2, "cbr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cbz"

    const-string v2, "cbz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cd-image"

    const-string v2, "iso"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cda"

    const-string v2, "cda"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cdrdao-toc"

    const-string v2, "toc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-chess-pgn"

    const-string v2, "pgn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-chm"

    const-string v2, "chm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cisco-vpn-settings"

    const-string v2, "pcf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-compress"

    const-string v2, "Z"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-compressed-tar"

    const-string v2, "tar.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cpio"

    const-string v2, "cpio"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-csh"

    const-string v2, "csh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-cue"

    const-string v2, "cue"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-dar"

    const-string v2, "dar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-dbf"

    const-string v2, "dbf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-dc-rom"

    const-string v2, "dc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-deb"

    const-string v2, "deb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-designer"

    const-string v2, "ui"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-desktop"

    const-string v2, "desktop"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-dia-diagram"

    const-string v2, "dia"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-drgeo"

    const-string v2, "fgeo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-dvi"

    const-string v2, "dvi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-e-theme"

    const-string v2, "etheme"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ect"

    const-string v2, "ect"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ees"

    const-string v2, "ees"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-egon"

    const-string v2, "egon"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-eio"

    const-string v2, "eio"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-eit"

    const-string v2, "eit"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-eiw"

    const-string v2, "eiw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-fictionbook+xml"

    const-string v2, "fb2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-fictionbook"

    const-string v2, "fb2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-fluid"

    const-string v2, "fl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-afm"

    const-string v2, "afm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-bdf"

    const-string v2, "bdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-linux-psf"

    const-string v2, "psf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-pcf"

    const-string v2, "pcf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-snf"

    const-string v2, "snf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-speedo"

    const-string v2, "spd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-ttf"

    const-string v2, "ttf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-ttx"

    const-string v2, "ttx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-font-type1"

    const-string v2, "pfb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-freemind"

    const-string v2, "mm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gameboy-rom"

    const-string v2, "gb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gba-rom"

    const-string v2, "gba"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gedcom"

    const-string v2, "ged"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-genesis-rom"

    const-string v2, "gen"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gettext-translation"

    const-string v2, "mo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-glade"

    const-string v2, "glade"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gnome-saved-search"

    const-string v2, "savedSearch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gnome-theme-package"

    const-string v2, "gtp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gnucash"

    const-string v2, "gnc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gnumeric"

    const-string v2, "gnumeric"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gnuplot"

    const-string v2, "gp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-go-sgf"

    const-string v2, "sgf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-google-gadget"

    const-string v2, "gg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-graphite"

    const-string v2, "gra"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gz-font-linux-psf"

    const-string v2, "psf.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gzdvi"

    const-string v2, "dvi.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gzip"

    const-string v2, "gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gzpdf"

    const-string v2, "pdf.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-gzpostscript"

    const-string v2, "ps.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-hcidump"

    const-string v2, "hcidump"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-hdf"

    const-string v2, "hdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-hwp"

    const-string v2, "hwp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-hwt"

    const-string v2, "hwt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ica"

    const-string v2, "ica"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-icq"

    const-string v2, "icq"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-java-archive"

    const-string v2, "jar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-java-jnlp-file"

    const-string v2, "jnlp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-java-pack200"

    const-string v2, "pack"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-java"

    const-string v2, "class"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-jbuilder-project"

    const-string v2, "jpx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-karbon"

    const-string v2, "karbon"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kchart"

    const-string v2, "chrt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kcsrc"

    const-string v2, "kcsrc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kexi-connectiondata"

    const-string v2, "kexic"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kexiproject-shortcut"

    const-string v2, "kexis"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kexiproject-sqlite2"

    const-string v2, "kexi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kexiproject-sqlite3"

    const-string v2, "kexi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kexiproject-sqlite"

    const-string v2, "kexi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kformula"

    const-string v2, "kfo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kgeo"

    const-string v2, "kgeo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kgetlist"

    const-string v2, "kgt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kig"

    const-string v2, "kig"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-killustrator"

    const-string v2, "kil"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kivio"

    const-string v2, "flw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kmplot"

    const-string v2, "ftk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kns"

    const-string v2, "kns"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kolf"

    const-string v2, "kolfgame"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kommander"

    const-string v2, "kmdr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-konsole"

    const-string v2, "shell"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kontour"

    const-string v2, "kon"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kopete-emoticons"

    const-string v2, "kopete-emoticons"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kourse"

    const-string v2, "kolf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kpovmodeler"

    const-string v2, "kpm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kpresenter"

    const-string v2, "kpr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-krita"

    const-string v2, "kra"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kseg"

    const-string v2, "seg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kspread"

    const-string v2, "ksp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ksysguard"

    const-string v2, "sgrd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ktheme"

    const-string v2, "kth"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kudesigner"

    const-string v2, "kut"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kugar"

    const-string v2, "kud"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kvtml"

    const-string v2, "kvtml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kwallet"

    const-string v2, "kwl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kword"

    const-string v2, "kwd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-kwordquiz"

    const-string v2, "wql"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lha"

    const-string v2, "lha"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lhz"

    const-string v2, "lhz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-linguist"

    const-string v2, "ts"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lyx"

    const-string v2, "lyx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lzma-compressed-tar"

    const-string v2, "tar.lzma"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lzma"

    const-string v2, "lzma"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-lzop"

    const-string v2, "lzo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-m4"

    const-string v2, "m4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-magicpoint"

    const-string v2, "mgp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-markaby"

    const-string v2, "mab"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-mif"

    const-string v2, "mif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-mimearchive"

    const-string v2, "eml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-mplayer2"

    const-string v2, "wmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-mps"

    const-string v2, "mps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ms-dos-executable"

    const-string v2, "exe"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-msi"

    const-string v2, "msi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-mswrite"

    const-string v2, "wri"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-msx-rom"

    const-string v2, "msx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-n64-rom"

    const-string v2, "n64"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-navi-animation"

    const-string v2, "ani"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-nes-rom"

    const-string v2, "nes"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-netcdf"

    const-string v2, "cdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-netshow-channel"

    const-string v2, "nsc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-nintendo-ds-rom"

    const-string v2, "nds"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-nzb"

    const-string v2, "nzb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-object"

    const-string v2, "o"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-oleo"

    const-string v2, "oleo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-openraster"

    const-string v2, "odr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pak"

    const-string v2, "pak"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-palm-database"

    const-string v2, "pdb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-par2"

    const-string v2, "par2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-perl"

    const-string v2, "pl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-php"

    const-string v2, "php"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pkcs12"

    const-string v2, "pfx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pkcs7-certificates"

    const-string v2, "spc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pktlog"

    const-string v2, "pklg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-planner"

    const-string v2, "planner"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-planperfect"

    const-string v2, "pln"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-plasma"

    const-string v2, "plasmoid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pocket-word"

    const-string v2, "psw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-pw"

    const-string v2, "pw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-python-bytecode"

    const-string v2, "pyc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-quanta"

    const-string v2, "quanta"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-quattropro"

    const-string v2, "wb3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-quicktime-media-link"

    const-string v2, "qtl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-qw"

    const-string v2, "qif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-rar"

    const-string v2, "rar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-reject"

    const-string v2, "rej"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-rpm"

    const-string v2, "rpm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ruby"

    const-string v2, "rb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-sami"

    const-string v2, "smi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-shar"

    const-string v2, "shar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-shared-library-la"

    const-string v2, "la"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-sharedlib"

    const-string v2, "so"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-shellscript"

    const-string v2, "sh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-shockwave-flash"

    const-string v2, "swf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-shorten"

    const-string v2, "shn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-siag"

    const-string v2, "siag"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-smaf"

    const-string v2, "mmf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-sms-rom"

    const-string v2, "sms"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-snes-rom"

    const-string v2, "smc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-stuffit"

    const-string v2, "sit"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-subrip"

    const-string v2, "srt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-superkaramba"

    const-string v2, "skz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-sv4cpio"

    const-string v2, "sv4cpio"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-sv4crc"

    const-string v2, "sv4crc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-t602"

    const-string v2, "602"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tar"

    const-string v2, "tar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tarz"

    const-string v2, "tar.Z"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tex-gf"

    const-string v2, "gf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tex-pk"

    const-string v2, "pk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tgif"

    const-string v2, "obj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-theme"

    const-string v2, "theme"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-trash"

    const-string v2, "bak"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-trig"

    const-string v2, "trig"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-troff-man"

    const-string v2, "man"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tuberling"

    const-string v2, "tuberling"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-tzo"

    const-string v2, "tar.lzo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ufraw"

    const-string v2, "ufraw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-uml"

    const-string v2, "xmi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-uof"

    const-string v2, "uof"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-ustar"

    const-string v2, "ustar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-vnc"

    const-string v2, "vnc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-vnd.kde.kexi"

    const-string v2, "kexi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-vnd.kde.kplato.work"

    const-string v2, "kplatowork"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-vnd.kde.kplato"

    const-string v2, "kplato"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-vnd.kde.kugar.mixed"

    const-string v2, "kug"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-wais-source"

    const-string v2, "src"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-webarchive"

    const-string v2, "war"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-win-lnk"

    const-string v2, "lnk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-wpg"

    const-string v2, "wpg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-x509-ca-cert"

    const-string v2, "cer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-xbel"

    const-string v2, "xbel"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-xliff"

    const-string v2, "xlf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/x-zoo"

    const-string v2, "zoo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xhtml+xml"

    const-string v2, "xhtml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xml-dtd"

    const-string v2, "dtd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xml-external-parsed-entity"

    const-string v2, "ent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xml"

    const-string v2, "xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xsd"

    const-string v2, "xsd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/xspf+xml"

    const-string v2, "xspf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "application/zip"

    const-string v2, "zip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/AMR-WB"

    const-string v2, "awb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/AMR"

    const-string v2, "amr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/aac"

    const-string v2, "aac"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/ac3"

    const-string v2, "ac3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/annodex"

    const-string v2, "axa"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/basic"

    const-string v2, "au"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/midi"

    const-string v2, "mid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/mp2"

    const-string v2, "mp2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/mp4"

    const-string v2, "m4a"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/mpeg"

    const-string v2, "mp3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/ogg"

    const-string v2, "ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/prs.sid"

    const-string v2, "sid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/vnd.rn-realaudio"

    const-string v2, "ra"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-aiff"

    const-string v2, "aif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-ape"

    const-string v2, "ape"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-flac+ogg"

    const-string v2, "ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-flac"

    const-string v2, "flac"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-gsm"

    const-string v2, "gsm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-iriver-pla"

    const-string v2, "pla"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-it"

    const-string v2, "it"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-m4b"

    const-string v2, "m4b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-matroska"

    const-string v2, "mka"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-minipsf"

    const-string v2, "minipsf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-mo3"

    const-string v2, "mo3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-mod"

    const-string v2, "mod"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-mpegurl"

    const-string v2, "m3u"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-ms-asx"

    const-string v2, "asx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-ms-wma"

    const-string v2, "wma"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-musepack"

    const-string v2, "mpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-psf"

    const-string v2, "psf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-psflib"

    const-string v2, "psflib"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-rosegarden"

    const-string v2, "rg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-s3m"

    const-string v2, "s3m"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-sbc"

    const-string v2, "sbc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-scpls"

    const-string v2, "pls"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-speex+ogg"

    const-string v2, "ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-speex"

    const-string v2, "spx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-stm"

    const-string v2, "stm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-tta"

    const-string v2, "tta"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-voc"

    const-string v2, "voc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-vorbis+ogg"

    const-string v2, "ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-wav"

    const-string v2, "wav"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-wavpack-correction"

    const-string v2, "wvc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-wavpack"

    const-string v2, "wv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-xi"

    const-string v2, "xi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-xm"

    const-string v2, "xm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "audio/x-xmf"

    const-string v2, "xmf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "fonts/package"

    const-string v2, "fonts.zip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/bmp"

    const-string v2, "bmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/cgm"

    const-string v2, "cgm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/fax-g3"

    const-string v2, "g3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/fits"

    const-string v2, "fits"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/gif"

    const-string v2, "gif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/ief"

    const-string v2, "ief"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/jp2"

    const-string v2, "jp2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/jpeg"

    const-string v2, "jpg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/png"

    const-string v2, "png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/rle"

    const-string v2, "rle"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/svg+xml-compressed"

    const-string v2, "svgz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/svg+xml"

    const-string v2, "svg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/tiff"

    const-string v2, "tif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.adobe.photoshop"

    const-string v2, "psd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.djvu"

    const-string v2, "djv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.dwg"

    const-string v2, "dwg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.dxf"

    const-string v2, "dxf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.microsoft.icon"

    const-string v2, "ico"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.ms-modi"

    const-string v2, "mdi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.rn-realpix"

    const-string v2, "rp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/vnd.wap.wbmp"

    const-string v2, "wbmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-3ds"

    const-string v2, "3ds"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-adobe-dng"

    const-string v2, "dng"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-applix-graphics"

    const-string v2, "ag"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-bzeps"

    const-string v2, "eps.bz2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-canon-cr2"

    const-string v2, "cr2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-canon-crw"

    const-string v2, "crw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-cmu-raster"

    const-string v2, "ras"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-compressed-xcf"

    const-string v2, "xcf.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-dds"

    const-string v2, "dds"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-emf"

    const-string v2, "emf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-eps"

    const-string v2, "eps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-exr"

    const-string v2, "exr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-fuji-raf"

    const-string v2, "raf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-gzeps"

    const-string v2, "eps.gz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-hdr"

    const-string v2, "hdr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-icns"

    const-string v2, "icns"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-iff"

    const-string v2, "iff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-ilbm"

    const-string v2, "ilbm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-jng"

    const-string v2, "jng"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-kodak-dcr"

    const-string v2, "dcr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-kodak-k25"

    const-string v2, "k25"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-kodak-kdc"

    const-string v2, "kdc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-lwo"

    const-string v2, "lwo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-lws"

    const-string v2, "lws"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-macpaint"

    const-string v2, "pntg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-minolta-mrw"

    const-string v2, "mrw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-msod"

    const-string v2, "msod"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-nikon-nef"

    const-string v2, "nef"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-olympus-orf"

    const-string v2, "orf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-panasonic-raw"

    const-string v2, "raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-pcx"

    const-string v2, "pcx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-pentax-pef"

    const-string v2, "pef"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-photo-cd"

    const-string v2, "pcd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-pict"

    const-string v2, "pict"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-portable-anymap"

    const-string v2, "pnm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-portable-bitmap"

    const-string v2, "pbm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-portable-graymap"

    const-string v2, "pgm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-portable-pixmap"

    const-string v2, "ppm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-quicktime"

    const-string v2, "qif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-rgb"

    const-string v2, "rgb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sgi"

    const-string v2, "sgi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sigma-x3f"

    const-string v2, "x3f"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-skencil"

    const-string v2, "sk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sony-arw"

    const-string v2, "arw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sony-sr2"

    const-string v2, "sr2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sony-srf"

    const-string v2, "srf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-sun-raster"

    const-string v2, "sun"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-tga"

    const-string v2, "tga"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-win-bitmap"

    const-string v2, "cur"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-wmf"

    const-string v2, "wmf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-xbitmap"

    const-string v2, "xbm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-xcf"

    const-string v2, "xcf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-xfig"

    const-string v2, "fig"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-xpixmap"

    const-string v2, "xpm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "image/x-xwindowdump"

    const-string v2, "xwd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "interface/x-winamp-skin"

    const-string v2, "wsz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "model/vrml"

    const-string v2, "wrl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/calendar"

    const-string v2, "ics"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/css"

    const-string v2, "css"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/csv"

    const-string v2, "csv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/directory"

    const-string v2, "vcf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/html"

    const-string v2, "htm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/mathml"

    const-string v2, "mml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/mrml"

    const-string v2, "mrml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/plain"

    const-string v2, "txt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/richtext"

    const-string v2, "rtx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/sgml"

    const-string v2, "sgm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/spreadsheet"

    const-string v2, "slk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/tab-separated-values"

    const-string v2, "tsv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/troff"

    const-string v2, "roff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.abc"

    const-string v2, "abc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.graphviz"

    const-string v2, "gv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.rn-realtext"

    const-string v2, "rt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.sun.j2me.app-descriptor"

    const-string v2, "jad"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.wap.wml"

    const-string v2, "wml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/vnd.wap.wmlscript"

    const-string v2, "wmls"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-adasrc"

    const-string v2, "ads"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-bibtex"

    const-string v2, "bib"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-c++hdr"

    const-string v2, "h"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-c++src"

    const-string v2, "cpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-chdr"

    const-string v2, "h"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-cmake"

    const-string v2, "cmake"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-csharp"

    const-string v2, "cs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-csrc"

    const-string v2, "c"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-dcl"

    const-string v2, "dcl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-dsl"

    const-string v2, "dsl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-dsrc"

    const-string v2, "d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-eiffel"

    const-string v2, "e"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-emacs-lisp"

    const-string v2, "el"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-erlang"

    const-string v2, "erl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-fortran"

    const-string v2, "f"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-gettext-translation-template"

    const-string v2, "pot"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-gettext-translation"

    const-string v2, "po"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-google-video-pointer"

    const-string v2, "gvp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-haskell"

    const-string v2, "hs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-hex"

    const-string v2, "hex"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-iMelody"

    const-string v2, "ime"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-idl"

    const-string v2, "idl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-iptables"

    const-string v2, "iptables"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-java"

    const-string v2, "java"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-katefilelist"

    const-string v2, "katefl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-lancelotpart"

    const-string v2, "lancelotpart"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-ldif"

    const-string v2, "ldif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-lilypond"

    const-string v2, "ly"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-literate-haskell"

    const-string v2, "lhs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-log"

    const-string v2, "log"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-lua"

    const-string v2, "lua"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-matlab"

    const-string v2, "m"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-microdvd"

    const-string v2, "sub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-moc"

    const-string v2, "moc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-mof"

    const-string v2, "mof"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-mpsub"

    const-string v2, "sub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-mrml"

    const-string v2, "mrml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-ms-regedit"

    const-string v2, "reg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-mup"

    const-string v2, "mup"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-nfo"

    const-string v2, "nfo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-objcsrc"

    const-string v2, "m"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-ocaml"

    const-string v2, "ml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-ocl"

    const-string v2, "ocl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-opml+xml"

    const-string v2, "opml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-pascal"

    const-string v2, "pas"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-patch"

    const-string v2, "diff"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-python"

    const-string v2, "py"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-rpm-spec"

    const-string v2, "spec"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-scheme"

    const-string v2, "scm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-setext"

    const-string v2, "etx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-sql"

    const-string v2, "sql"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-ssa"

    const-string v2, "ssa"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-subviewer"

    const-string v2, "sub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-tcl"

    const-string v2, "tcl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-tex"

    const-string v2, "tex"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-texinfo"

    const-string v2, "texinfo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-troff-me"

    const-string v2, "me"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-troff-mm"

    const-string v2, "mm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-troff-ms"

    const-string v2, "ms"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-txt2tags"

    const-string v2, "t2t"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-uil"

    const-string v2, "uil"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-uri"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-vala"

    const-string v2, "vala"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-vhdl"

    const-string v2, "vhd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-xmi"

    const-string v2, "xmi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "text/x-xslfo"

    const-string v2, "fo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/3gpp"

    const-string v2, "3gp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/annodex"

    const-string v2, "axv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/dv"

    const-string v2, "dv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/mp4"

    const-string v2, "mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/mpeg"

    const-string v2, "mpg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/ogg"

    const-string v2, "ogv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/quicktime"

    const-string v2, "mov"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/vivo"

    const-string v2, "viv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/vnd.rn-realvideo"

    const-string v2, "rvx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-anim"

    const-string v2, "anim[1-9j]"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-flic"

    const-string v2, "fli"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-flv"

    const-string v2, "flv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-matroska"

    const-string v2, "mkv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-mng"

    const-string v2, "mng"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-ms-asf"

    const-string v2, "asf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-ms-wmv"

    const-string v2, "wmv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-msvideo"

    const-string v2, "avi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-nsv"

    const-string v2, "nsv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-ogm+ogg"

    const-string v2, "ogm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-sgi-movie"

    const-string v2, "movie"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "video/x-theora+ogg"

    const-string v2, "ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    const-string v1, "x-epoc/x-sisx-app"

    const-string v2, "sisx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Ljava/net/URISyntaxException;
        }
    .end annotation

    const/4 v2, 0x3

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;

    invoke-direct {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;-><init>()V

    :cond_0
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    new-instance v4, Ljava/net/URI;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    const/16 v8, 0x7530

    invoke-static {v7, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/16 v8, 0x7530

    invoke-static {v7, v8}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    const-string v7, "User-Agent"

    const-string v8, "Mozilla/5.0 (Linux; U; Android 1.5; en-us) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1"

    invoke-virtual {v5, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const-string v7, "Referer"

    move-object/from16 v0, p2

    invoke-virtual {v5, v7, v0}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :try_start_0
    invoke-virtual {v3, v5}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    if-nez v3, :cond_3

    :cond_2
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    :catch_0
    move-exception v2

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    :catch_1
    move-exception v2

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    :cond_3
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    div-int/lit8 v5, v3, 0x64

    const/4 v8, 0x3

    if-ne v5, v8, :cond_6

    const-string v3, "location"

    invoke-interface {v7, v3}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v3

    if-eqz v3, :cond_5

    array-length v5, v3

    if-lez v5, :cond_5

    new-instance v5, Ljava/net/URL;

    invoke-virtual {v4}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v4

    const/4 v8, 0x0

    aget-object v3, v3, v8

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v4, v3}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p1

    add-int/lit8 v2, v2, -0x1

    if-gtz v2, :cond_0

    :cond_4
    :goto_0
    iget-object v2, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    if-nez v2, :cond_7

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Resource info had null url (redirect limit exceeded)."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    :cond_6
    div-int/lit8 v2, v3, 0x64

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p1

    iput-object v0, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->url:Ljava/lang/String;

    goto :goto_0

    :cond_7
    const/4 v3, 0x0

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v8

    array-length v9, v8

    const/4 v2, 0x0

    move v5, v2

    move-object v2, v3

    :goto_1
    if-ge v5, v9, :cond_e

    aget-object v3, v8, v5

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v10, "content-disposition"

    invoke-virtual {v4, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_a

    invoke-interface {v3}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v10

    array-length v11, v10

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v11, :cond_d

    aget-object v4, v10, v3

    invoke-interface {v4}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "attachment"

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_9

    invoke-interface {v4}, Lorg/apache/http/HeaderElement;->getParameters()[Lorg/apache/http/NameValuePair;

    move-result-object v12

    array-length v13, v12

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v13, :cond_9

    aget-object v14, v12, v4

    invoke-interface {v14}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "filename"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_8

    invoke-interface {v14}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v2

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_a
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v10, "content-type"

    invoke-virtual {v4, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_d

    invoke-interface {v3}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v10

    array-length v11, v10

    const/4 v3, 0x0

    move v4, v3

    :goto_4
    if-ge v4, v11, :cond_d

    aget-object v3, v10, v4

    invoke-interface {v3}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->contentType:Ljava/lang/String;

    invoke-interface {v3}, Lorg/apache/http/HeaderElement;->getParameters()[Lorg/apache/http/NameValuePair;

    move-result-object v12

    array-length v13, v12

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v13, :cond_c

    aget-object v14, v12, v3

    invoke-interface {v14}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "charset"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_b

    invoke-interface {v14}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->encoding:Ljava/lang/String;

    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_c
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    :cond_d
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_1

    :cond_e
    const/4 v3, 0x0

    if-eqz v2, :cond_11

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/PathUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_f
    :goto_6
    new-instance v3, Ljava/io/File;

    if-eqz v2, :cond_10

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :cond_10
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x1000

    new-array v7, v2, [B

    invoke-virtual {v4, v7}, Ljava/io/InputStream;->read([B)I

    move-result v2

    :goto_7
    if-lez v2, :cond_13

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8, v2}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {v4, v7}, Ljava/io/InputStream;->read([B)I

    move-result v2

    goto :goto_7

    :cond_11
    iget-object v2, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->contentType:Ljava/lang/String;

    if-eqz v2, :cond_14

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;->mimeMap:Ljava/util/HashMap;

    iget-object v3, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->contentType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :goto_8
    if-nez v2, :cond_f

    const/16 v2, 0x3f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_12

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/PathUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    :cond_12
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PathUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    :cond_13
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader$ResourceInfo;->path:Ljava/lang/String;

    return-object v6

    :cond_14
    move-object v2, v3

    goto :goto_8
.end method
