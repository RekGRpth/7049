.class Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;
.super Ljava/lang/Object;
.source "CssCommentParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$1;,
        Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z
    .locals 7
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0x2f

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->START_COMMENT:Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;

    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$CssCommentParser$ParsingState:[I

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_1
    :sswitch_0
    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    goto :goto_0

    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_2
    return v2

    :sswitch_1
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->SINGLE_LINE_COMMENT:Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;

    invoke-interface {p0, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_1

    :sswitch_2
    invoke-interface {p0, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->MULTI_LINE_COMMENT:Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;

    goto :goto_1

    :pswitch_1
    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_1

    :pswitch_3
    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    move v2, v3

    goto :goto_2

    :pswitch_4
    packed-switch v0, :pswitch_data_2

    goto :goto_1

    :pswitch_5
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->ENDING_MULTI_LINE_COMMENT:Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;

    goto :goto_1

    :pswitch_6
    sparse-switch v0, :sswitch_data_1

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;->MULTI_LINE_COMMENT:Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser$ParsingState;

    goto :goto_1

    :sswitch_3
    invoke-interface {p0}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    move v2, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_6
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_2
        0x2f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2a
        :pswitch_5
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x2a -> :sswitch_0
        0x2f -> :sswitch_3
    .end sparse-switch
.end method
