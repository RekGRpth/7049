.class final enum Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;
.super Ljava/lang/Enum;
.source "CssParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParsingCssState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum AT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum CHARSET_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum IMPORT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum PIPE_TILL_SEMI:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

.field public static final enum WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "WHITE_SPACES_BEFORE_TOKEN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "REGULAR_TOKEN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "AT_EXPRESSION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->AT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "IMPORT_EXPRESSION"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->IMPORT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "CHARSET_EXPRESSION"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->CHARSET_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const-string v1, "PIPE_TILL_SEMI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->PIPE_TILL_SEMI:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->WHITE_SPACES_BEFORE_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->AT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->IMPORT_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->CHARSET_EXPRESSION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->PIPE_TILL_SEMI:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingCssState;

    return-object v0
.end method
