.class final enum Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;
.super Ljava/lang/Enum;
.source "CssParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParsingUrlState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum BEFORE_URL:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum ESCAPE_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

.field public static final enum URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "BEFORE_URL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->BEFORE_URL:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "INSIDE_SINGLE_QUOTES"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "INSIDE_DOUBLE_QUOTES"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "ESCAPE_INSIDE_DOUBLE_QUOTES"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "ESCAPE_INSIDE_SINGLE_QUOTES"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "REGULAR_TOKEN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "TOKEN_U"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "TOKEN_R"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "TOKEN_L"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "URL_FUNCTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const-string v1, "URL_EXPECTING_CLOSE_BRACKET"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->BEFORE_URL:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->ESCAPE_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssParser$ParsingUrlState;

    return-object v0
.end method
