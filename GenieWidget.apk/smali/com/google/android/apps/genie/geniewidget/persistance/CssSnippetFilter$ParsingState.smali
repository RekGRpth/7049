.class final enum Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;
.super Ljava/lang/Enum;
.source "CssSnippetFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParsingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum AFTER_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

.field public static final enum URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "BEFORE_COLUMN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "AFTER_COLUMN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->AFTER_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "REGULAR_TOKEN"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "DOUBLE_QUOTES"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "ESCAPE_IN_DOUBLE_QUOTES"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "SINGLE_QUOTES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "ESCAPE_IN_SINGLE_QUOTES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "TOKEN_U"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "TOKEN_R"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "TOKEN_L"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_FUNCTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_INSIDE_DOUBLE_QUOTES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_INSIDE_SINGLE_QUOTES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_ESCAPE_IN_DOUBLE_QUOTES"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_ESCAPE_IN_SINGLE_QUOTES"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const-string v1, "URL_EXPECTING_CLOSE_BRACKET"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->AFTER_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    return-object v0
.end method
