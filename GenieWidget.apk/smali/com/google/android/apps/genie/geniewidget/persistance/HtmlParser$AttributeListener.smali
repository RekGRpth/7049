.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
.super Ljava/lang/Object;
.source "HtmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AttributeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)V

    return-void
.end method


# virtual methods
.method protected bypassAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v3, 0x27

    const/16 v2, 0x22

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V
    invoke-static {v1, p1, p2, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$000(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v0, v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V
    invoke-static {v1, p1, p2, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$000(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    :cond_2
    :goto_1
    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    goto :goto_1
.end method

.method public onAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x20

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    const/16 v0, 0x3d

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;->bypassAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    const/4 v0, 0x1

    return v0
.end method
