.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
.super Ljava/lang/Object;
.source "HtmlParser.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/ContentParser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;,
        Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
    }
.end annotation


# instance fields
.field private baseUrl:Ljava/lang/String;

.field private charsetName:Ljava/lang/String;

.field private resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
    .param p1    # Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    return-object v0
.end method

.method private bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/io/ByteArrayOutputStream;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "ISO-8859-1"

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, ""

    goto :goto_0
.end method

.method private dealWithBaseTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const-string v3, "href"

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/util/HashSet;Ljava/util/HashSet;)V

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V

    const-string v3, "href"

    invoke-virtual {v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_1

    const/16 v3, 0x20

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    return-void
.end method

.method private dealWithEncoding(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;

    invoke-direct {v4, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getElementCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    invoke-virtual {v4, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getElement(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;

    move-result-object v0

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->getParamCount()I

    move-result v6

    if-ge v2, v6, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->getParam(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "charset"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;->getValue()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private dealWithLinkTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v6, "rel"

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const-string v6, "href"

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/util/HashSet;Ljava/util/HashSet;)V

    invoke-direct {p0, p1, p2, v2, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V

    const-string v6, "href"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v6, "rel"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "stylesheet"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_0
    if-eqz v0, :cond_0

    const-string v6, " href=\""

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-interface {v6, v7, v0, v8, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    const/16 v6, 0x22

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v6

    const/16 v7, 0x2f

    if-ne v6, v7, :cond_1

    const/16 v6, 0x20

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    return-void

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private dealWithMetaTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const-string v5, "http-equiv"

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const-string v5, "content"

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;

    invoke-direct {v3, p0, v2, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/util/HashSet;Ljava/util/HashSet;)V

    const/4 v5, 0x1

    invoke-direct {p0, p1, p2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V

    const-string v5, "http-equiv"

    invoke-virtual {v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "content"

    invoke-virtual {v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributesRetriever;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v5, "refresh"

    invoke-virtual {v1, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithRefresh(Ljava/lang/String;Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    const-string v5, " content=\""

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write([B)V

    const/16 v5, 0x22

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v5

    const/16 v6, 0x2f

    if-ne v5, v6, :cond_2

    const/16 v5, 0x20

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    :cond_2
    return-void

    :cond_3
    const-string v5, "content-type"

    invoke-virtual {v1, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithEncoding(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private dealWithRefresh(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v7, " content=\""

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write([B)V

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;

    invoke-direct {v5, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getElementCount()I

    move-result v7

    if-ge v1, v7, :cond_5

    invoke-virtual {v5, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getElement(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;

    move-result-object v0

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->getParamCount()I

    move-result v7

    if-ge v2, v7, :cond_3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->getParam(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-interface {v7, v8, v6, v9, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->encodeToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write([B)V

    if-eqz v6, :cond_1

    const/16 v7, 0x3d

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->encodeToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write([B)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->getParamCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_2

    const/16 v7, 0x3b

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getElementCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v1, v7, :cond_4

    const/16 v7, 0x2c

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    const/16 v7, 0x22

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private dealWithScriptTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, -0x1

    const-string v1, "src"

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    :cond_0
    const-string v1, "</script"

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillString(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    :cond_1
    if-eq v0, v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_2
    return-void
.end method

.method private dealWithStyleTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v8, 0x3e

    const/4 v7, 0x0

    const/4 v6, -0x1

    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    invoke-direct {v2, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;)V

    :cond_0
    new-instance v3, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;

    const-string v4, "</style"

    invoke-direct {v3, p1, v4, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;Z)V

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssParser;->process(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->getEndPoint()[B

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    :cond_1
    invoke-direct {p0, p1, p2, v7}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v0

    if-eq v0, v8, :cond_2

    if-ne v0, v6, :cond_0

    :cond_2
    if-eq v0, v6, :cond_3

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    :cond_3
    return-void
.end method

.method private dealWithTextAreaTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    :cond_0
    const-string v1, "</textarea"

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillString(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    :cond_1
    if-eq v0, v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_2
    return-void
.end method

.method private dealWithXmlEncoding(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V

    return-void
.end method

.method private encodeToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :sswitch_0
    const/16 v5, 0x5c

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :sswitch_1
    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_1
    return-object v4

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0x20 -> :sswitch_1
        0x22 -> :sswitch_0
        0x2c -> :sswitch_1
        0x3b -> :sswitch_1
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method private getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;

    invoke-direct {v0, p0, p3, p4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V

    return-void
.end method

.method private parseAttributes(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;Z)V
    .locals 9
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x1

    const/16 v7, 0x3e

    const/4 v6, -0x1

    const/16 v5, 0x20

    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v3, v5, :cond_4

    const/16 v3, 0x61

    if-lt v1, v3, :cond_1

    const/16 v3, 0x7a

    if-le v1, v3, :cond_3

    :cond_1
    const/16 v3, 0x41

    if-lt v1, v3, :cond_2

    const/16 v3, 0x5a

    if-le v1, v3, :cond_3

    :cond_2
    const/16 v3, 0x5f

    if-eq v1, v3, :cond_3

    const/16 v3, 0x2d

    if-ne v1, v3, :cond_4

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    goto :goto_1

    :cond_4
    if-ne v1, v6, :cond_6

    :cond_5
    :goto_2
    return-void

    :cond_6
    if-ne v1, v7, :cond_7

    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ne v3, v5, :cond_b

    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_9

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write([B)V

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    const/16 v3, 0x2f

    if-ne v1, v3, :cond_a

    if-nez p4, :cond_5

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    :cond_a
    :goto_3
    if-eq v1, v5, :cond_0

    const/16 v3, 0xd

    if-eq v1, v3, :cond_0

    const/16 v3, 0xa

    if-eq v1, v3, :cond_0

    const/16 v3, 0x9

    if-eq v1, v3, :cond_0

    if-eq v1, v7, :cond_0

    if-eq v1, v6, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    goto :goto_3

    :cond_b
    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_c

    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2

    :cond_c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_d

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write([B)V

    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_2

    :cond_d
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v3

    const/16 v4, 0x3d

    if-eq v3, v4, :cond_e

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bypassWhiteSpaces(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_f

    invoke-virtual {p2, v5}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write([B)V

    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_2

    :cond_f
    invoke-virtual {p3, p1, p2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;->onAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v1

    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_0

    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_2
.end method

.method private parseTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    :goto_0
    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    const/16 v3, 0x61

    if-lt v0, v3, :cond_0

    const/16 v3, 0x7a

    if-le v0, v3, :cond_3

    :cond_0
    const/16 v3, 0x41

    if-lt v0, v3, :cond_1

    const/16 v3, 0x5a

    if-le v0, v3, :cond_3

    :cond_1
    const/16 v3, 0x30

    if-lt v0, v3, :cond_2

    const/16 v3, 0x39

    if-le v0, v3, :cond_3

    :cond_2
    const/16 v3, 0x5f

    if-eq v0, v3, :cond_3

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_4

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->stringToBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "a"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "href"

    invoke-direct {p0, p1, p2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    :goto_1
    return-void

    :cond_5
    const-string v3, "area"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "href"

    invoke-direct {p0, p1, p2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_6
    const-string v3, "base"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithBaseTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_7
    const-string v3, "form"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "action"

    invoke-direct {p0, p1, p2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_8
    const-string v3, "input"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "src"

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_9
    const-string v3, "link"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithLinkTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_a
    const-string v3, "meta"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithMetaTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_b
    const-string v3, "script"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithScriptTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_c
    const-string v3, "textarea"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithTextAreaTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_d
    const-string v3, "style"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithStyleTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_1

    :cond_e
    const-string v3, "bgsound"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "embed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "frame"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "iframe"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "img"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "input"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_f
    const-string v3, "src"

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_10
    const-string v3, "body"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "table"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "td"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "th"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    :cond_11
    const-string v3, "background"

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_12
    const-string v3, "applet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    const-string v3, "code"

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_13
    const-string v3, "object"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    const-string v3, "data"

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_14
    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method private pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, -0x1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    :goto_0
    if-eq v0, v2, :cond_0

    if-eq v0, p3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    goto :goto_0

    :cond_0
    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    return-void
.end method

.method private pipeTillString(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;

    const/4 v3, 0x0

    invoke-direct {v2, p1, p3, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->read()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->getEndPoint()[B

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    :cond_1
    return-void
.end method

.method private stringToBytes(Ljava/lang/String;)[B
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "ISO-8859-1"

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v1

    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public process(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/InputStream;
    .param p4    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x2d

    const/16 v3, 0x3e

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    invoke-direct {v1, p3}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_0
    const/16 v2, 0x3c

    invoke-direct {p0, v1, p4, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/16 v2, 0x61

    if-lt v0, v2, :cond_0

    const/16 v2, 0x7a

    if-le v0, v2, :cond_1

    :cond_0
    const/16 v2, 0x41

    if-lt v0, v2, :cond_4

    const/16 v2, 0x5a

    if-gt v0, v2, :cond_4

    :cond_1
    invoke-direct {p0, v1, p4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->parseTag(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_0

    :sswitch_0
    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v2

    invoke-virtual {p4, v2}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v0

    invoke-virtual {p4, v0}, Ljava/io/OutputStream;->write(I)V

    if-ne v0, v4, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v0

    invoke-virtual {p4, v0}, Ljava/io/OutputStream;->write(I)V

    if-ne v0, v4, :cond_2

    const-string v2, "-->"

    invoke-direct {p0, v1, p4, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillString(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1, p4, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1, p4, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, v1, p4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->dealWithXmlEncoding(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v1, p4, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->pipeTillCharacter(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    :sswitch_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_2
        0x21 -> :sswitch_0
        0x3f -> :sswitch_1
    .end sparse-switch
.end method
