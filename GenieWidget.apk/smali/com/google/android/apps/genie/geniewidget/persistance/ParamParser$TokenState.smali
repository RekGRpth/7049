.class final enum Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;
.super Ljava/lang/Enum;
.source "ParamParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TokenState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

.field public static final enum NORMAL_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

.field public static final enum QUOTED_ESCAPED:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

.field public static final enum QUOTED_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

.field public static final enum WHITE_SPACES:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    const-string v1, "WHITE_SPACES"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->WHITE_SPACES:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    const-string v1, "NORMAL_TOKEN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->NORMAL_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    const-string v1, "QUOTED_TOKEN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    const-string v1, "QUOTED_ESCAPED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_ESCAPED:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->WHITE_SPACES:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->NORMAL_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_ESCAPED:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    return-object v0
.end method
