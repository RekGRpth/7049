.class Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;
.super Ljava/lang/Object;
.source "ParamParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$1;,
        Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;,
        Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;,
        Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;
    }
.end annotation


# instance fields
.field elements:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->elements:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->parseParams(Ljava/lang/String;)V

    return-void
.end method

.method static getToken(Ljava/lang/String;ILjava/lang/StringBuilder;)I
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->WHITE_SPACES:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge p1, v3, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$ParamParser$TokenState:[I

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_1
    :sswitch_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->NORMAL_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 p1, p1, 0x1

    move v1, p1

    :goto_2
    return v1

    :sswitch_2
    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    goto :goto_1

    :pswitch_1
    sparse-switch v0, :sswitch_data_1

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_3
    move v1, p1

    goto :goto_2

    :pswitch_2
    sparse-switch v0, :sswitch_data_2

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_4
    add-int/lit8 p1, p1, 0x1

    move v1, p1

    goto :goto_2

    :sswitch_5
    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_ESCAPED:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;->QUOTED_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$TokenState;

    goto :goto_1

    :cond_0
    move v1, p1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_2
        0x2c -> :sswitch_1
        0x3b -> :sswitch_1
        0x3d -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x22 -> :sswitch_3
        0x2c -> :sswitch_3
        0x3b -> :sswitch_3
        0x3d -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_4
        0x5c -> :sswitch_5
    .end sparse-switch
.end method

.method private parseParams(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_6

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getToken(Ljava/lang/String;ILjava/lang/StringBuilder;)I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    const-string v6, "="

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, ";"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v3, v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v2, v6, :cond_4

    const-string v4, ","

    :cond_1
    const-string v6, "="

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getToken(Ljava/lang/String;ILjava/lang/StringBuilder;)I

    move-result v2

    const-string v5, ""

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ";"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v5, v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v2, v6, :cond_5

    const-string v4, ","

    :cond_2
    if-eqz v3, :cond_3

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;

    invoke-direct {v6, p0, v3, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->elements:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;

    invoke-direct {v7, p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_0

    :cond_4
    invoke-static {p1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getToken(Ljava/lang/String;ILjava/lang/StringBuilder;)I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    invoke-static {p1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->getToken(Ljava/lang/String;ILjava/lang/StringBuilder;)I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->elements:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;

    invoke-direct {v7, p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_7
    return-void
.end method


# virtual methods
.method public getElement(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->elements:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;

    return-object v0
.end method

.method public getElementCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;->elements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
