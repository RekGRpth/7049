.class Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;
.super Ljava/lang/Object;
.source "RecursiveDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadItem"
.end annotation


# instance fields
.field private encoding:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->path:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->encoding:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader$DownloadItem;->url:Ljava/lang/String;

    return-object v0
.end method
