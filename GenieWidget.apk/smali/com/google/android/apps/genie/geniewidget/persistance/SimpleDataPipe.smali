.class Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;
.super Ljava/lang/Object;
.source "SimpleDataPipe.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;


# instance fields
.field private input:Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

.field private output:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    invoke-direct {v0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->input:Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->output:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public peek()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->input:Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v0

    return v0
.end method

.method public pipe()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->input:Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write(I)V

    :cond_0
    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->input:Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v0

    return v0
.end method

.method public write(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public writeEscaped(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SimpleDataPipe;->output:Ljava/io/OutputStream;

    aget-byte v5, v0, v2

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    const-string v4, "ISO-8859-1"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v3

    :cond_0
    return-void
.end method
