.class Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;
.super Ljava/io/InputStream;
.source "SpecialEndCharacterInputStream.java"


# instance fields
.field private endCharacter:I

.field private eof:Z

.field private expected:Ljava/lang/String;

.field private input:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->endCharacter:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->eof:Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->input:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->expected:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public getEndCharacter()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->endCharacter:I

    return v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v1, -0x1

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->eof:Z

    if-eqz v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ne v0, v1, :cond_2

    iput-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->eof:Z

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->expected:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-eq v2, v1, :cond_0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->endCharacter:I

    iput-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->eof:Z

    move v0, v1

    goto :goto_0
.end method
