.class Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;
.super Ljava/io/InputStream;
.source "SpecialEndPointInputStream.java"


# instance fields
.field private buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

.field private caseSensitive:Z

.field private endPoint:[B

.field private eof:Z

.field private expected:[B

.field private input:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->eof:Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->input:Ljava/io/InputStream;

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->caseSensitive:Z

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    :goto_0
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    array-length v1, v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    goto :goto_0
.end method

.method private static toLowerCase(I)I
    .locals 1
    .param p0    # I

    const/16 v0, 0x41

    if-lt p0, v0, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p0

    :cond_0
    return p0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public getEndPoint()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    return-object v0
.end method

.method public read()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x1

    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v3, -0x1

    const/4 v2, -0x1

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v8

    if-lez v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->popFront()B

    move-result v3

    iget-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->caseSensitive:Z

    if-nez v8, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->toLowerCase(I)I

    move-result v2

    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    add-int/lit8 v6, v5, 0x1

    aget-byte v8, v8, v5

    if-eq v2, v8, :cond_1

    move v5, v6

    move v7, v2

    :goto_1
    return v7

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    move v5, v6

    :cond_2
    iget-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->eof:Z

    if-eqz v8, :cond_3

    move v7, v2

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v8

    if-ge v4, v8, :cond_5

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->get(I)B

    move-result v0

    iget-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->caseSensitive:Z

    if-nez v8, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->toLowerCase(I)I

    move-result v8

    int-to-byte v0, v8

    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    add-int/lit8 v6, v5, 0x1

    aget-byte v8, v8, v5

    if-eq v0, v8, :cond_6

    move v5, v6

    :cond_5
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v8

    if-ge v5, v8, :cond_8

    if-eq v2, v7, :cond_7

    move v7, v2

    goto :goto_1

    :cond_6
    add-int/lit8 v4, v4, 0x1

    move v5, v6

    goto :goto_2

    :cond_7
    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->popFront()B

    move-result v7

    goto :goto_1

    :cond_8
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v1

    :goto_3
    if-eq v1, v7, :cond_c

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    aget-byte v8, v8, v5

    if-eq v1, v8, :cond_9

    iget-boolean v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->caseSensitive:Z

    if-nez v8, :cond_c

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->toLowerCase(I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    aget-byte v9, v9, v5

    if-ne v8, v9, :cond_c

    :cond_9
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v9, v1

    invoke-virtual {v8, v9}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    add-int/lit8 v5, v5, 0x1

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->expected:[B

    array-length v8, v8

    if-ne v5, v8, :cond_b

    if-ne v3, v7, :cond_a

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray()[B

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    :goto_4
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->clear()V

    iput-boolean v11, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->eof:Z

    goto :goto_1

    :cond_a
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    const/4 v9, 0x0

    int-to-byte v10, v3

    aput-byte v10, v8, v9

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->endPoint:[B

    iget-object v10, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v10}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v10

    invoke-virtual {v8, v9, v11, v10}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->toByteArray([BII)V

    goto :goto_4

    :cond_b
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v1

    goto :goto_3

    :cond_c
    if-ne v1, v7, :cond_d

    iput-boolean v11, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->eof:Z

    :cond_d
    if-eq v2, v7, :cond_e

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v8, v1

    invoke-virtual {v7, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    move v7, v2

    goto/16 :goto_1

    :cond_e
    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->size()I

    move-result v7

    if-lez v7, :cond_f

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    int-to-byte v8, v1

    invoke-virtual {v7, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->pushBack(B)V

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndPointInputStream;->buffer:Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/persistance/FixedCapacityByteQueue;->popFront()B

    move-result v7

    goto/16 :goto_1

    :cond_f
    move v7, v1

    goto/16 :goto_1
.end method
