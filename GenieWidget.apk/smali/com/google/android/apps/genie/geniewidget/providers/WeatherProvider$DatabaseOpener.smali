.class Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "WeatherProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseOpener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;Landroid/content/Context;)V
    .locals 3
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;->this$0:Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;

    const-string v0, "weather.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    # getter for: Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->access$000()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    const-string v1, "Genie"

    const-string v2, "Creating weather database"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CREATE TABLE tblDailyWeather(_id INTEGER PRIMARY KEY AUTOINCREMENT,fakeLocation TEXT, location TEXT, timestamp INTEGER, begins INTEGER, ends INTEGER, pointInTime TEXT, description TEXT, temperature INTEGER, highTemperature INTEGER, lowTemperature INTEGER, chancePrecipitation INTEGER, wind TEXT, humidity TEXT, sunrise TEXT, sunset TEXT, iconUrl TEXT, iconResId INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE tblHourlyWeather(_id INTEGER PRIMARY KEY AUTOINCREMENT,fakeLocation TEXT, location TEXT, timestamp INTEGER, begins INTEGER, ends INTEGER, description TEXT, temperature INTEGER, highTemperature INTEGER, lowTemperature INTEGER, chancePrecipitation INTEGER, wind TEXT, humidity TEXT, sunrise TEXT, sunset TEXT, iconUrl TEXT, iconResId INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    const-string v0, "Genie"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading weather database from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "DROP TABLE IF EXISTS tblDailyWeather"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS tblHourlyWeather"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider$DatabaseOpener;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
