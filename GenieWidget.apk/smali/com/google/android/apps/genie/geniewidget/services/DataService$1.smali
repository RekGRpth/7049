.class Lcom/google/android/apps/genie/geniewidget/services/DataService$1;
.super Ljava/lang/Object;
.source "DataService.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/utils/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/services/DataService;->requestTopicList(Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
        "<[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/services/DataService;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-interface {v0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->success([B)V

    return-void
.end method

.method public success([B)V
    .locals 10
    .param p1    # [B

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->clearTopics()V

    const-string v6, "\n"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v6, v3

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v3, v7

    const/4 v8, 0x1

    aget-object v8, v3, v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->addTopic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->isPopulated()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    new-instance v7, Ljava/lang/Exception;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bad server response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    :goto_2
    return-void

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    move-result-object v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->setEditionLocale(Ljava/util/Locale;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;->this$0:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    # getter for: Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    invoke-static {v7}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->getAllTopics()Ljava/util/LinkedHashSet;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->success(Ljava/lang/Object;)V

    goto :goto_2
.end method
