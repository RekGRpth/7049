.class public Lcom/google/android/apps/genie/geniewidget/services/DataService;
.super Ljava/lang/Object;
.source "DataService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;
    }
.end annotation


# instance fields
.field private clickedWidgetItemIndex:I

.field private clickedWidgetItemType:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

.field private editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

.field private final model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private final urlFetcher:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;Lcom/google/android/apps/genie/geniewidget/GenieContext;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/GenieContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->urlFetcher:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    invoke-direct {v1}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NONE:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemType:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemIndex:I

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/services/DataService;)Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/services/DataService;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    return-object v0
.end method

.method private getNewsItem(I)Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    goto :goto_0
.end method


# virtual methods
.method public getClickedWidgetItemType()Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemType:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    return-object v0
.end method

.method public getClickedWidgetNewsItem()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemIndex:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->getNewsItem(I)Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public requestTopicList(Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;",
            ">;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->isPopulated()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->getEditionLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->editionNewsTopics:Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/model/EditionNewsTopics;->getAllTopics()Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->success(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/GConfig;->NEWS_TOPICS_URL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->urlFetcher:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/genie/geniewidget/services/DataService$1;-><init>(Lcom/google/android/apps/genie/geniewidget/services/DataService;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->fetchAsync(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    goto :goto_0
.end method

.method public setClickedWidgetItemType(Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;I)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemType:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/services/DataService;->clickedWidgetItemIndex:I

    return-void
.end method
