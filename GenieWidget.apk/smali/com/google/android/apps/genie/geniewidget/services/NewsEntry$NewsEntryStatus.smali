.class public final enum Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;
.super Ljava/lang/Enum;
.source "NewsEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NewsEntryStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

.field public static final enum DOWNLOADING:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

.field public static final enum FAILED:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

.field public static final enum READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->DOWNLOADING:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->FAILED:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->DOWNLOADING:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->FAILED:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;
    .locals 1

    const-class v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;
    .locals 1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->$VALUES:[Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-virtual {v0}, [Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    return-object v0
.end method
