.class public Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;
.super Ljava/lang/Object;
.source "NewsEntry.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x4c52b8c1ffebec2dL


# instance fields
.field private createTime:J

.field private final guid:Ljava/lang/String;

.field private localCache:Ljava/lang/String;

.field private status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->guid:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->DOWNLOADING:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->createTime:J

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;
    .locals 6
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p0}, Ljava/lang/String;-><init>([B)V

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Badly formed data"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v1, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-direct {v1, v3}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;-><init>(Ljava/lang/String;)V

    const-string v3, "null"

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v3}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setCacheFile(Ljava/lang/String;)V

    const/4 v3, 0x2

    aget-object v3, v0, v3

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setStatus(Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;)V

    const/4 v3, 0x3

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->setCreateTime(J)V

    return-object v1

    :cond_1
    aget-object v3, v0, v5

    goto :goto_0
.end method


# virtual methods
.method public getLocalCache()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->localCache:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    return-object v0
.end method

.method public serialize()[B
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->guid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->localCache:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->createTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    return-object v1
.end method

.method public setCacheFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->localCache:Ljava/lang/String;

    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->createTime:J

    return-void
.end method

.method public setStatus(Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;->READY:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NewsEntry: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->status:Lcom/google/android/apps/genie/geniewidget/services/NewsEntry$NewsEntryStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " guid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->guid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cache: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/services/NewsEntry;->localCache:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
