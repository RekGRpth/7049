.class public Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "FlingableLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;,
        Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnFlingListener;,
        Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;
    }
.end annotation


# instance fields
.field private flingListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnFlingListener;

.field private isScrollMode:Z

.field private leftScrollMargin:I

.field private motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

.field private motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

.field private notifyFlingFinished:Z

.field private preX:I

.field private rightScrollMargin:I

.field private scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

.field private scrollable:Z

.field private scroller:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-direct {v0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->init()V

    return-void
.end method

.method private doScrolling(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v0

    neg-int v1, p1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollBy(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->invalidate()V

    return-void
.end method

.method private fling(II)Z
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->HORIZONTAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getVelocityX()I

    move-result v9

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->NONE:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    invoke-direct {p0, v9}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getFlingDirection(I)Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v2

    iput-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->isScrollMode:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v1

    neg-int v3, v9

    move v5, p1

    move v6, p2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->invalidate()V

    const/4 v4, 0x1

    goto :goto_0
.end method

.method private getFlingDirection(I)Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;
    .locals 2
    .param p1    # I

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->HORIZONTAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->NONE:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    if-le v0, v1, :cond_2

    if-lez p1, :cond_1

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->RIGHT:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->LEFT:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->NONE:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    goto :goto_0
.end method

.method private getVelocityX()I
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->getVelocityTracker()Landroid/view/VelocityTracker;

    move-result-object v0

    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    float-to-int v1, v2

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->reset()V

    return v1
.end method

.method private init()V
    .locals 2

    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    return-void
.end method

.method private scrollInTouch(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->preX:I

    sub-int v1, v0, v2

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->doScrolling(I)V

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->preX:I

    return-void
.end method

.method private stopScroll()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->notifyFlingFinished:Z

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v2

    invoke-super {p0, v0, v2}, Landroid/widget/LinearLayout;->scrollTo(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->postInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->notifyFlingFinished:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->notifyFlingFinished:Z

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->flingListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnFlingListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->flingListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnFlingListener;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnFlingListener;->onFlingFinished(II)V

    goto :goto_0
.end method

.method public fling()Z
    .locals 3

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    neg-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->rightScrollMargin:I

    add-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->fling(II)Z

    move-result v0

    return v0
.end method

.method public flingTo(I)Z
    .locals 6
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    if-nez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v1

    sub-int v0, p1, v1

    iput-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->isScrollMode:Z

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v5, v1, v2, v0, v3}, Landroid/widget/Scroller;->startScroll(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->invalidate()V

    move v3, v4

    goto :goto_0
.end method

.method public getFlingDirection()Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getVelocityX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getFlingDirection(I)Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    move-result-object v0

    return-object v0
.end method

.method public isScrollable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->isScrollMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->stopScroll()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->start(Landroid/view/MotionEvent;)V

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->addMotionEvent(Landroid/view/MotionEvent;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->detect()Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->HORIZONTAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->preX:I

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->reset()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->rightScrollMargin:I

    add-int/2addr v2, v1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getWidth()I

    move-result v3

    if-le v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    neg-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v3

    invoke-super {p0, v2, v3}, Landroid/widget/LinearLayout;->scrollTo(II)V

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;->onScrollChanged(IIII)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->HORIZONTAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionIntent:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v1, v2, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollInTouch(Landroid/view/MotionEvent;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->addMotionEvent(Landroid/view/MotionEvent;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->reset()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollable:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->rightScrollMargin:I

    add-int v0, v1, v2

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    neg-int v1, v1

    if-ge p1, v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    neg-int p1, v1

    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->scrollTo(II)V

    goto :goto_0

    :cond_2
    if-le p1, v0, :cond_1

    move p1, v0

    goto :goto_1
.end method

.method public setLeftScrollMargin(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->leftScrollMargin:I

    neg-int v0, p1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/LinearLayout;->scrollTo(II)V

    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    return-void
.end method

.method public setRightScrollMargin(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->rightScrollMargin:I

    return-void
.end method
