.class public Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;
.super Ljava/lang/Object;
.source "MotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;
    }
.end annotation


# static fields
.field private static latestEvent:Landroid/view/MotionEvent;

.field private static startPoint:Landroid/view/MotionEvent;


# instance fields
.field private final maxMovementInClickingPx:I

.field private final scale:F

.field private velocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->scale:F

    const/high16 v0, 0x41200000

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->scale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->maxMovementInClickingPx:I

    return-void
.end method


# virtual methods
.method public addMotionEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->latestEvent:Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public detect()Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;
    .locals 7

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->startPoint:Landroid/view/MotionEvent;

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->latestEvent:Landroid/view/MotionEvent;

    if-nez v3, :cond_0

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    :goto_0
    return-object v2

    :cond_0
    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->startPoint:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->latestEvent:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->startPoint:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->latestEvent:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->maxMovementInClickingPx:I

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->maxMovementInClickingPx:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gez v3, :cond_1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    goto :goto_0

    :cond_1
    float-to-double v3, v0

    const-wide/high16 v5, 0x3ff8000000000000L

    mul-double/2addr v3, v5

    float-to-double v5, v1

    cmpl-double v3, v3, v5

    if-lez v3, :cond_2

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->HORIZONTAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->VIRTICAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    goto :goto_0
.end method

.method public getVelocityTracker()Landroid/view/VelocityTracker;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method public start(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->reset()V

    :cond_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->startPoint:Landroid/view/MotionEvent;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->velocityTracker:Landroid/view/VelocityTracker;

    return-void
.end method
