.class Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$1;
.super Ljava/lang/Object;
.source "ScrollPageView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->setTotalPages(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$1;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$1;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getPageNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(I)V

    :cond_0
    return-void
.end method
