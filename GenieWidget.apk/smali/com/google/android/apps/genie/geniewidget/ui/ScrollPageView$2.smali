.class Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;
.super Ljava/lang/Object;
.source "ScrollPageView.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # invokes: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->preparePage(I)V
    invoke-static {v3, v5}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$000(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$100(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$100(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$200(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-interface {v4, v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;->onPageShown(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    rem-int v3, p1, v1

    if-nez v3, :cond_0

    div-int v0, p1, v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$200(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    if-lez v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$200(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # invokes: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->preparePage(I)V
    invoke-static {v3, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$000(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$100(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$100(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;->this$0:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->access$200(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-interface {v4, v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;->onPageShown(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)V

    goto :goto_0
.end method
