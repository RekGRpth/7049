.class public Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;
.super Landroid/widget/FrameLayout;
.source "ScrollPageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;,
        Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;
    }
.end annotation


# instance fields
.field private curPageNumber:I

.field private firstShownPageNumber:I

.field private focusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private isPageFirstShown:Z

.field private pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

.field private pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

.field private pageProvider:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

.field private final pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/ui/PageView;",
            ">;"
        }
    .end annotation
.end field

.field private scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->isPageFirstShown:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->preparePage(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    return-object v0
.end method

.method private fingToPage(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->curPageNumber:I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    mul-int v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->flingTo(I)Z

    return-void
.end method

.method private getLeftPageNum(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v0

    add-int/lit8 v1, p1, -0x1

    div-int/2addr v1, v0

    return v1
.end method

.method private getRightPageNum(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v1

    div-int v1, p1, v1

    add-int/lit8 v0, v1, 0x1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method private preparePage(I)V
    .locals 6
    .param p1    # I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-int/lit8 v0, p1, -0x1

    add-int/lit8 v1, p1, 0x1

    if-ltz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildCount()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageProvider:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-interface {v5, v4}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;->requestPage(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)Z

    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->removePage(I)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildCount()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageProvider:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-interface {v5, v4}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;->requestPage(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)Z

    add-int/lit8 v4, v0, -0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->removePage(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->requestLayout()V

    return-void
.end method

.method private removePage(I)V
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->removeAllViews()V

    :cond_0
    return-void
.end method

.method private scrollToPage(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v2, p1

    div-int v1, v2, v0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(IZ)V

    return-void
.end method

.method private showPage(IZ)V
    .locals 6
    .param p1    # I
    .param p2    # Z

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->curPageNumber:I

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildCount()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageProvider:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-interface {v4, v3}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;->requestPage(Lcom/google/android/apps/genie/geniewidget/ui/PageView;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    mul-int v4, p1, v0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->flingTo(I)Z

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->isPageFirstShown:Z

    if-eqz v3, :cond_1

    if-nez p1, :cond_1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->isPageFirstShown:Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    invoke-interface {v3, v1, v2, v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;->onScrollChanged(IIII)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    mul-int v4, p1, v0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollTo(II)V

    goto :goto_0

    :cond_3
    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->firstShownPageNumber:I

    goto :goto_0
.end method


# virtual methods
.method public getCurrentPage()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->curPageNumber:I

    return v0
.end method

.method public getPage(I)Lcom/google/android/apps/genie/geniewidget/ui/PageView;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    const v0, 0x7f0a002d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setAlwaysDrawnWithCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setAnimationCacheEnabled(Z)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$2;-><init>(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->scrollListener:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setOnScrollListener(Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getFlingDirection()Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->NONE:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    if-ne v3, v0, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->scrollToPage(I)V

    :cond_0
    :goto_0
    const/4 v3, 0x0

    return v3

    :cond_1
    sget-object v3, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;->LEFT:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$FlingDirection;

    if-ne v3, v0, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getRightPageNum(I)I

    move-result v1

    :goto_1
    if-ltz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->fingToPage(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getLeftPageNum(I)I

    move-result v1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->layout(IIII)V

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->isPageFirstShown:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->isPageFirstShown:Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->firstShownPageNumber:I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getMeasuredWidth()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollY()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollTo(II)V

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->curPageNumber:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(I)V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/high16 v8, 0x40000000

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    if-ne v6, v8, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->setPageDimension(II)V

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    mul-int/2addr v7, v3

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v6, v7, p2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->measure(II)V

    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setPageEventObserver(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageEventObserver:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageEventObserver;

    return-void
.end method

.method public setPageProvider(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageProvider:Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$PageProvider;

    return-void
.end method

.method public setTotalPages(I)V
    .locals 5
    .param p1    # I

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView$1;-><init>(Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;)V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/ui/PageView;

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->setDescendantFocusability(I)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->setPageNumber(I)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pageContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->pages:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public showPage(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/ui/ScrollPageView;->showPage(IZ)V

    return-void
.end method
