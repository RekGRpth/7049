.class public Lcom/google/android/apps/genie/geniewidget/ui/TabView;
.super Landroid/widget/RelativeLayout;
.source "TabView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;
    }
.end annotation


# instance fields
.field private firstAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

.field private lastAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

.field private leftButtonBackground:Landroid/graphics/drawable/Drawable;

.field private leftScrollIndicator:Landroid/view/View;

.field private listTabs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/ui/Tab;",
            ">;"
        }
    .end annotation
.end field

.field private measuredChildWidth:I

.field private measuredWidth:I

.field private needScroll:Z

.field private rightButtonBackground:Landroid/graphics/drawable/Drawable;

.field private rightScrollIndicator:Landroid/view/View;

.field private selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

.field private tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

.field private tabIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tabSelectedListener:Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;

.field private tabs:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/ui/Tab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabs:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabIds:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->listTabs:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabs:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabIds:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->listTabs:Ljava/util/List;

    return-void
.end method

.method private calculateTabsPos()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-nez v1, :cond_0

    move v7, v5

    :goto_1
    add-int/lit8 v4, v0, -0x1

    if-ne v1, v4, :cond_1

    move v4, v5

    :goto_2
    invoke-virtual {v3, v2, v7, v4}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->calculatePosition(IZZ)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v7, v6

    goto :goto_1

    :cond_1
    move v4, v6

    goto :goto_2

    :cond_2
    return-void
.end method

.method public static createTabView(Landroid/content/Context;)Lcom/google/android/apps/genie/geniewidget/ui/TabView;
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030012

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    return-object v0
.end method

.method private doScrolling(I)V
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    neg-int v2, p1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->scrollBy(II)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->invalidate()V

    return-void
.end method

.method private getMovementForShowingInViewPort(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)I
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v1, v2, v4

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getRightPos()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getLeftPos()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getLeftPos()I

    move-result v5

    add-int v3, v4, v5

    sub-int v0, v1, v3

    if-gez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isLast()Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, -0x5

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x5

    goto :goto_0
.end method

.method private initChildViews()V
    .locals 3

    const/4 v2, 0x5

    const v0, 0x7f0a0039

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    const v0, 0x7f0a003b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftButtonBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightButtonBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setOnScrollListener(Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout$OnScrollListener;)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setLeftScrollMargin(I)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->setRightScrollMargin(I)V

    return-void
.end method

.method private scrollToTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getMovementForShowingInViewPort(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->doScrolling(I)V

    :cond_0
    return-void
.end method

.method private showIndicator()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x4

    iget-boolean v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->needScroll:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getScrollX()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->bringChildToFront(Landroid/view/View;)V

    :cond_1
    :goto_0
    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->measuredChildWidth:I

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->measuredWidth:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    if-lez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->bringChildToFront(Landroid/view/View;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->leftScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightScrollIndicator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public addTab(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/ui/Tab;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->initChildViews()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setIndex(I)V

    invoke-virtual {v0, p2}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setExtra(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabs:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabIds:Ljava/util/HashSet;

    invoke-virtual {v1, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->listTabs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->firstAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->firstAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    :goto_0
    return-object v0

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->lastAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    goto :goto_0
.end method

.method public getTab(I)Lcom/google/android/apps/genie/geniewidget/ui/Tab;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->listTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    return-object v0
.end method

.method public getTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getTabIds()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabIds:Ljava/util/HashSet;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabSelectedListener:Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabSelectedListener:Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;

    invoke-interface {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;->onTabSelected(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->setSelectedTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->scrollToTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->needScroll:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->fling()Z

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->isScrollable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->needScroll:Z

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->needScroll:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->showIndicator()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->calculateTabsPos()V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->scrollToTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->showIndicator()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->lastAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->lastAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->rightButtonBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->measuredWidth:I

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabContainer:Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/ui/FlingableLinearLayout;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->measuredChildWidth:I

    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->showIndicator()V

    return-void
.end method

.method public selectDefaultTab()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->simulateClick(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->firstAddedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    goto :goto_0
.end method

.method public setSelectedTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setSelected(Z)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->setSelected(Z)V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->scrollToTab(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    return-void
.end method

.method public setTabViewListener(Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->tabSelectedListener:Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;

    return-void
.end method

.method public simulateClick(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->onClick(Landroid/view/View;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->listTabs:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " selected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectedTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
