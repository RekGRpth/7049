.class Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;
.super Ljava/lang/Object;
.source "GenieLocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

.field final synthetic val$locationManager:Landroid/location/LocationManager;

.field final synthetic val$timeoutTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Landroid/location/LocationManager;Ljava/util/Timer;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$locationManager:Landroid/location/LocationManager;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$timeoutTimer:Ljava/util/Timer;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6
    .param p1    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$locationManager:Landroid/location/LocationManager;

    invoke-virtual {v1, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$timeoutTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;-><init>(DDF)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    invoke-interface {v1, v0}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->success(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "No location or location request timed out."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->val$callback:Lcom/google/android/apps/genie/geniewidget/utils/Callback;

    instance-of v1, v1, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    # getter for: Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->access$100(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->start()V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
