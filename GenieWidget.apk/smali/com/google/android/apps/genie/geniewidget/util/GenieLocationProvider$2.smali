.class Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;
.super Ljava/util/TimerTask;
.source "GenieLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

.field final synthetic val$listener:Landroid/location/LocationListener;

.field final synthetic val$locationManager:Landroid/location/LocationManager;

.field final synthetic val$provider:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Landroid/location/LocationListener;Landroid/location/LocationManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->this$0:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$listener:Landroid/location/LocationListener;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$locationManager:Landroid/location/LocationManager;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$provider:Ljava/lang/String;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$listener:Landroid/location/LocationListener;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$locationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;->val$provider:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method
