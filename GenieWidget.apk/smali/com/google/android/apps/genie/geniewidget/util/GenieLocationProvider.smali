.class public Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
.super Ljava/lang/Object;
.source "GenieLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;
    }
.end annotation


# instance fields
.field private final ctx:Landroid/content/Context;

.field private final locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;-><init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/utils/Callback;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    return-object v0
.end method

.method private getLocationManager()Landroid/location/LocationManager;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private getLocationProvider(Landroid/location/LocationManager;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/location/LocationManager;

    const-string v0, "network"

    return-object v0
.end method

.method private requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;
    .locals 5
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;J",
            "Ljava/lang/String;",
            ")",
            "Landroid/location/LocationListener;"
        }
    .end annotation

    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationManager()Landroid/location/LocationManager;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$1;-><init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Landroid/location/LocationManager;Ljava/util/Timer;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    if-eqz v1, :cond_0

    if-nez p4, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    new-instance v3, Ljava/lang/Exception;

    const-string v4, "No location or location request timed out."

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    instance-of v3, p1, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->start()V

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    const-wide/16 v3, 0x0

    cmp-long v3, p2, v3

    if-lez v3, :cond_3

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;

    invoke-direct {v3, p0, v0, v1, p4}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$2;-><init>(Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;Landroid/location/LocationListener;Landroid/location/LocationManager;Ljava/lang/String;)V

    invoke-virtual {v2, v3, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_3
    invoke-direct {p0, v1, v0, p1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestLocationUpdates(Landroid/location/LocationManager;Landroid/location/LocationListener;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    goto :goto_0
.end method

.method private requestLocationUpdates(Landroid/location/LocationManager;Landroid/location/LocationListener;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 8
    .param p1    # Landroid/location/LocationManager;
    .param p2    # Landroid/location/LocationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/LocationManager;",
            "Landroid/location/LocationListener;",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "Genie"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Genie"

    const-string v1, "no network location provider, can\'t get a location"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "no network location provider, can\'t get a location"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lcom/google/android/apps/genie/geniewidget/utils/Callback;->failure(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public getLastKnownLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
    .locals 12

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->ctx:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/util/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationManager()Landroid/location/LocationManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v11

    const/4 v6, 0x0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v8

    if-eqz v8, :cond_2

    if-eqz v6, :cond_3

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v8}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    :cond_3
    move-object v6, v8

    goto :goto_1

    :cond_4
    if-eqz v6, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v6}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;-><init>(DDF)V

    goto :goto_0
.end method

.method public requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;J)V
    .locals 2
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->getLocationProvider(Landroid/location/LocationManager;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->requestCurrentLocation(Lcom/google/android/apps/genie/geniewidget/utils/Callback;JLjava/lang/String;)Landroid/location/LocationListener;

    return-void
.end method

.method public stopMonitor()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider;->locationMonitor:Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/util/GenieLocationProvider$LocationMonitor;->stop()V

    :cond_0
    return-void
.end method
