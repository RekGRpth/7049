.class Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;
.super Ljava/util/LinkedHashMap;
.source "CachedUrlFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

.field final synthetic val$maxSize:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;->val$maxSize:I

    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;->val$maxSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
