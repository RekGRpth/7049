.class Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$UrlInputStreamSupplier;
.super Ljava/lang/Object;
.source "CachedUrlFetcher.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UrlInputStreamSupplier"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$UrlInputStreamSupplier;-><init>()V

    return-void
.end method


# virtual methods
.method public getInputStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
