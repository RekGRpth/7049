.class Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;
.super Ljava/util/LinkedHashMap;
.source "FileCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/utils/FileCache;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

.field final synthetic val$maxSize:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/FileCache;IFZI)V
    .locals 0
    .param p2    # I
    .param p3    # F
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/FileCache;

    iput p5, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;->val$maxSize:I

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;->val$maxSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
