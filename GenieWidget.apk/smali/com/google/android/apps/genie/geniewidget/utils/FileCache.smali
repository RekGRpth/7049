.class public Lcom/google/android/apps/genie/geniewidget/utils/FileCache;
.super Ljava/lang/Object;
.source "FileCache.java"


# instance fields
.field private final fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

.field private index:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;I)V
    .locals 6
    .param p1    # Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;

    const/high16 v3, 0x3f800000

    const/4 v4, 0x1

    move-object v1, p0

    move v2, p2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache$1;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/FileCache;IFZI)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->loadIndex()V

    return-void
.end method

.method private loadIndex()V
    .locals 11

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    const-string v9, "index.txt"

    invoke-interface {v8, v9}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->exists(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    const-string v9, "index.txt"

    invoke-interface {v8, v9}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->read(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([B)V

    const-string v8, ","

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    array-length v7, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_0

    aget-object v4, v0, v5

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v8, v4, v9}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    const-string v9, "Genie"

    const-string v10, "Couldn\'t read filecache index."

    invoke-interface {v8, v9, v10, v2}, Lcom/google/android/apps/genie/geniewidget/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private saveIndex()V
    .locals 9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    const-string v7, "index.txt"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->write(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    const-string v7, "Genie"

    const-string v8, "Couldn\'t write filecache index."

    invoke-interface {v6, v7, v8, v0}, Lcom/google/android/apps/genie/geniewidget/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized commit()V
    .locals 9

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->saveIndex()V

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    invoke-interface {v7}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->listFiles()[Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, v3

    :try_start_1
    array-length v6, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v2, v0, v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "index.txt"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    add-int/lit8 v1, v1, 0x1

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->delete(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public declared-synchronized getFile(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->has(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    invoke-interface {v0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->getFile(Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized has(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Ljava/lang/String;[B)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->write(Ljava/lang/String;[B)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    const-string v2, "Genie"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t write entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/genie/geniewidget/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->index:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items in index, and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/FileCache;->fileSystem:Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;

    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " files on disk."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
