.class public interface abstract Lcom/google/android/apps/genie/geniewidget/utils/FileSystemProvider;
.super Ljava/lang/Object;
.source "FileSystemProvider.java"


# virtual methods
.method public abstract delete(Ljava/lang/String;)Z
.end method

.method public abstract exists(Ljava/lang/String;)Z
.end method

.method public abstract getFile(Ljava/lang/String;)Ljava/io/File;
.end method

.method public abstract listFiles()[Ljava/io/File;
.end method

.method public abstract read(Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract write(Ljava/lang/String;[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
