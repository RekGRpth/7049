.class public Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;
.super Ljava/lang/Object;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/utils/Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogFactory"
.end annotation


# static fields
.field public static TEST_LOGS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->TEST_LOGS:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->TEST_LOGS:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/Logger$TestLogger;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$TestLogger;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/Logger$DeviceLogger;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$DeviceLogger;-><init>()V

    goto :goto_0
.end method
