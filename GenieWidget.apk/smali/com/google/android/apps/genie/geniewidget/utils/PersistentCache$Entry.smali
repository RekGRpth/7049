.class public Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;
.super Ljava/lang/Object;
.source "PersistentCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public final data:[B

.field public final timeStamp:J


# direct methods
.method public constructor <init>([B)V
    .locals 2
    .param p1    # [B

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([BJ)V

    return-void
.end method

.method public constructor <init>([BJ)V
    .locals 0
    .param p1    # [B
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    iput-wide p2, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->timeStamp:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0
.end method
