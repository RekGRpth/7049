.class public Lcom/google/android/apps/genie/geniewidget/utils/TemperatureConverter;
.super Ljava/lang/Object;
.source "TemperatureConverter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static celsiusToFahrenheit(I)I
    .locals 4
    .param p0    # I

    const-wide v0, 0x3ffccccccccccccdL

    int-to-double v2, p0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4040000000000000L

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
