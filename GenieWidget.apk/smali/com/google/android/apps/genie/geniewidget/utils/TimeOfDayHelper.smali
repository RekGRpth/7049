.class public Lcom/google/android/apps/genie/geniewidget/utils/TimeOfDayHelper;
.super Ljava/lang/Object;
.source "TimeOfDayHelper.java"


# static fields
.field private static final log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/utils/Logger$LogFactory;->getLogger()Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/utils/TimeOfDayHelper;->log:Lcom/google/android/apps/genie/geniewidget/utils/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDayOfWeek(Ljava/util/Calendar;J)I
    .locals 1
    .param p0    # Ljava/util/Calendar;
    .param p1    # J

    invoke-virtual {p0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public static getDayOfWeekName(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, p0

    return-object v1
.end method
