.class public Lcom/google/android/apps/genie/geniewidget/view/CurveView;
.super Landroid/view/View;
.source "CurveView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;
    }
.end annotation


# static fields
.field private static final CURVE_DASH_INTERVALS:[F


# instance fields
.field private context:Landroid/content/Context;

.field private currentTime:J

.field private endTime:J

.field private height:I

.field private knewMotionIntent:Z

.field listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

.field private motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

.field private precipitation:Landroid/graphics/Path;

.field precipitationData:[F

.field private startTime:J

.field private sunriseHour:I

.field private sunriseMin:I

.field private sunsetHour:I

.field private sunsetMin:I

.field private tempHigh:I

.field private tempLow:I

.field private temperature:Landroid/graphics/Path;

.field temperatureData:[F

.field private topPrecipitation:F

.field private topTemp:F

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x40800000
        0x40800000
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;[F[FJJLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;
    .param p3    # [F
    .param p4    # [F
    .param p5    # J
    .param p7    # J
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;

    const/16 v4, 0x3a

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->context:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setClickable(Z)V

    iput-wide p5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    iput-wide p7, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    invoke-virtual {p9, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p9, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseHour:I

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p9, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseMin:I

    invoke-virtual {p10, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p10, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetHour:I

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p10, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetMin:I

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-direct {v1, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    return-void
.end method

.method private drawGradient(Landroid/graphics/Canvas;F[I[I)V
    .locals 21
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # F
    .param p3    # [I
    .param p4    # [I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    new-instance v5, Landroid/graphics/drawable/GradientDrawable;

    sget-object v17, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v5, v0, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    new-instance v9, Landroid/graphics/drawable/GradientDrawable;

    sget-object v17, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-direct {v9, v0, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    new-instance v14, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-direct {v14, v0, v1}, Ljava/util/Date;-><init>(J)V

    new-instance v7, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14}, Ljava/util/Date;->getHours()I

    move-result v17

    mul-int/lit8 v17, v17, 0x3c

    invoke-virtual {v14}, Ljava/util/Date;->getMinutes()I

    move-result v18

    add-int v13, v17, v18

    invoke-virtual {v7}, Ljava/util/Date;->getHours()I

    move-result v17

    mul-int/lit8 v17, v17, 0x3c

    invoke-virtual {v7}, Ljava/util/Date;->getMinutes()I

    move-result v18

    add-int v6, v17, v18

    if-ge v6, v13, :cond_0

    add-int/lit16 v6, v6, 0x5a0

    :cond_0
    sub-int v12, v6, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseHour:I

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x3c

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x5a0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseMin:I

    move/from16 v18, v0

    add-int v15, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetHour:I

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x3c

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x5a0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetMin:I

    move/from16 v18, v0

    add-int v16, v17, v18

    :goto_0
    if-le v15, v13, :cond_1

    add-int/lit16 v15, v15, -0x5a0

    goto :goto_0

    :cond_1
    :goto_1
    move/from16 v0, v16

    if-le v0, v13, :cond_2

    move/from16 v0, v16

    add-int/lit16 v0, v0, -0x5a0

    move/from16 v16, v0

    goto :goto_1

    :cond_2
    :goto_2
    if-ge v15, v6, :cond_4

    move/from16 v0, v16

    if-ge v0, v6, :cond_4

    move/from16 v0, v16

    if-ge v15, v0, :cond_3

    move-object v4, v9

    sub-int v17, v16, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v12

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v8, v17, v18

    add-int/lit16 v15, v15, 0x5a0

    sub-int v17, v15, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v12

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v11, v17, v18

    :goto_3
    float-to-double v0, v8

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->floor(D)D

    move-result-wide v17

    move-wide/from16 v0, v17

    double-to-int v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x41f00000

    add-float v18, v18, p2

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    float-to-double v0, v11

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1e

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    :cond_3
    move-object v4, v5

    sub-int v17, v15, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v12

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v8, v17, v18

    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x5a0

    move/from16 v16, v0

    sub-int v17, v16, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v12

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v11, v17, v18

    goto :goto_3

    :cond_4
    return-void
.end method

.method private formatPrecipitation([F)[F
    .locals 5
    .param p1    # [F

    const/high16 v4, 0x42c80000

    array-length v2, p1

    new-array v1, v2, [F

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget v2, p1, v0

    sub-float v2, v4, v2

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private formatTemperature([F)[F
    .locals 8
    .param p1    # [F

    const/4 v6, 0x0

    const/high16 v7, 0x40a00000

    array-length v5, p1

    new-array v4, v5, [F

    aget v1, p1, v6

    aget v2, p1, v6

    const/4 v0, 0x1

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    aget v5, p1, v0

    cmpl-float v5, v5, v1

    if-lez v5, :cond_0

    aget v1, p1, v0

    :cond_0
    aget v5, p1, v0

    cmpg-float v5, v5, v2

    if-gez v5, :cond_1

    aget v2, p1, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    div-float v5, v1, v7

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/lit8 v5, v5, 0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    div-float v5, v2, v7

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/lit8 v5, v5, 0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    if-ne v5, v6, :cond_3

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    add-int/lit8 v5, v5, -0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    :cond_3
    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    sub-int/2addr v5, v6

    int-to-float v3, v5

    const/4 v0, 0x0

    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_4

    aget v5, p1, v0

    sub-float v5, v3, v5

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v3

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    return-object v4
.end method

.method private getCurve([F[F[F[F)Landroid/graphics/Path;
    .locals 11
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F
    .param p4    # [F

    const/high16 v10, -0x40000000

    const/4 v2, 0x0

    const/high16 v9, 0x40000000

    const/high16 v8, 0x41f00000

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    aget v1, p2, v2

    add-float/2addr v1, v8

    invoke-virtual {v0, v10, v1}, Landroid/graphics/Path;->moveTo(FF)V

    aget v1, p1, v2

    aget v2, p2, v2

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v7, 0x0

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v7, v1, :cond_0

    aget v1, p1, v7

    aget v2, p3, v7

    add-float/2addr v1, v2

    aget v2, p2, v7

    aget v3, p4, v7

    add-float/2addr v2, v3

    add-float/2addr v2, v8

    add-int/lit8 v3, v7, 0x1

    aget v3, p1, v3

    add-int/lit8 v4, v7, 0x1

    aget v4, p3, v4

    sub-float/2addr v3, v4

    add-int/lit8 v4, v7, 0x1

    aget v4, p2, v4

    add-int/lit8 v5, v7, 0x1

    aget v5, p4, v5

    sub-float/2addr v4, v5

    add-float/2addr v4, v8

    add-int/lit8 v5, v7, 0x1

    aget v5, p1, v5

    add-int/lit8 v6, v7, 0x1

    aget v6, p2, v6

    add-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    aget v2, p2, v2

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v2, v2

    add-float/2addr v2, v9

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    add-float/2addr v1, v8

    invoke-virtual {v0, v10, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    return-object v0
.end method

.method private getTopCPoint([F[F[F[F)F
    .locals 4
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F
    .param p4    # [F

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v1, v2

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    aget v2, p2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aget v2, p2, v0

    aget v3, p4, v0

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-int/lit8 v2, v0, 0x1

    aget v2, p2, v2

    aget v3, p4, v0

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget v2, p2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    return v1
.end method

.method private initCPoints([F[F[F[F)V
    .locals 10
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F
    .param p4    # [F

    array-length v3, p1

    add-int/lit8 v6, v3, -0x1

    new-array v4, v6, [F

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_0

    add-int/lit8 v6, v2, 0x1

    aget v6, p2, v6

    aget v7, p2, v2

    sub-float/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget v7, p1, v7

    aget v8, p1, v2

    sub-float/2addr v7, v8

    div-float/2addr v6, v7

    aput v6, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget v7, v4, v7

    aput v7, p4, v6

    const/4 v2, 0x1

    :goto_1
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_1

    const/high16 v6, 0x3f000000

    add-int/lit8 v7, v2, -0x1

    aget v7, v4, v7

    aget v8, v4, v2

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    aput v6, p4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v6, v3, -0x1

    add-int/lit8 v7, v3, -0x2

    aget v7, v4, v7

    aput v7, p4, v6

    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_5

    aget v6, v4, v2

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_3

    const/4 v6, 0x0

    aput v6, p4, v2

    add-int/lit8 v6, v2, 0x1

    const/4 v7, 0x0

    aput v7, p4, v6

    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v6, p4, v2

    aget v7, v4, v2

    div-float v0, v6, v7

    add-int/lit8 v6, v2, 0x1

    aget v6, p4, v6

    aget v7, v4, v2

    div-float v1, v6, v7

    const/4 v6, 0x0

    cmpl-float v6, v0, v6

    if-ltz v6, :cond_4

    const/4 v6, 0x0

    cmpl-float v6, v1, v6

    if-ltz v6, :cond_4

    const-wide/high16 v6, 0x4008000000000000L

    mul-float v8, v0, v0

    mul-float v9, v1, v1

    add-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    double-to-float v5, v6

    const/high16 v6, 0x3f800000

    cmpg-float v6, v5, v6

    if-gez v6, :cond_2

    aget v6, p4, v2

    mul-float/2addr v6, v5

    aput v6, p4, v2

    add-int/lit8 v6, v2, 0x1

    aget v7, p4, v6

    mul-float/2addr v7, v5

    aput v7, p4, v6

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    aput v6, p4, v2

    add-int/lit8 v6, v2, 0x1

    const/4 v7, 0x0

    aput v7, p4, v6

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_6

    const v6, 0x3eaaaaab

    add-int/lit8 v7, v2, 0x1

    aget v7, p1, v7

    aget v8, p1, v2

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    aput v6, p3, v2

    aget v6, p4, v2

    aget v7, p3, v2

    mul-float/2addr v6, v7

    aput v6, p4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    add-int/lit8 v6, v3, -0x1

    const v7, 0x3eaaaaab

    add-int/lit8 v8, v3, -0x1

    aget v8, p1, v8

    add-int/lit8 v9, v3, -0x2

    aget v9, p1, v9

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    aput v7, p3, v6

    add-int/lit8 v6, v3, -0x1

    const v7, 0x3eaaaaab

    add-int/lit8 v8, v3, -0x1

    aget v8, p2, v8

    add-int/lit8 v9, v3, -0x2

    aget v9, p2, v9

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    aput v7, p4, v6

    return-void
.end method

.method private initPoints([F[F[F)V
    .locals 4
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F

    array-length v2, p1

    add-int/lit8 v0, v2, -0x1

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v3, v0

    div-float/2addr v2, v3

    aput v2, p2, v1

    aget v2, p1, v1

    aput v2, p3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    aput v2, p2, v0

    aget v2, p1, v0

    aput v2, p3, v0

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 33
    .param p1    # Landroid/graphics/Canvas;

    new-instance v32, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v6, v6, 0x1e

    move-object/from16 v0, v32

    invoke-direct {v0, v2, v3, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v20

    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topTemp:F

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->drawGradient(Landroid/graphics/Canvas;F[I[I)V

    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topPrecipitation:F

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_2

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->drawGradient(Landroid/graphics/Canvas;F[I[I)V

    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v2, 0x40000000

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    new-instance v2, Landroid/graphics/DashPathEffect;

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    array-length v5, v5

    int-to-float v5, v5

    invoke-direct {v2, v3, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v8, v8, 0x1e

    add-int/lit8 v8, v8, 0x32

    add-int/lit8 v8, v8, -0x1

    invoke-direct {v2, v3, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    const/high16 v2, 0x3f800000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v2, 0x41400000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    const-string v2, "Droid Sans"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/16 v2, 0x5e

    const/16 v3, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-virtual {v7, v2, v3, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    const/16 v18, 0x0

    :goto_0
    const/16 v2, 0xa

    move/from16 v0, v18

    if-gt v0, v2, :cond_1

    const/high16 v2, 0x41f00000

    move/from16 v0, v18

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v5, v5

    mul-float/2addr v3, v5

    const/high16 v5, 0x41200000

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float v4, v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    rem-int/lit8 v2, v18, 0x5

    if-nez v2, :cond_0

    const/16 v2, 0xa

    :goto_1
    sub-int v2, v3, v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v2, p1

    move v6, v4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x5

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    sub-int/2addr v2, v3

    div-int/lit8 v22, v2, 0x5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    div-int/lit8 v2, v2, 0x5

    rem-int/lit8 v14, v2, 0x2

    const/16 v18, 0x0

    :goto_2
    move/from16 v0, v18

    move/from16 v1, v22

    if-gt v0, v1, :cond_3

    const/high16 v2, 0x41f00000

    move/from16 v0, v18

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v3, v5

    move/from16 v0, v22

    int-to-float v5, v0

    div-float/2addr v3, v5

    add-float v4, v2, v3

    const/4 v3, 0x0

    add-int v2, v14, v18

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_2

    const/high16 v5, 0x41200000

    :goto_3
    move-object/from16 v2, p1

    move v6, v4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    :cond_2
    const/high16 v5, 0x40a00000

    goto :goto_3

    :cond_3
    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const-string v2, "100%"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0xc

    int-to-float v3, v3

    const/high16 v5, 0x42040000

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const-string v2, "0%"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0xc

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, 0x1e

    add-int/lit8 v5, v5, 0x3

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x41400000

    const/high16 v5, 0x42040000

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x41400000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, 0x1e

    add-int/lit8 v5, v5, 0x3

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long v23, v2, v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    add-int/lit8 v2, v2, 0x32

    add-int/lit8 v15, v2, -0x1

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v25, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    move-object/from16 v0, v25

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/Date;->setMinutes(I)V

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/Date;->setSeconds(I)V

    invoke-virtual/range {v25 .. v25}, Ljava/util/Date;->getTime()J

    move-result-wide v26

    :goto_4
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    cmp-long v2, v26, v2

    if-gez v2, :cond_4

    const-wide/32 v2, 0x36ee80

    add-long v26, v26, v2

    goto :goto_4

    :cond_4
    move-wide/from16 v18, v26

    :goto_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    cmp-long v2, v18, v2

    if-gez v2, :cond_b

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long v2, v18, v2

    long-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move-wide/from16 v0, v23

    long-to-float v3, v0

    div-float v9, v2, v3

    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->getHours()I

    move-result v17

    rem-int/lit8 v2, v17, 0x3

    if-nez v2, :cond_6

    const/16 v2, 0xa

    :goto_6
    sub-int v2, v15, v2

    int-to-float v10, v2

    int-to-float v12, v15

    move-object/from16 v8, p1

    move v11, v9

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    rem-int/lit8 v2, v17, 0x6

    if-nez v2, :cond_5

    const-string v28, ""

    div-int/lit8 v2, v17, 0x6

    packed-switch v2, :pswitch_data_0

    :goto_7
    add-int/lit8 v2, v15, -0x14

    int-to-float v2, v2

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v9, v2, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    const-wide/32 v2, 0x36ee80

    add-long v18, v18, v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x5

    goto :goto_6

    :pswitch_0
    if-eqz v20, :cond_7

    const-string v28, "0"

    :goto_8
    goto :goto_7

    :cond_7
    const-string v28, "12 AM"

    goto :goto_8

    :pswitch_1
    if-eqz v20, :cond_8

    const-string v28, "6"

    :goto_9
    goto :goto_7

    :cond_8
    const-string v28, "6 AM"

    goto :goto_9

    :pswitch_2
    if-eqz v20, :cond_9

    const-string v28, "12"

    :goto_a
    goto :goto_7

    :cond_9
    const-string v28, "12 PM"

    goto :goto_a

    :pswitch_3
    if-eqz v20, :cond_a

    const-string v28, "18"

    :goto_b
    goto :goto_7

    :cond_a
    const-string v28, "6 PM"

    goto :goto_b

    :cond_b
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    const/high16 v2, 0x3f800000

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v2, 0x41400000

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    const-string v2, "Droid Sans"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-virtual {v13, v2, v3, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v21

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v2, 0xb

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v30

    const/16 v2, 0xc

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v31

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long/2addr v2, v5

    long-to-float v2, v2

    move-wide/from16 v0, v23

    long-to-float v3, v0

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float v9, v2, v3

    const/4 v10, 0x0

    add-int/lit8 v2, v15, -0x28

    int-to-float v12, v2

    move-object/from16 v8, p1

    move v11, v9

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v2, v15, -0xa

    int-to-float v10, v2

    int-to-float v12, v15

    move-object/from16 v8, p1

    move v11, v9

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    if-eqz v20, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xa

    move/from16 v0, v31

    if-ge v0, v2, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    :goto_d
    move-object/from16 v0, v28

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v29

    const/high16 v2, 0x40000000

    div-float v2, v29, v2

    cmpg-float v2, v9, v2

    if-gez v2, :cond_11

    const/high16 v2, 0x40000000

    div-float v9, v29, v2

    :cond_c
    :goto_e
    add-int/lit8 v2, v15, -0x14

    int-to-float v2, v2

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v9, v2, v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void

    :cond_d
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_c

    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, v30, 0xb

    rem-int/lit8 v3, v3, 0xc

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xa

    move/from16 v0, v31

    if-ge v0, v2, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xc

    move/from16 v0, v30

    if-ge v0, v2, :cond_10

    const-string v2, " AM"

    :goto_10
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    goto :goto_d

    :cond_f
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_f

    :cond_10
    const-string v2, " PM"

    goto :goto_10

    :cond_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v29, v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float/2addr v2, v3

    cmpl-float v2, v9, v2

    if-lez v2, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v29, v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float v9, v2, v3

    goto :goto_e

    :array_0
    .array-data 4
        -0x1a0000fe
        -0x1a859dd8
    .end array-data

    :array_1
    .array-data 4
        -0x1a0636ff
        -0x1ac2ceec
    .end array-data

    :array_2
    .array-data 4
        -0x1a330301
        -0x1ab99754
    .end array-data

    :array_3
    .array-data 4
        -0x1a996907
        -0x1ad3d1c1
    .end array-data

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v8, p4, p2

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    sub-int v8, p5, p3

    add-int/lit8 v8, v8, -0x1e

    add-int/lit8 v8, v8, -0x32

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v2, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v3, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v0, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v1, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v6, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v7, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v4, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v5, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    invoke-direct {p0, v8}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->formatPrecipitation([F)[F

    move-result-object v8

    invoke-direct {p0, v8, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initPoints([F[F[F)V

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initCPoints([F[F[F[F)V

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getTopCPoint([F[F[F[F)F

    move-result v8

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topPrecipitation:F

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getCurve([F[F[F[F)Landroid/graphics/Path;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    invoke-direct {p0, v8}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->formatTemperature([F)[F

    move-result-object v8

    invoke-direct {p0, v8, v6, v7}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initPoints([F[F[F)V

    invoke-direct {p0, v6, v7, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initCPoints([F[F[F[F)V

    invoke-direct {p0, v6, v7, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getTopCPoint([F[F[F[F)F

    move-result v8

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topTemp:F

    invoke-direct {p0, v6, v7, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getCurve([F[F[F[F)Landroid/graphics/Path;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->start(Landroid/view/MotionEvent;)V

    iput-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    :goto_0
    iget-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    iget-wide v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long/2addr v4, v6

    long-to-float v2, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v2, v4

    float-to-long v4, v2

    iget-wide v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

    iget-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    invoke-interface {v2, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;->timeChanged(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->postInvalidate()V

    return v3

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->addMotionEvent(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->detect()Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-eq v1, v2, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->VIRTICAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1
.end method

.method public setCurrentTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->postInvalidate()V

    return-void
.end method
