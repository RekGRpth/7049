.class public Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;
.super Landroid/webkit/WebView;
.source "NewsContentView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/view/NewsContentView$NewsContentWebViewClient;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView$NewsContentWebViewClient;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView$NewsContentWebViewClient;-><init>(Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsContentView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0002

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    return-void
.end method
