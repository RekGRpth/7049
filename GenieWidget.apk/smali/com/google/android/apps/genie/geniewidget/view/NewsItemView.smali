.class public Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;
.super Landroid/widget/LinearLayout;
.source "NewsItemView.java"


# instance fields
.field private imageView:Landroid/widget/ImageView;

.field private index:I

.field private newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

.field private snippetView:Landroid/widget/TextView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static createNewsItemView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->refreshData(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->setEnabled(Z)V

    return-object v0
.end method

.method private getLocaleDateTime(J)Ljava/lang/String;
    .locals 7
    .param p1    # J

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v0, 0x5265c00

    sub-long v0, v2, v0

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    const-wide/32 v4, 0xea60

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x80008

    invoke-static {v6, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private refreshData(Lcom/google/android/apps/genie/geniewidget/items/NewsItem;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0027

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->snippetView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->snippetView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTimestamp()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getLocaleDateTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getThumbnailUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getThumbnailUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->updateImage(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->index:I

    return v0
.end method

.method public getNewsItemData()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    return-object v0
.end method

.method public setIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->index:I

    return-void
.end method

.method public updateImage(Landroid/net/Uri;)V
    .locals 5
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    const v1, 0x7f0a002b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->imageView:Landroid/widget/ImageView;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    const v1, 0x7f0a0029

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->titleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->titleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a002a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->snippetView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->snippetView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getSource()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->newsItem:Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTimestamp()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->getLocaleDateTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0025

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0028

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;->invalidate()V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Genie"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Image too large: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
