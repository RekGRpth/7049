.class public Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;
.super Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
.source "NewsSectionViewBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;",
        ">;"
    }
.end annotation


# instance fields
.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    return-void
.end method


# virtual methods
.method public buildView()Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->resourceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getNewsStories(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->clickListener:Landroid/view/View$OnClickListener;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->createNewsSectionView(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curViewTimestamp:J

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    return-object v1
.end method

.method public bridge synthetic buildView()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->buildView()Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curViewTimestamp:J

    return-void
.end method

.method protected isStale()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curViewTimestamp:J

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRequestViewShown(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetDataService()Lcom/google/android/apps/genie/geniewidget/services/DataService;

    move-result-object v0

    if-eqz p1, :cond_1

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->getClickedWidgetItemType()Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    move-result-object v4

    if-ne v3, v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->getClickedWidgetNewsItem()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getGuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->getNewsItemView(Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionViewBuilder;->curView:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/genie/geniewidget/view/NewsSectionView;->setSelectedNewsItem(Lcom/google/android/apps/genie/geniewidget/view/NewsItemView;)V

    :cond_0
    sget-object v3, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NONE:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->setClickedWidgetItemType(Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;I)V

    :cond_1
    return-void
.end method
