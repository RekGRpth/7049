.class public Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;
.super Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
.source "TabViewBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/ui/TabView;",
        ">;"
    }
.end annotation


# instance fields
.field private dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private tabsByTopicId:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/ui/Tab;",
            ">;"
        }
    .end annotation
.end field

.field private weatherTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->tabsByTopicId:Ljava/util/Hashtable;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetDataService()Lcom/google/android/apps/genie/geniewidget/services/DataService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    return-void
.end method


# virtual methods
.method protected buildFailedView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildView()Lcom/google/android/apps/genie/geniewidget/ui/TabView;
    .locals 9

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->hasData()Z

    move-result v5

    if-nez v5, :cond_0

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    const-wide/16 v5, -0x1

    iput-wide v5, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curViewTimestamp:J

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->createTabView(Landroid/content/Context;)Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder$1;-><init>(Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->setTabViewListener(Lcom/google/android/apps/genie/geniewidget/ui/TabView$TabSelectedListener;)V

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curViewTimestamp:J

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->context:Landroid/content/Context;

    const v5, 0x7f0b0030

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    invoke-virtual {v4, v3, v3}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->addTab(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->weatherTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getCurrentNewsTopics()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->tabsByTopicId:Ljava/util/Hashtable;

    iget-object v6, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    iget-object v7, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicName:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;->topicKey:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->addTab(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->tabsByTopicId:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->clear()V

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    goto :goto_0
.end method

.method public bridge synthetic buildView()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->buildView()Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    move-result-object v0

    return-object v0
.end method

.method protected isStale()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onRequestViewShown(Z)V
    .locals 6
    .param p1    # Z

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->getClickedWidgetItemType()Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->WEATHER:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    if-ne v3, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->weatherTab:Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->simulateClick(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    sget-object v4, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NONE:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->setClickedWidgetItemType(Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;I)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;->NEWS:Lcom/google/android/apps/genie/geniewidget/services/DataService$ClickedWidgetItemType;

    if-ne v3, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->dataService:Lcom/google/android/apps/genie/geniewidget/services/DataService;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/services/DataService;->getClickedWidgetNewsItem()Lcom/google/android/apps/genie/geniewidget/items/NewsItem;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->tabsByTopicId:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/NewsItem;->getTopicId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/ui/Tab;

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->simulateClick(Lcom/google/android/apps/genie/geniewidget/ui/Tab;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/TabViewBuilder;->curView:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/ui/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/ui/TabView;->selectDefaultTab()V

    goto :goto_0
.end method
