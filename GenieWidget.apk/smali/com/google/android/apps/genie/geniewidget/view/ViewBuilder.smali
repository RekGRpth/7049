.class public abstract Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
.super Ljava/lang/Object;
.source "ViewBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final clickListener:Landroid/view/View$OnClickListener;

.field protected context:Landroid/content/Context;

.field protected curView:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected curViewTimestamp:J

.field private pageNumber:I

.field protected resourceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->resourceName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->clickListener:Landroid/view/View$OnClickListener;

    iput p3, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->pageNumber:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/view/View$OnClickListener;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected buildFailedView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract buildView()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCurrentView()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->isStale()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->curView:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getPageNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;->pageNumber:I

    return v0
.end method

.method protected abstract isStale()Z
.end method

.method protected onRequestViewShown(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method
