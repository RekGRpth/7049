.class Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;
.super Ljava/lang/Object;
.source "WeatherView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->refreshWeatherData(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->access$000(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->access$002(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    # invokes: Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->switchOnOffHourlyView()V
    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->access$100(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
