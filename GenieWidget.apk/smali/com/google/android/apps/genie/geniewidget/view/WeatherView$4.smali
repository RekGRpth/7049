.class Lcom/google/android/apps/genie/geniewidget/view/WeatherView$4;
.super Ljava/lang/Object;
.source "WeatherView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->refreshWeatherData(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$4;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://www.google.com/m/gne/weather.com?gl="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$4;->this$0:Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    # getter for: Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->access$200(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
