.class public Lcom/google/android/apps/genie/geniewidget/view/WeatherView;
.super Landroid/widget/RelativeLayout;
.source "WeatherView.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;


# instance fields
.field condition:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private curWeatherView:Landroid/view/View;

.field private curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

.field private data:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

.field private final dayOfWeekMapping:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private forecastViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private forecastWeatherLayout:Landroid/view/ViewGroup;

.field private final genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private hourlyConditionLayout:Landroid/view/ViewGroup;

.field private hourlyData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;",
            ">;"
        }
    .end annotation
.end field

.field humidity:Landroid/widget/TextView;

.field private final postToUI:Landroid/os/Handler;

.field private final prefersCelsius:Z

.field rainProb:Landroid/widget/TextView;

.field private showingHourly:Z

.field tempView:Landroid/widget/TextView;

.field private todaysConditionLayout:Landroid/view/ViewGroup;

.field private viewContainer:Landroid/widget/LinearLayout;

.field wind:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->postToUI:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$1;-><init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->dayOfWeekMapping:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v0

    const v1, 0x7f0b0009

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/utils/LocaleUtils;->prefersCelsius(Ljava/util/Locale;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getBooleanPreference(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->prefersCelsius:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/WeatherView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    invoke-direct {p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->switchOnOffHourlyView()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    return-object v0
.end method

.method private createDivider()Landroid/view/View;
    .locals 5

    const/16 v4, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setMaxWidth(I)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v0, v2, v4, v2, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public static createWeatherView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)Lcom/google/android/apps/genie/geniewidget/view/WeatherView;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    const/4 v4, 0x1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030017

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->refreshWeatherData(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->setAnimationCacheEnabled(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->buildDrawingCache()V

    return-object v0
.end method

.method private getTextView(I)Landroid/widget/TextView;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private initializeHourlyView(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V
    .locals 17
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v4, v1, [F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v5, v1, [F

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->prefersCelsius:Z

    if-eqz v1, :cond_0

    invoke-virtual {v14}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v1

    int-to-float v1, v1

    aput v1, v4, v16

    :goto_1
    invoke-virtual {v14}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getProbPrecipitation()I

    move-result v1

    int-to-float v1, v1

    aput v1, v5, v16

    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v14}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/utils/TemperatureConverter;->celsiusToFahrenheit(I)I

    move-result v1

    int-to-float v1, v1

    aput v1, v4, v16

    goto :goto_1

    :cond_1
    const v1, 0x7f0a005c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v13

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v8

    invoke-virtual {v13}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunrise()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunset()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;-><init>(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;[F[FJJLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setMinimumWidth(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setMinimumHeight(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    invoke-virtual {v12, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method private localizeTemp(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->prefersCelsius:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&deg;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/genie/geniewidget/utils/TemperatureConverter;->celsiusToFahrenheit(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&deg;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private localizeWindSpeed(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;

    move-object v1, p1

    const-string v2, ""

    iget-boolean v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->prefersCelsius:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b0042

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b0041

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const v5, 0x3f1f122f

    mul-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b0043

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b0044

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private refreshWeatherData(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V
    .locals 48
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->data:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    if-nez v43, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v26

    const v43, 0x7f0a0060

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/LinearLayout;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    const v43, 0x7f030013

    const/16 v44, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v43

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$2;-><init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    move-object/from16 v43, v0

    const v44, 0x7f0a0041

    invoke-virtual/range {v43 .. v44}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/view/ViewGroup;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->todaysConditionLayout:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->todaysConditionLayout:Landroid/view/ViewGroup;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    move-object/from16 v43, v0

    const v44, 0x7f0a004a

    invoke-virtual/range {v43 .. v44}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/view/ViewGroup;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastWeatherLayout:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    move-object/from16 v43, v0

    const v44, 0x7f0a0050

    invoke-virtual/range {v43 .. v44}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/view/ViewGroup;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyConditionLayout:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyConditionLayout:Landroid/view/ViewGroup;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    move-object/from16 v44, v0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getForecasts()Ljava/util/List;

    move-result-object v43

    invoke-interface/range {v43 .. v43}, Ljava/util/List;->size()I

    move-result v40

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    move/from16 v1, v40

    if-ge v0, v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v43

    const v44, 0x7f030014

    const/16 v45, 0x0

    invoke-virtual/range {v43 .. v45}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastWeatherLayout:Landroid/view/ViewGroup;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v43, v40, -0x1

    move/from16 v0, v21

    move/from16 v1, v43

    if-ge v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastWeatherLayout:Landroid/view/ViewGroup;

    move-object/from16 v43, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->createDivider()Landroid/view/View;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    const v44, 0x7f0a003f

    invoke-virtual/range {v43 .. v44}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    new-instance v43, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$3;-><init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v35, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$4;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$4;-><init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    const v44, 0x7f0a0042

    invoke-virtual/range {v43 .. v44}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    const v44, 0x7f0a0051

    invoke-virtual/range {v43 .. v44}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v43, 0x7f0a003e

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getLocation()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v43

    move-object/from16 v0, p1

    move-wide/from16 v1, v43

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v7

    if-eqz v7, :cond_7

    const v43, 0x7f0a0047

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v8

    const v43, 0x7f0a0049

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v13

    const v43, 0x7f0a0048

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curWeatherView:Landroid/view/View;

    move-object/from16 v43, v0

    const v44, 0x7f0a0043

    invoke-virtual/range {v43 .. v44}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v43

    sget-object v44, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->XL:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v43

    move-object/from16 v3, v44

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->setIcon(Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)V

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_3

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v23

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_4

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_5

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeWindSpeed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003f

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v47

    aput-object v47, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasTemperature()Z

    move-result v43

    if-eqz v43, :cond_7

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v36

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v43

    if-eqz v43, :cond_b

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHighTemp()I

    move-result v16

    :goto_1
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v43

    if-eqz v43, :cond_c

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getLowTemp()I

    move-result v27

    :goto_2
    const v43, 0x7f0a0044

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v12

    const v43, 0x7f0a0045

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v9

    const v43, 0x7f0a0046

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v11

    const v43, 0x7f0a0056

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v19

    const v43, 0x7f0a0057

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003c

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aput-object v39, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string v43, "--&deg;"

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v32

    const-string v33, "--"

    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v43

    if-eqz v43, :cond_d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003d

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aput-object v18, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-virtual {v7}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v43

    if-eqz v43, :cond_e

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003e

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aput-object v29, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_7
    :goto_4
    const/16 v25, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v42

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getForecasts()Ljava/util/List;

    move-result-object v43

    if-eqz v43, :cond_f

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getForecasts()Ljava/util/List;

    move-result-object v43

    invoke-interface/range {v43 .. v43}, Ljava/util/List;->size()I

    move-result v43

    if-lez v43, :cond_f

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getForecasts()Ljava/util/List;

    move-result-object v43

    invoke-interface/range {v43 .. v43}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v43

    if-eqz v43, :cond_f

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v43

    invoke-static/range {v42 .. v44}, Lcom/google/android/apps/genie/geniewidget/utils/TimeOfDayHelper;->getDayOfWeek(Ljava/util/Calendar;J)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->dayOfWeekMapping:Ljava/util/HashMap;

    move-object/from16 v43, v0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/Integer;

    invoke-virtual/range {v43 .. v43}, Ljava/lang/Integer;->intValue()I

    move-result v43

    move-object/from16 v0, v44

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v34

    const v43, 0x7f0a004c

    move/from16 v0, v43

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v41

    check-cast v41, Landroid/widget/TextView;

    move-object/from16 v0, v41

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v14}, Lcom/google/android/apps/genie/geniewidget/utils/TimeOfDayHelper;->getDayOfWeekName(I)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v41

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v43

    if-eqz v43, :cond_8

    const v43, 0x7f0a004e

    move/from16 v0, v43

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHighTemp()I

    move-result v43

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003d

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aput-object v39, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v37

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_8
    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v43

    if-eqz v43, :cond_9

    const v43, 0x7f0a004f

    move/from16 v0, v43

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getLowTemp()I

    move-result v43

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    move-object/from16 v43, v0

    const v44, 0x7f0b003e

    const/16 v45, 0x1

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aput-object v39, v45, v46

    invoke-virtual/range {v43 .. v45}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v38

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_9
    const v43, 0x7f0a004d

    move/from16 v0, v43

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v43

    sget-object v44, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->S:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v43

    move-object/from16 v3, v44

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->setIcon(Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)V

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    if-eqz v43, :cond_a

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v24

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_a
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_5

    :cond_b
    move/from16 v16, v36

    goto/16 :goto_1

    :cond_c
    move/from16 v27, v36

    goto/16 :goto_2

    :cond_d
    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v33

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, v32

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v33

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getHourlyData()Ljava/util/List;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    const v43, 0x7f0a0058

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->condition:Landroid/widget/TextView;

    const v43, 0x7f0a0055

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->rainProb:Landroid/widget/TextView;

    const v43, 0x7f0a0059

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->wind:Landroid/widget/TextView;

    const v43, 0x7f0a005a

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->humidity:Landroid/widget/TextView;

    const v43, 0x7f0a0054

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->getTextView(I)Landroid/widget/TextView;

    move-result-object v43

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->tempView:Landroid/widget/TextView;

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->initializeHourlyView(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)V

    return-void
.end method

.method private setCurrentCondition(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->condition:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->wind:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeWindSpeed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v5, 0x7f0b003f

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->humidity:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v5, 0x7f0b0040

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getProbPrecipitation()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->rainProb:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasTemperature()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->tempView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return-void
.end method

.method private setIcon(Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)V
    .locals 3
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->postToUI:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$5;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView$5;-><init>(Lcom/google/android/apps/genie/geniewidget/view/WeatherView;Landroid/widget/ImageView;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private switchOnOffHourlyView()V
    .locals 7

    const/16 v4, 0x8

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->viewContainer:Landroid/widget/LinearLayout;

    const v6, 0x7f0a003f

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    if-nez v3, :cond_1

    const v3, 0x7f02004f

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->todaysConditionLayout:Landroid/view/ViewGroup;

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    if-eqz v3, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastWeatherLayout:Landroid/view/ViewGroup;

    iget-boolean v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    if-eqz v3, :cond_3

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyConditionLayout:Landroid/view/ViewGroup;

    iget-boolean v6, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->showingHourly:Z

    if-eqz v6, :cond_4

    :goto_3
    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_1
    const v3, 0x7f02004e

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->curve:Lcom/google/android/apps/genie/geniewidget/view/CurveView;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setCurrentTime(J)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->data:Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;->getCurrentCondition(J)Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->setCurrentCondition(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)V

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_1

    :cond_3
    move v3, v5

    goto :goto_2

    :cond_4
    move v5, v4

    goto :goto_3
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x0

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastWeatherLayout:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    div-int v0, v5, v6

    const/high16 v5, 0x40000000

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->forecastViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public timeChanged(J)V
    .locals 11
    .param p1    # J

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->hourlyData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v5

    cmp-long v5, v5, p1

    if-lez v5, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->condition:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b0040

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getProbPrecipitation()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->rainProb:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->wind:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeWindSpeed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->context:Landroid/content/Context;

    const v6, 0x7f0b003f

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->humidity:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->tempView:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->localizeTemp(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    move-object v4, v0

    goto :goto_0
.end method
