.class public Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;
.super Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;
.source "WeatherViewBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/view/WeatherView;",
        ">;"
    }
.end annotation


# instance fields
.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private final model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

.field private weatherSettingRequestListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;I)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View$OnClickListener;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3, v0}, Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetModel()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method protected buildFailedView()Landroid/view/View;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getLastRequest()Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getLocation()Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;->getFakeLocation()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;->NETWORK_FAILURE:Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->weatherSettingRequestListener:Landroid/view/View$OnClickListener;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;->createWeatherUnknowView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;Landroid/view/View$OnClickListener;)Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView;

    move-result-object v2

    return-object v2

    :cond_1
    sget-object v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;->UNKNOWN_LOCATION:Lcom/google/android/apps/genie/geniewidget/view/WeatherUnknownView$WeatherUnknownViewType;

    goto :goto_0
.end method

.method public buildView()Lcom/google/android/apps/genie/geniewidget/view/WeatherView;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getWeatherForecast()Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;->createWeatherView(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;)Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curView:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curViewTimestamp:J

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curView:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildView()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->buildView()Lcom/google/android/apps/genie/geniewidget/view/WeatherView;

    move-result-object v0

    return-object v0
.end method

.method protected isStale()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curViewTimestamp:J

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->model:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetModel;->getLastUpdateTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/view/WeatherViewBuilder;->curView:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
