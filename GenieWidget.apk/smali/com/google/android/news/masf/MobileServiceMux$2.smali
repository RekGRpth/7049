.class Lcom/google/android/news/masf/MobileServiceMux$2;
.super Lcom/google/android/news/common/task/TimerTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/news/masf/MobileServiceMux;-><init>(Lcom/google/android/news/masf/MobileServiceMux$Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/news/masf/MobileServiceMux;


# direct methods
.method constructor <init>(Lcom/google/android/news/masf/MobileServiceMux;Lcom/google/android/news/common/task/TaskRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/news/masf/MobileServiceMux$2;->this$0:Lcom/google/android/news/masf/MobileServiceMux;

    invoke-direct {p0, p2}, Lcom/google/android/news/common/task/TimerTask;-><init>(Lcom/google/android/news/common/task/TaskRunner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    # getter for: Lcom/google/android/news/masf/MobileServiceMux;->logger:Lcom/google/android/news/debug/LogSource;
    invoke-static {}, Lcom/google/android/news/masf/MobileServiceMux;->access$800()Lcom/google/android/news/debug/LogSource;

    move-result-object v0

    const-string v1, "Running delayed flush"

    invoke-virtual {v0, v1}, Lcom/google/android/news/debug/LogSource;->info(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/news/masf/MobileServiceMux$2;->this$0:Lcom/google/android/news/masf/MobileServiceMux;

    # getter for: Lcom/google/android/news/masf/MobileServiceMux;->mutex:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/news/masf/MobileServiceMux;->access$700(Lcom/google/android/news/masf/MobileServiceMux;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/news/masf/MobileServiceMux$2;->this$0:Lcom/google/android/news/masf/MobileServiceMux;

    invoke-virtual {v0}, Lcom/google/android/news/masf/MobileServiceMux;->scheduleFlush()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
