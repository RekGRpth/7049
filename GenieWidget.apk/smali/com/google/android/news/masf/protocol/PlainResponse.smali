.class public Lcom/google/android/news/masf/protocol/PlainResponse;
.super Lcom/google/android/news/masf/protocol/Response;


# instance fields
.field private payload:Lcom/google/android/news/masf/protocol/EncodedPayload;


# direct methods
.method public constructor <init>(Lcom/google/android/news/masf/DelimitedInputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/news/masf/protocol/Response;-><init>(Lcom/google/android/news/masf/DelimitedInputStream;)V

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    new-instance v2, Lcom/google/android/news/masf/protocol/EncodedPayload;

    invoke-direct {v2, v1, v0, p1}, Lcom/google/android/news/masf/protocol/EncodedPayload;-><init>(Ljava/lang/String;ILcom/google/android/news/masf/DelimitedInputStream;)V

    iput-object v2, p0, Lcom/google/android/news/masf/protocol/PlainResponse;->payload:Lcom/google/android/news/masf/protocol/EncodedPayload;

    return-void
.end method


# virtual methods
.method public bufferAndDetach()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/news/masf/protocol/Response;->bufferAndDetach()V

    iget-object v0, p0, Lcom/google/android/news/masf/protocol/PlainResponse;->payload:Lcom/google/android/news/masf/protocol/EncodedPayload;

    invoke-virtual {v0}, Lcom/google/android/news/masf/protocol/EncodedPayload;->bufferAndDetach()V

    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/news/masf/protocol/PlainResponse;->payload:Lcom/google/android/news/masf/protocol/EncodedPayload;

    invoke-virtual {v0}, Lcom/google/android/news/masf/protocol/EncodedPayload;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getStreamLength()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/news/masf/protocol/PlainResponse;->payload:Lcom/google/android/news/masf/protocol/EncodedPayload;

    invoke-virtual {v0}, Lcom/google/android/news/masf/protocol/EncodedPayload;->getStreamLength()I

    move-result v0

    return v0
.end method
