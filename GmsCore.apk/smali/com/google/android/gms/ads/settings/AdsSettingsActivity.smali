.class public Lcom/google/android/gms/ads/settings/AdsSettingsActivity;
.super Landroid/app/ListActivity;
.source "AdsSettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;
    }
.end annotation


# static fields
.field public static final PERSONALIZED_ADS_LEARN_MORE_URL:Lcom/google/android/gms/common/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mUpButton:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->TAG:Ljava/lang/String;

    const-string v0, "vending_ad_prefs_more_url"

    const-string v1, "http://www.google.com/ads/preferences/html/mobile-about.html"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->PERSONALIZED_ADS_LEARN_MORE_URL:Lcom/google/android/gms/common/config/GservicesValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Lcom/google/android/gms/ads/settings/AdsCheckBox;
    .locals 1
    .param p0    # Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->setupLearnMoreLinkView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private setupLearnMoreLinkView()Landroid/view/View;
    .locals 7

    iget-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f04001d

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->PERSONALIZED_ADS_LEARN_MORE_URL:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/ads/settings/AdPrefsRequest;->getSigString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?sig="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const v4, 0x7f0b010e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-object v3
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mUpButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f04001a

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->setContentView(I)V

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    const v4, 0x7f0a0059

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v4, 0x7f0a005c

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0b010c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0a005b

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v4, 0x7f020056

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    const v4, 0x7f0a0058

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mUpButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mUpButton:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    iput-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mInflater:Landroid/view/LayoutInflater;

    iget-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f040001

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/ads/settings/AdsCheckBox;

    iput-object v4, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

    new-instance v4, Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;

    invoke-direct {v4, p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity$AdsSettingsAdapter;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->loadFromNetwork()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->mAdsCheckBox:Lcom/google/android/gms/ads/settings/AdsCheckBox;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->stop()V

    return-void
.end method
