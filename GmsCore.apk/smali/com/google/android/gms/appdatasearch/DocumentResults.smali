.class public Lcom/google/android/gms/appdatasearch/DocumentResults;
.super Ljava/lang/Object;
.source "DocumentResults.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;


# instance fields
.field mErrorMessage:Ljava/lang/String;

.field mFoundUris:Landroid/os/Bundle;

.field mSectionContent:Landroid/os/Bundle;

.field mTagUriSet:Landroid/os/Bundle;

.field mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mVersionCode:I

    return-void
.end method

.method constructor <init>(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mFoundUris:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mTagUriSet:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mSectionContent:Landroid/os/Bundle;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mErrorMessage:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mFoundUris:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mTagUriSet:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->mSectionContent:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/DocumentResultsCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/DocumentResults;Landroid/os/Parcel;I)V

    return-void
.end method
