.class public Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;
.super Ljava/lang/Object;
.source "SafeParcelablesCreator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addTag(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/os/Bundle;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method private static asBundle([Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6
    .param p0    # [Ljava/lang/String;

    new-instance v3, Landroid/os/Bundle;

    array-length v5, p0

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    move-object v0, p0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static createCorpusStatus()Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>()V

    return-object v0
.end method

.method public static createCorpusStatus(JJI)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 7
    .param p0    # J
    .param p2    # J
    .param p4    # I

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    int-to-long v5, p4

    move-wide v1, p0

    move-wide v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>(JJJ)V

    return-object v0
.end method

.method public static createDocumentResults([Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 5
    .param p0    # [Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p2    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .param p3    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p4    # Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static {p0, p2}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->getFoundUris([Ljava/lang/String;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->asBundle([Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p1, p2, p4, v0}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->getHasTagMap(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->getSectionMaps(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static createDocumentResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSearchResults(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 22
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec;",
            "Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
            ">;)",
            "Lcom/google/android/gms/appdatasearch/SearchResults;"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    new-array v7, v1, [Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    new-array v8, v1, [Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    new-array v9, v1, [Landroid/os/Bundle;

    new-instance v11, Landroid/util/SparseIntArray;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    invoke-direct {v11, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v14, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecCount()I

    move-result v1

    if-ge v14, v1, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getCorpusId()I

    move-result v1

    invoke-virtual {v11, v1, v14}, Landroid/util/SparseIntArray;->put(II)V

    invoke-virtual {v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getCorpusId()I

    move-result v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {v10}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->makeCorpusName(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v14

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    aput-object v1, v8, v14

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    aput-object v1, v9, v14

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    aget-object v12, v1, v14

    const/16 v16, 0x0

    :goto_1
    invoke-virtual {v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpecCount()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v18

    iget-object v1, v12, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    aget-object v17, v1, v16

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSectionId()I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v15

    aget-object v1, v8, v14

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentLengths:[I

    invoke-virtual {v1, v15, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    aget-object v1, v9, v14

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentBuffer:[B

    invoke-virtual {v1, v15, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    aput-object v1, v7, v14

    const/16 v19, 0x0

    :goto_2
    invoke-virtual {v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagCount()I

    move-result v1

    move/from16 v0, v19

    if-ge v0, v1, :cond_1

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTag(I)Ljava/lang/String;

    move-result-object v20

    iget-object v1, v12, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mTags:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    aget-object v21, v1, v19

    aget-object v1, v7, v14

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;->mDocHasTag:[Z

    move-object/from16 v0, v20

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpusIds:[I

    const/4 v14, 0x0

    :goto_3
    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    if-ge v14, v1, :cond_3

    aget v1, v3, v14

    invoke-virtual {v11, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aput v1, v3, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getWantUris()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriLengths:[I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriBuffer:[B

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;)V

    :goto_4
    return-object v1

    :cond_4
    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;)V

    goto :goto_4
.end method

.method public static createSearchResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSearchResultsForUniversalSearch(I[I[Ljava/lang/String;[Landroid/os/Bundle;[Landroid/os/Bundle;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 9
    .param p0    # I
    .param p1    # [I
    .param p2    # [Ljava/lang/String;
    .param p3    # [Landroid/os/Bundle;
    .param p4    # [Landroid/os/Bundle;

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static createSuggestionResults([Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 1
    .param p0    # [Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSuggestionResultsForError(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static getFoundUris([Ljava/lang/String;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;)[Ljava/lang/String;
    .locals 12
    .param p0    # [Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    new-instance v2, Ljava/util/ArrayList;

    array-length v9, p0

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v6, v0, v3

    iget v9, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    if-lt v8, v9, :cond_1

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-interface {v2, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    return-object v9

    :cond_1
    :try_start_0
    new-instance v7, Ljava/lang/String;

    iget-object v9, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriBuffer:[B

    iget-object v10, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriLengths:[I

    aget v10, v10, v8

    const-string v11, "UTF-8"

    invoke-direct {v7, v9, v5, v10, v11}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mUriLengths:[I

    aget v9, v9, v8
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v5, v9

    add-int/lit8 v8, v8, 0x1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string v10, "UTF-8 not supported"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method private static getHasTagMap(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .param p2    # Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .param p3    # [Ljava/lang/String;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-virtual {p2}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedTags()Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagCount()I

    move-result v6

    if-ge v3, v6, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTag(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mTags:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;

    aget-object v6, v6, v3

    iget-object v0, v6, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Tag;->mDocHasTag:[Z

    const/4 v1, 0x0

    :goto_1
    array-length v6, p3

    if-ge v1, v6, :cond_1

    aget-boolean v6, v0, v1

    if-eqz v6, :cond_0

    aget-object v6, p3, v1

    invoke-static {v2, v4, v6}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->addTag(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v2, 0x0

    :cond_3
    return-object v2
.end method

.method private static getSectionMaps(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 21
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .param p2    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p3    # Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .param p4    # [Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_1

    const/4 v7, 0x0

    :cond_0
    return-object v7

    :cond_1
    new-instance v16, Ljava/util/HashSet;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    invoke-direct/range {v16 .. v17}, Ljava/util/HashSet;-><init>(I)V

    new-instance v7, Landroid/os/Bundle;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v7, v0}, Landroid/os/Bundle;-><init>(I)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v0, v8, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-interface/range {v16 .. v17}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpecCount()I

    move-result v17

    move/from16 v0, v17

    if-ge v10, v0, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget-object v17, v17, v18

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    move-object/from16 v17, v0

    aget-object v13, v17, v10

    invoke-virtual {v14}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSectionId()I

    move-result v17

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    iget-object v11, v13, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentLengths:[I

    iget-object v9, v13, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentBuffer:[B

    const/4 v6, 0x0

    const/4 v4, 0x0

    :goto_2
    move-object/from16 v0, p4

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v4, v0, :cond_4

    aget v17, v11, v4

    if-lez v17, :cond_3

    :try_start_0
    aget-object v17, p4, v4

    new-instance v18, Ljava/lang/String;

    aget v19, v11, v4

    const-string v20, "UTF-8"

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v9, v6, v1, v2}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    aget v17, v11, v4

    add-int v6, v6, v17

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catch_0
    move-exception v3

    new-instance v17, Ljava/lang/IllegalStateException;

    const-string v18, "Encoding utf8 not available"

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_4
    invoke-virtual {v7, v12, v15}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private static makeCorpusName(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
