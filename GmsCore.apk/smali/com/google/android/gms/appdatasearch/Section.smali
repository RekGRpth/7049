.class public Lcom/google/android/gms/appdatasearch/Section;
.super Ljava/lang/Object;
.source "Section.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/SectionCreator;


# instance fields
.field mVersionCode:I

.field public name:Ljava/lang/String;

.field public snippetLength:I

.field public snippeted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/SectionCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/SectionCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/SectionCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/Section;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/SectionCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/appdatasearch/SectionCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/SectionCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/Section;Landroid/os/Parcel;I)V

    return-void
.end method
