.class public Lcom/google/android/gms/appdatasearch/UniversalSearchSections;
.super Ljava/lang/Object;
.source "UniversalSearchSections.java"


# static fields
.field private static final UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

.field private static final sNameIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "text1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "text2"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "icon"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "intent_action"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "intent_data"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "intent_data_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "intent_extra_data"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "suggest_large_icon"

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->sNameIdMap:Ljava/util/Map;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->sNameIdMap:Ljava/util/Map;

    sget-object v2, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSectionName(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    if-ltz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static getSectionsCount()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->UNIVERSAL_SECTION_NAMES:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
