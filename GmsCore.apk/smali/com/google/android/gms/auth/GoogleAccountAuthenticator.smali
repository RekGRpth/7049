.class public Lcom/google/android/gms/auth/GoogleAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "GoogleAccountAuthenticator.java"


# static fields
.field private static transient sInstance:Lcom/google/android/gms/auth/GoogleAccountAuthenticator;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mAppContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/auth/base/PackageManagerDecorator;

    iget-object v1, p0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/auth/base/PackageManagerDecorator;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    return-void
.end method

.method public static getInstance()Lcom/google/android/gms/auth/GoogleAccountAuthenticator;
    .locals 2

    sget-object v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->sInstance:Lcom/google/android/gms/auth/GoogleAccountAuthenticator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->sInstance:Lcom/google/android/gms/auth/GoogleAccountAuthenticator;

    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->sInstance:Lcom/google/android/gms/auth/GoogleAccountAuthenticator;

    return-object v0
.end method

.method private handleGLSTokenResponse(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/os/Bundle;
    .locals 16
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Ljava/lang/String;

    const-string v3, "authtoken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V

    if-nez v15, :cond_3

    sget-object v3, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v14, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->isUserRecoverableError(Lcom/google/android/gms/auth/login/Status;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v3, v0, v1, v2}, Lcom/google/android/gms/auth/TokenActivity;->createUserRecoveryIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v10

    const-string v3, "handle_notification"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v13

    sget-object v3, Lcom/google/android/gms/auth/login/Status;->NEED_PERMISSION:Lcom/google/android/gms/auth/login/Status;

    if-eq v13, v3, :cond_1

    const/4 v9, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/google/android/auth/base/PackageManagerDecorator;->getApplicationIconBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/google/android/auth/base/PackageManagerDecorator;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mAppContext:Landroid/content/Context;

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v3 .. v12}, Lcom/google/android/gms/auth/TokenActivity;->pushNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;ZLandroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-object v14

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    const-string v3, "userRecoveryIntent"

    invoke-virtual {v14, v3, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    :cond_3
    const-string v3, "authtoken"

    invoke-virtual {v14, v3, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "authAccount"

    move-object/from16 v0, p4

    invoke-virtual {v14, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v3

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/gms/auth/NotificationStore;->removeRelevantNotificationsAndFireCallbacks(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private isUserRecoverableError(Lcom/google/android/gms/auth/login/Status;)Z
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/login/Status;

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->CAPTCHA:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->DEVICE_MANAGEMENT_REQUIRED:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->NEED_PERMISSION:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->NEEDS_BROWSER:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->USER_CANCEL:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private validateCallbackIntent(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const-string v2, "callback_intent"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "callback_intent"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Given callback Intent is not serializable using getUri()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private validateSyncRequest(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v2, "sync_extras"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "sync_extras"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Landroid/content/ContentResolver;->validateSyncExtrasBundle(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Given sync request paramaters are not valid."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addAccount hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "confirmCredentials hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "editProperties hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 17
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->validateCallbackIntent(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->validateSyncRequest(Landroid/os/Bundle;)V

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    move-object/from16 v0, p2

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/auth/login/GLSUser;->get(Ljava/lang/String;)Lcom/google/android/gms/auth/login/GLSUser;

    move-result-object v10

    iget-boolean v2, v10, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Non existing account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->getGLSContext()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v11

    const-string v2, "session"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/google/android/gms/auth/login/GLSSession;

    if-nez v15, :cond_1

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v15

    :cond_1
    const-string v2, "callerUid"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v8, v4}, Lcom/google/android/auth/base/PackageManagerDecorator;->sameSignature(II)Z

    move-result v2

    if-eqz v2, :cond_2

    move v8, v13

    :cond_2
    const-string v2, "androidPackageName"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v2, v8, v7}, Lcom/google/android/auth/base/PackageManagerDecorator;->appPackageMatches(ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " not found in UID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    iput-object v7, v15, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    iput v8, v15, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    move-object/from16 v0, p4

    iput-object v0, v15, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    move-object/from16 v0, p3

    invoke-virtual {v10, v0, v8, v15}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v3

    iget-object v2, v15, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    if-nez v2, :cond_6

    const-string v9, "UNKNOWN ERROR"

    :goto_0
    move-object/from16 v0, p2

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/auth/GoogleAccountAuthenticator;->handleGLSTokenResponse(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v14

    const-string v2, "GLSUser"

    const/4 v4, 0x2

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "authtoken"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v2, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "callingUid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " package: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v15, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GetToken: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v10, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v16, :cond_7

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v15, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-object v14

    :cond_6
    iget-object v2, v15, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getAuthTokenLabel hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "hasFeatures hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "udpateCredentials hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
