.class public final enum Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
.super Ljava/lang/Enum;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/NotificationStore$NotificationData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

.field public static final enum ACCOUNT_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

.field public static final enum SCOPE_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    const-string v1, "ACCOUNT_LEVEL"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->ACCOUNT_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    new-instance v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    const-string v1, "SCOPE_LEVEL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->SCOPE_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    sget-object v1, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->ACCOUNT_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->SCOPE_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->$VALUES:[Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    .locals 1

    const-class v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->$VALUES:[Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    invoke-virtual {v0}, [Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    return-object v0
.end method
