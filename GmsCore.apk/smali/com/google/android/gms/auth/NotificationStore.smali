.class public Lcom/google/android/gms/auth/NotificationStore;
.super Ljava/lang/Object;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/NotificationStore$1;,
        Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/gms/auth/NotificationStore;


# instance fields
.field private mNotificationDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/auth/NotificationStore$NotificationData;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mNotificationLookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/auth/NotificationStore$NotificationData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/gms/auth/NotificationStore;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private getNotificationManager()Landroid/app/NotificationManager;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method public static getSingleton()Lcom/google/android/gms/auth/NotificationStore;
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->init()V

    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    return-object v0
.end method

.method private static init()V
    .locals 13

    const/4 v12, 0x0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Lcom/google/android/gms/auth/NotificationStore;->readFile(Landroid/content/Context;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    if-nez v8, :cond_0

    new-instance v10, Lcom/google/android/gms/auth/NotificationStore;

    invoke-direct {v10, v12}, Lcom/google/android/gms/auth/NotificationStore;-><init>(I)V

    sput-object v10, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string v10, "NotificationStore"

    const-string v11, "Error while reading file "

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v10, "notification_data.dat"

    invoke-virtual {v2, v10}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    new-instance v10, Lcom/google/android/gms/auth/NotificationStore;

    invoke-direct {v10, v12}, Lcom/google/android/gms/auth/NotificationStore;-><init>(I)V

    sput-object v10, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    array-length v10, v8

    invoke-virtual {v7, v8, v12, v10}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v7, v12}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {v7}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/NotificationStore;->isDataOutdated(Landroid/os/Bundle;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "NotificationStore"

    const-string v11, "System rebooted after Notification storage file was last written"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "NotificationStore"

    const-string v11, "Deleting the file"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "notification_data.dat"

    invoke-virtual {v2, v10}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    new-instance v10, Lcom/google/android/gms/auth/NotificationStore;

    invoke-direct {v10, v12}, Lcom/google/android/gms/auth/NotificationStore;-><init>(I)V

    sput-object v10, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    goto :goto_0

    :cond_1
    const-string v10, "notification_data_array"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v9, Lcom/google/android/gms/auth/NotificationStore;

    const-string v10, "notification_counter"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-direct {v9, v10}, Lcom/google/android/gms/auth/NotificationStore;-><init>(I)V

    if-nez v1, :cond_2

    sput-object v9, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    :try_start_1
    # invokes: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fromBundle(Landroid/os/Bundle;Lcom/google/android/gms/auth/NotificationStore;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v3, v9}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$000(Landroid/os/Bundle;Lcom/google/android/gms/auth/NotificationStore;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v6

    invoke-virtual {v9, v6}, Lcom/google/android/gms/auth/NotificationStore;->putNotificationData(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v4

    const-string v10, "NotificationStore"

    const-string v11, "Failed to parse a NotificationData"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    sput-object v9, Lcom/google/android/gms/auth/NotificationStore;->sInstance:Lcom/google/android/gms/auth/NotificationStore;

    goto :goto_0
.end method

.method private static isDataOutdated(Landroid/os/Bundle;)Z
    .locals 8
    .param p0    # Landroid/os/Bundle;

    const-string v4, "write_timestamp"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v0, v4, v6

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private makeAccountLevelLookupKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;

    return-object p1
.end method

.method private makeScopeLevelLookupKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static readFile(Landroid/content/Context;)[B
    .locals 10
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v7, "notification_data.dat"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "NotificationStore"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file does not exist: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/io/BufferedInputStream;

    const-string v7, "notification_data.dat"

    invoke-virtual {p0, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v7, 0x4

    new-array v6, v7, [B

    invoke-virtual {v3, v6}, Ljava/io/BufferedInputStream;->read([B)I

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/IntBuffer;->get()I

    move-result v5

    const-string v7, "NotificationStore"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "NotificationStore"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Size of parcel: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    add-int/lit8 v7, v5, 0x4

    new-array v0, v7, [B

    invoke-virtual {v3, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_0
.end method


# virtual methods
.method public getAccountLevelNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/NotificationStore;->makeAccountLevelLookupKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    return-object v0
.end method

.method public getNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    return-object v0
.end method

.method public getScopeLevelNotificationData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/NotificationStore;->makeScopeLevelLookupKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    return-object v0
.end method

.method public newNotificationDataBuilder()Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;-><init>(Lcom/google/android/gms/auth/NotificationStore;Lcom/google/android/gms/auth/NotificationStore$1;)V

    return-object v0
.end method

.method public declared-synchronized persist()Z
    .locals 15

    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string v12, "write_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {v1, v12, v13, v14}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v12, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v12

    if-lez v12, :cond_0

    const-string v12, "notification_counter"

    iget-object v13, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v13

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v12, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->toBundle()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    :cond_1
    :try_start_1
    const-string v12, "notification_data_array"

    invoke-virtual {v1, v12, v9}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v6, 0x0

    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v12

    const-string v13, "notification_data.dat"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/google/android/gms/common/app/GmsApplication;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v6

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    const/4 v12, 0x4

    invoke-static {v12}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v8}, Landroid/os/Parcel;->marshall()[B

    move-result-object v2

    array-length v12, v2

    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v6, v2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v6, :cond_2

    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :goto_1
    const/4 v12, 0x1

    :cond_3
    :goto_2
    monitor-exit p0

    return v12

    :catch_0
    move-exception v5

    :try_start_4
    const-string v12, "NotificationStore"

    const-string v13, "Error while closing file "

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v12, 0x0

    goto :goto_2

    :catch_1
    move-exception v12

    if-eqz v6, :cond_2

    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v5

    :try_start_6
    const-string v12, "NotificationStore"

    const-string v13, "Error while closing file "

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v12, 0x0

    goto :goto_2

    :catch_3
    move-exception v5

    :try_start_7
    const-string v12, "NotificationStore"

    const-string v13, "Error while writing to file "

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v12, 0x0

    if-eqz v6, :cond_3

    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    :catch_4
    move-exception v5

    :try_start_9
    const-string v12, "NotificationStore"

    const-string v13, "Error while closing file "

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/4 v12, 0x0

    goto :goto_2

    :catchall_1
    move-exception v12

    if-eqz v6, :cond_4

    :try_start_a
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_4
    :try_start_b
    throw v12

    :catch_5
    move-exception v5

    const-string v12, "NotificationStore"

    const-string v13, "Error while closing file "

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const/4 v12, 0x0

    goto :goto_2
.end method

.method public declared-synchronized putNotificationData(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$100(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$200(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->SCOPE_LEVEL:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    if-ne v4, v5, :cond_0

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$300(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "package_key"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$400(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v3, v1}, Lcom/google/android/gms/auth/NotificationStore;->makeScopeLevelLookupKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    invoke-interface {v4, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$400(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gms/auth/NotificationStore;->makeAccountLevelLookupKey(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized remove(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationDataMap:Ljava/util/Map;

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->access$100(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationDataKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationDataKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/auth/NotificationStore;->mNotificationLookup:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/NotificationStore;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/NotificationStore;->persist()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public removeRelevantNotificationsAndFireCallbacks(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/NotificationStore;->getAccountLevelNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/NotificationStore;->remove(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fireCallbacks()V

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/auth/NotificationStore;->getScopeLevelNotificationData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/NotificationStore;->remove(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fireCallbacks()V

    :cond_1
    return-void
.end method
