.class public Lcom/google/android/gms/auth/TokenActivity;
.super Landroid/app/Activity;
.source "TokenActivity.java"


# instance fields
.field private mExtras:Landroid/os/Bundle;

.field private mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

.field private mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

.field private mScope:Ljava/lang/String;

.field private mSession:Lcom/google/android/gms/auth/login/GLSSession;

.field private mUsername:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private addRequestDataToSession(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iput-object p2, v1, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v2, p2}, Lcom/google/android/auth/base/PackageManagerDecorator;->getUid(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    :cond_0
    const-string v1, "callerExtras"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v0, v1, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    :cond_1
    return-void
.end method

.method public static createUserRecoveryIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/TokenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "callerExtras"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scope:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extrashash:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private getEffectiveCallingPackage(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v3, v0}, Lcom/google/android/auth/base/PackageManagerDecorator;->getUid(Ljava/lang/String;)I

    move-result v1

    const-string v3, "androidPackageName"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/auth/base/PackageManagerDecorator;->sameSignature(II)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "androidPackageName"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const-string v2, "androidPackageName"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "GMS certs do not match caller certs."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    const-string v2, "androidPackageName"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getIntentToLaunch(Landroid/os/Bundle;Lcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/Intent;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "session"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x3e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private static getNotificationData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/NotificationStore;->getAccountLevelNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/gms/auth/NotificationStore;->getScopeLevelNotificationData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v1

    goto :goto_0
.end method

.method private isRequestValid(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/auth/login/GLSUser;->get(Ljava/lang/String;)Lcom/google/android/gms/auth/login/GLSUser;

    move-result-object v0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static newNotification(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/app/Notification;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/app/PendingIntent;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/graphics/Bitmap;
    .param p5    # Ljava/lang/String;

    const/4 v4, 0x1

    if-eqz p3, :cond_1

    const v1, 0x7f0b00f7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    const v1, 0x7f0b00f8

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p5, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v1, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x108008a

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f0b00fa

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    return-object v1

    :cond_1
    const v1, 0x7f0b00f9

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020056

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p4

    goto :goto_0
.end method

.method private static newRequestData(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "authAccount"

    invoke-virtual {v2, v4, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "uid_key"

    invoke-virtual {v2, v4, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "package_key"

    invoke-virtual {v2, v4, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "callback_intent"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "callback_intent"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "callback_intent"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v4, "authority"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "sync_extras"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "authority"

    const-string v5, "authority"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sync_extras"

    const-string v5, "sync_extras"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-object v2
.end method

.method public static pushNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;ZLandroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 19
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z
    .param p7    # Landroid/content/Intent;
    .param p8    # Landroid/graphics/Bitmap;
    .param p9    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    move/from16 v3, p6

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/auth/TokenActivity;->getNotificationData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v14

    if-nez v14, :cond_0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/auth/NotificationStore;->newNotificationDataBuilder()Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    move-result-object v11

    invoke-virtual {v11, v15}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->setNotificationDataKey(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->setAccount(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    :try_start_0
    move/from16 v0, p6

    invoke-virtual {v11, v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->build(Z)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    :try_end_0
    .catch Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    :cond_0
    const-string v5, "notification_data_key"

    invoke-virtual {v14}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationDataKey()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p7

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v5, 0x0

    const/high16 v7, 0x10000000

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v5, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    if-eqz p6, :cond_2

    const/4 v8, 0x0

    :goto_0
    move-object/from16 v5, p0

    move-object/from16 v7, p1

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v5 .. v10}, Lcom/google/android/gms/auth/TokenActivity;->newNotification(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v13

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v14, v0, v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getRequest(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v18

    if-nez v18, :cond_1

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/auth/TokenActivity;->newRequestData(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->addRequest(Landroid/os/Bundle;)V

    :cond_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/google/android/gms/auth/NotificationStore;->putNotificationData(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V

    const-string v5, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/NotificationManager;

    invoke-virtual {v14}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationId()I

    move-result v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/auth/NotificationStore;->persist()Z

    :goto_1
    return-void

    :catch_0
    move-exception v12

    const-string v5, "TokenActivity"

    const-string v7, "Error: Malformed NotificationData. This is a Bug."

    invoke-static {v5, v7, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    move-object/from16 v8, p2

    goto :goto_0
.end method

.method private returnResult(Landroid/content/Intent;Z)V
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "authAccount"

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "callerExtras"

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/auth/TokenActivity;->setResult(ILandroid/content/Intent;)V

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/auth/login/Status;->USER_CANCEL:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    if-eqz p1, :cond_2

    const-string v2, "authtoken"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "authtoken"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/auth/TokenActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v4, 0x3ea

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-nez p2, :cond_2

    const/4 v2, 0x1

    invoke-direct {p0, p3, v2}, Lcom/google/android/gms/auth/TokenActivity;->returnResult(Landroid/content/Intent;Z)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/NotificationStore;->remove(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)V

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fireCallbacks()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/16 v2, 0x3e9

    if-ne p1, v2, :cond_4

    const-string v2, "authtoken"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-direct {p0, p3, v3}, Lcom/google/android/gms/auth/TokenActivity;->returnResult(Landroid/content/Intent;Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p3, v4}, Lcom/google/android/gms/auth/TokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_4
    if-ne p1, v4, :cond_0

    invoke-direct {p0, p3, v3}, Lcom/google/android/gms/auth/TokenActivity;->returnResult(Landroid/content/Intent;Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/android/gms/auth/NotificationStore;->getSingleton()Lcom/google/android/gms/auth/NotificationStore;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v5, "authAccount"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    const-string v5, "callerExtras"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    const-string v5, "session"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    const-string v5, "notification_data_key"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/NotificationStore;->getNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    new-instance v5, Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/auth/base/PackageManagerDecorator;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "authAccount"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    const-string v5, "callerExtras"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v6, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    iput-object v6, v5, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v5, "callerExtras"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gms/auth/TokenActivity;->getEffectiveCallingPackage(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "notification_data_key"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "notification_data_key"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/NotificationStore;->getNotificationData(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    const-string v6, "androidPackageName"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->hasRequest()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    const-string v5, "TokenActivity"

    const-string v6, "Expected NotificationData to be present."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/TokenActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/auth/TokenActivity;->addRequestDataToSession(Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget v7, v7, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    invoke-direct {p0, v5, v6, v7}, Lcom/google/android/gms/auth/TokenActivity;->isRequestValid(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/TokenActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/TokenActivity;->finish()V

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v6, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    invoke-direct {p0, v3, v5, v6, v7}, Lcom/google/android/gms/auth/TokenActivity;->getIntentToLaunch(Landroid/os/Bundle;Lcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Landroid/content/Intent;

    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/auth/TokenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mScope:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "callerExtras"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    if-eqz v0, :cond_0

    const-string v0, "notification_data_key"

    iget-object v1, p0, Lcom/google/android/gms/auth/TokenActivity;->mNotificationData:Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->getNotificationDataKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
