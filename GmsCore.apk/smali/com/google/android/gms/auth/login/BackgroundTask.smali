.class public abstract Lcom/google/android/gms/auth/login/BackgroundTask;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "BackgroundTask.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/BackgroundTask$2;
    }
.end annotation


# instance fields
.field mCancelButton:Landroid/widget/Button;

.field mCancelable:Z

.field protected mHandler:Landroid/os/Handler;

.field protected mStartTime:J

.field protected mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

.field private mVerboseMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelable:Z

    new-instance v0, Lcom/google/android/gms/auth/login/BackgroundTask$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/BackgroundTask$1;-><init>(Lcom/google/android/gms/auth/login/BackgroundTask;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/auth/login/BackgroundTask;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/login/BackgroundTask;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BackgroundTask;->onReply(Landroid/os/Message;)V

    return-void
.end method

.method private onReply(Landroid/os/Message;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->cancelTaskThread()V

    invoke-static {p1}, Lcom/google/android/gms/auth/login/Status;->fromMessage(Landroid/os/Message;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReply() - status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v2, Lcom/google/android/gms/auth/login/BackgroundTask$2;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->onError(Lcom/google/android/gms/auth/login/Status;Landroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->finish(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/login/CaptchaActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/BackgroundTask;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected cancelTaskThread()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x3f1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/login/BackgroundTask;->finish(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_5

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_1

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Captcha answered, retry withthread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const v0, 0x320ca

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "with action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->start()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->onCancel()V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_4

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Captcha failed with resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->onCancel()V

    goto :goto_0

    :cond_5
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCancel()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->finish(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelable:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->onCancel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mStartTime:J

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->getActivityContentView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f040011

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->setContentView(I)V

    const v0, 0x7f0a0021

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0046

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BackgroundTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mVerboseMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BackgroundTask;->start()V

    :cond_0
    return-void
.end method

.method protected onError(Lcom/google/android/gms/auth/login/Status;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/login/Status;
    .param p2    # Landroid/content/Intent;

    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/BackgroundTask;->createErrorIntent(Lcom/google/android/gms/auth/login/Status;)Landroid/content/Intent;

    move-result-object p2

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/auth/login/BackgroundTask;->finish(ILandroid/content/Intent;)V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->onPause()V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause(), class="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected setMessage(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BackgroundTask;->mVerboseMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
