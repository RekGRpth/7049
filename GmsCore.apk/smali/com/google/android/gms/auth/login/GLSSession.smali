.class public Lcom/google/android/gms/auth/login/GLSSession;
.super Ljava/lang/Object;
.source "GLSSession.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/GLSSession$INDEXES;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAccessToken:Ljava/lang/String;

.field public mAccountManagerOptions:Landroid/os/Bundle;

.field private final mBools:[Z

.field public mCallingPackage:Ljava/lang/String;

.field public mCallingUID:I

.field public mCaptchaAnswer:Ljava/lang/String;

.field public mCaptchaBitmap:Landroid/graphics/Bitmap;

.field public mCaptchaToken:Ljava/lang/String;

.field public mDetail:Ljava/lang/String;

.field public mError:Lcom/google/android/gms/auth/login/Status;

.field public mFirstName:Ljava/lang/String;

.field public mGender:Ljava/lang/String;

.field public mLastName:Ljava/lang/String;

.field public mPassword:Ljava/lang/String;

.field public final mPermission:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mPermissionDetails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mPicasaUser:Ljava/lang/String;

.field public mProfileResult:I

.field public final mScopeData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation
.end field

.field public mSecondaryEmail:Ljava/lang/String;

.field public mSecurityAnswer:Ljava/lang/String;

.field public mSecurityQuestion:Ljava/lang/String;

.field public mUrl:Ljava/lang/String;

.field public mUsername:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession$1;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/GLSSession$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/login/GLSSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->values()[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mProfileResult:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermission:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermissionDetails:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->values()[Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mProfileResult:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPassword:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPicasaUser:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecurityQuestion:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecurityAnswer:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccessToken:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mProfileResult:I

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermission:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermissionDetails:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    sget-object v1, Lcom/google/android/gms/common/acl/ScopeData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    return-void
.end method

.method public static create()Lcom/google/android/gms/auth/login/GLSSession;
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/login/GLSSession;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/GLSSession;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasAgreedToMobileTOS()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_MOBILE_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public hasAgreedToPersonalizedContent()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public hasESMobile()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_ESMOBILE:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public hasGooglePlus()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isCreatingAccount()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->CREATING_ACCOUNT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isGooglePlusSelected()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->USER_SELECTED_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isInSetupWizard()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_SETUP_WIZARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public setAgreedToMobileTOS(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_MOBILE_TOS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setAgreedToPersonalizedContent(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->AGREED_TO_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setESMobile(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_ESMOBILE:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGooglePlus(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->HAS_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGooglePlusAllowed(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ALLOW_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGooglePlusSelected(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->USER_SELECTED_GOOGLE_PLUS:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNameActivityCompleted(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->NAME_ACTIVITY_COMPLETED:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setSetupWizardInProgress(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->IS_SETUP_WIZARD:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setTermsOfServiceShown(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    sget-object v1, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->TOS_SHOWN:Lcom/google/android/gms/auth/login/GLSSession$INDEXES;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/GLSSession$INDEXES;->ordinal()I

    move-result v1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPicasaUser:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecurityQuestion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mSecurityAnswer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mProfileResult:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mBools:[Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermission:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mPermissionDetails:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
