.class public Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/GLSUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GLSContext"
.end annotation


# instance fields
.field mAccountManager:Landroid/accounts/AccountManager;

.field private mAndroidIdHex:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mHttpClient:Lorg/apache/http/client/HttpClient;

.field mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

.field public final mLastErrors:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTestNoPermission:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lorg/apache/http/client/HttpClient;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/AccountManager;
    .param p3    # Lorg/apache/http/client/HttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iput-object p3, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lorg/apache/http/client/HttpClient;Lcom/google/android/gms/auth/login/GLSUser$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/AccountManager;
    .param p3    # Lorg/apache/http/client/HttpClient;
    .param p4    # Lcom/google/android/gms/auth/login/GLSUser$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lorg/apache/http/client/HttpClient;)V

    return-void
.end method

.method static jsonError(Lcom/google/android/gms/auth/login/Status;)Lorg/json/JSONObject;
    .locals 3
    .param p0    # Lcom/google/android/gms/auth/login/Status;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->JSON_STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/Status;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public checkRealName(Lcom/google/android/gms/auth/login/GLSSession;)Lcom/google/android/gms/auth/login/Status;
    .locals 7
    .param p1    # Lcom/google/android/gms/auth/login/GLSSession;

    :try_start_0
    new-instance v1, Lorg/json/JSONStringer;

    invoke-direct {v1}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->prepareRequestNoUser(Lorg/json/JSONStringer;)V

    sget-object v4, Lcom/google/android/gms/auth/login/RequestKey;->FIRST_NAME:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v4, Lcom/google/android/gms/auth/login/RequestKey;->LAST_NAME:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    invoke-virtual {v1}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    # getter for: Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->access$000()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v4

    const-string v5, "https://android.clients.google.com/setup/checkname"

    const-string v6, "checkRealName"

    invoke-virtual {v4, v5, v1, v6}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRealName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Res: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/gms/auth/login/Status;->fromJSON(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gms/auth/login/Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_INVALID_CHAR:Lcom/google/android/gms/auth/login/Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_NICKNAME:Lcom/google/android/gms/auth/login/Status;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_OTHER:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    sget-object v3, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    goto :goto_0
.end method

.method public getAndroidIdHex()Ljava/lang/String;
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAndroidIdHex:Ljava/lang/String;

    return-object v2
.end method

.method public getInjector()Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    return-object v0
.end method

.method public httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/json/JSONStringer;
    .param p3    # Ljava/lang/String;

    const-string v2, ""

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->requestJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Json request failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-static {v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->jsonError(Lcom/google/android/gms/auth/login/Status;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Json request failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-static {v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->jsonError(Lcom/google/android/gms/auth/login/Status;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_0
.end method

.method public httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/http/HttpEntity;
    .param p3    # Lorg/apache/http/Header;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpTestInjector:Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    :cond_0
    if-nez v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, 0xffffff

    and-int/2addr v3, v4

    invoke-static {v3}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsSetThreadStatsTag(I)V

    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    if-eqz p3, :cond_1

    invoke-virtual {v1, p3}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    :cond_1
    # getter for: Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->access$000()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    :cond_2
    return-object v2

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    throw v3
.end method

.method public log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "Token=[^&\n;]*"

    const-string v1, "Token=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "LSID=[^&\n;]*"

    const-string v1, "LSID=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "SID=[^&\n;]*"

    const-string v1, "SID=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "auth=[^&\n;]*"

    const-string v1, "auth=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "EncryptedPasswd=[^&\n;]*"

    const-string v1, "EncryptedPasswd=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Passwd=[^&\n;]*"

    const-string v1, "Passwd=SECRET"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "GLSUser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GLSUser"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x28

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mLastErrors:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public prepareRequestNoUser(Lorg/json/JSONStringer;)V
    .locals 5
    .param p1    # Lorg/json/JSONStringer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    # getter for: Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->access$000()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->ANDROID_ID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_0
    # getter for: Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->access$000()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_country"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->LANGUAGE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    return-void
.end method

.method public requestJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/json/JSONStringer;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {p2}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, p3}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v1

    throw v1
.end method
