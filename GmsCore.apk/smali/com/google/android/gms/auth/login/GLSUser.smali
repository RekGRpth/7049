.class public Lcom/google/android/gms/auth/login/GLSUser;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/GLSUser$1;,
        Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;,
        Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;,
        Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;


# instance fields
.field private captchaAnswer:Ljava/lang/String;

.field private captchaToken:Ljava/lang/String;

.field public existing:Z

.field public mAccount:Landroid/accounts/Account;

.field public mEmail:Ljava/lang/String;

.field public mEncryptedPassword:Ljava/lang/String;

.field private final mGmsPkgVersion:I

.field private final mGmsUID:I

.field private mHash:Ljava/lang/String;

.field public mMasterToken:Ljava/lang/String;

.field private final mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

.field private mUpdatedPassword:Ljava/lang/String;

.field public mUseBrowserFlow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/auth/login/GLSUser;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/auth/login/GLSUser;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ZILjava/lang/String;ILcom/google/android/auth/base/PackageManagerDecorator;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Lcom/google/android/auth/base/PackageManagerDecorator;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    iput p3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    iput p5, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsPkgVersion:I

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-eqz p1, :cond_0

    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    :cond_0
    if-eqz p2, :cond_3

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    invoke-virtual {v1, v2, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_4

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    :cond_1
    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    :cond_3
    return-void

    :cond_4
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v4, "1"

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    return-object v0
.end method

.method private add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v0, p2, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static addPhoto(Lcom/google/android/gms/auth/login/GLSSession;Lorg/json/JSONStringer;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/login/GLSSession;
    .param p1    # Lorg/json/JSONStringer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    return-void
.end method

.method private cacheKeyFromUidAndTokenType(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const-string v2, "SID"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "LSID"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const-string v2, "sierra"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "sierrasandbox"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "androidsecure"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/google/android/gms/auth/login/GLSUser;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-nez p3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v2, p3}, Lcom/google/android/auth/base/PackageManagerDecorator;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 p1, 0x0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static declared-synchronized get(Ljava/lang/String;)Lcom/google/android/gms/auth/login/GLSUser;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/gms/auth/login/GLSUser;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->get(Ljava/lang/String;Z)Lcom/google/android/gms/auth/login/GLSUser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized get(Ljava/lang/String;Z)Lcom/google/android/gms/auth/login/GLSUser;
    .locals 26
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    const-class v25, Lcom/google/android/gms/auth/login/GLSUser;

    monitor-enter v25

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v8, Lcom/google/android/auth/base/PackageManagerDecorator;

    move-object/from16 v0, v24

    invoke-direct {v8, v0}, Lcom/google/android/auth/base/PackageManagerDecorator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSUser;->getGLSContext()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    const/4 v7, -0x1

    :try_start_1
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v23

    move-object/from16 v0, v23

    iget v7, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-nez p0, :cond_0

    :try_start_2
    new-instance v2, Lcom/google/android/gms/auth/login/GLSUser;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/auth/login/GLSUser;-><init>(Ljava/lang/String;ZILjava/lang/String;ILcom/google/android/auth/base/PackageManagerDecorator;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v9, v2

    :goto_1
    monitor-exit v25

    return-object v9

    :catch_0
    move-exception v20

    :try_start_3
    const-string v3, "GLSUser"

    const-string v4, "Can\'t find package info for GooglePlayServices."

    move-object/from16 v0, v20

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v25

    throw v3

    :cond_0
    :try_start_4
    sget-object v3, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v17

    move-object/from16 v18, v17

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_2
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    aget-object v16, v18, v21

    move-object/from16 v0, v16

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v9, Lcom/google/android/gms/auth/login/GLSUser;

    const/4 v11, 0x1

    move-object/from16 v10, p0

    move v12, v5

    move-object v13, v6

    move v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/google/android/gms/auth/login/GLSUser;-><init>(Ljava/lang/String;ZILjava/lang/String;ILcom/google/android/auth/base/PackageManagerDecorator;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    :cond_2
    new-instance v9, Lcom/google/android/gms/auth/login/GLSUser;

    const/4 v11, 0x0

    move-object/from16 v10, p0

    move v12, v5

    move-object v13, v6

    move v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/google/android/gms/auth/login/GLSUser;-><init>(Ljava/lang/String;ZILjava/lang/String;ILcom/google/android/auth/base/PackageManagerDecorator;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "SID"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "LSID"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->AUTH:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;
    .locals 21
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gms/auth/login/GLSSession;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Landroid/os/Bundle;",
            "Z",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    if-nez p3, :cond_0

    const-string v3, "GLSUser"

    const-string v4, "No session"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object p3

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/login/GLSUser;->hasSecret()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No secrets - returning BAD_AUTH "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v20, v19

    :goto_0
    return-object v20

    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/auth/login/GLSUser;->prepareRequest(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/util/ArrayList;Ljava/lang/String;Z)V

    const/4 v13, 0x0

    :try_start_0
    new-instance v13, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v13, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v18, 0x0

    :try_start_1
    sget-object v3, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    const-string v4, "https://android.clients.google.com/auth"

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v13, v5, v6}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v16

    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v17

    const v3, 0x320d2

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gms/auth/login/Compat;->eventLogWriteEvent(ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->parseResponse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v19

    const-string v3, "x-status"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v17

    invoke-direct {v0, v9, v1, v3, v2}, Lcom/google/android/gms/auth/login/GLSUser;->logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V

    sget-object v3, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    if-eqz v14, :cond_2

    invoke-static {v14}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v18

    :cond_2
    const v4, 0x320d2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v18, :cond_3

    const/4 v3, 0x0

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object p1, v5, v3

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    move-object/from16 v20, v19

    goto/16 :goto_0

    :catch_0
    move-exception v12

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    :cond_3
    sget-object v3, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v3

    goto :goto_1

    :catch_1
    move-exception v12

    :try_start_2
    sget-object v18, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3, v4, v5}, Lcom/google/android/gms/auth/login/GLSUser;->logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v4, 0x320d2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v18, :cond_4

    const/4 v3, 0x0

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object p1, v5, v3

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    move-object/from16 v20, v19

    goto/16 :goto_0

    :cond_4
    sget-object v3, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v3

    goto :goto_2

    :catchall_0
    move-exception v3

    move-object v4, v3

    const v5, 0x320d2

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez v18, :cond_5

    const/4 v3, 0x0

    :goto_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object p1, v6, v3

    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAuthtoken("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") -> "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    throw v4

    :cond_5
    sget-object v3, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v3

    goto :goto_3
.end method

.method public static declared-synchronized getGLSContext()Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    .locals 6

    const-class v2, Lcom/google/android/gms/auth/login/GLSUser;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/auth/login/GLSUser;->getHttpClient(Landroid/content/Context;)Lcom/google/android/volley/GoogleHttpClient;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v1, v0, v3, v4, v5}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lorg/apache/http/client/HttpClient;Lcom/google/android/gms/auth/login/GLSUser$1;)V

    sput-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    :cond_0
    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method static getHttpClient(Landroid/content/Context;)Lcom/google/android/volley/GoogleHttpClient;
    .locals 5
    .param p0    # Landroid/content/Context;

    const/16 v4, 0x7530

    new-instance v0, Lcom/google/android/volley/GoogleHttpClient;

    const-string v2, "GoogleAuth/1.4"

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/volley/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/volley/GoogleHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const-wide/16 v2, 0x7530

    invoke-static {v1, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    return-object v0
.end method

.method private getICSPermissionKey(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v1, p1}, Lcom/google/android/auth/base/PackageManagerDecorator;->getDefaultAppPackage(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v0, v1}, Lcom/google/android/auth/base/PackageManagerDecorator;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "perm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPermissionKey(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "perm."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, p1, p3}, Lcom/google/android/gms/auth/login/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handlePermission(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Z",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->PERMISSION_ADVICE:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    if-eqz p5, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-nez p8, :cond_1

    if-eqz p7, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/gms/auth/login/Status;->NEED_PERMISSION:Lcom/google/android/gms/auth/login/Status;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gms/auth/login/Status;ILjava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "auto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "1"

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->STORE_CONSENT_REMOTELY:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p6, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p0, p3, p2, v1, v0}, Lcom/google/android/gms/auth/login/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v0, "1"

    iget-object v2, p6, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p0, p3, p2, v0, v2}, Lcom/google/android/gms/auth/login/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/google/android/gms/auth/login/Status;->NEED_PERMISSION:Lcom/google/android/gms/auth/login/Status;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gms/auth/login/Status;ILjava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private hasPermission(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 9

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "GLSUser"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-nez p2, :cond_1

    if-eqz v3, :cond_0

    const-string v0, "GLSUser"

    const-string v2, "has_permission because uid = 0"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mTestNoPermission:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    const-string v0, "Not has_permission because %s = %s"

    const-string v3, "GLSUser"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v2

    sget-object v5, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v5, v5, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mTestNoPermission:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v0, p2}, Lcom/google/android/auth/base/PackageManagerDecorator;->inSystemImage(I)Z

    move-result v4

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne p2, v5, :cond_5

    move v0, v1

    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v6, v5, p2}, Lcom/google/android/auth/base/PackageManagerDecorator;->sameSignature(II)Z

    move-result v5

    if-nez v4, :cond_4

    if-nez v0, :cond_4

    if-eqz v5, :cond_6

    :cond_4
    if-eqz v3, :cond_0

    const-string v3, "has_permission because either isSystem (%b), isSameUser (%b) or isSameSignature (%b)"

    const-string v6, "GLSUser"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser;->getStoredPermission(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    if-eqz v3, :cond_7

    const-string v3, "has_permission: %b - tokenType: %s, uid: %d, packageName:  %s)"

    const-string v4, "GLSUser"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object p1, v5, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v8

    aput-object p3, v5, v7

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v1, v0

    goto/16 :goto_0
.end method

.method private hasSecret()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 7
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, " Request: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/NameValuePair;

    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/auth/login/RequestKey;->TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/gms/auth/login/RequestKey;->ENCRYPTED_PASSWORD:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "&"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " RESULT: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " {"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " } Message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    return-void
.end method

.method public static parseConsentDataBytes(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-object v2

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/Base64Utils;->decodeUrlSafeNoPadding(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/auth/login/LsoProto$ConsentData;->parseFrom([B)Lcom/google/android/gms/auth/login/LsoProto$ConsentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$ConsentData;->getScopeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    new-instance v4, Lcom/google/android/gms/common/acl/ScopeData$Builder;

    invoke-direct {v4, v3}, Lcom/google/android/gms/common/acl/ScopeData$Builder;-><init>(Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;)V

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->build()Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private parseResponse(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_0

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Z",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/auth/login/GLSUser;->getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->STATUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/auth/login/GLSUser;->handlePermission(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/auth/login/GLSUser;->processTokenResponse(Ljava/util/Map;Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    if-eqz v9, :cond_8

    const-string v1, "0"

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->CAN_UPGRADE_PLUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/GLSSession;->setGooglePlusAllowed(Z)V

    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-eqz v1, :cond_5

    const-string v1, "SID"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "LSID"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->EXPIRY_IN_S:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object v5, p0

    move-object v6, p3

    move v7, p2

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/auth/login/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v1, "SID"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v1, "LSID"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    if-eqz v5, :cond_3

    const-string v2, "SID"

    const/4 v6, 0x0

    move-object v1, p0

    move v3, p2

    move-object v4, v8

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v7, :cond_4

    const-string v2, "LSID"

    const/4 v6, 0x0

    move-object v1, p0

    move v3, p2

    move-object v4, v8

    move-object v5, v7

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->updateSecret()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-nez v1, :cond_7

    const/4 v11, 0x1

    :goto_3
    move-object v6, p0

    move-object v7, p1

    move-object v8, p3

    move-object/from16 v10, p6

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gms/auth/login/GLSUser;->successIntent(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Z)Landroid/content/Intent;

    move-result-object v1

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    const/4 v11, 0x0

    goto :goto_3

    :cond_8
    if-nez v8, :cond_9

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t get error message from reply:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->SERVICE_UNAVAILABLE:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v8

    :cond_9
    const-string v1, "badauth"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v8

    :cond_a
    invoke-static {v8}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v3

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v1, :cond_b

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->INFO:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/auth/login/Status;->NEEDS_2F:Lcom/google/android/gms/auth/login/Status;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v3, Lcom/google/android/gms/auth/login/Status;->NEEDS_2F:Lcom/google/android/gms/auth/login/Status;

    :cond_b
    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/auth/login/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gms/auth/login/Status;ILjava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v4

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->CAPTCHA:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v1, :cond_d

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_URL:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {p0, v1, v2, v4, v0}, Lcom/google/android/gms/auth/login/GLSUser;->getCaptchaData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gms/auth/login/GLSSession;)V

    :cond_c
    :goto_4
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLS error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    goto/16 :goto_0

    :cond_d
    sget-object v1, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v1, :cond_f

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_e

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->resetPassword()V

    goto :goto_4

    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    if-nez v1, :cond_c

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->resetPassword()V

    goto :goto_4

    :cond_f
    sget-object v1, Lcom/google/android/gms/auth/login/Status;->CLIENT_LOGIN_DISABLED:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v1, :cond_c

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->resetPassword()V

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v5, "1"

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private processTokenResponse(Ljava/util/Map;Z)V
    .locals 5
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->TOKEN:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mHash:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private refreshServicesAndSyncAdapters(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v8, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v8}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v6, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v8, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v8}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, p1}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v5

    move-object v0, v5

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/4 v8, -0x1

    invoke-static {v6, v7, v8}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    invoke-static {v6, v7, v8}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private successIntent(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Z)Landroid/content/Intent;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    const/4 v8, 0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "accountType"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-nez p3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/login/GLSUser;->getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    if-nez p5, :cond_1

    const-string v0, "authtoken"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    sget-object v6, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICE_GPLUS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p4, v8}, Lcom/google/android/gms/auth/login/GLSSession;->setGooglePlus(Z)V

    :cond_2
    sget-object v6, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICE_ES_MOBILE:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p4, v8}, Lcom/google/android/gms/auth/login/GLSSession;->setESMobile(Z)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GLSUser;->refreshServicesAndSyncAdapters(Ljava/lang/String;)V

    :cond_5
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->FIRST_NAME:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->LAST_NAME:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    iput-object v0, p4, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    iput-object v1, p4, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    :cond_6
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->PICASA_USER:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_7

    iput-object v0, p4, Lcom/google/android/gms/auth/login/GLSSession;->mPicasaUser:Ljava/lang/String;

    :cond_7
    return-object v2
.end method


# virtual methods
.method public cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const-string v1, "weblogin:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not caching since unable to generate a cache key for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", service "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0, p4}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EXP:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkGplus(Lcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;
    .locals 8
    .param p1    # Lcom/google/android/gms/auth/login/GLSSession;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v1, "LSID"

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    move-object v0, p0

    move-object v3, p1

    move-object v6, v4

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    const-string v3, "LSID"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createProfile(Lcom/google/android/gms/auth/login/GLSSession;I)Lorg/json/JSONObject;
    .locals 7

    :try_start_0
    new-instance v6, Lorg/json/JSONStringer;

    invoke-direct {v6}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v6}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GLSUser;->prepareRequest(Lorg/json/JSONStringer;)V

    const-string v1, "LSID"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v2, p2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->jsonError(Lcom/google/android/gms/auth/login/Status;)Lorg/json/JSONObject;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->LSID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->FIRST_NAME:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->LAST_NAME:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->GENDER:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/GLSSession;->hasAgreedToPersonalizedContent()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->AGREE_PERSONALIZED_CONTENT:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/GLSSession;->hasAgreedToMobileTOS()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->AGREE_MOBILE_TOS:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/GLSSession;->isCreatingAccount()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->CREATED:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_4
    invoke-static {p1, v6}, Lcom/google/android/gms/auth/login/GLSUser;->addPhoto(Lcom/google/android/gms/auth/login/GLSSession;Lorg/json/JSONStringer;)V

    invoke-virtual {v6}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    const-string v1, "https://android.clients.google.com/setup/createprofile"

    const-string v2, "createProfile"

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createProfile: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->log(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->refreshServicesAndSyncAdapters(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->jsonError(Lcom/google/android/gms/auth/login/Status;)Lorg/json/JSONObject;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public errorIntent(Ljava/util/Map;Lcom/google/android/gms/auth/login/Status;ILjava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/auth/login/Status;",
            "I",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/auth/login/LoginActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-nez p5, :cond_0

    invoke-static {}, Lcom/google/android/gms/auth/login/GLSSession;->create()Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object p5

    :cond_0
    const-string v3, "session"

    invoke-virtual {v2, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iput p3, p5, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    const-string v3, "authAccount"

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v3, v2}, Lcom/google/android/gms/auth/login/Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V

    const-string v3, "authFailedMessage"

    const v4, 0x7f0b00a4

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    if-eqz p1, :cond_4

    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->SCOPE_CONSENT_DESCRIPTION:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v3, "\\|"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iget-object v0, p5, Lcom/google/android/gms/auth/login/GLSSession;->mPermission:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    :try_start_0
    iget-object v6, p5, Lcom/google/android/gms/auth/login/GLSSession;->mPermission:Ljava/util/ArrayList;

    const-string v7, "UTF-8"

    invoke-static {v5, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v6, "GLSUser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error decoding "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->SCOPE_CONSENT_DETAILS:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v3, "\\|"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iget-object v0, p5, Lcom/google/android/gms/auth/login/GLSSession;->mPermissionDetails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    array-length v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_2

    aget-object v1, v3, v0

    :try_start_1
    iget-object v5, p5, Lcom/google/android/gms/auth/login/GLSSession;->mPermissionDetails:Ljava/util/ArrayList;

    const-string v6, "UTF-8"

    invoke-static {v1, v6}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v5

    const-string v5, "GLSUser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error decoding "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_2
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->CONSENT_DATA_BASE64:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :try_start_2
    iget-object v1, p5, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p5, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/GLSUser;->parseConsentDataBytes(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_4
    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->DETAIL:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p5, Lcom/google/android/gms/auth/login/GLSSession;->mDetail:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/ResponseKey;->URL:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p5, Lcom/google/android/gms/auth/login/GLSSession;->mUrl:Ljava/lang/String;

    :cond_4
    return-object v2

    :catch_2
    move-exception v0

    const-string v1, "GLSUser"

    const-string v3, "Failed to parse consent data"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, p3, Lcom/google/android/gms/auth/login/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    iget-object v0, p3, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/auth/login/GLSUser;->hasPermission(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getAuthtoken(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gms/auth/login/GLSSession;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z

    if-nez p1, :cond_0

    const-string p1, "ac2dm"

    :cond_0
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GLSUser;->hasSecret()Z

    move-result v1

    if-eqz v1, :cond_2

    move/from16 v0, p2

    invoke-virtual {p0, p1, v0, v13}, Lcom/google/android/gms/auth/login/GLSUser;->getCachedToken(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v1, "GLSUser"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Return from cache "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->successIntent(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/login/GLSSession;Z)Landroid/content/Intent;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_2
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v5, p0

    move-object v6, p1

    move/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v6

    move-object v5, p0

    move/from16 v7, p2

    move-object v8, p1

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p3

    invoke-direct/range {v5 .. v11}, Lcom/google/android/gms/auth/login/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method getCachedToken(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    if-nez p1, :cond_1

    move-object v2, v5

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    if-nez p2, :cond_2

    move-object v2, v5

    goto :goto_0

    :cond_2
    const-string v6, "weblogin:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v2, v5

    goto :goto_0

    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v2, v5

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v6, "GLSUser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "not checking cache since unable to generate a cache key for uid "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", service "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v5

    goto :goto_0

    :cond_5
    sget-object v6, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v6, v7, v0}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v6, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EXP:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    cmp-long v6, v3, v6

    if-gez v6, :cond_0

    sget-object v6, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v7, "com.google"

    invoke-virtual {v6, v7, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v5

    goto/16 :goto_0
.end method

.method getCaptchaData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gms/auth/login/GLSSession;)V
    .locals 4

    const/4 v2, 0x2

    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    const-string v0, "GLSUser"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "captcha url is ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsSetThreadStatsTag(I)V

    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    const-string v1, "GLSUser"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap response is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v1, "X-Google-Captcha-Error"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/auth/login/Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    :goto_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://www.google.com/accounts/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_3
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    iput-object p1, p4, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaToken:Ljava/lang/String;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p4, Lcom/google/android/gms/auth/login/GLSSession;->mCaptchaBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/google/android/gms/auth/login/Status;->NETWORK_ERROR:Lcom/google/android/gms/auth/login/Status;

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/auth/login/Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->trafficStatsClearThreadStatsTag()V

    throw v0
.end method

.method public getStoredPermission(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 3

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, p2, p1, p3}, Lcom/google/android/gms/auth/login/GLSUser;->getPermissionKey(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p2, p1}, Lcom/google/android/gms/auth/login/GLSUser;->getICSPermissionKey(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/auth/login/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "perm."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/auth/login/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasService(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v7, v7, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v8, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v9, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICES:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v9}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    const-string v7, ","

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    array-length v5, v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v0, v3, v4

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public injectConditionalTestResponse(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->getInjector()Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->injectConditionalTestResponse(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public injectNoPermission(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iput-object p1, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mTestNoPermission:Ljava/lang/String;

    return-void
.end method

.method public injectTestResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->getInjector()Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->injectTestResponse(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public invalidateToken(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v0}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    invoke-virtual {v2, v3, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isBrowser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    return v0
.end method

.method public prepareRequest(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/gms/auth/login/GLSSession;",
            "Landroid/os/Bundle;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const/4 v5, 0x0

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "accountType"

    const-string v2, "HOSTED_OR_GOOGLE"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->GOOGLE_PLAY_SERVICES_VERSION:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsPkgVersion:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->SYSTEM_APP:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v0, p2}, Lcom/google/android/auth/base/PackageManagerDecorator;->inSystemImage(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "1"

    :goto_0
    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->EMAIL:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p5, :cond_1

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->STORED_PERMISSION:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p8, :cond_2

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->ADDED_ACCOUNT:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p3}, Lcom/google/android/gms/auth/login/GLSSession;->isCreatingAccount()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->CREATED:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    if-eqz v0, :cond_b

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->ENCRYPTED_PASSWORD:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->ACCOUNT_SOURCE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->ANDROID_ID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->CAPTCHA_TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->CAPTCHA_ANSWER:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    if-eqz v0, :cond_6

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->BROWSER_FLOW:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    if-eqz p2, :cond_12

    iget-object v0, p3, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    if-eqz v0, :cond_7

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->PACKAGE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->PACKAGE_SIG:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mPMD:Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {v3, v0}, Lcom/google/android/auth/base/PackageManagerDecorator;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz p4, :cond_12

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->CLIENT_ID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->CLIENT_ID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-virtual {p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->OAUTH2_EXTRA_PREFIX:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_a
    const-string v0, "0"

    goto/16 :goto_0

    :cond_b
    if-nez p7, :cond_c

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_c
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->ACCESS_TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_d
    const-string v0, "request_visible_actions"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->REQUEST_VISIBLE_ACTIONS:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v0, p3, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v2

    if-eqz v2, :cond_10

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v3, Lcom/google/android/gms/auth/login/RequestKey;->PACL_PICKER_DATA:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->getPaclPickerData()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->isAllCirclesVisible()Z

    move-result v2

    if-eqz v2, :cond_11

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->VISIBLE_EDGES:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->ALL_CIRCLES_VISIBLE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_11
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v3, Lcom/google/android/gms/auth/login/RequestKey;->VISIBLE_EDGES:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->getVisibleEdges()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->ALL_CIRCLES_VISIBLE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_12
    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_country"

    invoke-static {v0, v1, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p6, v1, v0}, Lcom/google/android/gms/auth/login/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p6, v1, v0}, Lcom/google/android/gms/auth/login/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->LANGUAGE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p6, v0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->SDK_VERSION:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p6, v0, v1}, Lcom/google/android/gms/auth/login/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public prepareRequest(Lorg/json/JSONStringer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->EMAIL:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->ANDROID_ID:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->CAPTCHA_TOKEN:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->CAPTCHA_ANSWER:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->BROWSER_FLOW:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_2
    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_country"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v1, Lcom/google/android/gms/auth/login/RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gms/auth/login/RequestKey;->LANGUAGE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    return-void
.end method

.method resetPassword()V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->existing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setCaptcha(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/auth/login/GLSUser;->captchaAnswer:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->encryptPassword(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEmail:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->hashPassword(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mHash:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    return-void
.end method

.method public setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, p2, p1, p4}, Lcom/google/android/gms/auth/login/GLSUser;->getPermissionKey(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    return-void
.end method

.method public updateSecret()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mUseBrowserFlow:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser;->mHash:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/login/GLSUser;->sGLSContext:Lcom/google/android/gms/auth/login/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GLSUser;->mHash:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateWithPassword(Lcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;
    .locals 9
    .param p1    # Lcom/google/android/gms/auth/login/GLSSession;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    const-string v1, "ac2dm"

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    const/4 v7, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    const-string v3, "ac2dm"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v8

    const-string v0, "authtoken"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->updateSecret()V

    :cond_0
    return-object v8
.end method

.method public updateWithRequestToken(Lcom/google/android/gms/auth/login/GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 9
    .param p1    # Lcom/google/android/gms/auth/login/GLSSession;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    const-string v1, "ac2dm"

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    if-nez p3, :cond_1

    move v7, v5

    :goto_0
    move-object v0, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/auth/login/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gms/auth/login/GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    iput-object v4, p1, Lcom/google/android/gms/auth/login/GLSSession;->mAccessToken:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/auth/login/GLSUser;->mGmsUID:I

    const-string v3, "ac2dm"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v8

    const-string v0, "authtoken"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GLSUser;->updateSecret()V

    :cond_0
    return-object v8

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method
