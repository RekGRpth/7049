.class public Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;
.super Ljava/lang/Object;
.source "GrantCredentialsWithAclActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# instance fields
.field private final mScopeData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation
.end field

.field private final mStatus:Lcom/google/android/gms/auth/login/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/login/Status;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/auth/login/Status;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/auth/login/Status;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->mStatus:Lcom/google/android/gms/auth/login/Status;

    iput-object p2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->mScopeData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getScopeData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->mScopeData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/gms/auth/login/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->mStatus:Lcom/google/android/gms/auth/login/Status;

    return-object v0
.end method
