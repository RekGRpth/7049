.class public Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "GrantCredentialsWithAclActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/auth/login/ScopeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;
    }
.end annotation


# static fields
.field private static final sAppNamePattern:Ljava/util/regex/Pattern;

.field private static final sLineBreakPattern:Ljava/util/regex/Pattern;


# instance fields
.field private mAccount:Lcom/google/android/auth/base/GoogleAccount;

.field private mApplicationLabel:Ljava/lang/String;

.field private mCallingPackage:Ljava/lang/String;

.field private mCallingUid:I

.field private mLastScopeIndex:I

.field private mScopeData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation
.end field

.field private mScopesLayout:Landroid/widget/LinearLayout;

.field private mService:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "<placeholder\\s*id=[\'\"]app_name[\'\"]\\s*/?>(.*</placeholder>)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->sAppNamePattern:Ljava/util/regex/Pattern;

    const-string v0, "<br\\s*/?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->sLineBreakPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    return-void
.end method

.method private addScopesForSessionScopeData()V
    .locals 10

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_3

    iget-object v8, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragmentTag(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_0

    const v8, 0x7f0a0040

    invoke-static {v3, v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->newInstance(ILcom/google/android/gms/common/acl/ScopeData;)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v9

    invoke-virtual {v1, v8, v9, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_4
    if-eqz v2, :cond_5

    const v8, 0x7f0b00b0

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setTitle(I)V

    :cond_5
    return-void
.end method

.method private clearSessionScopeData()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/acl/ScopeData;->setPaclPickerData(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/acl/ScopeData;->setVisibleEdges(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/acl/ScopeData;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/acl/ScopeData;->setAllCirclesVisible(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "callingPkg"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "callingUid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "service"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "acctName"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "scopeData"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v0
.end method

.method private createResponseIntent(Lcom/google/android/gms/auth/login/Status;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/android/gms/auth/login/Status;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "status"

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/Status;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "scopeData"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v0
.end method

.method private getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragmentTag(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/ScopeFragment;

    return-object v0
.end method

.method private getScopeFragmentTag(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scopeFragment"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "callingPkg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    const-string v0, "callingUid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingUid:I

    const-string v0, "service"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    const-string v0, "acctName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/auth/base/GoogleAccount;->retrieve(Ljava/lang/String;)Lcom/google/android/auth/base/GoogleAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    const-string v0, "scopeData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    const-string v0, "lastScopeIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    return-void
.end method

.method private static isSwarmAccessType(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audience:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private logAclCancelAnalyticsEvent()V
    .locals 5

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasPaclAudienceView()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasFriendPickerData()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    sget-object v4, Lcom/google/android/gms/common/analytics/Aspen$Action;->CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v4}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private logAclToBeSentToLsoIfDebugBuild()V
    .locals 6

    invoke-static {p0}, Lcom/google/android/gms/common/util/AndroidUtils;->isDebugBuild(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "GLSActivity"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasPaclAudienceView()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasFriendPickerData()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->toDebugString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v4, "GLSActivity"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/common/analytics/Aspen$View;->MAIN_CONSENT_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method private logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p2    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method private logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/common/analytics/Aspen$View;->MAIN_CONSENT_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method private logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p3    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            ")V"
        }
    .end annotation

    new-instance v1, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v2}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setStartView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setLoggedCircle(Ljava/util/ArrayList;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusEvent(Landroid/content/Context;Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;)V

    return-void
.end method

.method private logSessionScopeDataIfDebugBuild()V
    .locals 6

    invoke-static {p0}, Lcom/google/android/gms/common/util/AndroidUtils;->isDebugBuild(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "GLSActivity"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->toDebugString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v4, "GLSActivity"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private static parseSwarmName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    const-string v0, "audience:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected auth token type to start with \'audience:\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "audience:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static replaceAppName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->sAppNamePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static unpackResponse(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;
    .locals 3
    .param p0    # Landroid/content/Intent;

    new-instance v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;

    const-string v1, "status"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/login/Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v1

    const-string v2, "scopeData"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;-><init>(Lcom/google/android/gms/auth/login/Status;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method private updateSessionAcls()V
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasPaclAudienceView()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->getPaclSharingRoster()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/acl/ScopeData;->setPaclPickerData(Ljava/lang/String;)V

    const/4 v2, 0x1

    sget-object v6, Lcom/google/android/gms/common/analytics/Aspen$Action;->AUTH_COMPLETE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->getPaclLoggedCircles()Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;)V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->hasFriendPickerData()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->getVisibleEdges()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/acl/ScopeData;->setVisibleEdges(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->isAllCirclesVisible()Z

    move-result v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/acl/ScopeData;->setAllCirclesVisible(Z)V

    const/4 v1, 0x1

    sget-object v6, Lcom/google/android/gms/common/analytics/Aspen$Action;->FACL_UPDATED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/ScopeFragment;->getFaclLoggedCircles()Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v8}, Lcom/google/android/gms/common/acl/ScopeData;->setPaclPickerData(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Lcom/google/android/gms/common/acl/ScopeData;->setVisibleEdges(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/acl/ScopeData;->setAllCirclesVisible(Z)V

    goto :goto_1

    :cond_2
    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    sget-object v6, Lcom/google/android/gms/common/analytics/Aspen$Action;->ACCEPT_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_4
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, -0x1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->enablePacl()V

    if-ne p2, v3, :cond_1

    new-instance v2, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    invoke-direct {v2, p3}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAudience()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->setPaclAudience(Ljava/util/ArrayList;)V

    sget-object v2, Lcom/google/android/gms/common/analytics/Sharebox$Action;->ACL_VIEW_ACCEPTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->maybeSetPaclPrivateRadio()V

    sget-object v2, Lcom/google/android/gms/common/analytics/Sharebox$Action;->ACL_VIEW_ABANDONED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    iget v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getScopeFragment(I)Lcom/google/android/gms/auth/login/ScopeFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/ScopeFragment;->enableFacl()V

    if-ne p2, v3, :cond_3

    new-instance v2, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    invoke-direct {v2, p3}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAudience()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->setFaclAudience(Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->isEveryoneChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/ScopeFragment;->setAllCirclesVisible(Z)V

    sget-object v2, Lcom/google/android/gms/common/analytics/Aspen$Action;->VISIBLE_FRIENDS_OK_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/Aspen$View;->FRIEND_PICKER:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/google/android/gms/common/analytics/Aspen$Action;->VISIBLE_FRIENDS_CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/Aspen$View;->FRIEND_PICKER:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->clearSessionScopeData()V

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logAclCancelAnalyticsEvent()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingUid:I

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/auth/base/GoogleAccount;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->PERMISSION_DENIED:Lcom/google/android/gms/auth/login/Status;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->createResponseIntent(Lcom/google/android/gms/auth/login/Status;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->updateSessionAcls()V

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logAclToBeSentToLsoIfDebugBuild()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingUid:I

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    const-string v4, "1"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/auth/base/GoogleAccount;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->createResponseIntent(Lcom/google/android/gms/auth/login/Status;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a003e
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v13, 0x1

    const/4 v12, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v13}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->requestWindowFeature(I)Z

    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->initState(Landroid/os/Bundle;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logSessionScopeDataIfDebugBuild()V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v6}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    if-nez v6, :cond_4

    :cond_0
    const-string v6, "GLSActivity"

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v8, "GLSActivity"

    const-string v9, "%s started with username=%s callingPackage=%s service=%s"

    const/4 v6, 0x4

    new-array v10, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v12

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v6}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_3

    move-object v6, v7

    :goto_1
    aput-object v6, v10, v13

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    aput-object v7, v10, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    aput-object v7, v10, v6

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0, v12}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    :goto_2
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->initState(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    const-string v6, "<omitted>"

    goto :goto_1

    :cond_4
    const v6, 0x7f04000f

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setContentView(I)V

    const v6, 0x7f0a0040

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopesLayout:Landroid/widget/LinearLayout;

    const v6, 0x7f0a003e

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/auth/base/PackageManagerDecorator;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/google/android/auth/base/PackageManagerDecorator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-virtual {v0, v3}, Lcom/google/android/auth/base/PackageManagerDecorator;->getApplicationIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v3}, Lcom/google/android/auth/base/PackageManagerDecorator;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    if-nez v6, :cond_7

    :cond_5
    const-string v6, "GLSActivity"

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "GLSActivity"

    const-string v7, "Failed to get ApplicationInfo for package: %s"

    new-array v8, v13, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    aput-object v9, v8, v12

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {p0, v12}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    goto :goto_2

    :cond_7
    const v6, 0x7f0a003d

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b00b5

    new-array v10, v13, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f0a003c

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v6, 0x7f0a0041

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b00ba

    new-array v10, v13, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->isSwarmAccessType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0b00b7

    new-array v7, v13, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->parseSwarmName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-virtual {p0, v6, v7}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    :cond_8
    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_b

    :cond_9
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    invoke-virtual {v0, p0, v6}, Lcom/google/android/auth/base/PackageManagerDecorator;->getAuthTokenLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    :cond_a
    new-instance v6, Lcom/google/android/gms/common/acl/ScopeData$Builder;

    invoke-direct {v6, v1, v7}, Lcom/google/android/gms/common/acl/ScopeData$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->build()Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->addScopesForSessionScopeData()V

    goto/16 :goto_2
.end method

.method public onDetailsClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->replaceAppName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/auth/login/ScopeDetailsDialogFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/login/ScopeDetailsDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "scopeDetailsDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/login/ScopeDetailsDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "callingPkg"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "callingUid"

    iget v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mCallingUid:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mService:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "acctName"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v1}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "scopeData"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mScopeData:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "lastScopeIndex"

    iget v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onSelectFaclAudience(ILjava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const/4 v3, 0x0

    iput p1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    invoke-static {p0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getCircleSelectionActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v2}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    if-nez p2, :cond_0

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setAudience(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mApplicationLabel:Ljava/lang/String;

    invoke-static {p3, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->replaceAppName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setDescription(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setEveryoneChecked(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setLoadGroups(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setLoadPeople(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setOkText(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setShowChips(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setShowCancel(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    const v2, 0x7f0b00bd

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v1, Lcom/google/android/gms/common/analytics/Aspen$Action;->FRIEND_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_0
    return-void

    :cond_1
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSelectPaclAudience(ILjava/util/ArrayList;)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput p1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mLastScopeIndex:I

    invoke-static {p0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAudienceSelectionActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->mAccount:Lcom/google/android/auth/base/GoogleAccount;

    invoke-virtual {v2}, Lcom/google/android/auth/base/GoogleAccount;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setAudience(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    if-nez p2, :cond_0

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setKnownAudienceMembers(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    const v2, 0x7f0b00bb

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v1, Lcom/google/android/gms/common/analytics/Aspen$Action;->CIRCLE_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->logPlusAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_0
    return-void

    :cond_1
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
