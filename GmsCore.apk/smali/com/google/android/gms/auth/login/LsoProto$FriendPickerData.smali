.class public final Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LsoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/LsoProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FriendPickerData"
.end annotation


# instance fields
.field private activityText_:Ljava/lang/String;

.field private allCirclesVisible_:Z

.field private cachedSize:I

.field private hasActivityText:Z

.field private hasAllCirclesVisible:Z

.field private hasShowSpeedbump:Z

.field private hasSpeedbumpText:Z

.field private hasVisibleEdges:Z

.field private showSpeedbump_:Z

.field private speedbumpText_:Ljava/lang/String;

.field private visibleEdges_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->visibleEdges_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->activityText_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->showSpeedbump_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->speedbumpText_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->allCirclesVisible_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActivityText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->activityText_:Ljava/lang/String;

    return-object v0
.end method

.method public getAllCirclesVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->allCirclesVisible_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasVisibleEdges()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getVisibleEdges()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasActivityText()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getActivityText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasShowSpeedbump()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getShowSpeedbump()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasSpeedbumpText()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getSpeedbumpText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasAllCirclesVisible()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getAllCirclesVisible()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->cachedSize:I

    return v0
.end method

.method public getShowSpeedbump()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->showSpeedbump_:Z

    return v0
.end method

.method public getSpeedbumpText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->speedbumpText_:Ljava/lang/String;

    return-object v0
.end method

.method public getVisibleEdges()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->visibleEdges_:Ljava/lang/String;

    return-object v0
.end method

.method public hasActivityText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasActivityText:Z

    return v0
.end method

.method public hasAllCirclesVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasAllCirclesVisible:Z

    return v0
.end method

.method public hasShowSpeedbump()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasShowSpeedbump:Z

    return v0
.end method

.method public hasSpeedbumpText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasSpeedbumpText:Z

    return v0
.end method

.method public hasVisibleEdges()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasVisibleEdges:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->setVisibleEdges(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->setActivityText(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->setShowSpeedbump(Z)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->setSpeedbumpText(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->setAllCirclesVisible(Z)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    move-result-object v0

    return-object v0
.end method

.method public setActivityText(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasActivityText:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->activityText_:Ljava/lang/String;

    return-object p0
.end method

.method public setAllCirclesVisible(Z)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasAllCirclesVisible:Z

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->allCirclesVisible_:Z

    return-object p0
.end method

.method public setShowSpeedbump(Z)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasShowSpeedbump:Z

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->showSpeedbump_:Z

    return-object p0
.end method

.method public setSpeedbumpText(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasSpeedbumpText:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->speedbumpText_:Ljava/lang/String;

    return-object p0
.end method

.method public setVisibleEdges(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasVisibleEdges:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->visibleEdges_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasVisibleEdges()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getVisibleEdges()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasActivityText()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getActivityText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasShowSpeedbump()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getShowSpeedbump()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasSpeedbumpText()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getSpeedbumpText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->hasAllCirclesVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getAllCirclesVisible()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    return-void
.end method
