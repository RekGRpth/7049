.class public final Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LsoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/LsoProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OAuthClientInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private developerEmail_:Ljava/lang/String;

.field private hasDeveloperEmail:Z

.field private hasIconUri:Z

.field private hasName:Z

.field private iconUri_:Ljava/lang/String;

.field private name_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->iconUri_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->developerEmail_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->cachedSize:I

    return v0
.end method

.method public getDeveloperEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->developerEmail_:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->iconUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasIconUri()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getIconUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasDeveloperEmail()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getDeveloperEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->cachedSize:I

    return v0
.end method

.method public hasDeveloperEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasDeveloperEmail:Z

    return v0
.end method

.method public hasIconUri()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasIconUri:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasName:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->setName(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->setIconUri(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->setDeveloperEmail(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public setDeveloperEmail(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasDeveloperEmail:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->developerEmail_:Ljava/lang/String;

    return-object p0
.end method

.method public setIconUri(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasIconUri:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->iconUri_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasName:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasIconUri()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getIconUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->hasDeveloperEmail()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$OAuthClientInfo;->getDeveloperEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
