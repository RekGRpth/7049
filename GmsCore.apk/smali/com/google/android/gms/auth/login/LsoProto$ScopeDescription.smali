.class public final Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LsoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/LsoProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScopeDescription"
.end annotation


# instance fields
.field private cachedSize:I

.field private description_:Ljava/lang/String;

.field private detail_:Ljava/lang/String;

.field private developerCode_:Ljava/lang/String;

.field private friendPickerData_:Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

.field private hasDescription:Z

.field private hasDetail:Z

.field private hasDeveloperCode:Z

.field private hasFriendPickerData:Z

.field private hasIcon:Z

.field private hasPAclPickerData:Z

.field private icon_:Ljava/lang/String;

.field private pAclPickerData_:Ljava/lang/String;

.field private warning_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->detail_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->icon_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->pAclPickerData_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->friendPickerData_:Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->developerCode_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->warning_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addWarning(Lcom/google/android/gms/auth/login/LsoProto$Warning;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/login/LsoProto$Warning;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->warning_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->warning_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->warning_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->detail_:Ljava/lang/String;

    return-object v0
.end method

.method public getDeveloperCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->developerCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendPickerData()Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->friendPickerData_:Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->icon_:Ljava/lang/String;

    return-object v0
.end method

.method public getPAclPickerData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->pAclPickerData_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDescription()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDetail()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDetail()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasIcon()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getIcon()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasPAclPickerData()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getPAclPickerData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasFriendPickerData()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getFriendPickerData()Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDeveloperCode()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDeveloperCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getWarningList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    iput v2, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->cachedSize:I

    return v2
.end method

.method public getWarningList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->warning_:Ljava/util/List;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDescription:Z

    return v0
.end method

.method public hasDetail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDetail:Z

    return v0
.end method

.method public hasDeveloperCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDeveloperCode:Z

    return v0
.end method

.method public hasFriendPickerData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasFriendPickerData:Z

    return v0
.end method

.method public hasIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasIcon:Z

    return v0
.end method

.method public hasPAclPickerData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasPAclPickerData:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setDescription(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setDetail(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setIcon(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setPAclPickerData(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    invoke-direct {v1}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setFriendPickerData(Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->setDeveloperCode(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    invoke-direct {v1}, Lcom/google/android/gms/auth/login/LsoProto$Warning;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->addWarning(Lcom/google/android/gms/auth/login/LsoProto$Warning;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    move-result-object v0

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDescription:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setDetail(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDetail:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->detail_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeveloperCode(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDeveloperCode:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->developerCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setFriendPickerData(Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasFriendPickerData:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->friendPickerData_:Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    return-object p0
.end method

.method public setIcon(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasIcon:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->icon_:Ljava/lang/String;

    return-object p0
.end method

.method public setPAclPickerData(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasPAclPickerData:Z

    iput-object p1, p0, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->pAclPickerData_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDetail()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDetail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasIcon()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getIcon()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasPAclPickerData()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getPAclPickerData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasFriendPickerData()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getFriendPickerData()Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasDeveloperCode()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDeveloperCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getWarningList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    return-void
.end method
