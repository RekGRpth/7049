.class Lcom/google/android/gms/auth/login/PasswordEncrypter;
.super Ljava/lang/Object;
.source "PasswordEncrypter.java"


# static fields
.field private static HEX_CHARS:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/auth/login/PasswordEncrypter;->HEX_CHARS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static computeSha1Hash(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x2

    if-ge v3, v6, :cond_1

    :try_start_0
    const-string v6, "SHA-1"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v6, 0x1

    if-lt v3, v6, :cond_0

    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    :cond_1
    :try_start_1
    const-string v6, "UTF-8"

    invoke-virtual {p0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    :goto_1
    array-length v6, v1

    if-ge v2, v6, :cond_2

    sget-object v6, Lcom/google/android/gms/auth/login/PasswordEncrypter;->HEX_CHARS:[C

    aget-byte v7, v1, v2

    and-int/lit16 v7, v7, 0xff

    ushr-int/lit8 v7, v7, 0x4

    aget-char v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    sget-object v6, Lcom/google/android/gms/auth/login/PasswordEncrypter;->HEX_CHARS:[C

    aget-byte v7, v1, v2

    and-int/lit16 v7, v7, 0xff

    and-int/lit8 v7, v7, 0xf

    aget-char v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    return-object v6

    :catch_1
    move-exception v0

    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method public static createKeyFromString(Ljava/lang/String;[B)Ljava/security/PublicKey;
    .locals 14
    .param p0    # Ljava/lang/String;
    .param p1    # [B

    const/4 v10, 0x4

    const/4 v13, 0x1

    const/4 v11, 0x0

    invoke-static {p0, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0, v11}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->readInt([BI)I

    move-result v6

    new-array v9, v6, [B

    invoke-static {v0, v10, v9, v11, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v5, Ljava/math/BigInteger;

    invoke-direct {v5, v13, v9}, Ljava/math/BigInteger;-><init>(I[B)V

    add-int/lit8 v10, v6, 0x4

    invoke-static {v0, v10}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->readInt([BI)I

    move-result v3

    new-array v9, v3, [B

    add-int/lit8 v10, v6, 0x8

    invoke-static {v0, v10, v9, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v2, Ljava/math/BigInteger;

    invoke-direct {v2, v13, v9}, Ljava/math/BigInteger;-><init>(I[B)V

    const/4 v7, 0x0

    :goto_0
    const/4 v10, 0x2

    if-ge v7, v10, :cond_1

    :try_start_0
    const-string v10, "SHA-1"

    invoke-static {v10}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput-byte v11, p1, v10

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x4

    invoke-static {v4, v10, p1, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v10, "RSA"

    invoke-static {v10}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v10

    new-instance v11, Ljava/security/spec/RSAPublicKeySpec;

    invoke-direct {v11, v5, v2}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v10, v11}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    :goto_1
    return-object v10

    :catch_0
    move-exception v1

    if-lt v7, v13, :cond_0

    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    :catch_1
    move-exception v1

    const-string v10, "GoogleLoginService.PasswordEncrypter"

    const-string v11, "received bad google_login_public_key: "

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v10, 0x0

    goto :goto_1

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Execution should never reach here."

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method public static encryptPassword(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2, p0, v0}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->encryptPassword(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static encryptPassword(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u0000"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->encryptString(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static encryptString(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_1

    move-object/from16 v6, p2

    :goto_0
    if-nez v6, :cond_0

    const-string v11, "GoogleLoginService.PasswordEncrypter"

    const-string v12, "no public key available, using default"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "AAAAgMom/1a/v0lblO2Ubrt60J2gcuXSljGFQXgcyZWveWLEwo6prwgi3iJIZdodyhKZQrNWp5nKJ3srRXcUW+F1BD3baEVGcmEgqaLZUNBjm057pKRI16kB0YppeGx5qIQ5QjKzsR8ETQbKLNWgRY0QRNVz34kMJR3P/LgHax/6rmf5AAAAAwEAAQ=="

    :cond_0
    const/4 v11, 0x5

    new-array v3, v11, [B

    invoke-static {v6, v3}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->createKeyFromString(Ljava/lang/String;[B)Ljava/security/PublicKey;

    move-result-object v10

    if-nez v10, :cond_2

    const/4 v11, 0x0

    :goto_1
    return-object v11

    :cond_1
    const-string v11, "google_login_public_key"

    invoke-static {p1, v11}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    :goto_2
    const/4 v11, 0x2

    if-ge v7, v11, :cond_6

    :try_start_0
    const-string v11, "RSA/ECB/OAEPWITHSHA1ANDMGF1PADDING"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    const-string v11, "UTF-8"

    invoke-virtual {p0, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    array-length v11, v9

    add-int/lit8 v11, v11, -0x1

    div-int/lit8 v11, v11, 0x56

    add-int/lit8 v1, v11, 0x1

    mul-int/lit16 v11, v1, 0x85

    new-array v8, v11, [B

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v1, :cond_4

    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    mul-int/lit8 v12, v5, 0x56

    add-int/lit8 v11, v1, -0x1

    if-ne v5, v11, :cond_3

    array-length v11, v9

    mul-int/lit8 v13, v5, 0x56

    sub-int/2addr v11, v13

    :goto_4
    invoke-virtual {v0, v9, v12, v11}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v2

    const/4 v11, 0x0

    mul-int/lit16 v12, v5, 0x85

    array-length v13, v3

    invoke-static {v3, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x0

    mul-int/lit16 v12, v5, 0x85

    array-length v13, v3

    add-int/2addr v12, v13

    array-length v13, v2

    invoke-static {v2, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_3
    const/16 v11, 0x56

    goto :goto_4

    :cond_4
    const/16 v11, 0xa

    invoke-static {v8, v11}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v11

    goto :goto_1

    :catch_0
    move-exception v4

    const/4 v11, 0x1

    if-lt v7, v11, :cond_5

    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    :catch_1
    move-exception v4

    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    :catch_2
    move-exception v4

    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    :catch_3
    move-exception v4

    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    :catch_4
    move-exception v4

    const-string v11, "GoogleLoginService.PasswordEncrypter"

    const-string v12, "error encrypting password: "

    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v11, 0x0

    goto :goto_1

    :catch_5
    move-exception v4

    const-string v11, "GoogleLoginService.PasswordEncrypter"

    const-string v12, "received bad google_login_public_key: "

    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_6
    new-instance v11, Ljava/lang/RuntimeException;

    const-string v12, "Execution should never reach here."

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11
.end method

.method public static hashPassword(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-nez p0, :cond_0

    const-string v3, "GoogleLoginService.PasswordEncrypter"

    const-string v4, "attempt to hash null username"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v3, "null"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "GoogleLoginService.PasswordEncrypter"

    const-string v4, "attempt to hash \"null\" username"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-nez p1, :cond_2

    const-string v3, "GoogleLoginService.PasswordEncrypter"

    const-string v4, "attempt to hash null password"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v3, "null"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "GoogleLoginService.PasswordEncrypter"

    const-string v4, "attempt to hash \"null\" password"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\u0000"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\u0000"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->computeSha1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const-string v4, "AAAAgMom/1a/v0lblO2Ubrt60J2gcuXSljGFQXgcyZWveWLEwo6prwgi3iJIZdodyhKZQrNWp5nKJ3srRXcUW+F1BD3baEVGcmEgqaLZUNBjm057pKRI16kB0YppeGx5qIQ5QjKzsR8ETQbKLNWgRY0QRNVz34kMJR3P/LgHax/6rmf5AAAAAwEAAQ=="

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/auth/login/PasswordEncrypter;->encryptString(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    :goto_0
    return-object v1

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static readInt([BI)I
    .locals 2
    .param p0    # [B
    .param p1    # I

    const/4 v0, 0x0

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method
