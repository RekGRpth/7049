.class public interface abstract Lcom/google/android/gms/auth/login/ScopeListener;
.super Ljava/lang/Object;
.source "ScopeListener.java"


# virtual methods
.method public abstract onDetailsClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
.end method

.method public abstract onSelectFaclAudience(ILjava/util/ArrayList;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract onSelectPaclAudience(ILjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation
.end method
