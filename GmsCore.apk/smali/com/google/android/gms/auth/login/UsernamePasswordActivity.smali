.class public Lcom/google/android/gms/auth/login/UsernamePasswordActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "UsernamePasswordActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private mBackButton:Landroid/view/View;

.field private mLastPauseMillis:J

.field private mNextButton:Landroid/view/View;

.field protected mPasswordEdit:Landroid/widget/EditText;

.field private mPasswordError:Z

.field private mShouldFocusToPassword:Z

.field private mSignInAgreementLabel:Landroid/widget/TextView;

.field protected mUsernameEdit:Landroid/widget/EditText;

.field private mUsernameError:Z

.field protected prefilledPassword:Ljava/lang/String;

.field protected prefilledUsername:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/login/UsernamePasswordActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameError:Z

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/login/UsernamePasswordActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordError:Z

    return p1
.end method

.method private validateEmail(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->appendGmailHost(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->validateDomainNameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ge v1, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method


# virtual methods
.method protected getContentView()I
    .locals 1

    const v0, 0x7f040006

    return v0
.end method

.method protected hasMenu()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected initViews()V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    const v2, 0x7f0a001b

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v2, v5}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v2, 0x7f0a001a

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mBackButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setBackButton(Landroid/view/View;)V

    const v2, 0x7f0a0025

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f0a0026

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mAddAccount:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mConfirmCredentials:Z

    if-nez v2, :cond_0

    const v2, 0x7f0a0006

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0b00c0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setVisibility(I)V

    const v2, 0x7f0a0024

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    new-array v3, v5, [Landroid/text/InputFilter;

    new-instance v4, Lcom/google/android/gms/auth/login/UsernamePasswordActivity$2;

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity$2;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v2, v6}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v2, 0x7f0a0027

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSignInAgreementLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    new-array v3, v5, [Landroid/text/InputFilter;

    new-instance v4, Lcom/google/android/gms/auth/login/UsernamePasswordActivity$1;

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity$1;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    iput-boolean v5, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mShouldFocusToPassword:Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method

.method protected maybePrefillFields()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->initViews()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->maybePrefillFields()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->updateWidgetState()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v1, 0x7f0b00ab

    invoke-interface {p1, v2, v3, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02000a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/login/Compat;->menuItemSetShowAsAction(Landroid/view/MenuItem;I)V

    return v3
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v3, 0x7f0b0085

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-ne p1, v2, :cond_4

    if-nez p2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mAddAccount:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameError:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f0b0083

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f0b0091

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    if-ne p1, v2, :cond_0

    if-nez p2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordError:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    const v3, 0x7f0b0084

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->onPause()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mLastPauseMillis:J

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->updateWidgetState()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mLastPauseMillis:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mShouldFocusToPassword:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->prefilledPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordError:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_1
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->start()V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mAddAccount:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/login/GLSSession;->mPassword:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish(I)V

    return-void
.end method

.method public updateWidgetState()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->updateWidgetState()V

    iget-object v7, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordEdit:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-boolean v7, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mUsernameError:Z

    if-nez v7, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->validateEmail(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    move v4, v5

    :goto_0
    iget-boolean v7, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mAddAccount:Z

    if-nez v7, :cond_0

    const/4 v4, 0x1

    :cond_0
    iget-boolean v7, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mPasswordError:Z

    if-nez v7, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move v3, v5

    :goto_1
    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    move v0, v5

    :goto_2
    iget-object v5, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setFocusable(Z)V

    return-void

    :cond_1
    move v4, v6

    goto :goto_0

    :cond_2
    move v3, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_2
.end method
