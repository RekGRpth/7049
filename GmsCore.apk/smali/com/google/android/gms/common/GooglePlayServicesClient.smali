.class public interface abstract Lcom/google/android/gms/common/GooglePlayServicesClient;
.super Ljava/lang/Object;
.source "GooglePlayServicesClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;,
        Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    }
.end annotation


# virtual methods
.method public abstract connect()V
.end method

.method public abstract disconnect()V
.end method

.method public abstract isConnected()Z
.end method
