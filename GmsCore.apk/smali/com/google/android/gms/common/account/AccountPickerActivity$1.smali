.class Lcom/google/android/gms/common/account/AccountPickerActivity$1;
.super Ljava/lang/Object;
.source "AccountPickerActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/common/account/AccountPickerActivity;->populateUIAccountList([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/common/account/AccountPickerActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/account/AccountPickerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountPickerActivity;

    # setter for: Lcom/google/android/gms/common/account/AccountPickerActivity;->mSelectedItemIndex:I
    invoke-static {v0, p3}, Lcom/google/android/gms/common/account/AccountPickerActivity;->access$002(Lcom/google/android/gms/common/account/AccountPickerActivity;I)I

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountPickerActivity;

    # getter for: Lcom/google/android/gms/common/account/AccountPickerActivity;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->access$100(Lcom/google/android/gms/common/account/AccountPickerActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
