.class Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AccountTypePickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/account/AccountTypePickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p3, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;->mInfos:Ljava/util/ArrayList;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040017

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;

    invoke-direct {v0, v3}, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;-><init>(Lcom/google/android/gms/common/account/AccountTypePickerActivity$1;)V

    const v1, 0x7f0a0050

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;->text:Landroid/widget/TextView;

    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;->text:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;

    iget-object v1, v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AccountArrayAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;

    iget-object v1, v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$ViewHolder;

    goto :goto_0
.end method
