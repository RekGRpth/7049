.class public final Lcom/google/android/gms/common/acl/ScopeData$Builder;
.super Ljava/lang/Object;
.source "ScopeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/acl/ScopeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mActivityText:Ljava/lang/String;

.field private mAllCirclesVisible:Z

.field private final mDescription:Ljava/lang/String;

.field private final mDetail:Ljava/lang/String;

.field private mHasFriendPickerData:Z

.field private mIcon:Ljava/lang/String;

.field private mPaclPickerData:Ljava/lang/String;

.field private mShowSpeedbump:Z

.field private mVisibleEdges:Ljava/lang/String;

.field private mWarnings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getDetail()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/common/acl/ScopeData$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasIcon()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->setIcon(Ljava/lang/String;)Lcom/google/android/gms/common/acl/ScopeData$Builder;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getWarningList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->setWarnings(Ljava/util/List;)Lcom/google/android/gms/common/acl/ScopeData$Builder;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasPAclPickerData()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getPAclPickerData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->setPaclPickerData(Ljava/lang/String;)Lcom/google/android/gms/common/acl/ScopeData$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->hasFriendPickerData()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/LsoProto$ScopeDescription;->getFriendPickerData()Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getVisibleEdges()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getActivityText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getShowSpeedbump()Z

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$FriendPickerData;->getAllCirclesVisible()Z

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/gms/common/acl/ScopeData$Builder;->setFriendPickerData(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/gms/common/acl/ScopeData$Builder;

    :cond_2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mHasFriendPickerData:Z

    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mShowSpeedbump:Z

    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mAllCirclesVisible:Z

    # getter for: Lcom/google/android/gms/common/acl/ScopeData;->EMPTY_WARNING_LIST:Ljava/util/List;
    invoke-static {}, Lcom/google/android/gms/common/acl/ScopeData;->access$100()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mWarnings:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mDescription:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mDetail:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/common/acl/ScopeData;
    .locals 11

    new-instance v0, Lcom/google/android/gms/common/acl/ScopeData;

    iget-object v1, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mDescription:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mDetail:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mIcon:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mPaclPickerData:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mHasFriendPickerData:Z

    iget-object v6, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mVisibleEdges:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mActivityText:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mShowSpeedbump:Z

    iget-boolean v9, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mAllCirclesVisible:Z

    iget-object v10, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mWarnings:Ljava/util/List;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/acl/ScopeData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;)V

    return-object v0
.end method

.method public setFriendPickerData(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/gms/common/acl/ScopeData$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mHasFriendPickerData:Z

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mVisibleEdges:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mActivityText:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mShowSpeedbump:Z

    iput-boolean p4, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mAllCirclesVisible:Z

    return-object p0
.end method

.method public setIcon(Ljava/lang/String;)Lcom/google/android/gms/common/acl/ScopeData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mIcon:Ljava/lang/String;

    return-object p0
.end method

.method public setPaclPickerData(Ljava/lang/String;)Lcom/google/android/gms/common/acl/ScopeData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mPaclPickerData:Ljava/lang/String;

    return-object p0
.end method

.method public setWarnings(Ljava/util/List;)Lcom/google/android/gms/common/acl/ScopeData$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;)",
            "Lcom/google/android/gms/common/acl/ScopeData$Builder;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData$Builder;->mWarnings:Ljava/util/List;

    return-object p0
.end method
