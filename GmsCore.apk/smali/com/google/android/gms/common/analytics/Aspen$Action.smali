.class public final Lcom/google/android/gms/common/analytics/Aspen$Action;
.super Ljava/lang/Object;
.source "Aspen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/analytics/Aspen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final ACCEPT_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final AUTH_COMPLETE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final CIRCLE_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final CIRCLE_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final FACL_UPDATED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final FRIEND_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final FRIEND_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PEOPLE_SEARCH_TEXT_CLEARED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PEOPLE_SEARCH_TEXT_ENTERED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final SPEED_BUMP_ACCEPTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SPEED_BUMP_MODIFY_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SPEED_BUMP_NEVER_SHOW_SELECTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final VISIBLE_FRIENDS_CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final VISIBLE_FRIENDS_OK_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->PEOPLE_SEARCH_TEXT_ENTERED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->PEOPLE_SEARCH_TEXT_CLEARED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x3

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->FRIEND_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x4

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->FRIEND_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x5

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->CIRCLE_PICKER_INFO_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x6

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->CIRCLE_PICKER_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x7

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->VISIBLE_FRIENDS_OK_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x8

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->VISIBLE_FRIENDS_CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x9

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->SPEED_BUMP_ACCEPTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xa

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->SPEED_BUMP_MODIFY_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xb

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->SPEED_BUMP_NEVER_SHOW_SELECTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xc

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->ACCEPT_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xd

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->CANCEL_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xe

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->AUTH_COMPLETE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xf

    # invokes: Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Aspen;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$Action;->FACL_UPDATED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
