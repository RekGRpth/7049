.class public final Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;
.super Ljava/lang/Object;
.source "Aspen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/analytics/Aspen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoggedCircle"
.end annotation


# static fields
.field static final ALL_CIRCLES:Ljava/lang/Integer;

.field public static ALL_LOGGED_CIRCLES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field static final DEFAULT:Ljava/lang/Integer;

.field static final EXTENDED_CIRCLES:Ljava/lang/Integer;

.field static final PRIVATE:Ljava/lang/Integer;

.field public static PRIVATE_LOGGED_CIRCLES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field static final PUBLIC:Ljava/lang/Integer;

.field static final YOUR_CIRCLES:Ljava/lang/Integer;


# instance fields
.field private final mId:Ljava/lang/String;

.field private final mType:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->DEFAULT:Ljava/lang/Integer;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE:Ljava/lang/Integer;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->ALL_CIRCLES:Ljava/lang/Integer;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->EXTENDED_CIRCLES:Ljava/lang/Integer;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PUBLIC:Ljava/lang/Integer;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->YOUR_CIRCLES:Ljava/lang/Integer;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->ALL_LOGGED_CIRCLES:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE_LOGGED_CIRCLES:Ljava/util/ArrayList;

    sget-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->ALL_LOGGED_CIRCLES:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v2, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->ALL_CIRCLES:Ljava/lang/Integer;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE_LOGGED_CIRCLES:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v2, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE:Ljava/lang/Integer;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle$1;

    invoke-direct {v0}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mType:Ljava/lang/Integer;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/gms/common/analytics/Aspen$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/gms/common/analytics/Aspen$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mType:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mId:Ljava/lang/String;

    return-void
.end method

.method public static toLoggedCircles(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    sget-object v2, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PRIVATE_LOGGED_CIRCLES:Ljava/util/ArrayList;

    :cond_1
    return-object v2

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    sget-object v4, Lcom/google/android/gms/common/people/data/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v5, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->EXTENDED_CIRCLES:Ljava/lang/Integer;

    sget-object v6, Lcom/google/android/gms/common/people/data/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    sget-object v4, Lcom/google/android/gms/common/people/data/AudienceMember;->PUBLIC:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v5, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->PUBLIC:Ljava/lang/Integer;

    sget-object v6, Lcom/google/android/gms/common/people/data/AudienceMember;->PUBLIC:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    sget-object v4, Lcom/google/android/gms/common/people/data/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v5, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->YOUR_CIRCLES:Ljava/lang/Integer;

    sget-object v6, Lcom/google/android/gms/common/people/data/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    new-instance v4, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    sget-object v5, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->DEFAULT:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mType:Ljava/lang/Integer;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
