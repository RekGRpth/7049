.class public final Lcom/google/android/gms/common/images/ImageBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ImageBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v3, "com.google.android.gms.extras.resultReceiver"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/ResultReceiver;

    const-string v3, "com.google.android.gms.extras.uri"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    const-string v3, "com.google.android.gms.extras.priority"

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->loadImage(Landroid/content/Context;Landroid/net/Uri;ILandroid/os/ResultReceiver;)V

    return-void
.end method
