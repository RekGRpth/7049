.class final Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;
.super Ljava/lang/Object;
.source "ImageManager.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/images/ImageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LowMemoryListener"
.end annotation


# instance fields
.field private final mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/internal/support/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/support/LruCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/support/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/support/LruCache;->evictAll()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0x3c

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/support/LruCache;->evictAll()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageManager$LowMemoryListener;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/support/LruCache;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/support/LruCache;->trimToSize(I)V

    goto :goto_0
.end method
