.class public final Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;
.super Lcom/google/android/gms/common/app/MultiThreadedIntentService;
.source "ImageMultiThreadedIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;,
        Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;,
        Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sOperations:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->sOperations:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;-><init>(I)V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static loadImage(Landroid/content/Context;Landroid/net/Uri;ILandroid/os/ResultReceiver;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # Landroid/os/ResultReceiver;

    new-instance v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$LoadImageOperation;-><init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->startService(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;)V

    return-void
.end method

.method public static preloadImageData(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;

    invoke-direct {v1, p1, v0, p3}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;-><init>(Landroid/net/Uri;Ljava/util/ArrayList;I)V

    invoke-static {p0, v1}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->startService(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;)V

    return-void
.end method

.method public static preloadImageData(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;-><init>(Landroid/net/Uri;Ljava/util/ArrayList;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->startService(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;)V

    return-void
.end method

.method private static startService(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->sOperations:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/gms/common/images/ImageBroker;->acquireBroker(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageBroker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->sOperations:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->TAG:Ljava/lang/String;

    const-string v2, "No operation found when processing!"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;->execute(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageBroker;)V

    goto :goto_0
.end method
