.class public Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;
.super Ljava/lang/Object;
.source "SafeParcelWriter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static beginObjectHeader(Landroid/os/Parcel;)I
    .locals 1
    .param p0    # Landroid/os/Parcel;

    const/16 v0, 0x4f45

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    return v0
.end method

.method private static beginVariableData(Landroid/os/Parcel;I)I
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I

    const/high16 v0, -0x10000

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    return v0
.end method

.method public static finishObjectHeader(Landroid/os/Parcel;I)V
    .locals 0
    .param p0    # Landroid/os/Parcel;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    return-void
.end method

.method private static finishVariableData(Landroid/os/Parcel;I)V
    .locals 3
    .param p0    # Landroid/os/Parcel;
    .param p1    # I

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    sub-int v1, v0, p1

    add-int/lit8 v2, p1, -0x4

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    return-void
.end method

.method public static writeBigDecimal(Landroid/os/Parcel;ILjava/math/BigDecimal;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Ljava/math/BigDecimal;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p2}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    invoke-virtual {p2}, Ljava/math/BigDecimal;->scale()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeBigDecimalArray(Landroid/os/Parcel;I[Ljava/math/BigDecimal;Z)V
    .locals 4
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [Ljava/math/BigDecimal;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v3, 0x0

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v2

    array-length v1, p2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/math/BigDecimal;->scale()I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeBigInteger(Landroid/os/Parcel;ILjava/math/BigInteger;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Ljava/math/BigInteger;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeBigIntegerArray(Landroid/os/Parcel;I[Ljava/math/BigInteger;Z)V
    .locals 4
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [Ljava/math/BigInteger;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v3, 0x0

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v2

    array-length v1, p2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeBoolean(Landroid/os/Parcel;IZ)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static writeBooleanArray(Landroid/os/Parcel;I[ZZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [Z
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeBundle(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeByte(Landroid/os/Parcel;IB)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # B

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static writeByteArray(Landroid/os/Parcel;I[BZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [B
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeDouble(Landroid/os/Parcel;ID)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # D

    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2, p3}, Landroid/os/Parcel;->writeDouble(D)V

    return-void
.end method

.method public static writeDoubleArray(Landroid/os/Parcel;I[DZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [D
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeDoubleArray([D)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeFloat(Landroid/os/Parcel;IF)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # F

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method

.method public static writeFloatArray(Landroid/os/Parcel;I[FZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [F
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeFloatArray([F)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method private static writeHeader(Landroid/os/Parcel;II)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # I

    const v0, 0xffff

    if-lt p2, v0, :cond_0

    const/high16 v0, -0x10000

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void

    :cond_0
    shl-int/lit8 v0, p2, 0x10

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method

.method public static writeIBinder(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Landroid/os/IBinder;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeInt(Landroid/os/Parcel;II)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static writeIntArray(Landroid/os/Parcel;I[IZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [I
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeList(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Ljava/util/List;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeLong(Landroid/os/Parcel;IJ)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # J

    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method

.method public static writeLongArray(Landroid/os/Parcel;I[JZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [J
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeLongArray([J)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeParcel(Landroid/os/Parcel;ILandroid/os/Parcel;Z)V
    .locals 3
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Z

    const/4 v2, 0x0

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    invoke-static {p0, p1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->dataSize()I

    move-result v1

    invoke-virtual {p0, p2, v2, v1}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeParcelList(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 6
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcel;",
            ">;Z)V"
        }
    .end annotation

    const/4 v5, 0x0

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    invoke-static {p0, p1, v5}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcel;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    move-result v4

    invoke-virtual {p0, v1, v5, v4}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    :cond_3
    invoke-static {p0, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Landroid/os/Parcelable;
    .param p3    # I
    .param p4    # Z

    if-nez p2, :cond_1

    if-eqz p4, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-interface {p2, p0, p3}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeStringArray(Landroid/os/Parcel;I[Ljava/lang/String;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p2    # [Ljava/lang/String;
    .param p3    # Z

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeStringList(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 2
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static writeTypedArray(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V
    .locals 5
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Landroid/os/Parcel;",
            "I[TT;IZ)V"
        }
    .end annotation

    const/4 v4, 0x0

    if-nez p2, :cond_1

    if-eqz p4, :cond_0

    invoke-static {p0, p1, v4}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v3

    array-length v2, p2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v1, p2, v0

    if-nez v1, :cond_2

    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeInt(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, v1, p3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeTypedItemWithSize(Landroid/os/Parcel;Landroid/os/Parcelable;I)V

    goto :goto_2

    :cond_3
    invoke-static {p0, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method private static writeTypedItemWithSize(Landroid/os/Parcel;Landroid/os/Parcelable;I)V
    .locals 4
    .param p0    # Landroid/os/Parcel;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Landroid/os/Parcel;",
            "TT;I)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v1

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    invoke-interface {p1, p0, p2}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    sub-int v3, v0, v2

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    return-void
.end method

.method public static writeTypedList(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 5
    .param p0    # Landroid/os/Parcel;
    .param p1    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Landroid/os/Parcel;",
            "I",
            "Ljava/util/List",
            "<TT;>;Z)V"
        }
    .end annotation

    const/4 v4, 0x0

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    invoke-static {p0, p1, v4}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeHeader(Landroid/os/Parcel;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginVariableData(Landroid/os/Parcel;I)I

    move-result v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    if-nez v1, :cond_2

    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeInt(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, v1, v4}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeTypedItemWithSize(Landroid/os/Parcel;Landroid/os/Parcelable;I)V

    goto :goto_2

    :cond_3
    invoke-static {p0, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishVariableData(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
