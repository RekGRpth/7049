.class public final Lcom/google/android/gms/common/people/data/AudienceMemberConversions;
.super Ljava/lang/Object;
.source "AudienceMemberConversions.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toAudienceMembers(Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;)Ljava/util/List;
    .locals 9
    .param p0    # Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getTargetsCount()I

    move-result v6

    new-array v3, v6, [Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v0, 0x0

    :goto_0
    array-length v6, v3

    if-ge v0, v6, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getTargets(I)Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;->getId()Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->getPersonId()Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->hasGroupType()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->getGroupType()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/common/people/data/SystemGroupType;->fromInt(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    aput-object v6, v3, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->hasCircleId()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->getCircleId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    aput-object v6, v3, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->hasObfuscatedGaiaId()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v1, v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->forPerson(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    aput-object v6, v3, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->hasEmail()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->getEmail()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/people/data/AudienceMember;->forEmail(Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    aput-object v6, v3, v0

    goto :goto_1

    :cond_3
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unrecognized sharing target (at "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_4
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    return-object v6
.end method

.method public static toAudienceMembersFromRenderedSharingRosters([B)Ljava/util/List;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->parseFrom([B)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/AudienceMemberConversions;->toAudienceMembers(Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static toSharingRoster(Ljava/util/List;)Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;"
        }
    .end annotation

    new-instance v4, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    invoke-direct {v4}, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/people/data/AudienceMember;

    new-instance v5, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    invoke-direct {v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_1
    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;->addSharingTargetId(Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;)Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/people/data/SystemGroupType;->fromString(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->setGroupType(I)Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    goto :goto_1

    :pswitch_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->setCircleId(Ljava/lang/String;)Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    goto :goto_1

    :pswitch_2
    new-instance v0, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->setEmail(Ljava/lang/String;)Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->setPersonId(Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;)Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    goto :goto_1

    :pswitch_3
    new-instance v3, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;

    invoke-direct {v3}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;->setObfuscatedGaiaId(Ljava/lang/String;)Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;->setPersonId(Lcom/google/android/gms/common/people/proto/AclProtoData$data$CircleMemberId;)Lcom/google/android/gms/common/people/proto/AclProto$SharingTargetId;

    goto :goto_1

    :cond_0
    return-object v4

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static toSharingRosterBinary(Ljava/util/List;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)[B"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gms/common/people/data/AudienceMemberConversions;->toSharingRoster(Ljava/util/List;)Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/proto/AclProto$SharingRoster;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
