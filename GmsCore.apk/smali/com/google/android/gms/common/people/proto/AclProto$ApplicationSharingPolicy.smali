.class public final Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "AclProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/people/proto/AclProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplicationSharingPolicy"
.end annotation


# instance fields
.field private allowSquares_:Z

.field private allowedGroupType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private applicationId_:I

.field private cachedSize:I

.field private hasAllowSquares:Z

.field private hasApplicationId:Z

.field private hasShowUnderageWarnings:Z

.field private showUnderageWarnings_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->applicationId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowedGroupType_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->showUnderageWarnings_:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowSquares_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAllowedGroupType(I)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowedGroupType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowedGroupType_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowedGroupType_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAllowSquares()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowSquares_:Z

    return v0
.end method

.method public getAllowedGroupTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowedGroupType_:Ljava/util/List;

    return-object v0
.end method

.method public getApplicationId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->applicationId_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getAllowedGroupTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_0
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getAllowedGroupTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasShowUnderageWarnings()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getShowUnderageWarnings()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasAllowSquares()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getAllowSquares()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasApplicationId()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getApplicationId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    iput v3, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->cachedSize:I

    return v3
.end method

.method public getShowUnderageWarnings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->showUnderageWarnings_:Z

    return v0
.end method

.method public hasAllowSquares()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasAllowSquares:Z

    return v0
.end method

.method public hasApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasApplicationId:Z

    return v0
.end method

.method public hasShowUnderageWarnings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasShowUnderageWarnings:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->addAllowedGroupType(I)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->setShowUnderageWarnings(Z)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->setAllowSquares(Z)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->setApplicationId(I)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    move-result-object v0

    return-object v0
.end method

.method public setAllowSquares(Z)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasAllowSquares:Z

    iput-boolean p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->allowSquares_:Z

    return-object p0
.end method

.method public setApplicationId(I)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasApplicationId:Z

    iput p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->applicationId_:I

    return-object p0
.end method

.method public setShowUnderageWarnings(Z)Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasShowUnderageWarnings:Z

    iput-boolean p1, p0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->showUnderageWarnings_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getAllowedGroupTypeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasShowUnderageWarnings()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getShowUnderageWarnings()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasAllowSquares()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getAllowSquares()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->hasApplicationId()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;->getApplicationId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
