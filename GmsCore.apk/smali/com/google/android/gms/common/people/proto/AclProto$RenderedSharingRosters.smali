.class public final Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "AclProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/people/proto/AclProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RenderedSharingRosters"
.end annotation


# instance fields
.field private applicationPolicies_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private resourceSharingRosters_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;",
            ">;"
        }
    .end annotation
.end field

.field private targets_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->resourceSharingRosters_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->applicationPolicies_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    return-object v0
.end method


# virtual methods
.method public addApplicationPolicies(Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->applicationPolicies_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->applicationPolicies_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->applicationPolicies_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addResourceSharingRosters(Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->resourceSharingRosters_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->resourceSharingRosters_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->resourceSharingRosters_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTargets(Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getApplicationPoliciesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->applicationPolicies_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->cachedSize:I

    return v0
.end method

.method public getResourceSharingRostersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->resourceSharingRosters_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getResourceSharingRostersList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getApplicationPoliciesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getTargetsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_2
    iput v2, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->cachedSize:I

    return v2
.end method

.method public getTargets(I)Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    return-object v0
.end method

.method public getTargetsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTargetsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->targets_:Ljava/util/List;

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->addResourceSharingRosters(Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->addApplicationPolicies(Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->addTargets(Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getResourceSharingRostersList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$ResourceSharingRoster;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getApplicationPoliciesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$ApplicationSharingPolicy;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/proto/AclProto$RenderedSharingRosters;->getTargetsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/proto/AclProto$SharingTarget;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_2
    return-void
.end method
