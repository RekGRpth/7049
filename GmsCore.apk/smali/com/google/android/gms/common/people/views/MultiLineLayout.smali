.class public Lcom/google/android/gms/common/people/views/MultiLineLayout;
.super Landroid/view/ViewGroup;
.source "MultiLineLayout.java"


# instance fields
.field private mChipHeight:I

.field private mNumLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    iput v0, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 14
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v10, p4, p2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingTop()I

    move-result v11

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v12

    sub-int v12, v10, v12

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingRight()I

    move-result v13

    sub-int v8, v12, v13

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getChildCount()I

    move-result v5

    const/4 v12, 0x1

    iput v12, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    const/4 v12, 0x0

    iput v12, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v5, :cond_3

    invoke-virtual {p0, v7}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_0

    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v12, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    if-ge v12, v1, :cond_1

    iput v1, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    :cond_1
    add-int v12, v2, v4

    if-le v12, v8, :cond_2

    iget v12, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v2

    add-int v12, v9, v11

    add-int/2addr v3, v12

    const/4 v9, 0x0

    :cond_2
    add-int v12, v2, v4

    add-int v13, v3, v1

    invoke-virtual {v0, v2, v3, v12, v13}, Landroid/view/View;->layout(IIII)V

    add-int v12, v4, v6

    add-int/2addr v2, v12

    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    goto :goto_1

    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 19
    .param p1    # I
    .param p2    # I

    const/4 v13, 0x0

    const/4 v12, 0x0

    const v17, 0x7fffffff

    move/from16 v0, v17

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->resolveSize(II)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingTop()I

    move-result v16

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingTop()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v17

    sub-int v17, v15, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingRight()I

    move-result v18

    sub-int v11, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getChildCount()I

    move-result v8

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v8, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v4, :cond_1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mChipHeight:I

    :cond_1
    add-int v17, v5, v7

    move/from16 v0, v17

    if-le v0, v11, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/common/people/views/MultiLineLayout;->mNumLines:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingLeft()I

    move-result v5

    add-int v17, v14, v16

    add-int v6, v6, v17

    const/4 v14, 0x0

    :cond_2
    add-int v17, v5, v7

    move/from16 v0, v17

    invoke-static {v13, v0}, Ljava/lang/Math;->max(II)I

    move-result v13

    add-int v17, v6, v4

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    add-int v17, v7, v9

    add-int v5, v5, v17

    invoke-static {v14, v4}, Ljava/lang/Math;->max(II)I

    move-result v14

    goto :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingRight()I

    move-result v17

    add-int v13, v13, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->getPaddingBottom()I

    move-result v17

    add-int v12, v12, v17

    move/from16 v0, p1

    invoke-static {v13, v0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->resolveSize(II)I

    move-result v17

    move/from16 v0, p2

    invoke-static {v12, v0}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->resolveSize(II)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/people/views/MultiLineLayout;->setMeasuredDimension(II)V

    return-void
.end method
