.class public final Lcom/google/android/gms/common/provider/SelectionArgs;
.super Ljava/lang/Object;
.source "SelectionArgs.java"


# instance fields
.field private mSelection:Ljava/lang/StringBuilder;

.field private mSelectionArgs:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelection:Ljava/lang/StringBuilder;

    new-array v0, p2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelectionArgs:[Ljava/lang/String;

    return-void
.end method

.method public static forObjects(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/common/provider/SelectionArgs;
    .locals 1
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)",
            "Lcom/google/android/gms/common/provider/SelectionArgs;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/provider/SelectionArgs;->forObjects(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/android/gms/common/provider/SelectionArgs;

    move-result-object v0

    return-object v0
.end method

.method public static varargs forObjects(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/android/gms/common/provider/SelectionArgs;
    .locals 5
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[TT;)",
            "Lcom/google/android/gms/common/provider/SelectionArgs;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/common/provider/SelectionArgs;

    array-length v3, p1

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/common/provider/SelectionArgs;-><init>(Ljava/lang/String;I)V

    const/4 v2, 0x0

    array-length v1, p1

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v4, v0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelection:Ljava/lang/StringBuilder;

    if-nez v2, :cond_0

    const-string v3, "?"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelectionArgs:[Ljava/lang/String;

    aget-object v4, p1, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v3, ",?"

    goto :goto_1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public getSelection()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelection:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/provider/SelectionArgs;->mSelectionArgs:[Ljava/lang/String;

    return-object v0
.end method
