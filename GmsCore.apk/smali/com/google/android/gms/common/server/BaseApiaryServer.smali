.class public Lcom/google/android/gms/common/server/BaseApiaryServer;
.super Ljava/lang/Object;
.source "BaseApiaryServer.java"


# static fields
.field private static final CACHE_LOCK:Ljava/lang/Object;

.field private static sDiskCache:Lcom/android/volley/toolbox/DiskBasedCache;


# instance fields
.field protected final mApiaryBackendOverride:Ljava/lang/String;

.field protected final mApiaryTrace:Ljava/lang/String;

.field protected final mBaseServer:Ljava/lang/String;

.field protected final mContext:Landroid/content/Context;

.field protected final mEnableCache:Z

.field protected final mNetwork:Lcom/android/volley/Network;

.field protected final mRequestQueue:Lcom/android/volley/RequestQueue;

.field protected final mVerboseLogging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/BaseApiaryServer;->CACHE_LOCK:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/common/server/BaseApiaryServer;->sDiskCache:Lcom/android/volley/toolbox/DiskBasedCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mBaseServer:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mEnableCache:Z

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getCache(Landroid/content/Context;)Lcom/android/volley/Cache;

    move-result-object v0

    iput-boolean p5, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mVerboseLogging:Z

    iput-object p6, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryTrace:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryBackendOverride:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getNetwork(Landroid/content/Context;)Lcom/android/volley/Network;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mNetwork:Lcom/android/volley/Network;

    new-instance v1, Lcom/android/volley/RequestQueue;

    iget-object v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mNetwork:Lcom/android/volley/Network;

    invoke-direct {v1, v0, v2}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;)V

    iput-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    iget-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1}, Lcom/android/volley/RequestQueue;->start()V

    return-void
.end method

.method private checkVolleyError(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/ExecutionException;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v2, v0, Lcom/android/volley/VolleyError;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/android/volley/VolleyError;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->isInvalidTokenError(Lcom/android/volley/VolleyError;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mContext:Landroid/content/Context;

    invoke-static {v2, p2}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    throw v1

    :cond_1
    return-void
.end method

.method public static getPathlessServer(Landroid/content/Context;)Lcom/google/android/gms/common/server/BaseApiaryServer;
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/BaseApiaryServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private performNoResponseRequestBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V
    .locals 10
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getAuthTokenOrThrow(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v0, Lcom/google/android/gms/common/server/NoResponseRequest;

    iget-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mBaseServer:Ljava/lang/String;

    invoke-direct {p0, v1, p3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->toUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v1, p2

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/NoResponseRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {v9, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/server/RequestFuture;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v8

    new-instance v0, Lcom/android/volley/VolleyError;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v8

    invoke-direct {p0, v8, v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->checkVolleyError(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    new-instance v0, Lcom/android/volley/VolleyError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error executing network request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private toUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryTrace:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const-string v1, "&"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "trace="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryTrace:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_1
    const-string v1, "?"

    goto :goto_0
.end method


# virtual methods
.method protected buildRequest(ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/util/HashMap;)Lcom/google/android/gms/common/server/ApiaryRequest;
    .locals 11
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/json/JSONObject;
    .param p5    # Lcom/google/android/gms/common/server/response/FieldMappingDictionary;
    .param p6    # Ljava/lang/String;
    .param p8    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(I",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/gms/common/server/response/FieldMappingDictionary;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/common/server/ApiaryRequest",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/common/server/ApiaryRequest;

    iget-boolean v9, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mEnableCache:Z

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p6

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/server/ApiaryRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-object v0
.end method

.method protected createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryBackendOverride:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "X-Google-Backend-Override"

    iget-object v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mApiaryBackendOverride:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public fetchImageBlocking(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v0, Lcom/google/android/gms/common/server/ImageRequest;

    iget-boolean v4, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mEnableCache:Z

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/ImageRequest;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v8, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/gms/common/server/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v7

    new-instance v0, Lcom/android/volley/VolleyError;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v7

    new-instance v0, Lcom/android/volley/VolleyError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error executing network request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public fetchImageBlocking(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v0, Lcom/google/android/gms/common/server/ImageRequest;

    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mEnableCache:Z

    move-object v1, p1

    move-object v3, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/ImageRequest;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v8, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/gms/common/server/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v7

    new-instance v0, Lcom/android/volley/VolleyError;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v7

    new-instance v0, Lcom/android/volley/VolleyError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error executing network request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public fetchImageNonBlocking(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v0, Lcom/google/android/gms/common/server/ImageRequest;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/ImageRequest;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v7, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method protected getAuthTokenOrThrow(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->getOrThrow(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Lcom/android/volley/VolleyError;

    invoke-direct {v2, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected getCache(Landroid/content/Context;)Lcom/android/volley/Cache;
    .locals 4
    .param p1    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/gms/common/server/BaseApiaryServer;->CACHE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/server/BaseApiaryServer;->sDiskCache:Lcom/android/volley/toolbox/DiskBasedCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/volley/toolbox/DiskBasedCache;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const/high16 v3, 0x500000

    invoke-direct {v0, v2, v3}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    sput-object v0, Lcom/google/android/gms/common/server/BaseApiaryServer;->sDiskCache:Lcom/android/volley/toolbox/DiskBasedCache;

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/server/BaseApiaryServer;->sDiskCache:Lcom/android/volley/toolbox/DiskBasedCache;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getNetwork(Landroid/content/Context;)Lcom/android/volley/Network;
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v1, Lcom/google/android/gms/common/server/GmsHttpClientStack;

    iget-boolean v2, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mVerboseLogging:Z

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/common/server/GmsHttpClientStack;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v1}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;)V

    return-object v0
.end method

.method public getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "I",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/json/JSONObject;
    .param p6    # Lcom/google/android/gms/common/server/response/FieldMappingDictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "I",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/gms/common/server/response/FieldMappingDictionary;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/server/BaseApiaryServer;->isInvalidTokenError(Lcom/android/volley/VolleyError;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    goto :goto_0

    :cond_0
    throw v0
.end method

.method public getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v2, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public getResponseBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 12
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/json/JSONObject;
    .param p6    # Lcom/google/android/gms/common/server/response/FieldMappingDictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "I",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/gms/common/server/response/FieldMappingDictionary;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getAuthTokenOrThrow(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    iget-object v0, p0, Lcom/google/android/gms/common/server/BaseApiaryServer;->mBaseServer:Ljava/lang/String;

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->toUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v1, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/common/server/BaseApiaryServer;->buildRequest(ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/util/HashMap;)Lcom/google/android/gms/common/server/ApiaryRequest;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/gms/common/server/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v10

    new-instance v0, Lcom/android/volley/VolleyError;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v10

    invoke-direct {p0, v10, v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->checkVolleyError(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    new-instance v0, Lcom/android/volley/VolleyError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error executing network request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v10}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected isInvalidTokenError(Lcom/android/volley/VolleyError;)Z
    .locals 2
    .param p1    # Lcom/android/volley/VolleyError;

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v0, v0, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/server/BaseApiaryServer;->isInvalidTokenError(Lcom/android/volley/VolleyError;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlockingInternal(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0

    :cond_0
    throw v0
.end method

.method public performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method
