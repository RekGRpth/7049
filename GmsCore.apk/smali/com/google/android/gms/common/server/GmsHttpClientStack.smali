.class public final Lcom/google/android/gms/common/server/GmsHttpClientStack;
.super Lcom/google/android/volley/GoogleHttpClientStack;
.source "GmsHttpClientStack.java"


# static fields
.field private static final EMPTY_RESPONSE_ENTITY:Lorg/apache/http/HttpEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sput-object v0, Lcom/google/android/gms/common/server/GmsHttpClientStack;->EMPTY_RESPONSE_ENTITY:Lorg/apache/http/HttpEntity;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method public performRequest(Lcom/android/volley/Request;Ljava/util/Map;)Lorg/apache/http/HttpResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Request",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/google/android/volley/GoogleHttpClientStack;->performRequest(Lcom/android/volley/Request;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/gms/common/server/NoResponseRequest;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/common/server/GmsHttpClientStack;->EMPTY_RESPONSE_ENTITY:Lorg/apache/http/HttpEntity;

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/16 v1, 0xc8

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    :cond_0
    return-object v0
.end method
