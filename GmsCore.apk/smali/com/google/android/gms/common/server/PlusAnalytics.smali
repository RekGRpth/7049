.class public final Lcom/google/android/gms/common/server/PlusAnalytics;
.super Ljava/lang/Object;
.source "PlusAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/server/PlusAnalytics$1;,
        Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;,
        Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/common/server/PlusAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/server/PlusAnalytics;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p3    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    new-instance v0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setStartView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusEvent(Landroid/content/Context;Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;)V

    return-void
.end method

.method public static logPlusEvent(Landroid/content/Context;Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/google/android/gms/common/server/PlusAnalytics;->TAG:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/common/server/PlusAnalytics;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unabled to log plus action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p3    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    new-instance v0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setStartView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->setEndView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusEvent(Landroid/content/Context;Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;)V

    return-void
.end method
