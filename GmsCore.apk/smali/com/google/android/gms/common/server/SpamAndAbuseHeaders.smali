.class public final Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;
.super Ljava/lang/Object;
.source "SpamAndAbuseHeaders.java"


# static fields
.field private static sInstance:Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;


# instance fields
.field private mNetworkId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;
    .locals 2

    const-class v1, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->sInstance:Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->sInstance:Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->sInstance:Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getNetworkId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->mNetworkId:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/common/util/AndroidUtils;->getNetworkId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->mNetworkId:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->mNetworkId:Ljava/lang/String;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addHeaders(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    const-string v0, "X-Container-Url"

    invoke-virtual {p2, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "X-Network-ID"

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->getNetworkId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "X-Auth-Time"

    if-nez p4, :cond_0

    const-string v0, "none"

    :goto_0
    invoke-virtual {p2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
