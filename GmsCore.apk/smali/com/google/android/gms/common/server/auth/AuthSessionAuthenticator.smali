.class public final Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;
.super Ljava/lang/Object;
.source "AuthSessionAuthenticator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final mAccountName:Ljava/lang/String;

.field protected final mExtras:Landroid/os/Bundle;

.field protected final mPackageName:Ljava/lang/String;

.field protected final mScope:Ljava/lang/String;

.field protected mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mPackageName:Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    const-string v1, "androidPackageName"

    iget-object v2, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->isProxyingAuthentication()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    const-string v1, "callerUid"

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getVisibleActions()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    const-string v1, "request_visible_actions"

    const-string v2, " "

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getVisibleActions()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mAccountName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getScopesString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mScope:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public get(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mAccountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mScope:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    invoke-static {p1, v2, v3, v4}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mToken:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->getInstance()Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->updateTokenTime(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mToken:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Auth related exception is being ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v2, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Auth related exception is being ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/auth/GoogleAuthException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getOrThrow(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/gms/auth/GoogleAuthException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mAccountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mScope:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mExtras:Landroid/os/Bundle;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mToken:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->getInstance()Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->updateTokenTime(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->mToken:Ljava/lang/String;

    return-object v0
.end method
