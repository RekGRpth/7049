.class public Lcom/google/android/gms/common/server/response/FastParser;
.super Ljava/lang/Object;
.source "FastParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;,
        Lcom/google/android/gms/common/server/response/FastParser$ParseException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final BASE64_ALLOWED_CONTROL_CHARS:[C

.field private static final BIG_DECIMAL_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private static final BIG_INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field private static final BOOLEAN_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final DOUBLE_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private static final FALSE_TRAIL:[C

.field private static final FALSE_TRAIL_IN_STRING:[C

.field private static final FLOAT_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final LONG_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final NULL_TRAIL:[C

.field private static final STRING_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final TRUE_TRAIL:[C

.field private static final TRUE_TRAIL_IN_STRING:[C


# instance fields
.field private final mChar:[C

.field private final mLargeBuf:[C

.field private final mLargeSb:Ljava/lang/StringBuilder;

.field private final mSmallBuf:[C

.field private final mSmallSb:Ljava/lang/StringBuilder;

.field private final mStateStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x3

    const-class v0, Lcom/google/android/gms/common/server/response/FastParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->TAG:Ljava/lang/String;

    new-array v0, v1, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    new-array v0, v1, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->TRUE_TRAIL:[C

    new-array v0, v2, [C

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->TRUE_TRAIL_IN_STRING:[C

    new-array v0, v2, [C

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->FALSE_TRAIL:[C

    const/4 v0, 0x5

    new-array v0, v0, [C

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->FALSE_TRAIL_IN_STRING:[C

    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0xa

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->BASE64_ALLOWED_CONTROL_CHARS:[C

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$1;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$2;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$2;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->LONG_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$3;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$3;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->FLOAT_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$4;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$4;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->DOUBLE_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$5;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$5;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->BOOLEAN_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$6;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$6;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->STRING_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$7;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$7;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->BIG_INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    new-instance v0, Lcom/google/android/gms/common/server/response/FastParser$8;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/response/FastParser$8;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/response/FastParser;->BIG_DECIMAL_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    return-void

    nop

    :array_0
    .array-data 2
        0x75s
        0x6cs
        0x6cs
    .end array-data

    nop

    :array_1
    .array-data 2
        0x72s
        0x75s
        0x65s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x72s
        0x75s
        0x65s
        0x22s
    .end array-data

    :array_3
    .array-data 2
        0x61s
        0x6cs
        0x73s
        0x65s
    .end array-data

    :array_4
    .array-data 2
        0x61s
        0x6cs
        0x73s
        0x65s
        0x22s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x400

    const/16 v1, 0x20

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    new-array v0, v1, [C

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    new-array v0, v2, [C

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallSb:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeSb:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)I
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseInt(Ljava/io/BufferedReader;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)J
    .locals 2
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseLong(Ljava/io/BufferedReader;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)F
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseFloat(Ljava/io/BufferedReader;)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)D
    .locals 2
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseDouble(Ljava/io/BufferedReader;)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/server/response/FastParser;->parseBoolean(Ljava/io/BufferedReader;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseString(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)Ljava/math/BigInteger;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseBigInteger(Ljava/io/BufferedReader;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)Ljava/math/BigDecimal;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/server/response/FastParser;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseBigDecimal(Ljava/io/BufferedReader;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method private assumePop(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but had empty stack"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but had "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    return-void
.end method

.method private charArrayContains([CC)Z
    .locals 3
    .param p1    # [C
    .param p2    # C

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-char v2, p1, v0

    if-ne v2, p2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private expectChars(Ljava/io/BufferedReader;[C)V
    .locals 4
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    const/4 v2, 0x0

    array-length v3, p2

    invoke-virtual {p1, v1, v2, v3}, Ljava/io/BufferedReader;->read([CII)I

    move-result v1

    array-length v2, p2

    if-eq v1, v2, :cond_0

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v2, "Unexpected EOF"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_2

    aget-char v1, p2, v0

    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    aget-char v2, v2, v0

    if-eq v1, v2, :cond_1

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v2, "Unexpected character"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/BufferedReader;",
            "Lcom/google/android/gms/common/server/response/FastParser$ParseHelper",
            "<TO;>;)",
            "Ljava/util/ArrayList",
            "<TO;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x5

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v0

    const/16 v2, 0x6e

    if-ne v0, v2, :cond_0

    sget-object v2, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/16 v2, 0x5b

    if-eq v0, v2, :cond_1

    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v3, "Expected start of array"

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    :sswitch_0
    const/16 v2, 0x400

    invoke-virtual {p1, v2}, Ljava/io/BufferedReader;->mark(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    invoke-interface {p2, p0, p1}, Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;->parse(Lcom/google/android/gms/common/server/response/FastParser;Ljava/io/BufferedReader;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_1
    invoke-direct {p0, v4}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto :goto_0

    :sswitch_2
    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v3, "Unexpected EOF"

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2c -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 5
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->getFieldMappings()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    new-instance v3, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v4, "Object array response class must have a single Field"

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->parseObjectArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getOutputFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v1, v3, v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->addConcreteTypeArrayInternal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method private parseBigDecimal(Ljava/io/BufferedReader;)Ljava/math/BigDecimal;
    .locals 5
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/math/BigDecimal;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseBigInteger(Ljava/io/BufferedReader;)Ljava/math/BigInteger;
    .locals 5
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/math/BigInteger;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseBoolean(Ljava/io/BufferedReader;Z)Z
    .locals 4
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    move v1, v2

    :goto_0
    return v1

    :sswitch_1
    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->TRUE_TRAIL_IN_STRING:[C

    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    move v1, v3

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->TRUE_TRAIL:[C

    goto :goto_1

    :sswitch_2
    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->FALSE_TRAIL_IN_STRING:[C

    :goto_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    move v1, v2

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->FALSE_TRAIL:[C

    goto :goto_2

    :sswitch_3
    if-eqz p2, :cond_2

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v2, "No boolean value found in string"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/common/server/response/FastParser;->parseBoolean(Ljava/io/BufferedReader;Z)Z

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x66 -> :sswitch_2
        0x6e -> :sswitch_0
        0x74 -> :sswitch_1
    .end sparse-switch
.end method

.method private parseDouble(Ljava/io/BufferedReader;)D
    .locals 4
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    goto :goto_0
.end method

.method private parseFloat(Ljava/io/BufferedReader;)F
    .locals 4
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method private parseInt(Ljava/io/BufferedReader;)I
    .locals 2
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-static {v1, v0}, Lcom/google/android/gms/common/server/response/FastParser;->parseInt([CI)I

    move-result v1

    goto :goto_0
.end method

.method private static parseInt([CI)I
    .locals 10
    .param p0    # [C
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;
        }
    .end annotation

    const/16 v9, 0xa

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    if-lez p1, :cond_5

    const/4 v7, 0x0

    aget-char v7, p0, v7

    const/16 v8, 0x2d

    if-ne v7, v8, :cond_0

    const/4 v5, 0x1

    const/high16 v3, -0x80000000

    add-int/lit8 v1, v1, 0x1

    :goto_0
    div-int/lit8 v4, v3, 0xa

    if-ge v1, p1, :cond_9

    add-int/lit8 v2, v1, 0x1

    aget-char v7, p0, v1

    invoke-static {v7, v9}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    if-gez v0, :cond_1

    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "Unexpected non-digit character"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    const v3, -0x7fffffff

    goto :goto_0

    :cond_1
    neg-int v6, v0

    :goto_1
    if-ge v2, p1, :cond_6

    add-int/lit8 v1, v2, 0x1

    aget-char v7, p0, v2

    invoke-static {v7, v9}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    if-gez v0, :cond_2

    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "Unexpected non-digit character"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    if-ge v6, v4, :cond_3

    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "Number too large"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    mul-int/lit8 v6, v6, 0xa

    add-int v7, v3, v0

    if-ge v6, v7, :cond_4

    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "Number too large"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    sub-int/2addr v6, v0

    move v2, v1

    goto :goto_1

    :cond_5
    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "No number to parse"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_6
    if-eqz v5, :cond_8

    const/4 v7, 0x1

    if-le v2, v7, :cond_7

    :goto_2
    return v6

    :cond_7
    new-instance v7, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v8, "No digits to parse"

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_8
    neg-int v6, v6

    goto :goto_2

    :cond_9
    move v2, v1

    goto :goto_1
.end method

.method private parseInternal(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedReader;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/server/response/FastParser;->parseObject(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)Z

    :goto_0
    return-void

    :sswitch_1
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v2, "No data to parse"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x5b -> :sswitch_1
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private parseLong(Ljava/io/BufferedReader;)J
    .locals 3
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-static {v1, v0}, Lcom/google/android/gms/common/server/response/FastParser;->parseLong([CI)J

    move-result-wide v1

    goto :goto_0
.end method

.method private static parseLong([CI)J
    .locals 12
    .param p0    # [C
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;
        }
    .end annotation

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x0

    if-lez p1, :cond_5

    const/4 v10, 0x0

    aget-char v10, p0, v10

    const/16 v11, 0x2d

    if-ne v10, v11, :cond_0

    const/4 v7, 0x1

    const-wide/high16 v3, -0x8000000000000000L

    add-int/lit8 v1, v1, 0x1

    :goto_0
    const-wide/16 v10, 0xa

    div-long v5, v3, v10

    if-ge v1, p1, :cond_9

    add-int/lit8 v2, v1, 0x1

    aget-char v10, p0, v1

    const/16 v11, 0xa

    invoke-static {v10, v11}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    if-gez v0, :cond_1

    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "Unexpected non-digit character"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    const-wide v3, -0x7fffffffffffffffL

    goto :goto_0

    :cond_1
    neg-int v10, v0

    int-to-long v8, v10

    :goto_1
    if-ge v2, p1, :cond_6

    add-int/lit8 v1, v2, 0x1

    aget-char v10, p0, v2

    const/16 v11, 0xa

    invoke-static {v10, v11}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    if-gez v0, :cond_2

    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "Unexpected non-digit character"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_2
    cmp-long v10, v8, v5

    if-gez v10, :cond_3

    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "Number too large"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_3
    const-wide/16 v10, 0xa

    mul-long/2addr v8, v10

    int-to-long v10, v0

    add-long/2addr v10, v3

    cmp-long v10, v8, v10

    if-gez v10, :cond_4

    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "Number too large"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_4
    int-to-long v10, v0

    sub-long/2addr v8, v10

    move v2, v1

    goto :goto_1

    :cond_5
    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "No number to parse"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_6
    if-eqz v7, :cond_8

    const/4 v10, 0x1

    if-le v2, v10, :cond_7

    :goto_2
    return-wide v8

    :cond_7
    new-instance v10, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v11, "No digits to parse"

    invoke-direct {v10, v11}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_8
    neg-long v8, v8

    goto :goto_2

    :cond_9
    move v2, v1

    goto :goto_1
.end method

.method private parseObject(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)Z
    .locals 11
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->getFieldMappings()Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readFieldKey(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_0
    :goto_1
    if-eqz v1, :cond_f

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->skipToNextField(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    iget-object v8, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v9, 0x4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getTypeIn()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid field type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getTypeIn()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setIntegers(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    :goto_2
    const/4 v8, 0x4

    invoke-direct {p0, v8}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v8, 0x2

    invoke-direct {p0, v8}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v6

    sparse-switch v6, :sswitch_data_0

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Expected end of object or field separator, but found: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseInt(Ljava/io/BufferedReader;)I

    move-result v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setInteger(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;I)V

    goto :goto_2

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->BIG_INTEGER_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBigIntegers(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseBigInteger(Ljava/io/BufferedReader;)Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBigInteger(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/math/BigInteger;)V

    goto :goto_2

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_4

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->LONG_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setLongs(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseLong(Ljava/io/BufferedReader;)J

    move-result-wide v8

    invoke-virtual {p2, v0, v8, v9}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setLong(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;J)V

    goto :goto_2

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->FLOAT_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setFloats(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseFloat(Ljava/io/BufferedReader;)F

    move-result v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setFloat(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;F)V

    goto :goto_2

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_6

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->DOUBLE_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setDoubles(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_6
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseDouble(Ljava/io/BufferedReader;)D

    move-result-wide v8

    invoke-virtual {p2, v0, v8, v9}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setDouble(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;D)V

    goto/16 :goto_2

    :pswitch_5
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_7

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->BIG_DECIMAL_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBigDecimals(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseBigDecimal(Ljava/io/BufferedReader;)Ljava/math/BigDecimal;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBigDecimal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/math/BigDecimal;)V

    goto/16 :goto_2

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_8

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->BOOLEAN_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBooleans(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_8
    const/4 v8, 0x0

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseBoolean(Ljava/io/BufferedReader;Z)Z

    move-result v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setBoolean(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Z)V

    goto/16 :goto_2

    :pswitch_7
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_9

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->STRING_PARSE_HELPER:Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->parseArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastParser$ParseHelper;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setStrings(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseString(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setString(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_8
    iget-object v8, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    iget-object v9, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeSb:Ljava/lang/StringBuilder;

    sget-object v10, Lcom/google/android/gms/common/server/response/FastParser;->BASE64_ALLOWED_CONTROL_CHARS:[C

    invoke-direct {p0, p1, v8, v9, v10}, Lcom/google/android/gms/common/server/response/FastParser;->parseString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/common/util/Base64Utils;->decode(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setDecodedBytes(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;[B)V

    goto/16 :goto_2

    :pswitch_9
    iget-object v8, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    iget-object v9, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeSb:Ljava/lang/StringBuilder;

    sget-object v10, Lcom/google/android/gms/common/server/response/FastParser;->BASE64_ALLOWED_CONTROL_CHARS:[C

    invoke-direct {p0, p1, v8, v9, v10}, Lcom/google/android/gms/common/server/response/FastParser;->parseString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/common/util/Base64Utils;->decodeUrlSafe(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setDecodedBytes(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;[B)V

    goto/16 :goto_2

    :pswitch_a
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->parseStringMap(Ljava/io/BufferedReader;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {p2, v0, v8}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->setStringMap(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/util/Map;)V

    goto/16 :goto_2

    :pswitch_b
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->isTypeInArray()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v6

    const/16 v8, 0x6e

    if-ne v6, v8, :cond_a

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getOutputFieldName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p2, v0, v8, v9}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->addConcreteTypeArrayInternal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_a
    iget-object v8, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v9, 0x5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v8, 0x5b

    if-eq v6, v8, :cond_b

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v9, "Expected array start"

    invoke-direct {v8, v9}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_b
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getOutputFieldName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/server/response/FastParser;->parseObjectArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {p2, v0, v8, v9}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->addConcreteTypeArrayInternal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_c
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v7

    const/16 v8, 0x6e

    if-ne v7, v8, :cond_d

    sget-object v8, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getOutputFieldName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p2, v0, v8, v9}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->addConcreteTypeInternal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    goto/16 :goto_2

    :cond_d
    iget-object v8, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v8, 0x7b

    if-eq v7, v8, :cond_e

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v9, "Expected start of object"

    invoke-direct {v8, v9}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_e
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->newConcreteTypeInstance()Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/google/android/gms/common/server/response/FastParser;->parseObject(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->getOutputFieldName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v0, v8, v5}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->addConcreteTypeInternal(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    :catch_0
    move-exception v4

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v9, "Error instantiating inner object"

    invoke-direct {v8, v9, v4}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    :catch_1
    move-exception v3

    new-instance v8, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v9, "Error instantiating inner object"

    invoke-direct {v8, v9, v3}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    :sswitch_0
    const/4 v1, 0x0

    goto/16 :goto_1

    :sswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readFieldKey(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_f
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v8, 0x1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_1
        0x7d -> :sswitch_0
    .end sparse-switch
.end method

.method private parseObjectArray(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/util/ArrayList;
    .locals 9
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/io/BufferedReader;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_0
    invoke-direct {p0, v7}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    :cond_0
    :goto_0
    return-object v4

    :sswitch_1
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->newConcreteTypeInstance()Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/common/server/response/FastParser;->parseObject(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v3

    sparse-switch v3, :sswitch_data_1

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_2
    sget-object v5, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v5}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    invoke-direct {p0, v7}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Error instantiating inner object"

    invoke-direct {v5, v6, v1}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v0

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Error instantiating inner object"

    invoke-direct {v5, v6, v0}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    :sswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v5

    const/16 v6, 0x7b

    if-eq v5, v6, :cond_1

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Expected start of next object in array"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :sswitch_4
    invoke-direct {p0, v7}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5d -> :sswitch_0
        0x6e -> :sswitch_2
        0x7b -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_3
        0x5d -> :sswitch_4
    .end sparse-switch
.end method

.method private parseString(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallSb:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/common/server/response/FastParser;->parseString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private parseString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # [C
    .param p3    # Ljava/lang/StringBuilder;
    .param p4    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v2, "Expected string"

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/server/response/FastParser;->readString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :sswitch_1
    sget-object v1, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    const/4 v1, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x6e -> :sswitch_1
    .end sparse-switch
.end method

.method private parseStringMap(Ljava/io/BufferedReader;)Ljava/util/HashMap;
    .locals 8
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedReader;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    const/16 v5, 0x6e

    if-ne v1, v5, :cond_0

    sget-object v5, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v5}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    move-object v2, v4

    :goto_0
    return-object v2

    :cond_0
    const/16 v5, 0x7b

    if-eq v1, v5, :cond_1

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Expected start of a map object"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected EOF"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_1
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    iget-object v6, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallSb:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, v5, v6, v4}, Lcom/google/android/gms/common/server/response/FastParser;->readString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    const/16 v5, 0x3a

    if-eq v1, v5, :cond_3

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No map value found for key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    const/16 v5, 0x22

    if-eq v1, v5, :cond_4

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Expected String value for key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    iget-object v6, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallSb:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, v5, v6, v4}, Lcom/google/android/gms/common/server/response/FastParser;->readString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    const/16 v5, 0x2c

    if-eq v1, v5, :cond_2

    const/16 v4, 0x7d

    if-ne v1, v4, :cond_5

    invoke-direct {p0, v7}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto/16 :goto_0

    :cond_5
    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected character while parsing string map: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_2
    invoke-direct {p0, v7}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method private readFieldKey(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x3

    const/4 v0, 0x0

    const/4 v4, 0x2

    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected token: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_0
    invoke-direct {p0, v4}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    :cond_0
    :goto_0
    return-object v0

    :sswitch_1
    invoke-direct {p0, v4}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallBuf:[C

    iget-object v3, p0, Lcom/google/android/gms/common/server/response/FastParser;->mSmallSb:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, v2, v3, v0}, Lcom/google/android/gms/common/server/response/FastParser;->readString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v2

    const/16 v3, 0x3a

    if-eq v2, v3, :cond_0

    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v3, "Expected key/value separator"

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_2
        0x5d -> :sswitch_1
        0x7d -> :sswitch_0
    .end sparse-switch
.end method

.method private readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C
    .locals 3
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, -0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    invoke-virtual {p1, v1}, Ljava/io/BufferedReader;->read([C)I

    move-result v1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    aget-char v1, v1, v0

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    invoke-virtual {p1, v1}, Ljava/io/BufferedReader;->read([C)I

    move-result v1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    aget-char v0, v1, v0

    goto :goto_0
.end method

.method private readString(Ljava/io/BufferedReader;[CLjava/lang/StringBuilder;[C)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # [C
    .param p3    # Ljava/lang/StringBuilder;
    .param p4    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    const/4 v3, 0x0

    array-length v4, p2

    invoke-virtual {p1, v4}, Ljava/io/BufferedReader;->mark(I)V

    :goto_0
    invoke-virtual {p1, p2}, Ljava/io/BufferedReader;->read([C)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    aget-char v1, p2, v2

    invoke-static {v1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, p4, v1}, Lcom/google/android/gms/common/server/response/FastParser;->charArrayContains([CC)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected control character while reading string"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/16 v4, 0x22

    if-ne v1, v4, :cond_1

    const/16 v4, 0x5c

    if-eq v3, v4, :cond_1

    invoke-virtual {p3, p2, v5, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    add-int/lit8 v4, v2, 0x1

    int-to-long v4, v4

    invoke-virtual {p1, v4, v5}, Ljava/io/BufferedReader;->skip(J)J

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    :cond_1
    move v3, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    array-length v4, p2

    invoke-virtual {p1, v4}, Ljava/io/BufferedReader;->mark(I)V

    goto :goto_0

    :cond_3
    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected EOF while parsing string"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private readValueChars(Ljava/io/BufferedReader;[C)I
    .locals 10
    .param p1    # Ljava/io/BufferedReader;
    .param p2    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0x2c

    const/16 v6, 0x22

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    if-nez v1, :cond_0

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected EOF"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    if-ne v1, v9, :cond_1

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Missing value"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    const/16 v5, 0x6e

    if-ne v1, v5, :cond_2

    sget-object v5, Lcom/google/android/gms/common/server/response/FastParser;->NULL_TRAIL:[C

    invoke-direct {p0, p1, v5}, Lcom/google/android/gms/common/server/response/FastParser;->expectChars(Ljava/io/BufferedReader;[C)V

    move v2, v4

    :goto_0
    return v2

    :cond_2
    const/16 v5, 0x400

    invoke-virtual {p1, v5}, Ljava/io/BufferedReader;->mark(I)V

    const/4 v2, 0x1

    if-ne v1, v6, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    array-length v4, p2

    if-ge v2, v4, :cond_8

    invoke-virtual {p1, p2, v2, v8}, Ljava/io/BufferedReader;->read([CII)I

    move-result v4

    if-eq v4, v7, :cond_8

    aget-char v0, p2, v2

    invoke-static {v0}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected control character while reading string"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    if-ne v0, v6, :cond_4

    const/16 v4, 0x5c

    if-eq v3, v4, :cond_4

    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    add-int/lit8 v4, v2, 0x1

    int-to-long v4, v4

    invoke-virtual {p1, v4, v5}, Ljava/io/BufferedReader;->skip(J)J

    goto :goto_0

    :cond_4
    move v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    aput-char v1, p2, v4

    :goto_2
    array-length v5, p2

    if-ge v2, v5, :cond_8

    invoke-virtual {p1, p2, v2, v8}, Ljava/io/BufferedReader;->read([CII)I

    move-result v5

    if-eq v5, v7, :cond_8

    aget-char v5, p2, v2

    const/16 v6, 0x7d

    if-eq v5, v6, :cond_6

    aget-char v5, p2, v2

    if-eq v5, v9, :cond_6

    aget-char v5, p2, v2

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-nez v5, :cond_6

    aget-char v5, p2, v2

    const/16 v6, 0x5d

    if-ne v5, v6, :cond_7

    :cond_6
    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    add-int/lit8 v5, v2, -0x1

    int-to-long v5, v5

    invoke-virtual {p1, v5, v6}, Ljava/io/BufferedReader;->skip(J)J

    aput-char v4, p2, v2

    goto :goto_0

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    array-length v4, p2

    if-ne v2, v4, :cond_9

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Absurdly long value"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_9
    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v5, "Unexpected EOF"

    invoke-direct {v4, v5}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private skipToNextField(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 11
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, -0x1

    const/16 v10, 0x22

    const/4 v9, 0x5

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/16 v7, 0x400

    invoke-virtual {p1, v7}, Ljava/io/BufferedReader;->mark(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mLargeBuf:[C

    invoke-direct {p0, p1, v5}, Lcom/google/android/gms/common/server/response/FastParser;->readValueChars(Ljava/io/BufferedReader;[C)I

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v4

    sparse-switch v4, :sswitch_data_1

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected token "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_0
    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    invoke-virtual {p1, v5}, Ljava/io/BufferedReader;->read([C)I

    move-result v5

    if-ne v5, v8, :cond_1

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Unexpected EOF while parsing string"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    aget-char v1, v5, v6

    :cond_2
    if-ne v1, v10, :cond_3

    const/16 v5, 0x5c

    if-ne v3, v5, :cond_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    invoke-virtual {p1, v5}, Ljava/io/BufferedReader;->read([C)I

    move-result v5

    if-ne v5, v8, :cond_4

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Unexpected EOF while parsing string"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_4
    move v3, v1

    iget-object v5, p0, Lcom/google/android/gms/common/server/response/FastParser;->mChar:[C

    aget-char v1, v5, v6

    invoke-static {v1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Unexpected control character while reading string"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_1
    iget-object v6, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v6, 0x20

    invoke-virtual {p1, v6}, Ljava/io/BufferedReader;->mark(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v4

    const/16 v6, 0x7d

    if-ne v4, v6, :cond_5

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto :goto_0

    :cond_5
    if-ne v4, v10, :cond_7

    invoke-virtual {p1}, Ljava/io/BufferedReader;->reset()V

    const-wide/16 v6, 0x1

    invoke-virtual {p1, v6, v7}, Ljava/io/BufferedReader;->skip(J)J

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readFieldKey(Ljava/io/BufferedReader;)Ljava/lang/String;

    :cond_6
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->skipToNextField(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto/16 :goto_0

    :cond_7
    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected token "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_2
    iget-object v7, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v4

    const/16 v7, 0x5d

    if-ne v4, v7, :cond_8

    invoke-direct {p0, v9}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x1

    :goto_1
    if-lez v0, :cond_f

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readNextNonWhitespaceChar(Ljava/io/BufferedReader;)C

    move-result v1

    if-nez v1, :cond_9

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Unexpected EOF while parsing array"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_9
    invoke-static {v1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v7

    if-eqz v7, :cond_a

    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Unexpected control character while reading array"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_a
    if-ne v1, v10, :cond_b

    const/16 v7, 0x5c

    if-eq v3, v7, :cond_b

    if-nez v2, :cond_e

    move v2, v5

    :cond_b
    :goto_2
    const/16 v7, 0x5b

    if-ne v1, v7, :cond_c

    if-nez v2, :cond_c

    add-int/lit8 v0, v0, 0x1

    :cond_c
    const/16 v7, 0x5d

    if-ne v1, v7, :cond_d

    if-nez v2, :cond_d

    add-int/lit8 v0, v0, -0x1

    :cond_d
    move v3, v1

    goto :goto_1

    :cond_e
    move v2, v6

    goto :goto_2

    :cond_f
    invoke-direct {p0, v9}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    goto/16 :goto_0

    :sswitch_3
    new-instance v5, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    const-string v6, "Missing value"

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_4
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastParser;->readFieldKey(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v5

    :goto_3
    return-object v5

    :sswitch_5
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V

    const/4 v5, 0x0

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x2c -> :sswitch_3
        0x5b -> :sswitch_2
        0x7b -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_4
        0x7d -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 5
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v3, 0x400

    invoke-direct {v0, v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/server/response/FastParser;->mStateStack:Ljava/util/Stack;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/server/response/FastParser;->parseInternal(Ljava/io/BufferedReader;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/gms/common/server/response/FastParser;->assumePop(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/gms/common/server/response/FastParser;->TAG:Ljava/lang/String;

    const-string v3, "Failed to close reader while parsing."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_2
    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser$ParseException;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1
    throw v2

    :catch_2
    move-exception v1

    sget-object v3, Lcom/google/android/gms/common/server/response/FastParser;->TAG:Ljava/lang/String;

    const-string v4, "Failed to close reader while parsing."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public parse(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/common/server/response/FastParser$ParseException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/common/server/response/FastParser;->parse(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/gms/common/server/response/FastParser;->TAG:Ljava/lang/String;

    const-string v3, "Failed to close the input stream while parsing."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v2

    :catch_1
    move-exception v1

    sget-object v3, Lcom/google/android/gms/common/server/response/FastParser;->TAG:Ljava/lang/String;

    const-string v4, "Failed to close the input stream while parsing."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
