.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/dynamic/OnDelegateCreatedListener",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDelegateCreated(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # setter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v2, p1}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$002(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Lcom/google/android/gms/dynamic/LifecycleDelegate;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mQueuedActions:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$100(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v2}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$000(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;->run(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mQueuedActions:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$100(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$1;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mSavedStateToProcess:Landroid/os/Bundle;
    invoke-static {v2, v3}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$202(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Landroid/os/Bundle;)Landroid/os/Bundle;

    return-void
.end method
