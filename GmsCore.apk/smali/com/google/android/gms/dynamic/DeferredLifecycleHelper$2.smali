.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->onInflate(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$attrs:Landroid/os/Bundle;

.field final synthetic val$savedInstanceState:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    iput-object p2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$attrs:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public run(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/dynamic/LifecycleDelegate;

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v0}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$000(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$attrs:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$2;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/dynamic/LifecycleDelegate;->onInflate(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void
.end method
