.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->showGooglePlayUnavailableMessage(Landroid/widget/FrameLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$errorCode:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    iput-object p2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->val$context:Landroid/content/Context;

    iput p3, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->val$errorCode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->val$context:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$5;->val$errorCode:I

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getGooglePlayServicesAvailabilityRecoveryIntent(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
