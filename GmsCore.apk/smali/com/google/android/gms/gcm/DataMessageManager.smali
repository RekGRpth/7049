.class public Lcom/google/android/gms/gcm/DataMessageManager;
.super Landroid/content/BroadcastReceiver;
.source "DataMessageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;,
        Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;
    }
.end annotation


# static fields
.field private static final NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityManager:Landroid/app/ActivityManager;

.field private mContext:Landroid/content/Context;

.field mEndpoint:Lcom/google/buzz/mobile/GcmClient;

.field private mHandler:Landroid/os/Handler;

.field private mLockObject:Ljava/lang/Object;

.field private final mReceiverLookupMissInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSendFgStatus:Z

.field mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

.field private mUserManager:Lcom/google/android/gms/gcm/UserManager;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "android.intent.category.MASTER_CLEAR"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf.subscribedfeeds"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "INSTALL_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "REMOVE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "SERVER_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "DECLINE_ASSET"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.gsf"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    const-string v1, "com.google.android.apps.googlevoice.INBOX_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/gcm/UserManager;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/google/android/gms/gcm/UserManager;

    const/4 v3, 0x1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mSendFgStatus:Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mLockObject:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "GOOGLE_C2DM"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    iput-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mActivityManager:Landroid/app/ActivityManager;

    iput-object p3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    new-instance v1, Lcom/google/android/gms/gcm/StatusManager;

    iget-object v2, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/gcm/StatusManager;-><init>(Landroid/content/Context;Lcom/google/android/gms/gcm/DataMessageManager;)V

    iput-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V

    return-void
.end method

.method private extractExtras(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/util/Map;)V
    .locals 6
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;",
            "Landroid/content/Intent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getAppDataList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "from"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "GOOGLE."

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "GOOGLE."

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string v4, "GCM/DMM"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parse intent data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    return-void
.end method

.method public static getAppPackage(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Intent;

    const-string v3, "app"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v3, v2, Landroid/app/PendingIntent;

    if-eqz v3, :cond_0

    move-object v0, v2

    check-cast v0, Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getPermission(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p2    # Landroid/content/Intent;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    invoke-virtual {v1, p3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "INSTALL_ASSET"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "REMOVE_ASSET"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "DECLINE_ASSET"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "UPDATES_AVAILABLE"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SERVER_NOTIFICATION"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".permission.C2D_MESSAGE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "GCM/DMM"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private parseDataMessageIntent(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)Landroid/content/Intent;
    .locals 7
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "GCM/DMM"

    const-string v6, "found msg w/o category, dropping"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string v5, "GSYNC_TICKLE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v0, "com.google.android.gsf.subscribedfeeds"

    :cond_2
    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getDeviceUserId()I

    move-result v3

    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/google/android/gms/gcm/DataMessageManager;->NON_PACKAGE_CATEGORIES:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    const-string v5, "from"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "collapse_key"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private processInternalMessage(Ljava/lang/String;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p3    # Landroid/content/Intent;

    invoke-virtual {p2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.gsf"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "google.com"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "registration_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "com.google.android.c2dm.intent.REGISTRATION"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p3, p1}, Landroid/content/Intent;->removeCategory(Ljava/lang/String;)V

    const-string v2, "app"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "error_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public static queryPackageManagerForBroadcastIntentReceiver(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const/4 v2, 0x0

    const/4 v5, -0x1

    if-lez p2, :cond_2

    invoke-static {p1, p2}, Lcom/google/android/gms/gcm/Compat;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v6, "GCM/DMM"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "findReceiverForIntent: queryBroadcastReceivers took "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    sub-long/2addr v7, v3

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms, found="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_1
    return v0

    :cond_2
    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private shouldUnregisterRegIdForApp(Landroid/content/Intent;I)Z
    .locals 12
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    iget-object v8, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v8, "c2dm_num_app_lookup_misses_before_unregister"

    const/4 v9, 0x1

    invoke-static {v0, v8, v9}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    const-string v8, "c2dm_min_seconds_before_missing_app_unregister"

    const-wide/16 v9, 0x0

    invoke-static {v0, v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long v2, v8, v10

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v9, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    monitor-enter v9

    :try_start_0
    iget-object v8, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;

    if-nez v4, :cond_0

    new-instance v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;

    invoke-direct {v4}, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;-><init>()V

    iget-object v8, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mReceiverLookupMissInfo:Ljava/util/HashMap;

    invoke-virtual {v8, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v8, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-static {v8, p1, p2}, Lcom/google/android/gms/gcm/DataMessageManager;->queryPackageManagerForBroadcastIntentReceiver(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x0

    iput v8, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->count:I

    const/4 v8, 0x0

    monitor-exit v9

    :goto_0
    return v8

    :cond_1
    iget v8, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->count:I

    if-nez v8, :cond_2

    iput-wide v5, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->timeOfFirstMiss:J

    :cond_2
    iget v8, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->count:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->count:I

    iget v8, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->count:I

    if-lt v8, v7, :cond_3

    iget-wide v10, v4, Lcom/google/android/gms/gcm/DataMessageManager$MissInfo;->timeOfFirstMiss:J

    add-long/2addr v10, v2

    cmp-long v8, v5, v10

    if-ltz v8, :cond_3

    const/4 v8, 0x1

    :goto_1
    monitor-exit v9

    goto :goto_0

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    :cond_3
    const/4 v8, 0x0

    goto :goto_1
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected afterBroadcast(Landroid/content/Intent;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;JILandroid/os/Bundle;I)V
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p3    # J
    .param p5    # I
    .param p6    # Landroid/os/Bundle;
    .param p7    # I

    if-nez p5, :cond_0

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "broadcast intent callback: result=CANCELLED for"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p7}, Lcom/google/android/gms/gcm/DataMessageManager;->shouldUnregisterRegIdForApp(Landroid/content/Intent;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/gcm/DataMessageManager;->unregisterApp(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/DataMessageManager;->releaseWakeLock()V

    return-void
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/StatusManager;->destroy()V

    return-void
.end method

.method public handleMCSDataMessage(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V
    .locals 5
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getAppDataList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UFS"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "1"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mSendFgStatus:Z

    iget-object v4, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/UserManager;->sendUserStatus()V

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v4, p1}, Lcom/google/android/gms/gcm/StatusManager;->handleMCSDataMessage(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "GCM/DMM"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "GCM/DMM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DataMessageManager receive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->handleUserPresent()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->handleScreenOff()V

    goto :goto_0

    :cond_4
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->handleScreenOn()V

    goto :goto_0

    :cond_6
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/google/android/gms/gcm/StatusManager;->mPluggedIn:Z

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iget-boolean v3, v3, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->maybeSendSignals()V

    goto :goto_0

    :cond_7
    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iput-boolean v6, v3, Lcom/google/android/gms/gcm/StatusManager;->mPluggedIn:Z

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iget-boolean v3, v3, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->maybeSendSignals()V

    goto :goto_0

    :cond_8
    const-string v3, "android.net.conn.DATA_ACTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "isActive"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v3, "deviceType"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iput-boolean v1, v3, Lcom/google/android/gms/gcm/StatusManager;->mIsNetworkDataActive:Z

    if-eqz v1, :cond_1

    invoke-static {v2}, Lcom/google/android/gms/gcm/Compat;->isNetworkTypeMobile(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iget-boolean v3, v3, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    invoke-virtual {v3}, Lcom/google/android/gms/gcm/StatusManager;->maybeSendSignals()V

    goto/16 :goto_0

    :cond_9
    const-string v3, "android.intent.action.USER_STOPPED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "android.intent.action.USER_STOPPING"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    invoke-virtual {v3, p0, p2}, Lcom/google/android/gms/gcm/UserManager;->sendUserStatus(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public processPacket(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V
    .locals 9
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getCategory()Ljava/lang/String;

    move-result-object v6

    const-string v0, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/gcm/DataMessageManager;->handleMCSDataMessage(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/DataMessageManager;->parseDataMessageIntent(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "GCM/DMM"

    const-string v1, "processPacket: cannot parse data message "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v2, v8}, Lcom/google/android/gms/gcm/DataMessageManager;->extractExtras(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/util/Map;)V

    invoke-direct {p0, v6, p1, v2}, Lcom/google/android/gms/gcm/DataMessageManager;->processInternalMessage(Ljava/lang/String;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;)V

    new-instance v0, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;-><init>(Lcom/google/android/gms/gcm/DataMessageManager;Landroid/content/Intent;Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;J)V

    move-object v3, p0

    move-object v4, p1

    move-object v5, v2

    move-object v7, v0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/gcm/DataMessageManager;->sendGCMBroadcast(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public releaseWakeLock()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mLockObject:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendDataMessageStanza(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const-string v9, "use_rmq"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v9, "use_rmq"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/gcm/DataMessageManager;->getAppPackage(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v9, "missing_package_name"

    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/gcm/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v9, "app"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v9, "registration_id"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "registration_id"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    if-nez v6, :cond_1

    const-string v9, "missing_reg_id"

    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/gcm/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v7, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-direct {v7}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;-><init>()V

    const-string v9, "d2cm@google.com"

    invoke-virtual {v7, v9}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {v7, v1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {v7, v6}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setRegId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    instance-of v9, v5, Ljava/lang/String;

    if-eqz v9, :cond_3

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    invoke-virtual {v0, v4}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v7, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_1

    :cond_3
    const-string v9, "invalid_params"

    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/gcm/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v9, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v9}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v9

    if-nez v9, :cond_5

    if-nez v8, :cond_5

    const-string v9, "no_connection"

    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/gcm/DataMessageManager;->processSendDataMessageFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v9, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v9, v7, v8}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    goto :goto_0
.end method

.method public sendGCMBroadcast(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;Ljava/util/Map;)V
    .locals 10
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .param p2    # Landroid/content/Intent;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/gcm/DataMessageManager;->getPermission(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getDeviceUserId()I

    move-result v4

    iput v4, p4, Lcom/google/android/gms/gcm/DataMessageManager$BroadcastDoneReceiver;->mUserSerial:I

    const-string v0, "GCM/DMM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Send broadcast "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v2, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v9, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " extras: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/DataMessageManager;->acquireWakeLock()V

    if-gtz v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v3, p4

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    :goto_2
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " with permission="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lcom/google/android/gms/gcm/Compat;->isUserRunning(I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "GCM/DMM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to send message to stopped user "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mHandler:Landroid/os/Handler;

    move-object v5, p2

    move-object v7, p4

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/gcm/Compat;->sendOrderedBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;)V

    goto :goto_2
.end method

.method public setClient(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/GcmClient;

    iput-object p1, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/UserManager;->setClient(Lcom/google/buzz/mobile/GcmClient;)V

    :cond_0
    return-void
.end method

.method public setIsDevicePluggedIn(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mStatusManager:Lcom/google/android/gms/gcm/StatusManager;

    iput-boolean p1, v0, Lcom/google/android/gms/gcm/StatusManager;->mPluggedIn:Z

    return-void
.end method

.method protected unregisterApp(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Should not happen. Received intent with no package name. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/gcm/DataMessageManager;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "GCM/DMM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receiver package not found, unregister application "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sender "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "app"

    iget-object v3, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v3, v5, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "app_gsf"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/gcm/DataMessageManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
