.class public Lcom/google/android/gms/gcm/GcmService;
.super Landroid/app/Service;
.source "GcmService.java"


# static fields
.field static SERVICE:Lcom/google/android/gms/gcm/GcmService;

.field static sAndroidId:Ljava/lang/String;

.field static sServiceSem:Ljava/util/concurrent/Semaphore;


# instance fields
.field mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

.field mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

.field private mHandler:Landroid/os/Handler;

.field mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

.field private mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

.field mOverrideReg:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

.field mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

.field mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

.field mRegistrationStore:Lcom/google/android/gms/gcm/RegistrationStore;

.field private mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

.field mSecret:Ljava/lang/String;

.field protected mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mUserManager:Lcom/google/android/gms/gcm/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->sServiceSem:Ljava/util/concurrent/Semaphore;

    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/GcmService;->mOverrideReg:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/gcm/GcmService;I)V
    .locals 0
    .param p0    # Lcom/google/android/gms/gcm/GcmService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/GcmService;->stopServiceIfConnectionDisabled(I)V

    return-void
.end method

.method static getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/ContentResolver;

    const-string v0, "0"

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android_id"

    const-string v1, "0"

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    :cond_0
    const-string v0, "0"

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized preInit(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/gcm/GcmService;

    monitor-enter v1

    :try_start_0
    const-string v0, "0"

    sget-object v2, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/GcmService;->getAndroidId(Landroid/content/ContentResolver;)Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/gcm/Compat;->init(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private stopServiceIfConnectionDisabled(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/AndroidGcmClient;->getPort()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized initClient()V
    .locals 9

    const/4 v8, -0x1

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmService;->preInit(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/GcmProvisioning;->getToken()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "gcm_secure_port"

    const/4 v6, -0x1

    invoke-static {v2, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    sget-object v6, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/gcm/PushMessagingRegistrar;->setAndroidAuth(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "0"

    sget-object v6, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    if-nez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/GcmProvisioning;->disableGcmConnection()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->isGcmConnectionEnabled(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/GcmProvisioning;->disableGcmConnection()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    if-ne v1, v8, :cond_3

    :try_start_2
    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/GcmProvisioning;->enableGcmConnection()I

    move-result v1

    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    sget-object v6, Lcom/google/android/gms/gcm/GcmService;->sAndroidId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/gcm/AndroidGcmClient;->setAndroidIdAuth(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/gms/gcm/PushMessagingRegistrarProxy;->initSettings(Landroid/content/ContentResolver;)V

    const-string v5, "gcm_enable_registration"

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/google/android/gms/gcm/PushMessagingRegistrarProxy;->sRegistrationEnable:I

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "gtalk_rmq_ack_interval"

    const/16 v8, 0xa

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/gcm/AndroidGcmClient;->setAckingInterval(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "gtalk_ssl_handshake_timeout_ms"

    const v7, 0xea60

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    new-instance v5, Landroid/net/SSLSessionCache;

    invoke-direct {v5, p0}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    invoke-static {v4, v5}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/gcm/AndroidGcmClient;->setSocketFactory(Ljavax/net/SocketFactory;)V

    const-string v5, "gtalk_hostname"

    const-string v6, "mtalk.google.com"

    invoke-static {v2, v5, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/gms/gcm/AndroidGcmClient;->setServer(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 13

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Lcom/google/android/gms/gcm/Compat;->init(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/GcmProvisioning;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mPowerManager:Landroid/os/PowerManager;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mPowerManager:Landroid/os/PowerManager;

    const-string v1, "GTALK_CONN"

    invoke-virtual {v0, v12, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mSimpleWakeLock:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/ReconnectManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    new-instance v0, Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/Rmq2Manager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    new-instance v0, Lcom/google/android/common/http/GoogleHttpClient;

    const-string v1, "Android-GCM/1.2"

    invoke-direct {v0, p0, v1, v11}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    new-instance v0, Lcom/google/android/gms/gcm/GcmService$1;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/gcm/GcmService$1;-><init>(Lcom/google/android/gms/gcm/GcmService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mRegistrationStore:Lcom/google/android/gms/gcm/RegistrationStore;

    new-instance v0, Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->mRegistrationStore:Lcom/google/android/gms/gcm/RegistrationStore;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/gcm/PushMessagingRegistrar;-><init>(Landroid/content/Context;Lcom/google/android/common/http/GoogleHttpClient;Lcom/google/android/gms/gcm/RegistrationStore;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    new-instance v0, Lcom/google/android/gms/gcm/HeartbeatAlarm;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/gcm/HeartbeatAlarm;-><init>(Landroid/content/Context;Lcom/google/android/gms/gcm/ReconnectManager;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-static {}, Lcom/google/android/gms/gcm/Compat;->supportsMultipleUsers()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/gcm/UserManager;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/UserManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    :cond_0
    new-instance v0, Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/gcm/DataMessageManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/gcm/UserManager;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    new-instance v0, Lcom/google/android/gms/gcm/AndroidGcmClient;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    iget-object v4, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v6, p0, Lcom/google/android/gms/gcm/GcmService;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    iget-object v7, p0, Lcom/google/android/gms/gcm/GcmService;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/gcm/AndroidGcmClient;-><init>(Lcom/google/android/gms/gcm/GcmService;Lcom/google/android/gms/gcm/Rmq2Manager;Lcom/google/android/gms/gcm/HeartbeatAlarm;Lcom/google/android/gms/gcm/ReconnectManager;Lcom/google/android/gms/gcm/DataMessageManager;Lcom/google/android/gms/gcm/UserManager;Lcom/google/android/gms/gcm/GcmProvisioning;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/ReconnectManager;->init()V

    sput-object p0, Lcom/google/android/gms/gcm/GcmService;->SERVICE:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.intent.action.MCS_HEARTBEAT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.google.android.intent.action.GTALK_RECONNECT"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/gcm/Compat;->supportsMultipleUsers()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "android.intent.action.USER_ADDED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_REMOVED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_STOPPED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_STOPPING"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.net.conn.DATA_ACTIVITY_CHANGE"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_1
    const-string v0, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/4 v0, 0x0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v8

    const-string v0, "plugged"

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    if-eq v10, v12, :cond_2

    const/4 v0, 0x2

    if-ne v10, v0, :cond_3

    :cond_2
    move v0, v12

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/DataMessageManager;->setIsDevicePluggedIn(Z)V

    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->initClient()V

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->sServiceSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :cond_3
    move v0, v11

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    sput-object v2, Lcom/google/android/gms/gcm/GcmService;->SERVICE:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mClient:Lcom/google/android/gms/gcm/AndroidGcmClient;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/AndroidGcmClient;->disconnect(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    iget-object v0, v0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->destroy()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->destroy()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/DataMessageManager;->destroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "GCM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GcmService start "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/gms/gcm/Compat;->getUserSerial()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->checkGmsRegistration(Landroid/content/Context;)V

    invoke-virtual {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopServiceIfConnectionDisabled(I)V

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.c2dm.intent.REGISTER"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "GCM"

    const-string v3, "Missing token, falling back to GSF"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "GOOG.GMS_FALLBACK"

    const-string v3, "1"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "com.google.android.gsf"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    move v1, v2

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/Thread;

    new-instance v3, Lcom/google/android/gms/gcm/GcmService$2;

    invoke-direct {v3, p0, p1, p3}, Lcom/google/android/gms/gcm/GcmService$2;-><init>(Lcom/google/android/gms/gcm/GcmService;Landroid/content/Intent;I)V

    invoke-direct {v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    if-nez v3, :cond_6

    invoke-virtual {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    goto :goto_0

    :cond_6
    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->checkGmsRegistration(Landroid/content/Context;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v1, p0, p1}, Lcom/google/android/gms/gcm/ReconnectManager;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->resetBlackoutPeriod()V

    :cond_8
    :goto_1
    const-string v1, "com.google.android.c2dm.intent.SEND_DATA_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/gcm/DataMessageManager;->sendDataMessageStanza(Landroid/content/Intent;)V

    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopServiceIfConnectionDisabled(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_9
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->initClient()V

    goto :goto_1

    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mSecret:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    :cond_b
    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopServiceIfConnectionDisabled(I)V

    move v1, v2

    goto/16 :goto_0
.end method

.method resetBlackoutPeriod()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/PushMessagingRegistrar;->resetBlackoutPeriod()V

    return-void
.end method
