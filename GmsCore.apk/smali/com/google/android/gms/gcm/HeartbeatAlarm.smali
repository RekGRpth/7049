.class public Lcom/google/android/gms/gcm/HeartbeatAlarm;
.super Landroid/content/BroadcastReceiver;
.source "HeartbeatAlarm.java"


# instance fields
.field mAlarm:Lcom/google/android/gms/gcm/Alarm;

.field private mContext:Landroid/content/Context;

.field private mDefaultHeartbeat:I

.field private mEndpoint:Lcom/google/buzz/mobile/GcmClient;

.field mHeartbeatAckInterval:I

.field private mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

.field private mResetConnectionTs:J

.field private mServerHeartbeatInterval:I

.field private mUseWifiHeartbeatInterval:Z

.field private mWaitingForAck:Z

.field private wifiHeartbeatInterval:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/gcm/ReconnectManager;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gtalk_wifi_max_heartbeat_ping_interval_ms"

    const v3, 0xdbba0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->wifiHeartbeatInterval:I

    const-string v1, "gtalk_heartbeat_ack_timeout_ms"

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mHeartbeatAckInterval:I

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gtalk_nosync_heartbeat_ping_interval_ms"

    const v3, 0x19a280

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mDefaultHeartbeat:I

    new-instance v1, Lcom/google/android/gms/gcm/Alarm;

    const-string v2, "GTALK_HB_ALARM"

    invoke-direct {v1, p1, v2, p0}, Lcom/google/android/gms/gcm/Alarm;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    const-string v2, "com.google.android.intent.action.MCS_HEARTBEAT"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/gcm/Alarm;->setAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/Alarm;->init()V

    return-void
.end method

.method private computeHeartbeatInterval()J
    .locals 8

    const/4 v7, 0x1

    iget v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mDefaultHeartbeat:I

    int-to-long v1, v5

    iget v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mServerHeartbeatInterval:I

    int-to-long v3, v5

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_0

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    move-wide v1, v3

    :cond_0
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mUseWifiHeartbeatInterval:Z

    iget-object v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/ReconnectManager;->getNetworkType()I

    move-result v0

    if-ne v0, v7, :cond_1

    iget v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->wifiHeartbeatInterval:I

    int-to-long v5, v5

    cmp-long v5, v1, v5

    if-lez v5, :cond_1

    iget v5, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->wifiHeartbeatInterval:I

    int-to-long v1, v5

    iput-boolean v7, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mUseWifiHeartbeatInterval:Z

    :cond_1
    return-wide v1
.end method

.method private static logHeartbeatReset(IILjava/lang/String;)V
    .locals 5
    .param p0    # I
    .param p1    # I
    .param p2    # Ljava/lang/String;

    shl-int/lit8 v1, p1, 0x10

    add-int v0, v1, p0

    const v1, 0x31ce4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void
.end method

.method private resetHeartbeatAlarm()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->setAlarm()V

    return-void
.end method

.method private setAlarm()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mHeartbeatAckInterval:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/Alarm;->start(J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-direct {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->computeHeartbeatInterval()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/Alarm;->start(J)V

    goto :goto_0
.end method


# virtual methods
.method public clearAlarm()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->stop()V

    return-void
.end method

.method public markPacketReception()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->resetHeartbeatAlarm()V

    return-void
.end method

.method public onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V
    .locals 5
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginResponse;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;->getHeartbeatConfig()Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIntervalMs()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;->getIntervalMs()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mServerHeartbeatInterval:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->startAlarm()V

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/Alarm;->getAlarmTimer()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    long-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/ReconnectManager;->getNetworkType()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v3}, Lcom/google/buzz/mobile/GcmClient;->getRemoteAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->logHeartbeatReset(IILjava/lang/String;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCM"

    const-string v1, "Ignoring attempt to send heartbeat on dead connection."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    if-eqz v0, :cond_1

    const-string v0, "GCM"

    const-string v1, "Heartbeat timeout, GCM connection reset"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->clearAlarm()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mResetConnectionTs:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v1}, Lcom/google/buzz/mobile/GcmClient;->getTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->sendHeartbeat()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    invoke-direct {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->resetHeartbeatAlarm()V

    goto :goto_0
.end method

.method releaseWakelock()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method public setClient(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 0
    .param p1    # Lcom/google/buzz/mobile/GcmClient;

    iput-object p1, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    return-void
.end method

.method public startAlarm()V
    .locals 2

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mResetConnectionTs:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/HeartbeatAlarm;->mWaitingForAck:Z

    invoke-direct {p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->setAlarm()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
