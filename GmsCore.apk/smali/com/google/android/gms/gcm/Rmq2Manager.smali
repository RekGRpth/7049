.class public Lcom/google/android/gms/gcm/Rmq2Manager;
.super Ljava/lang/Object;
.source "Rmq2Manager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;
    }
.end annotation


# static fields
.field static PROTOBUF_TAG:Ljava/lang/String;

.field static RMQ_ID:Ljava/lang/String;

.field private static RMQ_ID_PROJECTION:[Ljava/lang/String;


# instance fields
.field private RMQ_PROJECTION:[Ljava/lang/String;

.field mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "type"

    sput-object v0, Lcom/google/android/gms/gcm/Rmq2Manager;->PROTOBUF_TAG:Ljava/lang/String;

    const-string v0, "rmq_id"

    sput-object v0, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->PROTOBUF_TAG:Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_PROJECTION:[Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;-><init>(Lcom/google/android/gms/gcm/Rmq2Manager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    return-void
.end method

.method private final queryHighestRmqId()J
    .locals 14

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v9, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v2, "outgoingRmqMessages"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v7, "rmq_id DESC"

    const-string v8, "1"

    const/4 v5, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const-wide/16 v11, 0x0

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v11

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-wide v11

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2
.end method


# virtual methods
.method public addS2dId(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v4, "s2dRmqIds"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    goto :goto_0
.end method

.method public deleteS2dIds(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/16 v7, 0x64

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v1, :cond_0

    add-int v10, v8, v7

    invoke-static {v10, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v10, v3, v8

    new-array v6, v10, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    move v5, v4

    move v9, v8

    :goto_1
    if-ge v9, v3, :cond_3

    if-lez v5, :cond_2

    const-string v10, " OR "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    sget-object v10, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "=?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v5, 0x1

    add-int/lit8 v8, v9, 0x1

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    aput-object v10, v6, v5

    move v5, v4

    move v9, v8

    goto :goto_1

    :cond_3
    iget-object v10, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v10}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v10, "s2dRmqIds"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    int-to-long v10, v10

    move v8, v9

    goto :goto_0
.end method

.method public enqueueMessage(IBLcom/google/protobuf/micro/MessageMicro;)V
    .locals 7
    .param p1    # I
    .param p2    # B
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;

    iget-object v5, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v5}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    sget-object v5, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p3}, Lcom/google/protobuf/micro/MessageMicro;->toByteArray()[B

    move-result-object v0

    const-string v5, "data"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    sget-object v5, Lcom/google/android/gms/gcm/Rmq2Manager;->PROTOBUF_TAG:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    const-string v5, "outgoingRmqMessages"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-void
.end method

.method public getInitialOutgoinPersistentId()J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/gms/gcm/Rmq2Manager;->queryHighestRmqId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/Rmq2Manager;->queryLastRmqId()J

    move-result-wide v0

    :cond_0
    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    return-wide v2
.end method

.method public getS2dIds()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v9, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v2, "s2dRmqIds"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID_PROJECTION:[Ljava/lang/String;

    const-string v7, "rmq_id ASC"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_1

    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v12
.end method

.method final queryLastRmqId()J
    .locals 14

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v9, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v2, "lastrmqid"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v7, "rmq_id DESC"

    const-string v8, "1"

    const/4 v5, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const-wide/16 v11, 0x0

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v11

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-wide v11

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public removeMessagesByRmq2Ids(Ljava/util/List;J)I
    .locals 17
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;J)I"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    if-nez v15, :cond_1

    :cond_0
    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v15}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v14, "outgoingRmqMessages"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    new-array v13, v15, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v12, 0x0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    if-lez v6, :cond_2

    const-string v15, " OR "

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    sget-object v15, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=?"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v6, 0x1

    aput-object v9, v13, v6

    :try_start_0
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    cmp-long v15, v10, p2

    if-ltz v15, :cond_3

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/Rmq2Manager;->saveLastOutgoingPersistentId(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    move v6, v7

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v14, v15, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    :catch_0
    move-exception v15

    goto :goto_2
.end method

.method public resendPackets(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 27
    .param p1    # Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v7}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const/4 v15, 0x0

    new-instance v5, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v5}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v7, "outgoingRmqMessages"

    invoke-virtual {v5, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v10, 0x0

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x0

    sget-object v7, Lcom/google/android/gms/gcm/Rmq2Manager;->PROTOBUF_TAG:Ljava/lang/String;

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!=0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, 0x0

    const-string v12, "rmq_id ASC"

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    if-nez v16, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v7, "data"

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    sget-object v7, Lcom/google/android/gms/gcm/Rmq2Manager;->PROTOBUF_TAG:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    sget-object v7, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    :goto_1
    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_1

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-wide/from16 v1, v23

    move/from16 v3, v25

    move-object/from16 v4, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/buzz/mobile/GcmClient;->resend(JI[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v7

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v7

    :cond_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public saveLastOutgoingPersistentId(J)V
    .locals 4
    .param p1    # J

    iget-object v2, p0, Lcom/google/android/gms/gcm/Rmq2Manager;->mOpenHelper:Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/Rmq2Manager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_id"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Lcom/google/android/gms/gcm/Rmq2Manager;->RMQ_ID:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "lastrmqid"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method
