.class public Lcom/google/android/gms/gcm/StatusManager;
.super Landroid/content/BroadcastReceiver;
.source "StatusManager.java"


# instance fields
.field private mAlarm:Lcom/google/android/gms/gcm/Alarm;

.field private mContext:Landroid/content/Context;

.field private mDMM:Lcom/google/android/gms/gcm/DataMessageManager;

.field private mEnableDataActiveNotification:Z

.field private mEnableIdleNotification:Z

.field mEnablePowerStateNotification:Z

.field mIdleTimeout:J

.field mIsNetworkDataActive:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field mPluggedIn:Z

.field mUserIsIdle:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/gcm/DataMessageManager;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/gcm/DataMessageManager;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-boolean v4, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableIdleNotification:Z

    iput-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    iput-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableDataActiveNotification:Z

    iput-object p2, p0, Lcom/google/android/gms/gcm/StatusManager;->mDMM:Lcom/google/android/gms/gcm/DataMessageManager;

    iput-object p1, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    iput-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    new-instance v2, Lcom/google/android/gms/gcm/Alarm;

    iget-object v5, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    const-string v6, "GTALK_IDLE_ALARM"

    invoke-direct {v2, v5, v6, p0}, Lcom/google/android/gms/gcm/Alarm;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    const-string v5, "com.google.android.intent.action.SEND_IDLE"

    invoke-virtual {v2, v5}, Lcom/google/android/gms/gcm/Alarm;->setAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v2}, Lcom/google/android/gms/gcm/Alarm;->init()V

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.google.android.intent.action.SEND_IDLE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "plugged"

    const/4 v5, -0x1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    move v2, v4

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mPluggedIn:Z

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gtalk_idle_timeout_ms"

    const-wide/16 v4, 0x7530

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mIdleTimeout:J

    return-void

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method private sendDeviceIdleStatusForConnections()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableIdleNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/StatusManager;->maybeSendSignals()V

    :cond_0
    return-void
.end method

.method private setIsUserIdle(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/gms/gcm/StatusManager;->mUserIsIdle:Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->destroy()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public handleMCSDataMessage(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V
    .locals 5
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getAppDataList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "IdleNotification"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableIdleNotification:Z

    goto :goto_0

    :cond_1
    const-string v4, "PowerNotification"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    goto :goto_0

    :cond_2
    const-string v4, "DataActiveNotification"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableDataActiveNotification:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/StatusManager;->maybeSendSignals()V

    return-void
.end method

.method handleScreenOff()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/StatusManager;->isUserIdle()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget-wide v1, p0, Lcom/google/android/gms/gcm/StatusManager;->mIdleTimeout:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/Alarm;->start(J)V

    :cond_0
    return-void
.end method

.method handleScreenOn()V
    .locals 5

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "isKeyguardLocked"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/gcm/StatusManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/StatusManager;->handleUserPresent()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method handleUserPresent()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->stop()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/StatusManager;->isUserIdle()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/StatusManager;->setIsUserIdle(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/gcm/StatusManager;->sendDeviceIdleStatusForConnections()V

    :cond_1
    return-void
.end method

.method public isUserIdle()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mUserIsIdle:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public maybeSendSignals()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mDMM:Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v3, v3, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v3}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-direct {v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;-><init>()V

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnablePowerStateNotification:Z

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    const-string v3, "PowerNotification"

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mPluggedIn:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v2, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    :cond_2
    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableIdleNotification:Z

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    const-string v3, "IdleNotification"

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mUserIsIdle:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v2, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mEnableDataActiveNotification:Z

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    const-string v3, "DataActiveNotification"

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    iget-boolean v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mIsNetworkDataActive:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v2, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    :cond_4
    if-eqz v1, :cond_0

    const-string v3, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v2, v3}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    iget-object v3, p0, Lcom/google/android/gms/gcm/StatusManager;->mDMM:Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v3, v3, Lcom/google/android/gms/gcm/DataMessageManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/StatusManager;->setIsUserIdle(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/gcm/StatusManager;->sendDeviceIdleStatusForConnections()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/StatusManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->stop()V

    return-void
.end method
