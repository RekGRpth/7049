.class public final Lcom/google/android/gms/icing/Proto$CompactStatus;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CompactStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$CompactStatus;


# instance fields
.field private hasNumDocuments:Z

.field private hasTimestampSecs:Z

.field private memoizedSerializedSize:I

.field private numDocuments_:I

.field private timestampSecs_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$CompactStatus;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$CompactStatus;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CompactStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$CompactStatus;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$CompactStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$CompactStatus;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->timestampSecs_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->numDocuments_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->timestampSecs_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->numDocuments_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$8902(Lcom/google/android/gms/icing/Proto$CompactStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CompactStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasTimestampSecs:Z

    return p1
.end method

.method static synthetic access$9002(Lcom/google/android/gms/icing/Proto$CompactStatus;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CompactStatus;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->timestampSecs_:J

    return-wide p1
.end method

.method static synthetic access$9102(Lcom/google/android/gms/icing/Proto$CompactStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CompactStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasNumDocuments:Z

    return p1
.end method

.method static synthetic access$9202(Lcom/google/android/gms/icing/Proto$CompactStatus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CompactStatus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->numDocuments_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$CompactStatus;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CompactStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$CompactStatus;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;->create()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;->access$8700()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/gms/icing/Proto$CompactStatus;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CompactStatus;->newBuilder()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;

    # invokes: Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;->buildParsed()Lcom/google/android/gms/icing/Proto$CompactStatus;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;->access$8600(Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;)Lcom/google/android/gms/icing/Proto$CompactStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getNumDocuments()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->numDocuments_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasTimestampSecs()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasNumDocuments()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getNumDocuments()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getTimestampSecs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->timestampSecs_:J

    return-wide v0
.end method

.method public hasNumDocuments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasNumDocuments:Z

    return v0
.end method

.method public hasTimestampSecs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasTimestampSecs:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CompactStatus;->newBuilder()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->newBuilderForType()Lcom/google/android/gms/icing/Proto$CompactStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasTimestampSecs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getTimestampSecs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->hasNumDocuments()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->getNumDocuments()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    return-void
.end method
