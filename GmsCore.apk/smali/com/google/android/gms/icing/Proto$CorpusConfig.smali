.class public final Lcom/google/android/gms/icing/Proto$CorpusConfig;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;,
        Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusConfig;


# instance fields
.field private contentProviderUri_:Ljava/lang/String;

.field private hasContentProviderUri:Z

.field private hasId:Z

.field private hasName:Z

.field private hasPackageName:Z

.field private hasQueryLimit:Z

.field private hasType:Z

.field private hasVersion:Z

.field private id_:I

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private packageName_:Ljava/lang/String;

.field private queryLimit_:I

.field private sections_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$SectionConfig;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

.field private universalSectionMappings_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation
.end field

.field private version_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->id_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->version_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->packageName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->contentProviderUri_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->queryLimit_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->id_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->version_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->packageName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->contentProviderUri_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->queryLimit_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/android/gms/icing/Proto$CorpusConfig;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->id_:I

    return p1
.end method

.method static synthetic access$3902(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName:Z

    return p1
.end method

.method static synthetic access$4002(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion:Z

    return p1
.end method

.method static synthetic access$4202(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->version_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName:Z

    return p1
.end method

.method static synthetic access$4402(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->packageName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType:Z

    return p1
.end method

.method static synthetic access$4602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->type_:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri:Z

    return p1
.end method

.method static synthetic access$4802(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->contentProviderUri_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit:Z

    return p1
.end method

.method static synthetic access$5002(Lcom/google/android/gms/icing/Proto$CorpusConfig;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->queryLimit_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->OTHER:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->type_:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->access$3300()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContentProviderUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->contentProviderUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->id_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->packageName_:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryLimit()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->queryLimit_:I

    return v0
.end method

.method public getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method

.method public getSectionsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSectionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$SectionConfig;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getQueryLimit()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_9
    iput v2, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public getType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->type_:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-object v0
.end method

.method public getUniversalSectionMappings(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    return-object v0
.end method

.method public getUniversalSectionMappingsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getUniversalSectionMappingsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->version_:Ljava/lang/String;

    return-object v0
.end method

.method public hasContentProviderUri()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName:Z

    return v0
.end method

.method public hasPackageName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName:Z

    return v0
.end method

.method public hasQueryLimit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType:Z

    return v0
.end method

.method public hasVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getQueryLimit()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_8
    return-void
.end method
