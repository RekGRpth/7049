.class public final Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$CorpusData;",
        "Lcom/google/android/gms/icing/Proto$CorpusData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$CorpusData;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$6900()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusData;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$CorpusData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$CorpusData;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    return-object v0
.end method

.method public hasConfig()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig()Z

    move-result v0

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7200(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7200(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7202(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7102(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7202(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusData;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->hasConfig()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->getConfig()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->getStatus()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7400(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7400(Lcom/google/android/gms/icing/Proto$CorpusData;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilder(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7402(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7302(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7402(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    goto :goto_0
.end method

.method public setConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7102(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7202(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object p0
.end method

.method public setConfig(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasConfig:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7102(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->config_:Lcom/google/android/gms/icing/Proto$CorpusConfig;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7202(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object p0
.end method

.method public setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7302(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7402(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object p0
.end method

.method public setStatus(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusData$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->hasStatus:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7302(Lcom/google/android/gms/icing/Proto$CorpusData;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusData$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusData;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusData;->status_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusData;->access$7402(Lcom/google/android/gms/icing/Proto$CorpusData;Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object p0
.end method
