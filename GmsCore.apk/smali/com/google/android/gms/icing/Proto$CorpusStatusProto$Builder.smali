.class public final Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$CorpusStatusProto;",
        "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$5900()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object v0
.end method


# virtual methods
.method public addCounter(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addCounter(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object v0
.end method

.method public clearCounter()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;

    return-object p0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCounterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastSeqno()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v0

    return-wide v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastFlushedSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    :cond_3
    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setLastFlushedSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->addCounter(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->valueOf(I)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->setState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setLastFlushedSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6402(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastFlushedSeqno_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6502(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;J)J

    return-object p0
.end method

.method public setLastSeqno(J)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6202(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastSeqno_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6302(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;J)J

    return-object p0
.end method

.method public setState(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6602(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->state_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->access$6702(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-object p0
.end method
