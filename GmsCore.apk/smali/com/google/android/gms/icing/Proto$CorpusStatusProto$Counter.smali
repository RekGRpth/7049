.class public final Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Counter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;


# instance fields
.field private count_:I

.field private hasCount:Z

.field private hasName:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->name_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->count_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->name_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->count_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$5402(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasName:Z

    return p1
.end method

.method static synthetic access$5502(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5602(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasCount:Z

    return p1
.end method

.method static synthetic access$5702(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->count_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->access$5200()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->count_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasCount()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getCount()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasCount:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->hasCount()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    return-void
.end method
