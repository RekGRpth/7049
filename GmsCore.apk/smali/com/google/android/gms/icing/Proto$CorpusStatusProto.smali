.class public final Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusStatusProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;,
        Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;,
        Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;


# instance fields
.field private counter_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;",
            ">;"
        }
    .end annotation
.end field

.field private hasLastFlushedSeqno:Z

.field private hasLastSeqno:Z

.field private hasState:Z

.field private lastFlushedSeqno_:J

.field private lastSeqno_:J

.field private memoizedSerializedSize:I

.field private state_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastSeqno_:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastFlushedSeqno_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastSeqno_:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastFlushedSeqno_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$6100(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6102(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$6202(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno:Z

    return p1
.end method

.method static synthetic access$6302(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastSeqno_:J

    return-wide p1
.end method

.method static synthetic access$6402(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno:Z

    return p1
.end method

.method static synthetic access$6502(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastFlushedSeqno_:J

    return-wide p1
.end method

.method static synthetic access$6602(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState:Z

    return p1
.end method

.method static synthetic access$6702(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->state_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->defaultInstance:Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->state_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->access$5900()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCounterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->counter_:Ljava/util/List;

    return-object v0
.end method

.method public getLastFlushedSeqno()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastFlushedSeqno_:J

    return-wide v0
.end method

.method public getLastSeqno()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->lastSeqno_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v2, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getCounterList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    iput v2, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->state_:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-object v0
.end method

.method public hasLastFlushedSeqno()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno:Z

    return v0
.end method

.method public hasLastSeqno()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno:Z

    return v0
.end method

.method public hasState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilderForType()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->newBuilder(Lcom/google/android/gms/icing/Proto$CorpusStatusProto;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastSeqno()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastSeqno()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasLastFlushedSeqno()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getLastFlushedSeqno()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getCounterList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->hasState()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto;->getState()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    return-void
.end method
