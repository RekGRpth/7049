.class public final Lcom/google/android/gms/icing/Proto$FlushStatus;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FlushStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$FlushStatus;


# instance fields
.field private docstoreSize_:J

.field private hasDocstoreSize:Z

.field private hasNumDocuments:Z

.field private hasNumUris:Z

.field private hasTimestampSecs:Z

.field private memoizedSerializedSize:I

.field private numDocuments_:I

.field private numUris_:I

.field private timestampSecs_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$FlushStatus;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$FlushStatus;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$FlushStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$FlushStatus;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$FlushStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$FlushStatus;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numDocuments_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->docstoreSize_:J

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numUris_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->timestampSecs_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numDocuments_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->docstoreSize_:J

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numUris_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->timestampSecs_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$7802(Lcom/google/android/gms/icing/Proto$FlushStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumDocuments:Z

    return p1
.end method

.method static synthetic access$7902(Lcom/google/android/gms/icing/Proto$FlushStatus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numDocuments_:I

    return p1
.end method

.method static synthetic access$8002(Lcom/google/android/gms/icing/Proto$FlushStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasDocstoreSize:Z

    return p1
.end method

.method static synthetic access$8102(Lcom/google/android/gms/icing/Proto$FlushStatus;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->docstoreSize_:J

    return-wide p1
.end method

.method static synthetic access$8202(Lcom/google/android/gms/icing/Proto$FlushStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumUris:Z

    return p1
.end method

.method static synthetic access$8302(Lcom/google/android/gms/icing/Proto$FlushStatus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numUris_:I

    return p1
.end method

.method static synthetic access$8402(Lcom/google/android/gms/icing/Proto$FlushStatus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasTimestampSecs:Z

    return p1
.end method

.method static synthetic access$8502(Lcom/google/android/gms/icing/Proto$FlushStatus;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->timestampSecs_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$FlushStatus;->defaultInstance:Lcom/google/android/gms/icing/Proto$FlushStatus;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->create()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->access$7600()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$FlushStatus;)Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$FlushStatus;->newBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$FlushStatus;)Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/icing/Proto$FlushStatus;->newBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    # invokes: Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->buildParsed()Lcom/google/android/gms/icing/Proto$FlushStatus;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;->access$7500(Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDocstoreSize()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->docstoreSize_:J

    return-wide v0
.end method

.method public getNumDocuments()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numDocuments_:I

    return v0
.end method

.method public getNumUris()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->numUris_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumDocuments()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumDocuments()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasDocstoreSize()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getDocstoreSize()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumUris()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumUris()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasTimestampSecs()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getTimestampSecs()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getTimestampSecs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->timestampSecs_:J

    return-wide v0
.end method

.method public hasDocstoreSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasDocstoreSize:Z

    return v0
.end method

.method public hasNumDocuments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumDocuments:Z

    return v0
.end method

.method public hasNumUris()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumUris:Z

    return v0
.end method

.method public hasTimestampSecs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasTimestampSecs:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$FlushStatus;->newBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->newBuilderForType()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->newBuilder(Lcom/google/android/gms/icing/Proto$FlushStatus;)Lcom/google/android/gms/icing/Proto$FlushStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumDocuments()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumDocuments()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasDocstoreSize()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getDocstoreSize()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasNumUris()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getNumUris()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->hasTimestampSecs()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$FlushStatus;->getTimestampSecs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_3
    return-void
.end method
