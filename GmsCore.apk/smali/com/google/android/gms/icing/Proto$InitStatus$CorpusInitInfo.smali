.class public final Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$InitStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusInitInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;


# instance fields
.field private corpusId_:I

.field private hasCorpusId:Z

.field private hasLastSeqno:Z

.field private lastSeqno_:J

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->defaultInstance:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->defaultInstance:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->corpusId_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->lastSeqno_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->corpusId_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->lastSeqno_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$9602(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId:Z

    return p1
.end method

.method static synthetic access$9702(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->corpusId_:I

    return p1
.end method

.method static synthetic access$9802(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno:Z

    return p1
.end method

.method static synthetic access$9902(Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->lastSeqno_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->defaultInstance:Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->create()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;->access$9400()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCorpusId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->corpusId_:I

    return v0
.end method

.method public getLastSeqno()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->lastSeqno_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getCorpusId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getLastSeqno()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasCorpusId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId:Z

    return v0
.end method

.method public hasLastSeqno()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->newBuilder()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->newBuilderForType()Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasCorpusId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getCorpusId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->hasLastSeqno()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$InitStatus$CorpusInitInfo;->getLastSeqno()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    return-void
.end method
