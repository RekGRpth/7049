.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$14200()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    return-object v0
.end method


# virtual methods
.method public addAllTag(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addAllUniversalSectionMappings(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;)",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addSectionSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSectionSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTag(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUniversalSectionMappings(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getCorpusId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    :cond_2
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addSectionSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addTag(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addUniversalSectionMappings(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->corpusId_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->access$14802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;I)I

    return-object p0
.end method
