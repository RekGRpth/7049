.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;


# instance fields
.field private corpusId_:I

.field private hasCorpusId:Z

.field private memoizedSerializedSize:I

.field private sectionSpec_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;",
            ">;"
        }
    .end annotation
.end field

.field private tag_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private universalSectionMappings_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->corpusId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->corpusId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$14400(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14500(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14600(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId:Z

    return p1
.end method

.method static synthetic access$14802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->corpusId_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->access$14200()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCorpusId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->corpusId_:I

    return v0
.end method

.method public getSectionSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    return-object v0
.end method

.method public getSectionSpecCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSectionSpecList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->sectionSpec_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v3, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getCorpusId()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpecList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_2

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v5, 0x4

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_3

    :cond_4
    iput v3, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public getTag(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTagCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTagList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->tag_:Ljava/util/List;

    return-object v0
.end method

.method public getUniversalSectionMappingsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getUniversalSectionMappingsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->universalSectionMappings_:Ljava/util/List;

    return-object v0
.end method

.method public hasCorpusId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->hasCorpusId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getCorpusId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getSectionSpecList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getTagList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_3
    return-void
.end method
