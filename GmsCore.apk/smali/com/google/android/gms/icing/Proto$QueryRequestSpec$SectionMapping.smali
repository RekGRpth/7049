.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionMapping"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;


# instance fields
.field private hasId:Z

.field private hasName:Z

.field private id_:I

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->name_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->id_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->name_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->id_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$15202(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasName:Z

    return p1
.end method

.method static synthetic access$15302(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$15402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasId:Z

    return p1
.end method

.method static synthetic access$15502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->id_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->access$15000()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->id_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->getId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasId:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->hasId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    return-void
.end method
