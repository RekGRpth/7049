.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;


# instance fields
.field private hasSectionId:Z

.field private hasSnippet:Z

.field private hasSnippetLength:Z

.field private hasSnippetStripHtml:Z

.field private memoizedSerializedSize:I

.field private sectionId_:I

.field private snippetLength_:I

.field private snippetStripHtml_:Z

.field private snippet_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->sectionId_:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippet_:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetStripHtml_:Z

    iput v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetLength_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->sectionId_:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippet_:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetStripHtml_:Z

    iput v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetLength_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$13302(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId:Z

    return p1
.end method

.method static synthetic access$13402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->sectionId_:I

    return p1
.end method

.method static synthetic access$13502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet:Z

    return p1
.end method

.method static synthetic access$13602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippet_:Z

    return p1
.end method

.method static synthetic access$13702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml:Z

    return p1
.end method

.method static synthetic access$13802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetStripHtml_:Z

    return p1
.end method

.method static synthetic access$13902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength:Z

    return p1
.end method

.method static synthetic access$14002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetLength_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;->access$13100()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSectionId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->sectionId_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSectionId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippet()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetStripHtml()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetLength()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getSnippet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippet_:Z

    return v0
.end method

.method public getSnippetLength()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetLength_:I

    return v0
.end method

.method public getSnippetStripHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->snippetStripHtml_:Z

    return v0
.end method

.method public hasSectionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId:Z

    return v0
.end method

.method public hasSnippet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet:Z

    return v0
.end method

.method public hasSnippetLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength:Z

    return v0
.end method

.method public hasSnippetStripHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSectionId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSectionId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippet()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippet()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetStripHtml()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetStripHtml()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->hasSnippetLength()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;->getSnippetLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_3
    return-void
.end method
