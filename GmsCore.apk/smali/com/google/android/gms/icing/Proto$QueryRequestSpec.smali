.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QueryRequestSpec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;,
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;,
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;,
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;,
        Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionSpec;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;


# instance fields
.field private corpusSpec_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;",
            ">;"
        }
    .end annotation
.end field

.field private hasNoCorpusFilter:Z

.field private hasUniversalSearch:Z

.field private hasVerboseScoring:Z

.field private hasWantUris:Z

.field private memoizedSerializedSize:I

.field private noCorpusFilter_:Z

.field private sectionMapping_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;",
            ">;"
        }
    .end annotation
.end field

.field private sectionWeight_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;",
            ">;"
        }
    .end annotation
.end field

.field private universalSearch_:Z

.field private verboseScoring_:Z

.field private wantUris_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->noCorpusFilter_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->wantUris_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->verboseScoring_:Z

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->universalSearch_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->noCorpusFilter_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->wantUris_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->verboseScoring_:Z

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->universalSearch_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$16802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$16902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$17002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$17102(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter:Z

    return p1
.end method

.method static synthetic access$17202(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->noCorpusFilter_:Z

    return p1
.end method

.method static synthetic access$17302(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris:Z

    return p1
.end method

.method static synthetic access$17402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->wantUris_:Z

    return p1
.end method

.method static synthetic access$17502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring:Z

    return p1
.end method

.method static synthetic access$17602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->verboseScoring_:Z

    return p1
.end method

.method static synthetic access$17702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch:Z

    return p1
.end method

.method static synthetic access$17802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->universalSearch_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->defaultInstance:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->access$16600()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCorpusSpec(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    return-object v0
.end method

.method public getCorpusSpecCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCorpusSpecList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;

    return-object v0
.end method

.method public getNoCorpusFilter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->noCorpusFilter_:Z

    return v0
.end method

.method public getSectionMappingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;

    return-object v0
.end method

.method public getSectionWeightList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getNoCorpusFilter()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getSectionMappingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getWantUris()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getSectionWeightList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;

    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getVerboseScoring()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getUniversalSearch()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_7
    iput v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public getUniversalSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->universalSearch_:Z

    return v0
.end method

.method public getVerboseScoring()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->verboseScoring_:Z

    return v0
.end method

.method public getWantUris()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->wantUris_:Z

    return v0
.end method

.method public hasNoCorpusFilter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter:Z

    return v0
.end method

.method public hasUniversalSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch:Z

    return v0
.end method

.method public hasVerboseScoring()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring:Z

    return v0
.end method

.method public hasWantUris()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->newBuilderForType()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getCorpusSpecList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getNoCorpusFilter()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getSectionMappingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getWantUris()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getSectionWeightList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getVerboseScoring()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getUniversalSearch()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_6
    return-void
.end method
