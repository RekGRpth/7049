.class public final Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Argument"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;


# instance fields
.field private hasLength:Z

.field private hasSectionId:Z

.field private hasSnippeted:Z

.field private length_:I

.field private memoizedSerializedSize:I

.field private sectionId_:I

.field private snippeted_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->sectionId_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->length_:I

    iput-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->snippeted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->sectionId_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->length_:I

    iput-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->snippeted_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1402(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSectionId:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->sectionId_:I

    return p1
.end method

.method static synthetic access$1602(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasLength:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->length_:I

    return p1
.end method

.method static synthetic access$1802(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSnippeted:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->snippeted_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;->create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;->access$1200()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->length_:I

    return v0
.end method

.method public getSectionId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->sectionId_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSectionId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getSectionId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasLength()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getLength()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSnippeted()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getSnippeted()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getSnippeted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->snippeted_:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasLength:Z

    return v0
.end method

.method public hasSectionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSectionId:Z

    return v0
.end method

.method public hasSnippeted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSnippeted:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSectionId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getSectionId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->hasSnippeted()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template$Argument;->getSnippeted()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    return-void
.end method
