.class public final Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UniversalSectionMapping"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;,
        Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;


# instance fields
.field private hasUniversalSectionId:Z

.field private hasValueTemplate:Z

.field private memoizedSerializedSize:I

.field private universalSectionId_:I

.field private valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->universalSectionId_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->universalSectionId_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2802(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->universalSectionId_:I

    return p1
.end method

.method static synthetic access$3002(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->defaultInstance:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->create()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->access$2600()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getUniversalSectionId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->universalSectionId_:I

    return v0
.end method

.method public getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->valueTemplate_:Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    return-object v0
.end method

.method public hasUniversalSectionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId:Z

    return v0
.end method

.method public hasValueTemplate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->newBuilderForType()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasUniversalSectionId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->hasValueTemplate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getValueTemplate()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    return-void
.end method
