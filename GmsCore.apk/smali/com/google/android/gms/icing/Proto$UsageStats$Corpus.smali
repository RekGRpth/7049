.class public final Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$UsageStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Corpus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;


# instance fields
.field private deletedDocumentsSize_:J

.field private deletedDocuments_:I

.field private documentsSize_:J

.field private documents_:I

.field private hasDeletedDocuments:Z

.field private hasDeletedDocumentsSize:Z

.field private hasDocuments:Z

.field private hasDocumentsSize:Z

.field private hasId:Z

.field private id_:I

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->id_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documents_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocuments_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documentsSize_:J

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocumentsSize_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->id_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documents_:I

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocuments_:I

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documentsSize_:J

    iput-wide v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocumentsSize_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$21202(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId:Z

    return p1
.end method

.method static synthetic access$21302(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->id_:I

    return p1
.end method

.method static synthetic access$21402(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments:Z

    return p1
.end method

.method static synthetic access$21502(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documents_:I

    return p1
.end method

.method static synthetic access$21602(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments:Z

    return p1
.end method

.method static synthetic access$21702(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocuments_:I

    return p1
.end method

.method static synthetic access$21802(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize:Z

    return p1
.end method

.method static synthetic access$21902(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documentsSize_:J

    return-wide p1
.end method

.method static synthetic access$22002(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize:Z

    return p1
.end method

.method static synthetic access$22102(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocumentsSize_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->defaultInstance:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->create()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->access$21000()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDeletedDocuments()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocuments_:I

    return v0
.end method

.method public getDeletedDocumentsSize()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocumentsSize_:J

    return-wide v0
.end method

.method public getDocuments()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documents_:I

    return v0
.end method

.method public getDocumentsSize()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documentsSize_:J

    return-wide v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->id_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocuments()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocuments()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocumentsSize()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocumentsSize()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iput v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasDeletedDocuments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments:Z

    return v0
.end method

.method public hasDeletedDocumentsSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize:Z

    return v0
.end method

.method public hasDocuments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments:Z

    return v0
.end method

.method public hasDocumentsSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->newBuilder()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->newBuilderForType()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocuments()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocuments()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocumentsSize()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocumentsSize()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_4
    return-void
.end method
