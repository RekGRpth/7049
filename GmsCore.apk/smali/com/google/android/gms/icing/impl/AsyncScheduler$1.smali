.class Lcom/google/android/gms/icing/impl/AsyncScheduler$1;
.super Ljava/lang/Thread;
.source "AsyncScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/AsyncScheduler;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/AsyncScheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    # setter for: Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->access$002(Lcom/google/android/gms/icing/impl/AsyncScheduler;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    # getter for: Lcom/google/android/gms/icing/impl/AsyncScheduler;->mStarted:Landroid/os/ConditionVariable;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->access$100(Lcom/google/android/gms/icing/impl/AsyncScheduler;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    # getter for: Lcom/google/android/gms/icing/impl/AsyncScheduler;->mStarted:Landroid/os/ConditionVariable;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->access$100(Lcom/google/android/gms/icing/impl/AsyncScheduler;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->access$002(Lcom/google/android/gms/icing/impl/AsyncScheduler;Landroid/os/Handler;)Landroid/os/Handler;

    return-void
.end method
