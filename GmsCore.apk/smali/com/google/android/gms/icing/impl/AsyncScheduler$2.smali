.class Lcom/google/android/gms/icing/impl/AsyncScheduler$2;
.super Ljava/lang/Object;
.source "AsyncScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/AsyncScheduler;->quit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/AsyncScheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler$2;->this$0:Lcom/google/android/gms/icing/impl/AsyncScheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    const-string v0, "Quit done"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    return-void
.end method
