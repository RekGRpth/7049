.class public final Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;
.super Ljava/lang/Object;
.source "CorpusMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/CorpusMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionInfo"
.end annotation


# instance fields
.field public final mId:I

.field public final mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;


# direct methods
.method constructor <init>(ILcom/google/android/gms/icing/Proto$SectionConfig;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mSectionConfig:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    check-cast p1, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;

    iget v3, p1, Lcom/google/android/gms/icing/impl/CorpusMap$SectionInfo;->mId:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
