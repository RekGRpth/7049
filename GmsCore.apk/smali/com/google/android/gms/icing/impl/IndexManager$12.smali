.class Lcom/google/android/gms/icing/impl/IndexManager$12;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->initIndexInternal(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$12;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$12;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->restoreIndex()Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$12;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mCorpusMap:Lcom/google/android/gms/icing/impl/CorpusMap;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1700(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/CorpusMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/CorpusMap;->setFlushStatus(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z

    :cond_0
    return-void
.end method
