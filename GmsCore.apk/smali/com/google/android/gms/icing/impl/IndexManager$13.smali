.class Lcom/google/android/gms/icing/impl/IndexManager$13;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProvider(Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;

.field final synthetic val$corpusKey:Ljava/lang/String;

.field final synthetic val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

.field final synthetic val$notificationInfo:Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$notificationInfo:Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProviderWithLimit(Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;)I
    invoke-static {v2, v3, v4}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1800(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/ContentCursors;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v3

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/ContentCursors;->close()V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$notificationInfo:Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->displayNotification(Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2100(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V

    :cond_0
    return-void

    :pswitch_0
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$notificationInfo:Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProvider(Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1900(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;Lcom/google/android/gms/icing/impl/ContentCursors;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const-string v2, "Aborting indexing of corpus %s"

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v3

    monitor-enter v3

    :try_start_3
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$corpusKey:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$cursors:Lcom/google/android/gms/icing/impl/ContentCursors;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/ContentCursors;->close()V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$13;->val$notificationInfo:Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->displayNotification(Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V
    invoke-static {v3, v4}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2100(Lcom/google/android/gms/icing/impl/IndexManager;Lcom/google/android/gms/icing/impl/IndexManager$NotificationInfo;)V

    :cond_1
    throw v2

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :catchall_2
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
