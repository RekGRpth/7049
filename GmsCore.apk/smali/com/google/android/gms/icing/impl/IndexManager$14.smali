.class Lcom/google/android/gms/icing/impl/IndexManager$14;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->scheduleIndexing(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 10

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v8

    monitor-enter v8

    const/4 v3, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    move-object v4, v3

    move-object v6, v5

    :goto_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_WAITING:Ljava/lang/Integer;
    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2200()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_RUNNING:Ljava/lang/Integer;
    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2300()Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v6, :cond_7

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v3, v4

    :goto_2
    move-object v4, v3

    move-object v6, v5

    goto :goto_0

    :cond_0
    :try_start_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->INDEXING_STATE_CANCELED:Ljava/lang/Integer;
    invoke-static {}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2400()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    if-nez v4, :cond_5

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_3
    :try_start_4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-object v5, v6

    goto :goto_2

    :cond_1
    if-eqz v4, :cond_3

    :try_start_5
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v7

    move-object v3, v4

    move-object v5, v6

    :goto_5
    :try_start_6
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v7

    :cond_2
    :try_start_7
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndexingState:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2000(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v6, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/IndexManager$14;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->indexContentProviders(Ljava/util/List;)V
    invoke-static {v7, v6}, Lcom/google/android/gms/icing/impl/IndexManager;->access$2500(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/util/List;)V

    :cond_4
    return-void

    :catchall_1
    move-exception v7

    goto :goto_5

    :catchall_2
    move-exception v7

    move-object v3, v4

    goto :goto_5

    :catchall_3
    move-exception v7

    move-object v5, v6

    goto :goto_5

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    move-object v3, v4

    move-object v5, v6

    goto :goto_2

    :cond_7
    move-object v5, v6

    goto :goto_1
.end method
