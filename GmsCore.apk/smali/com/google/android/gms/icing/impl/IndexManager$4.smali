.class Lcom/google/android/gms/icing/impl/IndexManager$4;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->registerCorpusInfo(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;

.field final synthetic val$configBuilder:Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

.field final synthetic val$key:Ljava/lang/String;

.field final synthetic val$needUnregister:Z

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iput-boolean p2, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$needUnregister:Z

    iput-object p3, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$key:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$packageName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$configBuilder:Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-boolean v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$needUnregister:Z

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$key:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$configBuilder:Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->registerCorpusInternal(ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/impl/IndexManager;->access$700(Lcom/google/android/gms/icing/impl/IndexManager;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$key:Ljava/lang/String;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->scheduleIndexing(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$800(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "Corpus registration for pkg %s name %s failed: too many corpora"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$4;->val$configBuilder:Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0
.end method
