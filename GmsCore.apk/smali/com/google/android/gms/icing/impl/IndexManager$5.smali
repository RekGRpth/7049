.class Lcom/google/android/gms/icing/impl/IndexManager$5;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->handlePackageOrDataRemovedIntent(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->val$packageName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Missed notification for %s: init failed"

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->val$intent:Landroid/content/Intent;

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->val$packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->unregisterAppInternal(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/impl/IndexManager;->access$900(Lcom/google/android/gms/icing/impl/IndexManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mIndex:Lcom/google/android/gms/icing/impl/NativeIndex;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1000(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->getUsageStats()Lcom/google/android/gms/icing/Proto$UsageStats;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mStorageMonitor:Lcom/google/android/gms/icing/impl/StorageMonitor;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1100(Lcom/google/android/gms/icing/impl/IndexManager;)Lcom/google/android/gms/icing/impl/StorageMonitor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->runCompactionEarly(Lcom/google/android/gms/icing/Proto$UsageStats;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$5;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->compactInternal()V
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$1200(Lcom/google/android/gms/icing/impl/IndexManager;)V

    goto :goto_0
.end method
