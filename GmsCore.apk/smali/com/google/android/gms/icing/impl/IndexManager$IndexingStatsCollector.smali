.class Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;
.super Ljava/lang/Object;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/IndexManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IndexingStatsCollector"
.end annotation


# instance fields
.field private mStatsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    return-void
.end method


# virtual methods
.method flushCounters()V
    .locals 7

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->getCounterList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->getCount()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->clearCounter()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatusBuilder:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter;->newBuilder()Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    const/4 v6, 0x0

    aget v3, v3, v6

    invoke-virtual {v5, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;->setCount(I)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;->addCounter(Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Counter$Builder;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$Builder;

    goto :goto_1
.end method

.method incrementCounter(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->incrementCounter(Ljava/lang/String;I)V

    return-void
.end method

.method incrementCounter(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput p2, v1, v2

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$IndexingStatsCollector;->mStatsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v1, v0, v2

    add-int/2addr v1, p2

    aput v1, v0, v2

    goto :goto_0
.end method
