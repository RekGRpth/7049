.class Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;
.super Ljava/lang/Object;
.source "IndexService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/IndexService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BatteryStatus"
.end annotation


# instance fields
.field public final isLow:Z

.field public final isPowerConnected:Z


# direct methods
.method constructor <init>(Landroid/app/Service;)V
    .locals 11
    .param p1    # Landroid/app/Service;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v10, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v5, 0x0

    new-instance v8, Landroid/content/IntentFilter;

    const-string v9, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v8, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v5, v8}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    const-string v5, "level"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v5, "scale"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    int-to-float v5, v2

    int-to-float v8, v3

    div-float v0, v5, v8

    const v5, 0x3e99999a

    cmpg-float v5, v0, v5

    if-gez v5, :cond_2

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;->isLow:Z

    const-string v5, "status"

    invoke-virtual {v1, v5, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    :cond_0
    move v7, v6

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;->isPowerConnected:Z

    return-void

    :cond_2
    move v5, v7

    goto :goto_0
.end method
