.class public Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;
.super Lcom/google/android/gms/icing/impl/KeepAwake;
.source "IndexService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/IndexService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ServiceKeepAwake"
.end annotation


# instance fields
.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexService;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/icing/impl/IndexService;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;-><init>()V

    const-string v1, "power"

    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/IndexService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "Icing"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method


# virtual methods
.method public declared-synchronized acquire()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->isDead()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    const-class v3, Lcom/google/android/gms/icing/impl/IndexService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/IndexService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->acquireNoStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized acquireNoStart()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-super {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDead()V
    .locals 2

    const-string v0, "%s: On dead called"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexService;->stopSelf()V

    return-void
.end method

.method public declared-synchronized release()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;->release()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
