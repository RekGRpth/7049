.class public Lcom/google/android/gms/icing/impl/IndexService;
.super Landroid/app/Service;
.source "IndexService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;,
        Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;
    }
.end annotation


# static fields
.field private static final sIndexManagerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/icing/impl/IndexManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mHandler:Landroid/os/Handler;

.field protected mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

.field private mMaintenanceFrequencyDays:J

.field private mMaintenanceHourOfDay:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/IndexService;->sIndexManagerMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gms/icing/impl/IndexService;J)J
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexService;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceFrequencyDays:J

    return-wide p1
.end method

.method static synthetic access$102(Lcom/google/android/gms/icing/impl/IndexService;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/IndexService;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceHourOfDay:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/gms/icing/impl/IndexService;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/IndexService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexService;->makeMaintenanceIntent(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private enablePowerConnectedReceiver(Z)V
    .locals 4

    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/google/android/gms/icing/impl/PowerConnectedReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;
    .locals 9
    .param p1    # Z

    const/4 v0, 0x0

    sget-object v8, Lcom/google/android/gms/icing/impl/IndexService;->sIndexManagerMap:Ljava/util/Map;

    monitor-enter v8

    :try_start_0
    sget-object v1, Lcom/google/android/gms/icing/impl/IndexService;->sIndexManagerMap:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/icing/impl/IndexManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_0

    if-eqz p1, :cond_0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/icing/impl/IndexManager;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexService;->indexFileDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/icing/impl/IndexManager$Options;

    invoke-direct {v5}, Lcom/google/android/gms/icing/impl/IndexManager$Options;-><init>()V

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/IndexService;->mHandler:Landroid/os/Handler;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/IndexManager;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/KeepAwake;Ljava/io/File;Ljava/lang/String;Lcom/google/android/gms/icing/impl/IndexManager$Options;Landroid/os/Handler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string v1, "%s: Starting asynchronous initialization"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->init(Z)V

    sget-object v1, Lcom/google/android/gms/icing/impl/IndexService;->sIndexManagerMap:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :cond_0
    :try_start_3
    const-string v1, "%s: Re-using cached"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v7

    goto :goto_0

    :catchall_0
    move-exception v1

    :goto_1
    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    move-object v0, v7

    goto :goto_1
.end method

.method private indexFileDir()Ljava/io/File;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private makeMaintenanceIntent(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/icing/impl/IndexService;

    invoke-direct {v0, p1, v1, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private reschedulePowerConnectedMaintenance()V
    .locals 6

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/IndexService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    const-string v4, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-direct {p0, v4}, Lcom/google/android/gms/icing/impl/IndexService;->makeMaintenanceIntent(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/IndexService;->getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/IndexManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method protected getKeepAwake()Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;
    .locals 1

    new-instance v0, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;-><init>(Lcom/google/android/gms/icing/impl/IndexService;)V

    return-object v0
.end method

.method protected getMaintenanceIntervalMillis()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceFrequencyDays:J

    const-wide/16 v2, 0x18

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xe10

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method protected getMaintenanceTriggerMillis()J
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceHourOfDay:I

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const-string v1, "%s: First maintenance at %s"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method protected getName()Ljava/lang/String;
    .locals 1

    const-string v0, "main"

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "%s: Binding with intent %s"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/IndexService;->getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->getServiceBroker()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getKeepAwake()Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "%s: On destroy"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x1

    const-string v2, "%s: On start command"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->acquireNoStart()V

    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    new-instance v1, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;-><init>(Landroid/app/Service;)V

    iget-boolean v2, v1, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;->isLow:Z

    if-eqz v2, :cond_3

    iget-boolean v2, v1, Lcom/google/android/gms/icing/impl/IndexService$BatteryStatus;->isPowerConnected:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexService;->reschedulePowerConnectedMaintenance()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/icing/impl/IndexService$1;

    invoke-direct {v3, p0}, Lcom/google/android/gms/icing/impl/IndexService$1;-><init>(Lcom/google/android/gms/icing/impl/IndexService;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return v4

    :cond_2
    const/4 v2, 0x1

    :try_start_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/IndexService;->enablePowerConnectedReceiver(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/IndexService;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/icing/impl/IndexService$1;

    invoke-direct {v4, p0}, Lcom/google/android/gms/icing/impl/IndexService$1;-><init>(Lcom/google/android/gms/icing/impl/IndexService;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v2

    :cond_3
    const/4 v2, 0x1

    :try_start_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/IndexService;->getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexManager;->doMaintenance()V

    goto :goto_0

    :cond_4
    const-string v2, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/IndexService;->getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;

    goto :goto_0

    :cond_5
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/IndexService;->enablePowerConnectedReceiver(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/IndexService;->reschedulePowerConnectedMaintenance()V

    goto :goto_0

    :cond_6
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/IndexService;->getMyIndexManager(Z)Lcom/google/android/gms/icing/impl/IndexManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/icing/impl/IndexManager;->handlePackageOrDataRemovedIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->startMaintenanceAlarm()V

    goto :goto_0

    :cond_a
    const-string v2, "Received unexpected intent: %s"

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "%s: Unbind"

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/IndexService;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    return v0
.end method

.method protected startMaintenanceAlarm()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->acquire()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/icing/impl/IndexService$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/IndexService$2;-><init>(Lcom/google/android/gms/icing/impl/IndexService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    return-void
.end method
