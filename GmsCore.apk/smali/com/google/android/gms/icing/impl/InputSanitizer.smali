.class public Lcom/google/android/gms/icing/impl/InputSanitizer;
.super Ljava/lang/Object;
.source "InputSanitizer.java"


# static fields
.field public static final SECTION_NAME_RE:Ljava/util/regex/Pattern;

.field public static final VALID_TOKENIZERS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "[a-z0-9_]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/InputSanitizer;->SECTION_NAME_RE:Ljava/util/regex/Pattern;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "plain"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "html"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "rfc822"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/icing/impl/InputSanitizer;->VALID_TOKENIZERS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static validateGetCorpusNames(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    const-string v1, "Package name"

    const/16 v2, 0x3e8

    invoke-static {v1, p0, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static validateGetCorpusStatus(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    :try_start_0
    const-string v1, "Package name"

    const/16 v2, 0x3e8

    invoke-static {v1, p0, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "Corpus name"

    const/16 v2, 0x64

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static validateGetDocuments([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    .locals 7
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/appdatasearch/QuerySpecification;

    if-nez p3, :cond_0

    :try_start_0
    const-string v5, "No query spec defined"

    :goto_0
    return-object v5

    :cond_0
    const-string v5, "Package name"

    const/16 v6, 0x3e8

    invoke-static {v5, p1, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v5, "Corpus name"

    const/16 v6, 0x64

    invoke-static {v5, p2, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    const-string v5, "Uri"

    const/16 v6, 0x100

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static final validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static final validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too long (max "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private static final validateNonNullButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too long (max "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public static final validatePreRegister(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    :try_start_0
    const-string v5, "Package name"

    const/16 v6, 0x3e8

    invoke-static {v5, p0, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v5, "Corpus name"

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    const/16 v7, 0x64

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v5, "Version tag"

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->version:Ljava/lang/String;

    const/16 v7, 0x64

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonNullButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v5, "Content provider uri"

    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x800

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v5, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    if-nez v5, :cond_0

    const-string v5, "Section information is missing"

    :goto_0
    return-object v5

    :cond_0
    iget-object v5, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v5, v5

    const/16 v6, 0x10

    if-le v5, v6, :cond_1

    const-string v5, "Too many sections (max: 16)"

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v4, v0, v2

    if-nez v4, :cond_2

    const-string v5, "Null section info"

    goto :goto_0

    :cond_2
    iget-object v5, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateSectionName(Ljava/lang/String;)V

    iget-object v5, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->format:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateSectionTokenizer(Ljava/lang/String;)V

    iget v5, v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->weight:I

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateSectionWeight(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v5, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->universalSearchConfig:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->universalSearchConfig:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->getUniversalSearchMappings()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->validateUniversalSectionMappings([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static validateQuery(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/gms/appdatasearch/QuerySpecification;

    if-nez p5, :cond_0

    :try_start_0
    const-string v5, "No query spec defined"

    :goto_0
    return-object v5

    :cond_0
    const-string v5, "Query"

    const/16 v6, 0x3e8

    invoke-static {v5, p0, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    if-eqz p1, :cond_2

    const-string v5, "Package name"

    const/16 v6, 0x3e8

    invoke-static {v5, p1, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    if-eqz p2, :cond_3

    move-object v0, p2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    const-string v5, "Corpus name"

    const/16 v6, 0x64

    invoke-static {v5, v1, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    if-eqz p2, :cond_1

    const-string v5, "Corpora specified but not package name"

    goto :goto_0

    :cond_3
    if-ltz p3, :cond_4

    if-gtz p4, :cond_5

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad start and num results: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static validateRequestIndexing(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    :try_start_0
    const-string v1, "Package name"

    const/16 v2, 0x3e8

    invoke-static {v1, p0, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "Corpus name"

    const/16 v2, 0x64

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-gez v1, :cond_0

    const-string v1, "Negative sequence number"
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static final validateSectionName(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "Section name"

    const/16 v1, 0x40

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/icing/impl/InputSanitizer;->SECTION_NAME_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad section name: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static final validateSectionTokenizer(Ljava/lang/String;)V
    .locals 7
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/icing/impl/InputSanitizer;->VALID_TOKENIZERS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad section format: ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private static final validateSectionWeight(I)V
    .locals 3
    .param p0    # I

    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x3f

    if-le p0, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad section weight: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public static validateSuggest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I

    const/16 v7, 0x64

    :try_start_0
    const-string v5, "Query"

    const/16 v6, 0x3e8

    invoke-static {v5, p0, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonNullButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    if-eqz p1, :cond_1

    const-string v5, "Package name"

    const/16 v6, 0x3e8

    invoke-static {v5, p1, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    if-eqz p2, :cond_2

    move-object v0, p2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    const-string v5, "Corpus name"

    const/16 v6, 0x64

    invoke-static {v5, v1, v6}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_0

    const-string v5, "Corpora specified but not package name"

    :goto_1
    return-object v5

    :cond_2
    if-lez p3, :cond_3

    if-le p3, v7, :cond_4

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad num suggestions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static final validateTagsList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Too many tags requested"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "Tag"

    const/16 v3, 0x3e8

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThan(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static validateUnregister(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    :try_start_0
    const-string v1, "Package name"

    const/16 v2, 0x3e8

    invoke-static {v1, p0, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    const-string v1, "Corpus name"

    const/16 v2, 0x64

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/icing/impl/InputSanitizer;->validateNonEmptyButShorterThanInternal(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
