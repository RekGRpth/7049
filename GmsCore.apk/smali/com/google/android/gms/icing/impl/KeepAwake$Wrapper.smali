.class public abstract Lcom/google/android/gms/icing/impl/KeepAwake$Wrapper;
.super Lcom/google/android/gms/icing/impl/KeepAwake;
.source "KeepAwake.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/impl/KeepAwake;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Wrapper"
.end annotation


# instance fields
.field private mParent:Lcom/google/android/gms/icing/impl/KeepAwake;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/KeepAwake;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/impl/KeepAwake;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/KeepAwake$Wrapper;->mParent:Lcom/google/android/gms/icing/impl/KeepAwake;

    return-void
.end method


# virtual methods
.method public final acquire()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake$Wrapper;->mParent:Lcom/google/android/gms/icing/impl/KeepAwake;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/KeepAwake;->acquire()V

    invoke-super {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;->acquire()V

    return-void
.end method

.method public final release()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/icing/impl/KeepAwake;->release()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/KeepAwake$Wrapper;->mParent:Lcom/google/android/gms/icing/impl/KeepAwake;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/KeepAwake;->release()V

    return-void
.end method
