.class public Lcom/google/android/gms/icing/impl/NativeIndex;
.super Ljava/lang/Object;
.source "NativeIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/NativeIndex$1;,
        Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    }
.end annotation


# instance fields
.field private mNativePtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "AppDataSearch"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCreate([B)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    return-void
.end method

.method static synthetic access$000(J)I
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumResults(J)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(J)I
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseNumScored(J)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(J)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseCorpusIds(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(J)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriLengths(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(J)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseUriBuffer(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(JII)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentLengths(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(JII)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseSectionContentBuffer(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(JII)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0    # J
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDocHasTag(JII)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(J)V
    .locals 0
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeQueryResponseDestroy(J)V

    return-void
.end method

.method private fromUTF8([B)Ljava/lang/String;
    .locals 4
    .param p1    # [B

    const/4 v1, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "Can\'t convert byte array to String"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v1, ""

    goto :goto_0
.end method

.method private native nativeClear(J)Z
.end method

.method private native nativeCompact(JD[J)[B
.end method

.method private native nativeCreate([B)J
.end method

.method private native nativeDeleteCorpus(JI)[B
.end method

.method private native nativeDeleteDocument(JJI[B)I
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeExecuteQuery(J[B[BIII)J
.end method

.method private native nativeFlush(J)[B
.end method

.method private native nativeGetCompactStatus(J)[B
.end method

.method private native nativeGetDebugInfo(JI)[B
.end method

.method private native nativeGetDocuments(J[[B[B)J
.end method

.method private native nativeGetUsageStats(J)[B
.end method

.method private native nativeIndexDocument(JJ[B)I
.end method

.method private native nativeInit(J[B)[B
.end method

.method private native nativeInitFlushed(J)[B
.end method

.method private native nativeMinFreeFraction(J)D
.end method

.method private native nativeNumDocuments(J)I
.end method

.method private native nativeNumPostingLists(J)I
.end method

.method private native nativeOnSleep(J)V
.end method

.method private native nativePostFlush(J[B)Z
.end method

.method private static native nativeQueryResponseCorpusIds(J)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseDestroy(J)V
.end method

.method private static native nativeQueryResponseDocHasTag(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseNumResults(J)I
.end method

.method private static native nativeQueryResponseNumScored(J)I
.end method

.method private static native nativeQueryResponseSectionContentBuffer(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseSectionContentLengths(JII)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseUriBuffer(J)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeQueryResponseUriLengths(J)Ljava/nio/ByteBuffer;
.end method

.method private native nativeRestoreIndex(J)[B
.end method

.method private native nativeSetLogPriority(I)V
.end method

.method private native nativeSuggest(J[B[II)[[B
.end method

.method private native nativeTagDocument(JJI[B[BZ)I
.end method

.method private parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 4
    .param p1    # [B

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$FlushStatus;->parseFrom([B)Lcom/google/android/gms/icing/Proto$FlushStatus;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Failed parsing flush status"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static resultToUserString(Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    sget-object v0, Lcom/google/android/gms/icing/impl/NativeIndex$1;->$SwitchMap$com$google$android$gms$icing$Proto$Enums$NativeIndexResult:[I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error internal "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "ok"

    goto :goto_0

    :pswitch_1
    const-string v0, "ok trimmed"

    goto :goto_0

    :pswitch_2
    const-string v0, "ok duplicate uri replaced"

    goto :goto_0

    :pswitch_3
    const-string v0, "error uri not found"

    goto :goto_0

    :pswitch_4
    const-string v0, "error i/o"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private toUTF8(Ljava/lang/String;)[B
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "UTF-8 not supported"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public clear()Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClear(J)Z

    move-result v0

    return v0
.end method

.method public compact()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 7

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JD[J)[B

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    return-object v0
.end method

.method public compactAndPurge(D[J)Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 7
    .param p1    # D
    .param p3    # [J

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    move-object v0, p0

    move-wide v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JD[J)[B

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v0

    return-object v0
.end method

.method public deleteCorpus(I)Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 3
    .param p1    # I

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteCorpus(JI)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    return-object v1
.end method

.method public deleteDocument(JILjava/lang/String;)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 7
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v6

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteDocument(JJI[B)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->valueOf(I)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    return-object v0
.end method

.method public destroy()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDestroy(J)V

    :cond_0
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    return-void
.end method

.method public executeQuery(Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec;III)Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    new-instance v11, Landroid/util/TimingLogger;

    const-string v0, "Icing"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v0, v1}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->toByteArray()[B

    move-result-object v4

    move-object v0, p0

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeExecuteQuery(J[B[BIII)J

    move-result-wide v8

    const-string v0, "execute query"

    invoke-virtual {v11, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_0
    new-instance v10, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    invoke-direct {v10, p2, v8, v9}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;-><init>(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;J)V

    const-string v0, "decode response"

    invoke-virtual {v11, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-virtual {v11}, Landroid/util/TimingLogger;->dumpToLog()V

    goto :goto_0
.end method

.method public finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->destroy()V

    return-void
.end method

.method public flush()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 3

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeFlush(J)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    return-object v1
.end method

.method public getCompactStatus()Lcom/google/android/gms/icing/Proto$CompactStatus;
    .locals 4

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetCompactStatus(J)[B

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CompactStatus;->parseFrom([B)Lcom/google/android/gms/icing/Proto$CompactStatus;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v2, "Failed parsing compact status"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDebugInfo(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDebugInfo(JI)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->fromUTF8([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDocuments([Ljava/lang/String;Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;
    .locals 15
    .param p1    # [Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-object/from16 v0, p1

    array-length v12, v0

    new-array v11, v12, [[B

    const/4 v3, 0x0

    move-object/from16 v1, p1

    array-length v5, v1

    const/4 v2, 0x0

    move v4, v3

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v10, v1, v2

    add-int/lit8 v3, v4, 0x1

    invoke-direct {p0, v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v12

    aput-object v12, v11, v4

    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    iget-wide v12, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->toByteArray()[B

    move-result-object v14

    invoke-direct {p0, v12, v13, v11, v14}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDocuments(J[[B[B)J

    move-result-wide v6

    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-nez v12, :cond_1

    const/4 v9, 0x0

    :goto_1
    return-object v9

    :cond_1
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setWantUris(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    invoke-direct {v9, v8, v6, v7}, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;-><init>(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;J)V

    goto :goto_1
.end method

.method public getUsageStats()Lcom/google/android/gms/icing/Proto$UsageStats;
    .locals 5

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetUsageStats(J)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$UsageStats;->parseFrom([B)Lcom/google/android/gms/icing/Proto$UsageStats;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Failed parsing usage stats"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public indexDocument(JLcom/google/android/gms/icing/Proto$Document;)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 6
    .param p1    # J
    .param p3    # Lcom/google/android/gms/icing/Proto$Document;

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-virtual {p3}, Lcom/google/android/gms/icing/Proto$Document;->toByteArray()[B

    move-result-object v5

    move-object v0, p0

    move-wide v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeIndexDocument(JJ[B)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->valueOf(I)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/google/android/gms/icing/Proto$FlushStatus;)Lcom/google/android/gms/icing/Proto$InitStatus;
    .locals 6
    .param p1    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$FlushStatus;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeInit(J[B)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$InitStatus;->parseFrom([B)Lcom/google/android/gms/icing/Proto$InitStatus;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Failed parsing init status"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public initFlushed()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 3

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeInitFlushed(J)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    return-object v1
.end method

.method public minFreeFraction()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeMinFreeFraction(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public numDocuments()I
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumDocuments(J)I

    move-result v0

    return v0
.end method

.method public numPostingLists()I
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumPostingLists(J)I

    move-result v0

    return v0
.end method

.method public onSleep()V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeOnSleep(J)V

    return-void
.end method

.method public postFlush(Lcom/google/android/gms/icing/Proto$FlushStatus;)Z
    .locals 3
    .param p1    # Lcom/google/android/gms/icing/Proto$FlushStatus;

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$FlushStatus;->toByteArray()[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativePostFlush(J[B)Z

    move-result v0

    return v0
.end method

.method public querySuggestions(Ljava/lang/String;[II)[Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # [I
    .param p3    # I

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v3

    move-object v0, p0

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSuggest(J[B[II)[[B

    move-result-object v8

    array-length v0, v8

    new-array v7, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    :goto_0
    array-length v0, v8

    if-ge v6, v0, :cond_0

    aget-object v0, v8, v6

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->fromUTF8([B)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    return-object v7
.end method

.method public restoreIndex()Lcom/google/android/gms/icing/Proto$FlushStatus;
    .locals 3

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeRestoreIndex(J)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->parseFlushStatus([B)Lcom/google/android/gms/icing/Proto$FlushStatus;

    move-result-object v1

    return-object v1
.end method

.method public setLogPriority(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSetLogPriority(I)V

    return-void
.end method

.method public tagDocument(JILjava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 9
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->mNativePtr:J

    invoke-direct {p0, p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v6

    invoke-direct {p0, p5}, Lcom/google/android/gms/icing/impl/NativeIndex;->toUTF8(Ljava/lang/String;)[B

    move-result-object v7

    move-object v0, p0

    move-wide v3, p1

    move v5, p3

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeTagDocument(JJI[B[BZ)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->valueOf(I)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    move-result-object v0

    return-object v0
.end method
