.class public Lcom/google/android/gms/icing/impl/StorageMonitor;
.super Ljava/lang/Object;
.source "StorageMonitor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mIndexFileDir:Ljava/io/File;

.field private mLastDiskSpaceCheck:J

.field private mLastDiskSpaceCheckEnough:Z

.field private mLastFreeFrac:D

.field private final mStorageCapacity:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheck:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheckEnough:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastFreeFrac:D

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Index file directory must be set"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path could not be created"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path is not a directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot create directory %s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->storageCapacity(Ljava/io/File;)J

    move-result-wide v0

    invoke-static {v0, v1, p4, p5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mStorageCapacity:J

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mStorageCapacity:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const-string v0, "There is no storage capacity, icing will not index"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_4
    return-void
.end method

.method private calculateDiskSpaceEnough(JD)Z
    .locals 11
    .param p1    # J
    .param p3    # D

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "gms_icing_storage_threshold_bytes"

    const-wide/32 v9, 0x40000000

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v7, p1, v2

    if-lez v7, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "gms_icing_storage_threshold_percent"

    const-wide/16 v9, 0x1e

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    long-to-double v7, v4

    const-wide/high16 v9, 0x4059000000000000L

    div-double v0, v7, v9

    const-wide/high16 v7, 0x3ff0000000000000L

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    cmpl-double v7, p3, v0

    if-gtz v7, :cond_0

    const/4 v6, 0x0

    goto :goto_0
.end method

.method private calculateFiguresIfNeeded()V
    .locals 9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheck:J

    sub-long v5, v2, v5

    const-wide/16 v7, 0x2710

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheck:J

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheckEnough:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/StorageMonitor;->storageAvailable(Ljava/io/File;)J

    move-result-wide v0

    iget-wide v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mStorageCapacity:J

    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/StorageMonitor;->calculateFreeDiskFrac(J)D

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastFreeFrac:D

    iget-wide v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastFreeFrac:D

    invoke-direct {p0, v0, v1, v5, v6}, Lcom/google/android/gms/icing/impl/StorageMonitor;->calculateDiskSpaceEnough(JD)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheckEnough:Z

    goto :goto_0
.end method

.method private calculateFreeDiskFrac(J)D
    .locals 8
    .param p1    # J

    const-wide/16 v6, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mStorageCapacity:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    return-wide v4

    :cond_0
    move-wide v0, p1

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/StorageMonitor;->storageUsage(Ljava/io/File;)J

    move-result-wide v2

    const-wide/32 v4, 0x1312d00

    add-long/2addr v4, v2

    sub-long/2addr v0, v4

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-double v4, v0

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mStorageCapacity:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    goto :goto_0
.end method

.method public static purgeTarget(D)D
    .locals 10
    .param p0    # D

    const-wide/16 v8, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L

    invoke-static {p0, p1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide p0

    sub-double v0, v6, p0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v8

    if-lez v4, :cond_0

    const-wide v4, 0x3fe6666666666666L

    div-double/2addr v4, v0

    sub-double v2, v6, v4

    :cond_0
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    return-wide v4
.end method

.method private static storageAvailable(Ljava/io/File;)J
    .locals 3
    .param p0    # Ljava/io/File;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-long v1, v1

    goto :goto_0
.end method

.method private static storageCapacity(Ljava/io/File;)J
    .locals 3
    .param p0    # Ljava/io/File;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-long v1, v1

    goto :goto_0
.end method

.method private static storageUsage(Ljava/io/File;)J
    .locals 8
    .param p0    # Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/StorageMonitor;->storageUsage(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v4, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    :cond_1
    return-wide v4
.end method


# virtual methods
.method public clearStorage()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/Utils;->deleteRecursively(Ljava/io/File;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    return v0
.end method

.method public enoughDiskSpace()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->calculateFiguresIfNeeded()V

    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastDiskSpaceCheckEnough:Z

    return v0
.end method

.method public freeDiskFrac()D
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/StorageMonitor;->calculateFiguresIfNeeded()V

    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mLastFreeFrac:D

    return-wide v0
.end method

.method public getStorageDirectory()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mIndexFileDir:Ljava/io/File;

    return-object v0
.end method

.method public runCompactionEarly(Lcom/google/android/gms/icing/Proto$UsageStats;)Z
    .locals 17
    .param p1    # Lcom/google/android/gms/icing/Proto$UsageStats;

    const-wide/16 v11, 0x0

    const-wide/16 v9, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpusCount()I

    move-result v13

    if-ge v4, v13, :cond_0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/Proto$UsageStats;->getCorpus(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocumentsSize()J

    move-result-wide v13

    add-long/2addr v11, v13

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocumentsSize()J

    move-result-wide v13

    add-long/2addr v9, v13

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    add-long/2addr v11, v9

    const-wide/16 v13, 0x0

    cmp-long v13, v11, v13

    if-nez v13, :cond_1

    const/4 v13, 0x0

    :goto_1
    return v13

    :cond_1
    long-to-double v13, v9

    long-to-double v15, v11

    div-double v2, v13, v15

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/icing/impl/StorageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "gms_icing_storage_compact_threshold"

    const-wide/16 v15, 0xa

    invoke-static/range {v13 .. v16}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v7

    long-to-double v13, v7

    const-wide/high16 v15, 0x4059000000000000L

    div-double v5, v13, v15

    const-wide/high16 v13, 0x3ff0000000000000L

    invoke-static {v13, v14, v5, v6}, Ljava/lang/Math;->min(DD)D

    move-result-wide v5

    cmpl-double v13, v2, v5

    if-ltz v13, :cond_2

    const/4 v13, 0x1

    goto :goto_1

    :cond_2
    const/4 v13, 0x0

    goto :goto_1
.end method
