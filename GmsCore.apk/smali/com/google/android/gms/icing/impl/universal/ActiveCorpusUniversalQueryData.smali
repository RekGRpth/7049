.class Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;
.super Ljava/lang/Object;
.source "ActiveCorpusUniversalQueryData.java"


# instance fields
.field private final mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/Proto$CorpusConfig;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-void
.end method

.method private addSectionWeights(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;Ljava/util/List;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$SectionConfig;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setSectionId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->setWeight(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addSectionWeight(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->setCorpusId(I)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->addSectionWeights(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappingsList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->addAllUniversalSectionMappings(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    return-void
.end method

.method getCorpusId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v0

    return v0
.end method

.method getCorpusName()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSections(Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;I)Landroid/util/Pair;
    .locals 6
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x0

    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappingsCount()I

    move-result v5

    if-ge v2, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->mConfig:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getUniversalSectionMappings(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->getUniversalSectionId()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->getSectionName(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentLengths:[I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    iget-object v5, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;->mSections:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus$Section;->mContentBuffer:[B

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    return-object v5
.end method
