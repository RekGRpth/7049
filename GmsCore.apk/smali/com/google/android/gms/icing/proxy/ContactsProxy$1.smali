.class Lcom/google/android/gms/icing/proxy/ContactsProxy$1;
.super Ljava/lang/Object;
.source "ContactsProxy.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/proxy/ContactsProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-static {p2}, Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->access$002(Lcom/google/android/gms/icing/proxy/ContactsProxy;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    # invokes: Lcom/google/android/gms/icing/proxy/ContactsProxy;->registerCorpus()V
    invoke-static {v0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->access$100(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->access$002(Lcom/google/android/gms/icing/proxy/ContactsProxy;Z)Z

    return-void
.end method
