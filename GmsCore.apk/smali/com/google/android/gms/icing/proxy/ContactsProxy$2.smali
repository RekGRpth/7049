.class Lcom/google/android/gms/icing/proxy/ContactsProxy$2;
.super Ljava/lang/Object;
.source "ContactsProxy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/proxy/ContactsProxy;->registerCorpus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$2;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    const-string v4, "display_name"

    invoke-direct {v3, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    const-string v3, "contacts"

    # getter for: Lcom/google/android/gms/icing/proxy/ContactsProxy;->CONTACT_CP_AUTHORITY:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->access$200()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)V

    iget-object v3, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$2;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    iget-object v3, v3, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    iget-object v4, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy$2;->this$0:Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;->registerCorpusInfo(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "Contacts proxy registering corpus"

    invoke-static {v3, v1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method
