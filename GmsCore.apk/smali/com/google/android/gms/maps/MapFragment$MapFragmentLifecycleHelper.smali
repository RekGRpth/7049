.class Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;
.super Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;
.source "MapFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/maps/MapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MapFragmentLifecycleHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/dynamic/DeferredLifecycleHelper",
        "<",
        "Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field protected mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/dynamic/OnDelegateCreatedListener",
            "<",
            "Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final mFragment:Landroid/app/Fragment;


# direct methods
.method constructor <init>(Landroid/app/Fragment;)V
    .locals 0
    .param p1    # Landroid/app/Fragment;

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mFragment:Landroid/app/Fragment;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;Landroid/app/Activity;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->setActivity(Landroid/app/Activity;)V

    return-void
.end method

.method private setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->maybeCreateDelegate()V

    return-void
.end method


# virtual methods
.method protected createDelegate(Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/dynamic/OnDelegateCreatedListener",
            "<",
            "Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->maybeCreateDelegate()V

    return-void
.end method

.method public maybeCreateDelegate()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mActivity:Landroid/app/Activity;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->getDelegate()Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/gms/maps/internal/MapsDynamicJar;->getCreator(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/ICreator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/maps/internal/ICreator;->newMapFragmentDelegate(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    new-instance v3, Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;

    iget-object v4, p0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->mFragment:Landroid/app/Fragment;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;-><init>(Landroid/app/Fragment;Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;->onDelegateCreated(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v2, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v2

    :catch_1
    move-exception v2

    goto :goto_0
.end method
