.class public Lcom/google/android/gms/maps/internal/CreatorImpl;
.super Lcom/google/android/gms/maps/internal/ICreator$Stub;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/ICreator$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v0}, Lmaps/z/y;->a(Landroid/content/res/Resources;)V

    return-void
.end method

.method public newBitmapDescriptorFactoryDelegate()Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;
    .locals 1

    new-instance v0, Lmaps/z/bj;

    invoke-direct {v0}, Lmaps/z/bj;-><init>()V

    return-object v0
.end method

.method public newCameraUpdateFactoryDelegate()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;
    .locals 1

    new-instance v0, Lmaps/z/v;

    invoke-direct {v0}, Lmaps/z/v;-><init>()V

    return-object v0
.end method

.method public newMapFragmentDelegate(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lmaps/z/r;->a(Landroid/app/Activity;)Lmaps/z/r;

    move-result-object v0

    return-object v0
.end method

.method public newMapViewDelegate(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/IMapViewDelegate;
    .locals 2

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v1, Lmaps/z/bo;

    invoke-direct {v1, v0, p2}, Lmaps/z/bo;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    return-object v1
.end method
