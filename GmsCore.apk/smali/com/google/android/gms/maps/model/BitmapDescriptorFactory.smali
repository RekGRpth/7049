.class public final Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;
.super Ljava/lang/Object;
.source "BitmapDescriptorFactory.java"


# static fields
.field private static sDelegate:Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;)V
    .locals 1
    .param p0    # Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;

    sget-object v0, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->sDelegate:Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;

    sput-object v0, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->sDelegate:Lcom/google/android/gms/maps/model/internal/IBitmapDescriptorFactoryDelegate;

    goto :goto_0
.end method
