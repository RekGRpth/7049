.class public Lcom/google/android/gms/panorama/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ANDROID_BLUE:[F

.field public static final ANDROID_GRAY:[F

.field public static final BACKGROUND_BLACK:[F

.field public static final GRAY:[F

.field public static final GREEN:[F

.field public static final GROUND_PLANE_COLOR:[F

.field public static final TRANSPARENT_GRAY:[F

.field public static final TRANSPARENT_WHITE:[F

.field public static final WHITE:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->GRAY:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->TRANSPARENT_GRAY:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->GREEN:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->ANDROID_BLUE:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->ANDROID_GRAY:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->BACKGROUND_BLACK:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->WHITE:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->TRANSPARENT_WHITE:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/gms/panorama/Constants;->GROUND_PLANE_COLOR:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x3ed6d5d0
        0x3ed6d5d0
        0x3ed6d5d0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3ed6d5d0
        0x3ed6d5d0
        0x3ed6d5d0
        0x3f000000
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x3f800000
    .end array-data

    :array_3
    .array-data 4
        0x3e4ccccd
        0x3f35c28f
        0x3f65e354
        0x3f800000
    .end array-data

    :array_4
    .array-data 4
        0x3f4ccccd
        0x3f4ccccd
        0x3f4ccccd
        0x3f800000
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000
    .end array-data

    :array_6
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data

    :array_7
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f19999a
    .end array-data

    :array_8
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3dcccccd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
