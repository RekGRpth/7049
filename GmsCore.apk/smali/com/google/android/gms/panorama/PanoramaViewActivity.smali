.class public Lcom/google/android/gms/panorama/PanoramaViewActivity;
.super Landroid/app/Activity;
.source "PanoramaViewActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

.field private sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/panorama/PanoramaViewActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/panorama/PanoramaViewActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->loadAndShowUrl(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/PanoramaViewActivity;

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/panorama/PanoramaViewActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/panorama/PanoramaViewActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->showFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private fetchSharedPanoFile(Ljava/lang/String;Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getMediumSizePanoUrlFromLandingPage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->showLoadingErrorAndExit()V

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "temp_pano.jpg"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/panorama/PanoramaViewActivity$6;

    invoke-direct {v1, p0, v0, v2, p2}, Lcom/google/android/gms/panorama/PanoramaViewActivity$6;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;Ljava/lang/String;Ljava/io/File;Lcom/google/android/gms/panorama/util/Callback;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static getMediumSizePanoUrlFromLandingPage(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v9, "UTF-8"

    invoke-static {v2, v9}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/http/NameValuePair;

    invoke-interface {v6}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "pano"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v6}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v5

    :cond_1
    if-nez v5, :cond_2

    :goto_0
    return-object v4

    :catch_0
    move-exception v0

    sget-object v9, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    const/16 v9, 0x2f

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/s2048"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v9, "https://"

    const-string v10, "http://"

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    sget-object v9, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Medium Size URL: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSendImage(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    const-string v1, "Got filename from Send intent"

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method private getViewImage(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "URI is null!"

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "Got filename from View intent"

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private isImageUsablePanorama(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/panorama/util/PanoMetadata;->parse(Ljava/lang/String;)Lcom/google/android/gms/panorama/util/PanoMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAndShowUrl(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->showLoadingErrorAndExit()V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0b004e

    invoke-static {v1, p0}, Lcom/google/android/gms/panorama/util/Dialogs;->createProgressDialog(ILandroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    new-instance v1, Lcom/google/android/gms/panorama/PanoramaViewActivity$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity$3;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;Landroid/app/ProgressDialog;)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->fetchSharedPanoFile(Ljava/lang/String;Lcom/google/android/gms/panorama/util/Callback;)V

    goto :goto_0
.end method

.method private onImageLoadingError(I)V
    .locals 2
    .param p1    # I

    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/panorama/PanoramaViewActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity$5;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-static {v0, p1, p0, v1}, Lcom/google/android/gms/panorama/util/Dialogs;->showDialog(IILandroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method private showFile(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const v8, 0x7f0b004f

    sget-object v5, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Attempting to show panorama : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/gms/panorama/PanoramaViewActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity$4;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-static {p1}, Lcom/google/android/gms/panorama/util/PanoMetadata;->parse(Ljava/lang/String;)Lcom/google/android/gms/panorama/util/PanoMetadata;

    move-result-object v2

    if-nez v2, :cond_0

    const v5, 0x7f0b0050

    invoke-static {v8, v5, p0, v0}, Lcom/google/android/gms/panorama/util/Dialogs;->showDialog(IILandroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not load file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x7f0b0051

    invoke-static {v8, v5, p0, v0}, Lcom/google/android/gms/panorama/util/Dialogs;->showDialog(IILandroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    goto :goto_0

    :cond_1
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xa

    if-ge v5, v6, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    new-instance v3, Lcom/google/android/gms/panorama/viewer/LegacyTileProvider;

    invoke-direct {v3, v1}, Lcom/google/android/gms/panorama/viewer/LegacyTileProvider;-><init>(Ljava/io/File;)V

    :goto_2
    iget-object v5, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    new-instance v6, Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-direct {v6, v3, v2}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;-><init>(Lcom/google/android/gms/panorama/viewer/TileProvider;Lcom/google/android/gms/panorama/util/PanoMetadata;)V

    invoke-virtual {v5, v6}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setPanoramaImage(Lcom/google/android/gms/panorama/viewer/PanoramaImage;)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;

    invoke-direct {v3, v1}, Lcom/google/android/gms/panorama/viewer/TileProviderImpl;-><init>(Ljava/io/File;)V

    goto :goto_2
.end method

.method private showLoadingErrorAndExit()V
    .locals 3

    const/4 v0, -0x1

    const v1, 0x7f0b004c

    new-instance v2, Lcom/google/android/gms/panorama/PanoramaViewActivity$7;

    invoke-direct {v2, p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity$7;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-static {v0, v1, p0, v2}, Lcom/google/android/gms/panorama/util/Dialogs;->showDialog(IILandroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    return-void
.end method

.method private writeStreamToTempFile(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v5, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getCacheDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "temp_pano.jpg"

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0x1000

    new-array v0, v1, [B

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrote stream to temporary file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2
.end method


# virtual methods
.method public getPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->releaseWakeLock()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/sensor/SensorReader;->stop()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 17

    invoke-super/range {p0 .. p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/gms/panorama/util/UiUtil;->switchSystemUiToLightsOut(Landroid/view/Window;)V

    new-instance v14, Lcom/google/android/gms/panorama/viewer/PanoramaView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/gms/panorama/viewer/PanoramaView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    new-instance v14, Lcom/google/android/gms/panorama/sensor/SensorReader;

    invoke-direct {v14}, Lcom/google/android/gms/panorama/sensor/SensorReader;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->setContentView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v15

    invoke-interface {v15}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setSensorReader(Landroid/view/Display;Lcom/google/android/gms/panorama/sensor/SensorReader;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    if-nez v13, :cond_0

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "android.intent.extra.STREAM"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    :cond_0
    const-string v14, "filename"

    invoke-virtual {v8, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v13, :cond_7

    if-nez v7, :cond_8

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Intent : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    const-string v14, "android.intent.action.VIEW"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    if-eqz v12, :cond_2

    const-string v14, "image/"

    invoke-virtual {v12, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getViewImage(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v7

    :cond_1
    :goto_0
    if-nez v7, :cond_6

    const v14, 0x7f0b004c

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->onImageLoadingError(I)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {v8}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    const-string v14, "https://panoramas.googleplex.com/s/"

    invoke-virtual {v2, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    const v14, 0x7f0b004b

    move-object/from16 v0, p0

    invoke-static {v14, v0}, Lcom/google/android/gms/panorama/util/Dialogs;->createProgressDialog(ILandroid/content/Context;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    const-string v14, "https://panoramas.googleplex.com/s/"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v2, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "http://goo.gl/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v14, Lcom/google/android/gms/panorama/PanoramaViewActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3}, Lcom/google/android/gms/panorama/PanoramaViewActivity$1;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;Landroid/app/ProgressDialog;)V

    move-object/from16 v0, p0

    invoke-static {v0, v6, v14}, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->expandAsync(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/panorama/util/Callback;)V

    goto :goto_1

    :cond_3
    const-string v14, "https://panoramas.googleplex.com"

    invoke-virtual {v2, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->loadAndShowUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v14, "android.intent.action.SEND"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    if-eqz v12, :cond_5

    const-string v14, "image/"

    invoke-virtual {v12, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getSendImage(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_5
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v14, "filename"

    invoke-virtual {v5, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v14, "Got filename from intent extras."

    invoke-static {v14}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->isImageUsablePanorama(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_8

    const v14, 0x7f0b004d

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->onImageLoadingError(I)V

    goto/16 :goto_1

    :cond_7
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    invoke-virtual {v14, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->writeStreamToTempFile(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->sensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/gms/panorama/sensor/SensorReader;->start(Landroid/content/Context;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->releaseWakeLock()V

    const-string v14, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/PowerManager;

    const v14, 0x2000001a

    sget-object v15, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v10, v14, v15}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v14}, Landroid/os/PowerManager$WakeLock;->acquire()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->mainView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    new-instance v15, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity$2;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-virtual {v14, v15}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setAutoRotationCallback(Lcom/google/android/gms/panorama/util/Callback;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->showFile(Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v4

    sget-object v14, Lcom/google/android/gms/panorama/PanoramaViewActivity;->TAG:Ljava/lang/String;

    const-string v15, "Could not open file. "

    invoke-static {v14, v15, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1
.end method
