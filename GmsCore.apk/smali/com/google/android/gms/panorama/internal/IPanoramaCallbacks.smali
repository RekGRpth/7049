.class public interface abstract Lcom/google/android/gms/panorama/internal/IPanoramaCallbacks;
.super Ljava/lang/Object;
.source "IPanoramaCallbacks.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/panorama/internal/IPanoramaCallbacks$Stub;
    }
.end annotation


# virtual methods
.method public abstract onPanoramaInfoLoaded(ILandroid/os/Bundle;ILandroid/content/Intent;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
