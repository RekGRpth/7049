.class public Lcom/google/android/gms/panorama/math/Matrix3x3d;
.super Ljava/lang/Object;
.source "Matrix3x3d.java"


# instance fields
.field public m:[D


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    return-void
.end method

.method public static add(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 10
    .param p0    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v5

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v5

    add-double/2addr v1, v3

    aput-wide v1, v0, v5

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v6

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v6

    add-double/2addr v1, v3

    aput-wide v1, v0, v6

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v7

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v7

    add-double/2addr v1, v3

    aput-wide v1, v0, v7

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v8

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v8

    add-double/2addr v1, v3

    aput-wide v1, v0, v8

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v9

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v9

    add-double/2addr v1, v3

    aput-wide v1, v0, v9

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x5

    aget-wide v2, v2, v3

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x6

    aget-wide v2, v2, v3

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x6

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x7

    aget-wide v2, v2, v3

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x7

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p2, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v3, 0x8

    aget-wide v2, v2, v3

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v5, 0x8

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    return-void
.end method

.method public static mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 24
    .param p0    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v2, 0x0

    aget-wide v1, v1, v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x0

    aget-wide v3, v3, v4

    mul-double/2addr v1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x1

    aget-wide v3, v3, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x3

    aget-wide v5, v5, v6

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x2

    aget-wide v3, v3, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x6

    aget-wide v5, v5, v6

    mul-double/2addr v3, v5

    add-double v2, v1, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x1

    aget-wide v6, v1, v6

    mul-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x1

    aget-wide v6, v1, v6

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x4

    aget-wide v8, v1, v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x2

    aget-wide v6, v1, v6

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x7

    aget-wide v8, v1, v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x0

    aget-wide v6, v1, v6

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x2

    aget-wide v8, v1, v8

    mul-double/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x1

    aget-wide v8, v1, v8

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x5

    aget-wide v10, v1, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x2

    aget-wide v8, v1, v8

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v10, 0x8

    aget-wide v10, v1, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x3

    aget-wide v8, v1, v8

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x0

    aget-wide v10, v1, v10

    mul-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x4

    aget-wide v10, v1, v10

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x3

    aget-wide v12, v1, v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x5

    aget-wide v10, v1, v10

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x6

    aget-wide v12, v1, v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x3

    aget-wide v10, v1, v10

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x1

    aget-wide v12, v1, v12

    mul-double/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x4

    aget-wide v12, v1, v12

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x4

    aget-wide v14, v1, v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x5

    aget-wide v12, v1, v12

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x7

    aget-wide v14, v1, v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x3

    aget-wide v12, v1, v12

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x2

    aget-wide v14, v1, v14

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x4

    aget-wide v14, v1, v14

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x5

    aget-wide v16, v1, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x5

    aget-wide v14, v1, v14

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x8

    aget-wide v16, v1, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x6

    aget-wide v14, v1, v14

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x0

    aget-wide v16, v1, v16

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x7

    aget-wide v16, v1, v16

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x3

    aget-wide v18, v1, v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x8

    aget-wide v16, v1, v16

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x6

    aget-wide v18, v1, v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x6

    aget-wide v16, v1, v16

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x1

    aget-wide v18, v1, v18

    mul-double v16, v16, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x7

    aget-wide v18, v1, v18

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v20, 0x4

    aget-wide v20, v1, v20

    mul-double v18, v18, v20

    add-double v16, v16, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x8

    aget-wide v18, v1, v18

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v20, 0x7

    aget-wide v20, v1, v20

    mul-double v18, v18, v20

    add-double v16, v16, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v18, 0x6

    aget-wide v18, v1, v18

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v20, 0x2

    aget-wide v20, v1, v20

    mul-double v18, v18, v20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v20, 0x7

    aget-wide v20, v1, v20

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v22, 0x5

    aget-wide v22, v1, v22

    mul-double v20, v20, v22

    add-double v18, v18, v20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v20, 0x8

    aget-wide v20, v1, v20

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v22, 0x8

    aget-wide v22, v1, v22

    mul-double v20, v20, v22

    add-double v18, v18, v20

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v19}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(DDDDDDDDD)V

    return-void
.end method

.method public static mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 12
    .param p0    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    iget-wide v8, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x1

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x2

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v8, v10

    add-double v0, v6, v8

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x3

    aget-wide v6, v6, v7

    iget-wide v8, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x4

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x5

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v8, v10

    add-double v2, v6, v8

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x6

    aget-wide v6, v6, v7

    iget-wide v8, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x7

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v9, 0x8

    aget-wide v8, v8, v9

    iget-wide v10, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    iput-wide v0, p2, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iput-wide v2, p2, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iput-wide v4, p2, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    return-void
.end method


# virtual methods
.method public determinant()D
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    invoke-virtual {p0, v12, v12}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v0

    invoke-virtual {p0, v10, v10}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v2

    invoke-virtual {p0, v11, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p0, v11, v10}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    invoke-virtual {p0, v10, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    invoke-virtual {p0, v12, v10}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v2

    invoke-virtual {p0, v10, v12}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    invoke-virtual {p0, v11, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-virtual {p0, v10, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    invoke-virtual {p0, v11, v12}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v8

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    invoke-virtual {p0, v12, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v2

    invoke-virtual {p0, v10, v12}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    invoke-virtual {p0, v11, v10}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-virtual {p0, v10, v10}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    invoke-virtual {p0, v11, v12}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v8

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public get(II)D
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    mul-int/lit8 v1, p1, 0x3

    add-int/2addr v1, p2

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public invert(Lcom/google/android/gms/panorama/math/Matrix3x3d;)Z
    .locals 29
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->determinant()D

    move-result-wide v21

    const-wide/16 v2, 0x0

    cmpl-double v2, v21, v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L

    div-double v23, v2, v21

    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v2

    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double v3, v2, v23

    const/4 v2, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v5

    const/4 v2, 0x2

    const/4 v7, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v7

    mul-double/2addr v5, v7

    const/4 v2, 0x1

    const/4 v7, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v7

    const/4 v2, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    neg-double v5, v5

    mul-double v5, v5, v23

    const/4 v2, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v7

    const/4 v2, 0x2

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    mul-double/2addr v7, v9

    const/4 v2, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    const/4 v2, 0x1

    const/4 v11, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    mul-double/2addr v9, v11

    sub-double/2addr v7, v9

    mul-double v7, v7, v23

    const/4 v2, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    const/4 v2, 0x2

    const/4 v11, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    mul-double/2addr v9, v11

    const/4 v2, 0x0

    const/4 v11, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    const/4 v2, 0x2

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v13

    mul-double/2addr v11, v13

    sub-double/2addr v9, v11

    neg-double v9, v9

    mul-double v9, v9, v23

    const/4 v2, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    const/4 v2, 0x2

    const/4 v13, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v13

    mul-double/2addr v11, v13

    const/4 v2, 0x0

    const/4 v13, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v13

    const/4 v2, 0x2

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v15

    mul-double/2addr v13, v15

    sub-double/2addr v11, v13

    mul-double v11, v11, v23

    const/4 v2, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v13

    const/4 v2, 0x2

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v15

    mul-double/2addr v13, v15

    const/4 v2, 0x2

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v15

    const/4 v2, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v17

    mul-double v15, v15, v17

    sub-double/2addr v13, v15

    neg-double v13, v13

    mul-double v13, v13, v23

    const/4 v2, 0x0

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v15

    const/4 v2, 0x1

    const/16 v17, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v17

    mul-double v15, v15, v17

    const/4 v2, 0x0

    const/16 v17, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v17

    const/4 v2, 0x1

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v19

    mul-double v17, v17, v19

    sub-double v15, v15, v17

    mul-double v15, v15, v23

    const/4 v2, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v17

    const/4 v2, 0x1

    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v19

    mul-double v17, v17, v19

    const/4 v2, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v19

    const/4 v2, 0x0

    const/16 v25, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v25

    mul-double v19, v19, v25

    sub-double v17, v17, v19

    move-wide/from16 v0, v17

    neg-double v0, v0

    move-wide/from16 v17, v0

    mul-double v17, v17, v23

    const/4 v2, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v19

    const/4 v2, 0x1

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v25

    mul-double v19, v19, v25

    const/4 v2, 0x1

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v25

    const/4 v2, 0x0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v27

    mul-double v25, v25, v27

    sub-double v19, v19, v25

    mul-double v19, v19, v23

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v20}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(DDDDDDDDD)V

    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public minusEquals(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 10
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v5

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v5

    sub-double/2addr v1, v3

    aput-wide v1, v0, v5

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v6

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v6

    sub-double/2addr v1, v3

    aput-wide v1, v0, v6

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v7

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v7

    sub-double/2addr v1, v3

    aput-wide v1, v0, v7

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v8

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v8

    sub-double/2addr v1, v3

    aput-wide v1, v0, v8

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v9

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v9

    sub-double/2addr v1, v3

    aput-wide v1, v0, v9

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    sub-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x6

    aget-wide v4, v4, v5

    sub-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x7

    aget-wide v4, v4, v5

    sub-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v5, 0x8

    aget-wide v4, v4, v5

    sub-double/2addr v2, v4

    aput-wide v2, v0, v1

    return-void
.end method

.method public plusEquals(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 10
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v5

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v5

    add-double/2addr v1, v3

    aput-wide v1, v0, v5

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v6

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v6

    add-double/2addr v1, v3

    aput-wide v1, v0, v6

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v7

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v7

    add-double/2addr v1, v3

    aput-wide v1, v0, v7

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v8

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v8

    add-double/2addr v1, v3

    aput-wide v1, v0, v8

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v0, v9

    iget-object v3, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v9

    add-double/2addr v1, v3

    aput-wide v1, v0, v9

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x6

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x7

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    aget-wide v2, v0, v1

    iget-object v4, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v5, 0x8

    aget-wide v4, v4, v5

    add-double/2addr v2, v4

    aput-wide v2, v0, v1

    return-void
.end method

.method public scale(D)V
    .locals 4
    .param p1    # D

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x0

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x1

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x2

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x3

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x4

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    aget-wide v2, v0, v1

    mul-double/2addr v2, p1

    aput-wide v2, v0, v1

    return-void
.end method

.method public set(DDDDDDDDD)V
    .locals 2
    .param p1    # D
    .param p3    # D
    .param p5    # D
    .param p7    # D
    .param p9    # D
    .param p11    # D
    .param p13    # D
    .param p15    # D
    .param p17    # D

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x1

    aput-wide p3, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x2

    aput-wide p5, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x3

    aput-wide p7, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x4

    aput-wide p9, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    aput-wide p11, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    aput-wide p13, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    aput-wide p15, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    aput-wide p17, v0, v1

    return-void
.end method

.method public set(IID)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # D

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    mul-int/lit8 v1, p1, 0x3

    add-int/2addr v1, p2

    aput-wide p3, v0, v1

    return-void
.end method

.method public set(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v3

    aput-wide v1, v0, v3

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v4

    aput-wide v1, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v5

    aput-wide v1, v0, v5

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v6

    aput-wide v1, v0, v6

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v1, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v1, v1, v7

    aput-wide v1, v0, v7

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x5

    aget-wide v2, v2, v3

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x6

    aget-wide v2, v2, v3

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x7

    aget-wide v2, v2, v3

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v3, 0x8

    aget-wide v2, v2, v3

    aput-wide v2, v0, v1

    return-void
.end method

.method public setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-wide v1, p2, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    aput-wide v1, v0, p1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    add-int/lit8 v1, p1, 0x3

    iget-wide v2, p2, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    add-int/lit8 v1, p1, 0x6

    iget-wide v2, p2, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    aput-wide v2, v0, v1

    return-void
.end method

.method public setIdentity()V
    .locals 14

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x5

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x6

    iget-object v10, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v11, 0x7

    const-wide/16 v12, 0x0

    aput-wide v12, v10, v11

    aput-wide v12, v8, v9

    aput-wide v12, v6, v7

    aput-wide v12, v4, v5

    aput-wide v12, v2, v3

    aput-wide v12, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v5, 0x8

    const-wide/high16 v6, 0x3ff0000000000000L

    aput-wide v6, v4, v5

    aput-wide v6, v2, v3

    aput-wide v6, v0, v1

    return-void
.end method

.method public setSameDiagonal(D)V
    .locals 6
    .param p1    # D

    iget-object v0, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v5, 0x8

    aput-wide p1, v4, v5

    aput-wide p1, v2, v3

    aput-wide p1, v0, v1

    return-void
.end method

.method public setZero()V
    .locals 21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v10, 0x4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v12, 0x5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v14, 0x6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v16, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    move-object/from16 v17, v0

    const/16 v18, 0x8

    const-wide/16 v19, 0x0

    aput-wide v19, v17, v18

    aput-wide v19, v15, v16

    aput-wide v19, v13, v14

    aput-wide v19, v11, v12

    aput-wide v19, v9, v10

    aput-wide v19, v7, v8

    aput-wide v19, v5, v6

    aput-wide v19, v3, v4

    aput-wide v19, v1, v2

    return-void
.end method

.method public transpose()V
    .locals 10

    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v0, v2, v5

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v3, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v7

    aput-wide v3, v2, v5

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aput-wide v0, v2, v7

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v0, v2, v6

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v3, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v3, v3, v9

    aput-wide v3, v2, v6

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aput-wide v0, v2, v9

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v0, v2, v8

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v3, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v4, 0x7

    aget-wide v3, v3, v4

    aput-wide v3, v2, v8

    iget-object v2, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v3, 0x7

    aput-wide v0, v2, v3

    return-void
.end method

.method public transpose(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 14
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v0, v6, v10

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v2, v6, v11

    iget-object v6, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x5

    aget-wide v4, v6, v7

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v7, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v7, v7, v9

    aput-wide v7, v6, v9

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v7, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v7, v7, v12

    aput-wide v7, v6, v10

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v7, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v8, 0x6

    aget-wide v7, v7, v8

    aput-wide v7, v6, v11

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aput-wide v0, v6, v12

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    iget-object v7, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    aget-wide v7, v7, v13

    aput-wide v7, v6, v13

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x5

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v9, 0x7

    aget-wide v8, v8, v9

    aput-wide v8, v6, v7

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x6

    aput-wide v2, v6, v7

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/4 v7, 0x7

    aput-wide v4, v6, v7

    iget-object v6, p1, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v7, 0x8

    iget-object v8, p0, Lcom/google/android/gms/panorama/math/Matrix3x3d;->m:[D

    const/16 v9, 0x8

    aget-wide v8, v8, v9

    aput-wide v8, v6, v7

    return-void
.end method
