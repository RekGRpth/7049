.class public abstract Lcom/google/android/gms/panorama/opengl/DrawableGL;
.super Ljava/lang/Object;
.source "DrawableGL.java"


# instance fields
.field protected mChildren:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/panorama/opengl/DrawableGL;",
            ">;"
        }
    .end annotation
.end field

.field protected mGlobalMatrix:[F

.field protected mIndices:Ljava/nio/ShortBuffer;

.field protected mLocalMatrix:[F

.field protected final mParent:Lcom/google/android/gms/panorama/opengl/DrawableGL;

.field protected mShader:Lcom/google/android/gms/panorama/opengl/Shader;

.field protected mTexCoords:Ljava/nio/FloatBuffer;

.field protected mTextures:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/gms/panorama/opengl/GLTexture;",
            ">;"
        }
    .end annotation
.end field

.field protected mVertices:Ljava/nio/FloatBuffer;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mVertices:Ljava/nio/FloatBuffer;

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mTexCoords:Ljava/nio/FloatBuffer;

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mIndices:Ljava/nio/ShortBuffer;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mTextures:Ljava/util/Vector;

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mLocalMatrix:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mGlobalMatrix:[F

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mChildren:Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mLocalMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iput-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mParent:Lcom/google/android/gms/panorama/opengl/DrawableGL;

    return-void
.end method


# virtual methods
.method public draw([F)V
    .locals 7
    .param p1    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mGlobalMatrix:[F

    iget-object v4, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mLocalMatrix:[F

    move-object v2, p1

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mChildren:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mChildren:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/panorama/opengl/DrawableGL;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mGlobalMatrix:[F

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/opengl/DrawableGL;->draw([F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mGlobalMatrix:[F

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/opengl/DrawableGL;->drawObject([F)V

    return-void
.end method

.method public abstract drawObject([F)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation
.end method

.method protected initGeometry(IIZ)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    mul-int/lit8 v0, p1, 0x3

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mVertices:Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mIndices:Ljava/nio/ShortBuffer;

    if-eqz p3, :cond_0

    mul-int/lit8 v0, p1, 0x2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mTexCoords:Ljava/nio/FloatBuffer;

    :cond_0
    return-void
.end method

.method protected putIndex(IS)V
    .locals 1
    .param p1    # I
    .param p2    # S

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mIndices:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    return-void
.end method

.method protected putVertex(IFFF)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # F
    .param p4    # F

    mul-int/lit8 v0, p1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mVertices:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mVertices:Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v2, v1, p3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mVertices:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0, p4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method

.method public setShader(Lcom/google/android/gms/panorama/opengl/Shader;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/panorama/opengl/Shader;

    iput-object p1, p0, Lcom/google/android/gms/panorama/opengl/DrawableGL;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    return-void
.end method
