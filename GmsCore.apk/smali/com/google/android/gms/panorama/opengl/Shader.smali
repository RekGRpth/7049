.class public abstract Lcom/google/android/gms/panorama/opengl/Shader;
.super Ljava/lang/Object;
.source "Shader.java"


# instance fields
.field protected mProgram:I

.field protected mSamplerIndex:I

.field protected mTextureCoordIndex:I

.field protected mTransformIndex:I

.field protected mVertexIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mVertexIndex:I

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTextureCoordIndex:I

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTransformIndex:I

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mSamplerIndex:I

    return-void
.end method

.method public static createProgram(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x8b31

    invoke-static {v4, p0}, Lcom/google/android/gms/panorama/opengl/Shader;->loadShader(ILjava/lang/String;)I

    move-result v3

    const v4, 0x8b30

    invoke-static {v4, p1}, Lcom/google/android/gms/panorama/opengl/Shader;->loadShader(ILjava/lang/String;)I

    move-result v1

    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v4, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    const-string v5, "Unable to create program"

    invoke-direct {v4, v5}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v4, "glAttachShader"

    invoke-static {v4}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v4, "glAttachShader"

    invoke-static {v4}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v0, v6, [I

    const v4, 0x8b82

    invoke-static {v2, v4, v0, v5}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v4, v0, v5

    if-eq v4, v6, :cond_1

    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    new-instance v4, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    const-string v5, "Could not link program"

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v4

    :cond_1
    return v2
.end method

.method protected static loadShader(ILjava/lang/String;)I
    .locals 6
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    if-nez v1, :cond_0

    new-instance v3, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    const-string v4, "Unable to create shader"

    invoke-direct {v3, v4}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    const/4 v3, 0x1

    new-array v0, v3, [I

    const v3, 0x8b81

    invoke-static {v1, v3, v0, v4}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    aget v3, v0, v4

    if-nez v3, :cond_1

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    new-instance v3, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to compile shader "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    :cond_1
    return v1
.end method


# virtual methods
.method public bind()V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    return-void
.end method

.method protected getAttribute(ILjava/lang/String;)I
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in shader"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glGetAttribLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    return v0
.end method

.method protected getUniform(ILjava/lang/String;)I
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Lcom/google/android/gms/panorama/opengl/OpenGLException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in shader"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/panorama/opengl/OpenGLException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glGetUniformLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->logError(Ljava/lang/String;)V

    return v0
.end method

.method public setTexCoords(Ljava/nio/FloatBuffer;)V
    .locals 6
    .param p1    # Ljava/nio/FloatBuffer;

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTextureCoordIndex:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTextureCoordIndex:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    move v4, v3

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTextureCoordIndex:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto :goto_0
.end method

.method public setTransform([F)V
    .locals 3
    .param p1    # [F

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTransformIndex:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mTransformIndex:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    goto :goto_0
.end method

.method public setVertices(Ljava/nio/FloatBuffer;)V
    .locals 6
    .param p1    # Ljava/nio/FloatBuffer;

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mVertexIndex:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mVertexIndex:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/16 v4, 0xc

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Shader;->mVertexIndex:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto :goto_0
.end method
