.class public Lcom/google/android/gms/panorama/sensor/OrientationEKF;
.super Ljava/lang/Object;
.source "OrientationEKF.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private accObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private down:Lcom/google/android/gms/panorama/math/Vector3d;

.field private filteredGyroTimestep:F

.field private gyroFilterValid:Z

.field private mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mNu:Lcom/google/android/gms/panorama/math/Vector3d;

.field private mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mQ:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mR:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mRaccel:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mS:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private magObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private mh:Lcom/google/android/gms/panorama/math/Vector3d;

.field private mu:Lcom/google/android/gms/panorama/math/Vector3d;

.field private mx:Lcom/google/android/gms/panorama/math/Vector3d;

.field private mz:Lcom/google/android/gms/panorama/math/Vector3d;

.field private north:Lcom/google/android/gms/panorama/math/Vector3d;

.field private numGyroTimestepSamples:I

.field private processAccTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processAccTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processAccTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processAccTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processAccVDelta:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processGyroTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processGyroTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempM6:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private processMagTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processMagTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processMagTempV3:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processMagTempV4:Lcom/google/android/gms/panorama/math/Vector3d;

.field private processMagTempV5:Lcom/google/android/gms/panorama/math/Vector3d;

.field private rotationMatrix:[D

.field private sensorTimeStampAcc:J

.field private sensorTimeStampGyro:J

.field private sensorTimeStampMag:J

.field private setHeadingDegreesTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private timestepFilterInit:Z

.field private updateCovariancesAfterMotionTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private updateCovariancesAfterMotionTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mQ:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mR:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mRaccel:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mS:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mNu:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mz:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mh:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mu:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mx:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->down:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->north:Lcom/google/android/gms/panorama/math/Vector3d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->timestepFilterInit:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->gyroFilterValid:Z

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->setHeadingDegreesTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccVDelta:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempV3:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempV4:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempV5:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processMagTempM6:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->accObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->magObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {p0}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->init()V

    return-void
.end method

.method private accObservationFunctionForNumericalJacobian(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->down:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mh:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mh:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mz:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->accObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->accObservationFunctionForNumericalJacobianTempM:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, p2}, Lcom/google/android/gms/panorama/sensor/So3Util;->muFromSO3(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    return-void
.end method

.method private filterGyroTimestep(F)V
    .locals 5
    .param p1    # F

    const/4 v4, 0x1

    const v0, 0x3f733333

    const/high16 v1, 0x41200000

    iget-boolean v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->timestepFilterInit:Z

    if-nez v2, :cond_1

    iput p1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->filteredGyroTimestep:F

    iput v4, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->numGyroTimestepSamples:I

    iput-boolean v4, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->timestepFilterInit:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v2, 0x3f733333

    iget v3, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->filteredGyroTimestep:F

    mul-float/2addr v2, v3

    const v3, 0x3d4cccd0

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->filteredGyroTimestep:F

    iget v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->numGyroTimestepSamples:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->numGyroTimestepSamples:I

    int-to-float v2, v2

    const/high16 v3, 0x41200000

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iput-boolean v4, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->gyroFilterValid:Z

    goto :goto_0
.end method

.method private init()V
    .locals 15

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampGyro:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampAcc:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampMag:J

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setIdentity()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setIdentity()V

    const-wide/high16 v7, 0x4014000000000000L

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const-wide/high16 v1, 0x4039000000000000L

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setSameDiagonal(D)V

    const-wide/high16 v9, 0x3ff0000000000000L

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mQ:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mQ:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const-wide/high16 v1, 0x3ff0000000000000L

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setSameDiagonal(D)V

    const-wide/high16 v11, 0x3fd0000000000000L

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mR:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mR:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const-wide/high16 v1, 0x3fb0000000000000L

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setSameDiagonal(D)V

    const-wide/high16 v13, 0x4000000000000000L

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mRaccel:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mRaccel:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const-wide/high16 v1, 0x4010000000000000L

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setSameDiagonal(D)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mS:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mNu:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mz:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mh:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mu:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mx:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->down:Lcom/google/android/gms/panorama/math/Vector3d;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const-wide v5, 0x40239eb851eb851fL

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->north:Lcom/google/android/gms/panorama/math/Vector3d;

    const-wide/16 v1, 0x0

    const-wide/high16 v3, 0x3ff0000000000000L

    const-wide/16 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    return-void
.end method

.method private updateCovariancesAfterMotion()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->transpose(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotionTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setIdentity()V

    return-void
.end method


# virtual methods
.method public getGLMatrix()[D
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v7, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    mul-int/lit8 v3, v0, 0x4

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    aput-wide v4, v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    iget-object v3, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/16 v6, 0xb

    aput-wide v8, v5, v6

    aput-wide v8, v3, v4

    aput-wide v8, v2, v7

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/16 v7, 0xe

    aput-wide v8, v6, v7

    aput-wide v8, v4, v5

    aput-wide v8, v2, v3

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    const/16 v3, 0xf

    const-wide/high16 v4, 0x3ff0000000000000L

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->rotationMatrix:[D

    return-object v2
.end method

.method public declared-synchronized processAcc([FJ)V
    .locals 12
    .param p1    # [F
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mz:Lcom/google/android/gms/panorama/math/Vector3d;

    const/4 v1, 0x0

    aget v1, p1, v1

    float-to-double v1, v1

    const/4 v3, 0x1

    aget v3, p1, v3

    float-to-double v3, v3

    const/4 v5, 0x2

    aget v5, p1, v5

    float-to-double v5, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    iget-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampAcc:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mNu:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->accObservationFunctionForNumericalJacobian(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    const-wide v9, 0x3e7ad7f29abcaf48L

    const/4 v8, 0x0

    :goto_0
    const/4 v0, 0x3

    if-ge v8, v0, :cond_0

    iget-object v7, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccVDelta:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v7}, Lcom/google/android/gms/panorama/math/Vector3d;->setZero()V

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/android/gms/panorama/math/Vector3d;->setComponent(ID)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v7, v0}, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromMu(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->accObservationFunctionForNumericalJacobian(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    iget-object v11, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV1:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mNu:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {v0, v11, v1}, Lcom/google/android/gms/panorama/math/Vector3d;->sub(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

    const-wide/high16 v1, 0x3ff0000000000000L

    div-double/2addr v1, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Vector3d;->scale(D)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempV2:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0, v8, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->transpose(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mRaccel:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mS:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->add(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mS:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->invert(Lcom/google/android/gms/panorama/math/Matrix3x3d;)Z

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->transpose(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM5:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mNu:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mx:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mK:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mH:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setIdentity()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->minusEquals(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM4:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processAccTempM3:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mx:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1}, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromMu(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotion()V

    :goto_1
    iput-wide p2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampAcc:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->down:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mz:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized processGyro([FJ)V
    .locals 10
    .param p1    # [F
    .param p2    # J

    monitor-enter p0

    const v8, 0x3d23d70a

    const v9, 0x3c23d70a

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampGyro:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampGyro:J

    sub-long v0, p2, v0

    long-to-float v0, v0

    const v1, 0x3089705f

    mul-float v7, v0, v1

    const v0, 0x3d23d70a

    cmpl-float v0, v7, v0

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->gyroFilterValid:Z

    if-eqz v0, :cond_1

    iget v7, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->filteredGyroTimestep:F

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mu:Lcom/google/android/gms/panorama/math/Vector3d;

    const/4 v1, 0x0

    aget v1, p1, v1

    neg-float v2, v7

    mul-float/2addr v1, v2

    float-to-double v1, v1

    const/4 v3, 0x1

    aget v3, p1, v3

    neg-float v4, v7

    mul-float/2addr v3, v4

    float-to-double v3, v3

    const/4 v5, 0x2

    aget v5, p1, v5

    neg-float v6, v7

    mul-float/2addr v5, v6

    float-to-double v5, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mu:Lcom/google/android/gms/panorama/math/Vector3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1}, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromMu(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3LastMotion:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->so3SensorFromWorld:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->updateCovariancesAfterMotion()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mQ:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    mul-float v1, v7, v7

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->scale(D)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->mP:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    iget-object v1, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->processGyroTempM2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->plusEquals(Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    :cond_0
    iput-wide p2, p0, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->sensorTimeStampGyro:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    const v7, 0x3c23d70a

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-direct {p0, v7}, Lcom/google/android/gms/panorama/sensor/OrientationEKF;->filterGyroTimestep(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
