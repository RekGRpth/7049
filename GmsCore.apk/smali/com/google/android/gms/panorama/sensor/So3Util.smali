.class public Lcom/google/android/gms/panorama/sensor/So3Util;
.super Ljava/lang/Object;
.source "So3Util.java"


# static fields
.field private static muFromSO3R2:Lcom/google/android/gms/panorama/math/Vector3d;

.field private static sO3FromTwoVec33R1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private static sO3FromTwoVec33R2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

.field private static sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

.field private static sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

.field private static sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

.field private static temp31:Lcom/google/android/gms/panorama/math/Vector3d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->temp31:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec33R1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Matrix3x3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec33R2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    new-instance v0, Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-direct {v0}, Lcom/google/android/gms/panorama/math/Vector3d;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->muFromSO3R2:Lcom/google/android/gms/panorama/math/Vector3d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static muFromSO3(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 22
    .param p0    # Lcom/google/android/gms/panorama/math/Matrix3x3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    add-double/2addr v4, v9

    const/4 v6, 0x2

    const/4 v9, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    add-double/2addr v4, v9

    const-wide/high16 v9, 0x3ff0000000000000L

    sub-double/2addr v4, v9

    const-wide/high16 v9, 0x3fe0000000000000L

    mul-double v18, v4, v9

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v9, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    sub-double/2addr v4, v9

    const-wide/high16 v9, 0x4000000000000000L

    div-double v2, v4, v9

    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    sub-double/2addr v4, v9

    const-wide/high16 v9, 0x4000000000000000L

    div-double/2addr v4, v9

    const/4 v6, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    const/4 v6, 0x0

    const/4 v11, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    sub-double/2addr v9, v11

    const-wide/high16 v11, 0x4000000000000000L

    div-double v6, v9, v11

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/panorama/math/Vector3d;->length()D

    move-result-wide v20

    const-wide v4, 0x3fe6a09e667f3bcdL

    cmpl-double v4, v18, v4

    if-lez v4, :cond_1

    const-wide/16 v4, 0x0

    cmpl-double v4, v20, v4

    if-lez v4, :cond_0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    div-double v4, v4, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3d;->scale(D)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v4, -0x40195f619980c433L

    cmpl-double v4, v18, v4

    if-lez v4, :cond_2

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->acos(D)D

    move-result-wide v16

    div-double v4, v16, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3d;->scale(D)V

    goto :goto_0

    :cond_2
    const-wide v4, 0x400921fb54442d18L

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->asin(D)D

    move-result-wide v9

    sub-double v16, v4, v9

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    sub-double v2, v4, v18

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    sub-double v7, v4, v18

    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    sub-double v14, v4, v18

    sget-object v1, Lcom/google/android/gms/panorama/sensor/So3Util;->muFromSO3R2:Lcom/google/android/gms/panorama/math/Vector3d;

    mul-double v4, v2, v2

    mul-double v9, v7, v7

    cmpl-double v4, v4, v9

    if-lez v4, :cond_4

    mul-double v4, v2, v2

    mul-double v9, v14, v14

    cmpl-double v4, v4, v9

    if-lez v4, :cond_4

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    add-double/2addr v4, v9

    const-wide/high16 v9, 0x4000000000000000L

    div-double/2addr v4, v9

    const/4 v6, 0x0

    const/4 v9, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    const/4 v6, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    add-double/2addr v9, v11

    const-wide/high16 v11, 0x4000000000000000L

    div-double v6, v9, v11

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gms/panorama/math/Vector3d;->dot(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)D

    move-result-wide v4

    const-wide/16 v9, 0x0

    cmpg-double v4, v4, v9

    if-gez v4, :cond_3

    const-wide/high16 v4, -0x4010000000000000L

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3d;->scale(D)V

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/gms/panorama/math/Vector3d;->normalize()V

    goto/16 :goto_0

    :cond_4
    mul-double v4, v7, v7

    mul-double v9, v14, v14

    cmpl-double v4, v4, v9

    if-lez v4, :cond_5

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    add-double/2addr v4, v9

    const-wide/high16 v9, 0x4000000000000000L

    div-double v5, v4, v9

    const/4 v4, 0x2

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    const/4 v4, 0x1

    const/4 v11, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v11

    add-double/2addr v9, v11

    const-wide/high16 v11, 0x4000000000000000L

    div-double/2addr v9, v11

    move-object v4, v1

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v9

    add-double/2addr v4, v9

    const-wide/high16 v9, 0x4000000000000000L

    div-double v10, v4, v9

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v9, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->get(II)D

    move-result-wide v12

    add-double/2addr v4, v12

    const-wide/high16 v12, 0x4000000000000000L

    div-double v12, v4, v12

    move-object v9, v1

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    goto :goto_1
.end method

.method public static rodriguesSo3Exp(Lcom/google/android/gms/panorama/math/Vector3d;DDLcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 17
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # D
    .param p3    # D
    .param p5    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double v5, v11, v13

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double v7, v11, v13

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double v9, v11, v13

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/high16 v13, 0x3ff0000000000000L

    add-double v15, v7, v9

    mul-double v15, v15, p3

    sub-double/2addr v13, v15

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    const/4 v11, 0x1

    const/4 v12, 0x1

    const-wide/high16 v13, 0x3ff0000000000000L

    add-double v15, v5, v9

    mul-double v15, v15, p3

    sub-double/2addr v13, v15

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    const/4 v11, 0x2

    const/4 v12, 0x2

    const-wide/high16 v13, 0x3ff0000000000000L

    add-double v15, v5, v7

    mul-double v15, v15, p3

    sub-double/2addr v13, v15

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double v1, p1, v11

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v11, v13

    mul-double v3, p3, v11

    const/4 v11, 0x0

    const/4 v12, 0x1

    sub-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    const/4 v11, 0x1

    const/4 v12, 0x0

    add-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double v1, p1, v11

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v11, v13

    mul-double v3, p3, v11

    const/4 v11, 0x0

    const/4 v12, 0x2

    add-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    sub-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double v1, p1, v11

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v11, v13

    mul-double v3, p3, v11

    const/4 v11, 0x1

    const/4 v12, 0x2

    sub-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    const/4 v11, 0x2

    const/4 v12, 0x1

    add-double v13, v3, v1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->set(IID)V

    return-void
.end method

.method public static sO3FromMu(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 22
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/gms/panorama/math/Vector3d;->dot(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    const-wide v14, 0x3e45798ee2308c3aL

    cmpg-double v2, v12, v14

    if-gez v2, :cond_0

    const-wide/high16 v14, 0x3ff0000000000000L

    const-wide v16, 0x3fc5555560000000L

    mul-double v16, v16, v12

    sub-double v3, v14, v16

    const-wide/high16 v5, 0x3fe0000000000000L

    :goto_0
    move-object/from16 v2, p0

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/panorama/sensor/So3Util;->rodriguesSo3Exp(Lcom/google/android/gms/panorama/math/Vector3d;DDLcom/google/android/gms/panorama/math/Matrix3x3d;)V

    return-void

    :cond_0
    const-wide v14, 0x3eb0c6f7a0b5ed8dL

    cmpg-double v2, v12, v14

    if-gez v2, :cond_1

    const-wide/high16 v14, 0x3fe0000000000000L

    const-wide v16, 0x3fa5555560000000L

    mul-double v16, v16, v12

    sub-double v5, v14, v16

    const-wide/high16 v14, 0x3ff0000000000000L

    const-wide v16, 0x3fc5555560000000L

    mul-double v16, v16, v12

    const-wide/high16 v18, 0x3ff0000000000000L

    const-wide v20, 0x3fc5555560000000L

    mul-double v20, v20, v12

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    sub-double v3, v14, v16

    goto :goto_0

    :cond_1
    const-wide/high16 v14, 0x3ff0000000000000L

    div-double v8, v14, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double v3, v14, v8

    const-wide/high16 v14, 0x3ff0000000000000L

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    sub-double v14, v14, v16

    mul-double v16, v8, v8

    mul-double v5, v14, v16

    goto :goto_0
.end method

.method public static sO3FromTwoVec(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V
    .locals 9
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Matrix3x3d;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setIdentity()V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {p0, p1, v2}, Lcom/google/android/gms/panorama/math/Vector3d;->cross(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/math/Vector3d;->length()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/panorama/math/Vector3d;->set(Lcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/panorama/math/Vector3d;->set(Lcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/math/Vector3d;->normalize()V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/math/Vector3d;->normalize()V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/math/Vector3d;->normalize()V

    sget-object v0, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec33R1:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0, v6, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0, v7, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    sget-object v3, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecA:Lcom/google/android/gms/panorama/math/Vector3d;

    sget-object v4, Lcom/google/android/gms/panorama/sensor/So3Util;->temp31:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/panorama/math/Vector3d;->cross(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->temp31:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v0, v8, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v1, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVec33R2:Lcom/google/android/gms/panorama/math/Matrix3x3d;

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v1, v6, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v1, v7, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecN:Lcom/google/android/gms/panorama/math/Vector3d;

    sget-object v3, Lcom/google/android/gms/panorama/sensor/So3Util;->sO3FromTwoVecB:Lcom/google/android/gms/panorama/math/Vector3d;

    sget-object v4, Lcom/google/android/gms/panorama/sensor/So3Util;->temp31:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/panorama/math/Vector3d;->cross(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V

    sget-object v2, Lcom/google/android/gms/panorama/sensor/So3Util;->temp31:Lcom/google/android/gms/panorama/math/Vector3d;

    invoke-virtual {v1, v8, v2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->setColumn(ILcom/google/android/gms/panorama/math/Vector3d;)V

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->transpose()V

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/panorama/math/Matrix3x3d;->mult(Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;Lcom/google/android/gms/panorama/math/Matrix3x3d;)V

    goto :goto_0
.end method
