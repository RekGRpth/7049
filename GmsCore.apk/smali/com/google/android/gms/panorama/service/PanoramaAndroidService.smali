.class public final Lcom/google/android/gms/panorama/service/PanoramaAndroidService;
.super Landroid/app/Service;
.source "PanoramaAndroidService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;,
        Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "com.google.android.gms.panorama.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;

    invoke-direct {v0, p0}, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;-><init>(Lcom/google/android/gms/panorama/service/PanoramaAndroidService;)V

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
