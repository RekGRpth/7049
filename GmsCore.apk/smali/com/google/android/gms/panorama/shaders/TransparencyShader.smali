.class public Lcom/google/android/gms/panorama/shaders/TransparencyShader;
.super Lcom/google/android/gms/panorama/opengl/Shader;
.source "TransparencyShader.java"


# instance fields
.field private mAlphaFactorIndex:I

.field private mFragmentShader:Ljava/lang/String;

.field private final mVertexShader:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/Shader;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mAlphaFactorIndex:I

    const-string v0, "uniform mat4 uMvpMatrix;                   \nattribute vec4 aPosition;                   \nattribute vec2 aTextureCoord;               \nvarying vec2 vTexCoord;                     \nvoid main()                                 \n{                                           \n   gl_Position = uMvpMatrix * aPosition;    \n   vTexCoord = aTextureCoord;               \n}                                           \n"

    iput-object v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mVertexShader:Ljava/lang/String;

    const-string v0, "precision highp float;                            \nuniform float uAlphaFactor;                         \nvarying vec2 vTexCoord;                             \nuniform sampler2D sTexture;                         \nvoid main()                                         \n{                                                   \n  vec4 texcolor;                                    \n  texcolor = texture2D( sTexture, vTexCoord );      \n  texcolor.a = uAlphaFactor;                        \n  gl_FragColor = texcolor;                          \n}                                                   \n"

    iput-object v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mFragmentShader:Ljava/lang/String;

    const-string v0, "uniform mat4 uMvpMatrix;                   \nattribute vec4 aPosition;                   \nattribute vec2 aTextureCoord;               \nvarying vec2 vTexCoord;                     \nvoid main()                                 \n{                                           \n   gl_Position = uMvpMatrix * aPosition;    \n   vTexCoord = aTextureCoord;               \n}                                           \n"

    iget-object v1, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mFragmentShader:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mProgram:I

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mProgram:I

    const-string v1, "aPosition"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->getAttribute(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mVertexIndex:I

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mProgram:I

    const-string v1, "aTextureCoord"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->getAttribute(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mTextureCoordIndex:I

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mProgram:I

    const-string v1, "uMvpMatrix"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->getUniform(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mTransformIndex:I

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mProgram:I

    const-string v1, "uAlphaFactor"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->getUniform(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mAlphaFactorIndex:I

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->bind()V

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mAlphaFactorIndex:I

    const v1, 0x3f666666

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    return-void
.end method


# virtual methods
.method public setAlpha(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->mAlphaFactorIndex:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    return-void
.end method
