.class public Lcom/google/android/gms/panorama/util/Dialogs;
.super Ljava/lang/Object;
.source "Dialogs.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createOkDialog(ILjava/lang/CharSequence;Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)Landroid/app/AlertDialog;
    .locals 4
    .param p0    # I
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/CharSequence;",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x1

    if-eq p0, v1, :cond_0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setTitle(I)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, -0x3

    const v2, 0x7f0b0053

    invoke-virtual {p2, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/panorama/util/Dialogs$2;

    invoke-direct {v3, p3}, Lcom/google/android/gms/panorama/util/Dialogs$2;-><init>(Lcom/google/android/gms/panorama/util/Callback;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object v0
.end method

.method public static createProgressDialog(ILandroid/content/Context;)Landroid/app/ProgressDialog;
    .locals 2
    .param p0    # I
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setTitle(I)V

    return-object v0
.end method

.method public static showDialog(IILandroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/android/gms/panorama/util/Dialogs;->showDialog(ILjava/lang/CharSequence;Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    return-void
.end method

.method public static showDialog(ILjava/lang/CharSequence;Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/CharSequence;",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/panorama/util/Dialogs;->createOkDialog(ILjava/lang/CharSequence;Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
