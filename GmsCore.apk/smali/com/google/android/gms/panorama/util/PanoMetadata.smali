.class public Lcom/google/android/gms/panorama/util/PanoMetadata;
.super Ljava/lang/Object;
.source "PanoMetadata.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final croppedAreaHeight:I

.field public final croppedAreaLeft:I

.field public final croppedAreaTop:I

.field public final croppedAreaWidth:I

.field public final firstPhotoTime:Ljava/util/Calendar;

.field public final fullPanoHeight:I

.field public final fullPanoWidth:I

.field public final imageHeight:I

.field public final imageWidth:I

.field public final largestValidInteriorRectHeight:I

.field public final largestValidInteriorRectLeft:I

.field public final largestValidInteriorRectTop:I

.field public final largestValidInteriorRectWidth:I

.field public final lastPhotoTime:Ljava/util/Calendar;

.field public final projectionType:Ljava/lang/String;

.field public final sourcePhotosCount:I

.field public final synthetic:Z

.field public final usePanoViewer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/util/PanoMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(IILjava/util/Calendar;Ljava/util/Calendar;ILjava/lang/String;ZIIIIIIIIIIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/util/Calendar;
    .param p4    # Ljava/util/Calendar;
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I
    .param p13    # I
    .param p14    # I
    .param p15    # I
    .param p16    # I
    .param p17    # I
    .param p18    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->imageWidth:I

    iput p2, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->imageHeight:I

    iput-object p3, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->firstPhotoTime:Ljava/util/Calendar;

    iput-object p4, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->lastPhotoTime:Ljava/util/Calendar;

    iput p5, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->sourcePhotosCount:I

    iput-object p6, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->projectionType:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->usePanoViewer:Z

    iput p8, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaWidth:I

    iput p9, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaHeight:I

    iput p10, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    iput p11, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoHeight:I

    iput p12, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaLeft:I

    iput p13, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaTop:I

    iput p14, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->largestValidInteriorRectLeft:I

    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->largestValidInteriorRectTop:I

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->largestValidInteriorRectWidth:I

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->largestValidInteriorRectHeight:I

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->synthetic:Z

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static getBoolean(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/adobe/xmp/XMPMeta;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->getPropertyBoolean(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getDate(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 1
    .param p0    # Lcom/adobe/xmp/XMPMeta;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->getPropertyCalendar(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I
    .locals 1
    .param p0    # Lcom/adobe/xmp/XMPMeta;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getString(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/adobe/xmp/XMPMeta;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNear(DDD)Z
    .locals 2
    .param p0    # D
    .param p2    # D
    .param p4    # D

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parse(Lcom/google/android/gms/panorama/util/InputStreamFactory;)Lcom/google/android/gms/panorama/util/PanoMetadata;
    .locals 35
    .param p0    # Lcom/google/android/gms/panorama/util/InputStreamFactory;

    invoke-interface/range {p0 .. p0}, Lcom/google/android/gms/panorama/util/InputStreamFactory;->create()Ljava/io/InputStream;

    move-result-object v28

    if-nez v28, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    const/16 v25, 0x0

    const/16 v29, 0x0

    const/16 v31, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v33, 0x0

    invoke-static/range {v28 .. v28}, Lcom/google/android/gms/panorama/xmp/XmpUtil;->extractXMPMeta(Ljava/io/InputStream;)Lcom/adobe/xmp/XMPMeta;

    move-result-object v34

    if-eqz v34, :cond_1

    :try_start_0
    const-string v6, "FirstPhotoDate"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getDate(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v25

    const-string v6, "LastPhotoDate"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getDate(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v29

    const-string v6, "SourcePhotosCount"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v31

    const-string v6, "ProjectionType"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getString(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v6, "UsePanoramaViewer"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getBoolean(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)Z

    move-result v13

    const-string v6, "CroppedAreaImageWidthPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v14

    const-string v6, "CroppedAreaImageHeightPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v15

    const-string v6, "FullPanoWidthPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v16

    const-string v6, "FullPanoHeightPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v17

    const-string v6, "CroppedAreaLeftPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v18

    const-string v6, "CroppedAreaTopPixels"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v19

    const-string v6, "LargestValidInteriorRectLeft"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v20

    const-string v6, "LargestValidInteriorRectTop"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v21

    const-string v6, "LargestValidInteriorRectWidth"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I

    move-result v22

    const-string v6, "LargestValidInteriorRectHeight"

    move-object/from16 v0, v34

    invoke-static {v0, v6}, Lcom/google/android/gms/panorama/util/PanoMetadata;->getInt(Lcom/adobe/xmp/XMPMeta;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v23

    const/16 v24, 0x0

    if-lez v14, :cond_4

    if-lez v15, :cond_4

    if-lez v16, :cond_4

    if-lez v17, :cond_4

    const/16 v33, 0x1

    :cond_1
    :goto_1
    new-instance v30, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v6, 0x1

    move-object/from16 v0, v30

    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-interface/range {p0 .. p0}, Lcom/google/android/gms/panorama/util/InputStreamFactory;->create()Ljava/io/InputStream;

    move-result-object v28

    const/4 v6, 0x0

    :try_start_1
    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v28, :cond_2

    :try_start_2
    invoke-virtual/range {v28 .. v28}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_2
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v27, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v26, v0

    mul-int/lit8 v6, v26, 0x2

    move/from16 v0, v27

    if-ne v6, v0, :cond_6

    const/16 v32, 0x1

    :goto_3
    if-nez v33, :cond_3

    if-eqz v32, :cond_7

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v25

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v29

    const/16 v31, -0x1

    const-string v12, "equirectangular"

    const/4 v13, 0x1

    move/from16 v14, v27

    move/from16 v15, v26

    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v16, v27

    move/from16 v17, v26

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v22, v27

    move/from16 v23, v26

    const/16 v24, 0x1

    :cond_3
    move/from16 v0, v27

    int-to-double v6, v0

    move/from16 v0, v26

    int-to-double v8, v0

    div-double v2, v6, v8

    int-to-double v6, v14

    int-to-double v8, v15

    div-double v4, v6, v8

    const-wide v6, 0x3f50624dd2f1a9fcL

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/panorama/util/PanoMetadata;->isNear(DDD)Z

    move-result v6

    if-nez v6, :cond_8

    sget-object v6, Lcom/google/android/gms/panorama/util/PanoMetadata;->TAG:Ljava/lang/String;

    const-string v7, "Pano metadata does not match file dimensions."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_4
    const/16 v33, 0x0

    goto :goto_1

    :catchall_0
    move-exception v6

    if-eqz v28, :cond_5

    :try_start_3
    invoke-virtual/range {v28 .. v28}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    :goto_4
    throw v6

    :cond_6
    const/16 v32, 0x0

    goto :goto_3

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_8
    move/from16 v0, v16

    int-to-double v6, v0

    move/from16 v0, v17

    int-to-double v8, v0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L

    const-wide v10, 0x3fb999999999999aL

    invoke-static/range {v6 .. v11}, Lcom/google/android/gms/panorama/util/PanoMetadata;->isNear(DDD)Z

    move-result v6

    if-nez v6, :cond_9

    sget-object v6, Lcom/google/android/gms/panorama/util/PanoMetadata;->TAG:Ljava/lang/String;

    const-string v7, "Pano metadata invalid: Full pano dimension not 2:1."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_9
    new-instance v6, Lcom/google/android/gms/panorama/util/PanoMetadata;

    move/from16 v7, v27

    move/from16 v8, v26

    move-object/from16 v9, v25

    move-object/from16 v10, v29

    move/from16 v11, v31

    invoke-direct/range {v6 .. v24}, Lcom/google/android/gms/panorama/util/PanoMetadata;-><init>(IILjava/util/Calendar;Ljava/util/Calendar;ILjava/lang/String;ZIIIIIIIIIIZ)V

    goto/16 :goto_0

    :catch_0
    move-exception v6

    goto/16 :goto_2

    :catch_1
    move-exception v7

    goto :goto_4

    :catch_2
    move-exception v6

    goto/16 :goto_1
.end method

.method public static parse(Ljava/lang/String;)Lcom/google/android/gms/panorama/util/PanoMetadata;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/panorama/util/PanoMetadata$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/panorama/util/PanoMetadata$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/panorama/util/PanoMetadata;->parse(Lcom/google/android/gms/panorama/util/InputStreamFactory;)Lcom/google/android/gms/panorama/util/PanoMetadata;

    move-result-object v0

    return-object v0
.end method
