.class public Lcom/google/android/gms/panorama/viewer/PanoramaImage;
.super Ljava/lang/Object;
.source "PanoramaImage.java"


# instance fields
.field private lastColumnWidthRad:F

.field private lastRowHeightRad:F

.field private panoHeightRad:F

.field private panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

.field private panoOffsetLeftRad:F

.field private panoOffsetTopRad:F

.field private panoWidthRad:F

.field private final tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

.field private tileSizeRad:F


# direct methods
.method public constructor <init>(Lcom/google/android/gms/panorama/viewer/TileProvider;Lcom/google/android/gms/panorama/util/PanoMetadata;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/panorama/viewer/TileProvider;
    .param p2    # Lcom/google/android/gms/panorama/util/PanoMetadata;

    const-wide/high16 v4, 0x4000000000000000L

    const-wide v2, 0x400921fb54442d18L

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    iput-object p2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v0, v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v1, v1, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoWidthRad:F

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v0, v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v1, v1, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoHeightRad:F

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v0, v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaLeft:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v1, v1, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoOffsetLeftRad:F

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v0, v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaTop:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v1, v1, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoOffsetTopRad:F

    return-void
.end method


# virtual methods
.method public getLastColumnWidthRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->lastColumnWidthRad:F

    return v0
.end method

.method public getLastRowHeightRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->lastRowHeightRad:F

    return v0
.end method

.method public getOffsetLeftRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoOffsetLeftRad:F

    return v0
.end method

.method public getOffsetTopRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoOffsetTopRad:F

    return v0
.end method

.method public getPanoHeightRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoHeightRad:F

    return v0
.end method

.method public getPanoWidthRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoWidthRad:F

    return v0
.end method

.method public getTileProvider()Lcom/google/android/gms/panorama/viewer/TileProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    return-object v0
.end method

.method public getTileSizeRad()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileSizeRad:F

    return v0
.end method

.method public init()V
    .locals 7

    const-wide/high16 v5, 0x4000000000000000L

    const-wide v3, 0x400921fb54442d18L

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v1, v1, Lcom/google/android/gms/panorama/util/PanoMetadata;->imageWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v2, v2, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaWidth:I

    int-to-float v2, v2

    div-float v0, v1, v2

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v1}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v1}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getTileSize()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v2, v2, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v1, v1

    mul-double/2addr v1, v3

    mul-double/2addr v1, v5

    double-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileSizeRad:F

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v1}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getLastColumnWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v2, v2, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v1, v1

    mul-double/2addr v1, v3

    mul-double/2addr v1, v5

    double-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->lastColumnWidthRad:F

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v1}, Lcom/google/android/gms/panorama/viewer/TileProvider;->getLastRowHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->panoMetadata:Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v2, v2, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v1, v1

    mul-double/2addr v1, v3

    double-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->lastRowHeightRad:F

    return-void
.end method

.method public setMaximumTextureSize(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->tileProvider:Lcom/google/android/gms/panorama/viewer/TileProvider;

    invoke-interface {v0, p1}, Lcom/google/android/gms/panorama/viewer/TileProvider;->setMaximumTextureSize(I)V

    return-void
.end method
