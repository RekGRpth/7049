.class public Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;
.super Ljava/lang/Object;
.source "PanoramaViewRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAutoRotationCallback:Lcom/google/android/gms/panorama/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mAutoSpin:Z

.field private mAutoSpinFadeFactor:F

.field private mAutospinRateDegrees:F

.field private mCompassMode:Z

.field private final mContext:Landroid/content/Context;

.field private mCurFieldOfViewHorizontalDegrees:F

.field private mCurFieldOfViewLongSideDegrees:F

.field private mCurFieldOfViewVerticalDegrees:F

.field private mCurrentFov:F

.field private mFieldOfViewDegreesZoomStart:F

.field private mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

.field private mImageBoundsDeg:Landroid/graphics/RectF;

.field private mIntroSpinAngleLeftDegrees:F

.field private mLockPaddingDegrees:F

.field private mMVPMatrix:[F

.field private mModelView:[F

.field private mObjectsInitialized:Z

.field private mOnInitializedCallback:Lcom/google/android/gms/panorama/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mOrientationAngleDegrees:F

.field private mOrientationAngleDegreesTarget:F

.field private mPanoSphereShader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

.field private mPanoramaOpacity:F

.field private mPanoramaSphere:Lcom/google/android/gms/panorama/opengl/PartialSphere;

.field private mPanoramaView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

.field private mPerspective:[F

.field private mPitchAngleDegrees:F

.field private mSensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

.field private mSurfaceHeight:I

.field private mSurfaceWidth:I

.field private mTempMatrix:[F

.field private mTexturedCube:Lcom/google/android/gms/panorama/opengl/TexturedCube;

.field private mWireShader:Lcom/google/android/gms/panorama/opengl/SingleColorShader;

.field private mWireSphere:Lcom/google/android/gms/panorama/opengl/Sphere;

.field private mYawAngleDegrees:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/panorama/viewer/PanoramaView;Landroid/content/Context;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/panorama/viewer/PanoramaView;
    .param p2    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x10

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    iput-boolean v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    const/high16 v0, 0x42960000

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewLongSideDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurrentFov:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegreesTarget:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaOpacity:F

    const v0, 0x3eb33333

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutospinRateDegrees:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    iput v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPerspective:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mMVPMatrix:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mTempMatrix:[F

    iput-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    iput-boolean v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCompassMode:Z

    iput-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoRotationCallback:Lcom/google/android/gms/panorama/util/Callback;

    iput-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    iput-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOnInitializedCallback:Lcom/google/android/gms/panorama/util/Callback;

    const/high16 v0, 0x41a00000

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    iput-object p2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static clamp(FFF)F
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F

    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private drawScene()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->initFrame()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mTexturedCube:Lcom/google/android/gms/panorama/opengl/TexturedCube;

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mMVPMatrix:[F

    invoke-virtual {v1, v2}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->draw([F)V

    const/high16 v1, 0x3f800000

    invoke-static {v1}, Landroid/opengl/GLES20;->glLineWidth(F)V

    const/16 v1, 0xbe2

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireShader:Lcom/google/android/gms/panorama/opengl/SingleColorShader;

    sget-object v2, Lcom/google/android/gms/panorama/Constants;->TRANSPARENT_GRAY:[F

    invoke-virtual {v1, v2}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->setColor([F)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoSphereShader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-virtual {v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->bind()V

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoSphereShader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    iget v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaOpacity:F

    invoke-virtual {v1, v2}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaSphere:Lcom/google/android/gms/panorama/opengl/PartialSphere;

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mMVPMatrix:[F

    invoke-virtual {v1, v2}, Lcom/google/android/gms/panorama/opengl/PartialSphere;->draw([F)V
    :try_end_0
    .catch Lcom/google/android/gms/panorama/opengl/OpenGLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->printStackTrace()V

    goto :goto_0
.end method

.method private enforceBounds()V
    .locals 11

    const/high16 v8, 0x42b40000

    const/high16 v10, 0x40000000

    const/high16 v6, 0x41a00000

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewLongSideDegrees:F

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    const/high16 v7, -0x3d4c0000

    invoke-static {v6, v7, v8}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->clamp(FFF)F

    move-result v6

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    iget-object v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {v6}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getPanoWidthRad()F

    move-result v6

    float-to-double v6, v6

    const-wide v8, 0x401921fb54442d18L

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    div-float v2, v6, v10

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    div-float v3, v6, v10

    iget-object v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    sub-float/2addr v6, v7

    add-float v4, v6, v2

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iget-object v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    add-float/2addr v6, v7

    sub-float v1, v6, v2

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    invoke-static {v6, v1}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iget-object v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    sub-float/2addr v6, v7

    add-float v5, v6, v3

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    neg-float v6, v6

    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v6

    neg-float v6, v6

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    iget-object v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mLockPaddingDegrees:F

    add-float/2addr v6, v7

    sub-float v0, v6, v3

    iget v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    neg-float v6, v6

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v6

    neg-float v6, v6

    iput v6, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    goto :goto_0
.end method

.method private getMaxTextureSize()I
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v0, v1, [I

    const/16 v1, 0xd33

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v1, v0, v2

    return v1
.end method

.method private initFrame()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    invoke-static {v2, v2, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0x100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    return-void
.end method

.method private initRendering()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/4 v14, 0x0

    const/4 v13, 0x1

    new-instance v8, Lcom/google/android/gms/panorama/opengl/SingleColorShader;

    invoke-direct {v8}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;-><init>()V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireShader:Lcom/google/android/gms/panorama/opengl/SingleColorShader;

    new-instance v8, Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-direct {v8}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;-><init>()V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoSphereShader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireShader:Lcom/google/android/gms/panorama/opengl/SingleColorShader;

    sget-object v9, Lcom/google/android/gms/panorama/Constants;->ANDROID_BLUE:[F

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->setColor([F)V

    const/16 v1, 0x20

    const/16 v3, 0x40

    const v5, 0x409ccccd

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->getMaxTextureSize()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->setMaximumTextureSize(I)V

    new-instance v8, Lcom/google/android/gms/panorama/opengl/PartialSphere;

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-direct {v8, v9, v5}, Lcom/google/android/gms/panorama/opengl/PartialSphere;-><init>(Lcom/google/android/gms/panorama/viewer/PanoramaImage;F)V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaSphere:Lcom/google/android/gms/panorama/opengl/PartialSphere;

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaSphere:Lcom/google/android/gms/panorama/opengl/PartialSphere;

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoSphereShader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/opengl/PartialSphere;->setShader(Lcom/google/android/gms/panorama/opengl/Shader;)V

    const/16 v2, 0x18

    const/16 v4, 0x30

    const/high16 v6, 0x41000000

    new-instance v8, Lcom/google/android/gms/panorama/opengl/Sphere;

    invoke-direct {v8, v2, v4, v6}, Lcom/google/android/gms/panorama/opengl/Sphere;-><init>(IIF)V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireSphere:Lcom/google/android/gms/panorama/opengl/Sphere;

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireSphere:Lcom/google/android/gms/panorama/opengl/Sphere;

    invoke-virtual {v8, v13}, Lcom/google/android/gms/panorama/opengl/Sphere;->setLineDrawing(Z)V

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireSphere:Lcom/google/android/gms/panorama/opengl/Sphere;

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mWireShader:Lcom/google/android/gms/panorama/opengl/SingleColorShader;

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/opengl/Sphere;->setShader(Lcom/google/android/gms/panorama/opengl/Shader;)V

    sget-object v8, Lcom/google/android/gms/panorama/Constants;->BACKGROUND_BLACK:[F

    aget v8, v8, v14

    sget-object v9, Lcom/google/android/gms/panorama/Constants;->BACKGROUND_BLACK:[F

    aget v9, v9, v13

    sget-object v10, Lcom/google/android/gms/panorama/Constants;->BACKGROUND_BLACK:[F

    const/4 v11, 0x2

    aget v10, v10, v11

    sget-object v11, Lcom/google/android/gms/panorama/Constants;->BACKGROUND_BLACK:[F

    const/4 v12, 0x3

    aget v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v14, v7, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0200bc

    invoke-static {v8, v9, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v8, Lcom/google/android/gms/panorama/opengl/TexturedCube;

    const/high16 v9, 0x42000000

    invoke-direct {v8, v0, v9}, Lcom/google/android/gms/panorama/opengl/TexturedCube;-><init>(Landroid/graphics/Bitmap;F)V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mTexturedCube:Lcom/google/android/gms/panorama/opengl/TexturedCube;

    const-string v8, "Rendering objects are intialized."

    invoke-static {v8}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    iput-boolean v13, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOnInitializedCallback:Lcom/google/android/gms/panorama/util/Callback;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V

    return-void
.end method

.method private onAutoRotationStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoRotationCallback:Lcom/google/android/gms/panorama/util/Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoRotationCallback:Lcom/google/android/gms/panorama/util/Callback;

    iget-boolean v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static scaleFov(FFF)F
    .locals 10
    .param p0    # F
    .param p1    # F
    .param p2    # F

    const-wide/high16 v8, 0x4000000000000000L

    float-to-double v4, p1

    float-to-double v6, p0

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    mul-double/2addr v6, v8

    div-double v0, v4, v6

    float-to-double v4, p2

    mul-double v6, v8, v0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    mul-double v2, v8, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v4, v2

    return v4
.end method

.method private setPerspective(FFFF)V
    .locals 8
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    float-to-double v0, p1

    const-wide v6, 0x4076800000000000L

    div-double/2addr v0, v6

    const-wide v6, 0x400921fb54442d18L

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float v5, v0, p3

    mul-float v3, v5, p2

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPerspective:[F

    const/4 v1, 0x0

    neg-float v2, v3

    neg-float v4, v5

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    return-void
.end method

.method private stopAutoRotate(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000

    :goto_0
    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->onAutoRotationStateChanged()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateFieldOfViewDegrees(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x42b40000

    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/high16 v0, 0x41a00000

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurrentFov:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    if-le v0, v1, :cond_0

    iput p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->scaleFov(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewLongSideDegrees:F

    :goto_0
    const v0, 0x3eb33333

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutospinRateDegrees:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateMatrices()V

    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    int-to-float v1, v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->scaleFov(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewHorizontalDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewLongSideDegrees:F

    goto :goto_0
.end method

.method private declared-synchronized updateMatrices()V
    .locals 14

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    int-to-float v1, v1

    div-float v11, v0, v1

    const/high16 v13, 0x3f000000

    const/high16 v12, 0x43480000

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewVerticalDegrees:F

    const/high16 v1, 0x3f000000

    const/high16 v2, 0x43480000

    invoke-direct {p0, v0, v11, v1, v2}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setPerspective(FFFF)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegrees:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegreesTarget:F

    iget v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegrees:F

    sub-float/2addr v1, v2

    const v2, 0x3da3d70a

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegrees:F

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCompassMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/sensor/SensorReader;->getFilterOutput()[F

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mTempMatrix:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mMVPMatrix:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPerspective:[F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mTempMatrix:[F

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v6, 0x0

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOrientationAngleDegrees:F

    neg-float v7, v0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    const/high16 v8, 0x3f800000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mMVPMatrix:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPerspective:[F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private xyzToPitchYaw([F)Lcom/google/android/gms/panorama/math/PitchYaw;
    .locals 7
    .param p1    # [F

    new-instance v1, Lcom/google/android/gms/panorama/math/Vector3;

    const/4 v3, 0x0

    aget v3, p1, v3

    const/4 v4, 0x1

    aget v4, p1, v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/gms/panorama/math/Vector3;-><init>(FFF)V

    invoke-virtual {v1}, Lcom/google/android/gms/panorama/math/Vector3;->normalize()F

    iget v3, v1, Lcom/google/android/gms/panorama/math/Vector3;->y:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->asin(D)D

    move-result-wide v3

    double-to-float v0, v3

    iget v3, v1, Lcom/google/android/gms/panorama/math/Vector3;->x:F

    float-to-double v3, v3

    iget v5, v1, Lcom/google/android/gms/panorama/math/Vector3;->z:F

    float-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    double-to-float v2, v3

    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    float-to-double v3, v2

    const-wide v5, 0x401921fb54442d18L

    add-double/2addr v3, v5

    double-to-float v2, v3

    :cond_0
    new-instance v3, Lcom/google/android/gms/panorama/math/PitchYaw;

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v5

    double-to-float v5, v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/panorama/math/PitchYaw;-><init>(FF)V

    return-object v3
.end method


# virtual methods
.method public endPinchZoom(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateFieldOfViewDegrees(F)V

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurFieldOfViewLongSideDegrees:F

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    return-void
.end method

.method public getPitchDegrees()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    return v0
.end method

.method public getYawDegrees()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaView:Lcom/google/android/gms/panorama/viewer/PanoramaView;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->onDrawFrame()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutospinRateDegrees:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iput v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutospinRateDegrees:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->stopAutoRotate(Z)V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateMatrices()V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    :cond_4
    :goto_1
    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaOpacity:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaOpacity:F

    sub-float v1, v3, v1

    const v2, 0x3d4ccccd

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPanoramaOpacity:F

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->drawScene()V

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    const v1, 0x3951b717

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutospinRateDegrees:F

    iget v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    const v1, 0x3f6b851f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpinFadeFactor:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateMatrices()V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    goto :goto_1
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->TAG:Ljava/lang/String;

    const-string v2, "Image file not set. Cannot initialize rendering."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput p2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    iput p3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    iget-boolean v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    if-nez v1, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->initRendering()V
    :try_end_0
    .catch Lcom/google/android/gms/panorama/opengl/OpenGLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mCurrentFov:F

    invoke-direct {p0, v1}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateFieldOfViewDegrees(F)V

    const-string v1, "Rendering init completed."

    invoke-static {v1}, Lcom/google/android/gms/panorama/util/LG;->d(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->printStackTrace()V

    goto :goto_1
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 0
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;

    return-void
.end method

.method public pinchZoom(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mFieldOfViewDegreesZoomStart:F

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateFieldOfViewDegrees(F)V

    return-void
.end method

.method public screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;
    .locals 11
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x4

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->updateMatrices()V

    iget v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    int-to-float v0, v0

    sub-float p2, v0, p2

    new-array v9, v1, [F

    const/high16 v2, -0x40800000

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mModelView:[F

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPerspective:[F

    new-array v7, v1, [I

    aput v4, v7, v4

    const/4 v0, 0x1

    aput v4, v7, v0

    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceWidth:I

    aput v1, v7, v0

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSurfaceHeight:I

    aput v1, v7, v0

    move v0, p1

    move v1, p2

    move v6, v4

    move v8, v4

    move v10, v4

    invoke-static/range {v0 .. v10}, Landroid/opengl/GLU;->gluUnProject(FFF[FI[FI[II[FI)I

    invoke-direct {p0, v9}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->xyzToPitchYaw([F)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v0

    return-object v0
.end method

.method public setAutoRotationCallback(Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoRotationCallback:Lcom/google/android/gms/panorama/util/Callback;

    return-void
.end method

.method public setOnInitializedCallback(Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mOnInitializedCallback:Lcom/google/android/gms/panorama/util/Callback;

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mObjectsInitialized:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setPanoramaImage(Lcom/google/android/gms/panorama/viewer/PanoramaImage;)V
    .locals 7
    .param p1    # Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getPanoWidthRad()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v1, v2

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {v3}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getOffsetLeftRad()F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x400921fb54442d18L

    sub-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    double-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getPanoHeightRad()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v0, v2

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    const/high16 v3, 0x42b40000

    iget-object v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImage:Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    invoke-virtual {v4}, Lcom/google/android/gms/panorama/viewer/PanoramaImage;->getOffsetTopRad()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v4, v4

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mImageBoundsDeg:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    return-void
.end method

.method public setPitchAngleDegrees(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mPitchAngleDegrees:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->onAutoRotationStateChanged()V

    return-void
.end method

.method public setSensorReader(Landroid/view/Display;Lcom/google/android/gms/panorama/sensor/SensorReader;)V
    .locals 0
    .param p1    # Landroid/view/Display;
    .param p2    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iput-object p2, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mSensorReader:Lcom/google/android/gms/panorama/sensor/SensorReader;

    return-void
.end method

.method public setYawAngleDegrees(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->onAutoRotationStateChanged()V

    return-void
.end method

.method public startIntroAnimation()V
    .locals 1

    const/high16 v0, 0x41c80000

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mIntroSpinAngleLeftDegrees:F

    const/high16 v0, -0x3e100000

    iput v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mYawAngleDegrees:F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->enforceBounds()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->onAutoRotationStateChanged()V

    return-void
.end method

.method public toggleAutoSpin()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->mAutoSpin:Z

    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->onAutoRotationStateChanged()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
