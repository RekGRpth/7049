.class public Lcom/google/android/gms/panorama/viewer/ThrowController$ThrowEvent;
.super Ljava/lang/Object;
.source "ThrowController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/panorama/viewer/ThrowController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThrowEvent"
.end annotation


# instance fields
.field public final from:Landroid/graphics/PointF;

.field public final startTime:J

.field public final throwVector:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 2
    .param p1    # Landroid/graphics/PointF;
    .param p2    # Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/ThrowController$ThrowEvent;->from:Landroid/graphics/PointF;

    iput-object p2, p0, Lcom/google/android/gms/panorama/viewer/ThrowController$ThrowEvent;->throwVector:Landroid/graphics/PointF;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/panorama/viewer/ThrowController$ThrowEvent;->startTime:J

    return-void
.end method
