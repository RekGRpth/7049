.class public Lcom/google/android/gms/panorama/viewer/Tile;
.super Ljava/lang/Object;
.source "Tile.java"


# instance fields
.field public final bitmap:Landroid/graphics/Bitmap;

.field public final height:I

.field public final width:I

.field public final x:I

.field public final y:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;IIII)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/panorama/viewer/Tile;->bitmap:Landroid/graphics/Bitmap;

    iput p2, p0, Lcom/google/android/gms/panorama/viewer/Tile;->x:I

    iput p3, p0, Lcom/google/android/gms/panorama/viewer/Tile;->y:I

    iput p4, p0, Lcom/google/android/gms/panorama/viewer/Tile;->width:I

    iput p5, p0, Lcom/google/android/gms/panorama/viewer/Tile;->height:I

    return-void
.end method
