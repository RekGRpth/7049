.class public Lcom/google/android/gms/plus/BadNameActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "BadNameActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private mAgreeToReview:Landroid/widget/CheckBox;

.field private mBackButton:Landroid/widget/Button;

.field private mNextButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    return-void
.end method

.method private initViews()V
    .locals 8

    const v7, 0x7f0b00cc

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    const v3, 0x7f0a001b

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0a001a

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mBackButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0a0018

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    const v3, 0x7f0a0016

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/BadNameActivity;->shouldDisplayLastNameFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v7, v3}, Lcom/google/android/gms/plus/BadNameActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v3, 0x7f0a0017

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0a0006

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gms/auth/login/Status;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_INVALID_CHAR:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v4, :cond_3

    const v3, 0x7f0b00ce

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_1
    const v3, 0x7f0b00cb

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    :goto_2
    return-void

    :cond_1
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v7, v3}, Lcom/google/android/gms/plus/BadNameActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v3, 0x7f0b00cd

    const v4, 0x7f0b00de

    const v5, 0x7f0b00dd

    invoke-static {p0, v0, v3, v4, v5}, Lcom/google/android/gms/plus/LearnMoreActivity;->linkifyAndSetText(Lcom/google/android/gms/auth/login/BaseActivity;Landroid/widget/TextView;III)V

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    const v4, 0x7f0b00d3

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_NICKNAME:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v4, :cond_4

    const v3, 0x7f0b00cf

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v4, Lcom/google/android/gms/auth/login/Status;->GPLUS_OTHER:Lcom/google/android/gms/auth/login/Status;

    if-ne v3, v4, :cond_0

    const v3, 0x7f0b00d0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/BadNameActivity;->mBackButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/BadNameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/BadNameActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/BadNameActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mError:Lcom/google/android/gms/auth/login/Status;

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gms/auth/login/Status;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/BadNameActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/GLSSession;->setGooglePlusSelected(Z)V

    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/BadNameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/BadNameActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/BadNameActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/BadNameActivity;->initViews()V

    return-void
.end method
