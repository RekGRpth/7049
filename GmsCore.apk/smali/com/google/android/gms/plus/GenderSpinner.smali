.class public Lcom/google/android/gms/plus/GenderSpinner;
.super Landroid/widget/Spinner;
.source "GenderSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public setOnSetSelectionListener(Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/GenderSpinner;->mListener:Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/GenderSpinner;->mListener:Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/GenderSpinner;->mListener:Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;->onSetSelection(Lcom/google/android/gms/plus/GenderSpinner;I)V

    :cond_0
    return-void
.end method
