.class Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;
.super Landroid/app/AlertDialog;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/LinkSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WebViewDialog"
.end annotation


# instance fields
.field private mHelper:Lcom/google/android/common/GoogleWebContentHelper;

.field private final mLinkSpan:Lcom/google/android/gms/plus/LinkSpan;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/auth/login/BaseActivity;Lcom/google/android/gms/plus/LinkSpan$AndroidPolicy;Lcom/google/android/gms/plus/LinkSpan;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/auth/login/BaseActivity;
    .param p2    # Lcom/google/android/gms/plus/LinkSpan$AndroidPolicy;
    .param p3    # Lcom/google/android/gms/plus/LinkSpan;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mLinkSpan:Lcom/google/android/gms/plus/LinkSpan;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/google/android/common/GoogleWebContentHelper;

    invoke-direct {v1, p1}, Lcom/google/android/common/GoogleWebContentHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    iget-object v1, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {p2, v0, p1}, Lcom/google/android/gms/plus/LinkSpan$AndroidPolicy;->getSecureUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, p1}, Lcom/google/android/gms/plus/LinkSpan$AndroidPolicy;->getPrettyUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/common/GoogleWebContentHelper;->setUrls(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/auth/login/Compat;->isWifiOnlyBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b00eb

    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/login/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/common/GoogleWebContentHelper;->setUnsuccessfulMessage(Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/common/GoogleWebContentHelper;->loadUrl()Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/LinkSpan$AndroidPolicy;->getTitleResId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->setTitle(I)V

    const/4 v1, -0x1

    const v2, 0x7f0b00e0

    invoke-virtual {p1, v2}, Lcom/google/android/gms/auth/login/BaseActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog$1;

    invoke-direct {v3, p0}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog$1;-><init>(Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;)V

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v1, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog$2;-><init>(Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v1}, Lcom/google/android/common/GoogleWebContentHelper;->getLayout()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->setView(Landroid/view/View;)V

    return-void

    :cond_0
    const v1, 0x7f0b00ea

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;)Lcom/google/android/gms/plus/LinkSpan;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;

    iget-object v0, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mLinkSpan:Lcom/google/android/gms/plus/LinkSpan;

    return-object v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/gms/plus/LinkSpan$WebViewDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/common/GoogleWebContentHelper;->handleKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
