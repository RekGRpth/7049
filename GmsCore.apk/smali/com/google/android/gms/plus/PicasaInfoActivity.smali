.class public Lcom/google/android/gms/plus/PicasaInfoActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "PicasaInfoActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f04000b

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/PicasaInfoActivity;->setContentView(I)V

    const v2, 0x7f0a001a

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/PicasaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/PicasaInfoActivity;->setBackButton(Landroid/view/View;)V

    const v2, 0x7f0a001b

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/PicasaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2, v6}, Lcom/google/android/gms/plus/PicasaInfoActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v2, 0x7f0a0017

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/PicasaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/plus/PicasaInfoActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v2, Lcom/google/android/gms/auth/login/GLSSession;->mPicasaUser:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/PicasaInfoActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    :cond_0
    const v2, 0x7f0b00d8

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/plus/PicasaInfoActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v5, v5, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    aput-object v5, v3, v4

    iget-object v4, p0, Lcom/google/android/gms/plus/PicasaInfoActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x2

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/plus/PicasaInfoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public start()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->start()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/PicasaInfoActivity;->finish(I)V

    return-void
.end method
