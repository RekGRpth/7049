.class Lcom/google/android/gms/plus/UnifiedSetupActivity$1;
.super Landroid/os/Handler;
.source "UnifiedSetupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/UnifiedSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Task reply has come. Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # invokes: Lcom/google/android/gms/plus/UnifiedSetupActivity;->onAuthTokenTaskReply(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$000(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # invokes: Lcom/google/android/gms/plus/UnifiedSetupActivity;->onNameCheckTaskReply(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$100(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # invokes: Lcom/google/android/gms/plus/UnifiedSetupActivity;->onProfileTaskReply(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$200(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
