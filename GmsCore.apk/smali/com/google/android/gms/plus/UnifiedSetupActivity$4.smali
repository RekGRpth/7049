.class Lcom/google/android/gms/plus/UnifiedSetupActivity$4;
.super Lcom/google/android/gms/auth/login/CancelableCallbackThread;
.source "UnifiedSetupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/plus/UnifiedSetupActivity;->startProfileCreationTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p2    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0, p2}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;-><init>(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method protected runInBackground()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v2, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # getter for: Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;
    invoke-static {v3}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$800(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSUser;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # getter for: Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;
    invoke-static {v4}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$700(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/auth/login/GLSUser;->createProfile(Lcom/google/android/gms/auth/login/GLSSession;I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/login/Status;->fromJSON(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    if-eq v1, v3, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->GPLUS_PROFILE_ERROR:Lcom/google/android/gms/auth/login/Status;

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/login/Status;->toMessage(Landroid/os/Message;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->mIsCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    const-wide/16 v4, 0x3e8

    # invokes: Lcom/google/android/gms/plus/UnifiedSetupActivity;->ensureBackgroundTaskDelay(J)V
    invoke-static {v3, v4, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$600(Lcom/google/android/gms/plus/UnifiedSetupActivity;J)V

    :cond_1
    return-void
.end method
