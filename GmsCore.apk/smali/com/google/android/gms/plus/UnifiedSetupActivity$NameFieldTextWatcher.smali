.class Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;
.super Ljava/lang/Object;
.source "UnifiedSetupActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/UnifiedSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NameFieldTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/widget/EditText;)V
    .locals 0
    .param p2    # Landroid/widget/EditText;

    iput-object p1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->updateWidgetState()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->updateWidgetState()V

    return-void
.end method
