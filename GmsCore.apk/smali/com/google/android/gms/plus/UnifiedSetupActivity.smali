.class public Lcom/google/android/gms/plus/UnifiedSetupActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "UnifiedSetupActivity.java"

# interfaces
.implements Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/UnifiedSetupActivity$7;,
        Lcom/google/android/gms/plus/UnifiedSetupActivity$ProgressDialogFragment;,
        Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;
    }
.end annotation


# instance fields
.field private mAgreeToPersonalizedContentCheckBox:Landroid/widget/CheckBox;

.field private mBackButton:Landroid/view/View;

.field private mDialogFragment:Landroid/support/v4/app/DialogFragment;

.field private mFirstNameEdit:Landroid/widget/EditText;

.field private mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/plus/GenderAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

.field private final mHandler:Landroid/os/Handler;

.field private mLastNameEdit:Landroid/widget/EditText;

.field private mNameCheckFinished:Z

.field private mNameCheckRequested:Z

.field private mNameContainer:Landroid/view/ViewGroup;

.field private mNextButton:Landroid/widget/Button;

.field private mOkToShowRetryScreen:Z

.field private mPicasaCheckFinished:Z

.field private mProfileCreationRequested:Z

.field private mSelectedGender:Ljava/lang/Integer;

.field private mStartTime:J

.field private mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

.field private mTermsOfServiceText:Landroid/widget/TextView;

.field private mTokenIsReady:Z

.field private mUnifiedSetupMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity$1;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onAuthTokenTaskReply(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onNameCheckTaskReply(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onProfileTaskReply(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSUser;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gms/plus/UnifiedSetupActivity;J)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->ensureBackgroundTaskDelay(J)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSUser;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/gms/plus/UnifiedSetupActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->dismissDialog()V

    return-void
.end method

.method private cancelTaskThread()V
    .locals 3

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelTaskThread() requested. thread:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    :cond_1
    return-void
.end method

.method private checkMobileTerms()V
    .locals 2

    const/16 v1, 0x8

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldOnlyDisplayMobileTOS()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b00ed

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mUnifiedSetupMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/GenderSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mAgreeToPersonalizedContentCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    const v1, 0x7f0b00f2

    invoke-static {p0, v1}, Lcom/google/android/gms/plus/LinkSpan;->linkify(Lcom/google/android/gms/auth/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f0b00f4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    const v1, 0x7f0b00f0

    invoke-static {p0, v1}, Lcom/google/android/gms/plus/LinkSpan;->linkify(Lcom/google/android/gms/auth/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private dismissDialog()V
    .locals 3

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismissDialog() requested. dialog:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getActivityContentView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private ensureBackgroundTaskDelay(J)V
    .locals 7
    .param p1    # J

    iget-wide v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mStartTime:J

    add-long v3, v5, p1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v5, 0x0

    cmp-long v5, p1, v5

    if-lez v5, :cond_0

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    sub-long v5, v3, v1

    :try_start_0
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private initViews()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v5, 0x7f0a0047

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mUnifiedSetupMessage:Landroid/widget/TextView;

    const v5, 0x7f0a001b

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {p0, v5, v9}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v5, 0x7f0a001a

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mBackButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->setBackButton(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    if-nez v5, :cond_1

    :cond_0
    const v5, 0x7f0a0048

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldDisplayLastNameFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040008

    iget-object v7, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0a0028

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0a0029

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    new-instance v6, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;

    iget-object v7, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-direct {v6, p0, v7}, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/widget/EditText;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    new-instance v6, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;

    iget-object v7, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-direct {v6, p0, v7}, Lcom/google/android/gms/plus/UnifiedSetupActivity$NameFieldTextWatcher;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/widget/EditText;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v5, 0x7f0a0049

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/plus/GenderSpinner;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    const/high16 v5, 0x7f0e0000

    const v6, 0x1090008

    invoke-static {p0, v5, v6}, Lcom/google/android/gms/plus/GenderAdapter;->createFromResource(Landroid/content/Context;II)Lcom/google/android/gms/plus/GenderAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    const v6, 0x1090009

    invoke-virtual {v5, v6}, Lcom/google/android/gms/plus/GenderAdapter;->setDropDownViewResource(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    iget-object v6, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/plus/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    invoke-virtual {v5, p0}, Lcom/google/android/gms/plus/GenderSpinner;->setOnSetSelectionListener(Lcom/google/android/gms/plus/GenderSpinner$OnSetSelectionListener;)V

    const v5, 0x7f0a004a

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mAgreeToPersonalizedContentCheckBox:Landroid/widget/CheckBox;

    const v5, 0x7f0a004b

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    const v6, 0x7f0b00f1

    invoke-static {p0, v6}, Lcom/google/android/gms/plus/LinkSpan;->linkify(Lcom/google/android/gms/auth/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v5, 0x7f0a004c

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-boolean v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mOkToShowRetryScreen:Z

    if-eqz v5, :cond_5

    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->checkMobileTerms()V

    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040007

    iget-object v7, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTermsOfServiceText:Landroid/widget/TextView;

    const v6, 0x7f0b00ef

    invoke-static {p0, v6}, Lcom/google/android/gms/plus/LinkSpan;->linkify(Lcom/google/android/gms/auth/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    sget-object v6, Lcom/google/android/gms/auth/login/ResponseKey;->SERVICE_HOSTED:Lcom/google/android/gms/auth/login/ResponseKey;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/login/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/auth/login/GLSUser;->hasService(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v5, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v3, :cond_6

    const/16 v5, 0x40

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_6

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :cond_6
    if-eqz v2, :cond_2

    const v5, 0x7f0b00d6

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private onAuthTokenTaskReply(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const-string v1, "PlusActivity"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PlusActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAuthTokenTaskReply(). msg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->cancelTaskThread()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->dismissDialog()V

    invoke-static {p1}, Lcom/google/android/gms/auth/login/Status;->fromMessage(Landroid/os/Message;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/UnifiedSetupActivity$7;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v1, "PlusActivity"

    const-string v2, "Failed to obtain auth token. Exiting."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->finish()V

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->checkMobileTerms()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private onNameCheckSuccess()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckFinished:Z

    iget-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mPicasaCheckFinished:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mPicasaUser:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startProfileCreationTask()V

    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mOkToShowRetryScreen:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/PicasaInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x405

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private onNameCheckTaskReply(Landroid/os/Message;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->cancelTaskThread()V

    invoke-static {p1}, Lcom/google/android/gms/auth/login/Status;->fromMessage(Landroid/os/Message;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/UnifiedSetupActivity$7;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mOkToShowRetryScreen:Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/BadNameActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x407

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onNameCheckSuccess()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private onProfileTaskReply(Landroid/os/Message;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->cancelTaskThread()V

    invoke-static {p1}, Lcom/google/android/gms/auth/login/Status;->fromMessage(Landroid/os/Message;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/UnifiedSetupActivity$7;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x40a

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->dismissDialog()V

    return-void

    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->finish(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private shouldEnableNext()Z
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldOnlyDisplayMobileTOS()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v2

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/GenderAdapter;->isHintShowing()Z

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    :goto_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    if-nez v2, :cond_0

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1
.end method

.method private shouldOnlyDisplayMobileTOS()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSSession;->hasGooglePlus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSSession;->hasESMobile()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDialog(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showDialog() requested. dialog:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/plus/UnifiedSetupActivity$ProgressDialogFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/gms/plus/UnifiedSetupActivity$ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mDialogFragment:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "sending"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showLoadingDialog()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getActivityContentView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0b00f5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showDialog(Ljava/lang/String;)V

    return-void
.end method

.method private showSendingDialog()V
    .locals 1

    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showDialog(Ljava/lang/String;)V

    return-void
.end method

.method private startAuthTokenTask()V
    .locals 3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mStartTime:J

    new-instance v0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->start()V

    return-void
.end method

.method private startNameCheckTask()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mStartTime:J

    new-instance v0, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity$3;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->start()V

    return-void
.end method

.method private startProfileCreationTask()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mStartTime:J

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/GLSSession;->setAgreedToMobileTOS(Z)V

    new-instance v0, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity$4;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTaskThread:Lcom/google/android/gms/auth/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;->start()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UnifiedSetupActivity#onActivityResult(). requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v0, 0x405

    if-ne p1, v0, :cond_2

    if-ne p2, v3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mPicasaCheckFinished:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startProfileCreationTask()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/plus/UnifiedSetupActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity$5;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    const/16 v0, 0x407

    if-ne p1, v0, :cond_5

    if-ne p2, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSSession;->isGooglePlusSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onNameCheckSuccess()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->finish(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->initViews()V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/plus/UnifiedSetupActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity$6;-><init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_5
    const/16 v0, 0x40a

    if-ne p1, v0, :cond_6

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->finish(I)V

    goto :goto_0

    :cond_6
    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->initViews()V

    return-void
.end method

.method protected onPause()V
    .locals 3

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause(). mTokenIsReady: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->cancelTaskThread()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->dismissDialog()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "tokenIsReady"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    const-string v0, "okToShowRetryScreen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mOkToShowRetryScreen:Z

    const-string v0, "nameCheckRequested"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    const-string v0, "nameCheckFinished"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckFinished:Z

    const-string v0, "picasaCheckFinished"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mPicasaCheckFinished:Z

    const-string v0, "profileCreationRequested"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    const-string v0, "selectedGender"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "selectedGender"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    const-string v0, "PlusActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume(). mTokenIsReady: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mNameCheckRequested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfileCreationRequested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/GenderSpinner;->setSelection(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldEnableNext()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showLoadingDialog()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startAuthTokenTask()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/GenderSpinner;->setSelection(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/GenderAdapter;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showSendingDialog()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startNameCheckTask()V

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showSendingDialog()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startProfileCreationTask()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "tokenIsReady"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mTokenIsReady:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "okToShowRetryScreen"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mOkToShowRetryScreen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "nameCheckRequested"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckRequested:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "nameCheckFinished"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckFinished:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "picasaCheckFinished"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mPicasaCheckFinished:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "profileCreationRequested"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mProfileCreationRequested:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "selectedGender"

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onSetSelection(Lcom/google/android/gms/plus/GenderSpinner;I)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/GenderSpinner;
    .param p2    # I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSelectedGender:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderAdapter:Lcom/google/android/gms/plus/GenderAdapter;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/GenderAdapter;->hideHint()V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldEnableNext()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public start()V
    .locals 6

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->start()V

    iget-object v4, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/auth/login/GLSSession;->setNameActivityCompleted(Z)V

    iget-object v4, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/auth/login/GLSSession;->setTermsOfServiceShown(Z)V

    iget-object v4, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGenderSpinner:Lcom/google/android/gms/plus/GenderSpinner;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/GenderSpinner;->getSelectedItemPosition()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/auth/login/GLSSession;->mGender:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v5, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mAgreeToPersonalizedContentCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_0
    invoke-virtual {v4, v3}, Lcom/google/android/gms/auth/login/GLSSession;->setAgreedToPersonalizedContent(Z)V

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNameCheckFinished:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v1, v3, Lcom/google/android/gms/auth/login/GLSSession;->mFirstName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iput-object v2, v3, Lcom/google/android/gms/auth/login/GLSSession;->mLastName:Ljava/lang/String;

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->showSendingDialog()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->startNameCheckTask()V

    :goto_2
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->onNameCheckSuccess()V

    goto :goto_2
.end method

.method public updateWidgetState()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/auth/login/BaseActivity;->updateWidgetState()V

    iget-object v0, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity;->mNextButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->shouldEnableNext()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
