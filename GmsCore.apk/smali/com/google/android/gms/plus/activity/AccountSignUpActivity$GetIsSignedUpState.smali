.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;
.source "AccountSignUpActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetIsSignedUpState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1000(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    new-instance v1, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/common/internal/GmsClient;->GOOGLE_PLUS_REQUIRED_FEATURES:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method public requiresRestartOnDisconnect()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const v5, 0x7f0b0027

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v4, 0x7f0b0027

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # setter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;
    invoke-static {v2, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1002(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V
    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$600(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const v4, 0x7f0b0080

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
