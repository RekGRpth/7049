.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;
.source "AccountSignUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetDefaultAccountState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1500(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$500(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->setDefaultAccountName(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1502(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Z)Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState$1;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
