.class abstract Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;
.super Ljava/lang/Object;
.source "AccountSignUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "State"
.end annotation


# instance fields
.field isStarted:Z

.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public abstract isFinished()Z
.end method

.method public abstract onStart()V
.end method

.method public abstract requiresRestartOnDisconnect()Z
.end method

.method public start()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(Z)V

    iput-boolean v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->onStart()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
