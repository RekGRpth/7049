.class public Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.super Landroid/app/Activity;
.source "AccountSignUpActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowClientAuthActivityState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowGmsAuthActivityState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SubActivityState;,
        Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;

.field private mAuthPackageName:Ljava/lang/String;

.field private mCallingPackageName:Ljava/lang/String;

.field private mClientAuthTokenIntent:Landroid/app/PendingIntent;

.field private mGmsAuthTokenIntent:Landroid/app/PendingIntent;

.field private mIsSignedUp:Ljava/lang/Boolean;

.field private mScopeString:Ljava/lang/String;

.field private mShouldSetDefaultAccount:Z

.field private mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

.field private mVisibleActions:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ResolveDefaultAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$PickAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowGmsAuthActivityState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowGmsAuthActivityState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetIsSignedUpState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$GetAuthTokenIntentState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowClientAuthActivityState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$ShowClientAuthActivityState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SetDefaultAccountState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->showSpinner()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->displayErrorAndFinish(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Landroid/app/PendingIntent;

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/AccountSignUpClient;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mGmsAuthTokenIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private deserializeFromBundle(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const-string v0, "com.google.android.gms.plus.intent.extra.CALLING_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.plus.intent.extra.CALLING_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    :cond_0
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.intent.extra.IS_SIGNED_UP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.google.android.gms.plus.intent.extra.IS_SIGNED_UP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    :goto_1
    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mVisibleActions:[Ljava/lang/String;

    const-string v0, "request_visible_actions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "request_visible_actions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mVisibleActions:[Ljava/lang/String;

    :cond_1
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private displayErrorAndFinish(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    return-void
.end method

.method private getStateIndex()I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v0, v1

    goto :goto_1
.end method

.method private handleClientAuthResult(ILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method private handleGmsAuthResult(ILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mGmsAuthTokenIntent:Landroid/app/PendingIntent;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method private handlePickAccountResult(ILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const-string v0, "authAccount"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->logResolutionIntentInvoked(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method private handleSignUpResult(ILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method private hideSpinner()V
    .locals 2

    const v0, 0x7f0a004d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private static logResolutionIntentInvoked(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/plus/analytics/SignIn$Action;->RESOLUTION_INTENT_INVOKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/plus/analytics/SignIn$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    sget-object v0, Lcom/google/android/gms/plus/analytics/SignIn$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/plus/analytics/SignIn$View;->RESOLUTION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method private proceedWithGetTokenFlow()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getStateIndex()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v0, v2, v1

    iget-boolean v2, v0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method public static serializeToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 2
    .param p0    # Landroid/os/Bundle;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Boolean;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # Landroid/app/PendingIntent;

    const-string v0, "authAccount"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.CALLING_PACKAGE_NAME"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    const-string v0, "com.google.android.gms.plus.intent.extra.IS_SIGNED_UP"

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    if-eqz p6, :cond_1

    const-string v0, "request_visible_actions"

    invoke-virtual {p0, v0, p6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_1
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {p0, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {p0, v0, p7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method private showSpinner()V
    .locals 2

    const v0, 0x7f0a004d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->handlePickAccountResult(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->handleGmsAuthResult(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->handleSignUpResult(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->handleClientAuthResult(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onConnected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz p1, :cond_3

    move-object v0, p1

    const-string v4, "stateIndex"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v4, v4

    if-ge v3, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->requiresRestartOnDisconnect()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v4, v4, v3

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    :cond_0
    const-string v4, "shouldSetDefaultAccount"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->deserializeFromBundle(Landroid/os/Bundle;)V

    if-nez p1, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->logResolutionIntentInvoked(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    new-instance v4, Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mVisibleActions:[Ljava/lang/String;

    invoke-direct {v4, p0, p0, p0, v5}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    const v4, 0x7f040013

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->proceedWithGetTokenFlow()V

    return-void

    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/common/util/AndroidUtils;->getCallingPackage(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/SecurityException;

    const-string v5, "Must be started via startActivityForResult"

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.google.android.gms.plus.intent.extra.CALLING_PACKAGE_NAME"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_6

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_7
    new-instance v4, Ljava/lang/SecurityException;

    const-string v5, "Calling/Auth package may only be set by GmsCore"

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public onDisconnected()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getStateIndex()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->requiresRestartOnDisconnect()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mCallingPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAuthPackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mScopeString:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mVisibleActions:[Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mClientAuthTokenIntent:Landroid/app/PendingIntent;

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->serializeToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getStateIndex()I

    move-result v8

    const-string v0, "stateIndex"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    array-length v0, v0

    if-ge v8, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mStates:[Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;

    aget-object v0, v0, v8

    iget-boolean v0, v0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->isStarted:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(Z)V

    const-string v0, "shouldSetDefaultAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mShouldSetDefaultAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->connect()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->disconnect()V

    return-void
.end method
