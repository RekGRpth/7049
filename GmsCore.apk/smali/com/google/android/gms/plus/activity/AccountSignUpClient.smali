.class Lcom/google/android/gms/plus/activity/AccountSignUpClient;
.super Lcom/google/android/gms/common/internal/GmsClient;
.source "AccountSignUpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mHandler:Landroid/os/Handler;

.field private final mVisibleActions:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # [Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/internal/GmsClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->mHandler:Landroid/os/Handler;

    iput-object p4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->mVisibleActions:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge synthetic createServiceInterface(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;

    move-result-object v0

    return-object v0
.end method

.method protected createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/IPlusService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;

    move-result-object v0

    return-object v0
.end method

.method public getAuthIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getService()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/IPlusService;

    new-instance v1, Lcom/google/android/gms/plus/activity/AccountSignUpClient$2;

    invoke-direct {v1, p0, p5}, Lcom/google/android/gms/plus/activity/AccountSignUpClient$2;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpClient;Lcom/google/android/gms/plus/activity/AccountSignUpClient$AuthIntentCallback;)V

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/IPlusService;->getAuthIntentInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v6

    sget-object v0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->TAG:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getDefaultAccountName(Ljava/lang/String;Lcom/google/android/gms/common/util/Function;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/util/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/IPlusService;

    new-instance v2, Lcom/google/android/gms/plus/activity/AccountSignUpClient$1;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/plus/activity/AccountSignUpClient$1;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpClient;Lcom/google/android/gms/common/util/Function;)V

    invoke-interface {v1, v2, p1}, Lcom/google/android/gms/plus/internal/IPlusService;->getDefaultAccountNameInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getServiceDescriptor()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method

.method protected getServiceFromBroker(Lcom/google/android/gms/common/internal/IGmsServiceBroker;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/common/internal/IGmsServiceBroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/IGmsServiceBroker;",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<",
            "Lcom/google/android/gms/plus/internal/IPlusService;",
            ">.GmsCallbacks;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "skip_oob"

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "request_visible_actions"

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->mVisibleActions:[Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const v2, 0x2e309c

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getScopes()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v4, v3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/common/internal/IGmsServiceBroker;->getPlusService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected getStartServiceAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method public setDefaultAccountName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v1, p1, p2}, Lcom/google/android/gms/plus/internal/IPlusService;->setDefaultAccountNameInternal(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
