.class public Lcom/google/android/gms/plus/activity/WriteMomentActivity;
.super Landroid/app/Activity;
.source "WriteMomentActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# instance fields
.field private mConfirmationText:Landroid/widget/TextView;

.field private mGPlusLogo:Landroid/widget/ImageView;

.field private mIsResumed:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mMomentJson:Ljava/lang/String;

.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mStatus:Lcom/google/android/gms/common/ConnectionResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private showConfirmationMessageAndFinish()V
    .locals 4

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mConfirmationText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mGPlusLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/gms/plus/activity/WriteMomentActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity$1;-><init>(Lcom/google/android/gms/plus/activity/WriteMomentActivity;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/google/android/gms/plus/activity/WriteMomentActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity$2;-><init>(Lcom/google/android/gms/plus/activity/WriteMomentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x2328

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    const-string v0, "WriteMoment"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    goto :goto_0
.end method

.method public onConnected()V
    .locals 5

    new-instance v1, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    invoke-direct {v1}, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;-><init>()V

    new-instance v2, Lcom/google/android/gms/common/server/response/FastParser;

    invoke-direct {v2}, Lcom/google/android/gms/common/server/response/FastParser;-><init>()V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mMomentJson:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/server/response/FastParser;->parse(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/plus/PlusClient;->writeMoment(Lcom/google/android/gms/plus/model/moments/Moment;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->showConfirmationMessageAndFinish()V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/FastParser$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "WriteMoment"

    const-string v4, "Unable to recover from a connection failure."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mIsResumed:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x2328

    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WriteMoment"

    const-string v2, "Unable to recover from a connection failure."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/gms/common/util/AndroidUtils;->getCallingPackage(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v0, "WriteMoment"

    const-string v1, "WriteMomentActivity must started via startActivityForResult"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v0, "com.google.android.gms.EXTRA_MOMENT_JSON"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mMomentJson:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mMomentJson:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "WriteMoment"

    const-string v1, "Moment JSON must be provided as an extra, with key \"com.google.android.gms.EXTRA_MOMENT_JSON\""

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    goto :goto_0

    :cond_1
    const-string v0, "authAccount"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v0, "WriteMoment"

    const-string v1, "No account name was specified; using default account"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "<<default account>>"

    :cond_2
    const v0, 0x7f040033

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setContentView(I)V

    const v0, 0x7f0a009d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mLayout:Landroid/widget/RelativeLayout;

    const v0, 0x7f0a0045

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f0a004e

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mConfirmationText:Landroid/widget/TextView;

    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mGPlusLogo:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const-string v1, "https://www.googleapis.com/auth/plus.login"

    aput-object v1, v6, v4

    move-object v1, p0

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mIsResumed:Z

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mIsResumed:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0x2328

    invoke-virtual {v1, p0, v2}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WriteMoment"

    const-string v2, "Unable to recover from a connection failure."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->finish()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/WriteMomentActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    return-void
.end method
