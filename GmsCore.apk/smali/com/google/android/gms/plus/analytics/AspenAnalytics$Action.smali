.class public final Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;
.super Ljava/lang/Object;
.source "AspenAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/analytics/AspenAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final DISCONNECT_APP_AND_DELETE_FRAMES_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final DISCONNECT_APP_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final DISCONNECT_APP_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final MANAGE_APPS_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final SHOW_MANAGE_APPS_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x15

    # invokes: Lcom/google/android/gms/plus/analytics/AspenAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/AspenAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->MANAGE_APPS_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x16

    # invokes: Lcom/google/android/gms/plus/analytics/AspenAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/AspenAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->SHOW_MANAGE_APPS_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x17

    # invokes: Lcom/google/android/gms/plus/analytics/AspenAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/AspenAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x18

    # invokes: Lcom/google/android/gms/plus/analytics/AspenAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/AspenAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_AND_DELETE_FRAMES_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x19

    # invokes: Lcom/google/android/gms/plus/analytics/AspenAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/AspenAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
