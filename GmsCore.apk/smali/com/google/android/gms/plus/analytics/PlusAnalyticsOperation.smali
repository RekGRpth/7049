.class public final Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;
.super Ljava/lang/Object;
.source "PlusAnalyticsOperation.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    return-void
.end method

.method private static createFavaDiagnostics(Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;I)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;
    .locals 4
    .param p0    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .param p2    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .param p3    # I

    new-instance v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v1

    const-string v2, "totalTimeMs"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p0, :cond_0

    const-string v1, "actionType"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    :cond_0
    const-string v1, "startView"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p2, :cond_1

    const-string v1, "endView"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    :cond_1
    return-object v0
.end method

.method static toClientActionData(Ljava/util/ArrayList;)Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;)",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;"
        }
    .end annotation

    const/4 v5, 0x0

    if-nez p0, :cond_0

    move-object v0, v5

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;

    new-instance v6, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircle;

    invoke-virtual {v3}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;->getType()Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v7, v5, v8}, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircle;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const-string v5, "circle"

    invoke-virtual {v0, v5, v1}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method static toClientOzEvent(Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;ILjava/util/ArrayList;)Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p2    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p3    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            "Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;)",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toNamespacedType(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    move-result-object v4

    invoke-static {p2}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toNamespacedType(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    move-result-object v5

    invoke-static {p3}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toNamespacedType(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    move-result-object v6

    invoke-static {v4, v5, v6, p4}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->createFavaDiagnostics(Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;I)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;-><init>()V

    const-string v4, "favaDiagnostics"

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v4

    const-string v5, "isNativePlatformEvent"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toClientActionData(Ljava/util/ArrayList;)Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;-><init>()V

    const-string v4, "ozEvent"

    invoke-virtual {v1, v4, v3}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz v0, :cond_0

    const-string v4, "actionData"

    invoke-virtual {v1, v4, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v4

    const-string v5, "clientTimeMsec"

    invoke-static {}, Lcom/google/android/gms/common/util/DefaultClock;->getInstance()Lcom/google/android/gms/common/util/Clock;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method private static toContentValues(Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;)Landroid/content/ContentValues;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "payload"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "timestamp"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->getClientTimeMsec()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method private static toNamespacedType(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .locals 4
    .param p0    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    iget-object v2, p0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;->namespace:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;->typeNum:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getAccountName()Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getAction()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getStartView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getEndView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v3

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getDuration()I

    move-result v4

    iget-object v8, p0, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->mDataBuilder:Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getLoggedCircles()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toClientOzEvent(Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;ILjava/util/ArrayList;)Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    move-result-object v6

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->TAG:Ljava/lang/String;

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->TAG:Ljava/lang/String;

    const-string v9, "Processing task %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {v0, v6}, Lcom/google/android/gms/plus/analytics/PlusAnalyticsOperation;->toContentValues(Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;)Landroid/content/ContentValues;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/google/android/gms/plus/internal/PlusContent$Analytics;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    return-void
.end method
