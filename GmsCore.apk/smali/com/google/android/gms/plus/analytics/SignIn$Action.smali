.class public final Lcom/google/android/gms/plus/analytics/SignIn$Action;
.super Ljava/lang/Object;
.source "SignIn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/analytics/SignIn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final GET_ACCOUNT:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final GET_OAUTH_TOKEN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final RESOLUTION_INTENT_INVOKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$Action;->RESOLUTION_INTENT_INVOKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$Action;->GET_ACCOUNT:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x3

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$Action;->GET_OAUTH_TOKEN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
