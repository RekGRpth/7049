.class public final Lcom/google/android/gms/plus/analytics/SignIn$View;
.super Ljava/lang/Object;
.source "SignIn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/analytics/SignIn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "View"
.end annotation


# static fields
.field public static final RESOLUTION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final UNKNOWN_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$View;->UNKNOWN_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x1

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/gms/plus/analytics/SignIn;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/SignIn;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/SignIn$View;->RESOLUTION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
