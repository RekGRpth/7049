.class public abstract Lcom/google/android/gms/plus/apps/DataBufferLoader;
.super Landroid/support/v4/content/Loader;
.source "DataBufferLoader.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DataBufferType:",
        "Lcom/google/android/gms/common/data/DataBuffer",
        "<*>;ClientType::",
        "Lcom/google/android/gms/common/GooglePlayServicesClient;",
        ">",
        "Landroid/support/v4/content/Loader",
        "<TDataBufferType;>;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TClientType;"
        }
    .end annotation
.end field

.field private mConnectionResult:Lcom/google/android/gms/common/ConnectionResult;

.field private mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDataBufferType;"
        }
    .end annotation
.end field

.field private mIsConnecting:Z

.field private final mMaxCount:I

.field private mNextPageToken:Ljava/lang/String;

.field private final mPageToken:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/Account;
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/support/v4/content/Loader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mAccount:Landroid/accounts/Account;

    iput p3, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mMaxCount:I

    iput-object p4, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mPageToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected abstract createClient(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Landroid/accounts/Account;)Lcom/google/android/gms/common/GooglePlayServicesClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
            "Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;",
            "Landroid/accounts/Account;",
            ")TClientType;"
        }
    .end annotation
.end method

.method protected final deliverResult(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/data/DataBuffer;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "TDataBufferType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mConnectionResult:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p3, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mNextPageToken:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->deliverResult(Lcom/google/android/gms/common/data/DataBuffer;)V

    return-void
.end method

.method public final deliverResult(Lcom/google/android/gms/common/data/DataBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataBufferType;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->isReset()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataBuffer;->close()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-super {p0, p1}, Landroid/support/v4/content/Loader;->deliverResult(Ljava/lang/Object;)V

    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataBuffer;->close()V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->deliverResult(Lcom/google/android/gms/common/data/DataBuffer;)V

    return-void
.end method

.method public final getConnectionResult()Lcom/google/android/gms/common/ConnectionResult;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mConnectionResult:Lcom/google/android/gms/common/ConnectionResult;

    return-object v0
.end method

.method protected abstract load(Lcom/google/android/gms/common/GooglePlayServicesClient;ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TClientType;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public final onConnected()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    iget v1, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mMaxCount:I

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mPageToken:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->load(Lcom/google/android/gms/common/GooglePlayServicesClient;ILjava/lang/String;)V

    return-void
.end method

.method public final onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    invoke-virtual {p0, p1, v1, v1}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->deliverResult(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/data/DataBuffer;Ljava/lang/String;)V

    return-void
.end method

.method public final onDisconnected()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->onStartLoading()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    return-void
.end method

.method protected final onForceLoad()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/content/Loader;->onForceLoad()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/GooglePlayServicesClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    iget v1, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mMaxCount:I

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mPageToken:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->load(Lcom/google/android/gms/common/GooglePlayServicesClient;ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/GooglePlayServicesClient;->connect()V

    goto :goto_0
.end method

.method protected final onReset()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/content/Loader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->onStopLoading()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataBuffer;->close()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    return-void
.end method

.method protected final onStartLoading()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0, v0, p0, p0, v1}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->createClient(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Landroid/accounts/Account;)Lcom/google/android/gms/common/GooglePlayServicesClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->deliverResult(Lcom/google/android/gms/common/data/DataBuffer;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mDataBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DataBufferLoader;->forceLoad()V

    :cond_3
    return-void
.end method

.method protected final onStopLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/GooglePlayServicesClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mClient:Lcom/google/android/gms/common/GooglePlayServicesClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/GooglePlayServicesClient;->disconnect()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/DataBufferLoader;->mIsConnecting:Z

    :cond_2
    return-void
.end method
