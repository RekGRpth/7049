.class public Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "DisconnectSourceDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAppDisplayData:Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

.field private mApplication:Lcom/google/android/gms/plus/model/apps/Application;

.field private mDeleteAllFrames:Z

.field private mOnDisconnectSourceAcceptedListener:Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private getAlertDialogContentView()Landroid/view/View;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAppDisplayData:Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040025

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a0009

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v4, 0x7f0b003f

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v7

    aput-object v1, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0a0012

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v4, 0x7f0b0041

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v4, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-object v3
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0b003e

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;
    .locals 4
    .param p0    # Landroid/accounts/Account;
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "aspen_app"

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->from(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mOnDisconnectSourceAcceptedListener:Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceDialog has to be hosted by an Activity that implements OnDisconnectSourceAcceptedListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/AspenAnalytics$View;->DISCONNECT_APP_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v1, 0x7f0a0012

    if-ne v0, v1, :cond_0

    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mOnDisconnectSourceAcceptedListener:Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;->onDisconnectSourceAccepted(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_AND_DELETE_FRAMES_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/AspenAnalytics$View;->DISCONNECT_APP_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->dismiss()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_CLICKED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/AspenAnalytics$View;->DISCONNECT_APP_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->DISCONNECT_APP_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/AspenAnalytics$View;->DISCONNECT_APP_DIALOG:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAccount:Landroid/accounts/Account;

    const-string v1, "aspen_app"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/model/apps/Application;

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    if-eqz p1, :cond_0

    const-string v1, "delete_all_frames"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/AppDisplayCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mAppDisplayData:Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0043

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->getAlertDialogContentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mOnDisconnectSourceAcceptedListener:Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "delete_all_frames"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->mDeleteAllFrames:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
