.class public Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "DisconnectSourceFailedDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mApplication:Lcom/google/android/gms/plus/model/apps/Application;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;
    .locals 4
    .param p0    # Lcom/google/android/gms/plus/model/apps/Application;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "application"

    invoke-static {p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->from(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->dismiss()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "application"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/apps/Application;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const v1, 0x7f0b0045

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    invoke-interface {v4}, Lcom/google/android/gms/plus/model/apps/Application;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
