.class public Lcom/google/android/gms/plus/apps/ListAppsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "ListAppsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/plus/apps/DisconnectSourceDialog$OnDisconnectSourceAcceptedListener;
.implements Lcom/google/android/gms/plus/apps/DisconnectSourceFragment$DisconnectSourceCallbacks;
.implements Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;


# static fields
.field private static sHelpUri:Landroid/net/Uri;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountNames:[Ljava/lang/String;

.field private mAppDisplayCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

.field private mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

.field private mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private getAccountNames()[Ljava/lang/String;
    .locals 6

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v5, "com.google"

    invoke-virtual {v0, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    :goto_0
    new-array v4, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v5, v1, v3

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method private getHelpIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getHelpUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static getHelpUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->sHelpUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/config/G;->plusListAppsHelpUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->sHelpUri:Landroid/net/Uri;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->sHelpUri:Landroid/net/Uri;

    return-object v0
.end method

.method private handleDisconnectSourceResult(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ProgressDialogFragment;->dismiss()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->newInstance(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/apps/DisconnectSourceFailedDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->removeApplication(Lcom/google/android/gms/plus/model/apps/Application;)V

    goto :goto_0
.end method

.method private needsMenuButton()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private resetFragments()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/ListAppsFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const/16 v3, 0x1003

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0a007c

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    const-string v5, "content"

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    const-string v4, "disconnect_source"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method


# virtual methods
.method public disconnectSource(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-static {p1, p2}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->newInstance(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "disconnect_source_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/DisconnectSourceDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->resetFragments()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setError()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f0a005e

    if-ne v0, v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    new-instance v2, Lcom/google/android/gms/plus/apps/ListAppsActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity$1;-><init>(Lcom/google/android/gms/plus/apps/ListAppsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v2, 0x7f0a0058

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v8, 0x7f040028

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setContentView(I)V

    const v8, 0x7f0a0059

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0a005b

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    const v9, 0x7f0200c8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->needsMenuButton()Z

    move-result v8

    if-eqz v8, :cond_0

    const v8, 0x7f0a005e

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    if-eqz p1, :cond_3

    const/4 v3, 0x0

    const-string v8, "selected_account"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/accounts/Account;

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    :cond_1
    :goto_0
    if-eqz p1, :cond_4

    const-string v8, "account_names"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    :goto_1
    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    array-length v8, v8

    if-nez v8, :cond_5

    const v8, 0x7f0a005c

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0a005d

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    const v8, 0x7f0a0058

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/AppDisplayCache;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAppDisplayCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v8, :cond_2

    const-string v8, "disconnect_source"

    invoke-virtual {v4, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v8}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    const-string v9, "disconnect_source"

    invoke-virtual {v5, v8, v9}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_2
    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v8}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/ListAppsFragment;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    const v8, 0x7f0a007c

    iget-object v9, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    const-string v10, "content"

    invoke-virtual {v5, v8, v9, v10}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void

    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.google.android.gms.extras.ACCOUNT_NAME"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v8, Landroid/accounts/Account;

    const-string v9, "com.google"

    invoke-direct {v8, v1, v9}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    goto/16 :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getAccountNames()[Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.google.android.gms.extras.ACCOUNT_NAME"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    if-eqz v3, :cond_6

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_3
    sget-object v9, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->SHOW_MANAGE_APPS_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v10, Lcom/google/android/gms/plus/analytics/NativeAppAnalytics$View;->LEFT_NAV:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {p0, v8, v9, v10}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_6
    const v8, 0x7f0a005c

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    const v8, 0x7f0b003d

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v8, 0x7f0a005c

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0a005d

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    :cond_8
    const v8, 0x7f0a005c

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    if-nez v8, :cond_9

    new-instance v8, Landroid/accounts/Account;

    iget-object v9, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    const-string v10, "com.google"

    invoke-direct {v8, v9, v10}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    :cond_9
    if-eqz v3, :cond_a

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v9, Lcom/google/android/gms/plus/analytics/AspenAnalytics$Action;->SHOW_MANAGE_APPS_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v10, Lcom/google/android/gms/plus/analytics/AspenAnalytics$View;->ANDROID_GOOGLE_SETTINGS_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {p0, v8, v9, v10}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_a
    new-instance v0, Lcom/google/android/gms/plus/apps/AccountSpinnerAdapter;

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    invoke-direct {v0, p0, v8}, Lcom/google/android/gms/plus/apps/AccountSpinnerAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    const v8, 0x7f0a005d

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/Spinner;->setVisibility(I)V

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/plus/apps/AccountSpinnerAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto/16 :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x0

    const v0, 0x7f0b0040

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method public onDisconnectSourceAccepted(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;Z)V
    .locals 5
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p3    # Z

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mDisconnectFragment:Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;

    invoke-virtual {v2, p2, p3}, Lcom/google/android/gms/plus/apps/DisconnectSourceFragment;->disconnectApplication(Lcom/google/android/gms/plus/model/apps/Application;Z)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAppDisplayCache:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v0

    const v2, 0x7f0b0044

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ProgressDialogFragment;->newInstance(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/apps/ProgressDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/apps/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    aget-object v0, v0, p3

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    aget-object v1, v1, p3

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->resetFragments()V

    goto :goto_0
.end method

.method public onLoadError(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setEmpty()V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setError()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mContentFragment:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setError()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getHelpIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account_names"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccountNames:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "selected_account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSourceDisconnected(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->handleDisconnectSourceResult(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/Application;)V

    return-void
.end method
