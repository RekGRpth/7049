.class final Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "ListAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/apps/ListAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LinkSpan"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/apps/ListAppsFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/apps/ListAppsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;->this$0:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/apps/ListAppsFragment;Lcom/google/android/gms/plus/apps/ListAppsFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/apps/ListAppsFragment;
    .param p2    # Lcom/google/android/gms/plus/apps/ListAppsFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;-><init>(Lcom/google/android/gms/plus/apps/ListAppsFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    # invokes: Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLearnMoreUri()Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->access$000()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;->this$0:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v7}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v7, 0x10000

    invoke-virtual {v2, v1, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v7, "com.google.android.apps.plus"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;->this$0:Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-virtual {v7, v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
