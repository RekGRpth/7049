.class public Lcom/google/android/gms/plus/apps/ListAppsFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ListAppsFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;
.implements Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/ListAppsFragment$1;,
        Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;,
        Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/ListFragment;",
        "Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;",
        ">;",
        "Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static sLearnMoreUri:Landroid/net/Uri;

.field private static sMaxResults:Ljava/lang/Integer;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

.field private mHasError:Z

.field private mImageManager:Lcom/google/android/gms/plus/apps/ListAppsImageManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/net/Uri;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLearnMoreUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getLearnMoreUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sLearnMoreUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/config/G;->plusListAppsLearnMoreUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sLearnMoreUri:Landroid/net/Uri;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sLearnMoreUri:Landroid/net/Uri;

    return-object v0
.end method

.method private getLoaderArguments()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method private static getMaxResults()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sMaxResults:Ljava/lang/Integer;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/config/G;->plusListAppsMaxItems:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    sput-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sMaxResults:Ljava/lang/Integer;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->sMaxResults:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static newInstance(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/ListAppsFragment;
    .locals 3
    .param p0    # Landroid/accounts/Account;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/gms/plus/apps/ListAppsFragment;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public bridge synthetic getListAdapter()Landroid/widget/ListAdapter;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    return-object v0
.end method

.method public imageLoaded(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->updateImage(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected linkifyAndSetText(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 9

    const/4 v1, 0x0

    new-instance v2, Landroid/text/SpannableString;

    invoke-virtual {p0, p3}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v3, Landroid/text/Annotation;

    invoke-virtual {v2, v1, v0, v3}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    array-length v4, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v0, v1

    invoke-virtual {v2, v5}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v2, v5}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    new-instance v7, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/google/android/gms/plus/apps/ListAppsFragment$LinkSpan;-><init>(Lcom/google/android/gms/plus/apps/ListAppsFragment;Lcom/google/android/gms/plus/apps/ListAppsFragment$1;)V

    invoke-virtual {v2, v7}, Landroid/text/SpannableString;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v3, v7, v6, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/16 v0, 0xf

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method public loadImage(Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mImageManager:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->loadImage(Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const-string v2, "Placeholder"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x41900000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f090037

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    const/16 v2, 0x33

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    const-string v2, "has_error"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mHasError:Z

    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mHasError:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setError()V

    :goto_1
    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v4, 0x7f0b003c

    invoke-virtual {p0, v2, v0, v4}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->linkifyAndSetText(Landroid/content/Context;Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLoaderArguments()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ListAppsFragment must be hosted in an Activity that implements ListAppsFragmentCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mImageManager:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mImageManager:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->registerListener(Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;)V

    return-void
.end method

.method public bridge synthetic onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->onCreateLoader(ILandroid/os/Bundle;)Lcom/google/android/gms/plus/apps/ListAppsLoader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Lcom/google/android/gms/plus/apps/ListAppsLoader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/apps/ListAppsLoader;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    invoke-static {}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getMaxResults()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/apps/ListAppsLoader;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown loader ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mImageManager:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->unregisterListener(Lcom/google/android/gms/plus/apps/ListAppsImageManager$OnImageLoadedListener;)V

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->getItem(I)Lcom/google/android/gms/plus/model/apps/Application;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;->disconnectSource(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/apps/Application;)V

    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;)V
    .locals 4
    .param p2    # Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;",
            ">;",
            "Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    if-nez v2, :cond_2

    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/plus/apps/ListAppsLoader;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/ListAppsLoader;->getConnectionResult()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mCallbacks:Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment$ListAppsFragmentCallbacks;->onLoadError(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->swapDataBuffer(Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;)Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    :cond_2
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "has_error"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mHasError:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public removeApplication(Lcom/google/android/gms/plus/model/apps/Application;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLoaderArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public setEmpty()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;->swapDataBuffer(Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;)Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public setError()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getListAdapter()Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/apps/ListAppsAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/plus/apps/ListAppsAdapter;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ListAppsAdapter$ListAppsAdapterCallbacks;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_1

    const v0, 0x7f0b0049

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ListAppsFragment;->mHasError:Z

    return-void

    :cond_1
    const v0, 0x7f0b0048

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
