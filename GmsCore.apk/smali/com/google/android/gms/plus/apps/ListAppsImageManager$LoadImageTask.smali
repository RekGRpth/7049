.class Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;
.super Ljava/lang/Object;
.source "ListAppsImageManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/apps/ListAppsImageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadImageTask"
.end annotation


# instance fields
.field private final mApplication:Lcom/google/android/gms/plus/model/apps/Application;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/apps/ListAppsImageManager;Lcom/google/android/gms/plus/model/apps/Application;Ljava/lang/String;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    iput-object p3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mUrl:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;)Lcom/google/android/gms/plus/model/apps/Application;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mApplication:Lcom/google/android/gms/plus/model/apps/Application;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$100(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Lcom/google/android/gms/common/server/BaseApiaryServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mApplicationContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$000(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/server/BaseApiaryServer;->fetchImageBlocking(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    array-length v3, v0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;
    invoke-static {v4}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$300(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsImageManager$LoadImageTask;->this$0:Lcom/google/android/gms/plus/apps/ListAppsImageManager;

    # getter for: Lcom/google/android/gms/plus/apps/ListAppsImageManager;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ListAppsImageManager;->access$200(Lcom/google/android/gms/plus/apps/ListAppsImageManager;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lcom/android/volley/VolleyError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
