.class public Lcom/google/android/gms/plus/apps/ListAppsLoader;
.super Lcom/google/android/gms/plus/apps/DataBufferLoader;
.source "ListAppsLoader.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/plus/apps/DataBufferLoader",
        "<",
        "Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;",
        "Lcom/google/android/gms/plus/PlusClient;",
        ">;",
        "Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/Account;
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/apps/DataBufferLoader;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createClient(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Landroid/accounts/Account;)Lcom/google/android/gms/common/GooglePlayServicesClient;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # Landroid/accounts/Account;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/apps/ListAppsLoader;->createClient(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Landroid/accounts/Account;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    return-object v0
.end method

.method protected createClient(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Landroid/accounts/Account;)Lcom/google/android/gms/plus/PlusClient;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # Landroid/accounts/Account;

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iget-object v1, p4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v2, v1, v3

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setScopes([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "service_googleme"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->setRequiredFeatures([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->build()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic load(Lcom/google/android/gms/common/GooglePlayServicesClient;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    check-cast p1, Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/plus/apps/ListAppsLoader;->load(Lcom/google/android/gms/plus/PlusClient;ILjava/lang/String;)V

    return-void
.end method

.method protected load(Lcom/google/android/gms/plus/PlusClient;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-virtual {p1, p0, p2, p3}, Lcom/google/android/gms/plus/PlusClient;->loadConnectedApps(Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;ILjava/lang/String;)V

    return-void
.end method

.method public onAppsLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/model/apps/ApplicationBuffer;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/plus/apps/ListAppsLoader;->deliverResult(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/data/DataBuffer;Ljava/lang/String;)V

    return-void
.end method
