.class public final Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;
.super Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;
.source "AudienceSelectionCircleView.java"


# instance fields
.field private mViewCount:Landroid/widget/TextView;

.field private mViewDescription:Landroid/widget/TextView;

.field private mViewIcon:Landroid/widget/ImageView;

.field private mViewName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public hideDescription()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewDescription:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic isChecked()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->isChecked()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->onFinishInflate()V

    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewName:Landroid/widget/TextView;

    const v0, 0x7f0a006c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewCount:Landroid/widget/TextView;

    const v0, 0x7f0a0069

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a006d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewDescription:Landroid/widget/TextView;

    return-void
.end method

.method public bridge synthetic setChecked(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->setChecked(Z)V

    return-void
.end method

.method public setCountText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewCount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewDescription:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setIcon(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public setName(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->mViewName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0
    .param p1    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public bridge synthetic toggle()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->toggle()V

    return-void
.end method
