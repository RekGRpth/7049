.class abstract Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;
.super Landroid/widget/FrameLayout;
.source "AudienceSelectionItemView.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private mViewCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a006a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    new-instance v0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView$1;-><init>(Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1
    .param p1    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionItemView;->mViewCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    return-void
.end method
