.class public interface abstract Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudienceChangedListener"
.end annotation


# virtual methods
.method public abstract audienceChanged(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)V
.end method

.method public abstract audienceMemberAdded(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
.end method

.method public abstract audienceMemberRemoved(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
.end method
