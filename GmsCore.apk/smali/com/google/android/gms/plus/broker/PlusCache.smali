.class public Lcom/google/android/gms/plus/broker/PlusCache;
.super Ljava/lang/Object;
.source "PlusCache.java"


# static fields
.field private static sInstance:Lcom/google/android/gms/plus/broker/PlusCache;


# instance fields
.field private final mImageCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private final mPlusOneCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/plus/broker/PlusOneMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreviewCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    const/16 v1, 0x14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPreviewCache:Landroid/support/v4/util/LruCache;

    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mImageCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method private getAccountKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string p1, "<<null account>>"

    goto :goto_0
.end method

.method public static getInstance()Lcom/google/android/gms/plus/broker/PlusCache;
    .locals 2

    const-class v1, Lcom/google/android/gms/plus/broker/PlusCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/plus/broker/PlusCache;->sInstance:Lcom/google/android/gms/plus/broker/PlusCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/broker/PlusCache;

    invoke-direct {v0}, Lcom/google/android/gms/plus/broker/PlusCache;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/broker/PlusCache;->sInstance:Lcom/google/android/gms/plus/broker/PlusCache;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/broker/PlusCache;->sInstance:Lcom/google/android/gms/plus/broker/PlusCache;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getPlusOne(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/broker/PlusOneMetadata;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/broker/PlusCache;->getAccountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public queryImage(Landroid/net/Uri;)[B
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mImageCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public queryPreview(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removePlusOne(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/broker/PlusCache;->getAccountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updatePlusOne(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/Plusones;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v4, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v4

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/broker/PlusCache;->getAccountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    if-nez v1, :cond_0

    const/4 v2, 0x0

    monitor-exit v4

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/broker/PlusOneMetadata;->getPlusones()Lcom/google/android/gms/plus/service/pos/Plusones;

    move-result-object v2

    if-eqz p3, :cond_1

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->isSetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    if-nez p3, :cond_3

    invoke-static {v2}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->isSetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    invoke-static {v2, p3}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->modifySetByViewer(Lcom/google/android/gms/plus/service/pos/Plusones;Z)V

    if-eqz p3, :cond_4

    const/4 v3, 0x1

    :goto_1
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/broker/PlusOneHelper;->modifyCount(Lcom/google/android/gms/plus/service/pos/Plusones;I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0, v1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_4
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public updatePlusOne(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/broker/PlusOneMetadata;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/plus/broker/PlusOneMetadata;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPlusOneCache:Landroid/support/v4/util/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/broker/PlusCache;->getAccountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updatePreview(Landroid/net/Uri;[B)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # [B

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mImageCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updatePreview(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusCache;->mPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
