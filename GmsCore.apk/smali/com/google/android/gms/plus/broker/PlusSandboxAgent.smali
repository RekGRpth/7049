.class public final Lcom/google/android/gms/plus/broker/PlusSandboxAgent;
.super Ljava/lang/Object;
.source "PlusSandboxAgent.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mPeopleApi:Lcom/google/android/gms/plus/service/v1/PeopleApi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/v1/PeopleApi;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/service/v1/PeopleApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->mPeopleApi:Lcom/google/android/gms/plus/service/v1/PeopleApi;

    return-void
.end method

.method public static getPersonContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "displayName"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "personId"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "image"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "objectType"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "url"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getCurrentPersonBytes(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->queryPlusPerson(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "updated"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Lcom/google/android/gms/common/util/DefaultClock;->getInstance()Lcom/google/android/gms/common/util/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-string v0, "profileJson"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v5, 0x36ee80

    cmp-long v5, v3, v5

    if-gtz v5, :cond_0

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    const-class v4, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v4}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->generateDictionary(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;-><init>(Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)V

    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser;

    invoke-direct {v4}, Lcom/google/android/gms/common/server/response/FastParser;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v4, v0, v3}, Lcom/google/android/gms/common/server/response/FastParser;->parse(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->getParcel()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
    :try_end_1
    .catch Lcom/google/android/gms/common/server/response/FastParser$ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v3, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->TAG:Ljava/lang/String;

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->TAG:Ljava/lang/String;

    const-string v4, "Unable to parse the cached profile data"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_1
    :try_start_3
    const-class v0, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v0}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->generateDictionary(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->mPeopleApi:Lcom/google/android/gms/plus/service/v1/PeopleApi;

    const-string v3, "me"

    const-class v4, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    invoke-virtual {v1, p2, v3, v4, v0}, Lcom/google/android/gms/plus/service/v1/PeopleApi;->getBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    sget-object v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/people/PersonEntityCreator;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->inflate(Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "accountName"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "packageName"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "profileJson"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1, v4}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->updatePlusPerson(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->getParcel()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
    :try_end_3
    .catch Lcom/android/volley/VolleyError; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v0

    :catch_1
    move-exception v0

    iget-object v1, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v1, v1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v3, 0x190

    if-lt v1, v3, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V

    :cond_2
    throw v0
.end method

.method public getPerson(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->mPeopleApi:Lcom/google/android/gms/plus/service/v1/PeopleApi;

    const-class v1, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    const-class v2, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->generateDictionary(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v2

    invoke-virtual {v0, p2, p3, v1, v2}, Lcom/google/android/gms/plus/service/v1/PeopleApi;->getBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    return-object v0
.end method

.method public listPeople(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "III",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/data/DataHolder;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    packed-switch p3, :pswitch_data_0

    const-string v3, "visible"

    :goto_0
    packed-switch p4, :pswitch_data_1

    const-string v5, "alphabetical"

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->mPeopleApi:Lcom/google/android/gms/plus/service/v1/PeopleApi;

    const-string v2, "me"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-class v7, Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    const/4 v8, 0x0

    move-object v1, p2

    move-object/from16 v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/plus/service/v1/PeopleApi;->listBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->getItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusContract$PersonColumns;->ALL_COLUMNS:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->builder([Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    move-result-object v6

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_1

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/model/people/Person;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getImage()Lcom/google/android/gms/plus/model/people/Person$Image;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_3
    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getObjectType()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ObjectTypeConverter;->fromInt(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v8, v2, v9, v1}, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->getPersonContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/google/android/gms/common/data/DataHolder$Builder;->withRow(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :pswitch_0
    const-string v3, "visible"

    goto :goto_0

    :pswitch_1
    const-string v5, "alphabetical"

    goto :goto_1

    :pswitch_2
    const-string v5, "best"

    goto :goto_1

    :cond_0
    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->getImage()Lcom/google/android/gms/plus/model/people/Person$Image;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/model/people/Person$Image;->getUrl()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_1
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/google/android/gms/common/data/DataHolder$Builder;->build(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->getNextPageToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
