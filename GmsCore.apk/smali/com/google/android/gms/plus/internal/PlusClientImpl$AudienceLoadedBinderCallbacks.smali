.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;
.super Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "AudienceLoadedBinderCallbacks"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :goto_0
    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    if-eqz p3, :cond_0

    const-class v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v3, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedCallback;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

    invoke-direct {v3, v4, v5, v2, v1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
