.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;
.super Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "LinkPreviewLoadedBinderCallbacks"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const-string v3, "pendingIntent"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    :cond_0
    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v2, p1, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    new-instance v0, Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {v0, p3}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;-><init>(Landroid/os/Bundle;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v4, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    invoke-direct {v4, v5, v6, v2, v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
