.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "LinkPreviewLoadedCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">.CallbackProxy<",
        "Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;",
        ">;"
    }
.end annotation


# instance fields
.field public final linkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

.field public final status:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .param p4    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->linkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->linkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;->onLinkPreviewLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;->deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;)V

    return-void
.end method
