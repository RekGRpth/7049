.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;
.super Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MomentsLoadedBinderCallbacks"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    return-void
.end method


# virtual methods
.method public onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/common/data/DataHolder;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->getMetadata()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->getMetadata()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pendingIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    :cond_0
    new-instance v3, Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->getStatusCode()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
