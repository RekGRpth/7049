.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$DataLoadedCallback;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MomentsLoadedCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">.Data",
        "LoadedCallback",
        "<",
        "Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNextPageToken:Ljava/lang/String;

.field private final mStatus:Lcom/google/android/gms/common/ConnectionResult;

.field private final mUpdated:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .param p4    # Lcom/google/android/gms/common/data/DataHolder;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/gms/common/internal/GmsClient$DataLoadedCallback;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mNextPageToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mUpdated:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    new-instance v1, Lcom/google/android/gms/plus/model/moments/MomentBuffer;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mDataHolder:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/model/moments/MomentBuffer;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mNextPageToken:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->mUpdated:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;->onMomentsLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/moments/MomentBuffer;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;->deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;)V

    return-void
.end method
