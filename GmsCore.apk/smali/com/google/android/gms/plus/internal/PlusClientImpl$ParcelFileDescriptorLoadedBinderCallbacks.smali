.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;
.super Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ParcelFileDescriptorLoadedBinderCallbacks"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

    return-void
.end method


# virtual methods
.method public onImageFileDescriptorLoaded(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/ParcelFileDescriptor;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string v2, "pendingIntent"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_0
    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    new-instance v3, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->mListener:Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

    invoke-direct {v3, v4, v5, v1, p3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
