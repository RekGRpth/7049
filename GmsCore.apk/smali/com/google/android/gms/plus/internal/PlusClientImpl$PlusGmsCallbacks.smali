.class public final Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;
.super Lcom/google/android/gms/common/internal/IGmsCallbacks$Stub;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "PlusGmsCallbacks"
.end annotation


# instance fields
.field private mGmsCallbacks:Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/IGmsCallbacks$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;->mGmsCallbacks:Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;

    return-void
.end method


# virtual methods
.method public onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/IBinder;
    .param p3    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    const-string v1, "loaded_person"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->fromByteArray([B)Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    move-result-object v1

    # setter for: Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCurrentPerson:Lcom/google/android/gms/plus/model/people/Person;
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->access$002(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/model/people/Person;)Lcom/google/android/gms/plus/model/people/Person;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;->mGmsCallbacks:Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method
