.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "TokenRevokedCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">.CallbackProxy<",
        "Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mStatus:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->disconnect()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {p1, v0}, Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;->onAccessRevoked(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;->deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;)V

    return-void
.end method
