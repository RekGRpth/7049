.class Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;
.super Ljava/lang/Object;
.source "PlusOneButtonContentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultOnPlusOneClickListener"
.end annotation


# instance fields
.field private final mProxy:Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->mProxy:Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    if-ne p1, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->mProxy:Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->mProxy:Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;->onPlusOneClick(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->onPlusOneClick(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onPlusOneClick(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mActivityRequestCode:I

    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method
