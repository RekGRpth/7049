.class public Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;
.super Ljava/lang/Object;
.source "PlusOneButtonContentView.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PlusOneButtonOnPlusOneLoadedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/PlusOne;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTogglePending:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTogglePending:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iput-object p2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateView()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->refreshButtonLayout()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showDisabled()V

    goto :goto_0
.end method
