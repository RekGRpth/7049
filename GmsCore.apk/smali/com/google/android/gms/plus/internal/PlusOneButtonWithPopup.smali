.class public final Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;
.super Landroid/view/ViewGroup;
.source "PlusOneButtonWithPopup.java"


# instance fields
.field private mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public cancelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->cancelClick()V

    return-void
.end method

.method public initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V

    return-void
.end method

.method public initializeAnonymous(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->initializeAnonymous(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {p0, v1, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->measureChild(Landroid/view/View;II)V

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->setMeasuredDimension(II)V

    return-void
.end method

.method public setAnnotation(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setAnnotation(I)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSize(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setSize(I)V

    return-void
.end method

.method public setType(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->mContentView:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setType(I)V

    return-void
.end method
