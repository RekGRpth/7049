.class Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;
.super Ljava/lang/Object;
.source "PlusOneButtonWithPopupContentView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mAttachedToWindow:Z
    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$000(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$100(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$100(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$1;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$102(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method
