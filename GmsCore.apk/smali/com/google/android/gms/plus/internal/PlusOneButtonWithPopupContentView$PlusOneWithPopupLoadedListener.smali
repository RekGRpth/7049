.class public Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;
.super Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;
.source "PlusOneButtonWithPopupContentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PlusOneWithPopupLoadedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;)V

    return-void
.end method


# virtual methods
.method public onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneButtonOnPlusOneLoadedListener;->onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PlusOneWithPopupLoadedListener;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mResultPending:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$202(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z

    return-void
.end method
