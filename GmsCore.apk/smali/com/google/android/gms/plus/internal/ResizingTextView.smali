.class public Lcom/google/android/gms/plus/internal/ResizingTextView;
.super Landroid/widget/FrameLayout;
.source "ResizingTextView.java"


# instance fields
.field private final mImageView:Landroid/widget/ImageView;

.field private mTextList:[Ljava/lang/String;

.field private final mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/16 v3, 0x11

    const/4 v2, -0x2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mImageView:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/ResizingTextView;->bringChildToFront(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getImageViewIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getImageViewIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public hasTextSet()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    array-length v1, v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTextSize()F

    move-result v7

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    array-length v4, v7

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_3

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    aget-object v7, v7, v0

    if-nez v7, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    aget-object v7, v7, v0

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v3, v7

    if-gt v3, v6, :cond_0

    if-lt v3, v1, :cond_0

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    aget-object v5, v7, v0

    move v1, v3

    goto :goto_2

    :cond_3
    if-eqz v5, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_4
    iget-object v7, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method public setGravity(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method public setImage(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    return-void
.end method

.method public setSingleLine()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    return-void
.end method

.method public setText([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextList:[Ljava/lang/String;

    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setTextSize(IF)V
    .locals 1
    .param p1    # I
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ResizingTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method
