.class public final Lcom/google/android/gms/plus/internal/model/apps/ApplicationColumns;
.super Ljava/lang/Object;
.source "ApplicationColumns.java"


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final APPLICATION_INFO_PARCEL_BYTES:Ljava/lang/String; = "application_info"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final ICON_URL:Ljava/lang/String; = "icon_url"

.field public static final ID:Ljava/lang/String; = "application_id"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "icon_url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "application_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "application_info"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationColumns;->ALL_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final deserializeApplicationInfo([B)Landroid/content/pm/ApplicationInfo;
    .locals 3

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    array-length v2, p0

    invoke-virtual {v1, p0, v0, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    const-class v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public static final serializeApplicationInfo(Landroid/content/pm/ApplicationInfo;)[B
    .locals 2
    .param p0    # Landroid/content/pm/ApplicationInfo;

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v1
.end method
