.class public interface abstract Lcom/google/android/gms/plus/model/people/Person;
.super Ljava/lang/Object;
.source "Person.java"

# interfaces
.implements Lcom/google/android/gms/common/data/Freezable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/model/people/Person$Urls;,
        Lcom/google/android/gms/plus/model/people/Person$PlacesLived;,
        Lcom/google/android/gms/plus/model/people/Person$Organizations;,
        Lcom/google/android/gms/plus/model/people/Person$Name;,
        Lcom/google/android/gms/plus/model/people/Person$Image;,
        Lcom/google/android/gms/plus/model/people/Person$Emails;,
        Lcom/google/android/gms/plus/model/people/Person$Cover;,
        Lcom/google/android/gms/plus/model/people/Person$AgeRange;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/data/Freezable",
        "<",
        "Lcom/google/android/gms/plus/model/people/Person;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getImage()Lcom/google/android/gms/plus/model/people/Person$Image;
.end method

.method public abstract getObjectType()I
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method
