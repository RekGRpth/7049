.class public final Lcom/google/android/gms/plus/plusone/PlusOneActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PlusOneActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mArgs:Landroid/os/Bundle;

.field private mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

.field private mNeedsSignIn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private createView()V
    .locals 4

    const v3, 0x7f0a007f

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->isViewConstrained()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f04002c

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PlusOneActivity#Fragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-direct {v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    const-string v2, "PlusOneActivity#Fragment"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    const v1, 0x7f0200c7

    const v2, 0x7f0b001c

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setTitlebarTitle(ILjava/lang/String;)V

    const v1, 0x7f0a007e

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    const v1, 0x7f04002b

    goto :goto_0
.end method

.method private isViewConstrained()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v3, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0x0

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v3, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0x3

    if-eqz v3, :cond_2

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setTitlebarTitle(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    const v5, 0x7f0a0095

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    const v5, 0x7f0a0093

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const v5, 0x7f090001

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v4, v6, v6, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->onResolveConnectionResult(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onAttachedToWindow()V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    if-eqz v1, :cond_0

    const v1, 0x7f0a0097

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->setProgressBar(Landroid/widget/ProgressBar;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->logOnBackOrCancel()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a007e

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->logOnBackOrCancel()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/gms/common/util/AndroidUtils;->getCallingPackage(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "com.google.android.gms.plus.intent.extra.TOKEN"

    invoke-virtual {v8, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v2, "com.google.android.gms.plus.intent.extra.URL"

    invoke-virtual {v8, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v2, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    invoke-virtual {v8, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v2, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v8, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget-object v2, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->TAG:Ljava/lang/String;

    const-string v3, "Intent missing required arguments"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, "<<default account>>"

    iput-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    :cond_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mAccount"

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mUrl"

    invoke-virtual {v2, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mApplyPlusOne"

    const-string v5, "com.google.android.gms.plus.action.PLUS_ONE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mArgs:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mToken"

    invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_5

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    if-nez v9, :cond_4

    const/4 v1, 0x1

    :cond_4
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    if-eqz v1, :cond_7

    if-nez p1, :cond_1

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    :goto_2
    sget-object v1, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->PLUS_SCOPES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v7, v1, :cond_6

    sget-object v1, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->PLUS_SCOPES:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->addGrantedScope(Ljava/lang/String;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_5
    const-string v1, "needs_sign_in"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/PlusIntents;->newSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v10

    const/4 v1, 0x3

    invoke-virtual {p0, v10, v1}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->createView()V

    goto/16 :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0021

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string v1, "errorCode"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    invoke-static {v1, p0, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mFragment:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->createView()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "needs_sign_in"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->mNeedsSignIn:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
