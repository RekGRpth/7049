.class Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;
.super Ljava/lang/Object;
.source "PlusOneFragment.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/plusone/PlusOneFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/PlusOne;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getToken()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$002(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z
    invoke-static {v1, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$102(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;
    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$400(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    invoke-static {v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$200(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$300(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$000(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/plus/PlusClient;->insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
