.class Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;
.super Ljava/lang/Object;
.source "PlusOneFragment.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/plusone/PlusOneFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLinkPreviewLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadingPreview:Z
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$502(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$600(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_PREVIEW_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "title"

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$300(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "url"

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$300(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "type"

    const-string v3, "article"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    new-instance v3, Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {v3, v1}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;-><init>(Landroid/content/ContentValues;)V

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$702(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    invoke-static {v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$700(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    move-result-object v3

    # invokes: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updatePreviewView(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V
    invoke-static {v2, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$800(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$900(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    invoke-static {v2, p2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$702(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    goto :goto_0
.end method
