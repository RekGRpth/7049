.class Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;
.super Ljava/lang/Object;
.source "PlusOneFragment.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/plusone/PlusOneFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlusOneLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/PlusOne;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/PlusOne;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # setter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertingPlusOne:Z
    invoke-static {v1, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$1002(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;->this$0:Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    # getter for: Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->access$900(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0
.end method
