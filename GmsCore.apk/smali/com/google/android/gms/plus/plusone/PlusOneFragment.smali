.class public final Lcom/google/android/gms/plus/plusone/PlusOneFragment;
.super Landroid/support/v4/app/Fragment;
.source "PlusOneFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# static fields
.field static final PLUS_SCOPES:[Ljava/lang/String;


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mApplyPlusOne:Z

.field private mCallingPackage:Ljava/lang/String;

.field private final mGetSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

.field private mGettingSignUpState:Z

.field private final mInsertPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

.field private mInsertingPlusOne:Z

.field private mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

.field private final mLoadLinkPreviewListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

.field private final mLoadPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

.field private mLoadingPreview:Z

.field private mLoggedPlusOne:Z

.field private mLoggedPreview:Z

.field private mPendingInsert:Z

.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;

.field private mProgressView:Landroid/widget/ProgressBar;

.field private mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

.field private mStatus:Lcom/google/android/gms/common/ConnectionResult;

.field private mToken:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->PLUS_SCOPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment$1;-><init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    new-instance v0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment$2;-><init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadLinkPreviewListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    new-instance v0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment$3;-><init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    new-instance v0, Lcom/google/android/gms/plus/plusone/PlusOneFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment$4;-><init>(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGetSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1002(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertingPlusOne:Z

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGettingSignUpState:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/SignUpState;)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Lcom/google/android/gms/plus/data/plusone/SignUpState;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/SignUpState;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateProfileView(Lcom/google/android/gms/plus/data/plusone/SignUpState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/PlusClient;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadingPreview:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/gms/plus/plusone/PlusOneFragment;Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;
    .param p1    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updatePreviewView(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/gms/plus/plusone/PlusOneFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/plusone/PlusOneFragment;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private getAppText(Ljava/lang/CharSequence;)Landroid/text/Spanned;
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method private updateContentView(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0a0080

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private updatePreviewView(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V
    .locals 7
    .param p1    # Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0a0089

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v3, Lcom/google/android/gms/plus/plusone/PreviewView;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/plusone/PreviewView;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/plusone/PreviewView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    invoke-virtual {v3, p1}, Lcom/google/android/gms/plus/plusone/PreviewView;->setLinkPreview(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)Z

    move-result v2

    iget-boolean v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPreview:Z

    if-nez v4, :cond_1

    if-eqz p1, :cond_1

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPreview:Z

    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_PREVIEW_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v6, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_1
    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_PREVIEW_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v6, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_1
.end method

.method private updateProfileView(Lcom/google/android/gms/plus/data/plusone/SignUpState;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/plus/data/plusone/SignUpState;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a0082

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/PlusClient;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/plus/internal/PlusContent$Avatar;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a0084

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public logOnBackOrCancel()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v1, :cond_3

    const/4 v1, -0x1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->UNDO_PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0a0086

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0a0087

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mCallingPackage:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getAppText(Ljava/lang/CharSequence;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0a008a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0a008b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0a008c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateContentView(Z)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-direct {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateProfileView(Lcom/google/android/gms/plus/data/plusone/SignUpState;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updatePreviewView(Lcom/google/android/gms/plus/data/plusone/LinkPreview;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    return-void

    :catch_0
    move-exception v3

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-boolean v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gms/plus/PlusClient;->deletePlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v6, :cond_3

    :goto_2
    invoke-virtual {v0, v4}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->UNDO_PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_1

    :cond_3
    move v4, v5

    goto :goto_2

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_SHARE_FROM_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->GOOGLE_PLUS_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    invoke-static {v0}, Lcom/google/android/gms/plus/GooglePlusUtil;->checkGooglePlusApp(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_SHARE_FROM_PLUS_ONE_NO_PLUS_APP:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v6, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "errorCode"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v1}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    :cond_4
    invoke-static {v0}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->from(Landroid/app/Activity;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setText(Ljava/lang/CharSequence;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v6

    const-string v7, "text/plain"

    invoke-virtual {v6, v7}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setType(Ljava/lang/String;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "com.google.android.apps.plus"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v6, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "com.google.android.apps.plus.IS_FROM_PLUSONE"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->startActivity(Landroid/content/Intent;)V

    iget-boolean v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v6, :cond_5

    :goto_3
    invoke-virtual {v0, v5}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_5
    move v5, v4

    goto :goto_3

    :pswitch_2
    iget-boolean v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :goto_4
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    iget-boolean v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    if-eqz v6, :cond_7

    :goto_5
    invoke-virtual {v0, v5}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_6
    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->UNDO_PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v6, v7, v8}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    goto :goto_4

    :cond_7
    move v5, v4

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x7f0a008a
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConnected()V
    .locals 6

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->getAccountName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPlusOne:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-boolean v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPlusOne:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusAction(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->THIRD_PARTY_APP_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$View;->CONFIRMATION_VIEW:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/server/PlusAnalytics;->logPlusNavigation(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    if-eqz v1, :cond_2

    iput-boolean v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertingPlusOne:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/PlusClient;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    if-nez v1, :cond_3

    iput-boolean v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadingPreview:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadLinkPreviewListener:Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/PlusClient;->loadLinkPreviewInternal(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    if-nez v1, :cond_4

    iput-boolean v5, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGettingSignUpState:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGetSignUpStateListener:Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/PlusClient;->getSignUpState(Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V

    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateContentView(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/PlusClient;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v5}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/plus/PlusClient;->insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v0, "PlusOneFragment#mAccount"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "PlusOneFragment#mToken"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    const-string v0, "PlusOneFragment#mApplyPlusOne"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    const-string v0, "PlusOneFragment#mUrl"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mUrl:Ljava/lang/String;

    const-string v0, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->PLUS_SCOPES:[Ljava/lang/String;

    move-object v5, p0

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iput-boolean v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertingPlusOne:Z

    iput-boolean v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadingPreview:Z

    iput-boolean v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGettingSignUpState:Z

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mApplyPlusOne:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    iput-boolean v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPreview:Z

    iput-boolean v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPlusOne:Z

    invoke-virtual {v1, v9}, Landroid/app/Activity;->setResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "pendingInsert"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    const-string v0, "loggedPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPreview:Z

    const-string v0, "loggedPlusOne"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPlusOne:Z

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    :cond_2
    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    :cond_3
    const-string v0, "linkPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    const-string v2, "linkPreview"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    :cond_4
    const-string v0, "signUpState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/data/plusone/SignUpState;

    const-string v2, "signUpState"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/plus/data/plusone/SignUpState;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f04002d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onResolveConnectionResult(ILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->showDialog(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "pendingInsert"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPendingInsert:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "loggedPreview"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPreview:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "loggedPlusOne"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoggedPlusOne:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "token"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    if-eqz v0, :cond_2

    const-string v0, "linkPreview"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLinkPreview:Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;->asBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    if-eqz v0, :cond_3

    const-string v0, "signUpState"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->asBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    return-void
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;)V
    .locals 1
    .param p1    # Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method protected updateSpinner(Landroid/widget/ProgressBar;)V
    .locals 1
    .param p1    # Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mInsertingPlusOne:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mLoadingPreview:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneFragment;->mGettingSignUpState:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method
