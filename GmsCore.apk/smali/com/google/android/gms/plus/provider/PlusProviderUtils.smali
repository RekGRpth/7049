.class public final Lcom/google/android/gms/plus/provider/PlusProviderUtils;
.super Ljava/lang/Object;
.source "PlusProviderUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static clearDeletedAccounts(Landroid/content/ContentResolver;[Landroid/accounts/Account;)V
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # [Landroid/accounts/Account;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/plus/provider/PlusProvider;->clearDeletedPlusAccounts([Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method

.method public static deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/plus/provider/PlusProvider;->deletePlusAccount(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method

.method public static queryPlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/plus/provider/PlusProvider;->queryPlusAccount(Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method

.method public static queryPlusPerson(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/plus/provider/PlusProvider;->queryPlusPerson(Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method

.method public static updatePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/plus/provider/PlusProvider;->updatePlusAccount(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method

.method public static updatePlusPerson(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/plus/provider/PlusProvider;->updatePlusPerson(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method
