.class public final Lcom/google/android/gms/plus/server/ApiaryRpcServer;
.super Lcom/google/android/gms/plus/server/PlusServer;
.source "ApiaryRpcServer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/plus/server/PlusServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 11
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Lorg/json/JSONArray;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/UserRecoverableAuthException;,
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->getAuthTokenOrThrow(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/common/server/RequestFuture;->newFuture()Lcom/google/android/gms/common/server/RequestFuture;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v0, Lcom/google/android/gms/plus/server/ApiaryRpcRequest;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->mBaseServer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v7, p0, Lcom/google/android/gms/plus/server/ApiaryRpcServer;->mEnableCache:Z

    move-object v2, p3

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/server/ApiaryRpcRequest;-><init>(Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;ZLjava/util/HashMap;)V

    invoke-virtual {v10, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/server/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v9

    new-instance v0, Lcom/android/volley/VolleyError;

    const-string v1, "Thread interrupted"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v9

    new-instance v0, Lcom/android/volley/VolleyError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error executing network request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v9}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
