.class public Lcom/google/android/gms/plus/server/UnicodeConverter;
.super Ljava/lang/Object;
.source "UnicodeConverter.java"

# interfaces
.implements Lcom/google/android/gms/common/server/response/FastJsonResponse$FieldConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/server/response/FastJsonResponse$FieldConverter",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static sFromStrings:[Ljava/lang/String;

.field private static sToStrings:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "\\\\u003d"

    aput-object v1, v0, v2

    const-string v1, "\\\\u003c"

    aput-object v1, v0, v3

    const-string v1, "\\\\u003e"

    aput-object v1, v0, v4

    const-string v1, "\\\\\\\""

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/plus/server/UnicodeConverter;->sFromStrings:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "="

    aput-object v1, v0, v2

    const-string v1, "<"

    aput-object v1, v0, v3

    const-string v1, ">"

    aput-object v1, v0, v4

    const-string v1, "\""

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/plus/server/UnicodeConverter;->sToStrings:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convert(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/server/UnicodeConverter;->convert(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public convert(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    move-object v1, p1

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/gms/plus/server/UnicodeConverter;->sFromStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/gms/plus/server/UnicodeConverter;->sFromStrings:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/google/android/gms/plus/server/UnicodeConverter;->sToStrings:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic convertBack(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/server/UnicodeConverter;->convertBack(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public convertBack(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;

    return-object p1
.end method

.method public getTypeIn()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public getTypeOut()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method
