.class public final Lcom/google/android/gms/plus/service/ImageIntentService;
.super Lcom/google/android/gms/plus/service/OperationIntentService;
.source "ImageIntentService.java"


# static fields
.field private static final START:Landroid/content/Intent;

.field private static final TAG:Ljava/lang/String;

.field private static sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/gms/plus/service/ImageIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.service.image.INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->START:Landroid/content/Intent;

    new-instance v0, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/plus/service/ImageIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/service/OperationIntentService;-><init>(Ljava/lang/String;Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;)V

    return-void
.end method

.method public static startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    sget-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->sQueue:Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/service/OperationIntentServiceQueue;->addOperation(Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    sget-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->START:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
