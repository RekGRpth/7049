.class final Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "OfflineActionSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$1;,
        Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$AnalyticsQuery;,
        Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;,
        Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$FramesQuery;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;


# instance fields
.field private mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

    return-void
.end method

.method static fromDatabasePayload(Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v6, 0x3

    new-instance v1, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;-><init>()V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_0
    new-instance v4, Lcom/google/android/gms/common/server/response/FastParser;

    invoke-direct {v4}, Lcom/google/android/gms/common/server/response/FastParser;-><init>()V

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/common/server/response/FastParser;->parse(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/FastParser$ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v3

    const-string v4, "OASyncAdapter"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "OASyncAdapter"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_2
    const-string v4, "OASyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "OASyncAdapter"

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/response/FastParser$ParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    const/4 v1, 0x0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v3

    const-string v4, "OASyncAdapter"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "OASyncAdapter"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_1
    throw v4

    :catch_3
    move-exception v3

    const-string v5, "OASyncAdapter"

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "OASyncAdapter"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->sInstance:Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;

    invoke-static {p0}, Lcom/google/android/gms/plus/broker/DataBroker;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/broker/DataBroker;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V

    sput-object v0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->sInstance:Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->sInstance:Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;

    return-object v0
.end method

.method private getPendingLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentProviderClient;
    .param p3    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentProviderClient;",
            "Landroid/content/SyncResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v5, 0x0

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusContent$Analytics;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$AnalyticsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "accountName=? AND type=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    const-string v5, "timestamp"

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :try_start_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->fromDatabasePayload(Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v8
.end method

.method private purgeLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;J)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentProviderClient;
    .param p3    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x1

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusContent$Analytics;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "accountName=? AND type=? AND timestamp<=?"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p2, v0, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private purgeOldLogs(Landroid/content/ContentProviderClient;)V
    .locals 8
    .param p1    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/common/util/DefaultClock;->getInstance()Lcom/google/android/gms/common/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0xa4cb800

    sub-long v1, v3, v5

    sget-object v3, Lcom/google/android/gms/plus/internal/PlusContent$Analytics;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "timestamp<?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private syncFrames(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 17
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/content/ContentProviderClient;
    .param p3    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v9, 0x0

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    sget-object v2, Lcom/google/android/gms/plus/internal/PlusContent$Frames;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$FramesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "accountName=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v5, v1

    const-string v6, "_id"

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v12, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    const/4 v1, 0x0

    invoke-direct {v12, v1}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;-><init>(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$1;)V

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    # setter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->id:J
    invoke-static {v12, v1, v2}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$102(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;J)J

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->url:Ljava/lang/String;
    invoke-static {v12, v1}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$202(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->payload:Ljava/lang/String;
    invoke-static {v12, v1}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$302(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->packageName:Ljava/lang/String;
    invoke-static {v12, v1}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$402(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v1

    :cond_1
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v8

    :goto_1
    if-ge v14, v8, :cond_4

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    :try_start_1
    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->packageName:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$400(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v15

    new-instance v7, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, v15, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->packageName:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$400(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "https://www.googleapis.com/auth/plus.login"

    invoke-virtual {v7, v1}, Lcom/google/android/gms/common/server/ClientContext;->addGrantedScope(Ljava/lang/String;)V

    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->url:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$200(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->payload:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$300(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v7, v2, v3}, Lcom/google/android/gms/plus/broker/DataBroker;->uploadFrame(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/volley/VolleyError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_2
    sget-object v1, Lcom/google/android/gms/plus/internal/PlusContent$Frames;->CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->id:J
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$100(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :catch_0
    move-exception v11

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusContent$Frames;->CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->id:J
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$100(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catch_1
    move-exception v11

    iget-object v1, v11, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-nez v1, :cond_6

    :cond_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    :try_start_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_3
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_5
    :goto_4
    return-void

    :cond_6
    move-object/from16 v0, p3

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v1, "OASyncAdapter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "OASyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to upload moment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->payload:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;->access$300(Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter$Frame;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_2
    move-exception v11

    const-string v1, "OASyncAdapter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "OASyncAdapter"

    const-string v2, "Failed to upload moment due to fatal authentication problem."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catch_3
    move-exception v11

    const-string v1, "OASyncAdapter"

    const-string v2, "Failed to delete"

    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    move-object/from16 v0, p3

    iput-boolean v1, v0, Landroid/content/SyncResult;->databaseError:Z

    goto :goto_4
.end method

.method private syncLogs(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 11
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/content/ContentProviderClient;
    .param p3    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v10, 0x3

    invoke-direct {p0, p2}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->purgeOldLogs(Landroid/content/ContentProviderClient;)V

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, p2, p3}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getPendingLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;->getClientTimeMsec()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "https://www.googleapis.com/auth/plus.me"

    invoke-virtual {v0, v5}, Lcom/google/android/gms/common/server/ClientContext;->addGrantedScope(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->mDataBroker:Lcom/google/android/gms/plus/broker/DataBroker;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/plus/server/UserAgent;->getVersionCode(Landroid/content/Context;)I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f080000

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    invoke-virtual {v5, v0, v4, v6, v7}, Lcom/google/android/gms/plus/broker/DataBroker;->sendLogEvents(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZ)V

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->purgeLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;J)V
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    iget-object v5, v1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v5, :cond_0

    iget-object v5, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v5, "OASyncAdapter"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "OASyncAdapter"

    invoke-virtual {v1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->purgeLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;J)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v5, "OASyncAdapter"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "OASyncAdapter"

    const-string v6, "Failed to upload logs due to transient authentication problem."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->purgeLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;J)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v5, "OASyncAdapter"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "OASyncAdapter"

    const-string v6, "Failed to upload logs due to fatal authentication problem."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->purgeLogs(Ljava/lang/String;Landroid/content/ContentProviderClient;J)V

    goto :goto_0

    :cond_4
    const-string v5, "OASyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "OASyncAdapter"

    const-string v6, "No logs to transmit at this time"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ContentProviderClient;
    .param p5    # Landroid/content/SyncResult;

    const-string v1, "com.google.android.gms.plus.action"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->syncFrames(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->syncLogs(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OASyncAdapter"

    const-string v2, "Sync Failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    iput-boolean v1, p5, Landroid/content/SyncResult;->databaseError:Z

    goto :goto_0
.end method
