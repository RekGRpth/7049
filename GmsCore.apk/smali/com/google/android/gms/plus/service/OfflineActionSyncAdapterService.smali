.class public Lcom/google/android/gms/plus/service/OfflineActionSyncAdapterService;
.super Landroid/app/Service;
.source "OfflineActionSyncAdapterService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/OfflineActionSyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
