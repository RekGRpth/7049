.class public final Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClearDefaultAccountOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/common/account/AccountUtils;->getSelectedAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/common/account/AccountUtils;->clearSelectedAccount(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    return-void
.end method
