.class public final Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetSignUpStateOperation"
.end annotation


# instance fields
.field private final mCacheOption:I

.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;I)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iput p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mCacheOption:I

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iget v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mCacheOption:I

    invoke-virtual {p2, p1, v4, v5}, Lcom/google/android/gms/plus/broker/DataBroker;->getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/plusone/SignUpState;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->asBundle()Landroid/os/Bundle;

    move-result-object v7

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {p1, v9, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v4, "pendingIntent"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v4, v10, v2, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v4, v10, v8, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v5, 0x7

    invoke-interface {v4, v5, v8, v8}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
