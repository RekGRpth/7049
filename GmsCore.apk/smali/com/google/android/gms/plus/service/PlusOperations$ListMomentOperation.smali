.class public Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListMomentOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mMaxResults:I

.field private final mPageToken:Ljava/lang/String;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

.field private final mTargetUrl:Landroid/net/Uri;

.field private final mType:Ljava/lang/String;

.field private final mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mMaxResults:I

    iput-object p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPageToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mTargetUrl:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mUserId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 22
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mMaxResults:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPageToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mTargetUrl:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mUserId:Ljava/lang/String;

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/gms/plus/broker/DataBroker;->listMoments(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    move-result-object v13

    const/4 v11, 0x0

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->getItems()Ljava/util/List;

    move-result-object v17

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v18

    sget-object v2, Lcom/google/android/gms/plus/internal/PlusContract$MomentColumns;->ALL_COLUMNS:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->builder([Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    move-result-object v15

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    new-instance v21, Landroid/content/ContentValues;

    const/4 v2, 0x1

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "momentImpl"

    invoke-virtual {v11}, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;->toByteArray()[B

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/google/android/gms/common/data/DataHolder$Builder;->withRow(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/DataHolder$Builder;

    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/gms/common/data/DataHolder$Builder;->build(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->getNextPageToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->getUpdated()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v14, v3, v4}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    return-void

    :catch_0
    move-exception v12

    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v12}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v19

    const-string v2, "pendingIntent"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v3, 0x4

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpBundle(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v3, 0x4

    invoke-static {v3, v10}, Lcom/google/android/gms/common/data/DataHolder;->empty(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v3, 0x7

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
