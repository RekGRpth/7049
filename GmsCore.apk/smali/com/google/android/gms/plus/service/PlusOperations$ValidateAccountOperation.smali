.class public Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ValidateAccountOperation"
.end annotation


# instance fields
.field private final mAssertedAccountName:Ljava/lang/String;

.field private final mAuthPackage:Ljava/lang/String;

.field private final mAuthUid:I

.field private final mCallingPackage:Ljava/lang/String;

.field private final mGmsCallbacks:Lcom/google/android/gms/common/internal/IGmsCallbacks;

.field private final mRequiredFeatures:[Ljava/lang/String;

.field private final mScopes:[Ljava/lang/String;

.field private final mVisibleActions:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lcom/google/android/gms/common/internal/IGmsCallbacks;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # I
    .param p7    # [Ljava/lang/String;
    .param p8    # Lcom/google/android/gms/common/internal/IGmsCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAssertedAccountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mCallingPackage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthPackage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mVisibleActions:[Ljava/lang/String;

    iput p6, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthUid:I

    iput-object p7, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mRequiredFeatures:[Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mGmsCallbacks:Lcom/google/android/gms/common/internal/IGmsCallbacks;

    return-void
.end method

.method private checkAuth(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthPackage:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthUid:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getScopesString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mVisibleActions:[Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->checkAuth(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v6

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v1, 0x6

    invoke-virtual {v6}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    goto :goto_0

    :catch_1
    move-exception v6

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/16 v1, 0x8

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, p2, v2}, Lcom/google/android/gms/plus/PlusIntents;->newSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    goto :goto_0

    :catch_2
    move-exception v6

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v1, 0x4

    invoke-static {p1, p2}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private createCallerClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthUid:I

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAssertedAccountName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mCallingPackage:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAuthPackage:Ljava/lang/String;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->setGrantedScopes([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mVisibleActions:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->setVisibleActions([Ljava/lang/String;)V

    return-object v0
.end method

.method private createGmsClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAssertedAccountName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/plus/service/PlusOperations;->FIRST_PARTY_SCOPES:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->setGrantedScopes([Ljava/lang/String;)V

    return-object v0
.end method

.method private sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/os/IBinder;
    .param p4    # Landroid/content/Intent;
    .param p5    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-eqz p2, :cond_0

    const/4 v2, 0x5

    if-eq p2, v2, :cond_0

    if-nez p4, :cond_0

    sget-object v2, Lcom/google/android/gms/plus/service/PlusOperations;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no resolution provided for status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "no resolution provided!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p4, :cond_1

    const-string v2, "com.google.android.gms.plus.intent.extra.AUTHENTICATED_CALLING_PACKAGE"

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {p4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p1, v2, p4, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    if-eqz p5, :cond_2

    const-string v2, "loaded_person"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mGmsCallbacks:Lcom/google/android/gms/common/internal/IGmsCallbacks;

    invoke-interface {v2, p2, p3, v0}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method private sendCallback(Landroid/content/Context;Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    iget v2, p2, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mStatusCode:I

    iget-object v4, p2, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mResolution:Landroid/content/Intent;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 19
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAssertedAccountName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mCallingPackage:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->resolveAccountName(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    array-length v3, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const-string v3, "plus_one_placeholder_scope"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v15, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    array-length v3, v3

    if-eqz v3, :cond_1

    :cond_0
    if-eqz v15, :cond_3

    :cond_1
    const/4 v10, 0x0

    new-instance v3, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->createGmsClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->createCallerClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mVisibleActions:[Ljava/lang/String;

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/PlusService$OperationStarter;[Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move v6, v10

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V

    :goto_1
    return-void

    :cond_2
    const/4 v15, 0x0

    goto :goto_0

    :cond_3
    if-nez v17, :cond_5

    const-string v3, "<<default account>>"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mAssertedAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v5, 0x5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V

    goto :goto_1

    :cond_4
    const/4 v5, 0x4

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->createCallerClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/Intent;

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->createGmsClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->createCallerClientContext(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mRequiredFeatures:[Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v5, v6, v3}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->checkSignUp(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    move-result-object v18

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mStatusCode:I

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;)V

    goto :goto_1

    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->checkAuth(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    move-result-object v18

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;->mStatusCode:I

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;)V

    goto :goto_1

    :cond_7
    const/4 v12, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->getCallingUid()I

    move-result v3

    invoke-virtual {v5}, Lcom/google/android/gms/common/server/ClientContext;->getCallingUid()I

    move-result v4

    if-eq v3, v4, :cond_a

    const/4 v13, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    const-string v4, "https://www.googleapis.com/auth/plus.me"

    invoke-static {v3, v4}, Lcom/google/android/gms/common/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mScopes:[Ljava/lang/String;

    const-string v4, "https://www.googleapis.com/auth/plus.login"

    invoke-static {v3, v4}, Lcom/google/android/gms/common/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_8
    const/16 v16, 0x1

    :goto_3
    if-eqz v13, :cond_9

    if-eqz v16, :cond_9

    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/gms/plus/broker/DataBroker;->getCurrentPersonBytes(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v12

    :cond_9
    :goto_4
    const/4 v9, 0x0

    new-instance v3, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;

    new-instance v7, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mVisibleActions:[Ljava/lang/String;

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/PlusService$OperationStarter;[Ljava/lang/String;)V

    const/4 v11, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v10, v3

    invoke-direct/range {v7 .. v12}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;[B)V

    goto/16 :goto_1

    :cond_a
    const/4 v13, 0x0

    goto :goto_2

    :cond_b
    const/16 v16, 0x0

    goto :goto_3

    :catch_0
    move-exception v14

    new-instance v18, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v3, 0x6

    invoke-virtual {v14}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;)V

    goto/16 :goto_1

    :catch_1
    move-exception v14

    new-instance v18, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/gms/plus/PlusIntents;->newAccountSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->sendCallback(Landroid/content/Context;Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;)V

    goto/16 :goto_1

    :catch_2
    move-exception v3

    goto :goto_4
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mGmsCallbacks:Lcom/google/android/gms/common/internal/IGmsCallbacks;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;->mGmsCallbacks:Lcom/google/android/gms/common/internal/IGmsCallbacks;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
