.class public Lcom/google/android/gms/plus/service/PlusOperations;
.super Ljava/lang/Object;
.source "PlusOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/service/PlusOperations$DisconnectSourceOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$SetDefaultAccountOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$LoadAudienceOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$LoadLinkPreviewOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$WriteMomentOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$GetPersonOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$DeletePlusOneOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$InsertPlusOneOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;,
        Lcom/google/android/gms/plus/service/PlusOperations$BasePlusOneOperation;
    }
.end annotation


# static fields
.field static final FIRST_PARTY_SCOPES:[Ljava/lang/String;

.field static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/gms/plus/service/PlusOperations;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/service/PlusOperations;->TAG:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/service/PlusOperations;->FIRST_PARTY_SCOPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
