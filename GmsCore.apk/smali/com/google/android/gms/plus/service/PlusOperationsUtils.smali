.class public final Lcom/google/android/gms/plus/service/PlusOperationsUtils;
.super Ljava/lang/Object;
.source "PlusOperationsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAuth(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/UserRecoverableAuthException;,
            Ljava/io/IOException;,
            Lcom/google/android/gms/auth/GoogleAuthException;
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "androidPackageName"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    const-string v2, "request_visible_actions"

    const-string v3, " "

    invoke-static {v3, p5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v2, "callerUid"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p3, :cond_1

    :try_start_0
    invoke-static {p0, p4, p3, v0}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    return-object v2

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p4}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V

    throw v1
.end method

.method public static checkSignUp(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/plus/broker/DataBroker;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Lcom/google/android/gms/common/server/ClientContext;
    .param p4    # [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->getAccount(Landroid/accounts/AccountManager;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v1, v0, p4, v5, v5}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    :try_start_0
    invoke-static {p0, p3, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->getSignedUpStatus(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/accounts/AccountManagerFuture;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v3

    :goto_1
    new-instance v3, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/16 v4, 0x8

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_1

    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method private static getAccount(Landroid/accounts/AccountManager;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 4
    .param p0    # Landroid/accounts/AccountManager;
    .param p1    # Ljava/lang/String;

    const-string v3, "com.google"

    invoke-virtual {p0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object v3, v0, v2

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getSignedUpStatus(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/accounts/AccountManagerFuture;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p2}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->deletePlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    new-instance v1, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v2, 0x4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/plus/PlusIntents;->newSignUpIntent(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;-><init>(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public static resolveAccountName(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "<<default account>>"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0, p2}, Lcom/google/android/gms/common/account/AccountUtils;->getSelectedAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v4, 0x0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.google"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_0

    aget-object v5, v1, v3

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v5, v1, v3

    iget-object v4, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_0
    return-object v4

    :cond_1
    move-object v0, p1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
