.class public Lcom/google/android/gms/plus/service/PlusService$OperationStarter;
.super Ljava/lang/Object;
.source "PlusService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OperationStarter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    invoke-static {p1, p2}, Lcom/google/android/gms/plus/service/DefaultIntentService;->startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public startImage(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/service/OperationIntentService$Operation;

    invoke-static {p1, p2}, Lcom/google/android/gms/plus/service/ImageIntentService;->startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method
