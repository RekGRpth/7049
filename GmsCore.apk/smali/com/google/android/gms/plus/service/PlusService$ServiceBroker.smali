.class final Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;
.super Lcom/google/android/gms/common/internal/AbstractServiceBroker;
.source "PlusService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ServiceBroker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/service/PlusService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/service/PlusService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/AbstractServiceBroker;-><init>()V

    return-void
.end method


# virtual methods
.method public getPlusService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Lcom/google/android/gms/common/internal/IGmsCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Landroid/os/Bundle;

    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "clientVersion too old"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v6, 0x0

    if-eqz p7, :cond_1

    const-string v2, "request_visible_actions"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid callingPackage"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    invoke-static {v2, p3}, Lcom/google/android/gms/plus/service/PlusService;->verifyPackageName(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/PlusService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    :cond_3
    const/4 v10, 0x0

    if-eqz p7, :cond_4

    const-string v2, "skip_oob"

    const/4 v3, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    :cond_4
    if-eqz v10, :cond_6

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    new-instance v1, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;-><init>()V

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/PlusService$OperationStarter;[Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->asBinder()Landroid/os/IBinder;

    move-result-object v11

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-interface {p1, v2, v11, v3}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_5
    invoke-virtual/range {p3 .. p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "invalid authPackage"

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    const/4 v8, 0x0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/service/PlusService;->getUidFromPackage(Ljava/lang/String;)I

    move-result v7

    const-string v2, "required_features"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    :cond_7
    if-nez v8, :cond_8

    sget-object v8, Lcom/google/android/gms/common/internal/GmsClient;->GOOGLE_PLUS_REQUIRED_FEATURES:[Ljava/lang/String;

    :cond_8
    new-instance v1, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;

    move-object/from16 v2, p6

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/plus/service/PlusOperations$ValidateAccountOperation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lcom/google/android/gms/common/internal/IGmsCallbacks;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$ServiceBroker;->this$0:Lcom/google/android/gms/plus/service/PlusService;

    invoke-static {v2, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method
