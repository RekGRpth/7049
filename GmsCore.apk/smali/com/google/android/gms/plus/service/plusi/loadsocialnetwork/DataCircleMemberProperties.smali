.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCircleMemberProperties.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "inIncomingCircle"

    const-string v2, "inIncomingCircle"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "lastUpdateTime"

    const-string v2, "lastUpdateTime"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "photoUrl"

    const-string v2, "photoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "disallowedInteractions"

    const-string v2, "disallowedInteractions"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "firstNameSortKey"

    const-string v2, "firstNameSortKey"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "contactId"

    const-string v2, "contactId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "interactionsRankSortKey"

    const-string v2, "interactionsRankSortKey"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "lastNameSortKey"

    const-string v2, "lastNameSortKey"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "occupation"

    const-string v2, "occupation"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "bannerInfo"

    const-string v2, "bannerInfo"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "verified"

    const-string v2, "verified"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "inSameVisibilityGroup"

    const-string v2, "inSameVisibilityGroup"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "entityInfo"

    const-string v2, "entityInfo"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "profileType"

    const-string v2, "profileType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "location"

    const-string v2, "location"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "email"

    const-string v2, "email"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataEmail;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "tagLine"

    const-string v2, "tagLine"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "company"

    const-string v2, "company"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "phone"

    const-string v2, "phone"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPhone;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "address"

    const-string v2, "address"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "focusPhotoUrl"

    const-string v2, "focusPhotoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "school"

    const-string v2, "school"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "displayName"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "firstName"

    const-string v2, "firstName"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "gender"

    const-string v2, "gender"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "esUser"

    const-string v2, "esUser"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "contact"

    const-string v2, "contact"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "interactionsRank"

    const-string v2, "interactionsRank"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forDouble(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "inviter"

    const-string v2, "inviter"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Double;Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;
    .param p11    # Ljava/lang/Boolean;
    .param p12    # Ljava/lang/Boolean;
    .param p13    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .param p18    # Ljava/lang/String;
    .param p21    # Ljava/lang/String;
    .param p22    # Ljava/lang/String;
    .param p23    # Ljava/lang/String;
    .param p24    # Ljava/lang/String;
    .param p25    # Ljava/lang/String;
    .param p26    # Ljava/lang/Boolean;
    .param p27    # Ljava/lang/Boolean;
    .param p28    # Ljava/lang/Double;
    .param p29    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataEmail;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPhone;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    if-eqz p1, :cond_0

    const-string v1, "inIncomingCircle"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v1, "lastUpdateTime"

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "photoUrl"

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "disallowedInteractions"

    invoke-virtual {p0, v1, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "firstNameSortKey"

    invoke-virtual {p0, v1, p5}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "contactId"

    invoke-virtual {p0, v1, p6}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "interactionsRankSortKey"

    invoke-virtual {p0, v1, p7}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "lastNameSortKey"

    invoke-virtual {p0, v1, p8}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "occupation"

    invoke-virtual {p0, v1, p9}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "bannerInfo"

    invoke-virtual {p0, v1, p10}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p11, :cond_1

    const-string v1, "verified"

    invoke-virtual {p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_1
    if-eqz p12, :cond_2

    const-string v1, "inSameVisibilityGroup"

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_2
    const-string v1, "entityInfo"

    move-object/from16 v0, p13

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "profileType"

    move-object/from16 v0, p14

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "location"

    move-object/from16 v0, p15

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "email"

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "tagLine"

    move-object/from16 v0, p17

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "company"

    move-object/from16 v0, p18

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "phone"

    move-object/from16 v0, p19

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "address"

    move-object/from16 v0, p20

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "focusPhotoUrl"

    move-object/from16 v0, p21

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "school"

    move-object/from16 v0, p22

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "displayName"

    move-object/from16 v0, p23

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "firstName"

    move-object/from16 v0, p24

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "gender"

    move-object/from16 v0, p25

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p26, :cond_3

    const-string v1, "esUser"

    invoke-virtual/range {p26 .. p26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_3
    if-eqz p27, :cond_4

    const-string v1, "contact"

    invoke-virtual/range {p27 .. p27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_4
    if-eqz p28, :cond_5

    const-string v1, "interactionsRank"

    invoke-virtual/range {p28 .. p28}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setDouble(Ljava/lang/String;D)V

    :cond_5
    if-eqz p29, :cond_6

    const-string v1, "inviter"

    invoke-virtual/range {p29 .. p29}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_6
    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getAddress()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesAddress;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getBannerInfo()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "bannerInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;

    return-object v0
.end method

.method public getCompany()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "company"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getContactId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "contactId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDisallowedInteractions()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "disallowedInteractions"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "displayName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataEmail;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getEntityInfo()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "entityInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "firstName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFirstNameSortKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "firstNameSortKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFocusPhotoUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "focusPhotoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gender"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getInteractionsRank()Ljava/lang/Double;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "interactionsRank"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    return-object v0
.end method

.method public getInteractionsRankSortKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "interactionsRankSortKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLastNameSortKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "lastNameSortKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLastUpdateTime()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "lastUpdateTime"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOccupation()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "occupation"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataPhone;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPhotoUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProfileType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "profileType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSchool()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "school"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTagLine()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "tagLine"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isContact()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isEsUser()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "esUser"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isInIncomingCircle()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "inIncomingCircle"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isInSameVisibilityGroup()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "inSameVisibilityGroup"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isInviter()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "inviter"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isVerified()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "verified"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
