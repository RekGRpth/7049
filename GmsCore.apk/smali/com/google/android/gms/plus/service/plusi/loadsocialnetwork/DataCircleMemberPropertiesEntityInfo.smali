.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCircleMemberPropertiesEntityInfo.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mPrimaryAddress:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "primaryLink"

    const-string v2, "primaryLink"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "primaryAddress"

    const-string v2, "primaryAddress"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "plusPageType"

    const-string v2, "plusPageType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "primaryPhone"

    const-string v2, "primaryPhone"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "primaryLink"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "primaryAddress"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p3, :cond_0

    const-string v0, "plusPageType"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->setInteger(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "primaryPhone"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_1

    const-string v0, "type"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->setInteger(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->mPrimaryAddress:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPlusPageType()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "plusPageType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPrimaryAddress()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->mPrimaryAddress:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfoAddress;

    return-object v0
.end method

.method public getPrimaryLink()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "primaryLink"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryPhone()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "primaryPhone"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleMemberPropertiesEntityInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method
