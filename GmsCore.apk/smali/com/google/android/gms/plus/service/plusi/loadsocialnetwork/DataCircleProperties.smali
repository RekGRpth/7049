.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCircleProperties.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "category"

    const-string v2, "category"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "forSharing"

    const-string v2, "forSharing"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "backgroundImageUrl"

    const-string v2, "backgroundImageUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "lastUpdateTime"

    const-string v2, "lastUpdateTime"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "photoUrl"

    const-string v2, "photoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "memberCount"

    const-string v2, "memberCount"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "interactionsRank"

    const-string v2, "interactionsRank"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forDouble(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "memberCountOutsideDomain"

    const-string v2, "memberCountOutsideDomain"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "selectedForChat"

    const-string v2, "selectedForChat"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "forFollowing"

    const-string v2, "forFollowing"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "circleMode"

    const-string v2, "circleMode"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "nameSortKey"

    const-string v2, "nameSortKey"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "email"

    const-string v2, "email"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "circleType"

    const-string v2, "circleType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    const-string v1, "canHaveOutsideMembers"

    const-string v2, "canHaveOutsideMembers"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/Boolean;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/Integer;
    .param p8    # Ljava/lang/Double;
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/Integer;
    .param p11    # Ljava/lang/Boolean;
    .param p12    # Ljava/lang/Boolean;
    .param p13    # Ljava/lang/Integer;
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "category"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setInteger(Ljava/lang/String;I)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v1, "forSharing"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_1
    const-string v1, "backgroundImageUrl"

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "description"

    invoke-virtual {p0, v1, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "lastUpdateTime"

    invoke-virtual {p0, v1, p5}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "photoUrl"

    invoke-virtual {p0, v1, p6}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p7, :cond_2

    const-string v1, "memberCount"

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setInteger(Ljava/lang/String;I)V

    :cond_2
    if-eqz p8, :cond_3

    const-string v1, "interactionsRank"

    invoke-virtual {p8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setDouble(Ljava/lang/String;D)V

    :cond_3
    const-string v1, "name"

    invoke-virtual {p0, v1, p9}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p10, :cond_4

    const-string v1, "memberCountOutsideDomain"

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setInteger(Ljava/lang/String;I)V

    :cond_4
    if-eqz p11, :cond_5

    const-string v1, "selectedForChat"

    invoke-virtual {p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_5
    if-eqz p12, :cond_6

    const-string v1, "forFollowing"

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_6
    if-eqz p13, :cond_7

    const-string v1, "circleMode"

    invoke-virtual/range {p13 .. p13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setInteger(Ljava/lang/String;I)V

    :cond_7
    const-string v1, "nameSortKey"

    move-object/from16 v0, p14

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "email"

    move-object/from16 v0, p15

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "circleType"

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p17, :cond_8

    const-string v1, "canHaveOutsideMembers"

    invoke-virtual/range {p17 .. p17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->setBoolean(Ljava/lang/String;Z)V

    :cond_8
    return-void
.end method


# virtual methods
.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "backgroundImageUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "category"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getCircleMode()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "circleMode"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getCircleType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "circleType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "description"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getInteractionsRank()Ljava/lang/Double;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "interactionsRank"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    return-object v0
.end method

.method public getLastUpdateTime()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "lastUpdateTime"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMemberCount()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "memberCount"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getMemberCountOutsideDomain()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "memberCountOutsideDomain"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNameSortKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "nameSortKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isCanHaveOutsideMembers()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "canHaveOutsideMembers"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isForFollowing()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "forFollowing"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isForSharing()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "forSharing"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isSelectedForChat()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "selectedForChat"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
