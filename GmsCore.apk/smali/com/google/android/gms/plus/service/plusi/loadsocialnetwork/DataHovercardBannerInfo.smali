.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataHovercardBannerInfo.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "scrapbookInfo"

    const-string v2, "scrapbookInfo"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "fullBleedPhotoUrl"

    const-string v2, "fullBleedPhotoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "origFullBleedImage"

    const-string v2, "origFullBleedImage"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "scrapbookPhotoUrl"

    const-string v2, "scrapbookPhotoUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;Ljava/lang/String;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->mConcreteTypes:Ljava/util/HashMap;

    const-string v0, "scrapbookInfo"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "fullBleedPhotoUrl"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "origFullBleedImage"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "scrapbookPhotoUrl"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFullBleedPhotoUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "fullBleedPhotoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrigFullBleedImage()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "origFullBleedImage"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfoImage;

    return-object v0
.end method

.method public getScrapbookInfo()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "scrapbookInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;

    return-object v0
.end method

.method public getScrapbookPhotoUrl()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataHovercardBannerInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "scrapbookPhotoUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
