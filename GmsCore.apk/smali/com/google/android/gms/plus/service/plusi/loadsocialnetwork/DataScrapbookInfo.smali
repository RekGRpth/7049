.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataScrapbookInfo.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mFullBleedPhoto:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "fullBleedPhotoId"

    const-string v2, "fullBleedPhotoId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "fullBleedPhoto"

    const-string v2, "fullBleedPhoto"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->sFields:Ljava/util/HashMap;

    const-string v1, "layout"

    const-string v2, "layout"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "fullBleedPhotoId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "fullBleedPhoto"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "layout"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->mFullBleedPhoto:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFullBleedPhoto()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->mFullBleedPhoto:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;

    return-object v0
.end method

.method public getFullBleedPhotoId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "fullBleedPhotoId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLayout()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfo;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "layout"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
