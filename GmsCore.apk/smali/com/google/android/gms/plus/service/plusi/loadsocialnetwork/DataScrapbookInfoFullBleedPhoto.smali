.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataScrapbookInfoFullBleedPhoto.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mOffset:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->sFields:Ljava/util/HashMap;

    const-string v1, "photoOwnerType"

    const-string v2, "photoOwnerType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->sFields:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->sFields:Ljava/util/HashMap;

    const-string v1, "offset"

    const-string v2, "offset"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "photoOwnerType"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "offset"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->mOffset:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOffset()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->mOffset:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoOffset;

    return-object v0
.end method

.method public getPhotoOwnerType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataScrapbookInfoFullBleedPhoto;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoOwnerType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
