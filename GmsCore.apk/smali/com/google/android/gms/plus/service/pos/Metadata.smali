.class public Lcom/google/android/gms/plus/service/pos/Metadata;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "Metadata.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mGlobalCounts:Lcom/google/android/gms/plus/service/pos/GlobalCounts;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    const-string v1, "globalCounts"

    const-string v2, "globalCounts"

    const-class v3, Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    const-string v1, "adgroupId"

    const-string v2, "adgroupId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    const-string v1, "creativeId"

    const-string v2, "creativeId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/pos/GlobalCounts;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/service/pos/GlobalCounts;
    .param p2    # Ljava/lang/Long;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Long;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    const-string v0, "globalCounts"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/pos/Metadata;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p2, :cond_0

    const-string v0, "adgroupId"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/pos/Metadata;->setLong(Ljava/lang/String;J)V

    :cond_0
    const-string v0, "type"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/pos/Metadata;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_1

    const-string v0, "creativeId"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/pos/Metadata;->setLong(Ljava/lang/String;J)V

    :cond_1
    const-string v0, "title"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/pos/Metadata;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/pos/Metadata;->mGlobalCounts:Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    return-void
.end method

.method public getAdgroupId()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Metadata;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "adgroupId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getCreativeId()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Metadata;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "creativeId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Metadata;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getGlobalCounts()Lcom/google/android/gms/plus/service/pos/GlobalCounts;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/Metadata;->mGlobalCounts:Lcom/google/android/gms/plus/service/pos/GlobalCounts;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Metadata;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/pos/Metadata;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
