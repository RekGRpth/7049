.class public Lcom/google/android/gms/plus/service/pos/Plusones;
.super Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;
.source "Plusones.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mMetadata:Lcom/google/android/gms/plus/service/pos/Metadata;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/pos/Plusones;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Plusones;->sFields:Ljava/util/HashMap;

    const-string v1, "abtk"

    const-string v2, "token"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Plusones;->sFields:Ljava/util/HashMap;

    const-string v1, "isSetByViewer"

    const-string v2, "state"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Plusones;->sFields:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const-class v3, Lcom/google/android/gms/plus/service/pos/Metadata;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 0
    .param p1    # Landroid/content/ContentValues;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>(Landroid/content/ContentValues;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/pos/Metadata;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Boolean;
    .param p3    # Lcom/google/android/gms/plus/service/pos/Metadata;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    const-string v0, "token"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/pos/Plusones;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string v0, "state"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/pos/Plusones;->setBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "metadata"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/pos/Plusones;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/pos/Metadata;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/pos/Plusones;->mMetadata:Lcom/google/android/gms/plus/service/pos/Metadata;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/pos/Plusones;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getMetadata()Lcom/google/android/gms/plus/service/pos/Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/Plusones;->mMetadata:Lcom/google/android/gms/plus/service/pos/Metadata;

    return-object v0
.end method
