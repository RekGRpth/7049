.class public Lcom/google/android/gms/plus/service/rpc/PlusoneResult;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "PlusoneResult.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mResult:Lcom/google/android/gms/plus/service/rpc/Plusone;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->sFields:Ljava/util/HashMap;

    const-string v1, "result"

    const-string v2, "result"

    const-class v3, Lcom/google/android/gms/plus/service/rpc/Plusone;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/rpc/Plusone;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->mResult:Lcom/google/android/gms/plus/service/rpc/Plusone;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getResult()Lcom/google/android/gms/plus/service/rpc/Plusone;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/rpc/PlusoneResult;->mResult:Lcom/google/android/gms/plus/service/rpc/Plusone;

    return-object v0
.end method
