.class public Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;
.super Ljava/lang/Object;
.source "ActivitiesApi.java"


# instance fields
.field private final mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    return-void
.end method

.method private static append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v1, "?"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const-string v0, "&"

    :goto_0
    const-string v1, "%s%s%s=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const-string v0, "?"

    goto :goto_0
.end method

.method private static enc(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static urlForInsert(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Ljava/lang/String;

    const-string v1, "people/%1$s/activities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "preview"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz p2, :cond_1

    const-string v1, "key"

    invoke-static {p2}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public insertBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/PlusObject;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Actor;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Acl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/whitelisted/Activity;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {p2, p3, p4}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->urlForInsert(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    if-eqz p5, :cond_0

    :try_start_0
    const-string v1, "placeName"

    invoke-virtual {v5, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    if-eqz p6, :cond_1

    const-string v1, "updated"

    invoke-virtual {v5, v1, p6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    if-eqz p7, :cond_2

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {p7}, Lcom/google/android/gms/plus/service/whitelisted/Provider;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "provider"

    invoke-virtual {v5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    if-eqz p8, :cond_3

    const-string v1, "title"

    invoke-virtual {v5, v1, p8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    if-eqz p9, :cond_4

    const-string v1, "url"

    move-object/from16 v0, p9

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    if-eqz p10, :cond_5

    const-string v1, "geocode"

    move-object/from16 v0, p10

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_5
    if-eqz p11, :cond_6

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/plus/service/whitelisted/PlusObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "object"

    invoke-virtual {v5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_6
    if-eqz p12, :cond_7

    const-string v1, "placeId"

    move-object/from16 v0, p12

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_7
    if-eqz p13, :cond_8

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/plus/service/whitelisted/Actor;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "actor"

    invoke-virtual {v5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_8
    if-eqz p14, :cond_9

    const-string v1, "id"

    move-object/from16 v0, p14

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_9
    if-eqz p15, :cond_a

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual/range {p15 .. p15}, Lcom/google/android/gms/plus/service/whitelisted/Acl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "access"

    invoke-virtual {v5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_a
    if-eqz p16, :cond_b

    const-string v1, "verb"

    move-object/from16 v0, p16

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_b
    if-eqz p17, :cond_c

    const-string v1, "etag"

    move-object/from16 v0, p17

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_c
    if-eqz p18, :cond_d

    const-string v1, "radius"

    move-object/from16 v0, p18

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_d
    if-eqz p19, :cond_e

    const-string v1, "address"

    move-object/from16 v0, p19

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_e
    if-eqz p20, :cond_f

    const-string v1, "crosspostSource"

    move-object/from16 v0, p20

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_f
    if-eqz p21, :cond_10

    const-string v1, "annotation"

    move-object/from16 v0, p21

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_10
    if-eqz p22, :cond_11

    const-string v1, "published"

    move-object/from16 v0, p22

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_11
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/plus/service/whitelisted/Activity;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/whitelisted/Activity;

    return-object v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method
