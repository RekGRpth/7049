.class public Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "ClientActionData.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "intrCelebsClick"

    const-string v2, "intrCelebsClick"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "ribbonClick"

    const-string v2, "ribbonClick"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "photoId"

    const-string v2, "photoId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "photoAlbumId"

    const-string v2, "photoAlbumId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "labelId"

    const-string v2, "labelId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "billboardPromoAction"

    const-string v2, "billboardPromoAction"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "shareboxInfo"

    const-string v2, "shareboxInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "gadgetId"

    const-string v2, "gadgetId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    const-string v2, "suggestionInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "rhsComponent"

    const-string v2, "rhsComponent"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "autoComplete"

    const-string v2, "autoComplete"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "autoCompleteQuery"

    const-string v2, "autoCompleteQuery"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionSummaryInfo"

    const-string v2, "suggestionSummaryInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "billboardImpression"

    const-string v2, "billboardImpression"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "circle"

    const-string v2, "circle"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircle;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "obfuscatedGaiaId"

    const-string v2, "obfuscatedGaiaId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "plusEventId"

    const-string v2, "plusEventId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "circleMember"

    const-string v2, "circleMember"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircleMember;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    const-string v1, "ribbonOrder"

    const-string v2, "ribbonOrder"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;
    .param p2    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;
    .param p7    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;
    .param p8    # Ljava/lang/String;
    .param p10    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;
    .param p11    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;
    .param p12    # Ljava/lang/String;
    .param p13    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;
    .param p14    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;
    .param p17    # Ljava/lang/String;
    .param p19    # Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionInfo;",
            ">;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircle;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircleMember;",
            ">;",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "intrCelebsClick"

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "ribbonClick"

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "photoId"

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "photoAlbumId"

    invoke-virtual {p0, v1, p4}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "labelId"

    invoke-virtual {p0, v1, p5}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "billboardPromoAction"

    invoke-virtual {p0, v1, p6}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "shareboxInfo"

    invoke-virtual {p0, v1, p7}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "gadgetId"

    invoke-virtual {p0, v1, p8}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "suggestionInfo"

    invoke-virtual {p0, v1, p9}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "rhsComponent"

    invoke-virtual {p0, v1, p10}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "autoComplete"

    invoke-virtual {p0, v1, p11}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "autoCompleteQuery"

    invoke-virtual {p0, v1, p12}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "suggestionSummaryInfo"

    invoke-virtual {p0, v1, p13}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "billboardImpression"

    move-object/from16 v0, p14

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "circle"

    move-object/from16 v0, p15

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "obfuscatedGaiaId"

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "plusEventId"

    move-object/from16 v0, p17

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "circleMember"

    move-object/from16 v0, p18

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "ribbonOrder"

    move-object/from16 v0, p19

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getAutoComplete()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "autoComplete"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedAutoComplete;

    return-object v0
.end method

.method public getAutoCompleteQuery()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "autoCompleteQuery"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getBillboardImpression()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "billboardImpression"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardImpression;

    return-object v0
.end method

.method public getBillboardPromoAction()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "billboardPromoAction"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedBillboardPromoAction;

    return-object v0
.end method

.method public getCircle()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "circle"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCircleMember()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "circleMember"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getGadgetId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gadgetId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getIntrCelebsClick()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "intrCelebsClick"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedIntrCelebsClick;

    return-object v0
.end method

.method public getLabelId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "labelId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getObfuscatedGaiaId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "obfuscatedGaiaId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPhotoAlbumId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoAlbumId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPlusEventId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "plusEventId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRhsComponent()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "rhsComponent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRhsComponent;

    return-object v0
.end method

.method public getRibbonClick()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "ribbonClick"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonClick;

    return-object v0
.end method

.method public getRibbonOrder()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "ribbonOrder"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedRibbonOrder;

    return-object v0
.end method

.method public getShareboxInfo()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "shareboxInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedShareboxInfo;

    return-object v0
.end method

.method public getSuggestionInfo()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSuggestionSummaryInfo()Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "suggestionSummaryInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ClientLoggedSuggestionSummaryInfo;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ClientActionData;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
