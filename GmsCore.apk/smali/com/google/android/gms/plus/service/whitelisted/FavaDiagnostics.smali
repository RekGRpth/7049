.class public Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "FavaDiagnostics.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "timeMs"

    const-string v2, "timeMs"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forIntegers(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "errorCode"

    const-string v2, "errorCode"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "actionType"

    const-string v2, "actionType"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "screenWidth"

    const-string v2, "screenWidth"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "endView"

    const-string v2, "endView"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "actionNumber"

    const-string v2, "actionNumber"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "numRequests"

    const-string v2, "numRequests"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "memoryStats"

    const-string v2, "memoryStats"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "requestId"

    const-string v2, "requestId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLongs(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "viewportWidth"

    const-string v2, "viewportWidth"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "mainPageId"

    const-string v2, "mainPageId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "requestStats"

    const-string v2, "requestStats"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsRequestStat;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "tracers"

    const-string v2, "tracers"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "jsLoadTimeMs"

    const-string v2, "jsLoadTimeMs"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "jsVersion"

    const-string v2, "jsVersion"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "viewportHeight"

    const-string v2, "viewportHeight"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "requiredJsLoad"

    const-string v2, "requiredJsLoad"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "numLogicalRequests"

    const-string v2, "numLogicalRequests"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "screenHeight"

    const-string v2, "screenHeight"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "timeUsecDelta"

    const-string v2, "timeUsecDelta"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "experimentIds"

    const-string v2, "experimentIds"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forIntegers(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "totalTimeMs"

    const-string v2, "totalTimeMs"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "isCacheHit"

    const-string v2, "isCacheHit"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    const-string v1, "startView"

    const-string v2, "startView"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Long;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;)V
    .locals 4
    .param p3    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .param p6    # Ljava/lang/Integer;
    .param p7    # Ljava/lang/Integer;
    .param p8    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;
    .param p10    # Ljava/lang/Integer;
    .param p11    # Ljava/lang/Long;
    .param p13    # Ljava/lang/String;
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/Integer;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/Integer;
    .param p18    # Ljava/lang/Boolean;
    .param p19    # Ljava/lang/Integer;
    .param p20    # Ljava/lang/Integer;
    .param p21    # Ljava/lang/Long;
    .param p23    # Ljava/lang/Integer;
    .param p24    # Ljava/lang/Boolean;
    .param p25    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsRequestStat;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "timeMs"

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setIntegers(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "errorCode"

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "actionType"

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p4, :cond_0

    const-string v1, "screenWidth"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_0
    const-string v1, "endView"

    invoke-virtual {p0, v1, p5}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p6, :cond_1

    const-string v1, "actionNumber"

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_1
    if-eqz p7, :cond_2

    const-string v1, "numRequests"

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_2
    const-string v1, "memoryStats"

    invoke-virtual {p0, v1, p8}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "requestId"

    invoke-virtual {p0, v1, p9}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setLongs(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p10, :cond_3

    const-string v1, "viewportWidth"

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_3
    if-eqz p11, :cond_4

    const-string v1, "mainPageId"

    invoke-virtual {p11}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setLong(Ljava/lang/String;J)V

    :cond_4
    const-string v1, "requestStats"

    move-object/from16 v0, p12

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "status"

    move-object/from16 v0, p13

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tracers"

    move-object/from16 v0, p14

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p15, :cond_5

    const-string v1, "jsLoadTimeMs"

    invoke-virtual/range {p15 .. p15}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_5
    const-string v1, "jsVersion"

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p17, :cond_6

    const-string v1, "viewportHeight"

    invoke-virtual/range {p17 .. p17}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_6
    if-eqz p18, :cond_7

    const-string v1, "requiredJsLoad"

    invoke-virtual/range {p18 .. p18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setBoolean(Ljava/lang/String;Z)V

    :cond_7
    if-eqz p19, :cond_8

    const-string v1, "numLogicalRequests"

    invoke-virtual/range {p19 .. p19}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_8
    if-eqz p20, :cond_9

    const-string v1, "screenHeight"

    invoke-virtual/range {p20 .. p20}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_9
    if-eqz p21, :cond_a

    const-string v1, "timeUsecDelta"

    invoke-virtual/range {p21 .. p21}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setLong(Ljava/lang/String;J)V

    :cond_a
    const-string v1, "experimentIds"

    move-object/from16 v0, p22

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setIntegers(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p23, :cond_b

    const-string v1, "totalTimeMs"

    invoke-virtual/range {p23 .. p23}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setInteger(Ljava/lang/String;I)V

    :cond_b
    if-eqz p24, :cond_c

    const-string v1, "isCacheHit"

    invoke-virtual/range {p24 .. p24}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->setBoolean(Ljava/lang/String;Z)V

    :cond_c
    const-string v1, "startView"

    move-object/from16 v0, p25

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getActionNumber()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "actionNumber"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getActionType()Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "actionType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    return-object v0
.end method

.method public getEndView()Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "endView"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    return-object v0
.end method

.method public getErrorCode()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExperimentIds()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "experimentIds"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getJsLoadTimeMs()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "jsLoadTimeMs"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getJsVersion()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "jsVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMainPageId()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "mainPageId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getMemoryStats()Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "memoryStats"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;

    return-object v0
.end method

.method public getNumLogicalRequests()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numLogicalRequests"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getNumRequests()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numRequests"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getRequestId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "requestId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRequestStats()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsRequestStat;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "requestStats"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getScreenHeight()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "screenHeight"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getScreenWidth()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "screenWidth"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getStartView()Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "startView"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsNamespacedType;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTimeMs()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "timeMs"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTimeUsecDelta()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "timeUsecDelta"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getTotalTimeMs()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "totalTimeMs"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getTracers()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "tracers"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getViewportHeight()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "viewportHeight"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getViewportWidth()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "viewportWidth"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isIsCacheHit()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "isCacheHit"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isRequiredJsLoad()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "requiredJsLoad"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
