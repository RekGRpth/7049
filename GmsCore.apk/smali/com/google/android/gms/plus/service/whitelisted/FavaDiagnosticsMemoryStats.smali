.class public Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "FavaDiagnosticsMemoryStats.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->sFields:Ljava/util/HashMap;

    const-string v1, "totalJsHeapSize"

    const-string v2, "totalJsHeapSize"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->sFields:Ljava/util/HashMap;

    const-string v1, "usedJsHeapSize"

    const-string v2, "usedJsHeapSize"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->sFields:Ljava/util/HashMap;

    const-string v1, "jsHeapSizeLimit"

    const-string v2, "jsHeapSizeLimit"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 3
    .param p1    # Ljava/lang/Long;
    .param p2    # Ljava/lang/Long;
    .param p3    # Ljava/lang/Long;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    if-eqz p1, :cond_0

    const-string v0, "totalJsHeapSize"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->setLong(Ljava/lang/String;J)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "usedJsHeapSize"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->setLong(Ljava/lang/String;J)V

    :cond_1
    if-eqz p3, :cond_2

    const-string v0, "jsHeapSizeLimit"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->setLong(Ljava/lang/String;J)V

    :cond_2
    return-void
.end method


# virtual methods
.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getJsHeapSizeLimit()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "jsHeapSizeLimit"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getTotalJsHeapSize()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "totalJsHeapSize"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getUsedJsHeapSize()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnosticsMemoryStats;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "usedJsHeapSize"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method
