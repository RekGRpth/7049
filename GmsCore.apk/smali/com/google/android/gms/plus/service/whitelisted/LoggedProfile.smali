.class public Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoggedProfile.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->sFields:Ljava/util/HashMap;

    const-string v1, "profileData"

    const-string v2, "profileData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfileData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->sFields:Ljava/util/HashMap;

    const-string v1, "numContacts"

    const-string v2, "numContacts"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->sFields:Ljava/util/HashMap;

    const-string v1, "contactData"

    const-string v2, "contactData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedContactData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/whitelisted/LoggedProfileData;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/LoggedContactData;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/LoggedProfileData;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Lcom/google/android/gms/plus/service/whitelisted/LoggedContactData;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    const-string v0, "profileData"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p2, :cond_0

    const-string v0, "numContacts"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->setInteger(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "contactData"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getContactData()Lcom/google/android/gms/plus/service/whitelisted/LoggedContactData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "contactData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedContactData;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getNumContacts()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numContacts"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getProfileData()Lcom/google/android/gms/plus/service/whitelisted/LoggedProfileData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "profileData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfileData;

    return-object v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedProfile;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
