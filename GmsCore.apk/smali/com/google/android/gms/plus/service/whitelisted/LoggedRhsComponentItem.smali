.class public Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoggedRhsComponentItem.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    const-string v2, "suggestionInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "connectSiteId"

    const-string v2, "connectSiteId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forIntegers(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "gamesLabelId"

    const-string v2, "gamesLabelId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "person"

    const-string v2, "person"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "circle"

    const-string v2, "circle"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "col"

    const-string v2, "col"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    const-string v1, "row"

    const-string v2, "row"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;
    .param p6    # Ljava/lang/Integer;
    .param p7    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v0, "suggestionInfo"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "connectSiteId"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->setIntegers(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "gamesLabelId"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "person"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "circle"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p6, :cond_0

    const-string v0, "col"

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->setInteger(Ljava/lang/String;I)V

    :cond_0
    if-eqz p7, :cond_1

    const-string v0, "row"

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->setInteger(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getCircle()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "circle"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCol()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "col"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getConnectSiteId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "connectSiteId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getGamesLabelId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gamesLabelId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPerson()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "person"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRow()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "row"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getSuggestionInfo()Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponentItem;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
