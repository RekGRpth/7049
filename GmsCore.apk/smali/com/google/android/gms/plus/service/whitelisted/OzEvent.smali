.class public Lcom/google/android/gms/plus/service/whitelisted/OzEvent;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "OzEvent.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "timestampUsecDelta"

    const-string v2, "timestampUsecDelta"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "overallUserSegment"

    const-string v2, "overallUserSegment"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "actionTarget"

    const-string v2, "actionTarget"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "interstitialRedirectorData"

    const-string v2, "interstitialRedirectorData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/InterstitialRedirectorData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "isNativePlatformEvent"

    const-string v2, "isNativePlatformEvent"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "badTiming"

    const-string v2, "badTiming"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "background"

    const-string v2, "background"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "favaDiagnostics"

    const-string v2, "favaDiagnostics"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "startViewData"

    const-string v2, "startViewData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/OutputData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "endViewData"

    const-string v2, "endViewData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/OutputData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    const-string v1, "inputData"

    const-string v2, "inputData"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/InputData;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;Lcom/google/android/gms/plus/service/whitelisted/InterstitialRedirectorData;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;Lcom/google/android/gms/plus/service/whitelisted/OutputData;Lcom/google/android/gms/plus/service/whitelisted/OutputData;Lcom/google/android/gms/plus/service/whitelisted/InputData;)V
    .locals 3
    .param p1    # Ljava/lang/Long;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;
    .param p4    # Lcom/google/android/gms/plus/service/whitelisted/InterstitialRedirectorData;
    .param p5    # Ljava/lang/Boolean;
    .param p6    # Ljava/lang/Boolean;
    .param p7    # Ljava/lang/Boolean;
    .param p8    # Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;
    .param p9    # Lcom/google/android/gms/plus/service/whitelisted/OutputData;
    .param p10    # Lcom/google/android/gms/plus/service/whitelisted/OutputData;
    .param p11    # Lcom/google/android/gms/plus/service/whitelisted/InputData;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    if-eqz p1, :cond_0

    const-string v0, "timestampUsecDelta"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->setLong(Ljava/lang/String;J)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "overallUserSegment"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->setInteger(Ljava/lang/String;I)V

    :cond_1
    const-string v0, "actionTarget"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "interstitialRedirectorData"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p5, :cond_2

    const-string v0, "isNativePlatformEvent"

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->setBoolean(Ljava/lang/String;Z)V

    :cond_2
    if-eqz p6, :cond_3

    const-string v0, "badTiming"

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->setBoolean(Ljava/lang/String;Z)V

    :cond_3
    if-eqz p7, :cond_4

    const-string v0, "background"

    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->setBoolean(Ljava/lang/String;Z)V

    :cond_4
    const-string v0, "favaDiagnostics"

    invoke-virtual {p0, v0, p8}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "startViewData"

    invoke-virtual {p0, v0, p9}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "endViewData"

    invoke-virtual {p0, v0, p10}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "inputData"

    invoke-virtual {p0, v0, p11}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getActionTarget()Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "actionTarget"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;

    return-object v0
.end method

.method public getEndViewData()Lcom/google/android/gms/plus/service/whitelisted/OutputData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "endViewData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/OutputData;

    return-object v0
.end method

.method public getFavaDiagnostics()Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "favaDiagnostics"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FavaDiagnostics;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getInputData()Lcom/google/android/gms/plus/service/whitelisted/InputData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "inputData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/InputData;

    return-object v0
.end method

.method public getInterstitialRedirectorData()Lcom/google/android/gms/plus/service/whitelisted/InterstitialRedirectorData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "interstitialRedirectorData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/InterstitialRedirectorData;

    return-object v0
.end method

.method public getOverallUserSegment()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "overallUserSegment"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getStartViewData()Lcom/google/android/gms/plus/service/whitelisted/OutputData;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "startViewData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/OutputData;

    return-object v0
.end method

.method public getTimestampUsecDelta()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "timestampUsecDelta"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public isBackground()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "background"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isBadTiming()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "badTiming"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isIsNativePlatformEvent()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/OzEvent;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "isNativePlatformEvent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
