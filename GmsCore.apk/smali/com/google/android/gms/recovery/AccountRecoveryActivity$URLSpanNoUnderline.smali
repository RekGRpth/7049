.class Lcom/google/android/gms/recovery/AccountRecoveryActivity$URLSpanNoUnderline;
.super Landroid/text/style/URLSpan;
.source "AccountRecoveryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/recovery/AccountRecoveryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "URLSpanNoUnderline"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$URLSpanNoUnderline;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0, p2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1    # Landroid/text/TextPaint;

    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->updateDrawState(Landroid/text/TextPaint;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    return-void
.end method
