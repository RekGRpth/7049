.class public Lcom/google/android/gms/recovery/AccountRecoveryService;
.super Landroid/app/IntentService;
.source "AccountRecoveryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/recovery/AccountRecoveryService$Receiver;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "AccountRecoveryServer"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private getGoogleAccounts()[Landroid/accounts/Account;
    .locals 2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    return-object v1
.end method

.method private getNewAlarmTimeMs()J
    .locals 9

    invoke-static {}, Lcom/google/android/gms/recovery/RecoveryState;->getSingleton()Lcom/google/android/gms/recovery/RecoveryState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/recovery/RecoveryState;->getRecoveryPolicy()Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    move-result-object v2

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getCurrentTimeMs()J

    move-result-wide v5

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->getPolicySilenceMs()J

    move-result-wide v7

    add-long/2addr v5, v7

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->getPolicySilenceRandomizePeriodMs()J

    move-result-wide v7

    long-to-double v7, v7

    mul-double/2addr v7, v0

    double-to-long v7, v7

    add-long/2addr v5, v7

    return-wide v5
.end method

.method private getNewAlarmTimeMsForDisabledRecovery()J
    .locals 7

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getCurrentTimeMs()J

    move-result-wide v3

    const-wide v5, 0x4194997000000000L

    mul-double/2addr v5, v0

    double-to-long v5, v5

    add-long/2addr v3, v5

    return-wide v3
.end method

.method private isNotificationDueLocally()Z
    .locals 5

    invoke-static {}, Lcom/google/android/gms/recovery/RecoveryState;->getSingleton()Lcom/google/android/gms/recovery/RecoveryState;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/recovery/RecoveryState;->getNextAlarmTimeMs(J)J

    move-result-wide v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gez v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getNewAlarmTimeMs()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/recovery/RecoveryState;->setNextAlarmTimeMs(J)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v3, v3, v0

    if-ltz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isRecoveryEnabledByServer()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "account_recovery_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Recovery"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Recovery"

    const-string v2, "Account recovery is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method private sendNotification(Lcom/google/android/gms/recovery/RecoveryResponse;I)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Action"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAction()Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Email"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PhoneNumber"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Country"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getDefaultCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "CountryList"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getCountryList()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getDetail()Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "Detail"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getDetail()Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getSecondaryEmail()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "SecondaryEmail"

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getSecondaryEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020056

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v2, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b00fc

    invoke-virtual {p0, v3}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x108008a

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const v1, 0x7f0b00fb

    invoke-virtual {p0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, p2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private setAlarm(J)V
    .locals 5
    .param p1    # J

    const/4 v4, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.recovery.WAKEUP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v4, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method protected getCurrentTimeMs()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 16
    .param p1    # Landroid/content/Intent;

    const-string v13, "Recovery"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    const-string v13, "Recovery"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, "Recovery"

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/recovery/RecoveryState;->getSingleton()Lcom/google/android/gms/recovery/RecoveryState;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->isRecoveryEnabledByServer()Z

    move-result v13

    if-nez v13, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getNewAlarmTimeMsForDisabledRecovery()J

    move-result-wide v6

    invoke-virtual {v12, v6, v7}, Lcom/google/android/gms/recovery/RecoveryState;->setNextAlarmTimeMs(J)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/gms/recovery/AccountRecoveryService;->setAlarm(J)V

    :goto_0
    return-void

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->isNotificationDueLocally()Z

    move-result v13

    if-nez v13, :cond_3

    const-wide/16 v13, -0x1

    invoke-virtual {v12, v13, v14}, Lcom/google/android/gms/recovery/RecoveryState;->getNextAlarmTimeMs(J)J

    move-result-wide v6

    const-wide/16 v13, 0x0

    cmp-long v13, v6, v13

    if-gez v13, :cond_2

    const-string v13, "Recovery"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Illegal next alarm time: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getNewAlarmTimeMs()J

    move-result-wide v6

    invoke-virtual {v12, v6, v7}, Lcom/google/android/gms/recovery/RecoveryState;->setNextAlarmTimeMs(J)V

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/gms/recovery/AccountRecoveryService;->setAlarm(J)V

    goto :goto_0

    :cond_3
    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/gms/recovery/RecoveryServerAdapter;->getSingleton()Lcom/google/android/gms/recovery/RecoveryServerAdapter;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v5, v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_7

    aget-object v1, v2, v4

    :try_start_0
    iget-object v13, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/google/android/gms/recovery/RecoveryState;->getAccountRecord(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;

    move-result-object v9

    iget-object v13, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v11, v13}, Lcom/google/android/gms/recovery/RecoveryServerAdapter;->getRecoveryAction(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryResponse;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/recovery/RecoveryResponse;->getError()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_4

    const-string v13, "Recovery"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error while talking to server: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/google/android/gms/recovery/RecoveryResponse;->getError()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v10}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAction()Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    move-result-object v13

    sget-object v14, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->REQUEST_RECOVERY_INFO:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    if-eq v13, v14, :cond_5

    invoke-virtual {v10}, Lcom/google/android/gms/recovery/RecoveryResponse;->getAction()Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    move-result-object v13

    sget-object v14, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->VERIFY_RECOVERY_INFO:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    if-ne v13, v14, :cond_6

    :cond_5
    invoke-virtual {v9}, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;->getNotificationId()I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v13}, Lcom/google/android/gms/recovery/AccountRecoveryService;->sendNotification(Lcom/google/android/gms/recovery/RecoveryResponse;I)V

    :cond_6
    invoke-virtual {v10}, Lcom/google/android/gms/recovery/RecoveryResponse;->getPolicy()Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_2

    :catch_0
    move-exception v3

    const-string v13, "Recovery"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error while checking recovery info for "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_7
    invoke-virtual {v12, v8}, Lcom/google/android/gms/recovery/RecoveryState;->setRecoveryPolicy(Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/recovery/AccountRecoveryService;->getNewAlarmTimeMs()J

    move-result-wide v6

    invoke-virtual {v12, v6, v7}, Lcom/google/android/gms/recovery/RecoveryState;->setNextAlarmTimeMs(J)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/gms/recovery/AccountRecoveryService;->setAlarm(J)V

    goto/16 :goto_0
.end method
