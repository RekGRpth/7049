.class public Lcom/google/android/gms/recovery/RecoveryResponse;
.super Ljava/lang/Object;
.source "RecoveryResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/recovery/RecoveryResponse$Detail;,
        Lcom/google/android/gms/recovery/RecoveryResponse$Action;
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

.field private mCountryList:Ljava/lang/String;

.field private mDefaultCountryCode:Ljava/lang/String;

.field private mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

.field private mError:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPolicy:Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

.field private mSecondaryEmail:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/String;

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static make(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/gms/recovery/RecoveryResponse;
    .locals 9
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/recovery/RecoveryResponse;"
        }
    .end annotation

    new-instance v2, Lcom/google/android/gms/recovery/RecoveryResponse;

    invoke-direct {v2}, Lcom/google/android/gms/recovery/RecoveryResponse;-><init>()V

    const-string v4, "Account cannot be null"

    invoke-static {p0, v4}, Lcom/google/android/gms/recovery/RecoveryResponse;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mAccount:Ljava/lang/String;

    const-string v4, "Error"

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v5, "Recovery"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error while updating recovery info: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v4, "Error"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Error"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mError:Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    const-string v4, "Action"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/recovery/RecoveryResponse$Action;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v4, "Detail"

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_1
    const-string v4, "Detail"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/recovery/RecoveryResponse$Detail;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    const-string v4, "SecondaryEmail"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mSecondaryEmail:Ljava/lang/String;

    const-string v4, "PhoneNumber"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mPhoneNumber:Ljava/lang/String;

    const-string v4, "CountryList"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mCountryList:Ljava/lang/String;

    const-string v4, "Country"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mDefaultCountryCode:Ljava/lang/String;

    const-string v4, "PolicySilenceMs"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "PolicySilenceRandomizePeriodMs"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :try_start_2
    new-instance v4, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;-><init>(JJ)V

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mPolicy:Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const-string v4, "Recovery"

    const-string v5, "Policy information not received from server"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mPolicy:Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    :cond_3
    const-string v4, "Recovery"

    const-string v5, "Error while parsing policy info"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "BadResponse"

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mError:Ljava/lang/String;

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v5, "Recovery"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad value for Action: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v4, "Action"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "BadResponse"

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mError:Ljava/lang/String;

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v5, "Recovery"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad value for Detail: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v4, "Detail"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "BadResponse"

    iput-object v4, v2, Lcom/google/android/gms/recovery/RecoveryResponse;->mError:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getAction()Lcom/google/android/gms/recovery/RecoveryResponse$Action;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mAction:Lcom/google/android/gms/recovery/RecoveryResponse$Action;

    return-object v0
.end method

.method public getCountryList()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mCountryList:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultCountryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mDefaultCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDetail()Lcom/google/android/gms/recovery/RecoveryResponse$Detail;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mDetail:Lcom/google/android/gms/recovery/RecoveryResponse$Detail;

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mError:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicy()Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mPolicy:Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;

    return-object v0
.end method

.method public getSecondaryEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/recovery/RecoveryResponse;->mSecondaryEmail:Ljava/lang/String;

    return-object v0
.end method
