.class public Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;
.super Ljava/lang/Object;
.source "RecoveryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/recovery/RecoveryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountRecord"
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mNotificationId:I


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;->mAccount:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;->mNotificationId:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/gms/recovery/RecoveryState$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gms/recovery/RecoveryState$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public getNotificationId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/recovery/RecoveryState$AccountRecord;->mNotificationId:I

    return v0
.end method
