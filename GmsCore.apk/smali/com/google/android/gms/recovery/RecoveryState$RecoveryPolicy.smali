.class public Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;
.super Ljava/lang/Object;
.source "RecoveryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/recovery/RecoveryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecoveryPolicy"
.end annotation


# instance fields
.field private final mPolicySilenceMs:J

.field private final mPolicySilenceRandomizePeriodMs:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->mPolicySilenceMs:J

    iput-wide p3, p0, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->mPolicySilenceRandomizePeriodMs:J

    return-void
.end method


# virtual methods
.method public getPolicySilenceMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->mPolicySilenceMs:J

    return-wide v0
.end method

.method public getPolicySilenceRandomizePeriodMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/recovery/RecoveryState$RecoveryPolicy;->mPolicySilenceRandomizePeriodMs:J

    return-wide v0
.end method
