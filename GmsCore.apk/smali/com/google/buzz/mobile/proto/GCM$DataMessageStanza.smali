.class public final Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DataMessageStanza"
.end annotation


# instance fields
.field private appData_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$AppData;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private category_:Ljava/lang/String;

.field private clientId_:Ljava/lang/String;

.field private deviceUserId_:I

.field private fromTrustedServer_:Z

.field private from_:Ljava/lang/String;

.field private hasCategory:Z

.field private hasClientId:Z

.field private hasDeviceUserId:Z

.field private hasFrom:Z

.field private hasFromTrustedServer:Z

.field private hasId:Z

.field private hasLastStreamIdReceived:Z

.field private hasPermission:Z

.field private hasPersistentId:Z

.field private hasPkgSignature:Z

.field private hasRegId:Z

.field private hasRmqId:Z

.field private hasStreamId:Z

.field private hasTo:Z

.field private hasToken:Z

.field private id_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private permission_:Ljava/lang/String;

.field private persistentId_:Ljava/lang/String;

.field private pkgSignature_:Ljava/lang/String;

.field private regId_:Ljava/lang/String;

.field private rmqId_:J

.field private streamId_:I

.field private to_:Ljava/lang/String;

.field private token_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->rmqId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->from_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->to_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->category_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->token_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->appData_:Ljava/util/List;

    iput-boolean v2, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->fromTrustedServer_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->persistentId_:Ljava/lang/String;

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->streamId_:I

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->lastStreamIdReceived_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->permission_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->regId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->pkgSignature_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->clientId_:Ljava/lang/String;

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->deviceUserId_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$AppData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->appData_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->appData_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->appData_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAppDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$AppData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->appData_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->cachedSize:I

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->category_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->clientId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceUserId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->deviceUserId_:I

    return v0
.end method

.method public getFrom()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->from_:Ljava/lang/String;

    return-object v0
.end method

.method public getFromTrustedServer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->fromTrustedServer_:Z

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->lastStreamIdReceived_:I

    return v0
.end method

.method public getPermission()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->permission_:Ljava/lang/String;

    return-object v0
.end method

.method public getPersistentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->persistentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPkgSignature()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->pkgSignature_:Ljava/lang/String;

    return-object v0
.end method

.method public getRegId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->regId_:Ljava/lang/String;

    return-object v0
.end method

.method public getRmqId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->rmqId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRmqId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getRmqId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasId()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFrom()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasTo()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasCategory()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getCategory()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasToken()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getAppDataList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFromTrustedServer()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFromTrustedServer()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPersistentId()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasStreamId()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getStreamId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasLastStreamIdReceived()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getLastStreamIdReceived()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPermission()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPermission()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRegId()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getRegId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPkgSignature()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPkgSignature()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasClientId()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getClientId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasDeviceUserId()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getDeviceUserId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->cachedSize:I

    return v2
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->streamId_:I

    return v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->to_:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->token_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCategory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasCategory:Z

    return v0
.end method

.method public hasClientId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasClientId:Z

    return v0
.end method

.method public hasDeviceUserId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasDeviceUserId:Z

    return v0
.end method

.method public hasFrom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFrom:Z

    return v0
.end method

.method public hasFromTrustedServer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFromTrustedServer:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasId:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasPermission()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPermission:Z

    return v0
.end method

.method public hasPersistentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPersistentId:Z

    return v0
.end method

.method public hasPkgSignature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPkgSignature:Z

    return v0
.end method

.method public hasRegId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRegId:Z

    return v0
.end method

.method public hasRmqId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRmqId:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasStreamId:Z

    return v0
.end method

.method public hasTo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasTo:Z

    return v0
.end method

.method public hasToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasToken:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setFromTrustedServer(Z)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setPermission(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setRegId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setPkgSignature(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setClientId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setDeviceUserId(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    move-result-object v0

    return-object v0
.end method

.method public setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasCategory:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->category_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasClientId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->clientId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeviceUserId(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasDeviceUserId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->deviceUserId_:I

    return-object p0
.end method

.method public setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFrom:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->from_:Ljava/lang/String;

    return-object p0
.end method

.method public setFromTrustedServer(Z)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFromTrustedServer:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->fromTrustedServer_:Z

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setPermission(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPermission:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->permission_:Ljava/lang/String;

    return-object p0
.end method

.method public setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPersistentId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->persistentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPkgSignature(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPkgSignature:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->pkgSignature_:Ljava/lang/String;

    return-object p0
.end method

.method public setRegId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRegId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->regId_:Ljava/lang/String;

    return-object p0
.end method

.method public setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRmqId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->rmqId_:J

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->streamId_:I

    return-object p0
.end method

.method public setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasTo:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->to_:Ljava/lang/String;

    return-object p0
.end method

.method public setToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasToken:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->token_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRmqId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getRmqId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFrom()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasTo()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasCategory()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getCategory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasToken()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getAppDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasFromTrustedServer()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getFromTrustedServer()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPersistentId()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasStreamId()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getStreamId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasLastStreamIdReceived()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getLastStreamIdReceived()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPermission()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPermission()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasRegId()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getRegId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasPkgSignature()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getPkgSignature()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasClientId()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->hasDeviceUserId()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->getDeviceUserId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    return-void
.end method
