.class public final Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginRequest"
.end annotation


# instance fields
.field private accountId_:J

.field private adaptiveHeartbeat_:Z

.field private authService_:I

.field private authToken_:Ljava/lang/String;

.field private cachedSize:I

.field private compress_:I

.field private deviceId_:Ljava/lang/String;

.field private domain_:Ljava/lang/String;

.field private hasAccountId:Z

.field private hasAdaptiveHeartbeat:Z

.field private hasAuthService:Z

.field private hasAuthToken:Z

.field private hasCompress:Z

.field private hasDeviceId:Z

.field private hasDomain:Z

.field private hasHeartbeatStat:Z

.field private hasId:Z

.field private hasIncludeStreamIds:Z

.field private hasLastRmqId:Z

.field private hasNetworkType:Z

.field private hasResource:Z

.field private hasUseRmq2:Z

.field private hasUser:Z

.field private heartbeatStat_:Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

.field private id_:Ljava/lang/String;

.field private includeStreamIds_:Z

.field private lastRmqId_:J

.field private networkType_:I

.field private receivedPersistentId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private resource_:Ljava/lang/String;

.field private setting_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Setting;",
            ">;"
        }
    .end annotation
.end field

.field private useRmq2_:Z

.field private user_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->domain_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->user_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->resource_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authToken_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->deviceId_:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->lastRmqId_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setting_:Ljava/util/List;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->compress_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->receivedPersistentId_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->includeStreamIds_:Z

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->adaptiveHeartbeat_:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->heartbeatStat_:Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    iput-boolean v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->useRmq2_:Z

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->accountId_:J

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authService_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->networkType_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addReceivedPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->receivedPersistentId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->receivedPersistentId_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->receivedPersistentId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSetting(Lcom/google/buzz/mobile/proto/GCM$Setting;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$Setting;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setting_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setting_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setting_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->accountId_:J

    return-wide v0
.end method

.method public getAdaptiveHeartbeat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->adaptiveHeartbeat_:Z

    return v0
.end method

.method public getAuthService()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authService_:I

    return v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authToken_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->cachedSize:I

    return v0
.end method

.method public getCompress()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->compress_:I

    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->deviceId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->domain_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeartbeatStat()Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->heartbeatStat_:Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getIncludeStreamIds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->includeStreamIds_:Z

    return v0
.end method

.method public getLastRmqId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->lastRmqId_:J

    return-wide v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->networkType_:I

    return v0
.end method

.method public getReceivedPersistentIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->receivedPersistentId_:Ljava/util/List;

    return-object v0
.end method

.method public getResource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->resource_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasId()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDomain()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getDomain()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUser()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getUser()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasResource()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getResource()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthToken()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDeviceId()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasLastRmqId()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getLastRmqId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getSettingList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/buzz/mobile/proto/GCM$Setting;

    const/16 v4, 0x8

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasCompress()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getCompress()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getReceivedPersistentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_9
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getReceivedPersistentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasIncludeStreamIds()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getIncludeStreamIds()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAdaptiveHeartbeat()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAdaptiveHeartbeat()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasHeartbeatStat()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getHeartbeatStat()Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUseRmq2()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getUseRmq2()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAccountId()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAccountId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthService()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAuthService()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasNetworkType()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getNetworkType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    iput v3, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->cachedSize:I

    return v3
.end method

.method public getSettingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Setting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setting_:Ljava/util/List;

    return-object v0
.end method

.method public getUseRmq2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->useRmq2_:Z

    return v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->user_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAccountId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAccountId:Z

    return v0
.end method

.method public hasAdaptiveHeartbeat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAdaptiveHeartbeat:Z

    return v0
.end method

.method public hasAuthService()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthService:Z

    return v0
.end method

.method public hasAuthToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthToken:Z

    return v0
.end method

.method public hasCompress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasCompress:Z

    return v0
.end method

.method public hasDeviceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDeviceId:Z

    return v0
.end method

.method public hasDomain()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDomain:Z

    return v0
.end method

.method public hasHeartbeatStat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasHeartbeatStat:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasId:Z

    return v0
.end method

.method public hasIncludeStreamIds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasIncludeStreamIds:Z

    return v0
.end method

.method public hasLastRmqId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasLastRmqId:Z

    return v0
.end method

.method public hasNetworkType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasNetworkType:Z

    return v0
.end method

.method public hasResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasResource:Z

    return v0
.end method

.method public hasUseRmq2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUseRmq2:Z

    return v0
.end method

.method public hasUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUser:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setDomain(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setUser(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setResource(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAuthToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setDeviceId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setLastRmqId(J)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$Setting;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->addSetting(Lcom/google/buzz/mobile/proto/GCM$Setting;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setCompress(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->addReceivedPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setIncludeStreamIds(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAdaptiveHeartbeat(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setHeartbeatStat(Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setUseRmq2(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAuthService(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setNetworkType(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    move-result-object v0

    return-object v0
.end method

.method public setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAccountId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->accountId_:J

    return-object p0
.end method

.method public setAdaptiveHeartbeat(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAdaptiveHeartbeat:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->adaptiveHeartbeat_:Z

    return-object p0
.end method

.method public setAuthService(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthService:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authService_:I

    return-object p0
.end method

.method public setAuthToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthToken:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->authToken_:Ljava/lang/String;

    return-object p0
.end method

.method public setCompress(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasCompress:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->compress_:I

    return-object p0
.end method

.method public setDeviceId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDeviceId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->deviceId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDomain(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDomain:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->domain_:Ljava/lang/String;

    return-object p0
.end method

.method public setHeartbeatStat(Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasHeartbeatStat:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->heartbeatStat_:Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setIncludeStreamIds(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasIncludeStreamIds:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->includeStreamIds_:Z

    return-object p0
.end method

.method public setLastRmqId(J)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasLastRmqId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->lastRmqId_:J

    return-object p0
.end method

.method public setNetworkType(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasNetworkType:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->networkType_:I

    return-object p0
.end method

.method public setResource(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasResource:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->resource_:Ljava/lang/String;

    return-object p0
.end method

.method public setUseRmq2(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUseRmq2:Z

    iput-boolean p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->useRmq2_:Z

    return-object p0
.end method

.method public setUser(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUser:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->user_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDomain()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getDomain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUser()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getUser()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasResource()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getResource()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthToken()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasDeviceId()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasLastRmqId()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getLastRmqId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getSettingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$Setting;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasCompress()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getCompress()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getReceivedPersistentIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasIncludeStreamIds()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getIncludeStreamIds()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAdaptiveHeartbeat()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAdaptiveHeartbeat()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasHeartbeatStat()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getHeartbeatStat()Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasUseRmq2()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getUseRmq2()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAccountId()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasAuthService()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getAuthService()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->hasNetworkType()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->getNetworkType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_10
    return-void
.end method
