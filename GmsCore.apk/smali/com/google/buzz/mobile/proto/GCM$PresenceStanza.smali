.class public final Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PresenceStanza"
.end annotation


# instance fields
.field private accountId_:J

.field private avatarHash_:Ljava/lang/String;

.field private cachedSize:I

.field private capabilitiesFlags_:I

.field private client_:I

.field private error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

.field private extension_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Extension;",
            ">;"
        }
    .end annotation
.end field

.field private from_:Ljava/lang/String;

.field private hasAccountId:Z

.field private hasAvatarHash:Z

.field private hasCapabilitiesFlags:Z

.field private hasClient:Z

.field private hasError:Z

.field private hasFrom:Z

.field private hasId:Z

.field private hasLastStreamIdReceived:Z

.field private hasPersistentId:Z

.field private hasPriority:Z

.field private hasRmqId:Z

.field private hasShow:Z

.field private hasStatus:Z

.field private hasStreamId:Z

.field private hasTo:Z

.field private hasType:Z

.field private id_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private persistentId_:Ljava/lang/String;

.field private priority_:I

.field private rmqId_:J

.field private show_:I

.field private status_:Ljava/lang/String;

.field private streamId_:I

.field private to_:Ljava/lang/String;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->rmqId_:J

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->from_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->to_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->show_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->status_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->priority_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->extension_:Ljava/util/List;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->client_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->avatarHash_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->persistentId_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->streamId_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->lastStreamIdReceived_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->capabilitiesFlags_:I

    iput-wide v2, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->accountId_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExtension(Lcom/google/buzz/mobile/proto/GCM$Extension;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$Extension;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->extension_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->extension_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->accountId_:J

    return-wide v0
.end method

.method public getAvatarHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->avatarHash_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->cachedSize:I

    return v0
.end method

.method public getCapabilitiesFlags()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->capabilitiesFlags_:I

    return v0
.end method

.method public getClient()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->client_:I

    return v0
.end method

.method public getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object v0
.end method

.method public getExtensionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$Extension;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->extension_:Ljava/util/List;

    return-object v0
.end method

.method public getFrom()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->from_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->lastStreamIdReceived_:I

    return v0
.end method

.method public getPersistentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->persistentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPriority()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->priority_:I

    return v0
.end method

.method public getRmqId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->rmqId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasRmqId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getRmqId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasType()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasId()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasFrom()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getFrom()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasTo()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasShow()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getShow()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStatus()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPriority()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getPriority()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasError()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getExtensionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$Extension;

    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasClient()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getClient()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAvatarHash()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getAvatarHash()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPersistentId()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStreamId()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getStreamId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasLastStreamIdReceived()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getLastStreamIdReceived()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasCapabilitiesFlags()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getCapabilitiesFlags()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAccountId()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getAccountId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->cachedSize:I

    return v2
.end method

.method public getShow()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->show_:I

    return v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->status_:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->streamId_:I

    return v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->to_:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->type_:I

    return v0
.end method

.method public hasAccountId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAccountId:Z

    return v0
.end method

.method public hasAvatarHash()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAvatarHash:Z

    return v0
.end method

.method public hasCapabilitiesFlags()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasCapabilitiesFlags:Z

    return v0
.end method

.method public hasClient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasClient:Z

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasError:Z

    return v0
.end method

.method public hasFrom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasFrom:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasId:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasPersistentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPersistentId:Z

    return v0
.end method

.method public hasPriority()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPriority:Z

    return v0
.end method

.method public hasRmqId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasRmqId:Z

    return v0
.end method

.method public hasShow()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasShow:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStatus:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStreamId:Z

    return v0
.end method

.method public hasTo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasTo:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setType(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setShow(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setStatus(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setPriority(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$Extension;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$Extension;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->addExtension(Lcom/google/buzz/mobile/proto/GCM$Extension;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setClient(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setAvatarHash(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setCapabilitiesFlags(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    move-result-object v0

    return-object v0
.end method

.method public setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAccountId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->accountId_:J

    return-object p0
.end method

.method public setAvatarHash(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAvatarHash:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->avatarHash_:Ljava/lang/String;

    return-object p0
.end method

.method public setCapabilitiesFlags(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasCapabilitiesFlags:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->capabilitiesFlags_:I

    return-object p0
.end method

.method public setClient(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasClient:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->client_:I

    return-object p0
.end method

.method public setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasError:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object p0
.end method

.method public setFrom(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasFrom:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->from_:Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPersistentId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->persistentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPriority(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPriority:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->priority_:I

    return-object p0
.end method

.method public setRmqId(J)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasRmqId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->rmqId_:J

    return-object p0
.end method

.method public setShow(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasShow:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->show_:I

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStatus:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->status_:Ljava/lang/String;

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->streamId_:I

    return-object p0
.end method

.method public setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasTo:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->to_:Ljava/lang/String;

    return-object p0
.end method

.method public setType(I)Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasType:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasRmqId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getRmqId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasType()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasFrom()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasTo()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasShow()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getShow()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPriority()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getPriority()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasError()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getExtensionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$Extension;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasClient()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getClient()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAvatarHash()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getAvatarHash()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasPersistentId()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasStreamId()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getStreamId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasLastStreamIdReceived()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getLastStreamIdReceived()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasCapabilitiesFlags()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getCapabilitiesFlags()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->hasAccountId()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_10
    return-void
.end method
