.class public final Lcom/google/buzz/mobile/proto/GCM;
.super Ljava/lang/Object;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;,
        Lcom/google/buzz/mobile/proto/GCM$StreamAck;,
        Lcom/google/buzz/mobile/proto/GCM$TalkMetadata;,
        Lcom/google/buzz/mobile/proto/GCM$HttpResponse;,
        Lcom/google/buzz/mobile/proto/GCM$HttpRequest;,
        Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;,
        Lcom/google/buzz/mobile/proto/GCM$AppData;,
        Lcom/google/buzz/mobile/proto/GCM$IqStanza;,
        Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;,
        Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;,
        Lcom/google/buzz/mobile/proto/GCM$MessageStanza;,
        Lcom/google/buzz/mobile/proto/GCM$Extension;,
        Lcom/google/buzz/mobile/proto/GCM$Close;,
        Lcom/google/buzz/mobile/proto/GCM$StreamErrorStanza;,
        Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;,
        Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;,
        Lcom/google/buzz/mobile/proto/GCM$LoginResponse;,
        Lcom/google/buzz/mobile/proto/GCM$LoginRequest;,
        Lcom/google/buzz/mobile/proto/GCM$HeartbeatConfig;,
        Lcom/google/buzz/mobile/proto/GCM$HeartbeatStat;,
        Lcom/google/buzz/mobile/proto/GCM$Setting;,
        Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;,
        Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;,
        Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
