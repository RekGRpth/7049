.class public final Lmaps/ab/e;
.super Ljava/util/concurrent/FutureTask;

# interfaces
.implements Lmaps/ab/c;


# instance fields
.field private final a:Lmaps/ab/m;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    new-instance v0, Lmaps/ab/m;

    invoke-direct {v0}, Lmaps/ab/m;-><init>()V

    iput-object v0, p0, Lmaps/ab/e;->a:Lmaps/ab/m;

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    new-instance v0, Lmaps/ab/m;

    invoke-direct {v0}, Lmaps/ab/m;-><init>()V

    iput-object v0, p0, Lmaps/ab/e;->a:Lmaps/ab/m;

    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lmaps/ab/e;
    .locals 1

    new-instance v0, Lmaps/ab/e;

    invoke-direct {v0, p0, p1}, Lmaps/ab/e;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lmaps/ab/e;
    .locals 1

    new-instance v0, Lmaps/ab/e;

    invoke-direct {v0, p0}, Lmaps/ab/e;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/e;->a:Lmaps/ab/m;

    invoke-virtual {v0, p1, p2}, Lmaps/ab/m;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected done()V
    .locals 1

    iget-object v0, p0, Lmaps/ab/e;->a:Lmaps/ab/m;

    invoke-virtual {v0}, Lmaps/ab/m;->a()V

    return-void
.end method
