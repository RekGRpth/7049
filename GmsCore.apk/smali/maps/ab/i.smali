.class public abstract Lmaps/ab/i;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ab/c;


# instance fields
.field private final a:Lmaps/ab/p;

.field private final b:Lmaps/ab/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ab/p;

    invoke-direct {v0}, Lmaps/ab/p;-><init>()V

    iput-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    new-instance v0, Lmaps/ab/m;

    invoke-direct {v0}, Lmaps/ab/m;-><init>()V

    iput-object v0, p0, Lmaps/ab/i;->b:Lmaps/ab/m;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/i;->b:Lmaps/ab/m;

    invoke-virtual {v0, p1, p2}, Lmaps/ab/m;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {v0, p1}, Lmaps/ab/p;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ab/i;->b:Lmaps/ab/m;

    invoke-virtual {v1}, Lmaps/ab/m;->a()V

    :cond_0
    return v0
.end method

.method protected a(Ljava/lang/Throwable;)Z
    .locals 2

    iget-object v1, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v1, v0}, Lmaps/ab/p;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ab/i;->b:Lmaps/ab/m;

    invoke-virtual {v1}, Lmaps/ab/m;->a()V

    :cond_0
    instance-of v1, p1, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast p1, Ljava/lang/Error;

    throw p1

    :cond_1
    return v0
.end method

.method public cancel(Z)Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {v0}, Lmaps/ab/p;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ab/i;->b:Lmaps/ab/m;

    invoke-virtual {v0}, Lmaps/ab/m;->a()V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lmaps/ab/i;->a()V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {v0}, Lmaps/ab/p;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/ab/p;->a(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {v0}, Lmaps/ab/p;->c()Z

    move-result v0

    return v0
.end method

.method public isDone()Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/i;->a:Lmaps/ab/p;

    invoke-virtual {v0}, Lmaps/ab/p;->b()Z

    move-result v0

    return v0
.end method
