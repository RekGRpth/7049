.class public Lmaps/ac/c;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmaps/ac/g;


# instance fields
.field private A:Ljava/util/Set;

.field private final B:Ljava/util/List;

.field private C:Ljava/util/Iterator;

.field private final D:Ljava/util/Comparator;

.field private E:Lmaps/cm/f;

.field private F:I

.field private final G:Ljava/util/Map;

.field private final b:Lmaps/p/y;

.field private volatile c:Lmaps/cf/a;

.field private final d:Lmaps/cr/b;

.field private e:Lmaps/t/ai;

.field private f:Lmaps/bq/d;

.field private final g:Lmaps/cr/c;

.field private h:F

.field private i:Lmaps/t/bf;

.field private j:Lmaps/p/ac;

.field private k:Ljava/util/Iterator;

.field private l:Ljava/util/ArrayList;

.field private m:I

.field private n:Ljava/util/ArrayList;

.field private o:I

.field private final p:Ljava/util/Map;

.field private q:I

.field private r:F

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private volatile w:Z

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ac/g;

    invoke-direct {v0}, Lmaps/ac/g;-><init>()V

    sput-object v0, Lmaps/ac/c;->a:Lmaps/ac/g;

    return-void
.end method

.method public constructor <init>(Lmaps/cf/a;Lmaps/cr/c;Landroid/content/res/Resources;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/c;->B:Ljava/util/List;

    new-instance v0, Lmaps/ac/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/ac/d;-><init>(Lmaps/ac/e;)V

    iput-object v0, p0, Lmaps/ac/c;->D:Ljava/util/Comparator;

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ac/c;->F:I

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/c;->G:Ljava/util/Map;

    new-instance v0, Lmaps/p/y;

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lmaps/p/y;-><init>(F)V

    iput-object v0, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    iput-object p1, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iput-object p2, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    new-instance v0, Lmaps/cr/b;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lmaps/cr/b;-><init>(I)V

    iput-object v0, p0, Lmaps/ac/c;->d:Lmaps/cr/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iput v2, p0, Lmaps/ac/c;->m:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    iput v2, p0, Lmaps/ac/c;->o:I

    return-void
.end method

.method static final a(Lmaps/bq/d;)I
    .locals 4

    const v3, 0x48435000

    invoke-virtual {p0}, Lmaps/bq/d;->n()F

    move-result v0

    invoke-virtual {p0}, Lmaps/bq/d;->m()I

    move-result v1

    invoke-virtual {p0}, Lmaps/bq/d;->l()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v0

    div-float v0, v1, v0

    cmpl-float v1, v0, v3

    if-lez v1, :cond_0

    sub-float/2addr v0, v3

    const v1, 0x38d1b717

    mul-float/2addr v0, v1

    const/high16 v1, 0x42300000

    add-float/2addr v0, v1

    :goto_0
    float-to-int v0, v0

    return v0

    :cond_0
    const v1, 0x3966afcd

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method static a(Lmaps/t/bh;Lmaps/t/aa;)I
    .locals 6

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v1

    :cond_0
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lmaps/t/aa;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cv;->f()I

    move-result v0

    :goto_1
    move v2, v1

    :goto_2
    invoke-virtual {p0}, Lmaps/t/bh;->b()I

    move-result v3

    if-ge v1, v3, :cond_5

    invoke-virtual {p0, v1}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/a;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lmaps/t/a;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lmaps/t/a;->j()Lmaps/t/aa;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/aa;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lmaps/t/a;->j()Lmaps/t/aa;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cv;->f()I

    move-result v3

    :goto_3
    invoke-virtual {v4}, Lmaps/t/a;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/2addr v3, v5

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {v4}, Lmaps/t/a;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v2, v2, 0x8

    :cond_2
    invoke-virtual {v4}, Lmaps/t/a;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    int-to-float v2, v2

    invoke-virtual {v4}, Lmaps/t/a;->k()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/16 v0, 0xa

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    move v3, v0

    goto :goto_3
.end method

.method static a(Lmaps/t/ca;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lmaps/t/ca;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    sget-boolean v1, Lmaps/ae/h;->m:Z

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown labeleable feature type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lmaps/t/ca;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    check-cast p0, Lmaps/t/bu;

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lmaps/t/bu;->d()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/t/bu;->a(I)Lmaps/t/bh;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/bu;->h()Lmaps/t/aa;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/ac/c;->a(Lmaps/t/bh;Lmaps/t/aa;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_2
    check-cast p0, Lmaps/t/n;

    invoke-virtual {p0}, Lmaps/t/n;->n()Lmaps/t/bh;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/n;->h()Lmaps/t/aa;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ac/c;->a(Lmaps/t/bh;Lmaps/t/aa;)I

    move-result v0

    invoke-virtual {p0}, Lmaps/t/n;->o()Lmaps/t/bh;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/t/n;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/ac/c;->a(Lmaps/t/bh;Lmaps/t/aa;)I

    move-result v1

    add-int/2addr v1, v0

    :cond_0
    :goto_1
    return v1

    :pswitch_3
    check-cast p0, Lmaps/t/az;

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lmaps/t/az;->d()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lmaps/t/az;->a(I)Lmaps/t/bh;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/az;->h()Lmaps/t/aa;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/ac/c;->a(Lmaps/t/bh;Lmaps/t/aa;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_4
    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lmaps/t/az;Lmaps/ac/g;Z)Lmaps/l/u;
    .locals 11

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/t/az;->d()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lmaps/t/az;->c()Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->a()Lmaps/t/ax;

    move-result-object v2

    iget-object v3, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {v3, v2}, Lmaps/t/bf;->b(Lmaps/t/an;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lmaps/ac/c;->a(Lmaps/t/cg;)Lmaps/t/cg;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lmaps/t/az;->h()Lmaps/t/aa;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v1, v1, Lmaps/cf/a;->i:F

    iget-object v2, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v2, v2, Lmaps/cf/a;->j:I

    iget-object v4, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v4, v4, Lmaps/cf/a;->k:I

    iget-object v5, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v5}, Lmaps/bq/d;->n()F

    move-result v5

    invoke-static {v0, v1, v2, v4, v5}, Lmaps/l/u;->a(Lmaps/t/aa;FIIF)F

    move-result v5

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmaps/t/az;->a(I)Lmaps/t/bh;

    move-result-object v2

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-object v6, v0, Lmaps/cf/a;->h:Lmaps/p/l;

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v7, v0, Lmaps/cf/a;->l:F

    iget-object v8, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v9, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-boolean v10, v0, Lmaps/cf/a;->q:Z

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v10}, Lmaps/l/ao;->a(Lmaps/t/az;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;ZFLmaps/p/l;FLmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lmaps/t/n;Lmaps/ac/g;Z)Lmaps/l/u;
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/t/n;->m()[Lmaps/t/y;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lmaps/ac/c;->a(Lmaps/t/bx;Lmaps/ac/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lmaps/t/n;->g()F

    move-result v1

    iget v2, p0, Lmaps/ac/c;->h:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Lmaps/t/n;->l()F

    move-result v1

    const/high16 v2, -0x40800000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lmaps/t/n;->l()F

    move-result v1

    iget v2, p0, Lmaps/ac/c;->h:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    invoke-direct {p0, p1}, Lmaps/ac/c;->a(Lmaps/t/n;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v4, p0, Lmaps/ac/c;->d:Lmaps/cr/b;

    iget-object v5, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    iget-object v6, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-object v7, p0, Lmaps/ac/c;->E:Lmaps/cm/f;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v7}, Lmaps/l/aw;->a(Lmaps/t/n;Lmaps/ac/g;ZLmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;Lmaps/cm/f;)Lmaps/l/aw;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lmaps/t/cg;)Lmaps/t/cg;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/ac/c;->e:Lmaps/t/ai;

    invoke-virtual {v0, p1, v5}, Lmaps/t/ai;->a(Lmaps/t/cg;Ljava/util/List;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-ne v6, v1, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->d()F

    move-result v2

    move v4, v1

    move-object v3, v0

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->d()F

    move-result v1

    cmpl-float v0, v1, v2

    if-lez v0, :cond_3

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method static a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    sget-object v0, Lmaps/ac/c;->a:Lmaps/ac/g;

    new-instance v2, Lmaps/t/d;

    invoke-direct {v2, v1}, Lmaps/t/d;-><init>(I)V

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/g;

    new-instance v3, Lmaps/t/d;

    add-int/lit8 v4, v1, -0x1

    invoke-direct {v3, v4}, Lmaps/t/d;-><init>(I)V

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/g;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/av;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ac/g;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/t/d;

    invoke-virtual {v2, v1}, Lmaps/t/d;->a(Lmaps/t/av;)V

    goto :goto_1
.end method

.method private a(Lmaps/t/az;Lmaps/ac/g;ZZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lmaps/ac/c;->a(Lmaps/t/az;Lmaps/ac/g;Z)Lmaps/l/u;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ac/c;->c(Lmaps/l/u;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/l/u;->a_(Z)V

    :cond_0
    return-void
.end method

.method private a(Lmaps/t/bu;Lmaps/ac/g;ZZ)V
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lmaps/t/bu;->c()Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->a()Lmaps/t/ax;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {v3, v2}, Lmaps/t/bf;->b(Lmaps/t/an;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/ac/c;->a(Lmaps/t/cg;)Lmaps/t/cg;

    move-result-object v15

    if-eqz v15, :cond_3

    invoke-virtual {v15}, Lmaps/t/cg;->d()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v2}, Lmaps/bq/d;->z()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v3}, Lmaps/bq/d;->n()F

    move-result v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42200000

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    const/4 v2, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lmaps/t/bu;->d()I

    move-result v1

    if-ge v2, v1, :cond_3

    const v1, 0x3f333333

    invoke-virtual {v15, v1}, Lmaps/t/cg;->a(F)Lmaps/t/bx;

    move-result-object v4

    const v1, 0x3e99999a

    invoke-virtual {v15, v1}, Lmaps/t/cg;->a(F)Lmaps/t/bx;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/ac/c;->c:Lmaps/cf/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/ac/c;->f:Lmaps/bq/d;

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/ac/c;->d:Lmaps/cr/b;

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move/from16 v6, p3

    invoke-static/range {v1 .. v9}, Lmaps/l/aw;->a(Lmaps/t/bu;ILmaps/ac/g;Lmaps/t/bx;Lmaps/t/bx;ZLmaps/cf/a;Lmaps/bq/d;Lmaps/cr/b;)Lmaps/l/aw;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/c;->e()F

    move-result v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-boolean v1, v1, Lmaps/cf/a;->o:Z

    if-eqz v1, :cond_2

    const/16 v7, 0xa

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lmaps/t/bu;->a(I)Lmaps/t/bh;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v10, v1, Lmaps/cf/a;->l:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-object v11, v1, Lmaps/cf/a;->a:Lmaps/p/l;

    move-object/from16 v0, p0

    iget-object v12, v0, Lmaps/ac/c;->f:Lmaps/bq/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/ac/c;->b:Lmaps/p/y;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-boolean v14, v1, Lmaps/cf/a;->q:Z

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object v6, v15

    move/from16 v8, p3

    invoke-static/range {v3 .. v14}, Lmaps/l/ao;->a(Lmaps/t/bu;Lmaps/ac/g;Lmaps/t/bh;Lmaps/t/cg;IZFFLmaps/p/l;Lmaps/bq/d;Lmaps/p/y;Z)Lmaps/l/ao;

    move-result-object v1

    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/ac/c;->c(Lmaps/l/u;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p4, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lmaps/l/u;->a_(Z)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method private a(Lmaps/t/ca;Lmaps/ac/g;ZZ)V
    .locals 1

    invoke-direct {p0, p2}, Lmaps/ac/c;->a(Lmaps/ac/g;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lmaps/t/bu;

    if-eqz v0, :cond_2

    check-cast p1, Lmaps/t/bu;

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/ac/c;->a(Lmaps/t/bu;Lmaps/ac/g;ZZ)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lmaps/t/az;

    if-eqz v0, :cond_3

    check-cast p1, Lmaps/t/az;

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/ac/c;->a(Lmaps/t/az;Lmaps/ac/g;ZZ)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lmaps/t/n;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/t/n;

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/ac/c;->a(Lmaps/t/n;Lmaps/ac/g;ZZ)V

    goto :goto_0
.end method

.method private a(Lmaps/t/n;Lmaps/ac/g;ZZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lmaps/ac/c;->a(Lmaps/t/n;Lmaps/ac/g;Z)Lmaps/l/u;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ac/c;->c(Lmaps/l/u;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/l/u;->a_(Z)V

    :cond_0
    return-void
.end method

.method private a(J)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot run labeler loop until all previous labels have either been copied into new label table or destroyed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    invoke-direct {p0, v0, v7, v4, v4}, Lmaps/ac/c;->a(Lmaps/t/ca;Lmaps/ac/g;ZZ)V

    :cond_1
    iget-object v0, p0, Lmaps/ac/c;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ac/c;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/ac/c;->s:I

    iget v1, p0, Lmaps/ac/c;->q:I

    if-ge v0, v1, :cond_3

    iget-boolean v0, p0, Lmaps/ac/c;->z:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lmaps/ac/c;->z:Z

    move v2, v4

    :cond_2
    :goto_0
    return v2

    :cond_3
    move v1, v2

    :goto_1
    iget-object v0, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v0}, Lmaps/p/ac;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_4
    iget v0, p0, Lmaps/ac/c;->s:I

    iget v3, p0, Lmaps/ac/c;->q:I

    if-lt v0, v3, :cond_5

    iget-object v0, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v0}, Lmaps/p/ac;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iget v3, p0, Lmaps/ac/c;->m:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    invoke-virtual {v0}, Lmaps/l/u;->r()I

    move-result v0

    iget-object v3, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v3}, Lmaps/p/ac;->b()Lmaps/ac/h;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/h;->b()I

    move-result v3

    if-gt v0, v3, :cond_2

    :cond_5
    iget-boolean v0, p0, Lmaps/ac/c;->z:Z

    if-eqz v0, :cond_6

    iput-boolean v2, p0, Lmaps/ac/c;->z:Z

    move v2, v4

    goto :goto_0

    :cond_6
    if-lez v1, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    cmp-long v0, v5, p1

    if-ltz v0, :cond_7

    move v2, v4

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    if-nez v0, :cond_9

    iget-object v0, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v0}, Lmaps/p/ac;->a()Lmaps/ac/h;

    move-result-object v0

    iget-object v3, p0, Lmaps/ac/c;->B:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v3, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v3}, Lmaps/p/ac;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v3}, Lmaps/p/ac;->b()Lmaps/ac/h;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/h;->b()I

    move-result v3

    invoke-virtual {v0}, Lmaps/ac/h;->b()I

    move-result v5

    if-ne v3, v5, :cond_8

    iget-object v3, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    invoke-virtual {v3}, Lmaps/p/ac;->a()Lmaps/ac/h;

    move-result-object v3

    iget-object v5, p0, Lmaps/ac/c;->B:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lmaps/ac/c;->B:Ljava/util/List;

    iget-object v3, p0, Lmaps/ac/c;->D:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lmaps/ac/c;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    :cond_9
    :goto_3
    iget-object v0, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    add-int/lit8 v3, v1, 0x1

    if-lez v1, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_a

    move v2, v4

    goto/16 :goto_0

    :cond_a
    iget-boolean v0, p0, Lmaps/ac/c;->z:Z

    if-eqz v0, :cond_b

    iput-boolean v2, p0, Lmaps/ac/c;->z:Z

    move v2, v4

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/h;

    iget v1, p0, Lmaps/ac/c;->s:I

    iget v5, p0, Lmaps/ac/c;->q:I

    if-lt v1, v5, :cond_c

    iget-object v1, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iget v5, p0, Lmaps/ac/c;->m:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/u;

    invoke-virtual {v1}, Lmaps/l/u;->r()I

    move-result v1

    invoke-virtual {v0}, Lmaps/ac/h;->b()I

    move-result v5

    if-lt v1, v5, :cond_c

    move v0, v3

    :goto_4
    iget-object v1, p0, Lmaps/ac/c;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object v7, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    move v1, v0

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v0}, Lmaps/ac/h;->a()Lmaps/t/ca;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ac/h;->c()Lmaps/ac/g;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2, v4}, Lmaps/ac/c;->a(Lmaps/t/ca;Lmaps/ac/g;ZZ)V

    iget v0, p0, Lmaps/ac/c;->s:I

    iget v1, p0, Lmaps/ac/c;->q:I

    if-le v0, v1, :cond_d

    iget v0, p0, Lmaps/ac/c;->m:I

    invoke-direct {p0, v0}, Lmaps/ac/c;->c(I)V

    :cond_d
    move v1, v3

    goto :goto_3

    :cond_e
    move v0, v1

    goto :goto_4
.end method

.method private a(Lmaps/ac/g;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->A:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/l/u;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lmaps/ac/c;->h:F

    invoke-virtual {p1}, Lmaps/l/u;->p()F

    move-result v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lmaps/l/u;->o()Lmaps/ac/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ac/c;->a(Lmaps/ac/g;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v0

    instance-of v0, v0, Lmaps/t/n;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v0

    check-cast v0, Lmaps/t/n;

    invoke-direct {p0, v0}, Lmaps/ac/c;->a(Lmaps/t/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/l/u;->q()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    iget v0, p0, Lmaps/ac/c;->h:F

    invoke-virtual {p1}, Lmaps/l/u;->q()F

    move-result v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private static a(Lmaps/l/u;Lmaps/l/u;)Z
    .locals 3

    invoke-virtual {p0}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v0}, Lmaps/t/ca;->b()Lmaps/t/v;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v1

    invoke-interface {v1}, Lmaps/t/ca;->b()Lmaps/t/v;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    instance-of v2, v0, Lmaps/t/bs;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lmaps/t/bs;

    if-eqz v2, :cond_0

    sget-object v2, Lmaps/t/v;->a:Lmaps/t/v;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/t/bx;Lmaps/ac/g;)Z
    .locals 1

    iget-object v0, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {v0, p1}, Lmaps/t/bf;->a(Lmaps/t/bx;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lmaps/ac/c;->b(Lmaps/t/bx;Lmaps/ac/g;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/t/n;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/t/n;->c()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    iget v1, p0, Lmaps/ac/c;->t:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lmaps/ac/c;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lmaps/ac/g;)Lmaps/t/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/c;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p1, :cond_2

    sget-object p1, Lmaps/ac/c;->a:Lmaps/ac/g;

    :cond_2
    iget-object v0, p0, Lmaps/ac/c;->G:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/av;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->G:Ljava/util/Map;

    sget-object v1, Lmaps/ac/c;->a:Lmaps/ac/g;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/av;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    invoke-direct {p0}, Lmaps/ac/c;->g()V

    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v3, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {v0, v3}, Lmaps/l/u;->a(Lmaps/t/bf;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v0}, Lmaps/ac/c;->g(Lmaps/l/u;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    invoke-direct {p0, v0}, Lmaps/ac/c;->d(Lmaps/l/u;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v0}, Lmaps/ac/c;->e(Lmaps/l/u;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v3}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/c;->o:I

    return-void
.end method

.method private b(J)Z
    .locals 5

    :try_start_0
    invoke-direct {p0, p1, p2}, Lmaps/ac/c;->a(J)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    throw v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "#:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lmaps/ac/c;->F:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lmaps/ac/c;->F:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " T:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " numL:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "Labeler.runLabeler"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lmaps/l/u;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget-boolean v0, v0, Lmaps/cf/a;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {p1}, Lmaps/l/u;->n()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bf;->b(Lmaps/t/an;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    invoke-virtual {p1, v0}, Lmaps/l/u;->a(Lmaps/t/bf;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Lmaps/l/u;Lmaps/l/u;)Z
    .locals 1

    instance-of v0, p0, Lmaps/l/aw;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lmaps/l/aw;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/l/aw;

    invoke-virtual {p1}, Lmaps/l/aw;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lmaps/l/aw;

    invoke-virtual {p0}, Lmaps/l/aw;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lmaps/t/bx;Lmaps/ac/g;)Z
    .locals 1

    invoke-direct {p0, p2}, Lmaps/ac/c;->b(Lmaps/ac/g;)Lmaps/t/av;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmaps/t/av;->a(Lmaps/t/bx;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    invoke-direct {p0, v0}, Lmaps/ac/c;->f(Lmaps/l/u;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lmaps/ac/c;->s:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lmaps/ac/c;->s:I

    :cond_0
    iget-object v1, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v1}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/l/u;->s()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lmaps/ac/c;->m:I

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lmaps/ac/c;->f()V

    :cond_1
    return-void
.end method

.method private c(Lmaps/l/u;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    iget-object v0, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/l/u;->s()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/l/u;->s()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_3

    invoke-static {v0, p1}, Lmaps/ac/c;->b(Lmaps/l/u;Lmaps/l/u;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v3}, Lmaps/ac/c;->c(I)V

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v3, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {p1, v0, v3}, Lmaps/l/u;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    invoke-direct {p0, p1}, Lmaps/ac/c;->g(Lmaps/l/u;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lmaps/ac/c;->d(Lmaps/l/u;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    iget-object v3, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v4, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {p1, v3, v4}, Lmaps/l/u;->b(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, p1}, Lmaps/ac/c;->g(Lmaps/l/u;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/ac/c;->d(Lmaps/l/u;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {p1, v0}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    :cond_4
    :goto_1
    return v1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {p1, v0}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    goto :goto_1

    :cond_8
    invoke-direct {p0, p1}, Lmaps/ac/c;->e(Lmaps/l/u;)V

    move v1, v2

    goto :goto_1
.end method

.method private d(Lmaps/l/u;)Z
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/l/u;->r()I

    move-result v6

    invoke-virtual {p1}, Lmaps/l/u;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lmaps/l/u;->m()Lmaps/t/an;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v8

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v3

    :goto_1
    if-ge v5, v9, :cond_7

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Lmaps/ac/c;->a(Lmaps/l/u;Lmaps/l/u;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Lmaps/l/u;->p()F

    move-result v4

    invoke-virtual {v0}, Lmaps/l/u;->p()F

    move-result v10

    cmpl-float v4, v4, v10

    if-lez v4, :cond_2

    invoke-direct {p0, v5}, Lmaps/ac/c;->c(I)V

    :cond_0
    :goto_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/l/u;->p()F

    move-result v4

    invoke-virtual {v0}, Lmaps/l/u;->p()F

    move-result v10

    cmpg-float v4, v4, v10

    if-gez v4, :cond_4

    :cond_3
    :goto_3
    return v2

    :cond_4
    invoke-virtual {v0}, Lmaps/l/u;->m()Lmaps/t/an;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v10

    invoke-virtual {v10, v8}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v4, v7}, Lmaps/t/an;->a(Lmaps/t/an;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lmaps/l/u;->u()Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v2

    :goto_4
    if-gt v1, v4, :cond_5

    if-ne v1, v4, :cond_3

    invoke-virtual {v0}, Lmaps/l/u;->r()I

    move-result v0

    if-le v6, v0, :cond_3

    :cond_5
    invoke-direct {p0, v5}, Lmaps/ac/c;->c(I)V

    goto :goto_2

    :cond_6
    move v4, v3

    goto :goto_4

    :cond_7
    move v2, v3

    goto :goto_3
.end method

.method private e(Lmaps/l/u;)V
    .locals 3

    invoke-direct {p0, p1}, Lmaps/ac/c;->f(Lmaps/l/u;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/ac/c;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ac/c;->s:I

    iget v0, p0, Lmaps/ac/c;->m:I

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Lmaps/l/u;->r()I

    move-result v1

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iget v2, p0, Lmaps/ac/c;->m:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    invoke-virtual {v0}, Lmaps/l/u;->r()I

    move-result v0

    if-ge v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lmaps/ac/c;->m:I

    :cond_1
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/l/u;->s()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private f()V
    .locals 4

    const v1, 0x7fffffff

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/c;->m:I

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/l/u;->r()I

    move-result v3

    if-ge v3, v2, :cond_0

    invoke-direct {p0, v0}, Lmaps/ac/c;->f(Lmaps/l/u;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lmaps/l/u;->r()I

    move-result v2

    iput v1, p0, Lmaps/ac/c;->m:I

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private f(Lmaps/l/u;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/l/u;->l()F

    move-result v1

    iget v2, p0, Lmaps/ac/c;->r:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lmaps/l/u;->o()Lmaps/ac/g;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lmaps/ac/g;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 5

    const/4 v2, 0x0

    sget-boolean v0, Lmaps/ae/h;->R:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/ac/c;->w:Z

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_1

    iget-object v4, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v4}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/c;->m:I

    iput-boolean v2, p0, Lmaps/ac/c;->w:Z

    iput-boolean v2, p0, Lmaps/ac/c;->x:Z

    iput-boolean v2, p0, Lmaps/ac/c;->y:Z

    :cond_3
    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iput-object v1, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    iput-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    iget v0, p0, Lmaps/ac/c;->o:I

    iget v1, p0, Lmaps/ac/c;->m:I

    iput v1, p0, Lmaps/ac/c;->o:I

    iput v0, p0, Lmaps/ac/c;->m:I

    iput v2, p0, Lmaps/ac/c;->s:I

    iget-object v0, p0, Lmaps/ac/c;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private g(Lmaps/l/u;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/l/u;->o()Lmaps/ac/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ac/c;->b(Lmaps/ac/g;)Lmaps/t/av;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/l/u;->m()Lmaps/t/an;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/t/av;->a(Lmaps/t/an;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/l/al;Lmaps/bq/d;)Lmaps/l/u;
    .locals 1

    iget-object v0, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    invoke-static {p1, v0, p2}, Lmaps/l/aw;->a(Lmaps/l/al;Lmaps/p/y;Lmaps/bq/d;)Lmaps/l/aw;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    iget-object v2, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v2}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    invoke-virtual {v0}, Lmaps/p/y;->a()V

    iget-object v0, p0, Lmaps/ac/c;->d:Lmaps/cr/b;

    invoke-virtual {v0}, Lmaps/cr/b;->a()V

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmaps/l/u;->a(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/t/bf;ILjava/util/Iterator;Lmaps/p/ac;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILmaps/o/c;)V
    .locals 10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move/from16 v0, p9

    int-to-long v3, v0

    add-long/2addr v3, v1

    iput-object p1, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iput-object p4, p0, Lmaps/ac/c;->k:Ljava/util/Iterator;

    iput-object p5, p0, Lmaps/ac/c;->j:Lmaps/p/ac;

    iput-object p2, p0, Lmaps/ac/c;->i:Lmaps/t/bf;

    iput p3, p0, Lmaps/ac/c;->t:I

    move-object/from16 v0, p7

    iput-object v0, p0, Lmaps/ac/c;->A:Ljava/util/Set;

    new-instance v1, Lmaps/t/ai;

    invoke-virtual {p2}, Lmaps/t/bf;->c()Lmaps/t/an;

    move-result-object v2

    invoke-direct {v1, v2}, Lmaps/t/ai;-><init>(Lmaps/t/an;)V

    iput-object v1, p0, Lmaps/ac/c;->e:Lmaps/t/ai;

    iget-object v1, p0, Lmaps/ac/c;->G:Ljava/util/Map;

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lmaps/ac/c;->a(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {p1}, Lmaps/ac/c;->a(Lmaps/bq/d;)I

    move-result v1

    iget v2, p0, Lmaps/ac/c;->q:I

    if-eq v1, v2, :cond_0

    iput v1, p0, Lmaps/ac/c;->q:I

    iget-object v2, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Lmaps/p/y;->a(I)V

    :cond_0
    iget-object v1, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v1}, Lmaps/bq/d;->n()F

    move-result v1

    iget-object v2, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v2}, Lmaps/bq/d;->n()F

    move-result v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43480000

    mul-float/2addr v1, v2

    iput v1, p0, Lmaps/ac/c;->r:F

    invoke-direct {p0}, Lmaps/ac/c;->g()V

    iget-object v1, p0, Lmaps/ac/c;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ac/c;->C:Ljava/util/Iterator;

    iget-object v1, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v1}, Lmaps/bq/d;->t()F

    move-result v1

    iput v1, p0, Lmaps/ac/c;->h:F

    iget v1, p0, Lmaps/ac/c;->t:I

    invoke-static {}, Lmaps/be/b;->c()Lmaps/p/k;

    move-result-object v2

    iget-object v7, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v7}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v7

    move-object/from16 v0, p10

    invoke-virtual {v2, v7, v0}, Lmaps/p/k;->a(Lmaps/t/bx;Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/p/ap;->b()I

    move-result v2

    if-ge v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lmaps/ac/c;->u:Z

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_7

    iget-object v1, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/u;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v7

    invoke-interface {v7}, Lmaps/t/ca;->b()Lmaps/t/v;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-direct {p0, v1}, Lmaps/ac/c;->a(Lmaps/l/u;)Z

    move-result v7

    if-nez v7, :cond_4

    :cond_1
    iget-object v7, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v1, v7}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    :cond_2
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v8, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v1, v7, v8}, Lmaps/l/u;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v1}, Lmaps/ac/c;->b(Lmaps/l/u;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v1}, Lmaps/ac/c;->g(Lmaps/l/u;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-direct {p0, v1}, Lmaps/ac/c;->d(Lmaps/l/u;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-direct {p0, v1}, Lmaps/ac/c;->e(Lmaps/l/u;)V

    goto :goto_2

    :cond_5
    iget-object v7, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v1, v7}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v1, v7}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_8

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/u;

    invoke-virtual {v1}, Lmaps/l/u;->t()Lmaps/t/ca;

    move-result-object v7

    invoke-virtual {v1}, Lmaps/l/u;->o()Lmaps/ac/g;

    move-result-object v8

    invoke-virtual {v1}, Lmaps/l/u;->u()Z

    move-result v1

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v1, v9}, Lmaps/ac/c;->a(Lmaps/t/ca;Lmaps/ac/g;ZZ)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v1, -0x1

    iput v1, p0, Lmaps/ac/c;->o:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ac/c;->x:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ac/c;->y:Z

    invoke-direct {p0, v3, v4}, Lmaps/ac/c;->b(J)Z

    move-result v1

    iput-boolean v1, p0, Lmaps/ac/c;->v:Z

    return-void
.end method

.method public a(Lmaps/cf/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    invoke-virtual {p0}, Lmaps/ac/c;->b()V

    :cond_0
    return-void
.end method

.method public a(Lmaps/cm/f;)V
    .locals 0

    iput-object p1, p0, Lmaps/ac/c;->E:Lmaps/cm/f;

    return-void
.end method

.method public a(Lmaps/t/bf;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/ac/c;->g()V

    invoke-virtual {p1}, Lmaps/t/bf;->a()Lmaps/t/bw;

    move-result-object v3

    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/u;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    iget-object v6, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v5, v6}, Lmaps/l/u;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lmaps/l/u;->n()Lmaps/t/ax;

    move-result-object v5

    invoke-virtual {v3, v5}, Lmaps/t/bw;->b(Lmaps/t/an;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v0}, Lmaps/ac/c;->e(Lmaps/l/u;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lmaps/ac/c;->g:Lmaps/cr/c;

    invoke-virtual {v0, v5}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/ac/c;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/c;->o:I

    iput-boolean v7, p0, Lmaps/ac/c;->x:Z

    iput-boolean v7, p0, Lmaps/ac/c;->y:Z

    iput-boolean v2, p0, Lmaps/ac/c;->v:Z

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    invoke-virtual {v0}, Lmaps/p/y;->a()V

    iget-object v0, p0, Lmaps/ac/c;->d:Lmaps/cr/b;

    invoke-virtual {v0}, Lmaps/cr/b;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ac/c;->b:Lmaps/p/y;

    invoke-virtual {v0}, Lmaps/p/y;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ac/c;->w:Z

    return-void
.end method

.method public b(I)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-boolean v2, p0, Lmaps/ac/c;->x:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lmaps/ac/c;->y:Z

    if-eqz v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lmaps/ac/c;->y:Z

    invoke-direct {p0, v2}, Lmaps/ac/c;->b(Z)V

    iput-boolean v4, p0, Lmaps/ac/c;->x:Z

    iput-boolean v4, p0, Lmaps/ac/c;->y:Z

    :cond_1
    invoke-direct {p0, v0, v1}, Lmaps/ac/c;->b(J)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ac/c;->v:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/c;->v:Z

    return v0
.end method

.method public d()Lmaps/ac/f;
    .locals 2

    new-instance v0, Lmaps/ac/f;

    iget-object v1, p0, Lmaps/ac/c;->l:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lmaps/ac/f;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method e()F
    .locals 3

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v0, v0, Lmaps/cf/a;->c:I

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v1}, Lmaps/bq/d;->s()F

    move-result v1

    const/high16 v2, 0x41680000

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    iget-object v0, p0, Lmaps/ac/c;->c:Lmaps/cf/a;

    iget v0, v0, Lmaps/cf/a;->b:I

    int-to-float v0, v0

    iget-boolean v2, p0, Lmaps/ac/c;->u:Z

    if-nez v2, :cond_0

    add-float/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lmaps/ac/c;->f:Lmaps/bq/d;

    invoke-virtual {v1}, Lmaps/bq/d;->n()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method
