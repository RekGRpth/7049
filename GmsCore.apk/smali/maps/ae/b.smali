.class public Lmaps/ae/b;
.super Ljava/lang/Object;


# static fields
.field protected static a:Lmaps/ae/b;

.field protected static final d:Ljava/lang/Object;


# instance fields
.field protected final b:Lmaps/bj/n;

.field protected final c:Lmaps/bj/b;

.field protected final e:Lmaps/ae/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/ae/b;->d:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/ae/b;->c:Lmaps/bj/b;

    iput-object v0, p0, Lmaps/ae/b;->b:Lmaps/bj/n;

    new-instance v0, Lmaps/co/a;

    invoke-direct {v0}, Lmaps/co/a;-><init>()V

    iput-object v0, p0, Lmaps/ae/b;->e:Lmaps/ae/d;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lmaps/bj/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lmaps/ae/b;->d:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p2, :cond_0

    :try_start_0
    iput-object p2, p0, Lmaps/ae/b;->c:Lmaps/bj/b;

    :goto_0
    new-instance v0, Lmaps/co/a;

    invoke-direct {v0}, Lmaps/co/a;-><init>()V

    iput-object v0, p0, Lmaps/ae/b;->e:Lmaps/ae/d;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sput-object p0, Lmaps/ae/b;->a:Lmaps/ae/b;

    new-instance v0, Lmaps/bs/d;

    invoke-direct {v0, p1}, Lmaps/bs/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmaps/ae/b;->b:Lmaps/bj/n;

    return-void

    :cond_0
    if-nez p1, :cond_1

    :try_start_1
    new-instance v0, Lmaps/bj/e;

    invoke-direct {v0}, Lmaps/bj/e;-><init>()V

    :goto_1
    iput-object v0, p0, Lmaps/ae/b;->c:Lmaps/bj/b;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    :try_start_2
    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0, p1}, Lmaps/bs/b;-><init>(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static a()Lmaps/ae/b;
    .locals 1

    sget-object v0, Lmaps/ae/b;->a:Lmaps/ae/b;

    return-object v0
.end method


# virtual methods
.method public b()Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/ae/b;->e:Lmaps/ae/d;

    return-object v0
.end method

.method public c()Lmaps/bj/b;
    .locals 1

    iget-object v0, p0, Lmaps/ae/b;->c:Lmaps/bj/b;

    return-object v0
.end method

.method public d()Lmaps/bj/n;
    .locals 1

    iget-object v0, p0, Lmaps/ae/b;->b:Lmaps/bj/n;

    return-object v0
.end method
