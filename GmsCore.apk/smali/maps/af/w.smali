.class public Lmaps/af/w;
.super Ljava/lang/Object;


# static fields
.field public static final a:[Lmaps/o/c;

.field public static final b:[Lmaps/o/c;

.field public static volatile c:I

.field private static d:Z

.field private static e:Z

.field private static volatile f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/o/c;

    sget-object v1, Lmaps/o/c;->b:Lmaps/o/c;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/o/c;->g:Lmaps/o/c;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/o/c;->h:Lmaps/o/c;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/o/c;->i:Lmaps/o/c;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/af/w;->a:[Lmaps/o/c;

    const/16 v0, 0xc

    new-array v0, v0, [Lmaps/o/c;

    sget-object v1, Lmaps/o/c;->a:Lmaps/o/c;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/o/c;->c:Lmaps/o/c;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/o/c;->f:Lmaps/o/c;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/o/c;->e:Lmaps/o/c;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lmaps/o/c;->j:Lmaps/o/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/o/c;->l:Lmaps/o/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/o/c;->k:Lmaps/o/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/o/c;->m:Lmaps/o/c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lmaps/o/c;->n:Lmaps/o/c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lmaps/o/c;->o:Lmaps/o/c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lmaps/o/c;->p:Lmaps/o/c;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/af/w;->b:[Lmaps/o/c;

    sput-boolean v3, Lmaps/af/w;->d:Z

    const/16 v0, 0xa

    sput v0, Lmaps/af/w;->c:I

    const/4 v0, -0x1

    sput v0, Lmaps/af/w;->f:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/ak/n;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    new-instance v3, Lmaps/ak/h;

    invoke-direct {v3}, Lmaps/ak/h;-><init>()V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/k/b;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->a(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v3

    invoke-static {}, Lmaps/ae/c;->x()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->b(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v3

    invoke-virtual {v3, p3}, Lmaps/ak/h;->c(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v3

    invoke-static {}, Lmaps/ae/c;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->d(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->a(Z)Lmaps/ak/h;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/ak/h;->b(Z)Lmaps/ak/h;

    move-result-object v3

    invoke-static {p0}, Lmaps/bt/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->e(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/res/Resources;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->c(Z)Lmaps/ak/h;

    move-result-object v3

    invoke-virtual {v0, p0}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lmaps/ak/h;->e(Z)Lmaps/ak/h;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/capabilities/a;->b()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lmaps/ak/h;->d(Z)Lmaps/ak/h;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v3}, Lmaps/ak/h;->a(I)Lmaps/ak/h;

    move-result-object v3

    const-string v0, "DriveAbout"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    sget-boolean v0, Lmaps/ae/h;->r:Z

    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    const-string v0, "GMM"

    :goto_1
    invoke-virtual {v3, v0}, Lmaps/ak/h;->f(Ljava/lang/String;)Lmaps/ak/h;

    sget-boolean v0, Lmaps/ae/h;->r:Z

    if-nez v0, :cond_0

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {v3, v1}, Lmaps/ak/h;->b(I)Lmaps/ak/h;

    invoke-virtual {v3, v1}, Lmaps/ak/h;->f(Z)Lmaps/ak/h;

    :cond_1
    invoke-virtual {v3}, Lmaps/ak/h;->a()Lmaps/ak/n;

    move-result-object v1

    const-string v0, "GMM"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v5, Lmaps/ae/h;->r:Z

    if-eqz v5, :cond_3

    if-nez v0, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Lmaps/ak/h;->b(I)Lmaps/ak/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/ak/h;->f(Z)Lmaps/ak/h;

    move-result-object v0

    const-string v2, "DriveAbout"

    invoke-virtual {v0, v2}, Lmaps/ak/h;->f(Ljava/lang/String;)Lmaps/ak/h;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ak/h;->b()Lmaps/ak/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ak/n;->a(Lmaps/ak/a;)V

    :cond_3
    new-instance v0, Lmaps/af/b;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lmaps/af/b;-><init>(Lmaps/af/z;)V

    invoke-virtual {v1, v0}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    const-string v0, "1"

    const-string v0, "2"

    const-string v0, "3"

    sget-boolean v0, Lmaps/ae/h;->m:Z

    if-eqz v0, :cond_8

    const-string v0, "1"

    :goto_2
    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_4

    const-string v2, "VectorGlobalState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Server:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lmaps/ak/n;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Type:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_5

    const-string v0, "VectorGlobalState"

    const-string v2, "Created DataRequestDispatcher"

    invoke-static {v0, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-object v1

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    move-object v0, p2

    goto/16 :goto_1

    :cond_8
    sget-boolean v0, Lmaps/ae/h;->n:Z

    if-eqz v0, :cond_9

    const-string v0, "2"

    goto :goto_2

    :cond_9
    const-string v0, "3"

    goto :goto_2
.end method

.method public static declared-synchronized a(Lmaps/o/c;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/an/y;
    .locals 7

    const-class v6, Lmaps/af/w;

    monitor-enter v6

    :try_start_0
    sget-boolean v0, Lmaps/af/w;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorGlobalState.initialize() must be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Lmaps/o/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p1}, Lmaps/bt/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lmaps/af/w;->a([Lmaps/o/c;Lmaps/ak/a;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-static {p0}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit v6

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;[Lmaps/o/c;Ljava/lang/String;ILmaps/bh/g;)V
    .locals 14

    const-class v11, Lmaps/af/w;

    monitor-enter v11

    :try_start_0
    sget-boolean v1, Lmaps/af/w;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit v11

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    const-string v1, "VectorGlobalState.initialize"

    invoke-static {v1}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lmaps/ae/c;->a(Landroid/content/Context;)Lmaps/ae/c;

    invoke-static {}, Lmaps/bt/a;->a()V

    invoke-static/range {p5 .. p5}, Lmaps/bh/k;->a(Lmaps/bh/g;)V

    const-string v1, "activity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    sput v1, Lmaps/af/w;->f:I

    invoke-static {p0}, Lmaps/af/w;->b(Landroid/content/Context;)V

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lmaps/bt/a;->c()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-static {p0, p1, v0, v1}, Lmaps/af/w;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/ak/n;

    move-result-object v2

    const-string v1, "DriveAbout"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-wide/16 v3, 0x7530

    invoke-virtual {v2, v3, v4}, Lmaps/ak/n;->a(J)V

    :cond_1
    if-eqz v1, :cond_9

    sget-boolean v1, Lmaps/ae/h;->r:Z

    if-nez v1, :cond_9

    invoke-static {v2}, Lmaps/az/c;->b(Lmaps/ak/n;)V

    :goto_1
    new-instance v1, Lmaps/be/j;

    invoke-direct {v1, v2}, Lmaps/be/j;-><init>(Lmaps/ak/n;)V

    invoke-virtual {v2, v1}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    :cond_2
    invoke-virtual {v2}, Lmaps/ak/n;->w()V

    invoke-static {p0}, Lmaps/bt/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-static {v2, v4}, Lmaps/by/b;->a(Lmaps/ak/n;Ljava/io/File;)Lmaps/by/b;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    const/4 v1, -0x1

    move/from16 v0, p4

    if-eq v0, v1, :cond_3

    :try_start_2
    move/from16 v0, p4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lmaps/an/a;->a(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :goto_2
    :try_start_3
    const-string v1, "DriveAbout"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v5, "GMM"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    sget-boolean v6, Lmaps/ae/h;->r:Z

    if-eqz v6, :cond_a

    if-nez v1, :cond_4

    if-eqz v5, :cond_a

    :cond_4
    sget-object v1, Lmaps/af/w;->b:[Lmaps/o/c;

    move-object v5, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lmaps/af/w;->a([Lmaps/o/c;Lmaps/ak/a;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    sget-object v5, Lmaps/af/w;->a:[Lmaps/o/c;

    invoke-static {}, Lmaps/ak/n;->f()Lmaps/ak/a;

    move-result-object v6

    move-object v7, v3

    move-object v8, v4

    move-object v9, p0

    move-object v10, p1

    invoke-static/range {v5 .. v10}, Lmaps/af/w;->a([Lmaps/o/c;Lmaps/ak/a;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    :cond_5
    :goto_3
    invoke-static {}, Lmaps/af/w;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lmaps/co/a;

    invoke-direct {v1}, Lmaps/co/a;-><init>()V

    invoke-static {v2, v4, v3, v1}, Lmaps/an/i;->a(Lmaps/ak/n;Ljava/io/File;Ljava/util/Locale;Lmaps/ae/d;)Lmaps/an/i;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lmaps/an/i;->c()V

    invoke-static {v1}, Lmaps/b/r;->a(Lmaps/an/i;)Lmaps/b/r;

    :cond_6
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lmaps/j/g;

    invoke-direct {v1, p0}, Lmaps/j/g;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lmaps/j/k;->a(Lmaps/j/k;)V

    :cond_7
    invoke-static {p0}, Lmaps/af/w;->a(Landroid/content/Context;)Z

    move-result v1

    sput-boolean v1, Lmaps/af/w;->e:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    sget-boolean v3, Lmaps/ae/h;->j:Z

    if-eqz v3, :cond_8

    const-string v3, "VectorGlobalState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Initialization took "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long/2addr v1, v12

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const/4 v1, 0x1

    sput-boolean v1, Lmaps/af/w;->d:Z

    const-string v1, "VectorGlobalState.initialize"

    invoke-static {v1}, Lmaps/ah/a;->b(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v11

    throw v1

    :cond_9
    :try_start_4
    invoke-static {v2}, Lmaps/az/c;->a(Lmaps/ak/n;)V

    goto/16 :goto_1

    :catch_0
    move-exception v1

    const-string v5, "Could not load encryption key"

    invoke-static {v5, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_a
    if-eqz p2, :cond_5

    move-object/from16 v1, p2

    move-object v5, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lmaps/af/w;->a([Lmaps/o/c;Lmaps/ak/a;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method

.method private static declared-synchronized a([Lmaps/o/c;Lmaps/ak/a;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 11

    const-class v9, Lmaps/af/w;

    monitor-enter v9

    :try_start_0
    const-string v0, "GMM"

    invoke-interface {p1}, Lmaps/ak/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x1

    sput v0, Lmaps/af/w;->c:I

    :cond_0
    array-length v10, p0

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_3

    aget-object v0, p0, v8

    invoke-static {v0}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_2
    move-object v1, p1

    move-object v2, p4

    move-object/from16 v3, p5

    move-object v4, p2

    move-object v5, p3

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lmaps/o/c;->a(Lmaps/ak/a;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/an/y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lmaps/an/y;->e()V

    invoke-static {v0, v1}, Lmaps/an/t;->a(Lmaps/o/c;Lmaps/an/y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    :cond_3
    monitor-exit v9

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lmaps/af/w;->e:Z

    return v0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 7

    const-wide/high16 v5, 0x3fd0000000000000L

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-gtz v1, :cond_0

    iget v1, v2, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-lez v1, :cond_1

    :cond_0
    move v1, v0

    :goto_0
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    div-float v1, v3, v1

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x41c80000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    iget v0, v2, Landroid/util/DisplayMetrics;->ydpi:F

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lmaps/bt/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-static {p0}, Lmaps/bt/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    invoke-static {p0}, Lmaps/bt/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    return-void
.end method

.method public static b()Z
    .locals 1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    invoke-static {}, Lmaps/k/b;->k()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .locals 4

    const-class v1, Lmaps/af/w;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmaps/af/w;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lmaps/o/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    invoke-interface {v0}, Lmaps/an/y;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/by/b;->a(Z)V

    :cond_4
    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/i;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static d()I
    .locals 1

    sget v0, Lmaps/af/w;->c:I

    return v0
.end method

.method public static declared-synchronized e()V
    .locals 4

    const-class v1, Lmaps/af/w;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmaps/af/w;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lmaps/o/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    invoke-interface {v0}, Lmaps/an/y;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/by/b;->a(Z)V

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/i;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static f()I
    .locals 1

    sget v0, Lmaps/af/w;->f:I

    return v0
.end method
