.class public Lmaps/ag/ad;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/be/i;

.field private final b:Lmaps/be/i;

.field private c:Lmaps/ag/f;

.field private final d:Lmaps/ae/d;

.field private e:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lmaps/ae/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    iput-object p2, p0, Lmaps/ag/ad;->d:Lmaps/ae/d;

    iput-object p1, p0, Lmaps/ag/ad;->e:Ljava/util/Locale;

    return-void
.end method

.method private a(Lmaps/t/bg;Z)Lmaps/t/e;
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bg;

    move-object p1, v0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/e;

    if-eqz v0, :cond_2

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz p2, :cond_3

    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_4
    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    invoke-virtual {p1}, Lmaps/t/bg;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/ag/f;->a(Ljava/lang/String;)Lmaps/ag/h;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, v0, Lmaps/ag/h;->a:Lmaps/bb/c;

    invoke-virtual {v2, v5}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v2

    if-nez v2, :cond_7

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_6

    const-string v2, "BuildingCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "malformed building id from cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lmaps/ag/h;->a:Lmaps/bb/c;

    invoke-virtual {v0, v5}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p1, v2}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    invoke-virtual {v2}, Lmaps/t/bg;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/ag/f;->a(Ljava/lang/String;)Lmaps/ag/h;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Lmaps/ag/h;->a:Lmaps/bb/c;

    invoke-virtual {v1, v5}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v2, v1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v1, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    monitor-enter v1

    :try_start_5
    iget-object v3, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v3, p1, v2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_9
    iget-object v1, v0, Lmaps/ag/h;->a:Lmaps/bb/c;

    iget-wide v2, v0, Lmaps/ag/h;->b:J

    invoke-static {v1, v2, v3}, Lmaps/t/e;->a(Lmaps/bb/c;J)Lmaps/t/e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lmaps/ag/ad;->b(Lmaps/t/e;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    :cond_a
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private b(Lmaps/t/bg;Lmaps/bb/c;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    invoke-virtual {p1}, Lmaps/t/bg;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lmaps/ag/f;->a(Ljava/lang/String;Lmaps/bb/c;)V

    invoke-virtual {p2, v6}, Lmaps/bb/c;->j(I)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v2, Lmaps/bb/c;

    sget-object v0, Lmaps/at/b;->a:Lmaps/bb/d;

    invoke-direct {v2, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {p1}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p2, v6, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    invoke-virtual {v3, v5}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    invoke-virtual {v3}, Lmaps/t/bg;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v2}, Lmaps/ag/f;->a(Ljava/lang/String;Lmaps/bb/c;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b(Lmaps/t/e;)V
    .locals 5

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    invoke-virtual {v0, v1, p1}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v2, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p1}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    iget-object v4, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bg;)Lmaps/t/e;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmaps/ag/ad;->a(Lmaps/t/bg;Z)Lmaps/t/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/t/bg;Lmaps/bb/c;)Lmaps/t/e;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v1

    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lmaps/ag/ad;->d:Lmaps/ae/d;

    invoke-interface {v2}, Lmaps/ae/d;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iget-object v4, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    if-eqz v4, :cond_2

    invoke-direct {p0, v1, p2}, Lmaps/ag/ad;->b(Lmaps/t/bg;Lmaps/bb/c;)V

    :cond_2
    invoke-static {p2, v2, v3}, Lmaps/t/e;->a(Lmaps/bb/c;J)Lmaps/t/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lmaps/ag/ad;->b(Lmaps/t/e;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Ljava/io/File;)V
    .locals 7

    new-instance v0, Lmaps/ag/f;

    iget-object v1, p0, Lmaps/ag/ad;->d:Lmaps/ae/d;

    const-string v2, "bd"

    sget-object v3, Lmaps/at/b;->a:Lmaps/bb/d;

    const/16 v4, 0xbb8

    const-wide/32 v5, 0x5265c00

    invoke-direct/range {v0 .. v6}, Lmaps/ag/f;-><init>(Lmaps/ae/d;Ljava/lang/String;Lmaps/bb/d;IJ)V

    invoke-virtual {v0, p1}, Lmaps/ag/f;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ag/ad;->e:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lmaps/ag/f;->a(Ljava/util/Locale;)Z

    iput-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/e;)Z
    .locals 1

    instance-of v0, p1, Lmaps/ag/k;

    return v0
.end method

.method public b(Lmaps/t/bg;)Lmaps/t/e;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/ag/ad;->a(Lmaps/t/bg;Z)Lmaps/t/e;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ag/ad;->b:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/ad;->c:Lmaps/ag/f;

    invoke-virtual {v0}, Lmaps/ag/f;->a()Z

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public c(Lmaps/t/bg;)V
    .locals 3

    new-instance v0, Lmaps/ag/k;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lmaps/ag/k;-><init>(Lmaps/t/bg;Lmaps/ag/o;)V

    iget-object v1, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ag/ad;->a:Lmaps/be/i;

    invoke-virtual {v2, p1, v0}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
