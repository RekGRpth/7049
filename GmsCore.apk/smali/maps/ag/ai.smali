.class Lmaps/ag/ai;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/ref/SoftReference;

.field final b:Lmaps/t/o;


# direct methods
.method public constructor <init>(Lmaps/t/o;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/t/ar;->a(Lmaps/t/o;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    :goto_0
    iput-object v0, p0, Lmaps/ag/ai;->b:Lmaps/t/o;

    iget-object v0, p0, Lmaps/ag/ai;->b:Lmaps/t/o;

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    :cond_0
    iput-object v1, p0, Lmaps/ag/ai;->a:Ljava/lang/ref/SoftReference;

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lmaps/t/o;
    .locals 1

    iget-object v0, p0, Lmaps/ag/ai;->b:Lmaps/t/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/ai;->b:Lmaps/t/o;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/ai;->a:Ljava/lang/ref/SoftReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ag/ai;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/o;

    goto :goto_0
.end method
