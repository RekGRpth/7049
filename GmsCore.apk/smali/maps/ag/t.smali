.class Lmaps/ag/t;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/aj;


# instance fields
.field private a:Lmaps/o/c;

.field private b:Ljava/util/Queue;

.field private c:Ljava/util/Map;

.field private d:Lmaps/ag/g;


# direct methods
.method public constructor <init>(Lmaps/o/c;Lmaps/ag/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ag/t;->a:Lmaps/o/c;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    iput-object p2, p0, Lmaps/ag/t;->d:Lmaps/ag/g;

    return-void
.end method

.method static synthetic a(Lmaps/ag/t;)Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/ag/t;->a:Lmaps/o/c;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/af;

    iget-object v1, p0, Lmaps/ag/t;->d:Lmaps/ag/g;

    invoke-interface {v0, v1}, Lmaps/ag/af;->a(Lmaps/ag/g;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "SDCardTileCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tiles were not inserted into the disk cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/p;

    invoke-direct {v1, p0, p1}, Lmaps/ag/p;-><init>(Lmaps/ag/t;I)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(J)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/bt/a;->b(J)Lmaps/t/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/al;

    invoke-direct {v2, p0, v0}, Lmaps/ag/al;-><init>(Lmaps/ag/t;Lmaps/t/ah;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(JI)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/bt/a;->b(J)Lmaps/t/ah;

    move-result-object v1

    iget-object v0, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/o;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/am;

    invoke-direct {v2, p0, v0, p3}, Lmaps/ag/am;-><init>(Lmaps/ag/t;Lmaps/t/o;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/o;)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/t;->c:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/y;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/ag/y;-><init>(Lmaps/ag/t;Lmaps/ag/ag;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(JI)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/bt/a;->b(J)Lmaps/t/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/ah;

    invoke-direct {v2, p0, v0, p3}, Lmaps/ag/ah;-><init>(Lmaps/ag/t;Lmaps/t/ah;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lmaps/ag/t;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/z;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/ag/z;-><init>(Lmaps/ag/t;Lmaps/ag/ag;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method
