.class Lmaps/ag/w;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    const/16 v0, 0x2000

    new-array v0, v0, [B

    invoke-direct {p0, p1, v0}, Lmaps/ag/w;-><init>(I[B)V

    return-void
.end method

.method constructor <init>(I[B)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ag/w;->d:I

    iput v0, p0, Lmaps/ag/w;->e:I

    iput-object p2, p0, Lmaps/ag/w;->a:[B

    iput p1, p0, Lmaps/ag/w;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/w;->c:I

    return-void
.end method

.method constructor <init>([B)V
    .locals 2

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ag/w;->d:I

    iput v0, p0, Lmaps/ag/w;->e:I

    iput-object p1, p0, Lmaps/ag/w;->a:[B

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/ag/l;->a([BI)I

    move-result v0

    iput v0, p0, Lmaps/ag/w;->b:I

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/ag/l;->a([BI)I

    move-result v0

    iput v0, p0, Lmaps/ag/w;->c:I

    return-void
.end method

.method static synthetic a(Lmaps/ag/w;)I
    .locals 1

    iget v0, p0, Lmaps/ag/w;->b:I

    return v0
.end method

.method static a(Lmaps/bv/a;)Lmaps/ag/w;
    .locals 5

    const/16 v2, 0x1ffc

    const/16 v0, 0x2000

    new-array v0, v0, [B

    invoke-interface {p0, v0}, Lmaps/bv/a;->b([B)V

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lmaps/ag/l;->c([BII)I

    move-result v1

    invoke-static {v0, v2}, Lmaps/ag/l;->a([BI)I

    move-result v2

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected checksum: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", expected: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lmaps/ag/w;

    invoke-direct {v1, v0}, Lmaps/ag/w;-><init>([B)V

    return-object v1
.end method

.method static synthetic b(Lmaps/ag/w;)I
    .locals 1

    iget v0, p0, Lmaps/ag/w;->c:I

    return v0
.end method


# virtual methods
.method a()I
    .locals 1

    iget v0, p0, Lmaps/ag/w;->b:I

    return v0
.end method

.method a(I)V
    .locals 4

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    const-wide/16 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lmaps/ag/l;->a([BIJ)V

    return-void
.end method

.method a(Lmaps/ag/d;)V
    .locals 2

    iget v0, p0, Lmaps/ag/w;->c:I

    invoke-virtual {p0, p1, v0}, Lmaps/ag/w;->a(Lmaps/ag/d;I)V

    iget v0, p0, Lmaps/ag/w;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ag/w;->c:I

    iget v0, p1, Lmaps/ag/d;->b:I

    iget v1, p1, Lmaps/ag/d;->d:I

    add-int/2addr v0, v1

    iget v1, p1, Lmaps/ag/d;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ag/w;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ag/w;->e:I

    return-void
.end method

.method a(Lmaps/ag/d;I)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p2, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-virtual {p1, v0, v1}, Lmaps/ag/d;->a([BI)V

    return-void
.end method

.method b()I
    .locals 1

    iget v0, p0, Lmaps/ag/w;->c:I

    return v0
.end method

.method b(I)J
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/d;->b([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method b(Lmaps/bv/a;)V
    .locals 5

    const/16 v4, 0x1ffc

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    iget v1, p0, Lmaps/ag/w;->b:I

    invoke-static {v0, v3, v1}, Lmaps/ag/l;->a([BII)V

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    const/4 v1, 0x4

    iget v2, p0, Lmaps/ag/w;->c:I

    invoke-static {v0, v1, v2}, Lmaps/ag/l;->a([BII)V

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    invoke-static {v0, v3, v4}, Lmaps/ag/l;->c([BII)I

    move-result v0

    iget-object v1, p0, Lmaps/ag/w;->a:[B

    invoke-static {v1, v4, v0}, Lmaps/ag/l;->a([BII)V

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    invoke-interface {p1, v0}, Lmaps/bv/a;->a([B)V

    return-void
.end method

.method c()I
    .locals 3

    iget v0, p0, Lmaps/ag/w;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/ag/w;->d:I

    if-gez v0, :cond_1

    iget v0, p0, Lmaps/ag/w;->c:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lmaps/ag/w;->f(I)Lmaps/ag/d;

    move-result-object v0

    iget v1, v0, Lmaps/ag/d;->b:I

    iget v2, v0, Lmaps/ag/d;->d:I

    add-int/2addr v1, v2

    iget v0, v0, Lmaps/ag/d;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/ag/w;->d:I

    :cond_1
    iget v0, p0, Lmaps/ag/w;->d:I

    goto :goto_0
.end method

.method c(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/d;->c([BI)I

    move-result v0

    return v0
.end method

.method d()I
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lmaps/ag/w;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iput v0, p0, Lmaps/ag/w;->e:I

    :goto_0
    iget v1, p0, Lmaps/ag/w;->c:I

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lmaps/ag/w;->e(I)I

    move-result v1

    if-lez v1, :cond_0

    iget v1, p0, Lmaps/ag/w;->e:I

    invoke-virtual {p0, v0}, Lmaps/ag/w;->c(I)I

    move-result v2

    invoke-virtual {p0, v0}, Lmaps/ag/w;->d(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lmaps/ag/w;->e:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/ag/w;->e:I

    return v0
.end method

.method d(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/d;->d([BI)I

    move-result v0

    return v0
.end method

.method e(I)I
    .locals 2

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lmaps/ag/d;->e([BI)I

    move-result v0

    return v0
.end method

.method f(I)Lmaps/ag/d;
    .locals 3

    iget-object v0, p0, Lmaps/ag/w;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    iget v2, p0, Lmaps/ag/w;->b:I

    invoke-static {v0, v1, v2, p1}, Lmaps/ag/d;->a([BIII)Lmaps/ag/d;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/w;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/ag/w;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
