.class public abstract Lmaps/aj/l;
.super Landroid/app/Service;


# static fields
.field public static final a:J

.field private static d:I


# instance fields
.field private volatile e:Lmaps/aj/j;

.field private final f:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x20

    sput v0, Lmaps/aj/l;->d:I

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x14

    :goto_0
    sput-wide v0, Lmaps/aj/l;->a:J

    return-void

    :cond_0
    const-wide/16 v0, 0xa

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    sget-object v0, Lmaps/aj/j;->d:Lmaps/aj/j;

    iput-object v0, p0, Lmaps/aj/l;->e:Lmaps/aj/j;

    new-instance v0, Lmaps/aj/s;

    invoke-direct {v0, p0}, Lmaps/aj/s;-><init>(Lmaps/aj/l;)V

    iput-object v0, p0, Lmaps/aj/l;->f:Landroid/os/IBinder;

    return-void
.end method

.method public static a(Lmaps/aj/t;)V
    .locals 5

    const-string v0, ""

    sget-object v1, Lmaps/aj/d;->a:[I

    invoke-virtual {p0}, Lmaps/aj/t;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "r="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x60

    const-string v2, "c"

    invoke-static {v1, v2, v0}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "u"

    goto :goto_0

    :pswitch_1
    const-string v0, "n"

    goto :goto_0

    :pswitch_2
    const-string v0, "o"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
