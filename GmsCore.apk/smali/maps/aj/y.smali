.class public abstract Lmaps/aj/y;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/ak;


# static fields
.field private static a:Lmaps/aj/y;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized c()Lmaps/aj/y;
    .locals 2

    const-class v1, Lmaps/aj/y;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/aj/y;->a:Lmaps/aj/y;

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->D:Z

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/aj/r;

    invoke-direct {v0}, Lmaps/aj/r;-><init>()V

    sput-object v0, Lmaps/aj/y;->a:Lmaps/aj/y;

    sget-object v0, Lmaps/aj/y;->a:Lmaps/aj/y;

    check-cast v0, Lmaps/aj/r;

    invoke-static {v0}, Lmaps/aj/r;->a(Lmaps/aj/r;)V

    :cond_0
    :goto_0
    sget-object v0, Lmaps/aj/y;->a:Lmaps/aj/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Lmaps/aj/k;

    invoke-direct {v0}, Lmaps/aj/k;-><init>()V

    sput-object v0, Lmaps/aj/y;->a:Lmaps/aj/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a()V
.end method
