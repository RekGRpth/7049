.class Lmaps/ak/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/util/Vector;

.field final synthetic b:Lmaps/ak/n;

.field private final c:Lmaps/bb/c;

.field private final d:Z

.field private final e:Z


# direct methods
.method constructor <init>(Lmaps/ak/n;Ljava/util/Vector;Lmaps/bb/c;)V
    .locals 1

    iput-object p1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    iput-object p3, p0, Lmaps/ak/k;->c:Lmaps/bb/c;

    invoke-static {p2}, Lmaps/ak/n;->a(Ljava/util/Vector;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ak/k;->d:Z

    invoke-static {p2}, Lmaps/ak/n;->b(Ljava/util/Vector;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/ak/k;->e:Z

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 5

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_6

    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad HTTP response code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->l()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-boolean v0, v0, Lmaps/ak/n;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-interface {v0}, Lmaps/bj/n;->a()Z

    move-result v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Server 500 for request types: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lmaps/ak/k;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lmaps/ak/n;->a(IZLjava/lang/String;)V

    :cond_2
    new-instance v0, Lmaps/ak/o;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Serverside failure (HTTP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/ak/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ak/o;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/16 v0, 0x193

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->e(Lmaps/ak/n;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->n()V

    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad HTTP response code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/ak/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/16 v0, 0x1f5

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/ak/n;->a(I)V

    const-string v0, "DRD"

    const-string v1, "Server side HTTP not implemented"

    invoke-static {v0, v1}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server side HTTP not implemented"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const-string v0, "application/binary"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_7

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad HTTP content type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad HTTP content type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/ak/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    return-void
.end method

.method private a(Ljava/io/DataInputStream;)V
    .locals 8

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1, v0}, Lmaps/ak/k;->a(Ljava/io/DataInput;Lmaps/ak/j;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/util/Vector;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    return-void

    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    :try_start_2
    sget-boolean v4, Lmaps/ae/h;->h:Z

    if-eqz v4, :cond_3

    const-string v4, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lmaps/ak/j;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    instance-of v4, v0, Ljava/io/EOFException;

    if-eqz v4, :cond_4

    invoke-interface {v1}, Lmaps/ak/j;->l()V

    iget-object v4, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-boolean v4, v4, Lmaps/ak/n;->d:Z

    if-eqz v4, :cond_4

    invoke-interface {v1}, Lmaps/ak/j;->a()I

    move-result v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No server support for data request: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v4, v4, Lmaps/ak/n;->j:Lmaps/bj/n;

    invoke-interface {v4}, Lmaps/bj/n;->a()Z

    move-result v4

    iget-object v5, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v6, 0x7

    invoke-virtual {v5, v6, v4, v1}, Lmaps/ak/n;->a(IZLjava/lang/String;)V

    :cond_4
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    iget-object v4, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v1, v2, v4}, Ljava/util/Vector;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1, v3}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    throw v0

    :catch_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    :try_start_3
    sget-boolean v4, Lmaps/ae/h;->h:Z

    if-eqz v4, :cond_6

    const-string v4, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RunTimeException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lmaps/ak/j;->a()I

    move-result v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto/16 :goto_1
.end method

.method private a(Ljava/io/DataInput;Lmaps/ak/j;)Z
    .locals 4

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processing DataRequest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    invoke-interface {p2}, Lmaps/ak/j;->a()I

    move-result v1

    if-eq v0, v1, :cond_2

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "REQUEST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expecting request type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lmaps/ak/j;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".  ABORTING!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " != "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Lmaps/ak/j;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-interface {p2, p1}, Lmaps/ak/j;->a(Ljava/io/DataInput;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Lmaps/ak/j;->k()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v0, p2}, Lmaps/ak/n;->b(Lmaps/ak/j;)V

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-interface {v0}, Lmaps/ak/j;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lmaps/ak/k;->c:Lmaps/bb/c;

    const/16 v4, 0x27

    invoke-virtual {v0, v4}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lmaps/ak/k;->c:Lmaps/bb/c;

    const/16 v5, 0x28

    invoke-virtual {v0, v5}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_0

    move v0, v1

    :goto_0
    const-string v6, "app version not set"

    invoke-static {v0, v6}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    if-eqz v5, :cond_1

    move v0, v1

    :goto_1
    const-string v6, "gmm version not set"

    invoke-static {v0, v6}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    const/16 v0, 0x2c

    invoke-static {v0}, Lmaps/ap/j;->a(C)Lmaps/ap/j;

    move-result-object v0

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v7, v7, Lmaps/ak/n;->c:Ljava/lang/String;

    aput-object v7, v6, v2

    aput-object v5, v6, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v2, v2, Lmaps/ak/n;->b:Ljava/lang/String;

    aput-object v2, v6, v1

    invoke-virtual {v0, v3, v4, v6}, Lmaps/ap/j;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private e()V
    .locals 15

    const-wide/16 v13, 0x3e8

    const/4 v4, 0x0

    const-string v0, "DataRequestDispatcher.PendingRequests.RequestsBeingProcessed.serviceRequests"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->k:Lmaps/ak/d;

    invoke-virtual {v0, p0}, Lmaps/ak/d;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lmaps/ak/k;->b()[B

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "DRD"

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lmaps/ak/n;->y()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ""

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-interface {v0}, Lmaps/ak/j;->a()I

    move-result v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v2

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->d(Lmaps/ak/m;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v1, v1, Lmaps/ak/n;->j:Lmaps/bj/n;

    const/4 v5, 0x1

    invoke-interface {v1, v0, v5}, Lmaps/bj/n;->a(Ljava/lang/String;Z)Lmaps/bj/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v8

    :try_start_1
    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_1

    const-string v1, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connection opened to:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "Content-Type"

    const-string v1, "application/binary"

    invoke-interface {v8, v0, v1}, Lmaps/bj/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Content-Length"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v5, v9

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Lmaps/bj/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->e(Lmaps/ak/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "X-Google-Maps-Mobile-API"

    invoke-direct {p0}, Lmaps/ak/k;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Lmaps/bj/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_3

    const-string v0, "REQUEST"

    const-string v1, "Open Connection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-interface {v8}, Lmaps/bj/l;->a()Ljava/io/DataOutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v7

    :try_start_2
    invoke-virtual {v7, v9}, Ljava/io/DataOutputStream;->write([B)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget v1, v0, Lmaps/ak/n;->h:I

    array-length v5, v9

    add-int/2addr v1, v5

    iput v1, v0, Lmaps/ak/n;->h:I

    invoke-interface {v8}, Lmaps/bj/l;->b()Ljava/io/DataInputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v6

    :try_start_3
    invoke-interface {v8}, Lmaps/bj/l;->d()I

    move-result v0

    invoke-interface {v8}, Lmaps/bj/l;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v4}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v4

    invoke-interface {v4}, Lmaps/ae/d;->b()J

    move-result-wide v4

    sub-long v11, v4, v2

    const-string v4, ", "

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    cmp-long v4, v11, v13

    if-gez v4, :cond_9

    const-string v4, "<1s"

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-direct {p0, v0, v1}, Lmaps/ak/k;->a(ILjava/lang/String;)V

    invoke-interface {v8}, Lmaps/bj/l;->c()J

    move-result-wide v0

    long-to-int v13, v0

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget v1, v0, Lmaps/ak/n;->i:I

    add-int/2addr v1, v13

    iput v1, v0, Lmaps/ak/n;->i:I

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/16 v1, 0x17

    if-eq v0, v1, :cond_a

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmaps/ak/n;->a(I)V

    const-string v1, "DRD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Protocol version mismatch. Client = 23 Server = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Protocol version mismatch with the server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v4, v7

    move-object v2, v8

    move-object v0, v6

    :goto_2
    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_4

    const-string v3, "REQUEST"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    if-eqz v0, :cond_5

    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_5
    :goto_3
    if-eqz v4, :cond_6

    :try_start_5
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_6
    :goto_4
    if-eqz v2, :cond_7

    :try_start_6
    invoke-interface {v2}, Lmaps/bj/l;->f()V

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_7

    const-string v0, "REQUEST"

    const-string v2, "Close"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_7
    :goto_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->l_()Z

    move-result v4

    if-nez v4, :cond_15

    sget-boolean v4, Lmaps/ae/h;->f:Z

    if-eqz v4, :cond_8

    const-string v4, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error processing: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not retrying"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v4, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v4, v0}, Lmaps/ak/n;->c(Lmaps/ak/j;)V

    goto :goto_6

    :cond_9
    const-wide/16 v4, 0x3e8

    :try_start_7
    div-long v4, v11, v4

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_a
    invoke-direct {p0, v6}, Lmaps/ak/k;->a(Ljava/io/DataInputStream;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const/16 v0, 0x16

    const-string v1, "fb"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ""

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x16

    const-string v1, "lb"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ""

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x16

    const-string v1, "flbs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "fb="

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v14, "|lb="

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v14, "|s="

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->k:Lmaps/ak/d;

    long-to-int v4, v11

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lmaps/ak/d;->a(Ljava/lang/Object;JII)V

    const/16 v0, 0x2000

    if-lt v13, v0, :cond_b

    int-to-long v0, v5

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_b

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    mul-int/lit16 v1, v13, 0x3e8

    div-int/2addr v1, v5

    invoke-static {v0, v1}, Lmaps/ak/n;->b(Lmaps/ak/n;I)I

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_b

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Loaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes.  Byte/Sec = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v2}, Lmaps/ak/n;->f(Lmaps/ak/n;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const-string v0, ", "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x3e8

    if-ge v13, v0, :cond_11

    const-string v0, "<1kb"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_7
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_c

    const-string v0, "REQUEST"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    if-eqz v6, :cond_d

    :try_start_8
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    :cond_d
    :goto_8
    if-eqz v7, :cond_e

    :try_start_9
    invoke-virtual {v7}, Ljava/io/DataOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    :cond_e
    :goto_9
    if-eqz v8, :cond_f

    :try_start_a
    invoke-interface {v8}, Lmaps/bj/l;->f()V

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_f

    const-string v0, "REQUEST"

    const-string v1, "Close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    :cond_f
    :goto_a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->l_()Z

    move-result v3

    if-nez v3, :cond_12

    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_10

    const-string v3, "REQUEST"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error processing: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not retrying"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget-object v3, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v3, v0}, Lmaps/ak/n;->c(Lmaps/ak/j;)V

    goto :goto_b

    :cond_11
    :try_start_b
    div-int/lit16 v0, v13, 0x3e8

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "kb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_7

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_d

    const-string v1, "REQUEST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :catch_1
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_e

    const-string v1, "REQUEST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing os: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    :catch_2
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_f

    const-string v1, "REQUEST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing hc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_12
    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_13

    const-string v3, "REQUEST"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retrying: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    :cond_14
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    const-string v0, "DataRequestDispatcher.PendingRequests.RequestsBeingProcessed.serviceRequests"

    invoke-static {v0}, Lmaps/ah/a;->b(Ljava/lang/String;)V

    return-void

    :catch_3
    move-exception v0

    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_5

    const-string v3, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Closing is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catch_4
    move-exception v0

    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_6

    const-string v3, "REQUEST"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Closing os: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :catch_5
    move-exception v0

    sget-boolean v2, Lmaps/ae/h;->f:Z

    if-eqz v2, :cond_7

    const-string v2, "REQUEST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Closing hc: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_15
    sget-boolean v4, Lmaps/ae/h;->f:Z

    if-eqz v4, :cond_16

    const-string v4, "REQUEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Retrying: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_17
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    const-string v0, "DataRequestDispatcher.PendingRequests.RequestsBeingProcessed.serviceRequests"

    invoke-static {v0}, Lmaps/ah/a;->b(Ljava/lang/String;)V

    throw v1

    :catchall_1
    move-exception v0

    move-object v1, v0

    move-object v2, v4

    move-object v0, v4

    goto/16 :goto_2

    :catchall_2
    move-exception v0

    move-object v1, v0

    move-object v2, v8

    move-object v0, v4

    goto/16 :goto_2

    :catchall_3
    move-exception v0

    move-object v1, v0

    move-object v2, v8

    move-object v0, v4

    move-object v4, v7

    goto/16 :goto_2
.end method

.method private f()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-static {v1}, Lmaps/ak/n;->c(Ljava/util/Vector;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->g(Lmaps/ak/n;)Lmaps/k/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/d;->a()Lmaps/bb/c;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ak/k;->c:Lmaps/bb/c;

    invoke-virtual {v0}, Lmaps/bb/c;->a()Lmaps/bb/c;

    move-result-object v0

    const/16 v2, 0x1f

    invoke-virtual {v0, v2, v1}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    :goto_1
    new-instance v1, Lmaps/ak/c;

    invoke-direct {v1, v0}, Lmaps/ak/c;-><init>(Lmaps/bb/c;)V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    instance-of v0, v0, Lmaps/ak/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1, v3}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_2
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ak/k;->c:Lmaps/bb/c;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-boolean v1, p0, Lmaps/ak/k;->d:Z

    iget-boolean v2, p0, Lmaps/ak/k;->e:Z

    invoke-static {v0, v1, v2}, Lmaps/ak/n;->a(Lmaps/ak/n;ZZ)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v1}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lmaps/ak/n;->a(Lmaps/ak/n;J)J

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "DataRequestDispatcher"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method b()[B
    .locals 5

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0}, Lmaps/ak/k;->f()V

    const/16 v0, 0x17

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-static {}, Lmaps/ae/c;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->h(Lmaps/ak/n;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->a()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    invoke-interface {v0, v2}, Lmaps/ak/j;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 4

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->c(Lmaps/ak/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_2

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->b(Lmaps/ak/m;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :try_start_2
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :goto_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-direct {p0}, Lmaps/ak/k;->e()V

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v1, v1, Lmaps/ak/n;->j:Lmaps/bj/n;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lmaps/bj/n;->a(Z)Z

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v1}, Lmaps/ak/n;->d(Lmaps/ak/n;)V

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v2, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    invoke-static {v2}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v2

    invoke-interface {v2}, Lmaps/ae/d;->b()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lmaps/ak/n;->b(Lmaps/ak/n;J)J

    iget-object v1, p0, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v1, "DRD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No requests are processed. Request count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No requests are processed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lmaps/ak/o; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception v0

    :try_start_5
    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v1, v1, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v1, v0}, Lmaps/ak/m;->a(Lmaps/ak/m;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-boolean v2, p0, Lmaps/ak/k;->d:Z

    iget-boolean v3, p0, Lmaps/ak/k;->e:Z

    invoke-static {v1, v2, v3}, Lmaps/ak/n;->b(Lmaps/ak/n;ZZ)V

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v1, v1, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v1}, Lmaps/ak/m;->c(Lmaps/ak/m;)V

    throw v0

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Lmaps/ak/n;->a(Lmaps/ak/n;ILjava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Lmaps/ak/n;->a(Lmaps/ak/n;ILjava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Lmaps/ak/n;->a(Lmaps/ak/n;ILjava/lang/Throwable;)V

    const-string v1, "REQUEST"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-static {}, Lmaps/ae/i;->a()V

    iget-object v1, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Lmaps/ak/n;->a(Lmaps/ak/n;ILjava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-boolean v1, p0, Lmaps/ak/k;->d:Z

    iget-boolean v2, p0, Lmaps/ak/k;->e:Z

    invoke-static {v0, v1, v2}, Lmaps/ak/n;->b(Lmaps/ak/n;ZZ)V

    iget-object v0, p0, Lmaps/ak/k;->b:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->c(Lmaps/ak/m;)V

    return-void

    :catch_5
    move-exception v0

    goto/16 :goto_1
.end method
