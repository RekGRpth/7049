.class public Lmaps/al/e;
.super Lmaps/al/o;


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/al/o;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/e;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/e;->i:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/al/o;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/e;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/e;->i:J

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, Lmaps/al/e;->g:Lmaps/av/i;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/e;->g:Lmaps/av/i;

    invoke-virtual {v1}, Lmaps/av/i;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/al/e;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/e;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmaps/al/o;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/al/e;->h:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/e;->h:[I

    aput v1, v0, v1

    :cond_0
    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/al/e;->h:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/e;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lmaps/al/e;->i:J

    invoke-static {v0, v1}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/e;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, Lmaps/al/e;->h:[I

    aput v3, v0, v3

    iput v3, p0, Lmaps/al/e;->a:I

    :cond_1
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/al/e;->i:J

    return-void
.end method

.method protected c(Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lmaps/al/e;->d:I

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, Lmaps/cr/c;->P()Lmaps/al/m;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/al/m;->c()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    iget-object v1, p0, Lmaps/al/e;->g:Lmaps/av/i;

    if-nez v1, :cond_2

    iget-object v1, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, Lmaps/al/e;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    :goto_0
    iget-object v1, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/IntBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/al/e;->g:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/e;->g:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->c()V

    iput-object v4, p0, Lmaps/al/e;->g:Lmaps/av/i;

    :cond_0
    iput-object v4, p0, Lmaps/al/e;->b:[I

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lmaps/al/e;->b()V

    iget-object v1, p0, Lmaps/al/e;->g:Lmaps/av/i;

    iget-object v2, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v2}, Lmaps/av/i;->a(Ljava/nio/IntBuffer;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lmaps/al/o;->c(Lmaps/cr/c;)V

    goto :goto_1
.end method

.method public d(Lmaps/cr/c;)V
    .locals 9

    const/16 v3, 0x140c

    const/4 v2, 0x3

    const v8, 0x8892

    const/4 v1, 0x0

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v4

    iput-wide v4, p0, Lmaps/al/e;->i:J

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v0, p0, Lmaps/al/e;->h:[I

    aget v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lmaps/al/e;->c(Lmaps/cr/c;)V

    :cond_2
    iget-object v0, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v4, p0, Lmaps/al/e;->h:[I

    invoke-interface {v7, v0, v4, v1}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v0, p0, Lmaps/al/e;->h:[I

    aget v0, v0, v1

    invoke-interface {v7, v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v0, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/al/e;->a:I

    iget v0, p0, Lmaps/al/e;->a:I

    iget-object v4, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    const v5, 0x88e4

    invoke-interface {v7, v8, v0, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/e;->e:Ljava/nio/IntBuffer;

    :cond_3
    iget-object v0, p0, Lmaps/al/e;->h:[I

    aget v0, v0, v1

    invoke-interface {v7, v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_4

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_1
    invoke-interface {v7, v8, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0

    :cond_4
    invoke-interface {v7, v2, v3, v1, v1}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    goto :goto_1
.end method
