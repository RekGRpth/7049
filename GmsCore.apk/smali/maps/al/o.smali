.class public Lmaps/al/o;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/al/l;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/IntBuffer;

.field f:I

.field protected g:Lmaps/av/i;

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/o;->a:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/al/o;-><init>(IZ)V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/o;->a:I

    iput-boolean p2, p0, Lmaps/al/o;->h:Z

    iput p1, p0, Lmaps/al/o;->c:I

    invoke-direct {p0}, Lmaps/al/o;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lmaps/al/o;->f:I

    iget-object v0, p0, Lmaps/al/o;->b:[I

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/al/o;->c:I

    mul-int/lit8 v0, v0, 0x3

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    iget-boolean v1, p0, Lmaps/al/o;->h:Z

    if-eqz v1, :cond_2

    :cond_0
    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/o;->b:[I

    :cond_1
    :goto_0
    iput v2, p0, Lmaps/al/o;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    return-void

    :cond_2
    new-instance v1, Lmaps/av/i;

    invoke-direct {v1, v0}, Lmaps/av/i;-><init>(I)V

    iput-object v1, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {p0}, Lmaps/al/o;->b()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->a()V

    invoke-virtual {p0}, Lmaps/al/o;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    const/16 v0, 0x20

    iget-object v1, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {v1}, Lmaps/av/i;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lmaps/al/o;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/o;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(FFF)V
    .locals 4

    const/high16 v3, 0x47800000

    iget v0, p0, Lmaps/al/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/o;->d:I

    iget-object v0, p0, Lmaps/al/o;->b:[I

    iget v1, p0, Lmaps/al/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/o;->f:I

    mul-float v2, p1, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/o;->b:[I

    iget v1, p0, Lmaps/al/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/o;->f:I

    mul-float v2, p2, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/o;->b:[I

    iget v1, p0, Lmaps/al/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/al/o;->f:I

    mul-float v2, p3, v3

    float-to-int v2, v2

    aput v2, v0, v1

    iget v0, p0, Lmaps/al/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/al/o;->b()V

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/al/o;->e()V

    return-void
.end method

.method public a(Lmaps/t/bx;I)V
    .locals 2

    iget v0, p0, Lmaps/al/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/o;->d:I

    iget-object v0, p0, Lmaps/al/o;->b:[I

    iget v1, p0, Lmaps/al/o;->f:I

    invoke-virtual {p1, p2, v0, v1}, Lmaps/t/bx;->a(I[II)V

    iget v0, p0, Lmaps/al/o;->f:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lmaps/al/o;->f:I

    iget v0, p0, Lmaps/al/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/al/o;->b()V

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/bx;IB)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    return-void
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget v1, p0, Lmaps/al/o;->f:I

    invoke-virtual {v0, v1}, Lmaps/av/i;->b(I)V

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget-object v0, v0, Lmaps/av/i;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, Lmaps/al/o;->b:[I

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget v0, v0, Lmaps/av/i;->d:I

    iput v0, p0, Lmaps/al/o;->f:I

    :cond_0
    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/al/o;->d:I

    return v0
.end method

.method protected c(Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lmaps/al/o;->d:I

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v1

    mul-int/lit8 v2, v0, 0x4

    invoke-virtual {v1, v2}, Lmaps/s/h;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    iget-object v1, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, Lmaps/al/o;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    :goto_0
    iget-object v0, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    iput-object v4, p0, Lmaps/al/o;->b:[I

    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/al/o;->b()V

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget-object v1, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v1}, Lmaps/av/i;->a(Ljava/nio/IntBuffer;)V

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->c()V

    iput-object v4, p0, Lmaps/al/o;->g:Lmaps/av/i;

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/al/o;->a:I

    return v0
.end method

.method public d(I)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/al/o;->c:I

    if-le p1, v0, :cond_4

    iget v0, p0, Lmaps/al/o;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v0, v1, 0x3

    iget-object v2, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-nez v2, :cond_6

    const/16 v2, 0x400

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, Lmaps/al/o;->h:Z

    if-eqz v2, :cond_5

    :cond_0
    iget-boolean v2, p0, Lmaps/al/o;->h:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_3

    const-string v2, "VertexBuffer"

    const-string v3, "Attempt to grow fixed size buffer"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-array v0, v0, [I

    iget-object v2, p0, Lmaps/al/o;->b:[I

    iget v3, p0, Lmaps/al/o;->f:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lmaps/al/o;->b:[I

    :goto_0
    iput v1, p0, Lmaps/al/o;->c:I

    :cond_4
    return-void

    :cond_5
    new-instance v2, Lmaps/av/i;

    invoke-direct {v2, v0}, Lmaps/av/i;-><init>(I)V

    iput-object v2, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget-object v2, p0, Lmaps/al/o;->b:[I

    iget v3, p0, Lmaps/al/o;->f:I

    invoke-virtual {v0, v2, v3}, Lmaps/av/i;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget-object v0, v0, Lmaps/av/i;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, Lmaps/al/o;->b:[I

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    iget v0, v0, Lmaps/av/i;->d:I

    iput v0, p0, Lmaps/al/o;->f:I

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {v2, v0}, Lmaps/av/i;->c(I)V

    goto :goto_0
.end method

.method public d(Lmaps/cr/c;)V
    .locals 7

    const/16 v3, 0x140c

    const/4 v2, 0x3

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/al/o;->c(Lmaps/cr/c;)V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/al/o;->a:I

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    iget-object v6, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZILjava/nio/Buffer;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v4, p0, Lmaps/al/o;->e:Ljava/nio/IntBuffer;

    invoke-interface {v0, v2, v3, v1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public e(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/o;->g:Lmaps/av/i;

    invoke-virtual {v0}, Lmaps/av/i;->c()V

    iput-object v1, p0, Lmaps/al/o;->g:Lmaps/av/i;

    :cond_0
    iput-object v1, p0, Lmaps/al/o;->b:[I

    return-void
.end method
