.class public final enum Lmaps/an/ad;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/an/ad;

.field public static final enum b:Lmaps/an/ad;

.field public static final enum c:Lmaps/an/ad;

.field public static final enum d:Lmaps/an/ad;

.field public static final enum e:Lmaps/an/ad;

.field private static final synthetic g:[Lmaps/an/ad;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x1

    new-instance v0, Lmaps/an/ad;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lmaps/an/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/an/ad;->a:Lmaps/an/ad;

    new-instance v0, Lmaps/an/ad;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v3}, Lmaps/an/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/an/ad;->b:Lmaps/an/ad;

    new-instance v0, Lmaps/an/ad;

    const-string v1, "PREFETCH_OFFLINE_MAP"

    invoke-direct {v0, v1, v6, v4}, Lmaps/an/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/an/ad;->c:Lmaps/an/ad;

    new-instance v0, Lmaps/an/ad;

    const-string v1, "PREFETCH_ROUTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lmaps/an/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/an/ad;->d:Lmaps/an/ad;

    new-instance v0, Lmaps/an/ad;

    const-string v1, "PREFETCH_AREA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, Lmaps/an/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lmaps/an/ad;->e:Lmaps/an/ad;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/an/ad;

    sget-object v1, Lmaps/an/ad;->a:Lmaps/an/ad;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/an/ad;->b:Lmaps/an/ad;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/an/ad;->c:Lmaps/an/ad;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/an/ad;->d:Lmaps/an/ad;

    aput-object v1, v0, v7

    sget-object v1, Lmaps/an/ad;->e:Lmaps/an/ad;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/an/ad;->g:[Lmaps/an/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lmaps/an/ad;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/an/ad;
    .locals 1

    const-class v0, Lmaps/an/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/an/ad;

    return-object v0
.end method

.method public static values()[Lmaps/an/ad;
    .locals 1

    sget-object v0, Lmaps/an/ad;->g:[Lmaps/an/ad;

    invoke-virtual {v0}, [Lmaps/an/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/an/ad;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/an/ad;->f:I

    return v0
.end method
