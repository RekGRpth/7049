.class public abstract Lmaps/an/ah;
.super Lmaps/an/f;


# instance fields
.field private volatile i:Z

.field private final j:I

.field private final k:Ljava/util/List;

.field private final l:I

.field private final m:I

.field private final n:F


# direct methods
.method protected constructor <init>(Lmaps/ak/a;Ljava/lang/String;Lmaps/o/c;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V
    .locals 15

    invoke-virtual/range {p3 .. p3}, Lmaps/o/c;->d()Lmaps/ag/a;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p11

    move-object/from16 v3, p13

    invoke-static {v0, v1, v2, v3}, Lmaps/an/ah;->a(Ljava/lang/String;Lmaps/o/c;ZLmaps/ag/g;)Lmaps/ag/s;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Lmaps/an/ah;->a(Lmaps/o/c;)I

    move-result v10

    move-object v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p2

    move/from16 v11, p9

    move/from16 v12, p7

    move-object/from16 v13, p10

    move-object/from16 v14, p12

    invoke-direct/range {v4 .. v14}, Lmaps/an/f;-><init>(Lmaps/ak/a;Lmaps/o/c;Ljava/lang/String;Lmaps/ag/a;Lmaps/ag/s;IZILjava/util/Locale;Ljava/io/File;)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lmaps/an/ah;->i:Z

    move/from16 v0, p4

    iput v0, p0, Lmaps/an/ah;->j:I

    move-object/from16 v0, p5

    iput-object v0, p0, Lmaps/an/ah;->k:Ljava/util/List;

    move/from16 v0, p6

    iput v0, p0, Lmaps/an/ah;->m:I

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x0

    iput v4, p0, Lmaps/an/ah;->l:I

    :goto_0
    move/from16 v0, p8

    iput v0, p0, Lmaps/an/ah;->n:F

    return-void

    :cond_1
    invoke-static/range {p4 .. p4}, Lmaps/an/ah;->a(I)I

    move-result v4

    iput v4, p0, Lmaps/an/ah;->l:I

    goto :goto_0
.end method

.method static a(I)I
    .locals 3

    const/16 v2, 0x80

    const/4 v0, 0x0

    move v1, p0

    :goto_0
    if-le v1, v2, :cond_0

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v1, v2, :cond_1

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    return v0
.end method

.method static synthetic a(Lmaps/an/ah;)I
    .locals 1

    iget v0, p0, Lmaps/an/ah;->j:I

    return v0
.end method

.method private static a(Lmaps/o/c;)I
    .locals 1

    sget-object v0, Lmaps/o/c;->d:Lmaps/o/c;

    if-ne p0, v0, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xbb8

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lmaps/o/c;ZLmaps/ag/g;)Lmaps/ag/s;
    .locals 1

    invoke-static {}, Lmaps/bt/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0, p2, p3}, Lmaps/o/c;->a(Ljava/lang/String;ZLmaps/ag/g;)Lmaps/ag/s;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/an/ah;)I
    .locals 1

    iget v0, p0, Lmaps/an/ah;->m:I

    return v0
.end method

.method static synthetic c(Lmaps/an/ah;)F
    .locals 1

    iget v0, p0, Lmaps/an/ah;->n:F

    return v0
.end method

.method static synthetic d(Lmaps/an/ah;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/an/ah;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lmaps/an/ah;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/ah;->i:Z

    return v0
.end method

.method static synthetic f(Lmaps/an/ah;)I
    .locals 1

    iget v0, p0, Lmaps/an/ah;->l:I

    return v0
.end method
