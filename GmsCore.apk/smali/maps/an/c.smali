.class Lmaps/an/c;
.super Lmaps/an/g;


# instance fields
.field a:[Lmaps/bb/c;

.field private b:Lmaps/ae/d;


# direct methods
.method constructor <init>(Lmaps/ae/d;)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lmaps/an/g;-><init>(I)V

    new-array v0, v0, [Lmaps/bb/c;

    iput-object v0, p0, Lmaps/an/c;->a:[Lmaps/bb/c;

    iput-object p1, p0, Lmaps/an/c;->b:Lmaps/ae/d;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x24

    return v0
.end method

.method public a(I)Lmaps/t/o;
    .locals 9

    const/4 v8, 0x3

    iget-object v0, p0, Lmaps/an/c;->a:[Lmaps/bb/c;

    aget-object v2, v0, p1

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v4, Lmaps/t/cl;

    invoke-direct {v4}, Lmaps/t/cl;-><init>()V

    invoke-virtual {p0, p1}, Lmaps/an/c;->b(I)Lmaps/an/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    check-cast v0, Lmaps/an/h;

    invoke-virtual {v2, v8}, Lmaps/bb/c;->j(I)I

    move-result v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v8, v1}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v6

    invoke-virtual {v4}, Lmaps/t/cl;->a()Lmaps/t/aa;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lmaps/an/k;->a(Lmaps/bb/c;Lmaps/t/aa;Lmaps/t/ah;)Lmaps/t/ca;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0x14

    if-ne v6, v7, :cond_3

    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lmaps/t/ca;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/t/ca;

    invoke-virtual {v0}, Lmaps/an/h;->a()Lmaps/t/z;

    move-result-object v5

    const-wide/16 v2, -0x1

    invoke-virtual {v5}, Lmaps/t/z;->c()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v2, p0, Lmaps/an/c;->b:Lmaps/ae/d;

    invoke-interface {v2}, Lmaps/ae/d;->b()J

    move-result-wide v2

    invoke-virtual {v5}, Lmaps/t/z;->d()J

    move-result-wide v5

    add-long/2addr v2, v5

    :cond_2
    new-instance v5, Lmaps/t/cj;

    invoke-direct {v5}, Lmaps/t/cj;-><init>()V

    invoke-virtual {v5, v4}, Lmaps/t/cj;->a(Lmaps/t/cl;)Lmaps/t/cj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lmaps/t/cj;->a(Lmaps/t/ah;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/t/cj;->a([Lmaps/t/ca;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lmaps/t/cj;->a(J)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cj;->a()Lmaps/t/ar;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0}, Lmaps/an/c;->b()Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/bb/c;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    sget-object v0, Lmaps/c/hl;->h:Lmaps/bb/d;

    invoke-static {v0, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v1, v6}, Lmaps/bb/c;->j(I)I

    move-result v2

    invoke-virtual {p0}, Lmaps/an/c;->e()I

    move-result v0

    if-eq v2, v0, :cond_1

    :cond_0
    return v5

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lmaps/an/c;->a:[Lmaps/bb/c;

    invoke-virtual {v1, v6, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected a(Lmaps/an/v;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/an/c;->e()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    check-cast v0, Lmaps/an/h;

    invoke-virtual {p0, v2}, Lmaps/an/c;->b(I)Lmaps/an/v;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v1

    check-cast v1, Lmaps/an/h;

    invoke-virtual {v0, v1}, Lmaps/an/h;->a(Lmaps/an/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method b()Lmaps/bb/c;
    .locals 11

    const/16 v10, 0x16

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x2

    new-instance v2, Lmaps/bb/c;

    sget-object v0, Lmaps/c/hl;->f:Lmaps/bb/d;

    invoke-direct {v2, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/16 v0, 0x80

    invoke-virtual {v2, v8, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {p0, v1}, Lmaps/an/c;->b(I)Lmaps/an/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    check-cast v0, Lmaps/an/h;

    invoke-virtual {v0}, Lmaps/an/h;->a()Lmaps/t/z;

    move-result-object v0

    invoke-virtual {v2, v7}, Lmaps/bb/c;->a(I)Lmaps/bb/c;

    move-result-object v3

    const/16 v4, 0x15

    invoke-virtual {v0}, Lmaps/t/z;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    invoke-virtual {v0}, Lmaps/t/z;->b()[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    invoke-virtual {v3, v10}, Lmaps/bb/c;->a(I)Lmaps/bb/c;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v5, v8, v6}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v5, v7, v6}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    invoke-virtual {v3, v10, v5}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v7, v3}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    :goto_1
    invoke-virtual {p0}, Lmaps/an/c;->e()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lmaps/an/c;->b(I)Lmaps/an/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    new-instance v3, Lmaps/bb/c;

    sget-object v4, Lmaps/c/bi;->i:Lmaps/bb/d;

    invoke-direct {v3, v4}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/16 v4, 0x8

    invoke-virtual {v3, v8, v4}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v4

    invoke-virtual {v3, v7, v4}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v0}, Lmaps/t/ah;->e()I

    move-result v4

    invoke-virtual {v3, v9, v4}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    const/4 v4, 0x4

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    invoke-virtual {v3, v4, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v2, v9, v3}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method
