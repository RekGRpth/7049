.class public Lmaps/an/h;
.super Lmaps/t/ah;


# instance fields
.field private final d:Lmaps/t/z;


# direct methods
.method public constructor <init>(Lmaps/t/ah;Lmaps/t/z;)V
    .locals 4

    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    invoke-virtual {p1}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {p1}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual {p1}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    iput-object p2, p0, Lmaps/an/h;->d:Lmaps/t/z;

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/al;)Lmaps/t/ah;
    .locals 3

    new-instance v0, Lmaps/an/h;

    invoke-super {p0, p1}, Lmaps/t/ah;->a(Lmaps/t/al;)Lmaps/t/ah;

    move-result-object v1

    iget-object v2, p0, Lmaps/an/h;->d:Lmaps/t/z;

    invoke-direct {v0, v1, v2}, Lmaps/an/h;-><init>(Lmaps/t/ah;Lmaps/t/z;)V

    return-object v0
.end method

.method public a()Lmaps/t/z;
    .locals 1

    iget-object v0, p0, Lmaps/an/h;->d:Lmaps/t/z;

    return-object v0
.end method

.method public a(Lmaps/an/h;)Z
    .locals 2

    iget-object v0, p0, Lmaps/an/h;->d:Lmaps/t/z;

    iget-object v1, p1, Lmaps/an/h;->d:Lmaps/t/z;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/an/h;

    if-eqz v1, :cond_2

    invoke-super {p0, p1}, Lmaps/t/ah;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Lmaps/an/h;

    invoke-virtual {p0, p1}, Lmaps/an/h;->a(Lmaps/an/h;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lmaps/t/ah;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lmaps/an/h;->d:Lmaps/t/z;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[layer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/an/h;->d:Lmaps/t/z;

    invoke-virtual {v1}, Lmaps/t/z;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, " params: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lmaps/an/h;->d:Lmaps/t/z;

    invoke-virtual {v0}, Lmaps/t/z;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    const-string v0, " coords: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lmaps/t/ah;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
