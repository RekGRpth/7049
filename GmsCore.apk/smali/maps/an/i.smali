.class public Lmaps/an/i;
.super Lmaps/be/l;

# interfaces
.implements Lmaps/ak/i;


# static fields
.field private static a:Lmaps/an/i;


# instance fields
.field private final b:Lmaps/ae/d;

.field private final c:Lmaps/ak/n;

.field private final d:Lmaps/ag/ad;

.field private final e:Ljava/io/File;

.field private f:Z

.field private g:Landroid/os/Handler;

.field private final h:Ljava/util/Map;

.field private i:Z


# direct methods
.method protected constructor <init>(Lmaps/ak/n;Ljava/io/File;Ljava/util/Locale;Lmaps/ae/d;)V
    .locals 2

    const-string v0, "ibs"

    invoke-direct {p0, v0}, Lmaps/be/l;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    iput-object p4, p0, Lmaps/an/i;->b:Lmaps/ae/d;

    new-instance v0, Lmaps/ag/ad;

    iget-object v1, p0, Lmaps/an/i;->b:Lmaps/ae/d;

    invoke-direct {v0, p3, v1}, Lmaps/ag/ad;-><init>(Ljava/util/Locale;Lmaps/ae/d;)V

    iput-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    iput-object p2, p0, Lmaps/an/i;->e:Ljava/io/File;

    invoke-static {}, Lmaps/f/cs;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/an/i;->h:Ljava/util/Map;

    return-void
.end method

.method public static a(Lmaps/ak/n;Ljava/io/File;Ljava/util/Locale;Lmaps/ae/d;)Lmaps/an/i;
    .locals 1

    sget-object v0, Lmaps/an/i;->a:Lmaps/an/i;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/an/i;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/an/i;-><init>(Lmaps/ak/n;Ljava/io/File;Ljava/util/Locale;Lmaps/ae/d;)V

    sput-object v0, Lmaps/an/i;->a:Lmaps/an/i;

    :cond_0
    sget-object v0, Lmaps/an/i;->a:Lmaps/an/i;

    return-object v0
.end method

.method static synthetic a(Lmaps/an/i;)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/i;->f()V

    return-void
.end method

.method static synthetic a(Lmaps/an/i;Lmaps/an/q;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/i;->a(Lmaps/an/q;)V

    return-void
.end method

.method static synthetic a(Lmaps/an/i;Lmaps/bx/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/i;->a(Lmaps/bx/c;)V

    return-void
.end method

.method private a(Lmaps/an/q;)V
    .locals 7

    const/4 v6, 0x1

    invoke-direct {p0}, Lmaps/an/i;->h()V

    iget-object v1, p1, Lmaps/an/q;->a:Lmaps/t/bg;

    iget-object v2, p1, Lmaps/an/q;->b:Lmaps/bx/d;

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v0, v1}, Lmaps/ag/ad;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-boolean v3, Lmaps/ae/h;->j:Z

    if-eqz v3, :cond_0

    const-string v3, "IndoorBuildingStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fetch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v2, :cond_1

    iget-object v3, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v3, v0}, Lmaps/ag/ad;->a(Lmaps/t/e;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v1, v3, v4}, Lmaps/bx/d;->a(Lmaps/t/bg;ILmaps/t/e;)V

    :cond_1
    :goto_0
    iget-object v3, p0, Lmaps/an/i;->b:Lmaps/ae/d;

    invoke-virtual {v0, v3}, Lmaps/t/e;->a(Lmaps/ae/d;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, v0}, Lmaps/bx/d;->a(Lmaps/t/bg;ILmaps/t/e;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/an/i;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bx/c;

    if-nez v0, :cond_5

    new-instance v0, Lmaps/bx/c;

    invoke-direct {v0, v1}, Lmaps/bx/c;-><init>(Lmaps/t/bg;)V

    iget-object v3, p0, Lmaps/an/i;->h:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v0, v2}, Lmaps/bx/c;->a(Lmaps/bx/d;)V

    :cond_6
    invoke-virtual {v0}, Lmaps/bx/c;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lmaps/an/i;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v6, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iput-boolean v6, p0, Lmaps/an/i;->i:Z

    goto :goto_1
.end method

.method private a(Lmaps/bx/c;)V
    .locals 3

    invoke-direct {p0}, Lmaps/an/i;->h()V

    iget-object v0, p0, Lmaps/an/i;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorBuildingStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleResponse: Received unexpected response for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/bx/c;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ag/ad;->c(Lmaps/t/bg;)V

    :cond_1
    :goto_0
    invoke-virtual {p1, v0}, Lmaps/bx/c;->a(Lmaps/t/e;)V

    return-void

    :cond_2
    invoke-virtual {p1}, Lmaps/bx/c;->i()Lmaps/bb/c;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lmaps/ag/ad;->a(Lmaps/t/bg;Lmaps/bb/c;)Lmaps/t/e;

    move-result-object v0

    goto :goto_0
.end method

.method public static b()Lmaps/an/i;
    .locals 1

    sget-object v0, Lmaps/an/i;->a:Lmaps/an/i;

    return-object v0
.end method

.method static synthetic b(Lmaps/an/i;Lmaps/bx/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/i;->b(Lmaps/bx/c;)V

    return-void
.end method

.method private b(Lmaps/bx/c;)V
    .locals 3

    invoke-direct {p0}, Lmaps/an/i;->h()V

    iget-object v0, p0, Lmaps/an/i;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorBuildingStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleError: Received unexpected response for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/bx/c;->f()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmaps/bx/c;->a(Lmaps/t/e;)V

    return-void
.end method

.method private f()V
    .locals 3

    invoke-direct {p0}, Lmaps/an/i;->h()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/i;->i:Z

    iget-object v0, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->j()V

    :try_start_0
    iget-object v0, p0, Lmaps/an/i;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bx/c;

    invoke-virtual {v0}, Lmaps/bx/c;->h()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    invoke-virtual {v2, v0}, Lmaps/ak/n;->a(Lmaps/ak/j;)V

    invoke-virtual {v0}, Lmaps/bx/c;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    invoke-virtual {v1}, Lmaps/ak/n;->k()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->k()V

    return-void
.end method

.method private g()V
    .locals 1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lmaps/an/i;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private h()V
    .locals 2

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on IndoorBuildingStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bg;)Lmaps/t/e;
    .locals 2

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v0, p1}, Lmaps/ag/ad;->b(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v1, v0}, Lmaps/ag/ad;->a(Lmaps/t/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorBuildingStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetworkError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 3

    invoke-interface {p1}, Lmaps/ak/j;->a()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/bg;Lmaps/bx/d;)V
    .locals 4

    iget-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lmaps/an/q;

    invoke-direct {v3, p1, p2}, Lmaps/an/q;-><init>(Lmaps/t/bg;Lmaps/bx/d;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public b(Lmaps/ak/j;)V
    .locals 3

    invoke-interface {p1}, Lmaps/ak/j;->a()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public b(Lmaps/t/bg;)Z
    .locals 2

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v0, p1}, Lmaps/ag/ad;->b(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v1, v0}, Lmaps/ag/ad;->a(Lmaps/t/e;)Z

    move-result v0

    return v0
.end method

.method public c(Lmaps/t/bg;)Lmaps/t/bi;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/an/i;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    invoke-virtual {p0}, Lmaps/an/i;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    iget-object v0, p0, Lmaps/an/i;->c:Lmaps/ak/n;

    invoke-virtual {v0, p0}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public d()V
    .locals 1

    invoke-direct {p0}, Lmaps/an/i;->g()V

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v0}, Lmaps/ag/ad;->a()V

    return-void
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lmaps/an/i;->g()V

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    invoke-virtual {v0}, Lmaps/ag/ad;->b()V

    return-void
.end method

.method public n_()V
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/af/w;->d()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lmaps/an/j;

    invoke-direct {v0, p0}, Lmaps/an/j;-><init>(Lmaps/an/i;)V

    iput-object v0, p0, Lmaps/an/i;->g:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lmaps/bt/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/i;->d:Lmaps/ag/ad;

    iget-object v1, p0, Lmaps/an/i;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Lmaps/ag/ad;->a(Ljava/io/File;)V

    :cond_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lmaps/an/i;->f:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/an/i;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
