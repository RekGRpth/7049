.class public Lmaps/an/k;
.super Lmaps/an/f;


# direct methods
.method public constructor <init>(Lmaps/ak/a;ILjava/util/Locale;Ljava/io/File;)V
    .locals 11

    sget-object v2, Lmaps/o/c;->h:Lmaps/o/c;

    const-string v3, "lts"

    new-instance v4, Lmaps/ag/b;

    const/16 v0, 0x100

    invoke-direct {v4, v0}, Lmaps/ag/b;-><init>(I)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move v8, p2

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v0 .. v10}, Lmaps/an/f;-><init>(Lmaps/ak/a;Lmaps/o/c;Ljava/lang/String;Lmaps/ag/a;Lmaps/ag/s;IZILjava/util/Locale;Ljava/io/File;)V

    return-void
.end method

.method private static a(Lmaps/bb/c;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0x22

    const-string v0, ""

    invoke-virtual {p0, v2}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v2}, Lmaps/bb/c;->d(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static a(Lmaps/bb/c;I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method static a(Lmaps/bb/c;Lmaps/t/aa;Lmaps/t/ah;)Lmaps/t/ca;
    .locals 11

    const/4 v8, 0x7

    const/4 v0, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v9, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lmaps/bb/c;->d(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v5}, Lmaps/bb/c;->j(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v4}, Lmaps/bb/c;->j(I)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v5, v9}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    const/16 v1, 0x1f

    invoke-virtual {v3, v1}, Lmaps/bb/c;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x1f

    invoke-virtual {v3, v1}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v1

    invoke-static {v1}, Lmaps/be/c;->a(Lmaps/bb/c;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p2}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/t/ax;->a(Lmaps/t/bx;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v4, v9}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v5}, Lmaps/an/k;->a(Lmaps/bb/c;I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    invoke-static {v0, v4}, Lmaps/an/k;->a(Lmaps/bb/c;I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    invoke-static {v0, v5}, Lmaps/an/k;->a(Lmaps/bb/c;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v8}, Lmaps/bb/c;->i(I)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0, v8}, Lmaps/bb/c;->d(I)I

    move-result v0

    div-int/lit8 v8, v0, 0xa

    :goto_1
    invoke-static {v3}, Lmaps/an/k;->a(Lmaps/bb/c;)Ljava/lang/String;

    move-result-object v3

    new-array v10, v9, [I

    sget-object v5, Lmaps/t/v;->a:Lmaps/t/v;

    :try_start_0
    invoke-static {v7}, Lmaps/t/v;->a(Ljava/lang/String;)Lmaps/t/v;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_2
    move-object v0, p2

    move-object v7, p1

    invoke-static/range {v0 .. v10}, Lmaps/t/n;->a(Lmaps/t/ah;Lmaps/t/bx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lmaps/t/v;Ljava/lang/String;Lmaps/t/aa;II[I)Lmaps/t/n;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v8, v9

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public a(Lmaps/t/ah;Z)Lmaps/t/o;
    .locals 2

    instance-of v0, p1, Lmaps/an/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "DashServerLayerTileStore only supports LayerCoords"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2}, Lmaps/an/f;->a(Lmaps/t/ah;Z)Lmaps/t/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/t/ah;Lmaps/bx/b;)V
    .locals 2

    instance-of v0, p1, Lmaps/an/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "DashServerLayerTileStore only supports LayerCoords"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2}, Lmaps/an/f;->a(Lmaps/t/ah;Lmaps/bx/b;)V

    return-void
.end method

.method protected h()Lmaps/an/g;
    .locals 2

    new-instance v0, Lmaps/an/c;

    iget-object v1, p0, Lmaps/an/k;->d:Lmaps/ae/d;

    invoke-direct {v0, v1}, Lmaps/an/c;-><init>(Lmaps/ae/d;)V

    return-object v0
.end method

.method public i()Lmaps/o/c;
    .locals 1

    sget-object v0, Lmaps/o/c;->h:Lmaps/o/c;

    return-object v0
.end method
