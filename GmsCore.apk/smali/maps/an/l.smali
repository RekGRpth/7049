.class public Lmaps/an/l;
.super Lmaps/an/ah;


# static fields
.field private static final i:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lmaps/an/l;->i:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lmaps/ak/a;Lmaps/o/c;Ljava/util/Locale;Ljava/io/File;Lmaps/ag/g;)V
    .locals 15

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rgts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    iget-object v2, v0, Lmaps/o/c;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x100

    sget-object v6, Lmaps/an/l;->i:Ljava/util/List;

    const/4 v7, 0x1

    const/16 v8, 0x18

    const/high16 v9, 0x3f800000

    const/4 v10, 0x1

    const/4 v12, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move-object/from16 v11, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    invoke-direct/range {v1 .. v14}, Lmaps/an/ah;-><init>(Lmaps/ak/a;Ljava/lang/String;Lmaps/o/c;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V

    return-void
.end method


# virtual methods
.method protected h()Lmaps/an/g;
    .locals 1

    new-instance v0, Lmaps/an/z;

    invoke-direct {v0, p0}, Lmaps/an/z;-><init>(Lmaps/an/l;)V

    return-object v0
.end method
