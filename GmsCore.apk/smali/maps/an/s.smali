.class Lmaps/an/s;
.super Lmaps/an/x;


# instance fields
.field final synthetic a:Lmaps/an/ae;


# direct methods
.method constructor <init>(Lmaps/an/ae;)V
    .locals 0

    iput-object p1, p0, Lmaps/an/s;->a:Lmaps/an/ae;

    invoke-direct {p0, p1}, Lmaps/an/x;-><init>(Lmaps/an/ah;)V

    return-void
.end method


# virtual methods
.method protected a(I)Lmaps/t/o;
    .locals 8

    const-wide/16 v6, -0x1

    iget-object v0, p0, Lmaps/an/s;->c:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/an/s;->a:Lmaps/an/ae;

    iget-wide v0, v0, Lmaps/an/ae;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/an/s;->a:Lmaps/an/ae;

    iget-wide v2, v2, Lmaps/an/ae;->j:J

    add-long v4, v0, v2

    :goto_1
    invoke-virtual {p0, p1}, Lmaps/an/s;->b(I)Lmaps/an/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/s;->c:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lmaps/an/s;->b(I)Lmaps/an/v;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v3

    invoke-static/range {v0 .. v7}, Lmaps/t/ar;->a(Lmaps/t/ah;[BILmaps/o/c;JJ)Lmaps/t/ar;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/s;->a:Lmaps/an/ae;

    iget-object v1, v1, Lmaps/an/ae;->d:Lmaps/ae/d;

    invoke-interface {v0, v1}, Lmaps/t/o;->c(Lmaps/ae/d;)V

    goto :goto_0

    :cond_1
    move-wide v4, v6

    goto :goto_1
.end method

.method protected a(II)[B
    .locals 2

    invoke-static {}, Lmaps/t/ar;->u()I

    move-result v0

    add-int/2addr v0, p1

    new-array v0, v0, [B

    iget v1, p0, Lmaps/an/s;->b:I

    invoke-static {v1, p2, v0}, Lmaps/t/ar;->a(II[B)V

    return-object v0
.end method

.method protected c(I)[B
    .locals 1

    iget-object v0, p0, Lmaps/an/s;->c:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
