.class public abstract Lmaps/an/x;
.super Lmaps/an/g;


# instance fields
.field protected b:I

.field protected c:[[B

.field final synthetic d:Lmaps/an/ah;


# direct methods
.method protected constructor <init>(Lmaps/an/ah;)V
    .locals 1

    const/16 v0, 0x8

    iput-object p1, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-direct {p0, v0}, Lmaps/an/g;-><init>(I)V

    new-array v0, v0, [[B

    iput-object v0, p0, Lmaps/an/x;->c:[[B

    return-void
.end method

.method private a(Lmaps/bb/c;)Landroid/util/Pair;
    .locals 8

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v2

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iget-object v3, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v3}, Lmaps/an/ah;->f(Lmaps/an/ah;)I

    move-result v3

    sub-int v3, v0, v3

    new-instance v4, Lmaps/t/al;

    invoke-direct {v4}, Lmaps/t/al;-><init>()V

    invoke-static {}, Lmaps/t/cm;->values()[Lmaps/t/cm;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    invoke-virtual {v7, p1}, Lmaps/t/cm;->a(Lmaps/bb/c;)Lmaps/t/ao;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v4, v7}, Lmaps/t/al;->a(Lmaps/t/ao;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    invoke-static {v0}, Lmaps/o/c;->a(I)Lmaps/o/c;

    move-result-object v0

    new-instance v5, Lmaps/t/ah;

    invoke-direct {v5, v3, v1, v2, v4}, Lmaps/t/ah;-><init>(IIILmaps/t/al;)V

    invoke-static {v0, v5}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 5

    new-instance v0, Lmaps/bb/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    sget-object v1, Lmaps/c/bi;->g:Lmaps/bb/d;

    invoke-static {v1, p1, v0}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/InputStream;Lmaps/bb/c;)I

    move-result v1

    sget-boolean v2, Lmaps/ae/h;->k:Z

    if-eqz v2, :cond_0

    int-to-long v1, v1

    const-wide/16 v3, 0x1

    invoke-static {v1, v2, v3, v4}, Lmaps/ah/d;->a(JJ)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/bb/c;->d(I)I

    move-result v1

    iput v1, p0, Lmaps/an/x;->b:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bb/c;->d(I)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-virtual {v1}, Lmaps/an/ah;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received tile response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private b(Ljava/io/InputStream;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/an/x;->e()I

    move-result v5

    move v1, v2

    :goto_0
    new-instance v4, Lmaps/bb/c;

    const/4 v0, 0x0

    invoke-direct {v4, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    sget-object v0, Lmaps/c/bi;->g:Lmaps/bb/d;

    invoke-static {v0, p1, v4}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/InputStream;Lmaps/bb/c;)I

    move-result v0

    if-ne v0, v3, :cond_1

    if-eq v1, v5, :cond_0

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-virtual {v0}, Lmaps/an/ah;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tiles, expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v6

    invoke-virtual {v6, v9}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6, v9}, Lmaps/bb/c;->d(I)I

    move-result v0

    :goto_1
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, Lmaps/bb/c;->c(I)[B

    move-result-object v7

    if-eqz v7, :cond_5

    array-length v4, v7

    :goto_2
    invoke-virtual {p0, v4, v0}, Lmaps/an/x;->a(II)[B

    move-result-object v0

    if-eqz v7, :cond_2

    array-length v8, v0

    sub-int/2addr v8, v4

    invoke-static {v7, v2, v0, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    if-lt v1, v5, :cond_6

    :cond_3
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    move v4, v2

    goto :goto_2

    :cond_6
    invoke-direct {p0, v6}, Lmaps/an/x;->a(Lmaps/bb/c;)Landroid/util/Pair;

    move-result-object v6

    invoke-virtual {p0, v6}, Lmaps/an/x;->a(Landroid/util/Pair;)Ljava/lang/Integer;

    move-result-object v7

    if-nez v7, :cond_7

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-virtual {v0}, Lmaps/an/ah;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Received wrong tile"

    invoke-static {v0, v4}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    if-nez v4, :cond_8

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-virtual {v0}, Lmaps/an/ah;->getName()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "COMPACT-0 tile with key: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    iget-object v4, p0, Lmaps/an/x;->c:[[B

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput-object v0, v4, v6

    goto :goto_3
.end method

.method private h()Z
    .locals 1

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/o;->d()Z

    move-result v0

    return v0
.end method

.method private i()Lmaps/an/ad;
    .locals 5

    sget-object v1, Lmaps/an/ad;->a:Lmaps/an/ad;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lmaps/an/x;->e()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lmaps/an/x;->b(I)Lmaps/an/v;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/an/v;->d()Lmaps/an/ad;

    move-result-object v2

    sget-object v3, Lmaps/an/ad;->a:Lmaps/an/ad;

    if-eq v1, v3, :cond_0

    invoke-virtual {v2}, Lmaps/an/ad;->a()I

    move-result v3

    invoke-virtual {v1}, Lmaps/an/ad;->a()I

    move-result v4

    if-ge v3, v4, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x6c

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 12

    const/4 v4, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x3

    new-instance v1, Lmaps/bb/c;

    sget-object v0, Lmaps/c/bi;->c:Lmaps/bb/d;

    invoke-direct {v1, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    new-instance v2, Lmaps/bb/c;

    sget-object v0, Lmaps/c/bi;->d:Lmaps/bb/d;

    invoke-direct {v2, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {v2, v9, v1}, Lmaps/bb/c;->b(ILmaps/bb/c;)Lmaps/bb/c;

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v0}, Lmaps/an/ah;->a(Lmaps/an/ah;)I

    move-result v0

    invoke-virtual {v1, v9, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v0}, Lmaps/an/ah;->b(Lmaps/an/ah;)I

    move-result v0

    invoke-virtual {v1, v11, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-direct {p0}, Lmaps/an/x;->i()Lmaps/an/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/ad;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v0}, Lmaps/an/ah;->c(Lmaps/an/ah;)F

    move-result v0

    const/high16 v3, 0x3f800000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    const/4 v0, 0x6

    iget-object v3, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v3}, Lmaps/an/ah;->c(Lmaps/an/ah;)F

    move-result v3

    invoke-virtual {v1, v0, v3}, Lmaps/bb/c;->a(IF)Lmaps/bb/c;

    :cond_0
    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v0}, Lmaps/an/ah;->d(Lmaps/an/ah;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v10, v0}, Lmaps/bb/c;->a(II)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v0}, Lmaps/an/ah;->e(Lmaps/an/ah;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v8, v10}, Lmaps/bb/c;->a(II)V

    :cond_2
    invoke-static {}, Lmaps/af/w;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {v1, v8, v0}, Lmaps/bb/c;->a(II)V

    :cond_3
    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, v8, v11}, Lmaps/bb/c;->a(II)V

    :cond_4
    invoke-direct {p0}, Lmaps/an/x;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1, v8, v8}, Lmaps/bb/c;->a(II)V

    :cond_5
    invoke-direct {p0}, Lmaps/an/x;->i()Lmaps/an/ad;

    move-result-object v0

    sget-object v3, Lmaps/an/ad;->a:Lmaps/an/ad;

    if-eq v0, v3, :cond_6

    invoke-direct {p0}, Lmaps/an/x;->i()Lmaps/an/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/ad;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    :cond_6
    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-eqz v0, :cond_7

    invoke-virtual {v1, v8, v4}, Lmaps/bb/c;->a(II)V

    :cond_7
    const/4 v0, 0x6

    invoke-virtual {v1, v8, v0}, Lmaps/bb/c;->a(II)V

    invoke-virtual {p0}, Lmaps/an/x;->e()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_9

    invoke-virtual {p0, v0}, Lmaps/an/x;->b(I)Lmaps/an/v;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v4

    new-instance v5, Lmaps/bb/c;

    sget-object v6, Lmaps/c/bi;->i:Lmaps/bb/d;

    invoke-direct {v5, v6}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {v4}, Lmaps/t/ah;->d()I

    move-result v6

    invoke-virtual {v5, v10, v6}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v4}, Lmaps/t/ah;->e()I

    move-result v6

    invoke-virtual {v5, v8, v6}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v4}, Lmaps/t/ah;->c()I

    move-result v6

    iget-object v7, p0, Lmaps/an/x;->d:Lmaps/an/ah;

    invoke-static {v7}, Lmaps/an/ah;->f(Lmaps/an/ah;)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v5, v11, v6}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v3}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v6

    iget v6, v6, Lmaps/o/c;->w:I

    invoke-virtual {v5, v9, v6}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    const/4 v6, 0x7

    invoke-virtual {v3}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v7

    iget v7, v7, Lmaps/o/c;->x:I

    invoke-virtual {v5, v6, v7}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    invoke-virtual {v3}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Lmaps/t/ah;->a(Lmaps/o/c;Lmaps/bb/c;)V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/k/b;->j()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x8

    invoke-virtual {v3}, Lmaps/an/v;->g()I

    move-result v3

    invoke-virtual {v5, v4, v3}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    :cond_8
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v5}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    invoke-static {p1, v2}, Lmaps/bb/b;->a(Ljava/io/DataOutput;Lmaps/bb/c;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    invoke-static {p1}, Lmaps/bb/b;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lmaps/an/x;->a(Ljava/io/InputStream;)V

    invoke-direct {p0, v1}, Lmaps/an/x;->b(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method protected a(Lmaps/an/v;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/an/x;->e()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v1}, Lmaps/an/x;->b(I)Lmaps/an/v;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ah;->c()I

    move-result v2

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ah;->c()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected a(II)[B
    .locals 1

    new-array v0, p1, [B

    return-object v0
.end method

.method protected g()I
    .locals 1

    iget v0, p0, Lmaps/an/x;->b:I

    return v0
.end method
