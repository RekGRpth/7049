.class Lmaps/an/z;
.super Lmaps/an/x;


# instance fields
.field final synthetic a:Lmaps/an/l;


# direct methods
.method constructor <init>(Lmaps/an/l;)V
    .locals 0

    iput-object p1, p0, Lmaps/an/z;->a:Lmaps/an/l;

    invoke-direct {p0, p1}, Lmaps/an/x;-><init>(Lmaps/an/ah;)V

    return-void
.end method


# virtual methods
.method protected a(I)Lmaps/t/o;
    .locals 7

    iget-object v0, p0, Lmaps/an/z;->c:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/an/z;->b(I)Lmaps/an/v;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/z;->c:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v3

    invoke-interface {v3}, Lmaps/ae/d;->b()J

    move-result-wide v3

    const-wide/32 v5, 0x48190800

    add-long/2addr v3, v5

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/br/d;->a(Lmaps/t/ah;[BIJ)Lmaps/br/d;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(I)[B
    .locals 1

    iget-object v0, p0, Lmaps/an/z;->c:[[B

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected g()I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/an/z;->c:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmaps/an/z;->c:[[B

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/an/z;->c:[[B

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmaps/br/d;->a([BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
