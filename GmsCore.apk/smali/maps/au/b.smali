.class public final Lmaps/au/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/lang/Runnable;

.field private volatile c:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/au/b;->a:Landroid/os/Handler;

    new-instance v0, Lmaps/au/g;

    invoke-direct {v0, p0, p2}, Lmaps/au/g;-><init>(Lmaps/au/b;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lmaps/au/b;->b:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0, p1}, Lmaps/au/b;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lmaps/au/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmaps/au/b;->c:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-boolean v0, p0, Lmaps/au/b;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/b;->c:Z

    iget-object v0, p0, Lmaps/au/b;->a:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/au/b;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
