.class public final Lmaps/au/c;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(D)I
    .locals 2

    const-wide v0, 0x412e848000000000L

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static a(I)I
    .locals 4

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    mul-int/2addr v1, v0

    div-int/lit16 v1, v1, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    mul-int/2addr v2, v0

    div-int/lit16 v2, v2, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    mul-int/2addr v3, v0

    div-int/lit16 v3, v3, 0xff

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(Lmaps/bq/a;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    invoke-virtual {p0}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-static {v1}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bq/a;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bq/a;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->bearing(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bq/a;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->tilt(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 5

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lmaps/t/bx;->b()D

    move-result-wide v1

    invoke-virtual {p0}, Lmaps/t/bx;->d()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lmaps/t/bw;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->builder()Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v1

    invoke-static {v1}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v1

    invoke-static {v1}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/t/u;)Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 6

    new-instance v0, Lcom/google/android/gms/maps/model/VisibleRegion;

    invoke-virtual {p0}, Lmaps/t/u;->d()Lmaps/t/bx;

    move-result-object v1

    invoke-static {v1}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/t/u;->e()Lmaps/t/bx;

    move-result-object v2

    invoke-static {v2}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {p0}, Lmaps/t/u;->g()Lmaps/t/bx;

    move-result-object v3

    invoke-static {v3}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {p0}, Lmaps/t/u;->f()Lmaps/t/bx;

    move-result-object v4

    invoke-static {v4}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    invoke-virtual {p0}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v5

    invoke-static {v5}, Lmaps/au/c;->a(Lmaps/t/bw;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/maps/model/VisibleRegion;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/bq/a;
    .locals 6

    new-instance v0, Lmaps/bq/a;

    iget-object v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/af;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    iget v3, p0, Lcom/google/android/gms/maps/model/CameraPosition;->tilt:F

    iget v4, p0, Lcom/google/android/gms/maps/model/CameraPosition;->bearing:F

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/af;FFFF)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/af;
    .locals 4

    new-instance v0, Lmaps/t/af;

    iget-wide v1, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v1, v2}, Lmaps/au/c;->a(D)I

    move-result v1

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v2, v3}, Lmaps/au/c;->a(D)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lmaps/t/af;-><init>(II)V

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v0, v1, v2, v3}, Lmaps/t/bx;->a(DD)Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/bl/a;
    .locals 6

    const-wide v4, 0x412e848000000000L

    new-instance v0, Lmaps/bl/a;

    iget-wide v1, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    mul-double/2addr v1, v4

    double-to-int v1, v1

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lmaps/bl/a;-><init>(II)V

    return-object v0
.end method
