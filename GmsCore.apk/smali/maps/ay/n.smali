.class public Lmaps/ay/n;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lmaps/ay/q;)Lmaps/ay/e;
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x3

    :try_start_0
    invoke-virtual {p0}, Lmaps/ay/q;->f()Lmaps/ay/l;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/l;->d()I

    move-result v0

    if-nez v0, :cond_5

    iget v0, v1, Lmaps/ay/l;->b:I

    const/4 v2, 0x5

    if-ge v0, v2, :cond_4

    iget v0, v1, Lmaps/ay/l;->b:I

    if-lt v0, v4, :cond_0

    invoke-virtual {v1}, Lmaps/ay/l;->e()D

    move-result-wide v2

    cmpl-double v0, v2, v5

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lmaps/ay/l;->a()Lmaps/ay/m;

    move-result-object v0

    invoke-static {v0}, Lmaps/ay/e;->a(Lmaps/ay/m;)Lmaps/ay/e;

    move-result-object v0

    iget v2, v1, Lmaps/ay/l;->b:I

    if-ne v2, v4, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/e;->a(III)Z
    :try_end_0
    .catch Lmaps/ay/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    :try_start_1
    invoke-virtual {v1, v2, v3, v4}, Lmaps/ay/l;->a(III)D

    move-result-wide v2

    cmpg-double v2, v2, v5

    if-gez v2, :cond_3

    const/4 v2, 0x2

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lmaps/ay/l;->a(III)D

    move-result-wide v1

    cmpg-double v1, v1, v5

    if-gez v1, :cond_3

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/e;->a(III)Z

    const/4 v1, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/e;->a(III)Z
    :try_end_1
    .catch Lmaps/ay/o; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lmaps/ay/o;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error when tessellating polygon: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lmaps/ay/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_2
    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/e;->a(III)Z

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/e;->a(III)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lmaps/ay/l;->b()Lmaps/ay/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/ay/n;->a(Lmaps/ay/z;Ljava/util/List;)Lmaps/ay/e;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lmaps/ay/l;->c()Lmaps/ay/r;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/ay/n;->a(Lmaps/ay/r;Ljava/util/List;)Lmaps/ay/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/ay/n;->a(Lmaps/ay/z;Ljava/util/List;)Lmaps/ay/e;
    :try_end_2
    .catch Lmaps/ay/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lmaps/ay/z;Ljava/util/List;)Lmaps/ay/e;
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v0, Lmaps/ay/i;

    invoke-direct {v0, p0}, Lmaps/ay/i;-><init>(Lmaps/ay/m;)V

    invoke-static {p0, v0, v2, p1, v3}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/i;ILjava/util/List;Lmaps/ay/h;)Lmaps/ay/h;

    move-result-object v5

    sget-object v0, Lmaps/ay/k;->b:[I

    iget-object v1, v5, Lmaps/ay/h;->a:Lmaps/ay/d;

    invoke-virtual {v1}, Lmaps/ay/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lmaps/ay/c;

    const-string v1, "Unknown subdivide result in tessellation"

    invoke-direct {v0, v1}, Lmaps/ay/c;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, v5, Lmaps/ay/h;->b:Lmaps/ay/z;

    invoke-static {v0}, Lmaps/ay/e;->a(Lmaps/ay/m;)Lmaps/ay/e;

    move-result-object v0

    iget-object v1, v5, Lmaps/ay/h;->b:Lmaps/ay/z;

    invoke-static {v1, v0, v3, v2}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/e;[II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, v5, Lmaps/ay/h;->b:Lmaps/ay/z;

    iget-object v1, v5, Lmaps/ay/h;->c:Lmaps/ay/i;

    iget v2, v5, Lmaps/ay/h;->d:I

    iget v3, v5, Lmaps/ay/h;->e:I

    iget-object v4, v5, Lmaps/ay/h;->f:Lmaps/ay/t;

    iget-object v5, v5, Lmaps/ay/h;->g:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/e;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/e;
    .locals 34

    invoke-static/range {p0 .. p0}, Lmaps/ay/e;->a(Lmaps/ay/m;)Lmaps/ay/e;

    move-result-object v27

    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v28

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v25, v12

    move-object/from16 v26, v13

    move-object/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    move-object/from16 v10, p1

    move-object/from16 v11, p0

    :goto_0
    if-nez v3, :cond_27

    if-eqz v6, :cond_0

    const/4 v2, 0x4

    new-array v2, v2, [D

    const/4 v12, 0x0

    invoke-virtual {v11, v9}, Lmaps/ay/z;->a(I)D

    move-result-wide v13

    aput-wide v13, v2, v12

    const/4 v12, 0x1

    invoke-virtual {v11, v9}, Lmaps/ay/z;->b(I)D

    move-result-wide v13

    aput-wide v13, v2, v12

    const/4 v12, 0x2

    invoke-virtual {v11, v8}, Lmaps/ay/z;->a(I)D

    move-result-wide v13

    aput-wide v13, v2, v12

    const/4 v12, 0x3

    invoke-virtual {v11, v8}, Lmaps/ay/z;->b(I)D

    move-result-wide v13

    aput-wide v13, v2, v12

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-le v9, v8, :cond_3

    move/from16 v22, v8

    :goto_1
    if-le v9, v8, :cond_4

    move v2, v9

    :goto_2
    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v12, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0, v2}, Lmaps/ay/z;->a(II)I

    move-result v13

    if-nez v13, :cond_5

    const/4 v12, 0x1

    invoke-virtual {v11, v2}, Lmaps/ay/z;->c(I)I

    move-result v14

    invoke-virtual {v11, v2}, Lmaps/ay/z;->d(I)I

    move-result v13

    move/from16 v21, v22

    move/from16 v23, v13

    move/from16 v24, v22

    move v13, v14

    :goto_3
    if-eqz v12, :cond_9

    move/from16 v0, v24

    invoke-virtual {v11, v0, v13}, Lmaps/ay/z;->g(II)Lmaps/ay/z;

    move-result-object v14

    move/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lmaps/ay/z;->g(II)Lmaps/ay/z;

    move-result-object v13

    new-instance v12, Lmaps/ay/i;

    invoke-direct {v12, v14}, Lmaps/ay/i;-><init>(Lmaps/ay/m;)V

    new-instance v2, Lmaps/ay/i;

    invoke-direct {v2, v13}, Lmaps/ay/i;-><init>(Lmaps/ay/m;)V

    invoke-virtual/range {v27 .. v27}, Lmaps/ay/e;->e()V

    :goto_4
    move/from16 v0, v16

    move-object/from16 v1, v26

    invoke-static {v14, v12, v0, v6, v1}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/i;ILjava/util/List;Lmaps/ay/h;)Lmaps/ay/h;

    move-result-object v12

    move-object/from16 v0, v25

    invoke-static {v13, v2, v15, v6, v0}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/i;ILjava/util/List;Lmaps/ay/h;)Lmaps/ay/h;

    move-result-object v2

    if-eqz v5, :cond_1f

    invoke-virtual {v14}, Lmaps/ay/z;->c()I

    move-result v15

    iget-object v14, v12, Lmaps/ay/h;->h:[I

    if-eqz v14, :cond_1

    iget-object v14, v12, Lmaps/ay/h;->h:[I

    array-length v14, v14

    if-ge v14, v15, :cond_2

    :cond_1
    new-array v14, v15, [I

    iput-object v14, v12, Lmaps/ay/h;->h:[I

    :cond_2
    const/4 v14, 0x0

    :goto_5
    if-ge v14, v15, :cond_20

    iget-object v0, v12, Lmaps/ay/h;->h:[I

    move-object/from16 v16, v0

    add-int v17, v24, v14

    aget v17, v5, v17

    aput v17, v16, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_3
    move/from16 v22, v9

    goto :goto_1

    :cond_4
    move v2, v8

    goto :goto_2

    :cond_5
    invoke-virtual {v11, v2}, Lmaps/ay/z;->c(I)I

    move-result v13

    move/from16 v0, v22

    invoke-virtual {v11, v0, v13}, Lmaps/ay/z;->a(II)I

    move-result v13

    if-nez v13, :cond_6

    const/4 v12, 0x1

    invoke-virtual {v11, v2}, Lmaps/ay/z;->c(I)I

    move-result v13

    move/from16 v21, v22

    move/from16 v23, v2

    move/from16 v24, v22

    goto :goto_3

    :cond_6
    invoke-virtual {v11, v2}, Lmaps/ay/z;->d(I)I

    move-result v13

    move/from16 v0, v22

    invoke-virtual {v11, v0, v13}, Lmaps/ay/z;->a(II)I

    move-result v13

    if-nez v13, :cond_7

    const/4 v12, 0x1

    invoke-virtual {v11, v2}, Lmaps/ay/z;->d(I)I

    move-result v13

    move/from16 v21, v22

    move/from16 v23, v13

    move/from16 v24, v22

    move v13, v2

    goto/16 :goto_3

    :cond_7
    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lmaps/ay/z;->c(I)I

    move-result v13

    invoke-virtual {v11, v13, v2}, Lmaps/ay/z;->a(II)I

    move-result v13

    if-nez v13, :cond_8

    const/4 v12, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lmaps/ay/z;->c(I)I

    move-result v13

    move/from16 v21, v13

    move/from16 v23, v2

    move/from16 v24, v22

    move v13, v2

    goto/16 :goto_3

    :cond_8
    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lmaps/ay/z;->d(I)I

    move-result v13

    invoke-virtual {v11, v13, v2}, Lmaps/ay/z;->a(II)I

    move-result v13

    if-nez v13, :cond_29

    const/4 v12, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lmaps/ay/z;->d(I)I

    move-result v13

    move/from16 v21, v22

    move/from16 v23, v2

    move/from16 v24, v13

    move v13, v2

    goto/16 :goto_3

    :cond_9
    move/from16 v0, v22

    invoke-virtual {v11, v0, v2}, Lmaps/ay/z;->g(II)Lmaps/ay/z;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1, v2}, Lmaps/ay/i;->a(Lmaps/ay/z;II)Lmaps/ay/i;

    move-result-object v15

    sub-int v12, v9, v22

    const/4 v13, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Lmaps/ay/z;->b(II)I

    move-result v19

    move/from16 v0, v22

    invoke-virtual {v11, v2, v0}, Lmaps/ay/z;->g(II)Lmaps/ay/z;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v10, v0, v2, v1}, Lmaps/ay/i;->a(Lmaps/ay/z;II)Lmaps/ay/i;

    move-result-object v14

    sub-int v12, v2, v22

    add-int/lit8 v29, v12, -0x1

    move/from16 v0, v22

    if-gt v8, v0, :cond_a

    move/from16 v20, v8

    :goto_6
    move/from16 v0, v22

    if-gt v9, v0, :cond_b

    move v12, v9

    :goto_7
    const/4 v13, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v13}, Lmaps/ay/z;->b(II)I

    move-result v18

    invoke-virtual {v11, v9}, Lmaps/ay/z;->c(I)I

    move-result v30

    invoke-virtual {v11, v9}, Lmaps/ay/z;->d(I)I

    move-result v13

    sget-object v31, Lmaps/ay/k;->a:[I

    invoke-virtual {v7}, Lmaps/ay/t;->ordinal()I

    move-result v32

    aget v31, v31, v32

    packed-switch v31, :pswitch_data_0

    move-object v2, v14

    move-object v12, v15

    move-object/from16 v13, v16

    move-object/from16 v14, v17

    move/from16 v15, v18

    move/from16 v16, v19

    goto/16 :goto_4

    :cond_a
    sub-int v12, v8, v29

    move/from16 v20, v12

    goto :goto_6

    :cond_b
    sub-int v12, v9, v29

    goto :goto_7

    :pswitch_0
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1
    sub-int v2, v8, v22

    sub-int v13, v9, v22

    invoke-virtual {v15, v2, v13}, Lmaps/ay/i;->d(II)V

    move/from16 v0, v20

    invoke-virtual {v14, v0, v12}, Lmaps/ay/i;->d(II)V

    move-object v2, v14

    move-object v12, v15

    move-object/from16 v13, v16

    move-object/from16 v14, v17

    move/from16 v15, v18

    move/from16 v16, v19

    goto/16 :goto_4

    :pswitch_2
    move/from16 v0, v30

    move/from16 v1, v22

    if-lt v0, v1, :cond_d

    move/from16 v0, v30

    if-gt v0, v2, :cond_d

    add-int/lit8 v13, v19, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_c

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    add-int/lit8 v2, v18, 0x1

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    move-object/from16 v33, v16

    move/from16 v16, v13

    move-object/from16 v13, v33

    goto/16 :goto_4

    :cond_d
    sub-int v2, v9, v22

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_e

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_e
    add-int/lit8 v12, v19, 0x1

    add-int/lit8 v2, v18, 0x1

    move-object/from16 v13, v16

    move/from16 v16, v12

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :pswitch_3
    move/from16 v0, v30

    move/from16 v1, v22

    if-lt v0, v1, :cond_10

    move/from16 v0, v30

    if-gt v0, v2, :cond_10

    sub-int v2, v9, v22

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_f

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_f
    add-int/lit8 v12, v19, 0x1

    add-int/lit8 v2, v18, 0x1

    move-object/from16 v13, v16

    move/from16 v16, v12

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :cond_10
    add-int/lit8 v13, v19, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_11

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    add-int/lit8 v2, v18, 0x1

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    move-object/from16 v33, v16

    move/from16 v16, v13

    move-object/from16 v13, v33

    goto/16 :goto_4

    :pswitch_4
    move/from16 v0, v30

    move/from16 v1, v22

    if-lt v0, v1, :cond_15

    move/from16 v0, v30

    if-gt v0, v2, :cond_15

    move/from16 v0, v30

    invoke-virtual {v10, v0, v9}, Lmaps/ay/i;->c(II)Z

    move-result v2

    if-eqz v2, :cond_13

    sub-int v2, v8, v22

    sub-int v13, v9, v22

    invoke-virtual {v15, v2, v13}, Lmaps/ay/i;->d(II)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_12

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_12
    add-int/lit8 v2, v18, 0x1

    move-object v12, v15

    move-object/from16 v13, v16

    move v15, v2

    move/from16 v16, v19

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :cond_13
    sub-int v2, v9, v22

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_14

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_14
    add-int/lit8 v12, v19, 0x1

    add-int/lit8 v2, v18, 0x1

    move-object/from16 v13, v16

    move/from16 v16, v12

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :cond_15
    move/from16 v0, v30

    invoke-virtual {v10, v0, v9}, Lmaps/ay/i;->c(II)Z

    move-result v2

    if-eqz v2, :cond_17

    sub-int v2, v9, v22

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v13, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v13, :cond_16

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_16
    add-int/lit8 v2, v19, 0x1

    move/from16 v0, v20

    invoke-virtual {v14, v0, v12}, Lmaps/ay/i;->d(II)V

    move-object v12, v15

    move-object/from16 v13, v16

    move/from16 v15, v18

    move/from16 v16, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :cond_17
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v2

    sget-object v12, Lmaps/ay/t;->f:Lmaps/ay/t;

    if-eq v2, v12, :cond_18

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_18
    add-int/lit8 v2, v18, 0x1

    add-int/lit8 v12, v19, 0x1

    move-object/from16 v13, v16

    move/from16 v16, v12

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :pswitch_5
    move/from16 v0, v22

    if-lt v13, v0, :cond_1a

    if-gt v13, v2, :cond_1a

    sub-int v2, v13, v22

    sub-int v12, v9, v22

    invoke-virtual {v15, v2, v12}, Lmaps/ay/i;->c(II)Z

    move-result v2

    if-eqz v2, :cond_19

    sub-int v2, v13, v22

    sub-int v12, v9, v22

    sget-object v13, Lmaps/ay/t;->f:Lmaps/ay/t;

    invoke-virtual {v15, v2, v12, v13}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v2

    const/4 v12, -0x1

    if-eq v2, v12, :cond_1e

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_19
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1a
    move/from16 v0, v22

    if-gt v13, v0, :cond_1b

    move v2, v13

    :goto_8
    invoke-virtual {v14, v2, v12}, Lmaps/ay/i;->c(II)Z

    move-result v2

    if-eqz v2, :cond_1d

    move/from16 v0, v22

    if-gt v13, v0, :cond_1c

    :goto_9
    sget-object v2, Lmaps/ay/t;->f:Lmaps/ay/t;

    invoke-virtual {v14, v13, v12, v2}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v2

    const/4 v12, -0x1

    if-eq v2, v12, :cond_1e

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1b
    sub-int v2, v13, v29

    goto :goto_8

    :cond_1c
    sub-int v13, v13, v29

    goto :goto_9

    :cond_1d
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Impossible case in cutAndTessellate."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1e
    add-int/lit8 v12, v19, 0x1

    add-int/lit8 v2, v18, 0x1

    move-object/from16 v13, v16

    move/from16 v16, v12

    move-object v12, v15

    move v15, v2

    move-object v2, v14

    move-object/from16 v14, v17

    goto/16 :goto_4

    :cond_1f
    const/4 v14, 0x0

    iput-object v14, v12, Lmaps/ay/h;->h:[I

    add-int v14, v4, v24

    iput v14, v12, Lmaps/ay/h;->i:I

    :cond_20
    invoke-virtual {v13}, Lmaps/ay/z;->c()I

    move-result v14

    iget-object v13, v2, Lmaps/ay/h;->h:[I

    if-eqz v13, :cond_21

    iget-object v13, v2, Lmaps/ay/h;->h:[I

    array-length v13, v13

    if-ge v13, v14, :cond_22

    :cond_21
    new-array v13, v14, [I

    iput-object v13, v2, Lmaps/ay/h;->h:[I

    :cond_22
    if-eqz v5, :cond_24

    const/4 v13, 0x0

    iget-object v15, v2, Lmaps/ay/h;->h:[I

    const/16 v16, 0x0

    add-int/lit8 v17, v21, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v5, v13, v15, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v13, v5

    move/from16 v0, v23

    if-ge v0, v13, :cond_23

    iget-object v13, v2, Lmaps/ay/h;->h:[I

    add-int/lit8 v15, v21, 0x1

    sub-int v14, v14, v21

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, v23

    invoke-static {v5, v0, v13, v15, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_23
    iget-object v13, v12, Lmaps/ay/h;->a:Lmaps/ay/d;

    sget-object v14, Lmaps/ay/d;->c:Lmaps/ay/d;

    if-ne v13, v14, :cond_28

    iget-object v13, v2, Lmaps/ay/h;->a:Lmaps/ay/d;

    sget-object v14, Lmaps/ay/d;->c:Lmaps/ay/d;

    if-eq v13, v14, :cond_28

    :goto_a
    sget-object v13, Lmaps/ay/k;->b:[I

    iget-object v14, v2, Lmaps/ay/h;->a:Lmaps/ay/d;

    invoke-virtual {v14}, Lmaps/ay/d;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_1

    new-instance v2, Lmaps/ay/c;

    const-string v3, "Unknown subdivide result in tessellation"

    invoke-direct {v2, v3}, Lmaps/ay/c;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_24
    const/4 v13, 0x0

    :goto_b
    add-int/lit8 v15, v21, 0x1

    if-ge v13, v15, :cond_25

    iget-object v15, v2, Lmaps/ay/h;->h:[I

    add-int v16, v13, v4

    aput v16, v15, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_25
    sub-int v13, v23, v21

    add-int/lit8 v13, v13, -0x1

    add-int v15, v13, v4

    add-int/lit8 v13, v21, 0x1

    :goto_c
    if-ge v13, v14, :cond_23

    iget-object v0, v2, Lmaps/ay/h;->h:[I

    move-object/from16 v16, v0

    add-int v17, v13, v15

    aput v17, v16, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    :pswitch_6
    move-object v13, v2

    :goto_d
    sget-object v2, Lmaps/ay/k;->b:[I

    iget-object v14, v12, Lmaps/ay/h;->a:Lmaps/ay/d;

    invoke-virtual {v14}, Lmaps/ay/d;->ordinal()I

    move-result v14

    aget v2, v2, v14

    packed-switch v2, :pswitch_data_2

    new-instance v2, Lmaps/ay/c;

    const-string v3, "Unknown subdivide result in tessellation"

    invoke-direct {v2, v3}, Lmaps/ay/c;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_7
    iget-object v13, v2, Lmaps/ay/h;->b:Lmaps/ay/z;

    iget-object v14, v2, Lmaps/ay/h;->h:[I

    iget v15, v2, Lmaps/ay/h;->i:I

    move-object/from16 v0, v27

    invoke-static {v13, v0, v14, v15}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/e;[II)V

    move-object v13, v2

    goto :goto_d

    :pswitch_8
    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    move-object v13, v2

    goto :goto_d

    :pswitch_9
    iget-object v10, v12, Lmaps/ay/h;->b:Lmaps/ay/z;

    iget-object v9, v12, Lmaps/ay/h;->c:Lmaps/ay/i;

    iget v8, v12, Lmaps/ay/h;->d:I

    iget v7, v12, Lmaps/ay/h;->e:I

    iget-object v6, v12, Lmaps/ay/h;->f:Lmaps/ay/t;

    iget-object v5, v12, Lmaps/ay/h;->g:Ljava/util/List;

    iget-object v4, v12, Lmaps/ay/h;->h:[I

    iget v2, v12, Lmaps/ay/h;->i:I

    move/from16 v33, v3

    move v3, v2

    move/from16 v2, v33

    :goto_e
    move-object/from16 v25, v12

    move-object/from16 v26, v13

    move-object v11, v10

    move-object v10, v9

    move v9, v8

    move v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    move v4, v3

    move v3, v2

    goto/16 :goto_0

    :pswitch_a
    iget-object v2, v12, Lmaps/ay/h;->b:Lmaps/ay/z;

    iget-object v14, v12, Lmaps/ay/h;->h:[I

    const/4 v15, 0x0

    move-object/from16 v0, v27

    invoke-static {v2, v0, v14, v15}, Lmaps/ay/n;->a(Lmaps/ay/z;Lmaps/ay/e;[II)V

    :pswitch_b
    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_26

    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ay/h;

    iget-object v10, v2, Lmaps/ay/h;->b:Lmaps/ay/z;

    iget-object v9, v2, Lmaps/ay/h;->c:Lmaps/ay/i;

    iget v8, v2, Lmaps/ay/h;->d:I

    iget v7, v2, Lmaps/ay/h;->e:I

    iget-object v6, v2, Lmaps/ay/h;->f:Lmaps/ay/t;

    iget-object v5, v2, Lmaps/ay/h;->g:Ljava/util/List;

    iget-object v4, v2, Lmaps/ay/h;->h:[I

    iget v2, v2, Lmaps/ay/h;->i:I

    move/from16 v33, v3

    move v3, v2

    move/from16 v2, v33

    goto :goto_e

    :cond_26
    const/4 v2, 0x1

    move v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move v7, v8

    move v8, v9

    move-object v9, v10

    move-object v10, v11

    goto :goto_e

    :cond_27
    return-object v27

    :cond_28
    move-object/from16 v33, v2

    move-object v2, v12

    move-object/from16 v12, v33

    goto/16 :goto_a

    :cond_29
    move/from16 v21, v22

    move/from16 v23, v2

    move v13, v2

    move/from16 v24, v22

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method

.method private static a(Lmaps/ay/z;Lmaps/ay/i;ILjava/util/List;Lmaps/ay/h;)Lmaps/ay/h;
    .locals 8

    if-nez p4, :cond_7

    new-instance v0, Lmaps/ay/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/ay/h;-><init>(Lmaps/ay/k;)V

    :goto_0
    invoke-virtual {p0}, Lmaps/ay/z;->c()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    sget-object v1, Lmaps/ay/d;->a:Lmaps/ay/d;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    :goto_2
    iget v1, p0, Lmaps/ay/z;->c:I

    if-ge p2, v1, :cond_6

    invoke-virtual {p0, p2}, Lmaps/ay/z;->i(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lmaps/ay/z;->c(I)I

    move-result v1

    invoke-virtual {p0, v4}, Lmaps/ay/z;->d(I)I

    move-result v2

    invoke-virtual {p0, v4}, Lmaps/ay/z;->e(I)Lmaps/ay/t;

    move-result-object v6

    sget-object v3, Lmaps/ay/k;->a:[I

    invoke-virtual {v6}, Lmaps/ay/t;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :pswitch_0
    invoke-virtual {p1, v1, v4, v2}, Lmaps/ay/i;->a(III)V

    goto :goto_3

    :pswitch_1
    invoke-virtual {p1, v1, v4, v6}, Lmaps/ay/i;->a(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_2

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v4, v2, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_1

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1, v4, v2, v6}, Lmaps/ay/i;->a(IILmaps/ay/t;)I

    move-result v5

    const/4 v2, -0x1

    if-eq v5, v2, :cond_3

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v1, v4, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_1

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p1, v4}, Lmaps/ay/i;->b(I)I

    move-result v5

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    invoke-virtual {p1, v4, v2, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v2, -0x1

    if-eq v5, v2, :cond_4

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1, v1, v4, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_1

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p1, v1, v4, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_5

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p1, v4, v2, v6}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    move-result v5

    const/4 v1, -0x1

    if-eq v5, v1, :cond_1

    sget-object v1, Lmaps/ay/d;->c:Lmaps/ay/d;

    move-object v2, p0

    move-object v3, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    sget-object v1, Lmaps/ay/d;->b:Lmaps/ay/d;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-virtual/range {v0 .. v7}, Lmaps/ay/h;->a(Lmaps/ay/d;Lmaps/ay/z;Lmaps/ay/i;IILmaps/ay/t;Ljava/util/List;)Lmaps/ay/h;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move-object v0, p4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Lmaps/ay/r;Ljava/util/List;)Lmaps/ay/z;
    .locals 11

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/ay/r;->b()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v3, Lmaps/ay/i;

    invoke-direct {v3, p0}, Lmaps/ay/i;-><init>(Lmaps/ay/m;)V

    mul-int/lit8 v1, v2, 0x2

    new-array v4, v1, [I

    move v1, v0

    :goto_1
    iget v5, p0, Lmaps/ay/r;->c:I

    if-ge v0, v5, :cond_3

    invoke-virtual {p0, v0}, Lmaps/ay/r;->i(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lmaps/ay/r;->c(I)I

    move-result v6

    invoke-virtual {p0, v5}, Lmaps/ay/r;->d(I)I

    move-result v7

    invoke-virtual {p0, v5}, Lmaps/ay/r;->e(I)Lmaps/ay/t;

    move-result-object v8

    sget-object v9, Lmaps/ay/k;->a:[I

    invoke-virtual {v8}, Lmaps/ay/t;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_0
    invoke-virtual {v3, v6, v5, v8}, Lmaps/ay/i;->a(IILmaps/ay/t;)I

    invoke-virtual {v3, v5, v7, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    goto :goto_2

    :pswitch_1
    invoke-virtual {v3, v5, v7, v8}, Lmaps/ay/i;->a(IILmaps/ay/t;)I

    invoke-virtual {v3, v6, v5, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    goto :goto_2

    :pswitch_2
    invoke-virtual {v3, v5}, Lmaps/ay/i;->a(I)I

    move-result v8

    invoke-virtual {p0, v5, v8}, Lmaps/ay/r;->f(II)Z

    move-result v9

    if-eqz v9, :cond_2

    aput v5, v4, v1

    add-int/lit8 v9, v1, 0x1

    aput v8, v4, v9

    add-int/lit8 v1, v1, 0x2

    mul-int/lit8 v8, v2, 0x2

    if-ne v1, v8, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0, v4}, Lmaps/ay/r;->b([I)Lmaps/ay/z;

    move-result-object p0

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v6, v5, v7}, Lmaps/ay/i;->b(III)V

    goto :goto_2

    :pswitch_3
    invoke-virtual {v3, v5, v7, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    invoke-virtual {v3, v6, v5, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    goto :goto_2

    :pswitch_4
    invoke-virtual {v3, v6, v5, v7}, Lmaps/ay/i;->a(III)V

    goto :goto_2

    :pswitch_5
    invoke-virtual {v3, v6, v5, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    invoke-virtual {v3, v5, v7, v8}, Lmaps/ay/i;->b(IILmaps/ay/t;)I

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Tesselation could not cut all holes open."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Lmaps/ay/e;III[II)V
    .locals 3

    if-eqz p4, :cond_0

    aget v0, p4, p1

    aget v1, p4, p2

    aget v2, p4, p3

    invoke-virtual {p0, v0, v1, v2}, Lmaps/ay/e;->a(III)Z

    :goto_0
    return-void

    :cond_0
    add-int v0, p1, p5

    add-int v1, p2, p5

    add-int v2, p3, p5

    invoke-virtual {p0, v0, v1, v2}, Lmaps/ay/e;->a(III)Z

    goto :goto_0
.end method

.method private static a(Lmaps/ay/z;Lmaps/ay/e;[II)V
    .locals 14

    invoke-virtual {p0}, Lmaps/ay/z;->c()I

    move-result v12

    const/4 v0, 0x3

    if-ge v12, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/ay/z;->a()Lmaps/ay/m;

    move-result-object v13

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/ay/z;->i(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lmaps/ay/z;->i(I)I

    move-result v2

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    aput v0, v1, v3

    invoke-virtual {v13, v1}, Lmaps/ay/m;->a([I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    invoke-virtual {v13, v0}, Lmaps/ay/m;->a([I)V

    const/4 v0, 0x2

    move v11, v0

    :goto_0
    if-ge v11, v12, :cond_0

    invoke-virtual {p0, v11}, Lmaps/ay/z;->i(I)I

    move-result v1

    invoke-virtual {p0, v11}, Lmaps/ay/z;->k(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v13}, Lmaps/ay/m;->d()I

    move-result v3

    :goto_1
    iget v0, v13, Lmaps/ay/m;->c:I

    const/4 v4, 0x1

    if-le v0, v4, :cond_2

    invoke-virtual {p0, v1, v2, v3}, Lmaps/ay/z;->a(III)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_2

    move-object v0, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    invoke-static/range {v0 .. v5}, Lmaps/ay/n;->a(Lmaps/ay/e;III[II)V

    invoke-virtual {v13}, Lmaps/ay/m;->f()V

    invoke-virtual {v13}, Lmaps/ay/m;->d()I

    move-result v0

    move v2, v3

    move v3, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v1, v0, v2

    invoke-virtual {v13, v0}, Lmaps/ay/m;->a([I)V

    :goto_2
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    move v2, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v11}, Lmaps/ay/z;->l(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v13}, Lmaps/ay/m;->d()I

    move-result v3

    move v4, v2

    :goto_3
    iget v0, v13, Lmaps/ay/m;->c:I

    const/4 v2, 0x1

    if-le v0, v2, :cond_4

    invoke-virtual {p0, v3, v4, v1}, Lmaps/ay/z;->a(III)D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpl-double v0, v5, v7

    if-lez v0, :cond_4

    move-object v2, p1

    move v5, v1

    move-object/from16 v6, p2

    move/from16 v7, p3

    invoke-static/range {v2 .. v7}, Lmaps/ay/n;->a(Lmaps/ay/e;III[II)V

    invoke-virtual {v13}, Lmaps/ay/m;->f()V

    invoke-virtual {v13}, Lmaps/ay/m;->d()I

    move-result v0

    move v4, v3

    move v3, v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v1, v0, v2

    invoke-virtual {v13, v0}, Lmaps/ay/m;->a([I)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v13, v0}, Lmaps/ay/m;->i(I)I

    move-result v3

    const/4 v0, 0x1

    move v10, v0

    :goto_4
    iget v0, v13, Lmaps/ay/m;->c:I

    if-ge v10, v0, :cond_7

    invoke-virtual {v13, v10}, Lmaps/ay/m;->i(I)I

    move-result v2

    invoke-virtual {p0, v3, v2, v1}, Lmaps/ay/z;->a(III)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_6

    move-object v0, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    invoke-static/range {v0 .. v5}, Lmaps/ay/n;->a(Lmaps/ay/e;III[II)V

    :goto_5
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    move v3, v2

    goto :goto_4

    :cond_6
    move-object v4, p1

    move v5, v3

    move v6, v2

    move v7, v1

    move-object/from16 v8, p2

    move/from16 v9, p3

    invoke-static/range {v4 .. v9}, Lmaps/ay/n;->a(Lmaps/ay/e;III[II)V

    goto :goto_5

    :cond_7
    invoke-virtual {v13}, Lmaps/ay/m;->e()V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v3, v0, v2

    invoke-virtual {v13, v0}, Lmaps/ay/m;->a([I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v1, v0, v2

    invoke-virtual {v13, v0}, Lmaps/ay/m;->a([I)V

    goto/16 :goto_2
.end method

.method public static b(Lmaps/ay/q;)Lmaps/ay/b;
    .locals 1

    invoke-virtual {p0}, Lmaps/ay/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ay/b;->a:Lmaps/ay/b;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lmaps/ay/q;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/ay/b;->b:Lmaps/ay/b;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/ay/q;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lmaps/ay/b;->d:Lmaps/ay/b;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/ay/q;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lmaps/ay/b;->c:Lmaps/ay/b;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
