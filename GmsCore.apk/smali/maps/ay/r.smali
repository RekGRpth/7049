.class Lmaps/ay/r;
.super Lmaps/ay/z;


# instance fields
.field protected d:[Z


# direct methods
.method constructor <init>(Lmaps/ay/l;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/ay/z;-><init>(Lmaps/ay/l;)V

    invoke-virtual {p0}, Lmaps/ay/r;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lmaps/ay/r;->d:[Z

    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([ZZ)V

    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, v2

    return-void
.end method

.method private a([DIDDII)I
    .locals 3

    move v0, p7

    :goto_0
    if-ge v0, p8, :cond_1

    mul-int v1, p2, v0

    aget-wide v1, p1, v1

    cmpl-double v1, v1, p3

    if-nez v1, :cond_0

    mul-int v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aget-wide v1, p1, v1

    cmpl-double v1, v1, p5

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(DDDDDD)Z
    .locals 8

    sub-double v0, p1, p5

    sub-double v2, p3, p7

    sub-double v4, p9, p5

    sub-double v6, p11, p7

    mul-double/2addr v0, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lmaps/ay/r;

    return v0
.end method

.method public b([I)Lmaps/ay/z;
    .locals 32

    invoke-virtual/range {p0 .. p0}, Lmaps/ay/r;->b()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v4, 0x2

    invoke-virtual/range {p0 .. p0}, Lmaps/ay/r;->c()I

    move-result v2

    move-object/from16 v0, p1

    array-length v3, v0

    add-int/2addr v2, v3

    mul-int/2addr v2, v4

    new-array v3, v2, [D

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lmaps/ay/s;->h(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v3, v6, v10}, Lmaps/ay/s;->a(I[DII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->d:[Z

    const/4 v5, 0x0

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([ZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->d:[Z

    const/4 v5, 0x0

    const/4 v6, 0x1

    aput-boolean v6, v2, v5

    const/4 v2, 0x0

    move/from16 v24, v2

    :goto_1
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, v24

    if-ge v0, v2, :cond_9

    aget v5, p1, v24

    add-int/lit8 v2, v24, 0x1

    aget v6, p1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v2, v6}, Lmaps/ay/s;->g(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/ay/r;->d:[Z

    aget-boolean v7, v7, v2

    if-eqz v7, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v2, v5}, Lmaps/ay/s;->g(I)I

    move-result v2

    move/from16 v25, v2

    move/from16 v26, v5

    move v2, v6

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lmaps/ay/s;->h(I)I

    move-result v29

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    add-int/lit8 v6, v25, 0x1

    invoke-virtual {v5, v6}, Lmaps/ay/s;->h(I)I

    move-result v30

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v5, v2}, Lmaps/ay/s;->a(I)D

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v7, v2}, Lmaps/ay/s;->b(I)D

    move-result-wide v7

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lmaps/ay/r;->a([DIDDII)I

    move-result v28

    add-int/lit8 v9, v28, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lmaps/ay/r;->a([DIDDII)I

    move-result v2

    const/4 v9, -0x1

    if-eq v2, v9, :cond_6

    const/16 v27, 0x0

    :goto_3
    if-nez v27, :cond_6

    add-int/lit8 v2, v28, -0x1

    rem-int/2addr v2, v10

    if-gez v2, :cond_1

    add-int/2addr v2, v10

    :cond_1
    add-int/lit8 v9, v28, 0x1

    rem-int/2addr v9, v10

    if-gez v9, :cond_2

    add-int/2addr v9, v10

    :cond_2
    mul-int v11, v4, v2

    aget-wide v12, v3, v11

    mul-int v11, v4, v2

    add-int/lit8 v11, v11, 0x1

    aget-wide v14, v3, v11

    mul-int v11, v4, v28

    aget-wide v16, v3, v11

    mul-int v11, v4, v28

    add-int/lit8 v11, v11, 0x1

    aget-wide v18, v3, v11

    mul-int v11, v4, v9

    aget-wide v20, v3, v11

    mul-int v11, v4, v9

    add-int/lit8 v11, v11, 0x1

    aget-wide v22, v3, v11

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v23}, Lmaps/ay/r;->a(DDDDDD)Z

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmaps/ay/r;->a(I)D

    move-result-wide v12

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmaps/ay/r;->b(I)D

    move-result-wide v14

    mul-int v11, v4, v28

    aget-wide v16, v3, v11

    mul-int v11, v4, v28

    add-int/lit8 v11, v11, 0x1

    aget-wide v18, v3, v11

    mul-int v11, v4, v9

    aget-wide v20, v3, v11

    mul-int/2addr v9, v4

    add-int/lit8 v9, v9, 0x1

    aget-wide v22, v3, v9

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v23}, Lmaps/ay/r;->a(DDDDDD)Z

    move-result v9

    mul-int v11, v4, v2

    aget-wide v12, v3, v11

    mul-int/2addr v2, v4

    add-int/lit8 v2, v2, 0x1

    aget-wide v14, v3, v2

    mul-int v2, v4, v28

    aget-wide v16, v3, v2

    mul-int v2, v4, v28

    add-int/lit8 v2, v2, 0x1

    aget-wide v18, v3, v2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmaps/ay/r;->a(I)D

    move-result-wide v20

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmaps/ay/r;->b(I)D

    move-result-wide v22

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v23}, Lmaps/ay/r;->a(DDDDDD)Z

    move-result v2

    if-eqz v31, :cond_4

    if-eqz v9, :cond_5

    if-eqz v2, :cond_5

    :cond_3
    add-int/lit8 v9, v28, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lmaps/ay/r;->a([DIDDII)I

    move-result v2

    move v9, v2

    move/from16 v2, v27

    :goto_4
    move/from16 v27, v2

    move/from16 v28, v9

    goto/16 :goto_3

    :cond_4
    if-nez v9, :cond_3

    if-nez v2, :cond_3

    :cond_5
    const/4 v2, 0x1

    move/from16 v9, v28

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Lmaps/ay/s;->a(I)D

    move-result-wide v11

    cmpl-double v2, v11, v5

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Lmaps/ay/s;->b(I)D

    move-result-wide v5

    cmpl-double v2, v5, v7

    if-eqz v2, :cond_8

    :cond_7
    sub-int v2, v10, v28

    add-int v5, v28, v30

    sub-int v5, v5, v29

    add-int/lit8 v5, v5, 0x2

    mul-int v6, v28, v4

    mul-int/2addr v5, v4

    mul-int/2addr v2, v4

    invoke-static {v3, v6, v3, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v28, 0x1

    sub-int v5, v30, v26

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v26

    invoke-virtual {v6, v0, v3, v2, v5}, Lmaps/ay/s;->a(I[DII)V

    add-int/2addr v2, v5

    sub-int v5, v26, v29

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v29

    invoke-virtual {v6, v0, v3, v2, v5}, Lmaps/ay/s;->a(I[DII)V

    sub-int v2, v30, v29

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v10, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->d:[Z

    const/4 v5, 0x1

    aput-boolean v5, v2, v25

    :goto_5
    add-int/lit8 v2, v24, 0x2

    move/from16 v24, v2

    goto/16 :goto_1

    :cond_8
    sub-int v2, v10, v28

    add-int/lit8 v2, v2, -0x1

    add-int v5, v28, v30

    sub-int v5, v5, v29

    add-int/lit8 v6, v28, 0x1

    mul-int/2addr v6, v4

    mul-int/2addr v5, v4

    mul-int/2addr v2, v4

    invoke-static {v3, v6, v3, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v28, 0x1

    sub-int v5, v30, v26

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v26

    invoke-virtual {v6, v0, v3, v2, v5}, Lmaps/ay/s;->a(I[DII)V

    add-int/2addr v2, v5

    sub-int v5, v26, v29

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/ay/r;->a:Lmaps/ay/s;

    move/from16 v0, v29

    invoke-virtual {v6, v0, v3, v2, v5}, Lmaps/ay/s;->a(I[DII)V

    sub-int v2, v30, v29

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v10, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ay/r;->d:[Z

    const/4 v5, 0x1

    aput-boolean v5, v2, v25

    goto :goto_5

    :cond_9
    new-instance p0, Lmaps/ay/z;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lmaps/ay/z;-><init>([D)V

    goto/16 :goto_0

    :cond_a
    move/from16 v25, v2

    move/from16 v26, v6

    move v2, v5

    goto/16 :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/ay/r;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lmaps/ay/r;

    invoke-virtual {p1, p0}, Lmaps/ay/r;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-super {p0, p1}, Lmaps/ay/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/ay/r;->d:[Z

    iget-object v3, p1, Lmaps/ay/r;->d:[Z

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f(II)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v2, p1}, Lmaps/ay/s;->g(I)I

    move-result v2

    iget-object v3, p0, Lmaps/ay/r;->a:Lmaps/ay/s;

    invoke-virtual {v3, p2}, Lmaps/ay/s;->g(I)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v4, p0, Lmaps/ay/r;->d:[Z

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_2

    iget-object v4, p0, Lmaps/ay/r;->d:[Z

    aget-boolean v4, v4, v3

    if-nez v4, :cond_0

    :cond_2
    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    aput-boolean v1, v0, v3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    aget-boolean v0, v0, v3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ay/r;->d:[Z

    aput-boolean v1, v0, v2

    move v0, v1

    goto :goto_0

    :cond_4
    new-instance v0, Lmaps/ay/c;

    const-string v1, "Some outer chains have not been cut."

    invoke-direct {v0, v1}, Lmaps/ay/c;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lmaps/ay/z;->hashCode()I

    move-result v0

    iget-object v1, p0, Lmaps/ay/r;->d:[Z

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Z)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method
