.class Lmaps/ay/z;
.super Lmaps/ay/m;

# interfaces
.implements Lmaps/cg/d;


# direct methods
.method constructor <init>(Lmaps/ay/l;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ay/m;-><init>(Lmaps/ay/s;)V

    invoke-direct {p0, p1}, Lmaps/ay/z;->a(Lmaps/ay/s;)V

    return-void
.end method

.method protected constructor <init>([D)V
    .locals 1

    invoke-direct {p0, p1}, Lmaps/ay/m;-><init>([D)V

    iget-object v0, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-direct {p0, v0}, Lmaps/ay/z;->a(Lmaps/ay/s;)V

    return-void
.end method

.method private constructor <init>([D[I)V
    .locals 1

    invoke-static {p1}, Lmaps/ay/l;->a([D)Lmaps/ay/l;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmaps/ay/m;-><init>(Lmaps/ay/s;[I)V

    return-void
.end method

.method private a(Lmaps/ay/s;)V
    .locals 3

    const/4 v1, 0x0

    iget v0, p1, Lmaps/ay/s;->b:I

    iput v0, p0, Lmaps/ay/z;->c:I

    iget v0, p0, Lmaps/ay/z;->c:I

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/ay/z;->b:[I

    move v0, v1

    :goto_0
    iget v2, p0, Lmaps/ay/z;->c:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lmaps/ay/z;->b:[I

    aput v0, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lmaps/cg/b;->a()Lmaps/cg/c;

    move-result-object v0

    iget v2, p0, Lmaps/ay/z;->c:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, p0, v1, v2}, Lmaps/cg/c;->a(Lmaps/cg/d;II)V

    return-void
.end method

.method private f(II)[I
    .locals 7

    const/4 v0, 0x0

    if-le p1, p2, :cond_3

    sub-int v1, p1, p2

    add-int/lit8 v3, v1, -0x1

    iget v1, p0, Lmaps/ay/z;->c:I

    sub-int/2addr v1, v3

    new-array v1, v1, [I

    iget-object v4, p0, Lmaps/ay/z;->b:[I

    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    aget v6, v4, v2

    if-gt v6, p2, :cond_0

    aput v6, v1, v0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    if-lt v6, p1, :cond_1

    sub-int/2addr v6, v3

    aput v6, v1, v0

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_3
    sub-int v1, p2, p1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iget-object v3, p0, Lmaps/ay/z;->b:[I

    array-length v4, v3

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_5

    aget v5, v3, v2

    if-lt v5, p1, :cond_4

    if-gt v5, p2, :cond_4

    sub-int/2addr v5, p1

    aput v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public varargs a([I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a_(II)Z
    .locals 9

    const/4 v8, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    iget-object v3, p0, Lmaps/ay/z;->b:[I

    aget v3, v3, p1

    iget-object v4, p0, Lmaps/ay/z;->b:[I

    aget v4, v4, p2

    invoke-virtual {v2, v3, v4}, Lmaps/ay/s;->b(II)I

    move-result v2

    if-eqz v2, :cond_1

    if-gez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmaps/ay/z;->b:[I

    aget v2, v2, p1

    iget-object v3, p0, Lmaps/ay/z;->b:[I

    aget v3, v3, p2

    new-array v4, v8, [I

    aput v2, v4, v1

    iget-object v5, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v5, v2}, Lmaps/ay/s;->d(I)I

    move-result v5

    aput v5, v4, v0

    new-array v5, v8, [I

    aput v3, v5, v1

    iget-object v6, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v6, v3}, Lmaps/ay/s;->d(I)I

    move-result v6

    aput v6, v5, v0

    new-array v6, v8, [I

    aput v2, v6, v1

    iget-object v7, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v7, v2}, Lmaps/ay/s;->c(I)I

    move-result v7

    aput v7, v6, v0

    new-array v7, v8, [I

    aput v3, v7, v1

    iget-object v8, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v8, v3}, Lmaps/ay/s;->c(I)I

    move-result v3

    aput v3, v7, v0

    new-instance v3, Lmaps/ay/w;

    iget-object v8, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-direct {v3, v8, v2}, Lmaps/ay/w;-><init>(Lmaps/ay/s;I)V

    invoke-virtual {v3, v4, v5}, Lmaps/ay/w;->a([I[I)I

    move-result v2

    if-gez v2, :cond_4

    move v2, v0

    :goto_1
    invoke-virtual {v3, v6, v7}, Lmaps/ay/w;->a([I[I)I

    move-result v8

    if-gez v8, :cond_5

    invoke-virtual {v3, v6, v5}, Lmaps/ay/w;->a([I[I)I

    move-result v3

    if-ltz v3, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    move v1, v0

    :cond_3
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v3, v4, v7}, Lmaps/ay/w;->a([I[I)I

    move-result v3

    if-gez v3, :cond_3

    if-eqz v2, :cond_3

    move v1, v0

    goto :goto_2
.end method

.method public b_(II)V
    .locals 3

    iget-object v0, p0, Lmaps/ay/z;->b:[I

    aget v0, v0, p1

    iget-object v1, p0, Lmaps/ay/z;->b:[I

    iget-object v2, p0, Lmaps/ay/z;->b:[I

    aget v2, v2, p2

    aput v2, v1, p1

    iget-object v1, p0, Lmaps/ay/z;->b:[I

    aput v0, v1, p2

    return-void
.end method

.method public d(II)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(II)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g(II)Lmaps/ay/z;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v0}, Lmaps/ay/s;->d()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot create a sorted sublist when there are holes."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-le p1, p2, :cond_1

    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p0}, Lmaps/ay/z;->c()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v0, p1

    mul-int/lit8 v0, v0, 0x2

    new-array v1, v0, [D

    iget-object v0, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, v3, v1, v3, v2}, Lmaps/ay/s;->a(I[DII)V

    iget-object v0, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0}, Lmaps/ay/z;->c()I

    move-result v3

    sub-int/2addr v3, p1

    invoke-virtual {v0, p1, v1, v2, v3}, Lmaps/ay/s;->a(I[DII)V

    new-instance v0, Lmaps/ay/z;

    invoke-direct {p0, p1, p2}, Lmaps/ay/z;->f(II)[I

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/ay/z;-><init>([D[I)V

    :goto_0
    return-object v0

    :cond_1
    sub-int v0, p2, p1

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v1, v0, 0x2

    new-array v1, v1, [D

    iget-object v2, p0, Lmaps/ay/z;->a:Lmaps/ay/s;

    invoke-virtual {v2, p1, v1, v3, v0}, Lmaps/ay/s;->a(I[DII)V

    new-instance v0, Lmaps/ay/z;

    invoke-direct {p0, p1, p2}, Lmaps/ay/z;->f(II)[I

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/ay/z;-><init>([D[I)V

    goto :goto_0
.end method

.method public m(I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SortedVertexMapping is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
