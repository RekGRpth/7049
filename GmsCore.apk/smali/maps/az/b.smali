.class public final Lmaps/az/b;
.super Ljava/lang/Object;


# static fields
.field private static j:Lmaps/az/b;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:J

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lmaps/az/b;->j:Lmaps/az/b;

    return-void
.end method

.method public constructor <init>(Lmaps/bb/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->a:I

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->b:I

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->d:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->e:I

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->f:I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->g:I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/b;->c:I

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmaps/bb/c;->e(I)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/az/b;->h:J

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/az/b;->i:Z

    return-void
.end method

.method public static h()Lmaps/bb/c;
    .locals 2

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->r:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    return-object v0
.end method


# virtual methods
.method public e()J
    .locals 4

    iget v0, p0, Lmaps/az/b;->g:I

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "maxTiles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " maxServerTiles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchPeriod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchInitiatorDelay: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prefetchInitiatorPeriod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timeToWipe: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/b;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
