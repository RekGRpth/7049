.class public final Lmaps/az/f;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:Z

.field private volatile b:Z

.field private volatile c:Z

.field private volatile d:Z

.field private volatile e:Z

.field private volatile f:Z

.field private volatile g:Z

.field private volatile h:Z

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Z

.field private volatile o:Z

.field private volatile p:Z

.field private volatile q:Z

.field private volatile r:Z

.field private volatile s:Z

.field private volatile t:Z

.field private volatile u:Z

.field private volatile v:Z

.field private volatile w:Z

.field private volatile x:Z


# direct methods
.method public constructor <init>(Lmaps/bb/c;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/az/f;->a:Z

    iput-boolean v0, p0, Lmaps/az/f;->b:Z

    iput-boolean v0, p0, Lmaps/az/f;->c:Z

    iput-boolean v0, p0, Lmaps/az/f;->d:Z

    iput-boolean v0, p0, Lmaps/az/f;->e:Z

    iput-boolean v0, p0, Lmaps/az/f;->f:Z

    iput-boolean v0, p0, Lmaps/az/f;->g:Z

    iput-boolean v0, p0, Lmaps/az/f;->h:Z

    iput-boolean v0, p0, Lmaps/az/f;->i:Z

    iput-boolean v0, p0, Lmaps/az/f;->j:Z

    iput-boolean v0, p0, Lmaps/az/f;->k:Z

    iput-boolean v0, p0, Lmaps/az/f;->l:Z

    iput-boolean v0, p0, Lmaps/az/f;->m:Z

    iput-boolean v0, p0, Lmaps/az/f;->n:Z

    iput-boolean v0, p0, Lmaps/az/f;->o:Z

    iput-boolean v0, p0, Lmaps/az/f;->p:Z

    iput-boolean v0, p0, Lmaps/az/f;->q:Z

    iput-boolean v0, p0, Lmaps/az/f;->r:Z

    iput-boolean v0, p0, Lmaps/az/f;->s:Z

    iput-boolean v0, p0, Lmaps/az/f;->t:Z

    iput-boolean v0, p0, Lmaps/az/f;->u:Z

    iput-boolean v0, p0, Lmaps/az/f;->v:Z

    iput-boolean v0, p0, Lmaps/az/f;->w:Z

    iput-boolean v0, p0, Lmaps/az/f;->x:Z

    invoke-direct {p0, p1}, Lmaps/az/f;->b(Lmaps/bb/c;)V

    return-void
.end method

.method private b(Lmaps/bb/c;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lmaps/az/f;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmaps/az/f;->a:Z

    iget-boolean v0, p0, Lmaps/az/f;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lmaps/az/f;->b:Z

    iget-boolean v0, p0, Lmaps/az/f;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lmaps/az/f;->c:Z

    iget-boolean v0, p0, Lmaps/az/f;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lmaps/az/f;->d:Z

    iget-boolean v0, p0, Lmaps/az/f;->e:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lmaps/az/f;->e:Z

    iget-boolean v0, p0, Lmaps/az/f;->f:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lmaps/az/f;->f:Z

    iget-boolean v0, p0, Lmaps/az/f;->g:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lmaps/az/f;->g:Z

    iget-boolean v0, p0, Lmaps/az/f;->h:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lmaps/az/f;->h:Z

    iget-boolean v0, p0, Lmaps/az/f;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lmaps/az/f;->i:Z

    iget-boolean v0, p0, Lmaps/az/f;->j:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lmaps/az/f;->j:Z

    iget-boolean v0, p0, Lmaps/az/f;->k:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lmaps/az/f;->k:Z

    iget-boolean v0, p0, Lmaps/az/f;->l:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lmaps/az/f;->l:Z

    iget-boolean v0, p0, Lmaps/az/f;->m:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lmaps/az/f;->m:Z

    iget-boolean v0, p0, Lmaps/az/f;->n:Z

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lmaps/az/f;->n:Z

    iget-boolean v0, p0, Lmaps/az/f;->o:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lmaps/az/f;->o:Z

    iget-boolean v0, p0, Lmaps/az/f;->p:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lmaps/az/f;->p:Z

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/az/f;->r:Z

    iget-boolean v0, p0, Lmaps/az/f;->q:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lmaps/az/f;->q:Z

    iget-boolean v0, p0, Lmaps/az/f;->s:Z

    if-eqz v0, :cond_11

    const/16 v0, 0x13

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lmaps/az/f;->s:Z

    iget-boolean v0, p0, Lmaps/az/f;->u:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lmaps/az/f;->u:Z

    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/az/f;->v:Z

    iget-boolean v0, p0, Lmaps/az/f;->w:Z

    if-eqz v0, :cond_13

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lmaps/az/f;->w:Z

    iget-boolean v0, p0, Lmaps/az/f;->x:Z

    if-eqz v0, :cond_14

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_14

    :goto_14
    iput-boolean v1, p0, Lmaps/az/f;->x:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto/16 :goto_8

    :cond_9
    move v0, v2

    goto/16 :goto_9

    :cond_a
    move v0, v2

    goto/16 :goto_a

    :cond_b
    move v0, v2

    goto/16 :goto_b

    :cond_c
    move v0, v2

    goto/16 :goto_c

    :cond_d
    move v0, v2

    goto/16 :goto_d

    :cond_e
    move v0, v2

    goto/16 :goto_e

    :cond_f
    move v0, v2

    goto/16 :goto_f

    :cond_10
    move v0, v2

    goto :goto_10

    :cond_11
    move v0, v2

    goto :goto_11

    :cond_12
    move v0, v2

    goto :goto_12

    :cond_13
    move v0, v2

    goto :goto_13

    :cond_14
    move v1, v2

    goto :goto_14
.end method

.method public static d()Lmaps/bb/c;
    .locals 2

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->e:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    return-object v0
.end method


# virtual methods
.method public a(Lmaps/bb/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/az/f;->b(Lmaps/bb/c;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/az/f;->s:Z

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/az/f;->t:Z

    return v0
.end method
