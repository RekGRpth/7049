.class public final Lmaps/az/j;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:J

.field private final e:J

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lmaps/bb/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/j;->a:I

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/j;->b:I

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/az/j;->c:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bb/c;->e(I)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/az/j;->d:J

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bb/c;->e(I)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/az/j;->e:J

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/az/j;->f:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/az/j;->g:Ljava/lang/String;

    return-void
.end method

.method public static a()Lmaps/bb/c;
    .locals 2

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->u:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mapMoveDelayInMs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/j;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " refreshPeriodInMs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minZoomLevel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/az/j;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " distanceThresholdInMeters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmaps/az/j;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " useSavedSearchDistanceThresholdInmeters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmaps/az/j;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mobileOffersHubBaseUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/az/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " offersHubLogUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/az/j;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
