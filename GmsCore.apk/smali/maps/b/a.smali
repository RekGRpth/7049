.class public Lmaps/b/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/u;
.implements Lmaps/by/a;


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Lmaps/b/u;

.field private d:Lmaps/b/g;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/b/a;->b:Ljava/lang/String;

    new-instance v0, Lmaps/b/e;

    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/b/e;-><init>(Lmaps/b/a;Lmaps/ai/d;)V

    invoke-virtual {v0}, Lmaps/b/e;->d()V

    return-void
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/b/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lmaps/by/b;->a(Ljava/lang/String;Lmaps/by/a;)Lmaps/by/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/b/a;->a(Lmaps/by/c;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/b/a;)V
    .locals 0

    invoke-direct {p0}, Lmaps/b/a;->a()V

    return-void
.end method

.method private static b(Lmaps/by/c;)Lmaps/b/u;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/by/c;->d()[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-static {v0}, Lmaps/b/l;->a(Ljava/io/Reader;)Lmaps/b/l;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lmaps/b/f;

    invoke-direct {v0}, Lmaps/b/f;-><init>()V

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "ResourceBasedBuildingBoundProvider"

    invoke-static {v2, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/t/ah;)Ljava/util/Collection;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/ah;)Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lmaps/b/a;->a:Ljava/util/Collection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/b/g;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/b/a;->d:Lmaps/b/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/by/c;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {p1}, Lmaps/by/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lmaps/b/a;->b(Lmaps/by/c;)Lmaps/b/u;

    move-result-object v0

    monitor-enter p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iput-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    iget-object v0, p0, Lmaps/b/a;->d:Lmaps/b/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/b/a;->d:Lmaps/b/g;

    invoke-interface {v0}, Lmaps/b/g;->j()V

    :cond_2
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_3
    :try_start_6
    monitor-exit p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method public declared-synchronized a(Lmaps/t/v;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/b/a;->c:Lmaps/b/u;

    invoke-interface {v0, p1}, Lmaps/b/u;->a(Lmaps/t/v;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/b/g;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lmaps/b/a;->d:Lmaps/b/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
