.class public Lmaps/b/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/u;
.implements Lmaps/by/a;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/t/bx;

.field private final d:Lmaps/t/bx;

.field private volatile e:Z

.field private volatile f:Lmaps/t/bx;

.field private final g:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile h:Lmaps/b/t;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    const v1, 0x4c4b40

    const v2, 0x3d0900

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, v1, v1}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1, v2, v2}, Lmaps/t/bx;-><init>(II)V

    invoke-direct {p0, p1, v0, v1}, Lmaps/b/aa;-><init>(Ljava/lang/String;Lmaps/t/bx;Lmaps/t/bx;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lmaps/t/bx;Lmaps/t/bx;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/b/aa;->b:Ljava/lang/String;

    iput-object p2, p0, Lmaps/b/aa;->c:Lmaps/t/bx;

    invoke-virtual {p2}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {p3}, Lmaps/t/bx;->f()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p2}, Lmaps/t/bx;->g()I

    move-result v0

    invoke-virtual {p3}, Lmaps/t/bx;->g()I

    move-result v1

    if-lt v0, v1, :cond_0

    iput-object p3, p0, Lmaps/b/aa;->d:Lmaps/t/bx;

    :goto_0
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmaps/b/aa;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/b/aa;->f:Lmaps/t/bx;

    new-instance v0, Lmaps/b/t;

    invoke-direct {v0}, Lmaps/b/t;-><init>()V

    iput-object v0, p0, Lmaps/b/aa;->h:Lmaps/b/t;

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/b/aa;->c:Lmaps/t/bx;

    iput-object v0, p0, Lmaps/b/aa;->d:Lmaps/t/bx;

    goto :goto_0
.end method

.method static a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/bw;
    .locals 5

    const/16 v2, 0xf

    invoke-virtual {p0, p1}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/t/ah;->b(ILmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->h()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p0, p1}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/t/ah;->b(ILmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->i()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v2

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v3

    if-le v2, v3, :cond_0

    new-instance v2, Lmaps/t/bx;

    const/high16 v3, 0x40000000

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v0, v2}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    :cond_0
    new-instance v2, Lmaps/t/ax;

    invoke-direct {v2, v1, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v2}, Lmaps/t/bw;->a(Lmaps/t/ax;)Lmaps/t/bw;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/b/aa;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lmaps/by/b;->a(Ljava/lang/String;Lmaps/by/a;)Lmaps/by/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/b/aa;->a(Lmaps/by/c;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lmaps/b/aa;)V
    .locals 0

    invoke-direct {p0}, Lmaps/b/aa;->a()V

    return-void
.end method

.method private a(Lmaps/t/bx;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/b/aa;->e:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/b/aa;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object p1, p0, Lmaps/b/aa;->f:Lmaps/t/bx;

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "LazyBuildingBoundProvider"

    const-string v1, "fetch thread is scheduled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lmaps/b/d;

    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmaps/b/d;-><init>(Lmaps/b/aa;Lmaps/ai/d;)V

    invoke-virtual {v0}, Lmaps/b/d;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(Lmaps/by/c;)V
    .locals 7

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "LazyBuildingBoundProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start fetchBuildingBoundMap : center = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/b/aa;->f:Lmaps/t/bx;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lmaps/by/c;->d()[B

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v1, v0

    if-lez v1, :cond_3

    :try_start_0
    iget-object v1, p0, Lmaps/b/aa;->f:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/b/aa;->c:Lmaps/t/bx;

    invoke-static {v1, v2}, Lmaps/b/aa;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/bw;

    move-result-object v1

    iget-object v2, p0, Lmaps/b/aa;->f:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/b/aa;->d:Lmaps/t/bx;

    invoke-static {v2, v3}, Lmaps/b/aa;->a(Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/bw;

    move-result-object v2

    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_1

    const-string v3, "LazyBuildingBoundProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Active Area: ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->b()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->d()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->b()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->d()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_2

    const-string v3, "LazyBuildingBoundProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No Refetch Area: ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->b()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->d()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->b()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/t/bx;->d()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lmaps/b/l;->a(Ljava/io/Reader;Lmaps/t/bw;)Lmaps/b/l;

    move-result-object v0

    new-instance v3, Lmaps/b/t;

    invoke-direct {v3, v0, v1, v2}, Lmaps/b/t;-><init>(Lmaps/b/l;Lmaps/t/bw;Lmaps/t/bw;)V

    iput-object v3, p0, Lmaps/b/aa;->h:Lmaps/b/t;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/b/aa;->e:Z

    return-void

    :catch_0
    move-exception v0

    const-string v1, "LazyBuildingBoundProvider"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Ljava/util/Collection;
    .locals 3

    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    iget-object v1, p0, Lmaps/b/aa;->h:Lmaps/b/t;

    iget-boolean v2, p0, Lmaps/b/aa;->e:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lmaps/b/t;->c:Lmaps/t/bw;

    invoke-virtual {v2, v0}, Lmaps/t/bw;->a(Lmaps/t/an;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v2

    invoke-direct {p0, v2}, Lmaps/b/aa;->a(Lmaps/t/bx;)V

    :cond_1
    iget-object v2, v1, Lmaps/b/t;->b:Lmaps/t/bw;

    invoke-virtual {v2, v0}, Lmaps/t/bw;->a(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lmaps/b/t;->a:Lmaps/b/l;

    invoke-virtual {v0, p1}, Lmaps/b/l;->a(Lmaps/t/ah;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lmaps/b/aa;->a:Ljava/util/Collection;

    goto :goto_0
.end method

.method public a(Lmaps/b/g;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/b/aa;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/by/c;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p1

    :try_start_0
    iget-boolean v0, p0, Lmaps/b/aa;->e:Z

    if-nez v0, :cond_1

    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lmaps/by/c;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/b/aa;->b(Lmaps/by/c;)V

    iget-object v0, p0, Lmaps/b/aa;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/g;

    invoke-interface {v0}, Lmaps/b/g;->j()V

    goto :goto_1

    :cond_2
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Lmaps/t/v;)Z
    .locals 1

    iget-object v0, p0, Lmaps/b/aa;->h:Lmaps/b/t;

    iget-object v0, v0, Lmaps/b/t;->a:Lmaps/b/l;

    invoke-virtual {v0, p1}, Lmaps/b/l;->a(Lmaps/t/v;)Z

    move-result v0

    return v0
.end method

.method public b(Lmaps/b/g;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/b/aa;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
