.class Lmaps/b/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/an/ag;


# instance fields
.field final synthetic a:Lmaps/b/m;


# direct methods
.method constructor <init>(Lmaps/b/m;)V
    .locals 0

    iput-object p1, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/an/y;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-virtual {v0}, Lmaps/b/m;->a()V

    return-void
.end method

.method public a(Lmaps/an/y;Lmaps/t/o;)V
    .locals 4

    iget-object v0, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-static {v0}, Lmaps/b/m;->a(Lmaps/b/m;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-static {v0}, Lmaps/b/m;->a(Lmaps/b/m;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {p2}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/i;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-static {v1}, Lmaps/b/m;->b(Lmaps/b/m;)Lmaps/be/i;

    move-result-object v2

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-static {v1}, Lmaps/b/m;->b(Lmaps/b/m;)Lmaps/be/i;

    move-result-object v1

    invoke-interface {p2}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/be/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_1

    const-string v0, "TileBasedBuildingBoundProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed data for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from the cache "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lmaps/b/b;->a:Lmaps/b/m;

    invoke-static {v0}, Lmaps/b/m;->c(Lmaps/b/m;)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
