.class public Lmaps/b/i;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/b;
.implements Lmaps/bx/d;


# instance fields
.field private final a:Lmaps/an/y;

.field private final b:Lmaps/an/i;

.field private final c:Lmaps/t/ah;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Collection;

.field private volatile f:Z

.field private volatile g:Lmaps/b/o;


# direct methods
.method public constructor <init>(Lmaps/an/y;Lmaps/an/i;Lmaps/t/ah;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/i;->d:Ljava/util/Map;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/i;->e:Ljava/util/Collection;

    iput-object p1, p0, Lmaps/b/i;->a:Lmaps/an/y;

    iput-object p2, p0, Lmaps/b/i;->b:Lmaps/an/i;

    iput-object p3, p0, Lmaps/b/i;->c:Lmaps/t/ah;

    return-void
.end method

.method private static a(Lmaps/t/ar;)Ljava/util/Collection;
    .locals 5

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/t/ar;->d()Lmaps/t/h;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Lmaps/t/h;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    invoke-interface {v0}, Lmaps/t/ca;->a()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    check-cast v0, Lmaps/t/co;

    invoke-virtual {v0}, Lmaps/t/co;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lmaps/t/co;->b()Lmaps/t/v;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v4, Lmaps/t/v;->a:Lmaps/t/v;

    if-eq v3, v4, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/b/i;->g:Lmaps/b/o;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lmaps/b/i;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/b/i;->g:Lmaps/b/o;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lmaps/b/o;->a(Lmaps/b/i;Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/b/i;->g:Lmaps/b/o;

    iget-object v1, p0, Lmaps/b/i;->e:Ljava/util/Collection;

    invoke-interface {v0, p0, v1}, Lmaps/b/o;->a(Lmaps/b/i;Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/b/i;->c:Lmaps/t/ah;

    return-object v0
.end method

.method public a(Lmaps/b/o;)V
    .locals 2

    iput-object p1, p0, Lmaps/b/i;->g:Lmaps/b/o;

    iget-object v0, p0, Lmaps/b/i;->a:Lmaps/an/y;

    iget-object v1, p0, Lmaps/b/i;->c:Lmaps/t/ah;

    invoke-interface {v0, v1, p0}, Lmaps/an/y;->a(Lmaps/t/ah;Lmaps/bx/b;)V

    return-void
.end method

.method public a(Lmaps/t/ah;ILmaps/t/o;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_5

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_2

    const-string v0, "BuildingBoundFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tile not found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    const/4 v0, 0x0

    if-eqz p3, :cond_3

    check-cast p3, Lmaps/t/ar;

    invoke-static {p3}, Lmaps/b/i;->a(Lmaps/t/ar;)Ljava/util/Collection;

    move-result-object v0

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_3

    const-string v1, "BuildingBoundFetcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tile "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " indoor areas "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_7

    :cond_4
    invoke-direct {p0}, Lmaps/b/i;->b()V

    goto :goto_0

    :cond_5
    if-ne p2, v3, :cond_2

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_6

    const-string v0, "BuildingBoundFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IO error for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iput-boolean v3, p0, Lmaps/b/i;->f:Z

    goto :goto_1

    :cond_7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/co;

    new-instance v2, Lmaps/b/v;

    invoke-direct {v2, v0}, Lmaps/b/v;-><init>(Lmaps/t/co;)V

    iget-object v0, p0, Lmaps/b/i;->d:Ljava/util/Map;

    invoke-static {v2}, Lmaps/b/v;->a(Lmaps/b/v;)Lmaps/t/bg;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lmaps/b/i;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/v;

    iget-object v2, p0, Lmaps/b/i;->b:Lmaps/an/i;

    invoke-static {v0}, Lmaps/b/v;->a(Lmaps/b/v;)Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Lmaps/an/i;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_3
.end method

.method public a(Lmaps/t/bg;ILmaps/t/e;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lmaps/b/i;->d:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/i;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/v;

    iget-object v2, p0, Lmaps/b/i;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    monitor-exit v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lmaps/t/e;->e()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/b/v;->a(Lmaps/t/bx;)V

    iget-object v1, p0, Lmaps/b/i;->e:Ljava/util/Collection;

    invoke-virtual {v0}, Lmaps/b/v;->a()Lmaps/b/x;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    if-ne p2, v3, :cond_3

    iput-boolean v3, p0, Lmaps/b/i;->f:Z

    :cond_3
    if-eqz v2, :cond_0

    invoke-direct {p0}, Lmaps/b/i;->b()V

    goto :goto_0
.end method
