.class public Lmaps/b/r;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/d;


# static fields
.field private static b:Lmaps/b/r;

.field private static final p:Lmaps/t/bb;


# instance fields
.field private final a:Ljava/util/Map;

.field private final c:Lmaps/be/i;

.field private final d:Lmaps/be/i;

.field private e:Lmaps/t/bg;

.field private f:Lmaps/t/bb;

.field private g:Lmaps/t/e;

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/List;

.field private final j:Ljava/util/Set;

.field private final k:Ljava/lang/Object;

.field private final l:Lmaps/an/i;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Lmaps/b/u;

.field private volatile q:Lmaps/t/bb;

.field private volatile r:Lmaps/t/bb;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Lmaps/t/bb;

    new-instance v1, Lmaps/t/bg;

    invoke-direct {v1, v2, v3, v2, v3}, Lmaps/t/bg;-><init>(JJ)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/t/bb;-><init>(Lmaps/t/bg;I)V

    sput-object v0, Lmaps/b/r;->p:Lmaps/t/bb;

    return-void
.end method

.method constructor <init>(Lmaps/an/i;)V
    .locals 2

    const/16 v1, 0x64

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->j:Ljava/util/Set;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/r;->n:Ljava/util/Map;

    new-instance v0, Lmaps/be/i;

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/b/r;->c:Lmaps/be/i;

    new-instance v0, Lmaps/be/i;

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/b/r;->d:Lmaps/be/i;

    iput-object p1, p0, Lmaps/b/r;->l:Lmaps/an/i;

    new-instance v0, Lmaps/b/n;

    invoke-direct {v0}, Lmaps/b/n;-><init>()V

    iput-object v0, p0, Lmaps/b/r;->o:Lmaps/b/u;

    return-void
.end method

.method public static a()Lmaps/b/r;
    .locals 1

    sget-object v0, Lmaps/b/r;->b:Lmaps/b/r;

    return-object v0
.end method

.method public static a(Lmaps/an/i;)Lmaps/b/r;
    .locals 1

    sget-object v0, Lmaps/b/r;->b:Lmaps/b/r;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/b/r;

    invoke-direct {v0, p0}, Lmaps/b/r;-><init>(Lmaps/an/i;)V

    sput-object v0, Lmaps/b/r;->b:Lmaps/b/r;

    :cond_0
    sget-object v0, Lmaps/b/r;->b:Lmaps/b/r;

    return-object v0
.end method

.method static synthetic a(Lmaps/b/r;Lmaps/t/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/b/r;->d(Lmaps/t/e;)V

    return-void
.end method

.method private a(Lmaps/t/bg;Lmaps/bx/d;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {v0, p1}, Lmaps/an/i;->b(Lmaps/t/bg;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {v0, p1, p2}, Lmaps/an/i;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_0
.end method

.method private a(Lmaps/t/bg;Lmaps/t/bb;Lmaps/t/bb;)V
    .locals 6

    iget-object v2, p0, Lmaps/b/r;->c:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->d:Lmaps/be/i;

    invoke-virtual {v0, p1, p3}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {p3}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/an/i;->c(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v3

    if-nez v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/b/r;->p:Lmaps/t/bb;

    if-ne p2, v0, :cond_2

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v3}, Lmaps/t/bi;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bg;

    invoke-virtual {v0, p1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lmaps/b/r;->d:Lmaps/be/i;

    iget-object v5, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {v5, v0}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v4, p0, Lmaps/b/r;->c:Lmaps/be/i;

    sget-object v5, Lmaps/b/r;->p:Lmaps/t/bb;

    invoke-virtual {v4, v0, v5}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {p2}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/an/i;->c(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    if-nez v0, :cond_3

    monitor-exit v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lmaps/t/bi;->c()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private a(Lmaps/t/e;Lmaps/t/bb;)V
    .locals 4

    invoke-virtual {p1, p2}, Lmaps/t/e;->a(Lmaps/t/bb;)Lmaps/t/bi;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lmaps/t/bi;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bg;

    invoke-virtual {v1}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/t/bb;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lmaps/b/w;

    invoke-direct {v3, p0}, Lmaps/b/w;-><init>(Lmaps/b/r;)V

    invoke-direct {p0, v0, v3}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_0

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_2

    const-string v0, "IndoorState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to look up level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in building "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private c(Lmaps/t/e;)Lmaps/t/bb;
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/b/r;->c:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bb;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lmaps/t/e;->c()Lmaps/t/bi;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lmaps/b/r;->p:Lmaps/t/bb;

    :goto_0
    iget-object v3, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v3, Lmaps/b/r;->p:Lmaps/t/bb;

    if-eq v0, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lmaps/b/r;->d(Lmaps/t/e;)V

    :cond_1
    return-object v0

    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private d(Lmaps/t/e;)V
    .locals 3

    invoke-direct {p0}, Lmaps/b/r;->m()V

    iget-object v1, p0, Lmaps/b/r;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/y;

    invoke-interface {v0, p0, p1}, Lmaps/b/y;->a(Lmaps/b/r;Lmaps/t/e;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private k()V
    .locals 3

    iget-object v1, p0, Lmaps/b/r;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/y;

    invoke-interface {v0, p0}, Lmaps/b/y;->a(Lmaps/b/r;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private l()V
    .locals 3

    invoke-direct {p0}, Lmaps/b/r;->m()V

    iget-object v1, p0, Lmaps/b/r;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/y;

    invoke-interface {v0, p0}, Lmaps/b/y;->b(Lmaps/b/r;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private m()V
    .locals 6

    iget-object v2, p0, Lmaps/b/r;->n:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p0}, Lmaps/b/r;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmaps/b/r;->b(Lmaps/t/bb;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/b/r;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/b/z;

    if-nez v1, :cond_1

    new-instance v1, Lmaps/b/z;

    invoke-direct {v1, v0}, Lmaps/b/z;-><init>(Lmaps/t/bi;)V

    iget-object v4, p0, Lmaps/b/r;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v4, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {v1, v0}, Lmaps/b/z;->a(Lmaps/t/bi;)Z

    goto :goto_1

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bg;ZZZ)Lmaps/b/z;
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lmaps/b/r;->n:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/z;

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit v3

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-eqz p3, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lmaps/b/z;->g()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_5

    move-object v2, v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_1

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {v0, p1}, Lmaps/an/i;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    if-nez v0, :cond_3

    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v1, Lmaps/b/z;

    invoke-direct {v1, v0}, Lmaps/b/z;-><init>(Lmaps/t/bi;)V

    if-eqz p4, :cond_4

    iget-object v0, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Lmaps/b/z;->a(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/b/z;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bg;

    iget-object v5, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v2, v1

    goto :goto_1
.end method

.method public a(Lmaps/t/bg;)Lmaps/t/bb;
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->c:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bb;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {v1, p1}, Lmaps/an/i;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v1}, Lmaps/b/r;->c(Lmaps/t/e;)Lmaps/t/bb;

    move-result-object v0

    :cond_0
    :goto_0
    sget-object v1, Lmaps/b/r;->p:Lmaps/t/bb;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-direct {p0, p1, p0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)V
    .locals 5

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    :try_start_0
    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lmaps/b/r;->j:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/b/r;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/b/r;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/b/r;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bg;

    iget-object v3, p0, Lmaps/b/r;->l:Lmaps/an/i;

    invoke-virtual {v3, v0}, Lmaps/an/i;->a(Lmaps/t/bg;)Lmaps/t/e;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-direct {p0, v0, p0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_1

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lmaps/b/r;->l()V

    goto :goto_0
.end method

.method public a(Lmaps/b/y;)V
    .locals 2

    iget-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lmaps/t/bb;)V
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    invoke-virtual {p1, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-direct {p0, v0, p0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/t/bb;Lmaps/t/bb;)V
    .locals 0

    iput-object p1, p0, Lmaps/b/r;->q:Lmaps/t/bb;

    iput-object p2, p0, Lmaps/b/r;->r:Lmaps/t/bb;

    invoke-direct {p0}, Lmaps/b/r;->m()V

    return-void
.end method

.method public a(Lmaps/t/bg;ILmaps/t/e;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Building id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v3, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    invoke-virtual {v4}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-virtual {v4, p1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v2, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    const/4 v4, 0x0

    iput-object v4, p0, Lmaps/b/r;->f:Lmaps/t/bb;

    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    invoke-direct {p0, p3, v2}, Lmaps/b/r;->a(Lmaps/t/e;Lmaps/t/bb;)V

    :cond_3
    invoke-direct {p0, p3}, Lmaps/b/r;->c(Lmaps/t/e;)Lmaps/t/bb;

    iget-object v3, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lmaps/b/r;->e:Lmaps/t/bg;

    invoke-virtual {p1, v2}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmaps/b/r;->g:Lmaps/t/e;

    if-eqz v2, :cond_4

    invoke-virtual {p3}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v2

    iget-object v4, p0, Lmaps/b/r;->g:Lmaps/t/e;

    invoke-virtual {v4}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v4

    invoke-virtual {v2, v4}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_4
    invoke-virtual {p3}, Lmaps/t/e;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lmaps/b/r;->g:Lmaps/t/e;

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    iput-object v2, p0, Lmaps/b/r;->g:Lmaps/t/e;

    move v2, v0

    :goto_1
    const/4 v4, 0x0

    iput-object v4, p0, Lmaps/b/r;->e:Lmaps/t/bg;

    :goto_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lmaps/b/r;->k()V

    :cond_5
    iget-object v2, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v3, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v1, p0, Lmaps/b/r;->h:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/b/r;->l()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_6
    :try_start_4
    iput-object p3, p0, Lmaps/b/r;->g:Lmaps/t/e;

    move v2, v0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_1

    :cond_9
    move v2, v1

    goto :goto_2
.end method

.method public a(Lmaps/t/e;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    sget-object v1, Lmaps/b/r;->p:Lmaps/t/bb;

    invoke-virtual {p0, v0, v1}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/t/bb;)Z

    invoke-direct {p0, p1}, Lmaps/b/r;->d(Lmaps/t/e;)V

    :cond_0
    return-void
.end method

.method a(Lmaps/t/bg;Lmaps/t/bb;)Z
    .locals 3

    iget-object v1, p0, Lmaps/b/r;->c:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bb;

    invoke-virtual {p2, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lmaps/b/r;->c:Lmaps/be/i;

    invoke-virtual {v2, p1, p2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, v0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/t/bb;Lmaps/t/bb;)V

    :cond_1
    monitor-exit v1

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lmaps/t/e;)Lmaps/t/bi;
    .locals 1

    invoke-virtual {p1}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/b/r;->a(Lmaps/t/bg;)Lmaps/t/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lmaps/t/e;->a(Lmaps/t/bb;)Lmaps/t/bi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/t/bg;)Lmaps/t/cu;
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/b/r;->a(Lmaps/t/bg;)Lmaps/t/bb;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lmaps/t/be;

    invoke-direct {v1}, Lmaps/t/be;-><init>()V

    invoke-virtual {v1, v0}, Lmaps/t/be;->a(Lmaps/t/bb;)Lmaps/t/be;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/be;->a()Lmaps/t/cu;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/b/r;->q:Lmaps/t/bb;

    iput-object v0, p0, Lmaps/b/r;->r:Lmaps/t/bb;

    invoke-direct {p0}, Lmaps/b/r;->m()V

    return-void
.end method

.method public b(Lmaps/b/y;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(Lmaps/t/bb;)Z
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/b/r;->q:Lmaps/t/bb;

    invoke-virtual {p1, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/r;->r:Lmaps/t/bb;

    invoke-virtual {p1, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/t/bb;Lmaps/t/bb;)Z
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/b/r;->q:Lmaps/t/bb;

    invoke-virtual {p1, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/b/r;->r:Lmaps/t/bb;

    invoke-virtual {p2, v0}, Lmaps/t/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lmaps/t/bg;)Lmaps/t/bb;
    .locals 3

    iget-object v1, p0, Lmaps/b/r;->c:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->d:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bb;

    sget-object v2, Lmaps/b/r;->p:Lmaps/t/bb;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Lmaps/t/e;
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->g:Lmaps/t/e;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(Lmaps/t/bg;)V
    .locals 3

    if-nez p1, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/b/r;->g:Lmaps/t/e;

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/b/r;->e:Lmaps/t/bg;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/b/r;->g:Lmaps/t/e;

    const/4 v0, 0x1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/b/r;->k()V

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lmaps/b/r;->e:Lmaps/t/bg;

    invoke-virtual {p1, v0}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/b/r;->g:Lmaps/t/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/b/r;->g:Lmaps/t/e;

    invoke-virtual {v0}, Lmaps/t/e;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    :try_start_3
    iput-object p1, p0, Lmaps/b/r;->e:Lmaps/t/bg;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-direct {p0, p1, p0}, Lmaps/b/r;->a(Lmaps/t/bg;Lmaps/bx/d;)V

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()Ljava/util/List;
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->i:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/ef;->a(Ljava/util/Collection;)Lmaps/f/ef;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e(Lmaps/t/bg;)Lmaps/b/z;
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1, v1}, Lmaps/b/r;->a(Lmaps/t/bg;ZZZ)Lmaps/b/z;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Lmaps/b/r;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/e;

    invoke-virtual {p0, v0}, Lmaps/b/r;->b(Lmaps/t/e;)Lmaps/t/bi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public g()Ljava/util/Set;
    .locals 3

    invoke-virtual {p0}, Lmaps/b/r;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/e;

    invoke-virtual {p0, v0}, Lmaps/b/r;->b(Lmaps/t/e;)Lmaps/t/bi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/t/bi;->a()Lmaps/t/bb;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public h()Z
    .locals 2

    invoke-virtual {p0}, Lmaps/b/r;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/util/Set;
    .locals 2

    iget-object v1, p0, Lmaps/b/r;->n:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/b/r;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()Lmaps/b/u;
    .locals 1

    iget-object v0, p0, Lmaps/b/r;->o:Lmaps/b/u;

    return-object v0
.end method
