.class public Lmaps/b/s;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/b;


# static fields
.field public static final a:Lmaps/b/k;


# instance fields
.field private final b:Lmaps/an/y;

.field private final c:Lmaps/an/ag;

.field private final d:Lmaps/be/i;

.field private e:I

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmaps/b/k;

    invoke-static {}, Lmaps/f/ef;->f()Lmaps/f/ef;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/b/k;-><init>(Ljava/util/List;)V

    sput-object v0, Lmaps/b/s;->a:Lmaps/b/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/s;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lmaps/b/s;->g:Ljava/util/Set;

    sget-object v0, Lmaps/o/c;->n:Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/o/c;->n:Lmaps/o/c;

    invoke-static {v0}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/s;->b:Lmaps/an/y;

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/b/s;->d:Lmaps/be/i;

    new-instance v0, Lmaps/b/q;

    invoke-direct {v0, p0}, Lmaps/b/q;-><init>(Lmaps/b/s;)V

    iput-object v0, p0, Lmaps/b/s;->c:Lmaps/an/ag;

    iget-object v0, p0, Lmaps/b/s;->b:Lmaps/an/y;

    iget-object v1, p0, Lmaps/b/s;->c:Lmaps/an/ag;

    invoke-interface {v0, v1}, Lmaps/an/y;->a(Lmaps/an/ag;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v1, p0, Lmaps/b/s;->b:Lmaps/an/y;

    iput-object v1, p0, Lmaps/b/s;->d:Lmaps/be/i;

    iput-object v1, p0, Lmaps/b/s;->c:Lmaps/an/ag;

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorOutlineFetcher"

    const-string v1, "No indoor tile store registered, doing nothing."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static a(Lmaps/t/ar;)Lmaps/b/k;
    .locals 4

    new-instance v1, Lmaps/b/j;

    invoke-direct {v1}, Lmaps/b/j;-><init>()V

    invoke-virtual {p0}, Lmaps/t/ar;->d()Lmaps/t/h;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Lmaps/t/h;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    instance-of v3, v0, Lmaps/t/co;

    if-eqz v3, :cond_0

    check-cast v0, Lmaps/t/co;

    invoke-virtual {v0}, Lmaps/t/co;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Lmaps/b/j;->a(Lmaps/t/co;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lmaps/b/j;->a()Lmaps/b/k;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/t/ah;Lmaps/b/k;)V
    .locals 2

    iget-object v0, p0, Lmaps/b/s;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/c;

    invoke-interface {v0, p1, p2}, Lmaps/b/c;->a(Lmaps/t/ah;Lmaps/b/k;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private declared-synchronized b(Lmaps/t/ah;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/s;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/b/s;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/b/s;->b:Lmaps/an/y;

    invoke-interface {v0, p1, p0}, Lmaps/an/y;->a(Lmaps/t/ah;Lmaps/bx/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/t/ah;)Lmaps/b/k;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/s;->d:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/b/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lmaps/b/s;->b(Lmaps/t/ah;)V

    iget v0, p0, Lmaps/b/s;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/b/s;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/s;->d:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    iget-object v0, p0, Lmaps/b/s;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/b/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/s;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmaps/t/ah;ILmaps/t/o;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-boolean v3, Lmaps/ae/h;->f:Z

    if-eqz v3, :cond_0

    const-string v3, "IndoorOutlineFetcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleTile "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v1, v0

    :goto_0
    :pswitch_1
    if-eqz v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/b/s;->d:Lmaps/be/i;

    invoke-virtual {v0, p1, v2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-direct {p0, p1, v2}, Lmaps/b/s;->a(Lmaps/t/ah;Lmaps/b/k;)V

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmaps/b/s;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    return-void

    :pswitch_2
    instance-of v0, p3, Lmaps/t/ar;

    if-eqz v0, :cond_3

    check-cast p3, Lmaps/t/ar;

    invoke-static {p3}, Lmaps/b/s;->a(Lmaps/t/ar;)Lmaps/b/k;

    move-result-object v0

    :goto_1
    move-object v2, v0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v0, Lmaps/b/s;->a:Lmaps/b/k;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lmaps/b/s;->a:Lmaps/b/k;

    move-object v2, v0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lmaps/b/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/b/s;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
