.class Lmaps/b/w;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/d;


# instance fields
.field final synthetic a:Lmaps/b/r;


# direct methods
.method constructor <init>(Lmaps/b/r;)V
    .locals 0

    iput-object p1, p0, Lmaps/b/w;->a:Lmaps/b/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bg;ILmaps/t/e;)V
    .locals 3

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Building id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lmaps/b/w;->a:Lmaps/b/r;

    invoke-static {v0, p3}, Lmaps/b/r;->a(Lmaps/b/r;Lmaps/t/e;)V

    goto :goto_0
.end method
