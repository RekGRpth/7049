.class public Lmaps/b/z;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/p/z;


# instance fields
.field private final a:Ljava/util/Map;

.field private b:Ljava/util/Set;

.field private final c:F

.field private d:J

.field private e:I

.field private f:F

.field private g:Z


# direct methods
.method private constructor <init>(F)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/b/z;->d:J

    iput v2, p0, Lmaps/b/z;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/b/z;->f:F

    iput-boolean v2, p0, Lmaps/b/z;->g:Z

    iput p1, p0, Lmaps/b/z;->c:F

    return-void
.end method

.method constructor <init>(Lmaps/t/bi;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/b/z;->d:J

    iput v2, p0, Lmaps/b/z;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/b/z;->f:F

    iput-boolean v2, p0, Lmaps/b/z;->g:Z

    invoke-virtual {p1}, Lmaps/t/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/b/z;->c:F

    invoke-virtual {p0, p1}, Lmaps/b/z;->a(Lmaps/t/bi;)Z

    return-void
.end method

.method private static a(F)F
    .locals 3

    mul-float v0, p0, p0

    const/high16 v1, 0x40400000

    const/high16 v2, 0x40000000

    mul-float/2addr v2, p0

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(FFF)F
    .locals 1

    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method private static a(FFFFF)F
    .locals 2

    cmpg-float v0, p0, p1

    if-gtz v0, :cond_0

    :goto_0
    return p3

    :cond_0
    cmpl-float v0, p0, p2

    if-ltz v0, :cond_1

    move p3, p4

    goto :goto_0

    :cond_1
    sub-float v0, p0, p1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    add-float/2addr p3, v0

    goto :goto_0
.end method

.method static a(FFIFF)F
    .locals 6

    const/high16 v5, 0x42c80000

    const/high16 v4, 0x41a00000

    const/high16 v3, 0x41900000

    const/4 v1, 0x0

    cmpl-float v0, p4, v1

    if-lez v0, :cond_1

    const/high16 v0, 0x40400000

    invoke-static {p1, v3, v4, v0, v1}, Lmaps/b/z;->a(FFFFF)F

    move-result v0

    :goto_0
    const/high16 v2, 0x41200000

    invoke-static {p0, v1, v2, v1, v0}, Lmaps/b/z;->a(FFFFF)F

    move-result v0

    invoke-static {p3}, Lmaps/b/z;->a(F)F

    move-result v1

    and-int/lit8 v2, p2, 0x2

    if-eqz v2, :cond_3

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    :cond_0
    :goto_1
    return v0

    :cond_1
    cmpg-float v0, p4, v1

    if-gez v0, :cond_2

    const/high16 v0, -0x3fc00000

    const/high16 v2, -0x40800000

    invoke-static {p1, v3, v4, v0, v2}, Lmaps/b/z;->a(FFFFF)F

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_0

    const/high16 v2, 0x3f800000

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    add-float/2addr v0, v1

    goto :goto_1
.end method

.method static a(IF)I
    .locals 4

    const/high16 v1, 0x3f800000

    invoke-static {p1}, Lmaps/b/z;->a(F)F

    move-result v0

    and-int/lit8 v2, p0, 0x4

    if-eqz v2, :cond_0

    :goto_0
    and-int/lit8 v2, p0, 0x10

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    const v3, 0x3f19999a

    invoke-static {v0, v2, v1, v3, v1}, Lmaps/b/z;->a(FFFFF)F

    move-result v0

    invoke-static {v1, v0, v0, v0}, Lmaps/s/k;->a(FFFF)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    and-int/lit8 v2, p0, 0x8

    if-eqz v2, :cond_1

    sub-float v0, v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0, v0, v0, v0}, Lmaps/s/k;->a(FFFF)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public a(Lmaps/bq/d;Lmaps/t/bx;)F
    .locals 5

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v1

    iget v2, p0, Lmaps/b/z;->e:I

    iget v3, p0, Lmaps/b/z;->f:F

    iget v4, p0, Lmaps/b/z;->c:F

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/b/z;->a(FFIFF)F

    move-result v0

    invoke-virtual {p2}, Lmaps/t/bx;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public a(Lmaps/p/z;)I
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/b/z;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lmaps/b/z;->b()F

    move-result v0

    check-cast p1, Lmaps/b/z;

    invoke-virtual {p1}, Lmaps/b/z;->b()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    :cond_0
    return v0
.end method

.method public a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/b/z;->b:Ljava/util/Set;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/z;->b:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, Lmaps/b/z;->b:Ljava/util/Set;

    return-object v0
.end method

.method a(Lmaps/t/bg;)Lmaps/b/z;
    .locals 4

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v1, Lmaps/b/z;

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->f()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v1, v0}, Lmaps/b/z;-><init>(F)V

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v0}, Lmaps/b/z;->a(Lmaps/t/bi;)Z

    goto :goto_1

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lmaps/b/z;->a(IJ)V

    return-void
.end method

.method a(IJ)V
    .locals 1

    iput p1, p0, Lmaps/b/z;->e:I

    iput-wide p2, p0, Lmaps/b/z;->d:J

    const/4 v0, 0x0

    iput v0, p0, Lmaps/b/z;->f:F

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/s;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-boolean v1, p0, Lmaps/b/z;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->A()V

    :cond_0
    const/4 v1, -0x1

    invoke-static {v0, v1}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;Lmaps/t/bx;)V
    .locals 6

    const/16 v5, 0x1e00

    const/4 v4, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p0, p2, p4}, Lmaps/b/z;->a(Lmaps/bq/d;Lmaps/t/bx;)F

    move-result v0

    invoke-virtual {p2}, Lmaps/bq/d;->x()F

    move-result v2

    mul-float/2addr v0, v2

    invoke-interface {v1, v4, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget v2, p0, Lmaps/b/z;->e:I

    iget v3, p0, Lmaps/b/z;->f:F

    invoke-static {v2, v3}, Lmaps/b/z;->a(IF)I

    move-result v2

    invoke-static {v1, v2}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/p/av;->b()Lmaps/p/j;

    move-result-object v2

    sget-object v3, Lmaps/p/j;->f:Lmaps/p/j;

    if-eq v2, v3, :cond_0

    sget-object v3, Lmaps/p/j;->g:Lmaps/p/j;

    if-eq v2, v3, :cond_0

    sget-object v3, Lmaps/p/j;->h:Lmaps/p/j;

    if-ne v2, v3, :cond_2

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/b/z;->g:Z

    iget-boolean v0, p0, Lmaps/b/z;->g:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/cr/c;->z()V

    invoke-interface {v1, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    const/16 v0, 0x202

    const/16 v2, 0xff

    const/16 v3, 0x80

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(J)Z
    .locals 3

    iget-wide v0, p0, Lmaps/b/z;->d:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43fa0000

    div-float/2addr v0, v1

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    invoke-static {v0, v1, v2}, Lmaps/b/z;->a(FFF)F

    move-result v0

    iput v0, p0, Lmaps/b/z;->f:F

    invoke-virtual {p0}, Lmaps/b/z;->c()Z

    move-result v0

    return v0
.end method

.method public a(Lmaps/t/bi;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/t/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lmaps/b/z;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    iget-object v0, p0, Lmaps/b/z;->b:Ljava/util/Set;

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/t/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lmaps/b/z;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    goto :goto_1
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/b/z;->c:F

    return v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lmaps/b/z;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/b/z;->f:F

    const/high16 v1, 0x3f800000

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/p/z;

    invoke-virtual {p0, p1}, Lmaps/b/z;->a(Lmaps/p/z;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/b/z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/b/z;->e:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lmaps/b/z;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/b/z;->f:F

    return-void
.end method

.method public f()Z
    .locals 2

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/b/z;->a(J)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/b/z;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Lmaps/b/z;->c:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "animationStartTimeMs"

    iget-wide v2, p0, Lmaps/b/z;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ap/n;->a(Ljava/lang/String;J)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "animationPosition"

    iget v2, p0, Lmaps/b/z;->f:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "animationType"

    iget v2, p0, Lmaps/b/z;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "featureIds"

    iget-object v2, p0, Lmaps/b/z;->b:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
