.class public Lmaps/ba/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ba/d;


# instance fields
.field private A:I

.field private B:I

.field private C:F

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:I

.field private I:I

.field private J:F

.field private K:Z

.field private L:I

.field private M:J

.field private N:J

.field public a:Lmaps/ba/a;

.field protected b:I

.field protected c:I

.field protected d:Lmaps/bw/a;

.field protected e:Lmaps/bw/a;

.field private f:[F

.field private g:[F

.field private h:J

.field private i:J

.field private j:Lmaps/ba/c;

.field private k:J

.field private l:J

.field private m:J

.field private n:F

.field private o:I

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:Lmaps/ba/f;

.field private v:J

.field private w:Lmaps/bw/a;

.field private x:[Lmaps/ba/f;

.field private y:I

.field private z:F


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/ba/b;->f:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmaps/ba/b;->g:[F

    iput-wide v4, p0, Lmaps/ba/b;->h:J

    iput-wide v4, p0, Lmaps/ba/b;->i:J

    const-wide/32 v0, 0x3b9aca00

    iput-wide v0, p0, Lmaps/ba/b;->k:J

    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lmaps/ba/b;->l:J

    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lmaps/ba/b;->m:J

    iput v3, p0, Lmaps/ba/b;->b:I

    iput v3, p0, Lmaps/ba/b;->c:I

    iput v2, p0, Lmaps/ba/b;->n:F

    iput v3, p0, Lmaps/ba/b;->o:I

    iput v2, p0, Lmaps/ba/b;->p:F

    iput v2, p0, Lmaps/ba/b;->q:F

    iput v2, p0, Lmaps/ba/b;->r:F

    iput v2, p0, Lmaps/ba/b;->s:F

    iput v2, p0, Lmaps/ba/b;->t:F

    iput-object v6, p0, Lmaps/ba/b;->u:Lmaps/ba/f;

    iput-object v6, p0, Lmaps/ba/b;->w:Lmaps/bw/a;

    const/16 v0, 0x12c

    iput v0, p0, Lmaps/ba/b;->y:I

    const v0, 0x3dcccccd

    iput v0, p0, Lmaps/ba/b;->z:F

    const/16 v0, 0x1770

    iput v0, p0, Lmaps/ba/b;->A:I

    const/16 v0, 0x3a98

    iput v0, p0, Lmaps/ba/b;->B:I

    const/high16 v0, 0x3f400000

    iput v0, p0, Lmaps/ba/b;->C:F

    const v0, 0x3ee66666

    iput v0, p0, Lmaps/ba/b;->D:F

    const v0, 0x3e99999a

    iput v0, p0, Lmaps/ba/b;->E:F

    const v0, 0x45bb8000

    iput v0, p0, Lmaps/ba/b;->F:F

    const v0, 0x466a6000

    iput v0, p0, Lmaps/ba/b;->G:F

    const v0, 0x7a120

    iput v0, p0, Lmaps/ba/b;->H:I

    const v0, 0x927c0

    iput v0, p0, Lmaps/ba/b;->I:I

    const v0, 0x3f19999a

    iput v0, p0, Lmaps/ba/b;->J:F

    iput-boolean v3, p0, Lmaps/ba/b;->K:Z

    const/16 v0, 0x3e8

    iput v0, p0, Lmaps/ba/b;->L:I

    const-wide/16 v0, 0x78

    iget-wide v2, p0, Lmaps/ba/b;->m:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/ba/b;->M:J

    iput-wide v4, p0, Lmaps/ba/b;->N:J

    new-instance v0, Lmaps/ba/e;

    invoke-direct {v0}, Lmaps/ba/e;-><init>()V

    iput-object v0, p0, Lmaps/ba/b;->j:Lmaps/ba/c;

    iget-object v0, p0, Lmaps/ba/b;->j:Lmaps/ba/c;

    invoke-interface {v0, p0}, Lmaps/ba/c;->a(Lmaps/ba/d;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000
    .end array-data
.end method

.method public constructor <init>(FIFIZ)V
    .locals 0

    invoke-direct {p0}, Lmaps/ba/b;-><init>()V

    iput p1, p0, Lmaps/ba/b;->z:F

    iput p2, p0, Lmaps/ba/b;->A:I

    iput p3, p0, Lmaps/ba/b;->C:F

    iput p4, p0, Lmaps/ba/b;->y:I

    iput-boolean p5, p0, Lmaps/ba/b;->K:Z

    return-void
.end method

.method private a(Lmaps/bw/a;)V
    .locals 9

    const v3, 0x4b189680

    const/high16 v5, 0x40400000

    iget v0, p1, Lmaps/bw/a;->b:I

    iput v0, p0, Lmaps/ba/b;->b:I

    iget v0, p1, Lmaps/bw/a;->c:I

    iput v0, p0, Lmaps/ba/b;->c:I

    iget v0, p1, Lmaps/bw/a;->b:I

    iget v1, p0, Lmaps/ba/b;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const v1, 0x47d90a8f

    mul-float/2addr v0, v1

    div-float v4, v0, v3

    iget v0, p1, Lmaps/bw/a;->c:I

    iget v1, p0, Lmaps/ba/b;->c:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lmaps/ba/b;->b:I

    int-to-double v1, v1

    invoke-static {v1, v2}, Lmaps/bw/b;->b(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    div-float v2, v0, v3

    new-instance v0, Lmaps/ba/a;

    sub-float v1, v2, v5

    add-float/2addr v2, v5

    sub-float v3, v4, v5

    add-float/2addr v4, v5

    const-wide/high16 v5, 0x3ff0000000000000L

    iget v7, p0, Lmaps/ba/b;->y:I

    invoke-direct/range {v0 .. v7}, Lmaps/ba/a;-><init>(FFFFDI)V

    iput-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    new-instance v0, Lmaps/bw/a;

    iget v1, p1, Lmaps/bw/a;->b:I

    iget v2, p1, Lmaps/bw/a;->c:I

    iget v3, p1, Lmaps/bw/a;->d:I

    iget-object v4, p1, Lmaps/bw/a;->f:Ljava/lang/String;

    iget-object v5, p1, Lmaps/bw/a;->g:Ljava/lang/String;

    iget v6, p1, Lmaps/bw/a;->h:I

    iget v7, p1, Lmaps/bw/a;->e:F

    iget-object v8, p1, Lmaps/bw/a;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lmaps/bw/a;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    iput-object v0, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    return-void
.end method

.method private a(Lmaps/bw/a;F)V
    .locals 12

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget-object v0, v0, Lmaps/ba/a;->i:[Lmaps/ba/f;

    array-length v1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget-object v2, v2, Lmaps/ba/a;->i:[Lmaps/ba/f;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lmaps/ba/b;->a(Lmaps/ba/f;)Lmaps/bw/a;

    move-result-object v3

    iget v4, v3, Lmaps/bw/a;->b:I

    iget v5, p1, Lmaps/bw/a;->b:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    iget v3, v3, Lmaps/bw/a;->c:I

    iget v6, p1, Lmaps/bw/a;->c:I

    sub-int/2addr v3, v6

    int-to-double v6, v3

    mul-double v3, v4, v4

    mul-double v5, v6, v6

    add-double/2addr v3, v5

    iget v5, v2, Lmaps/ba/f;->d:F

    iget v6, p0, Lmaps/ba/b;->n:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v5, v5

    const-wide v7, 0x400921fb54442d18L

    cmpl-double v7, v5, v7

    if-lez v7, :cond_0

    const-wide v7, 0x401921fb54442d18L

    sub-double v5, v7, v5

    :cond_0
    iget v5, v2, Lmaps/ba/f;->e:F

    float-to-double v5, v5

    float-to-double v7, p2

    const-wide/high16 v9, -0x4010000000000000L

    mul-double/2addr v3, v9

    iget v9, p0, Lmaps/ba/b;->H:I

    int-to-float v9, v9

    iget-object v10, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    iget v10, v10, Lmaps/bw/a;->d:I

    int-to-float v10, v10

    iget v11, p0, Lmaps/ba/b;->F:F

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-double v9, v9

    div-double/2addr v3, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->exp(D)D

    move-result-wide v3

    mul-double/2addr v3, v7

    add-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, v2, Lmaps/ba/f;->e:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Lmaps/bw/a;)V
    .locals 9

    iget-object v0, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/ba/b;->r:F

    iget-object v1, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    invoke-virtual {p1, v1}, Lmaps/bw/a;->a(Lmaps/bw/a;)D

    move-result-wide v1

    double-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/ba/b;->r:F

    :cond_0
    new-instance v0, Lmaps/bw/a;

    iget v1, p1, Lmaps/bw/a;->b:I

    iget v2, p1, Lmaps/bw/a;->c:I

    iget v3, p1, Lmaps/bw/a;->d:I

    iget-object v4, p1, Lmaps/bw/a;->f:Ljava/lang/String;

    iget-object v5, p1, Lmaps/bw/a;->g:Ljava/lang/String;

    iget v6, p1, Lmaps/bw/a;->h:I

    iget v7, p1, Lmaps/bw/a;->e:F

    iget-object v8, p1, Lmaps/bw/a;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lmaps/bw/a;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    iput-object v0, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    invoke-virtual {v0}, Lmaps/ba/a;->a()Lmaps/ba/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ba/b;->a(Lmaps/ba/f;)Lmaps/bw/a;

    move-result-object v1

    iput-object v1, p0, Lmaps/ba/b;->e:Lmaps/bw/a;

    iget-object v1, p0, Lmaps/ba/b;->u:Lmaps/ba/f;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ba/b;->u:Lmaps/ba/f;

    invoke-virtual {p0, v1}, Lmaps/ba/b;->a(Lmaps/ba/f;)Lmaps/bw/a;

    move-result-object v1

    iget v2, p0, Lmaps/ba/b;->s:F

    float-to-double v2, v2

    iget-object v4, p0, Lmaps/ba/b;->e:Lmaps/bw/a;

    invoke-virtual {v1, v4}, Lmaps/bw/a;->a(Lmaps/bw/a;)D

    move-result-wide v4

    add-double v1, v2, v4

    double-to-float v1, v1

    iput v1, p0, Lmaps/ba/b;->s:F

    :cond_1
    iput-object v0, p0, Lmaps/ba/b;->u:Lmaps/ba/f;

    iget-object v1, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget v0, v0, Lmaps/ba/f;->d:F

    iput v0, v1, Lmaps/ba/a;->h:F

    return-void
.end method

.method private b(Lmaps/bw/a;F)V
    .locals 8

    const/4 v0, 0x0

    :goto_0
    int-to-float v1, v0

    iget-object v2, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget v2, v2, Lmaps/ba/a;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, p2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget-object v1, v1, Lmaps/ba/a;->i:[Lmaps/ba/f;

    aget-object v1, v1, v0

    iget v2, p1, Lmaps/bw/a;->c:I

    iget v3, p0, Lmaps/ba/b;->c:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    iget v4, p0, Lmaps/ba/b;->b:I

    int-to-double v4, v4

    invoke-static {v4, v5}, Lmaps/bw/b;->b(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v4, v4

    const-wide v6, 0x416312d000000000L

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lmaps/ba/f;->a:F

    iget-object v1, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget-object v1, v1, Lmaps/ba/a;->i:[Lmaps/ba/f;

    aget-object v1, v1, v0

    iget v2, p1, Lmaps/bw/a;->b:I

    iget v3, p0, Lmaps/ba/b;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    const-wide v4, 0x3f86c22813448063L

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lmaps/ba/f;->b:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method a(Lmaps/bw/a;JJJJ)F
    .locals 12

    sub-long v2, p8, p2

    long-to-double v2, v2

    const-wide v4, 0x41cdcd6500000000L

    div-double v10, v2, v4

    const-wide/16 v2, 0x0

    cmpl-double v2, v10, v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget v2, p1, Lmaps/bw/a;->b:I

    int-to-double v2, v2

    iget v4, p1, Lmaps/bw/a;->c:I

    int-to-double v4, v4

    move-wide/from16 v0, p4

    long-to-double v6, v0

    move-wide/from16 v0, p6

    long-to-double v8, v0

    invoke-static/range {v2 .. v9}, Lmaps/bw/b;->a(DDDD)D

    move-result-wide v2

    div-double/2addr v2, v10

    double-to-float v2, v2

    goto :goto_0
.end method

.method public a(Lmaps/ba/f;)Lmaps/bw/a;
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lmaps/ba/b;->a(Lmaps/ba/f;Lmaps/bw/a;JZ)Lmaps/bw/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/ba/f;Lmaps/bw/a;JZ)Lmaps/bw/a;
    .locals 13

    if-eqz p5, :cond_0

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    invoke-virtual {v0, p1}, Lmaps/ba/a;->a(Lmaps/ba/f;)I

    move-result v0

    move v10, v0

    :goto_0
    iget v0, p0, Lmaps/ba/b;->b:I

    iget v1, p1, Lmaps/ba/f;->b:F

    float-to-double v1, v1

    const-wide v3, 0x40567f46328ec073L

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int v11, v0, v1

    iget v0, p0, Lmaps/ba/b;->c:I

    iget v1, p1, Lmaps/ba/f;->a:F

    float-to-double v1, v1

    const-wide v3, 0x416312d000000000L

    iget v5, p0, Lmaps/ba/b;->b:I

    int-to-double v5, v5

    invoke-static {v5, v6}, Lmaps/bw/b;->b(D)D

    move-result-wide v5

    div-double/2addr v3, v5

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int v12, v0, v1

    if-nez p2, :cond_1

    const/4 v7, 0x0

    :goto_1
    iget-object v0, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    if-nez v0, :cond_2

    const-string v8, "wifi"

    :goto_2
    new-instance v0, Lmaps/bw/a;

    const-string v4, ""

    const-string v5, ""

    const/4 v6, -0x1

    move v1, v11

    move v2, v12

    move v3, v10

    invoke-direct/range {v0 .. v8}, Lmaps/bw/a;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v0, -0x1

    move v10, v0

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lmaps/ba/b;->v:J

    int-to-long v4, v11

    int-to-long v6, v12

    move-object v0, p0

    move-object v1, p2

    move-wide/from16 v8, p3

    invoke-virtual/range {v0 .. v9}, Lmaps/ba/b;->a(Lmaps/bw/a;JJJJ)F

    move-result v7

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    iget-object v8, v0, Lmaps/bw/a;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method public a(F)V
    .locals 0

    return-void
.end method

.method public a(J)V
    .locals 6

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    const v1, 0x3f19999a

    invoke-virtual {v0, v1}, Lmaps/ba/a;->b(F)V

    const-wide/high16 v0, 0x3fe0000000000000L

    iget-wide v2, p0, Lmaps/ba/b;->i:J

    sub-long v2, p1, v2

    iget-wide v4, p0, Lmaps/ba/b;->k:J

    div-long/2addr v2, v4

    long-to-int v2, v2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lmaps/ba/b;->d:Lmaps/bw/a;

    invoke-direct {p0, v1, v0}, Lmaps/ba/b;->a(Lmaps/bw/a;F)V

    iget v0, p0, Lmaps/ba/b;->n:F

    invoke-virtual {p0, v0}, Lmaps/ba/b;->a(F)V

    return-void
.end method

.method public a(JFFF)V
    .locals 6

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ba/b;->j:Lmaps/ba/c;

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lmaps/ba/c;->a(JFFF)V

    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p3, v0, v1

    const/4 v1, 0x1

    aput p4, v0, v1

    const/4 v1, 0x2

    aput p5, v0, v1

    invoke-static {v0}, Lmaps/ba/g;->c([F)[F

    move-result-object v0

    iput-object v0, p0, Lmaps/ba/b;->f:[F

    iget-object v0, p0, Lmaps/ba/b;->g:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ba/b;->f:[F

    iput-object v0, p0, Lmaps/ba/b;->g:[F

    :cond_0
    iget-object v0, p0, Lmaps/ba/b;->g:[F

    const/4 v1, 0x0

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lmaps/ba/b;->g:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lmaps/ba/b;->f:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/ba/b;->g:[F

    const/4 v1, 0x1

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lmaps/ba/b;->g:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lmaps/ba/b;->f:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/ba/b;->g:[F

    const/4 v1, 0x2

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lmaps/ba/b;->g:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lmaps/ba/b;->f:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/ba/b;->g:[F

    invoke-static {v0}, Lmaps/ba/g;->c([F)[F

    move-result-object v0

    iput-object v0, p0, Lmaps/ba/b;->g:[F

    :cond_1
    return-void
.end method

.method public a(JLmaps/bw/a;)V
    .locals 4

    iget-wide v0, p0, Lmaps/ba/b;->N:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lmaps/ba/b;->l:J

    div-long v0, p1, v0

    iget-wide v2, p0, Lmaps/ba/b;->N:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lmaps/ba/b;->M:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-wide p1, p0, Lmaps/ba/b;->i:J

    if-eqz p3, :cond_0

    iget v0, p3, Lmaps/bw/a;->d:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    if-nez v0, :cond_2

    invoke-direct {p0, p3}, Lmaps/ba/b;->a(Lmaps/bw/a;)V

    goto :goto_0

    :cond_2
    const/high16 v0, 0x3f800000

    invoke-direct {p0, p3, v0}, Lmaps/ba/b;->a(Lmaps/bw/a;F)V

    invoke-direct {p0, p3}, Lmaps/ba/b;->b(Lmaps/bw/a;)V

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    invoke-virtual {v0}, Lmaps/ba/a;->b()V

    iget v0, p0, Lmaps/ba/b;->n:F

    invoke-virtual {p0, v0}, Lmaps/ba/b;->a(F)V

    iget v0, p0, Lmaps/ba/b;->z:F

    iget v1, p3, Lmaps/bw/a;->d:I

    iget v2, p0, Lmaps/ba/b;->A:I

    if-ge v1, v2, :cond_3

    iget v0, p0, Lmaps/ba/b;->C:F

    :cond_3
    invoke-direct {p0, p3, v0}, Lmaps/ba/b;->b(Lmaps/bw/a;F)V

    goto :goto_0
.end method

.method public b(J)Lmaps/bw/a;
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    invoke-virtual {v0}, Lmaps/ba/a;->a()Lmaps/ba/f;

    move-result-object v1

    iget-object v2, p0, Lmaps/ba/b;->w:Lmaps/bw/a;

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v3, p1

    invoke-virtual/range {v0 .. v5}, Lmaps/ba/b;->a(Lmaps/ba/f;Lmaps/bw/a;JZ)Lmaps/bw/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ba/b;->w:Lmaps/bw/a;

    iput-wide p1, p0, Lmaps/ba/b;->v:J

    iget-boolean v1, p0, Lmaps/ba/b;->K:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ba/b;->x:[Lmaps/ba/f;

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget v1, v1, Lmaps/ba/a;->a:I

    new-array v1, v1, [Lmaps/ba/f;

    iput-object v1, p0, Lmaps/ba/b;->x:[Lmaps/ba/f;

    :cond_0
    iget-object v1, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget-object v1, v1, Lmaps/ba/a;->i:[Lmaps/ba/f;

    iget-object v2, p0, Lmaps/ba/b;->x:[Lmaps/ba/f;

    iget-object v3, p0, Lmaps/ba/b;->a:Lmaps/ba/a;

    iget v3, v3, Lmaps/ba/a;->a:I

    invoke-static {v1, v6, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
