.class Lmaps/be/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/i;


# instance fields
.field private final a:Lmaps/ak/n;

.field private final b:Ljava/lang/Runnable;

.field private final c:Z


# direct methods
.method public constructor <init>(Lmaps/ak/n;Ljava/lang/Runnable;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/be/e;->a:Lmaps/ak/n;

    iput-object p2, p0, Lmaps/be/e;->b:Ljava/lang/Runnable;

    invoke-virtual {p1}, Lmaps/ak/n;->i()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/e;->c:Z

    iget-boolean v0, p0, Lmaps/be/e;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/ak/n;->k()V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, Lmaps/be/b;->f()Lmaps/p/k;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/be/e;->a:Lmaps/ak/n;

    invoke-virtual {v0, p0}, Lmaps/ak/n;->b(Lmaps/ak/i;)V

    iget-object v0, p0, Lmaps/be/e;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/be/e;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    const-class v1, Lmaps/be/b;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Lmaps/be/b;->a(Z)Z

    const-class v0, Lmaps/be/b;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/be/e;->b()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/ak/j;)V
    .locals 1

    instance-of v0, p1, Lmaps/be/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/be/e;->a:Lmaps/ak/n;

    invoke-virtual {v0, p0}, Lmaps/ak/n;->b(Lmaps/ak/i;)V

    iget-boolean v0, p0, Lmaps/be/e;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/be/e;->a:Lmaps/ak/n;

    invoke-virtual {v0}, Lmaps/ak/n;->j()V

    :cond_0
    return-void
.end method

.method public b(Lmaps/ak/j;)V
    .locals 2

    invoke-interface {p1}, Lmaps/ak/j;->a()I

    move-result v0

    const/16 v1, 0x4b

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lmaps/be/e;->b()V

    :cond_0
    return-void
.end method
