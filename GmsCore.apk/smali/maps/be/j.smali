.class public Lmaps/be/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/i;


# instance fields
.field private volatile a:I

.field private final b:Lmaps/ak/n;


# direct methods
.method public constructor <init>(Lmaps/ak/n;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/be/j;->a:I

    iput-object p1, p0, Lmaps/be/j;->b:Lmaps/ak/n;

    return-void
.end method

.method private b()V
    .locals 7

    new-instance v0, Lmaps/j/n;

    iget v1, p0, Lmaps/be/j;->a:I

    iget-object v2, p0, Lmaps/be/j;->b:Lmaps/ak/n;

    invoke-virtual {v2}, Lmaps/ak/n;->s()I

    move-result v2

    iget-object v3, p0, Lmaps/be/j;->b:Lmaps/ak/n;

    invoke-virtual {v3}, Lmaps/ak/n;->t()I

    move-result v3

    iget-object v4, p0, Lmaps/be/j;->b:Lmaps/ak/n;

    invoke-virtual {v4}, Lmaps/ak/n;->q()J

    move-result-wide v4

    iget-object v6, p0, Lmaps/be/j;->b:Lmaps/ak/n;

    invoke-virtual {v6}, Lmaps/ak/n;->u()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lmaps/j/n;-><init>(IIIJI)V

    invoke-static {v0}, Lmaps/j/k;->b(Lmaps/j/h;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 1

    new-instance v0, Lmaps/j/i;

    invoke-direct {v0, p1, p2}, Lmaps/j/i;-><init>(IZ)V

    invoke-static {v0}, Lmaps/j/k;->b(Lmaps/j/h;)V

    invoke-direct {p0}, Lmaps/be/j;->b()V

    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 2

    new-instance v0, Lmaps/j/j;

    const-string v1, "onComplete"

    invoke-direct {v0, v1, p1}, Lmaps/j/j;-><init>(Ljava/lang/String;Lmaps/ak/j;)V

    invoke-static {v0}, Lmaps/j/k;->b(Lmaps/j/h;)V

    iget v0, p0, Lmaps/be/j;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/be/j;->a:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/be/j;->b()V

    :cond_0
    return-void
.end method

.method public b(Lmaps/ak/j;)V
    .locals 0

    return-void
.end method
