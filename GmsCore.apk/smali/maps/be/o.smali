.class public final Lmaps/be/o;
.super Ljava/lang/Object;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:Lmaps/be/h;

.field private final D:Z

.field private E:I

.field private F:Z

.field private G:Z

.field private final a:D

.field private final b:D

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:D

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:F

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:D

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:Z

.field private final z:I


# direct methods
.method constructor <init>(Lmaps/bb/c;)V
    .locals 3

    const/16 v2, 0x1d

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lmaps/be/o;->b(Lmaps/bb/c;I)D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/be/o;->a:D

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lmaps/be/o;->a(Lmaps/bb/c;I)D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/be/o;->b:D

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->c:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->d:I

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->e:I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->f:I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->g:I

    const/16 v0, 0x8

    invoke-static {p1, v0}, Lmaps/be/o;->a(Lmaps/bb/c;I)D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/be/o;->h:D

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->i:I

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->j:I

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->k:I

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->l:I

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->m:I

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->n:I

    const/16 v0, 0x1b

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3c23d70a

    mul-float/2addr v0, v1

    iput v0, p0, Lmaps/be/o;->o:F

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->p:I

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->q:I

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->r:I

    const/16 v0, 0x13

    invoke-static {p1, v0}, Lmaps/be/o;->a(Lmaps/bb/c;I)D

    move-result-wide v0

    iput-wide v0, p0, Lmaps/be/o;->s:D

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->t:I

    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->u:I

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->v:I

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->w:I

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->x:I

    const/16 v0, 0x19

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/o;->y:Z

    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->z:I

    const/16 v0, 0x1c

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->A:I

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->B:I

    new-instance v1, Lmaps/be/h;

    invoke-virtual {p1, v2}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lmaps/be/h;-><init>(Lmaps/bb/c;)V

    iput-object v1, p0, Lmaps/be/o;->C:Lmaps/be/h;

    const/16 v0, 0x1f

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/o;->D:Z

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/o;->E:I

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/o;->F:Z

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lmaps/bb/c;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/o;->G:Z

    return-void

    :cond_0
    new-instance v0, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->j:Lmaps/bb/d;

    invoke-direct {v0, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    goto :goto_0
.end method

.method private static final a(Lmaps/bb/c;I)D
    .locals 4

    invoke-virtual {p0, p1}, Lmaps/bb/c;->d(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method private static final b(Lmaps/bb/c;I)D
    .locals 6

    const-wide/high16 v0, 0x4024000000000000L

    invoke-virtual {p0, p1}, Lmaps/bb/c;->d(I)I

    move-result v2

    neg-int v2, v2

    int-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/be/o;->x:I

    return v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/o;->y:Z

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/be/o;->z:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/o;->F:Z

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/o;->G:Z

    return v0
.end method
