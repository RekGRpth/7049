.class public final Lmaps/bf/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/bf/b;


# instance fields
.field private final a:Ljava/util/Map;

.field private b:J

.field private final c:Lmaps/ae/d;

.field private final d:Landroid/os/Handler;

.field private final e:Lmaps/bf/a;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lmaps/ae/d;Lmaps/bf/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    iput-object p1, p0, Lmaps/bf/d;->d:Landroid/os/Handler;

    iput-object p2, p0, Lmaps/bf/d;->c:Lmaps/ae/d;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bf/d;->b:J

    iput-object p3, p0, Lmaps/bf/d;->e:Lmaps/bf/a;

    return-void
.end method

.method public static b()Lmaps/bf/b;
    .locals 4

    new-instance v0, Lmaps/bf/d;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lmaps/co/a;

    invoke-direct {v2}, Lmaps/co/a;-><init>()V

    new-instance v3, Lmaps/bf/f;

    invoke-direct {v3}, Lmaps/bf/f;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lmaps/bf/d;-><init>(Landroid/os/Handler;Lmaps/ae/d;Lmaps/bf/a;)V

    return-object v0
.end method

.method private c()V
    .locals 6

    iget-wide v0, p0, Lmaps/bf/d;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lmaps/bf/d;->b:J

    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bf/e;

    iget-wide v2, p0, Lmaps/bf/d;->b:J

    invoke-static {v0}, Lmaps/bf/e;->a(Lmaps/bf/e;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/bf/d;->b:J

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/bf/d;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lmaps/bf/d;->d:Landroid/os/Handler;

    iget-wide v1, p0, Lmaps/bf/d;->b:J

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bf/e;

    iget-object v2, p0, Lmaps/bf/d;->e:Lmaps/bf/a;

    iget-object v3, v0, Lmaps/bf/e;->a:Lmaps/bf/c;

    iget-object v3, v3, Lmaps/bf/c;->bn:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Lmaps/bf/e;->b:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lmaps/bf/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/bf/d;->e:Lmaps/bf/a;

    invoke-interface {v0}, Lmaps/bf/a;->a()V

    iget-object v0, p0, Lmaps/bf/d;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bf/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bf/d;->e:Lmaps/bf/a;

    iget-object v1, p1, Lmaps/bf/c;->bn:Ljava/lang/String;

    invoke-interface {v0, v1}, Lmaps/bf/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/bf/c;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bf/e;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/bf/e;

    iget-object v1, p0, Lmaps/bf/d;->c:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->c()J

    move-result-wide v1

    const-wide/16 v3, 0x2710

    add-long/2addr v1, v3

    invoke-direct {v0, p1, v1, v2}, Lmaps/bf/e;-><init>(Lmaps/bf/c;J)V

    iget-object v1, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v1, v0, Lmaps/bf/e;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lmaps/bf/e;->b:I

    invoke-direct {p0}, Lmaps/bf/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 9

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lmaps/bf/d;->b:J

    iget-object v0, p0, Lmaps/bf/d;->c:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->c()J

    move-result-wide v2

    iget-object v0, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bf/c;

    iget-object v1, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bf/e;

    invoke-static {v1}, Lmaps/bf/e;->a(Lmaps/bf/e;)J

    move-result-wide v5

    cmp-long v5, v2, v5

    if-ltz v5, :cond_0

    iget-object v5, p0, Lmaps/bf/d;->e:Lmaps/bf/a;

    iget-object v6, v1, Lmaps/bf/e;->a:Lmaps/bf/c;

    iget-object v6, v6, Lmaps/bf/c;->bn:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "c="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v1, v1, Lmaps/bf/e;->b:I

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v6, v1}, Lmaps/bf/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/bf/d;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lmaps/bf/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method
