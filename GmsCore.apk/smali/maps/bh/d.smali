.class public Lmaps/bh/d;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lmaps/bb/c;Ljava/util/Set;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lmaps/bb/c;->j(I)I

    move v0, v1

    :goto_0
    invoke-virtual {p0, v4}, Lmaps/bb/c;->j(I)I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v4, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, v2}, Lmaps/bb/c;->d(I)I

    move-result v3

    int-to-short v3, v3

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
