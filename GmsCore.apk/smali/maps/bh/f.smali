.class public final Lmaps/bh/f;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)J
    .locals 6

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v0

    invoke-virtual {v0, p0, v2, v3}, Lmaps/ae/f;->b(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v4

    invoke-interface {v4}, Lmaps/ae/d;->a()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static a()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->L:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bj/b;->a()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lmaps/bh/b;

    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/bh/b;-><init>(Lmaps/ai/d;)V

    invoke-virtual {v0}, Lmaps/bh/b;->d()V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v2

    invoke-virtual {v2, p0, v0, v1}, Lmaps/ae/f;->a(Ljava/lang/String;J)V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bj/b;->a()V

    return-void
.end method
