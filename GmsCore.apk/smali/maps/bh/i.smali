.class Lmaps/bh/i;
.super Lmaps/ak/b;


# instance fields
.field final synthetic a:Lmaps/bh/k;


# direct methods
.method constructor <init>(Lmaps/bh/k;I[BZZZLjava/lang/Object;)V
    .locals 7

    iput-object p1, p0, Lmaps/bh/i;->a:Lmaps/bh/k;

    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lmaps/ak/b;-><init>(I[BZZZLjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public m()V
    .locals 4

    const-class v1, Lmaps/bh/k;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lmaps/bh/k;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/bh/k;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/bh/k;->g()Lmaps/bh/g;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bh/g;->a()Lmaps/bb/c;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lmaps/bh/k;->g()Lmaps/bh/g;

    move-result-object v0

    invoke-interface {v0}, Lmaps/bh/g;->a()Lmaps/bb/c;

    move-result-object v0

    iget-object v2, p0, Lmaps/bh/i;->a:Lmaps/bh/k;

    invoke-static {v2}, Lmaps/bh/k;->a(Lmaps/bh/k;)I

    move-result v2

    invoke-static {v0, v2}, Lmaps/bh/k;->a(Lmaps/bb/c;I)Lmaps/bb/c;

    move-result-object v0

    invoke-static {}, Lmaps/bh/k;->g()Lmaps/bh/g;

    move-result-object v2

    invoke-interface {v2, v0}, Lmaps/bh/g;->a(Lmaps/bb/c;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/bh/i;->a:Lmaps/bh/k;

    invoke-virtual {v0}, Lmaps/bh/k;->d()V

    :cond_1
    monitor-exit v1

    return-void

    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lmaps/bh/k;

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".uploadEventLog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lmaps/ae/h;->m:Z

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    const-string v2, "USER_EVENTSUserEventReporter"

    invoke-static {v2, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lmaps/bh/i;->a:Lmaps/bh/k;

    invoke-virtual {v0}, Lmaps/bh/k;->d()V

    return-void
.end method
