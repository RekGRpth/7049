.class public Lmaps/bk/b;
.super Ljava/lang/Object;


# static fields
.field private static final b:Lmaps/l/al;


# instance fields
.field protected volatile a:Lmaps/cr/c;

.field private c:Lmaps/ca/a;

.field private final d:Lmaps/an/y;

.field private volatile e:Lmaps/ca/e;

.field private final f:Lmaps/be/n;

.field private final g:Ljava/util/List;

.field private h:Lmaps/o/l;

.field private final i:Lmaps/be/i;

.field private j:I

.field private k:I

.field private final l:Lmaps/an/ag;

.field private m:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/LinkedList;

.field private p:Ljava/util/Map;

.field private q:Ljava/util/Map;

.field private volatile r:I

.field private final s:Lmaps/bk/f;

.field private t:Lmaps/bk/a;

.field private final u:Lmaps/bx/b;

.field private final v:Lmaps/bx/b;

.field private final w:Lmaps/bk/h;

.field private volatile x:J

.field private y:Lmaps/ae/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/l/n;

    invoke-direct {v0}, Lmaps/l/n;-><init>()V

    sput-object v0, Lmaps/bk/b;->b:Lmaps/l/al;

    return-void
.end method

.method public constructor <init>(Lmaps/o/c;Lmaps/bk/f;)V
    .locals 2

    new-instance v0, Lmaps/ca/e;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lmaps/ca/e;-><init>(Lmaps/o/c;Ljava/util/Set;)V

    sget-object v1, Lmaps/be/n;->a:Lmaps/be/n;

    invoke-direct {p0, p1, v0, p2, v1}, Lmaps/bk/b;-><init>(Lmaps/o/c;Lmaps/ca/e;Lmaps/bk/f;Lmaps/be/n;)V

    return-void
.end method

.method public constructor <init>(Lmaps/o/c;Lmaps/ca/e;Lmaps/bk/f;Lmaps/be/n;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    iput v3, p0, Lmaps/bk/b;->j:I

    iput v3, p0, Lmaps/bk/b;->k:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lmaps/bk/b;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/b;->p:Ljava/util/Map;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/b;->q:Ljava/util/Map;

    iput v3, p0, Lmaps/bk/b;->r:I

    new-instance v0, Lmaps/bk/c;

    invoke-direct {v0, p0, v2}, Lmaps/bk/c;-><init>(Lmaps/bk/b;Lmaps/bk/d;)V

    iput-object v0, p0, Lmaps/bk/b;->u:Lmaps/bx/b;

    new-instance v0, Lmaps/bk/e;

    invoke-direct {v0, p0, v2}, Lmaps/bk/e;-><init>(Lmaps/bk/b;Lmaps/bk/d;)V

    iput-object v0, p0, Lmaps/bk/b;->v:Lmaps/bx/b;

    new-instance v0, Lmaps/bk/h;

    invoke-direct {v0, p0, v2}, Lmaps/bk/h;-><init>(Lmaps/bk/b;Lmaps/bk/d;)V

    iput-object v0, p0, Lmaps/bk/b;->w:Lmaps/bk/h;

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    iput-object p2, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    iput-object v2, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iput-object p4, p0, Lmaps/bk/b;->f:Lmaps/be/n;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/bk/b;->g:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-static {p1}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    new-instance v0, Lmaps/bk/d;

    invoke-direct {v0, p0}, Lmaps/bk/d;-><init>(Lmaps/bk/b;)V

    iput-object v0, p0, Lmaps/bk/b;->l:Lmaps/an/ag;

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    iget-object v1, p0, Lmaps/bk/b;->l:Lmaps/an/ag;

    invoke-interface {v0, v1}, Lmaps/an/y;->a(Lmaps/an/ag;)V

    :goto_0
    iput-object p3, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    return-void

    :cond_0
    iput-object v2, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    iput-object v2, p0, Lmaps/bk/b;->l:Lmaps/an/ag;

    goto :goto_0
.end method

.method static synthetic a(Lmaps/bk/b;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/bk/a;)Lmaps/bk/a;
    .locals 0

    iput-object p1, p0, Lmaps/bk/b;->t:Lmaps/bk/a;

    return-object p1
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/t/ah;)Lmaps/l/al;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/bk/b;->b(Lmaps/t/ah;)Lmaps/l/al;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/bk/b;->a(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/bk/b;->a(Lmaps/t/ah;ILmaps/t/o;Z)Lmaps/l/al;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/t/ah;ILmaps/t/o;Z)Lmaps/l/al;
    .locals 8

    const/4 v5, 0x1

    iget-object v2, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    invoke-virtual {v1, p1, p1}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/bk/b;->h:Lmaps/o/l;

    invoke-virtual {p1}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v2

    invoke-interface {v1, v2}, Lmaps/o/l;->a(Lmaps/t/al;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    const/4 v4, 0x0

    iget-object v1, p0, Lmaps/bk/b;->g:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v1

    if-eqz v1, :cond_4

    instance-of v1, p3, Lmaps/t/ar;

    if-eqz v1, :cond_4

    iget-object v6, p0, Lmaps/bk/b;->g:Ljava/util/List;

    monitor-enter v6

    :try_start_2
    iget-object v1, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, p3

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/an/ac;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v3}, Lmaps/an/ac;->a(Lmaps/t/ah;Z)Lmaps/t/o;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object v0, v2

    check-cast v0, Lmaps/t/ar;

    move-object v1, v0

    move-object v0, v3

    check-cast v0, Lmaps/t/ar;

    move-object v2, v0

    invoke-static {v1, v2}, Lmaps/t/f;->a(Lmaps/t/ar;Lmaps/t/ar;)Lmaps/t/ar;

    move-result-object v2

    move v1, v4

    :goto_2
    move v4, v1

    goto :goto_1

    :cond_2
    move-object v0, p3

    check-cast v0, Lmaps/t/ar;

    move-object v3, v0

    invoke-virtual {v1, p1, v3}, Lmaps/an/ac;->a(Lmaps/t/ah;Lmaps/t/ar;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v5

    goto :goto_2

    :cond_3
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object p3, v2

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lmaps/bk/b;->b(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v1

    if-eqz v4, :cond_0

    const-wide/16 v2, 0xbb8

    invoke-interface {v1, v2, v3}, Lmaps/l/al;->a(J)V

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    :cond_5
    move v1, v4

    goto :goto_2
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/t/ah;Lmaps/l/al;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmaps/bk/b;->a(Lmaps/t/ah;Lmaps/l/al;J)V

    return-void
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/t/ah;ZLmaps/bx/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmaps/bk/b;->a(Lmaps/t/ah;ZLmaps/bx/b;)V

    return-void
.end method

.method private a(Lmaps/ca/e;Z)V
    .locals 2

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    invoke-virtual {v0, v1, p1}, Lmaps/ca/a;->b(Lmaps/cr/c;Lmaps/ca/e;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    invoke-virtual {v0, v1, p1}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;)V

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/l/al;J)V
    .locals 6

    if-eqz p2, :cond_0

    sget-object v0, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq p2, v0, :cond_1

    move-object v2, p2

    :goto_0
    invoke-virtual {p0}, Lmaps/bk/b;->j()Z

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lmaps/bk/b;->a(Lmaps/t/ah;Lmaps/l/al;ZJ)V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/l/al;ZJ)V
    .locals 9

    const/4 v0, 0x0

    iget-object v7, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-object v1, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bk/g;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lmaps/bk/g;->a(Lmaps/t/ah;Lmaps/l/al;ZJ)V

    :cond_0
    monitor-exit v7

    return-void

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v6, v0

    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bk/g;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lmaps/bk/g;->a(Lmaps/t/ah;Lmaps/l/al;ZJ)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lmaps/t/ah;ZLmaps/bx/b;)V
    .locals 2

    iget-object v1, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v0, p1, p3}, Lmaps/an/y;->b(Lmaps/t/ah;Lmaps/bx/b;)V

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lmaps/bk/b;->c(Lmaps/t/ah;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v0, p1, p3}, Lmaps/an/y;->a(Lmaps/t/ah;Lmaps/bx/b;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/bk/b;Lmaps/t/ah;Lmaps/l/al;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/bk/b;->a(Lmaps/t/ah;Lmaps/l/al;)Z

    move-result v0

    return v0
.end method

.method private a(Lmaps/t/ah;Lmaps/l/al;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    invoke-interface {p2, v1}, Lmaps/l/al;->a(Lmaps/ae/d;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lmaps/bk/b;->p:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lmaps/bk/b;->u:Lmaps/bx/b;

    invoke-direct {p0, p1, v0, v1}, Lmaps/bk/b;->a(Lmaps/t/ah;ZLmaps/bx/b;)V

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1, p2}, Lmaps/bk/b;->b(Lmaps/t/ah;Lmaps/l/al;)V

    goto :goto_0
.end method

.method static synthetic b(Lmaps/bk/b;)Lmaps/ca/e;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    return-object v0
.end method

.method static synthetic b(Lmaps/bk/b;Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmaps/bk/b;->c(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v0

    return-object v0
.end method

.method private b(Lmaps/t/ah;)Lmaps/l/al;
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v1, v2, v3, p1, v0}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v0

    sget-object v1, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    invoke-interface {v0, v1}, Lmaps/l/al;->b(Lmaps/ae/d;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private b(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;
    .locals 8

    const/4 v3, 0x0

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    :goto_0
    iget-object v4, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    if-eqz v4, :cond_7

    if-nez p2, :cond_7

    instance-of v2, p3, Lmaps/t/ar;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmaps/bk/b;->f:Lmaps/be/n;

    invoke-static {p3, v2, v4}, Lmaps/l/at;->a(Lmaps/t/o;Lmaps/be/n;Lmaps/cr/c;)Lmaps/l/at;

    move-result-object v2

    :goto_1
    sget-boolean v5, Lmaps/ae/h;->A:Z

    if-eqz v5, :cond_0

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    long-to-int v0, v0

    invoke-virtual {v5, v2, v0}, Lmaps/p/f;->a(Lmaps/l/al;I)V

    :cond_0
    if-nez v2, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    sget-object v2, Lmaps/bk/b;->b:Lmaps/l/al;

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v4, v1, p1, v2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Lmaps/l/al;)V

    :cond_2
    move-object v3, v2

    :cond_3
    return-object v3

    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_5
    instance-of v2, p3, Lmaps/t/bz;

    if-eqz v2, :cond_7

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_6

    check-cast p3, Lmaps/t/bz;

    invoke-static {p3, v4}, Lmaps/l/at;->a(Lmaps/t/bz;Lmaps/cr/c;)Lmaps/l/at;

    move-result-object v2

    goto :goto_1

    :cond_6
    invoke-static {p3, v4}, Lmaps/l/r;->a(Lmaps/t/o;Lmaps/cr/c;)Lmaps/l/r;

    move-result-object v2

    goto :goto_1

    :cond_7
    move-object v2, v3

    goto :goto_1
.end method

.method private b(Lmaps/t/ah;Lmaps/l/al;)V
    .locals 0

    return-void
.end method

.method static synthetic c(Lmaps/bk/b;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->p:Ljava/util/Map;

    return-object v0
.end method

.method private c(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;
    .locals 6

    const/4 v1, 0x0

    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p3

    check-cast v0, Lmaps/t/ar;

    invoke-virtual {v0}, Lmaps/t/ar;->q()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p3

    check-cast v0, Lmaps/t/ar;

    invoke-virtual {v0}, Lmaps/t/ar;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, p1, v4}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v2, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq v0, v2, :cond_2

    check-cast v0, Lmaps/l/at;

    check-cast p3, Lmaps/t/ar;

    invoke-virtual {p3}, Lmaps/t/ar;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lmaps/l/at;->b(J)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget v0, p0, Lmaps/bk/b;->k:I

    iget v3, p0, Lmaps/bk/b;->j:I

    add-int/2addr v0, v3

    and-int/lit8 v0, v0, 0x7f

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_3

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_3

    const-string v0, "TileFetcher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "numSkippedTileRequestsFromModifierCallback = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lmaps/bk/b;->k:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", numTileRequestsFromModifierCallback = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lmaps/bk/b;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    iget-object v2, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/bk/b;->i:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    iget v0, p0, Lmaps/bk/b;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bk/b;->k:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_5
    :try_start_3
    iget v0, p0, Lmaps/bk/b;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bk/b;->j:I

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v0, 0x1

    iget-object v2, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v2, p1, v0}, Lmaps/an/y;->a(Lmaps/t/ah;Z)Lmaps/t/o;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p1, p2, v0}, Lmaps/bk/b;->a(Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private c(Lmaps/t/ah;)V
    .locals 5

    iget-object v0, p0, Lmaps/bk/b;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/an/ae;->h:Lmaps/t/ah;

    if-eq p1, v0, :cond_1

    iget-object v2, p0, Lmaps/bk/b;->g:Ljava/util/List;

    monitor-enter v2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lmaps/an/y;->a(Lmaps/t/ah;Z)Lmaps/t/o;

    move-result-object v0

    check-cast v0, Lmaps/t/ar;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/ac;

    iget-object v4, p0, Lmaps/bk/b;->w:Lmaps/bk/h;

    invoke-virtual {v0, p1, v1, v4}, Lmaps/an/ac;->a(Lmaps/t/ah;Lmaps/t/ar;Lmaps/bx/b;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic d(Lmaps/bk/b;)Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    return-object v0
.end method

.method static synthetic e(Lmaps/bk/b;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lmaps/bk/b;)Lmaps/bk/f;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    return-object v0
.end method

.method static synthetic g(Lmaps/bk/b;)I
    .locals 2

    iget v0, p0, Lmaps/bk/b;->r:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmaps/bk/b;->r:I

    return v0
.end method

.method static synthetic h(Lmaps/bk/b;)I
    .locals 2

    iget v0, p0, Lmaps/bk/b;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lmaps/bk/b;->r:I

    return v0
.end method

.method static synthetic i(Lmaps/bk/b;)Lmaps/bx/b;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->v:Lmaps/bx/b;

    return-object v0
.end method

.method static synthetic j(Lmaps/bk/b;)Lmaps/bk/a;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->t:Lmaps/bk/a;

    return-object v0
.end method

.method static synthetic l()Lmaps/l/al;
    .locals 1

    sget-object v0, Lmaps/bk/b;->b:Lmaps/l/al;

    return-object v0
.end method

.method private final m()V
    .locals 4

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/bk/b;->x:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called from renderer thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Lmaps/l/al;
    .locals 6

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v3, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v4, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v1, v3, v4, p1, v2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v1

    sget-object v3, Lmaps/bk/b;->b:Lmaps/l/al;

    if-ne v1, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v3, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    invoke-interface {v1, v3}, Lmaps/l/al;->b(Lmaps/ae/d;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_2
    iget-object v1, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v3, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v4, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {p1}, Lmaps/t/ah;->b()Lmaps/t/ah;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5, v2}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Lmaps/t/ah;Z)Lmaps/l/al;

    move-result-object v1

    sget-object v2, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    invoke-interface {v1, v2}, Lmaps/l/al;->b(Lmaps/ae/d;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v1, p0, Lmaps/bk/b;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public a()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v0}, Lmaps/an/y;->i()Lmaps/o/c;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lmaps/bk/b;->x:J

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    invoke-virtual {v0, p1, p2}, Lmaps/ca/a;->a(J)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    sget-object v3, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/ca/a;->a(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V

    return-void
.end method

.method public a(Lmaps/bk/g;)V
    .locals 2

    iget-object v1, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/bk/b;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/cq/a;Lmaps/t/bx;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V
    .locals 8

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->e(Lmaps/cr/c;Lmaps/ca/e;)V

    iget-object v7, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lmaps/bk/f;->a(Lmaps/cq/a;Lmaps/t/bx;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    invoke-virtual {v0}, Lmaps/bk/f;->b()Lmaps/bk/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/bk/b;->t:Lmaps/bk/a;

    if-nez v1, :cond_0

    iget-object v1, v0, Lmaps/bk/a;->a:Lmaps/t/ah;

    iget-boolean v2, v0, Lmaps/bk/a;->b:Z

    iget-object v3, p0, Lmaps/bk/b;->v:Lmaps/bx/b;

    invoke-direct {p0, v1, v2, v3}, Lmaps/bk/b;->a(Lmaps/t/ah;ZLmaps/bx/b;)V

    :cond_0
    iput-object v0, p0, Lmaps/bk/b;->t:Lmaps/bk/a;

    monitor-exit v7

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    const-string v0, "GLState should not be null"

    invoke-static {p1, v0}, Lmaps/ap/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    new-instance v0, Lmaps/co/a;

    invoke-direct {v0}, Lmaps/co/a;-><init>()V

    invoke-static {v0}, Lmaps/ca/a;->a(Lmaps/ae/d;)V

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    return-void
.end method

.method public a(Lmaps/o/c;)V
    .locals 4

    iget-object v0, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    instance-of v0, v0, Lmaps/an/ae;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Modifiers not supported on store \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v2}, Lmaps/an/y;->i()Lmaps/o/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p1, Lmaps/o/c;->y:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only modifiers may be added, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lmaps/an/t;->a(Lmaps/o/c;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lmaps/an/t;->b(Lmaps/o/c;)Lmaps/an/y;

    move-result-object v0

    instance-of v1, v0, Lmaps/an/ac;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Modifier store \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lmaps/an/y;->i()Lmaps/o/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' must be a vector modifier store"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lmaps/bk/b;->g:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    monitor-exit v1

    :goto_0
    return-void

    :cond_3
    iget-object v2, p0, Lmaps/bk/b;->l:Lmaps/an/ag;

    invoke-interface {v0, v2}, Lmaps/an/y;->a(Lmaps/an/ag;)V

    iget-object v2, p0, Lmaps/bk/b;->g:Ljava/util/List;

    check-cast v0, Lmaps/an/ac;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iget-object v2, p0, Lmaps/bk/b;->g:Ljava/util/List;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/bk/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/ac;

    invoke-virtual {v0}, Lmaps/an/ac;->i()Lmaps/o/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown tile store "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v2, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    monitor-enter v2

    :try_start_4
    iget-object v0, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    iget-object v3, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    new-instance v0, Lmaps/ca/e;

    iget-object v2, p0, Lmaps/bk/b;->d:Lmaps/an/y;

    invoke-interface {v2}, Lmaps/an/y;->i()Lmaps/o/c;

    move-result-object v2

    iget-object v3, p0, Lmaps/bk/b;->f:Lmaps/be/n;

    invoke-direct {v0, v2, v1, v3}, Lmaps/ca/e;-><init>(Lmaps/o/c;Ljava/util/Set;Lmaps/be/n;)V

    iput-object v0, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    goto :goto_0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public a(Lmaps/o/l;)V
    .locals 0

    iput-object p1, p0, Lmaps/bk/b;->h:Lmaps/o/l;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    invoke-virtual {v0, p1}, Lmaps/ca/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public b(Ljava/util/List;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    sget-object v3, Lmaps/bk/b;->b:Lmaps/l/al;

    if-eq v0, v3, :cond_0

    invoke-interface {v0}, Lmaps/l/al;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v2, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v2, v3, v1}, Lmaps/ca/a;->b(Lmaps/cr/c;Lmaps/ca/e;Ljava/util/List;)V

    return-void
.end method

.method public c()V
    .locals 3

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->e(Lmaps/cr/c;Lmaps/ca/e;)V

    return-void
.end method

.method public d()V
    .locals 3

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->d(Lmaps/cr/c;Lmaps/ca/e;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    invoke-virtual {v0}, Lmaps/bk/f;->a()V

    return-void
.end method

.method public f()Lmaps/ca/e;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    return-object v0
.end method

.method public g()V
    .locals 1

    invoke-direct {p0}, Lmaps/bk/b;->m()V

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    invoke-virtual {v0}, Lmaps/ca/a;->b()V

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ca/e;

    invoke-direct {p0, v0, v2}, Lmaps/bk/b;->a(Lmaps/ca/e;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/bk/b;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public i()V
    .locals 3

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->c:Lmaps/ca/a;

    iget-object v1, p0, Lmaps/bk/b;->a:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/bk/b;->e:Lmaps/ca/e;

    invoke-virtual {v0, v1, v2}, Lmaps/ca/a;->c(Lmaps/cr/c;Lmaps/ca/e;)V

    invoke-virtual {p0}, Lmaps/bk/b;->h()Z

    :cond_0
    return-void
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/bk/b;->r:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/bk/b;->s:Lmaps/bk/f;

    invoke-virtual {v0}, Lmaps/bk/f;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/bk/b;->y:Lmaps/ae/d;

    return-object v0
.end method
