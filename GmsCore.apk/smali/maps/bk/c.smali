.class Lmaps/bk/c;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/b;


# instance fields
.field final synthetic a:Lmaps/bk/b;


# direct methods
.method private constructor <init>(Lmaps/bk/b;)V
    .locals 0

    iput-object p1, p0, Lmaps/bk/c;->a:Lmaps/bk/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/bk/b;Lmaps/bk/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/bk/c;-><init>(Lmaps/bk/b;)V

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;ILmaps/t/o;)V
    .locals 6

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/bk/c;->a:Lmaps/bk/b;

    invoke-static {v0, p1, p2, p3}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/bk/c;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->c(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_2

    const-string v0, "TileFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received an unknown tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v0, p0, Lmaps/bk/c;->a:Lmaps/bk/b;

    invoke-static {v0, p1, v1, v2, v3}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;Lmaps/l/al;J)V

    goto :goto_0
.end method
