.class public final Lmaps/bl/c;
.super Ljava/lang/Object;


# static fields
.field private static c:I

.field private static d:I

.field private static final e:[Lmaps/bl/c;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    const/16 v5, 0x16

    sput v0, Lmaps/bl/c;->c:I

    sput v5, Lmaps/bl/c;->d:I

    new-array v1, v5, [Lmaps/bl/c;

    sput-object v1, Lmaps/bl/c;->e:[Lmaps/bl/c;

    const/16 v1, 0x100

    :goto_0
    if-gt v0, v5, :cond_0

    sget-object v2, Lmaps/bl/c;->e:[Lmaps/bl/c;

    add-int/lit8 v3, v0, -0x1

    new-instance v4, Lmaps/bl/c;

    invoke-direct {v4, v0, v1}, Lmaps/bl/c;-><init>(II)V

    aput-object v4, v2, v3

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/bl/c;->b:I

    iput p2, p0, Lmaps/bl/c;->a:I

    return-void
.end method

.method public static a(I)Lmaps/bl/c;
    .locals 2

    const/4 v0, 0x0

    sget v1, Lmaps/bl/c;->c:I

    if-lt p0, v1, :cond_1

    const/16 v1, 0x16

    if-gt p0, v1, :cond_1

    sget v0, Lmaps/bl/c;->d:I

    if-le p0, v0, :cond_0

    sget p0, Lmaps/bl/c;->d:I

    :cond_0
    sget-object v0, Lmaps/bl/c;->e:[Lmaps/bl/c;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lmaps/bl/c;->b:I

    return v0
.end method

.method public a(II)I
    .locals 1

    iget v0, p0, Lmaps/bl/c;->b:I

    if-ge v0, p2, :cond_0

    iget v0, p0, Lmaps/bl/c;->b:I

    sub-int v0, p2, v0

    shl-int v0, p1, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/bl/c;->b:I

    sub-int/2addr v0, p2

    shr-int v0, p1, v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmaps/bl/c;->a:I

    return v0
.end method

.method public c()Lmaps/bl/c;
    .locals 1

    iget v0, p0, Lmaps/bl/c;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lmaps/bl/c;->a(I)Lmaps/bl/c;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->m:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/bl/c;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
