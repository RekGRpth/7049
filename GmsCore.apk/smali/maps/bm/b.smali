.class public Lmaps/bm/b;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z

.field public static e:Z

.field public static final f:Z

.field public static final g:Z

.field public static final h:Z

.field public static final i:Z

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;

.field private static final l:[Ljava/lang/String;

.field private static final m:[Ljava/lang/String;

.field private static final n:[Ljava/lang/String;

.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;

.field private static final q:[Ljava/lang/String;

.field private static final r:[Ljava/lang/String;

.field private static final s:[Ljava/lang/String;

.field private static volatile t:Ljava/lang/String;

.field private static u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v7, 0xa

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SOJU"

    aput-object v1, v0, v5

    const-string v1, "SOJUA"

    aput-object v1, v0, v4

    const-string v1, "SOJUK"

    aput-object v1, v0, v3

    const-string v1, "SOJU_L10N"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "GT-I9000"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "GT-I9000B"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GT-I9000M"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GT-I9000T"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SC-02B"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SGH-T959"

    aput-object v2, v0, v1

    const-string v1, "SGH-T959D"

    aput-object v1, v0, v7

    const/16 v1, 0xb

    const-string v2, "SGH-T959V"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "VIBRANT T959"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SHW-M110S"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SCH-I400"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SGH-I897"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SGH-I896"

    aput-object v2, v0, v1

    sput-object v0, Lmaps/bm/b;->j:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "RTGB"

    aput-object v1, v0, v5

    const-string v1, "SHADOW_VZW"

    aput-object v1, v0, v4

    const-string v1, "DAYTONA"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/bm/b;->k:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "SHADOW_VZW"

    aput-object v1, v0, v5

    const-string v1, "DAYTONA"

    aput-object v1, v0, v4

    const-string v1, "SPYDER_VZW"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/bm/b;->l:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "SHADOW"

    aput-object v1, v0, v5

    const-string v1, "DAYTONA"

    aput-object v1, v0, v4

    const-string v1, "SPYDER"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/bm/b;->m:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "HTC_VISION"

    aput-object v1, v0, v5

    sput-object v0, Lmaps/bm/b;->n:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "HTC_MARVEL"

    aput-object v1, v0, v5

    const-string v1, "HTC_MARVELC"

    aput-object v1, v0, v4

    const-string v1, "MARVELC"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/bm/b;->o:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "PASSION"

    aput-object v1, v0, v5

    const-string v1, "PASSION_KT"

    aput-object v1, v0, v4

    const-string v1, "PASSION_VF"

    aput-object v1, v0, v3

    sput-object v0, Lmaps/bm/b;->p:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "HTC_PYRAMID"

    aput-object v1, v0, v5

    const-string v1, "HTC_VIGOR"

    aput-object v1, v0, v4

    sput-object v0, Lmaps/bm/b;->q:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SONY ERICSSON"

    aput-object v1, v0, v5

    sput-object v0, Lmaps/bm/b;->r:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "TG03"

    aput-object v1, v0, v5

    const-string v1, "F11EIF"

    aput-object v1, v0, v4

    sput-object v0, Lmaps/bm/b;->s:[Ljava/lang/String;

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, ""

    :goto_1
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, ""

    :goto_2
    sget-object v3, Lmaps/bm/b;->j:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lmaps/bm/f;->c()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    :goto_3
    sput-boolean v3, Lmaps/bm/b;->a:Z

    sget-object v3, Lmaps/bm/b;->n:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    sput-boolean v3, Lmaps/bm/b;->b:Z

    sget-object v3, Lmaps/bm/b;->k:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    sput-boolean v3, Lmaps/bm/b;->c:Z

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v3, v7, :cond_5

    sget-object v3, Lmaps/bm/b;->l:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lmaps/bm/b;->m:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_0
    :goto_4
    sput-boolean v4, Lmaps/bm/b;->d:Z

    sget-object v1, Lmaps/bm/b;->r:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lmaps/bm/b;->e:Z

    sget-object v1, Lmaps/bm/b;->o:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lmaps/bm/b;->f:Z

    sget-object v1, Lmaps/bm/b;->p:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lmaps/bm/b;->g:Z

    sget-object v1, Lmaps/bm/b;->q:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lmaps/bm/b;->h:Z

    sget-object v1, Lmaps/bm/b;->s:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lmaps/bm/b;->i:Z

    return-void

    :cond_1
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_3
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_4
    move v3, v5

    goto/16 :goto_3

    :cond_5
    move v4, v5

    goto :goto_4
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lmaps/bm/b;->t:Ljava/lang/String;

    return-void
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, Lmaps/bm/b;->u:Z

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lmaps/bm/b;->u:Z

    return v0
.end method
