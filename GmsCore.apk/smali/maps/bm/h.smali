.class public Lmaps/bm/h;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(J)V
    .locals 4

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v0

    invoke-static {}, Lmaps/az/c;->b()Lmaps/az/b;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/az/b;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "MAPS"

    const-string v1, "Offline too long, wiping the cache."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lmaps/ci/b;->a()Lmaps/ci/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ci/b;->d()V

    invoke-static {}, Lmaps/af/w;->e()V

    invoke-static {}, Lmaps/aj/y;->c()Lmaps/aj/y;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/y;->a()V

    sget-object v0, Lmaps/aj/t;->b:Lmaps/aj/t;

    invoke-static {v0}, Lmaps/aj/l;->a(Lmaps/aj/t;)V

    const-string v0, "LAST_NETWORK_CONNECTED"

    invoke-static {v0}, Lmaps/bh/f;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lmaps/bm/h;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bm/h;->a(J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    sget-object v0, Lmaps/k/g;->a:Lmaps/k/g;

    new-instance v1, Lmaps/bm/a;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v3}, Lmaps/bm/a;-><init>(Lmaps/bm/h;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lmaps/k/c;->a(Lmaps/k/g;Lmaps/k/i;I)V

    return-void
.end method
