.class public Lmaps/bs/d;
.super Lmaps/bj/j;


# static fields
.field private static e:I

.field private static final f:Ljava/lang/Object;


# instance fields
.field private b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private d:Lorg/apache/http/client/HttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/bs/d;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lmaps/bj/j;-><init>()V

    iput-object p1, p0, Lmaps/bs/d;->c:Landroid/content/Context;

    const-string v0, "GoogleMobile/1.0"

    iput-object v0, p0, Lmaps/bs/d;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lmaps/bs/d;)Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lmaps/bs/d;->d:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lmaps/bs/d;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e()I
    .locals 2

    sget v0, Lmaps/bs/d;->e:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lmaps/bs/d;->e:I

    return v0
.end method

.method static synthetic f()I
    .locals 2

    sget v0, Lmaps/bs/d;->e:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lmaps/bs/d;->e:I

    return v0
.end method

.method static synthetic g()I
    .locals 1

    sget v0, Lmaps/bs/d;->e:I

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)Lmaps/bj/l;
    .locals 4

    iget-object v0, p0, Lmaps/bs/d;->d:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/am/g;

    iget-object v1, p0, Lmaps/bs/d;->c:Landroid/content/Context;

    iget-object v2, p0, Lmaps/bs/d;->b:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lmaps/am/g;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lmaps/bs/d;->d:Lorg/apache/http/client/HttpClient;

    iget-object v0, p0, Lmaps/bs/d;->d:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    new-instance v1, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    :cond_0
    new-instance v0, Lmaps/bs/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lmaps/bs/a;-><init>(Lmaps/bs/d;Ljava/lang/String;ZLmaps/bs/c;)V

    return-object v0
.end method
