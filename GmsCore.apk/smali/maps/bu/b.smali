.class public Lmaps/bu/b;
.super Lmaps/bu/c;


# instance fields
.field private final d:Lmaps/y/aj;


# direct methods
.method public constructor <init>(Lmaps/o/l;)V
    .locals 1

    sget-object v0, Lmaps/o/c;->h:Lmaps/o/c;

    invoke-direct {p0, v0, p1}, Lmaps/bu/c;-><init>(Lmaps/o/c;Lmaps/o/l;)V

    new-instance v0, Lmaps/y/aj;

    invoke-direct {v0}, Lmaps/y/aj;-><init>()V

    iput-object v0, p0, Lmaps/bu/b;->d:Lmaps/y/aj;

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;)Ljava/util/List;
    .locals 4

    const/16 v3, 0x10

    invoke-super {p0, p1}, Lmaps/bu/c;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lmaps/bu/b;->d:Lmaps/y/aj;

    invoke-virtual {p1}, Lmaps/bq/d;->i()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/y/aj;->a(Lmaps/t/bx;)V

    iget-object v1, p0, Lmaps/bu/b;->d:Lmaps/y/aj;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0
.end method
