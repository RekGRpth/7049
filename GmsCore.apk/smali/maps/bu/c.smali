.class public Lmaps/bu/c;
.super Lmaps/bu/f;


# instance fields
.field private d:Z

.field private e:J

.field private f:Lmaps/t/u;

.field private g:Lmaps/t/u;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(Lmaps/o/c;Lmaps/o/l;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/bu/f;-><init>(Lmaps/o/c;Lmaps/o/l;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bu/c;->e:J

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->o()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lmaps/bu/c;->a(Lmaps/o/c;D)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/bu/c;->d:Z

    return-void
.end method

.method private static a(Lmaps/o/c;D)Z
    .locals 2

    sget-object v0, Lmaps/o/c;->j:Lmaps/o/c;

    if-eq p0, v0, :cond_0

    sget-object v0, Lmaps/o/c;->k:Lmaps/o/c;

    if-eq p0, v0, :cond_0

    sget-object v0, Lmaps/o/c;->l:Lmaps/o/c;

    if-ne p0, v0, :cond_1

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lmaps/bu/c;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/bu/c;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v0

    iget-object v1, p0, Lmaps/bu/c;->b:Lmaps/o/l;

    invoke-interface {v1}, Lmaps/o/l;->a()Lmaps/t/al;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/al;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/bx;)F
    .locals 2

    iget-boolean v0, p0, Lmaps/bu/c;->d:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lmaps/bu/f;->a(Lmaps/t/bx;)F

    move-result v0

    const/high16 v1, 0x3f800000

    sub-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lmaps/bu/f;->a(Lmaps/t/bx;)F

    move-result v0

    goto :goto_0
.end method

.method public a()J
    .locals 2

    iget-wide v0, p0, Lmaps/bu/c;->e:J

    return-wide v0
.end method

.method public a(Lmaps/bq/d;)Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v1

    iget-object v0, p0, Lmaps/bu/c;->f:Lmaps/t/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/c;->f:Lmaps/t/u;

    invoke-virtual {v1, v0}, Lmaps/t/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/bu/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/c;->h:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, p0, Lmaps/bu/c;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lmaps/bu/c;->e:J

    invoke-virtual {v1}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v0

    invoke-virtual {p0, p1}, Lmaps/bu/c;->a_(Lmaps/bq/d;)I

    move-result v2

    iget-object v3, p0, Lmaps/bu/c;->b:Lmaps/o/l;

    invoke-interface {v3}, Lmaps/o/l;->a()Lmaps/t/al;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lmaps/t/ah;->a(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p0, v1, v2}, Lmaps/bu/c;->a(Lmaps/t/u;Ljava/util/ArrayList;)V

    :cond_1
    iput-object v2, p0, Lmaps/bu/c;->h:Ljava/util/List;

    iput-object v1, p0, Lmaps/bu/c;->f:Lmaps/t/u;

    iget-object v0, p0, Lmaps/bu/c;->h:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lmaps/t/u;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmaps/t/u;->b(Lmaps/t/an;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v4, -0x1

    :goto_2
    if-lt v0, v2, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method protected a_(Lmaps/bq/d;)I
    .locals 4

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v0

    iget-object v1, p0, Lmaps/bu/c;->c:Lmaps/p/k;

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v2

    iget-object v3, p0, Lmaps/bu/c;->a:Lmaps/o/c;

    invoke-virtual {v1, v2, v3}, Lmaps/p/k;->a(Lmaps/t/bx;Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lmaps/p/ap;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    float-to-int v0, v0

    goto :goto_0
.end method

.method public b(Lmaps/bq/d;)Ljava/util/List;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v1

    iget-object v0, p0, Lmaps/bu/c;->g:Lmaps/t/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/c;->g:Lmaps/t/u;

    invoke-virtual {v1, v0}, Lmaps/t/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/c;->i:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v0

    invoke-virtual {p0, p1}, Lmaps/bu/c;->a_(Lmaps/bq/d;)I

    move-result v2

    invoke-static {v0, v2}, Lmaps/t/ah;->b(Lmaps/t/bw;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p0, v1, v2}, Lmaps/bu/c;->a(Lmaps/t/u;Ljava/util/ArrayList;)V

    :cond_1
    iput-object v1, p0, Lmaps/bu/c;->g:Lmaps/t/u;

    iput-object v2, p0, Lmaps/bu/c;->i:Ljava/util/List;

    iget-object v0, p0, Lmaps/bu/c;->i:Ljava/util/List;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
