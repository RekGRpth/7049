.class public Lmaps/bu/d;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Set;Ljava/util/Set;I)I
    .locals 2

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    if-nez p2, :cond_2

    :cond_1
    return p2

    :cond_2
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p2, p2, -0x1

    goto :goto_0
.end method

.method public static a(Lmaps/cq/a;ILjava/util/Collection;Lmaps/t/bx;ILjava/util/LinkedHashSet;)Ljava/util/LinkedHashSet;
    .locals 9

    const/4 v2, 0x1

    const/4 v8, 0x3

    const/4 v7, 0x2

    if-nez p5, :cond_0

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object p5

    :cond_0
    invoke-static {p2}, Lmaps/f/a;->b(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-static {v0}, Lmaps/bu/d;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v0, p3}, Lmaps/bu/d;->a(Lmaps/cq/a;Ljava/util/Set;Lmaps/t/bx;)Ljava/util/Set;

    move-result-object v3

    invoke-static {p0, v1, p3}, Lmaps/bu/d;->a(Lmaps/cq/a;Ljava/util/Set;Lmaps/t/bx;)Ljava/util/Set;

    move-result-object v1

    const/4 v0, 0x0

    move-object v4, v1

    move-object v5, v3

    move v1, v2

    move v3, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    if-lez p4, :cond_9

    if-gt v1, p1, :cond_9

    if-ge v3, v8, :cond_9

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v6, 0xd

    if-gt v0, v6, :cond_4

    if-ge v3, v2, :cond_1

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v7, :cond_3

    :cond_1
    if-ge v3, v7, :cond_2

    const/4 v0, 0x4

    if-ge v1, v0, :cond_3

    :cond_2
    if-ge v3, v8, :cond_4

    const/4 v0, 0x6

    if-lt v1, v0, :cond_4

    :cond_3
    invoke-static {v5, p5, p4}, Lmaps/bu/d;->a(Ljava/util/Set;Ljava/util/Set;I)I

    move-result v0

    if-eq p4, v0, :cond_a

    add-int/lit8 v3, v3, 0x1

    move p4, v0

    :cond_4
    :goto_1
    if-eqz v4, :cond_6

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v7, :cond_5

    if-lt v1, v8, :cond_6

    :cond_5
    invoke-interface {v5, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    :cond_6
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    if-gt v0, v7, :cond_7

    invoke-static {v5}, Lmaps/bu/d;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    :cond_7
    if-eqz v4, :cond_8

    invoke-static {p0, v4, p3}, Lmaps/bu/d;->a(Lmaps/cq/a;Ljava/util/Set;Lmaps/t/bx;)Ljava/util/Set;

    move-result-object v4

    :cond_8
    invoke-static {p0, v5, p3}, Lmaps/bu/d;->a(Lmaps/cq/a;Ljava/util/Set;Lmaps/t/bx;)Ljava/util/Set;

    move-result-object v5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_9
    return-object p5

    :cond_a
    move p4, v0

    goto :goto_1
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 12

    const/4 v2, -0x1

    const/4 v11, 0x1

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v4

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v1

    shl-int v6, v11, v1

    add-int/lit8 v7, v6, -0x1

    move v3, v2

    :goto_0
    if-gt v3, v11, :cond_0

    move v1, v2

    :goto_1
    if-gt v1, v11, :cond_3

    invoke-virtual {v0}, Lmaps/t/ah;->e()I

    move-result v8

    add-int/2addr v8, v1

    if-ltz v8, :cond_1

    if-lt v8, v6, :cond_2

    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v9

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v10

    add-int/2addr v10, v3

    add-int/2addr v10, v6

    and-int/2addr v10, v7

    invoke-virtual {v0, v9, v10, v8}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_4
    return-object v4
.end method

.method private static a(Lmaps/cq/a;Ljava/util/Set;Lmaps/t/bx;)Ljava/util/Set;
    .locals 3

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-interface {p0, v0, p2}, Lmaps/cq/a;->a(Lmaps/t/ah;Lmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static b(Ljava/util/Set;)Ljava/util/Set;
    .locals 14

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lmaps/f/a;->b()Ljava/util/LinkedHashSet;

    move-result-object v9

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v9

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v10

    shl-int v5, v3, v10

    add-int/lit8 v11, v5, -0x1

    shr-int/lit8 v12, v5, 0x1

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v2, v1

    goto :goto_1

    :cond_1
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->d()I

    move-result v1

    sub-int/2addr v1, v2

    if-lt v1, v12, :cond_2

    move v2, v3

    :goto_2
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v3, v5

    move v6, v4

    move v7, v5

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->d()I

    move-result v8

    if-eqz v2, :cond_3

    if-ge v8, v12, :cond_3

    add-int/2addr v8, v5

    :cond_3
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v1}, Lmaps/t/ah;->e()I

    move-result v8

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v1}, Lmaps/t/ah;->e()I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_3

    :cond_4
    and-int v1, v7, v11

    invoke-virtual {v0, v10, v1, v3}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    and-int v1, v7, v11

    invoke-virtual {v0, v10, v1, v4}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    and-int v1, v6, v11

    invoke-virtual {v0, v10, v1, v3}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    and-int v1, v6, v11

    invoke-virtual {v0, v10, v1, v4}, Lmaps/t/ah;->a(III)Lmaps/t/ah;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v0, v9

    goto/16 :goto_0

    :cond_5
    move v2, v4

    goto :goto_2
.end method
