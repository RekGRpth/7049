.class public Lmaps/bu/e;
.super Lmaps/cq/c;


# instance fields
.field private a:Lmaps/cq/a;

.field private b:Lmaps/bu/c;

.field private c:Lmaps/o/c;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmaps/cq/c;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lmaps/o/c;ZLmaps/o/l;)Lmaps/bu/c;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bu/e;->b:Lmaps/bu/c;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lmaps/cq/c;->a(Lmaps/o/c;ZLmaps/o/l;)Lmaps/bu/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/bu/e;->b:Lmaps/bu/c;

    :cond_0
    iget-object v0, p0, Lmaps/bu/e;->b:Lmaps/bu/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bu/e;->a:Lmaps/cq/a;

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Lmaps/cq/c;->a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/bu/e;->a:Lmaps/cq/a;

    iput-object p1, p0, Lmaps/bu/e;->c:Lmaps/o/c;

    iput p2, p0, Lmaps/bu/e;->d:I

    iput-boolean p3, p0, Lmaps/bu/e;->e:Z

    :cond_0
    iget-object v0, p0, Lmaps/bu/e;->a:Lmaps/cq/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    sget-boolean v0, Lmaps/ae/h;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/e;->c:Lmaps/o/c;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lmaps/bu/e;->d:I

    if-ne p2, v0, :cond_2

    iget-boolean v0, p0, Lmaps/bu/e;->e:Z

    if-eq p3, v0, :cond_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Arguments changed! Was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/bu/e;->c:Lmaps/o/c;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/bu/e;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lmaps/bu/e;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; now: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
