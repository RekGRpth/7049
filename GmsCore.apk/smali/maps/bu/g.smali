.class public Lmaps/bu/g;
.super Lmaps/bu/f;


# instance fields
.field private d:Lmaps/t/u;

.field private final e:Ljava/util/List;

.field private final f:Lmaps/t/bx;

.field private g:Lmaps/bq/d;

.field private h:Lmaps/t/u;

.field private i:F

.field private final j:F

.field private k:J


# direct methods
.method public constructor <init>(Lmaps/o/c;ILmaps/o/l;)V
    .locals 2

    invoke-direct {p0, p1, p3}, Lmaps/bu/f;-><init>(Lmaps/o/c;Lmaps/o/l;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/bu/g;->f:Lmaps/t/bx;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/bu/g;->k:J

    mul-int v0, p2, p2

    int-to-float v0, v0

    iput v0, p0, Lmaps/bu/g;->j:F

    return-void
.end method

.method private a(Lmaps/t/ah;Lmaps/t/bx;Z)V
    .locals 6

    const/4 v5, 0x1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lmaps/bu/g;->h:Lmaps/t/u;

    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/u;->b(Lmaps/t/an;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    const/high16 v1, 0x20000000

    shr-int/2addr v1, v0

    iget-object v2, p0, Lmaps/bu/g;->f:Lmaps/t/bx;

    invoke-virtual {p1}, Lmaps/t/ah;->f()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p1}, Lmaps/t/ah;->g()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Lmaps/t/bx;->d(II)V

    iget-object v2, p0, Lmaps/bu/g;->g:Lmaps/bq/d;

    iget-object v3, p0, Lmaps/bu/g;->f:Lmaps/t/bx;

    invoke-virtual {v2, v3, v5}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v2

    iget-object v3, p0, Lmaps/bu/g;->g:Lmaps/bq/d;

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v3, v1, v2}, Lmaps/bq/d;->b(FF)F

    move-result v1

    iget v2, p0, Lmaps/bu/g;->i:F

    mul-float/2addr v2, v1

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/bu/g;->j:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_3

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, p1, p2}, Lmaps/bu/g;->b(Lmaps/t/ah;Lmaps/t/bx;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-direct {p0, v0, p2, v5}, Lmaps/bu/g;->a(Lmaps/t/ah;Lmaps/t/bx;Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {v0}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v0

    iget-object v1, p0, Lmaps/bu/g;->b:Lmaps/o/l;

    invoke-interface {v1}, Lmaps/o/l;->a()Lmaps/t/al;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/al;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lmaps/bu/g;->k:J

    return-wide v0
.end method

.method public a(Lmaps/bq/d;)Ljava/util/List;
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v3

    iget-object v0, p0, Lmaps/bu/g;->d:Lmaps/t/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/g;->d:Lmaps/t/u;

    invoke-virtual {v3, v0}, Lmaps/t/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/bu/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lmaps/bu/g;->k:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, p0, Lmaps/bu/g;->k:J

    invoke-virtual {v3}, Lmaps/t/u;->c()Lmaps/t/an;

    move-result-object v0

    check-cast v0, Lmaps/t/ak;

    invoke-virtual {v0}, Lmaps/t/ak;->e()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/t/ak;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v0

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lmaps/bq/d;->c(FF)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lmaps/bu/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lmaps/bu/g;->g:Lmaps/bq/d;

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v1

    iput-object v1, p0, Lmaps/bu/g;->h:Lmaps/t/u;

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v1

    const v4, 0x3c8efa35

    mul-float/2addr v1, v4

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v1

    iput v1, p0, Lmaps/bu/g;->i:F

    invoke-virtual {v3}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v1

    iget-object v4, p0, Lmaps/bu/g;->b:Lmaps/o/l;

    invoke-interface {v4}, Lmaps/o/l;->a()Lmaps/t/al;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lmaps/t/ah;->a(Lmaps/t/bw;ILmaps/t/al;)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ah;

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v5

    invoke-direct {p0, v0, v5, v2}, Lmaps/bu/g;->a(Lmaps/t/ah;Lmaps/t/bx;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iput-object v3, p0, Lmaps/bu/g;->d:Lmaps/t/u;

    iget-object v0, p0, Lmaps/bu/g;->e:Ljava/util/List;

    goto :goto_0
.end method
