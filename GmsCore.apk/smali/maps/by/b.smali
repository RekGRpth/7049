.class public Lmaps/by/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/i;


# static fields
.field private static a:Lmaps/by/b;


# instance fields
.field private final b:Lmaps/ak/n;

.field private final c:Lmaps/ae/d;

.field private final d:Lmaps/be/i;

.field private final e:Lmaps/be/i;

.field private volatile f:Lmaps/ag/aa;

.field private final g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/by/b;->b:Lmaps/ak/n;

    iput-object v0, p0, Lmaps/by/b;->c:Lmaps/ae/d;

    iput-object v0, p0, Lmaps/by/b;->d:Lmaps/be/i;

    iput-object v0, p0, Lmaps/by/b;->e:Lmaps/be/i;

    iput-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    iput-object v0, p0, Lmaps/by/b;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private constructor <init>(Lmaps/ak/n;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/by/b;->b:Lmaps/ak/n;

    iget-object v0, p0, Lmaps/by/b;->b:Lmaps/ak/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/by/b;->b:Lmaps/ak/n;

    invoke-virtual {v0, p0}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    :cond_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    iput-object v0, p0, Lmaps/by/b;->c:Lmaps/ae/d;

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/by/b;->d:Lmaps/be/i;

    new-instance v0, Lmaps/be/i;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/by/b;->e:Lmaps/be/i;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lmaps/by/b;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method static synthetic a(Lmaps/by/b;)Lmaps/ag/aa;
    .locals 1

    iget-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    return-object v0
.end method

.method public static a(Lmaps/ak/n;Ljava/io/File;)Lmaps/by/b;
    .locals 2

    sget-object v0, Lmaps/by/b;->a:Lmaps/by/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/by/b;

    invoke-direct {v0, p0}, Lmaps/by/b;-><init>(Lmaps/ak/n;)V

    sput-object v0, Lmaps/by/b;->a:Lmaps/by/b;

    :cond_0
    new-instance v0, Lmaps/by/e;

    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmaps/by/e;-><init>(Lmaps/ai/d;Ljava/io/File;)V

    invoke-virtual {v0}, Lmaps/by/e;->d()V

    sget-object v0, Lmaps/by/b;->a:Lmaps/by/b;

    return-object v0
.end method

.method private a(Ljava/io/File;)V
    .locals 1

    invoke-static {p1}, Lmaps/ag/aa;->a(Ljava/io/File;)Lmaps/ag/aa;

    move-result-object v0

    iput-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    invoke-direct {p0}, Lmaps/by/b;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;Lmaps/by/c;)V
    .locals 4

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ga;->a:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    invoke-virtual {p2}, Lmaps/by/c;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p2}, Lmaps/by/c;->e()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lmaps/bb/c;->b(IJ)Lmaps/bb/c;

    :cond_0
    new-instance v1, Lmaps/by/d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, p2, v2}, Lmaps/by/d;-><init>(Lmaps/by/b;Lmaps/bb/c;Lmaps/by/c;Lmaps/by/e;)V

    new-instance v0, Lmaps/j/j;

    const-string v2, "addRequest"

    invoke-direct {v0, v2, v1}, Lmaps/j/j;-><init>(Ljava/lang/String;Lmaps/ak/j;)V

    invoke-static {v0}, Lmaps/j/k;->b(Lmaps/j/h;)V

    iget-object v0, p0, Lmaps/by/b;->b:Lmaps/ak/n;

    invoke-virtual {v0, v1}, Lmaps/ak/n;->a(Lmaps/ak/j;)V

    return-void
.end method

.method static synthetic a(Lmaps/by/b;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/by/b;->a(Ljava/io/File;)V

    return-void
.end method

.method public static b()Lmaps/by/b;
    .locals 1

    sget-object v0, Lmaps/by/b;->a:Lmaps/by/b;

    return-object v0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lmaps/by/b;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method private d()V
    .locals 1

    :goto_0
    iget-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/by/b;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lmaps/by/a;)Lmaps/by/c;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lmaps/by/b;->a(Ljava/lang/String;Lmaps/by/a;Z)Lmaps/by/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lmaps/by/a;Z)Lmaps/by/c;
    .locals 7

    const/4 v1, 0x0

    if-eqz p3, :cond_3

    iget-object v2, p0, Lmaps/by/b;->e:Lmaps/be/i;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/by/b;->e:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/by/c;

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lmaps/by/c;

    invoke-direct {v0}, Lmaps/by/c;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/by/c;->a(Z)V

    iget-object v1, p0, Lmaps/by/b;->e:Lmaps/be/i;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v3}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-enter v0

    :try_start_1
    iget-object v1, p0, Lmaps/by/b;->c:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->a()J

    move-result-wide v1

    invoke-virtual {v0}, Lmaps/by/c;->f()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    add-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-gez v3, :cond_1

    invoke-direct {p0, p1, v0}, Lmaps/by/b;->a(Ljava/lang/String;Lmaps/by/c;)V

    invoke-virtual {v0, v1, v2}, Lmaps/by/c;->a(J)V

    :cond_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lmaps/by/c;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p2}, Lmaps/by/c;->a(Lmaps/by/a;)V

    :cond_2
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    iget-object v1, p0, Lmaps/by/b;->d:Lmaps/be/i;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lmaps/by/b;->d:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/by/c;

    if-nez v0, :cond_4

    iget-object v2, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    invoke-virtual {v0, p1}, Lmaps/ag/aa;->a(Ljava/lang/String;)Lmaps/by/c;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Lmaps/by/c;

    invoke-direct {v0}, Lmaps/by/c;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/by/c;->a(Z)V

    :cond_5
    iget-object v2, p0, Lmaps/by/b;->d:Lmaps/be/i;

    invoke-virtual {v2, p1, v0}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 1

    instance-of v0, p1, Lmaps/by/d;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/by/d;

    invoke-virtual {p1}, Lmaps/by/d;->f()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v1, p0, Lmaps/by/b;->e:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/by/b;->e:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/by/b;->d:Lmaps/be/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/by/b;->d:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lmaps/by/b;->d()V

    iget-object v0, p0, Lmaps/by/b;->f:Lmaps/ag/aa;

    invoke-virtual {v0}, Lmaps/ag/aa;->a()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b(Lmaps/ak/j;)V
    .locals 0

    return-void
.end method
