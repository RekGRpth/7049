.class public Lmaps/cf/a;
.super Ljava/lang/Object;


# static fields
.field public static final s:Lmaps/cf/a;

.field public static final t:Lmaps/cf/a;

.field public static final u:Lmaps/cf/a;

.field public static final v:Lmaps/cf/a;


# instance fields
.field public final a:Lmaps/p/l;

.field public final b:I

.field public final c:I

.field public final d:Lmaps/p/l;

.field public final e:F

.field public final f:I

.field public final g:I

.field public final h:Lmaps/p/l;

.field public final i:F

.field public final j:I

.field public final k:I

.field public final l:F

.field public final m:F

.field public final n:F

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 19

    new-instance v0, Lmaps/cf/a;

    sget-object v1, Lmaps/p/y;->d:Lmaps/p/l;

    const/16 v2, 0x12

    const/16 v3, 0xc

    sget-object v4, Lmaps/p/y;->c:Lmaps/p/l;

    const v5, 0x3fcccccd

    const/16 v6, 0xe

    const/16 v7, 0x20

    sget-object v8, Lmaps/p/y;->b:Lmaps/p/l;

    const v9, 0x3f99999a

    const/16 v10, 0x8

    const/16 v11, 0x10

    const v12, 0x3feccccd

    const/high16 v13, 0x3fc00000

    const/high16 v14, 0x3f800000

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, Lmaps/cf/a;-><init>(Lmaps/p/l;IILmaps/p/l;FIILmaps/p/l;FIIFFFZZZZ)V

    sput-object v0, Lmaps/cf/a;->s:Lmaps/cf/a;

    new-instance v0, Lmaps/cf/a;

    sget-object v1, Lmaps/p/y;->b:Lmaps/p/l;

    const/16 v2, 0xb

    const/16 v3, 0xb

    sget-object v4, Lmaps/p/y;->b:Lmaps/p/l;

    const/high16 v5, 0x3f800000

    const/16 v6, 0x8

    const/16 v7, 0x18

    sget-object v8, Lmaps/p/y;->b:Lmaps/p/l;

    const v9, 0x3f99999a

    const/16 v10, 0x8

    const/16 v11, 0x10

    const v12, 0x3f99999a

    const/high16 v13, 0x3f800000

    const/high16 v14, 0x3f800000

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, Lmaps/cf/a;-><init>(Lmaps/p/l;IILmaps/p/l;FIILmaps/p/l;FIIFFFZZZZ)V

    sput-object v0, Lmaps/cf/a;->t:Lmaps/cf/a;

    new-instance v0, Lmaps/cf/a;

    sget-object v1, Lmaps/p/y;->b:Lmaps/p/l;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lmaps/p/y;->b:Lmaps/p/l;

    const v5, 0x3f99999a

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lmaps/p/y;->b:Lmaps/p/l;

    const v9, 0x3f99999a

    const/16 v10, 0xa

    const/16 v11, 0x14

    const v12, 0x3f99999a

    const/high16 v13, 0x3fa00000

    const/high16 v14, 0x3fc00000

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, Lmaps/cf/a;-><init>(Lmaps/p/l;IILmaps/p/l;FIILmaps/p/l;FIIFFFZZZZ)V

    sput-object v0, Lmaps/cf/a;->u:Lmaps/cf/a;

    new-instance v0, Lmaps/cf/a;

    sget-object v1, Lmaps/p/y;->b:Lmaps/p/l;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lmaps/p/y;->b:Lmaps/p/l;

    const v5, 0x3f99999a

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lmaps/p/y;->b:Lmaps/p/l;

    const v9, 0x3f99999a

    const/16 v10, 0xa

    const/16 v11, 0x14

    const v12, 0x3f99999a

    const/high16 v13, 0x3f800000

    const v14, 0x3fa66666

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, Lmaps/cf/a;-><init>(Lmaps/p/l;IILmaps/p/l;FIILmaps/p/l;FIIFFFZZZZ)V

    sput-object v0, Lmaps/cf/a;->v:Lmaps/cf/a;

    return-void
.end method

.method private constructor <init>(Lmaps/p/l;IILmaps/p/l;FIILmaps/p/l;FIIFFFZZZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/cf/a;->a:Lmaps/p/l;

    iput p2, p0, Lmaps/cf/a;->b:I

    iput p3, p0, Lmaps/cf/a;->c:I

    iput-object p4, p0, Lmaps/cf/a;->d:Lmaps/p/l;

    iput p5, p0, Lmaps/cf/a;->e:F

    iput p6, p0, Lmaps/cf/a;->f:I

    iput p7, p0, Lmaps/cf/a;->g:I

    iput-object p8, p0, Lmaps/cf/a;->h:Lmaps/p/l;

    iput p9, p0, Lmaps/cf/a;->i:F

    iput p10, p0, Lmaps/cf/a;->j:I

    iput p11, p0, Lmaps/cf/a;->k:I

    iput p12, p0, Lmaps/cf/a;->l:F

    iput p13, p0, Lmaps/cf/a;->m:F

    iput p14, p0, Lmaps/cf/a;->n:F

    move/from16 v0, p15

    iput-boolean v0, p0, Lmaps/cf/a;->o:Z

    move/from16 v0, p16

    iput-boolean v0, p0, Lmaps/cf/a;->p:Z

    move/from16 v0, p17

    iput-boolean v0, p0, Lmaps/cf/a;->q:Z

    move/from16 v0, p18

    iput-boolean v0, p0, Lmaps/cf/a;->r:Z

    return-void
.end method
