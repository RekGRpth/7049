.class final Lmaps/ch/c;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/e/ab;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/location/LocationManager;

.field private final c:J

.field private d:Landroid/location/LocationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/ch/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/location/LocationManager;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ch/c;->b:Landroid/location/LocationManager;

    iput-wide p2, p0, Lmaps/ch/c;->c:J

    return-void
.end method

.method public static a(Landroid/content/Context;J)Lmaps/ch/c;
    .locals 2

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v1, Lmaps/ch/c;

    invoke-direct {v1, v0, p1, p2}, Lmaps/ch/c;-><init>(Landroid/location/LocationManager;J)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    const-string v1, "Disconnecting from Android NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    if-nez v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    const-string v1, "NativeNlpLocationProvider not started yet."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/ch/c;->b:Landroid/location/LocationManager;

    iget-object v1, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    goto :goto_0
.end method

.method public a(ZLandroid/location/LocationListener;)V
    .locals 7

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    const-string v1, "Connecting to Android NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    if-ne v0, p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "NativeNlpLocationProvider already started with a different location listener."

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-static {p2}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationListener;

    iput-object v0, p0, Lmaps/ch/c;->d:Landroid/location/LocationListener;

    iget-object v0, p0, Lmaps/ch/c;->b:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ch/c;->b:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-wide v2, p0, Lmaps/ch/c;->c:J

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Android NLP is bound with polling interval "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lmaps/ch/c;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/ch/c;->a:Ljava/lang/String;

    const-string v1, "Android NLP cannot be bound"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
