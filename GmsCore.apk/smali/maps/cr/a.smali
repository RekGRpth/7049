.class public Lmaps/cr/a;
.super Lmaps/q/au;


# instance fields
.field private i:Z

.field private j:Landroid/graphics/Bitmap;

.field private k:I

.field private l:I

.field private final m:[I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:F

.field private s:F

.field private final t:J

.field private u:I

.field private v:I

.field private w:Z


# direct methods
.method public constructor <init>(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/q/au;-><init>()V

    new-array v0, v3, [I

    iput-object v0, p0, Lmaps/cr/a;->m:[I

    iput-boolean v2, p0, Lmaps/cr/a;->n:Z

    iput-boolean v2, p0, Lmaps/cr/a;->o:Z

    iput-boolean v2, p0, Lmaps/cr/a;->p:Z

    iput-boolean v3, p0, Lmaps/cr/a;->q:Z

    iput v2, p0, Lmaps/cr/a;->v:I

    iput-boolean v2, p0, Lmaps/cr/a;->w:Z

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/cr/a;->t:J

    iget-object v0, p0, Lmaps/cr/a;->m:[I

    aput v2, v0, v2

    iput v3, p0, Lmaps/cr/a;->u:I

    return-void
.end method

.method public constructor <init>(Lmaps/cr/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-boolean p2, p0, Lmaps/cr/a;->w:Z

    return-void
.end method

.method public static a(II)I
    .locals 0

    :goto_0
    if-ge p1, p0, :cond_0

    shl-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return p1
.end method

.method static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lmaps/s/h;)Landroid/graphics/Bitmap;
    .locals 11

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v3}, Lmaps/cr/a;->a(II)I

    move-result v2

    invoke-static {v1, v3}, Lmaps/cr/a;->a(II)I

    move-result v3

    invoke-virtual {p2, v2, v3, p1}, Lmaps/s/h;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v5, p0, v7, v7, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-le v2, v0, :cond_0

    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v0, -0x1

    invoke-direct {v7, v8, v10, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v0, 0x1

    invoke-direct {v8, v0, v10, v9, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    if-le v3, v1, :cond_1

    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v1, -0x1

    invoke-direct {v7, v10, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v1, 0x1

    invoke-direct {v8, v10, v1, v0, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    if-le v2, v0, :cond_2

    if-le v3, v1, :cond_2

    new-instance v2, Landroid/graphics/Rect;

    add-int/lit8 v3, v0, -0x1

    add-int/lit8 v7, v1, -0x1

    invoke-direct {v2, v3, v7, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    add-int/lit8 v7, v0, 0x1

    add-int/lit8 v8, v1, 0x1

    invoke-direct {v3, v0, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    return-object v4
.end method

.method static c(Landroid/graphics/Bitmap;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/lit8 v2, v0, -0x1

    and-int/2addr v0, v2

    if-nez v0, :cond_0

    add-int/lit8 v0, v1, -0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    invoke-static {p0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private j()Lmaps/cr/c;
    .locals 2

    iget-wide v0, p0, Lmaps/cr/a;->t:J

    invoke-static {v0, v1}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Texture is out of date."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Ljavax/microedition/khronos/opengles/GL10;
    .locals 1

    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v0

    iget-object v0, v0, Lmaps/cr/c;->a:Ljavax/microedition/khronos/opengles/GL10;

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;I)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/cr/a;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;II)V

    iget-boolean v1, p0, Lmaps/cr/a;->i:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lmaps/cr/a;->w:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 8

    const/4 v0, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {p1}, Lmaps/cr/a;->c(Landroid/graphics/Bitmap;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v5

    invoke-static {p1, v1, v5}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lmaps/s/h;)Landroid/graphics/Bitmap;

    move-result-object v1

    move v7, v0

    :goto_0
    sget-boolean v5, Lmaps/ae/h;->t:Z

    if-nez v5, :cond_1

    move v6, v0

    :goto_1
    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;IIZZZ)V

    if-eqz v7, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->w:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void

    :cond_1
    move v6, v4

    goto :goto_1

    :cond_2
    move v7, v4

    move-object v1, p1

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;II)V
    .locals 8

    const/4 v4, 0x0

    invoke-static {p1}, Lmaps/cr/a;->c(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lmaps/s/h;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v0, 0x1

    move v7, v0

    :goto_0
    move-object v0, p0

    move v2, p2

    move v3, p3

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;IIZZZ)V

    if-eqz v7, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->w:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void

    :cond_1
    move v7, v4

    move-object v1, p1

    goto :goto_0
.end method

.method declared-synchronized a(Landroid/graphics/Bitmap;IIZZZ)V
    .locals 13

    monitor-enter p0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot have both isMipMap and autoGenerateMipMap be true."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v9

    iget-object v1, v9, Lmaps/cr/c;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eqz p6, :cond_2

    invoke-virtual {v9}, Lmaps/cr/c;->L()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move v4, v2

    :goto_0
    if-nez p1, :cond_3

    iput p2, p0, Lmaps/cr/a;->k:I

    move/from16 v0, p3

    iput v0, p0, Lmaps/cr/a;->l:I

    const/4 v2, 0x1

    invoke-static {p2, v2}, Lmaps/cr/a;->a(II)I

    move-result v7

    const/4 v2, 0x1

    move/from16 v0, p3

    invoke-static {v0, v2}, Lmaps/cr/a;->a(II)I

    move-result v8

    :goto_1
    invoke-virtual {v9}, Lmaps/cr/c;->I()I

    move-result v2

    if-gt v7, v2, :cond_1

    if-le v8, v2, :cond_5

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Textures with dimensions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " are larger than "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " the maximum supported size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto :goto_0

    :cond_3
    iput p2, p0, Lmaps/cr/a;->k:I

    if-eqz p5, :cond_4

    div-int/lit8 v2, p3, 0x2

    :goto_2
    iput v2, p0, Lmaps/cr/a;->l:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    goto :goto_1

    :cond_4
    move/from16 v2, p3

    goto :goto_2

    :cond_5
    int-to-float v2, p2

    int-to-float v3, v7

    div-float/2addr v2, v3

    iput v2, p0, Lmaps/cr/a;->r:F

    move/from16 v0, p3

    int-to-float v2, v0

    int-to-float v3, v8

    div-float/2addr v2, v3

    iput v2, p0, Lmaps/cr/a;->s:F

    iget-boolean v2, p0, Lmaps/cr/a;->w:Z

    if-eqz v2, :cond_d

    iget-boolean v2, p0, Lmaps/cr/a;->n:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x2901

    move v3, v2

    :goto_3
    iget-boolean v2, p0, Lmaps/cr/a;->o:Z

    if-eqz v2, :cond_9

    const/16 v2, 0x2901

    :goto_4
    invoke-virtual {p0, v3, v2}, Lmaps/cr/a;->b(II)V

    iget-boolean v2, p0, Lmaps/cr/a;->p:Z

    if-eqz v2, :cond_c

    if-nez p5, :cond_6

    if-eqz v4, :cond_b

    :cond_6
    iget-boolean v2, p0, Lmaps/cr/a;->q:Z

    if-eqz v2, :cond_a

    const/16 v2, 0x2703

    const/16 v3, 0x2601

    invoke-virtual {p0, v2, v3}, Lmaps/cr/a;->c(II)V

    :goto_5
    if-eqz p1, :cond_1e

    if-eqz p5, :cond_17

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-boolean v2, Lmaps/ae/h;->Q:Z

    if-eqz v2, :cond_7

    instance-of v2, v1, Lmaps/j/d;

    if-eqz v2, :cond_7

    move-object v0, v1

    check-cast v0, Lmaps/j/d;

    move-object v2, v0

    const v5, 0x3faaaa8f

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v6, v10

    mul-float/2addr v5, v6

    float-to-int v5, v5

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Lmaps/j/d;->a(J)V

    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    move v6, v3

    move v3, v4

    :goto_6
    if-lez v2, :cond_18

    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10}, Landroid/graphics/Canvas;-><init>()V

    new-instance v11, Landroid/graphics/Rect;

    const/4 v4, 0x0

    add-int v5, v2, v6

    invoke-direct {v11, v4, v6, v2, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v12, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v12, v4, v5, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    if-eqz p4, :cond_15

    invoke-virtual {v9}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4, v2, v2, v5}, Lmaps/s/h;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object v5, v4

    :goto_7
    invoke-virtual {v10, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v4, 0x0

    invoke-virtual {v10, p1, v11, v12, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v4, p0, Lmaps/cr/a;->w:Z

    if-eqz v4, :cond_16

    const/4 v2, 0x1

    new-instance v4, Lmaps/q/ab;

    invoke-direct {v4, v5}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v4, v2}, Lmaps/cr/a;->a(Lmaps/q/ab;Z)V

    const/4 v2, 0x0

    :goto_8
    add-int v4, v6, v2

    div-int/lit8 v2, v2, 0x2

    move v6, v4

    goto :goto_6

    :cond_8
    const v2, 0x812f

    move v3, v2

    goto/16 :goto_3

    :cond_9
    const v2, 0x812f

    goto/16 :goto_4

    :cond_a
    const/16 v2, 0x2701

    const/16 v3, 0x2601

    invoke-virtual {p0, v2, v3}, Lmaps/cr/a;->c(II)V

    goto :goto_5

    :cond_b
    const/16 v2, 0x2601

    const/16 v3, 0x2601

    invoke-virtual {p0, v2, v3}, Lmaps/cr/a;->c(II)V

    goto/16 :goto_5

    :cond_c
    const/16 v2, 0x2600

    const/16 v3, 0x2600

    invoke-virtual {p0, v2, v3}, Lmaps/cr/a;->c(II)V

    goto/16 :goto_5

    :cond_d
    iget-object v2, p0, Lmaps/cr/a;->m:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-nez v2, :cond_e

    const/4 v2, 0x1

    iget-object v3, p0, Lmaps/cr/a;->m:[I

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    :cond_e
    const/16 v2, 0xde1

    iget-object v3, p0, Lmaps/cr/a;->m:[I

    const/4 v5, 0x0

    aget v3, v3, v5

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    iget-boolean v2, p0, Lmaps/cr/a;->n:Z

    if-eqz v2, :cond_10

    const/16 v2, 0xde1

    const/16 v3, 0x2802

    const v5, 0x46240400

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    :goto_9
    iget-boolean v2, p0, Lmaps/cr/a;->o:Z

    if-eqz v2, :cond_11

    const/16 v2, 0xde1

    const/16 v3, 0x2803

    const v5, 0x46240400

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    :goto_a
    iget-boolean v2, p0, Lmaps/cr/a;->p:Z

    if-eqz v2, :cond_14

    if-nez p5, :cond_f

    if-eqz v4, :cond_13

    :cond_f
    iget-boolean v2, p0, Lmaps/cr/a;->q:Z

    if-eqz v2, :cond_12

    const/16 v2, 0xde1

    const/16 v3, 0x2801

    const v5, 0x461c0c00

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    :goto_b
    const/16 v2, 0xde1

    const/16 v3, 0x2800

    const v5, 0x46180400

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto/16 :goto_5

    :cond_10
    const/16 v2, 0xde1

    const/16 v3, 0x2802

    const v5, 0x47012f00

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_9

    :cond_11
    const/16 v2, 0xde1

    const/16 v3, 0x2803

    const v5, 0x47012f00

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_a

    :cond_12
    const/16 v2, 0xde1

    const/16 v3, 0x2801

    const v5, 0x461c0400

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_b

    :cond_13
    const/16 v2, 0xde1

    const/16 v3, 0x2801

    const v5, 0x46180400

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_b

    :cond_14
    const/16 v2, 0xde1

    const/16 v3, 0x2801

    const/high16 v5, 0x46180000

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0xde1

    const/16 v3, 0x2800

    const/high16 v5, 0x46180000

    invoke-interface {v1, v2, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto/16 :goto_5

    :cond_15
    invoke-virtual {v9}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4, v2, v2, v5}, Lmaps/s/h;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object v5, v4

    goto/16 :goto_7

    :cond_16
    const/16 v4, 0xde1

    const v10, 0x8191

    const/4 v11, 0x0

    invoke-interface {v1, v4, v10, v11}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v10, 0xde1

    add-int/lit8 v4, v3, 0x1

    const/4 v11, 0x0

    invoke-static {v10, v3, v5, v11}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    move v3, v4

    goto/16 :goto_8

    :cond_17
    if-eqz v4, :cond_1c

    iget-boolean v2, p0, Lmaps/cr/a;->w:Z

    if-eqz v2, :cond_1a

    const/4 v1, 0x1

    new-instance v2, Lmaps/q/ab;

    invoke-direct {v2, p1}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2, v1}, Lmaps/cr/a;->a(Lmaps/q/ab;Z)V

    :cond_18
    :goto_c
    if-nez p1, :cond_1f

    mul-int v1, v7, v8

    mul-int/lit8 v1, v1, 0x3

    :goto_d
    iput v1, p0, Lmaps/cr/a;->v:I

    iget-boolean v1, p0, Lmaps/cr/a;->i:Z

    if-eqz v1, :cond_19

    iput-object p1, p0, Lmaps/cr/a;->j:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_19
    monitor-exit p0

    return-void

    :cond_1a
    :try_start_2
    sget-boolean v2, Lmaps/ae/h;->t:Z

    if-eqz v2, :cond_1b

    const v2, 0x84c0

    invoke-static {v2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const/16 v2, 0xde1

    invoke-static {v2}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    :goto_e
    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    sget-boolean v2, Lmaps/ae/h;->Q:Z

    if-eqz v2, :cond_18

    instance-of v2, v1, Lmaps/j/d;

    if-eqz v2, :cond_18

    check-cast v1, Lmaps/j/d;

    const v2, 0x3faaaa8f

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lmaps/j/d;->a(J)V

    goto :goto_c

    :cond_1b
    const/16 v2, 0xde1

    const v3, 0x8191

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterx(III)V

    goto :goto_e

    :cond_1c
    iget-boolean v2, p0, Lmaps/cr/a;->w:Z

    if-eqz v2, :cond_1d

    const/4 v1, 0x0

    new-instance v2, Lmaps/q/ab;

    invoke-direct {v2, p1}, Lmaps/q/ab;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2, v1}, Lmaps/cr/a;->a(Lmaps/q/ab;Z)V

    goto :goto_c

    :cond_1d
    const/16 v2, 0xde1

    const v3, 0x8191

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    sget-boolean v2, Lmaps/ae/h;->Q:Z

    if-eqz v2, :cond_18

    instance-of v2, v1, Lmaps/j/d;

    if-eqz v2, :cond_18

    check-cast v1, Lmaps/j/d;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lmaps/j/d;->a(J)V

    goto/16 :goto_c

    :cond_1e
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glFinish()V

    const/16 v2, 0xde1

    const v3, 0x8191

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/16 v4, 0x1907

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v1 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glCopyTexImage2D(IIIIIIII)V

    goto/16 :goto_c

    :cond_1f
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    mul-int/2addr v1, v2

    goto/16 :goto_d
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v0

    iget-object v0, v0, Lmaps/cr/c;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempted to bind texture into an OpenGL context other than the one it was created from."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/cr/a;->m:[I

    aget v0, v0, v2

    if-eqz v0, :cond_1

    const/16 v0, 0xde1

    iget-object v1, p0, Lmaps/cr/a;->m:[I

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/cr/a;->n:Z

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/cr/a;->r:F

    return v0
.end method

.method public b(Landroid/content/res/Resources;I)V
    .locals 7

    const/4 v4, 0x0

    invoke-static {p1, p2}, Lmaps/cr/a;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;IIZZZ)V

    iget-boolean v0, p0, Lmaps/cr/a;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->w:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;II)V

    return-void
.end method

.method public b(Landroid/graphics/Bitmap;II)V
    .locals 8

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Lmaps/cr/a;->c(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Lmaps/cr/a;->j()Lmaps/cr/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/cr/c;->l()Lmaps/s/h;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lmaps/s/h;)Landroid/graphics/Bitmap;

    move-result-object p1

    move v7, v4

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;IIZZZ)V

    if-eqz v7, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->w:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void

    :cond_1
    move v7, v5

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/cr/a;->o:Z

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lmaps/cr/a;->s:F

    return v0
.end method

.method public c(Landroid/content/res/Resources;I)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/cr/a;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;II)V

    iget-boolean v1, p0, Lmaps/cr/a;->i:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lmaps/cr/a;->w:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/cr/a;->p:Z

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/cr/a;->k:I

    return v0
.end method

.method public d(Landroid/content/res/Resources;I)V
    .locals 7

    const/4 v4, 0x1

    invoke-static {p1, p2}, Lmaps/cr/a;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;IIZZZ)V

    iget-boolean v0, p0, Lmaps/cr/a;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/cr/a;->w:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/cr/a;->q:Z

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/cr/a;->l:I

    return v0
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/cr/a;->i:Z

    return-void
.end method

.method public declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/cr/a;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/cr/a;->u:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/cr/a;->u:I

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "releaseRef called on Texture with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/cr/a;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " references!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    const-string v1, "Texture"

    invoke-static {v1, v0}, Lmaps/bt/a;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    iget-wide v0, p0, Lmaps/cr/a;->t:J

    invoke-static {v0, v1}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v0

    iget v1, p0, Lmaps/cr/a;->u:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lmaps/cr/a;->u:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lmaps/cr/a;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    iget-object v1, p0, Lmaps/cr/a;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lmaps/cr/c;->c(I)V

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmaps/cr/a;->v:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lmaps/cr/a;->u:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/cr/a;->v:I

    return v0
.end method
