.class public Lmaps/d/a;
.super Ljava/lang/Object;


# instance fields
.field private A:F

.field private B:F

.field private C:J

.field private D:F

.field private E:F

.field private F:F

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Lmaps/d/h;

.field private a:Landroid/content/Context;

.field private b:Landroid/view/MotionEvent;

.field private c:Landroid/view/MotionEvent;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lmaps/d/e;

.field private final g:Lmaps/d/e;

.field private final h:Lmaps/d/e;

.field private final i:Lmaps/d/e;

.field private final j:Ljava/util/LinkedList;

.field private k:J

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmaps/d/w;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/d/a;->e:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object p1, p0, Lmaps/d/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/d/a;->D:F

    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    new-instance v1, Lmaps/d/t;

    invoke-direct {v1, p2}, Lmaps/d/t;-><init>(Lmaps/d/w;)V

    iput-object v1, p0, Lmaps/d/a;->g:Lmaps/d/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    new-instance v1, Lmaps/d/p;

    invoke-direct {v1, p2}, Lmaps/d/p;-><init>(Lmaps/d/w;)V

    iput-object v1, p0, Lmaps/d/a;->h:Lmaps/d/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "MD"

    const-string v1, "T"

    invoke-static {v0, v1}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    new-instance v1, Lmaps/d/g;

    invoke-direct {v1, p2}, Lmaps/d/g;-><init>(Lmaps/d/w;)V

    iput-object v1, p0, Lmaps/d/a;->f:Lmaps/d/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    new-instance v1, Lmaps/d/o;

    invoke-direct {v1, p2}, Lmaps/d/o;-><init>(Lmaps/d/w;)V

    iput-object v1, p0, Lmaps/d/a;->i:Lmaps/d/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/d/h;

    invoke-direct {v0, p1, p2}, Lmaps/d/h;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lmaps/d/a;->K:Lmaps/d/h;

    iget-object v0, p0, Lmaps/d/a;->K:Lmaps/d/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/d/h;->a(Z)V

    iget-object v0, p0, Lmaps/d/a;->K:Lmaps/d/h;

    invoke-virtual {v0, p2}, Lmaps/d/h;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    new-instance v1, Lmaps/d/i;

    invoke-direct {v1, p2}, Lmaps/d/i;-><init>(Lmaps/d/w;)V

    iput-object v1, p0, Lmaps/d/a;->h:Lmaps/d/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "MD"

    const-string v1, "F"

    invoke-static {v0, v1}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .locals 2

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V
    .locals 10

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/d/a;->k:J

    :cond_0
    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    new-instance v2, Lmaps/d/c;

    invoke-direct {v2, p1}, Lmaps/d/c;-><init>(Landroid/view/MotionEvent;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_1

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->e()V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lmaps/d/a;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->e()V

    goto :goto_0

    :cond_2
    sparse-switch v1, :sswitch_data_0

    :goto_1
    move v4, v7

    :goto_2
    iget-boolean v0, p0, Lmaps/d/a;->J:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_3
    :pswitch_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/e;

    invoke-virtual {v0}, Lmaps/d/e;->a()Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v9, Lmaps/d/q;->a:[I

    iget-wide v1, p0, Lmaps/d/a;->k:J

    iget-object v3, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    iget-object v5, p0, Lmaps/d/a;->e:Ljava/util/List;

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/d/e;->a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)Lmaps/d/f;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/d/f;->ordinal()I

    move-result v1

    aget v1, v9, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_3

    :cond_4
    :pswitch_1
    if-eqz v4, :cond_5

    invoke-direct {p0}, Lmaps/d/a;->j()V

    iput-boolean v7, p0, Lmaps/d/a;->J:Z

    :cond_5
    return-void

    :sswitch_0
    const/4 v4, 0x1

    goto :goto_2

    :sswitch_1
    iput-boolean v7, p0, Lmaps/d/a;->J:Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {v0, p0}, Lmaps/d/e;->a(Lmaps/d/a;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/d/a;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x106 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/StringBuilder;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/d/a;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/e;

    sget-boolean v3, Lmaps/ae/h;->k:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lmaps/d/e;->a()Z

    move-result v3

    invoke-static {v3}, Lmaps/ah/d;->a(Z)V

    :cond_0
    invoke-virtual {v0, p0}, Lmaps/d/e;->e(Lmaps/d/a;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_1
    return v1
.end method

.method private a(Lmaps/d/e;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmaps/d/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/view/MotionEvent;I)F
    .locals 2

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 11

    const/high16 v1, -0x40800000

    const/high16 v10, 0x3f000000

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    iput v1, p0, Lmaps/d/a;->t:F

    iput v1, p0, Lmaps/d/a;->u:F

    iput v1, p0, Lmaps/d/a;->x:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/d/a;->y:F

    iput-boolean v9, p0, Lmaps/d/a;->H:Z

    iput-boolean v9, p0, Lmaps/d/a;->I:Z

    iget-object v0, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    sub-float/2addr v3, v1

    sub-float/2addr v4, v2

    sub-float/2addr v7, v5

    sub-float/2addr v8, v6

    iput v3, p0, Lmaps/d/a;->p:F

    iput v4, p0, Lmaps/d/a;->q:F

    iput v7, p0, Lmaps/d/a;->r:F

    iput v8, p0, Lmaps/d/a;->s:F

    iput v2, p0, Lmaps/d/a;->v:F

    iput v6, p0, Lmaps/d/a;->w:F

    mul-float/2addr v7, v10

    add-float/2addr v5, v7

    iput v5, p0, Lmaps/d/a;->l:F

    mul-float v5, v8, v10

    add-float/2addr v5, v6

    iput v5, p0, Lmaps/d/a;->m:F

    mul-float/2addr v3, v10

    add-float/2addr v1, v3

    iput v1, p0, Lmaps/d/a;->n:F

    mul-float v1, v4, v10

    add-float/2addr v1, v2

    iput v1, p0, Lmaps/d/a;->o:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/d/a;->C:J

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lmaps/d/a;->A:F

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/d/a;->B:F

    return-void
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lmaps/d/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lmaps/d/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/e;

    sget-boolean v2, Lmaps/ae/h;->k:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lmaps/d/e;->a()Z

    move-result v2

    invoke-static {v2}, Lmaps/ah/d;->a(Z)V

    :cond_0
    invoke-virtual {v0, p0}, Lmaps/d/e;->c(Lmaps/d/a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private k()Z
    .locals 5

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->a()J

    move-result-wide v1

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->a()J

    move-result-wide v3

    sub-long v0, v3, v1

    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    iput-object v1, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    iput-boolean v0, p0, Lmaps/d/a;->G:Z

    iput-boolean v0, p0, Lmaps/d/a;->J:Z

    iget-object v0, p0, Lmaps/d/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lmaps/d/a;->m()V

    iget-object v0, p0, Lmaps/d/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/e;

    invoke-virtual {v0}, Lmaps/d/e;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p0}, Lmaps/d/e;->c(Lmaps/d/a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/s;

    invoke-virtual {v0}, Lmaps/d/s;->e()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/d/a;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmaps/d/a;->l:F

    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 11

    const/high16 v10, -0x40800000

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const v4, 0xff00

    and-int/2addr v4, v3

    shr-int/lit8 v4, v4, 0x8

    iget-boolean v5, p0, Lmaps/d/a;->J:Z

    if-nez v5, :cond_14

    const/4 v1, 0x5

    if-eq v3, v1, :cond_0

    const/16 v1, 0x105

    if-eq v3, v1, :cond_0

    if-nez v3, :cond_9

    :cond_0
    iget-object v1, p0, Lmaps/d/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, Lmaps/d/a;->D:F

    sub-float/2addr v3, v4

    iput v3, p0, Lmaps/d/a;->E:F

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v3, p0, Lmaps/d/a;->D:F

    sub-float/2addr v1, v3

    iput v1, p0, Lmaps/d/a;->F:F

    invoke-direct {p0}, Lmaps/d/a;->l()V

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lmaps/d/a;->C:J

    invoke-direct {p0, p1}, Lmaps/d/a;->b(Landroid/view/MotionEvent;)V

    iget v1, p0, Lmaps/d/a;->D:F

    iget v4, p0, Lmaps/d/a;->E:F

    iget v5, p0, Lmaps/d/a;->F:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lmaps/d/a;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lmaps/d/a;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    cmpg-float v9, v3, v1

    if-ltz v9, :cond_1

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_1

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_1

    cmpl-float v3, v6, v5

    if-lez v3, :cond_4

    :cond_1
    move v3, v2

    :goto_0
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_2

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_2

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_2

    cmpl-float v1, v8, v5

    if-lez v1, :cond_5

    :cond_2
    move v1, v2

    :goto_1
    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    iput v10, p0, Lmaps/d/a;->l:F

    iput v10, p0, Lmaps/d/a;->m:F

    iput-boolean v2, p0, Lmaps/d/a;->G:Z

    :cond_3
    :goto_2
    iget-object v0, p0, Lmaps/d/a;->K:Lmaps/d/h;

    invoke-virtual {v0, p1}, Lmaps/d/h;->a(Landroid/view/MotionEvent;)Z

    return v2

    :cond_4
    move v3, v0

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    iput-boolean v2, p0, Lmaps/d/a;->G:Z

    goto :goto_2

    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    iput-boolean v2, p0, Lmaps/d/a;->G:Z

    goto :goto_2

    :cond_8
    iput-boolean v2, p0, Lmaps/d/a;->J:Z

    goto :goto_2

    :cond_9
    const/4 v1, 0x2

    if-ne v3, v1, :cond_11

    iget-boolean v1, p0, Lmaps/d/a;->G:Z

    if-eqz v1, :cond_11

    iget v1, p0, Lmaps/d/a;->D:F

    iget v4, p0, Lmaps/d/a;->E:F

    iget v5, p0, Lmaps/d/a;->F:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lmaps/d/a;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lmaps/d/a;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    cmpg-float v9, v3, v1

    if-ltz v9, :cond_a

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_a

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_a

    cmpl-float v3, v6, v5

    if-lez v3, :cond_c

    :cond_a
    move v3, v2

    :goto_3
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_b

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_b

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_b

    cmpl-float v1, v8, v5

    if-lez v1, :cond_d

    :cond_b
    move v1, v2

    :goto_4
    if-eqz v3, :cond_e

    if-eqz v1, :cond_e

    iput v10, p0, Lmaps/d/a;->l:F

    iput v10, p0, Lmaps/d/a;->m:F

    goto/16 :goto_2

    :cond_c
    move v3, v0

    goto :goto_3

    :cond_d
    move v1, v0

    goto :goto_4

    :cond_e
    if-eqz v3, :cond_f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    goto/16 :goto_2

    :cond_f
    if-eqz v1, :cond_10

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    goto/16 :goto_2

    :cond_10
    iput-boolean v0, p0, Lmaps/d/a;->G:Z

    iput-boolean v2, p0, Lmaps/d/a;->J:Z

    goto/16 :goto_2

    :cond_11
    const/4 v1, 0x6

    if-eq v3, v1, :cond_12

    const/16 v1, 0x106

    if-eq v3, v1, :cond_12

    if-ne v3, v2, :cond_3

    :cond_12
    iget-boolean v1, p0, Lmaps/d/a;->G:Z

    if-eqz v1, :cond_3

    if-nez v4, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_13
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    goto/16 :goto_2

    :cond_14
    invoke-direct {p0}, Lmaps/d/a;->i()Z

    move-result v5

    if-nez v5, :cond_15

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lmaps/d/a;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    goto/16 :goto_2

    :cond_15
    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    invoke-direct {p0, p1}, Lmaps/d/a;->b(Landroid/view/MotionEvent;)V

    if-nez v4, :cond_16

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_16
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/d/a;->l:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->m:F

    iget-boolean v0, p0, Lmaps/d/a;->G:Z

    if-nez v0, :cond_17

    invoke-direct {p0}, Lmaps/d/a;->j()V

    :cond_17
    invoke-direct {p0}, Lmaps/d/a;->l()V

    goto/16 :goto_2

    :sswitch_1
    iget-boolean v0, p0, Lmaps/d/a;->G:Z

    if-nez v0, :cond_18

    invoke-direct {p0}, Lmaps/d/a;->j()V

    :cond_18
    invoke-direct {p0}, Lmaps/d/a;->l()V

    goto/16 :goto_2

    :sswitch_2
    invoke-direct {p0, p1}, Lmaps/d/a;->b(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v1}, Lmaps/d/a;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    iget v0, p0, Lmaps/d/a;->A:F

    iget v3, p0, Lmaps/d/a;->B:F

    div-float/2addr v0, v3

    const v3, 0x3f2b851f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    invoke-direct {p0, v1}, Lmaps/d/a;->a(Ljava/lang/StringBuilder;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x106 -> :sswitch_0
    .end sparse-switch
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/d/a;->m:F

    return v0
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lmaps/d/a;->n:F

    return v0
.end method

.method public d()F
    .locals 2

    iget v0, p0, Lmaps/d/a;->t:F

    const/high16 v1, -0x40800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/d/a;->r:F

    iget v1, p0, Lmaps/d/a;->s:F

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lmaps/d/a;->t:F

    :cond_0
    iget v0, p0, Lmaps/d/a;->t:F

    return v0
.end method

.method public e()F
    .locals 2

    iget v0, p0, Lmaps/d/a;->u:F

    const/high16 v1, -0x40800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/d/a;->p:F

    iget v1, p0, Lmaps/d/a;->q:F

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lmaps/d/a;->u:F

    :cond_0
    iget v0, p0, Lmaps/d/a;->u:F

    return v0
.end method

.method public f()F
    .locals 3

    const/high16 v0, 0x3f800000

    iget-object v1, p0, Lmaps/d/a;->f:Lmaps/d/e;

    invoke-direct {p0, v1}, Lmaps/d/a;->a(Lmaps/d/e;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v0, p0, Lmaps/d/a;->x:F

    const/high16 v1, -0x40800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmaps/d/a;->d()F

    move-result v0

    invoke-virtual {p0}, Lmaps/d/a;->e()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/d/a;->x:F

    :cond_2
    iget v0, p0, Lmaps/d/a;->x:F

    goto :goto_0
.end method

.method public g()F
    .locals 2

    iget-object v0, p0, Lmaps/d/a;->g:Lmaps/d/e;

    invoke-direct {p0, v0}, Lmaps/d/a;->a(Lmaps/d/e;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lmaps/d/a;->H:Z

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/d/a;->w:F

    iget v1, p0, Lmaps/d/a;->v:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3e800000

    mul-float/2addr v0, v1

    iput v0, p0, Lmaps/d/a;->y:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/d/a;->H:Z

    :cond_1
    iget v0, p0, Lmaps/d/a;->y:F

    goto :goto_0
.end method

.method public h()F
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lmaps/d/a;->h:Lmaps/d/e;

    invoke-direct {p0, v1}, Lmaps/d/a;->a(Lmaps/d/e;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-boolean v0, p0, Lmaps/d/a;->I:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    iget-object v3, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    iget-object v4, p0, Lmaps/d/a;->c:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lmaps/d/s;->a(FFFF)F

    move-result v0

    iget-object v1, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget-object v2, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    iget-object v4, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    iget-object v5, p0, Lmaps/d/a;->b:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmaps/d/s;->a(FFFF)F

    move-result v1

    invoke-static {v1, v0}, Lmaps/d/e;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/d/a;->z:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/d/a;->I:Z

    :cond_2
    iget v0, p0, Lmaps/d/a;->z:F

    goto :goto_0
.end method
