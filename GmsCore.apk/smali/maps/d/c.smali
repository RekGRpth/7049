.class public Lmaps/d/c;
.super Lmaps/d/s;


# instance fields
.field private a:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/view/MotionEvent;)V
    .locals 1

    invoke-direct {p0}, Lmaps/d/s;-><init>()V

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/ah/d;->a(Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    return-void
.end method

.method private h()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "Event has been recycled."

    iget-object v1, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-static {v0, v1}, Lmaps/ah/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)F
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    iget-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    invoke-direct {p0}, Lmaps/d/c;->h()V

    iget-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)F
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    iget-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    iget-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    return v0
.end method

.method public c()F
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->m()F

    move-result v0

    return v0
.end method

.method public d()F
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->n()F

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    invoke-direct {p0}, Lmaps/d/c;->h()V

    iget-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/d/c;->a:Landroid/view/MotionEvent;

    return-void
.end method
