.class public abstract Lmaps/d/e;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lmaps/d/w;

.field private b:Z


# direct methods
.method public constructor <init>(Lmaps/d/w;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/ah/d;->a(Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lmaps/d/e;->a:Lmaps/d/w;

    return-void
.end method

.method protected static a(FF)F
    .locals 5

    cmpl-float v0, p1, p0

    if-ltz v0, :cond_0

    sub-float v0, p1, p0

    const-wide v1, 0x401921fb54442d18L

    float-to-double v3, p0

    add-double/2addr v1, v3

    float-to-double v3, p1

    sub-double/2addr v1, v3

    double-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p0}, Lmaps/d/e;->a(FF)F

    move-result v0

    neg-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lmaps/d/f;
.end method

.method public a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)Lmaps/d/f;
    .locals 6

    invoke-virtual {p0}, Lmaps/d/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/d/e;

    invoke-virtual {v0}, Lmaps/d/e;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x1

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lmaps/ah/d;->a(JJ)V

    :cond_2
    sget-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/d/e;->d()Z

    move-result v0

    if-eq p4, v0, :cond_4

    sget-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    goto :goto_0

    :cond_4
    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lmaps/d/e;->a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lmaps/d/f;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x63

    invoke-static {v0, p1}, Lmaps/bh/k;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/d/e;->b:Z

    return v0
.end method

.method public a(Lmaps/d/a;)Z
    .locals 3

    iget-boolean v0, p0, Lmaps/d/e;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture already active: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/d/e;->b(Lmaps/d/a;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/d/e;->b:Z

    iget-boolean v0, p0, Lmaps/d/e;->b:Z

    return v0
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract b(Lmaps/d/a;)Z
.end method

.method public c(Lmaps/d/a;)V
    .locals 3

    iget-boolean v0, p0, Lmaps/d/e;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture already inactive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/d/e;->b:Z

    invoke-virtual {p0, p1}, Lmaps/d/e;->d(Lmaps/d/a;)V

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract d(Lmaps/d/a;)V
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(Lmaps/d/a;)Z
    .locals 3

    iget-boolean v0, p0, Lmaps/d/e;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture is not active: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/d/e;->f(Lmaps/d/a;)Z

    move-result v0

    return v0
.end method

.method protected abstract f(Lmaps/d/a;)Z
.end method
