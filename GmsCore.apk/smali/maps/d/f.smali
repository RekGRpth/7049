.class public final enum Lmaps/d/f;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/d/f;

.field public static final enum b:Lmaps/d/f;

.field public static final enum c:Lmaps/d/f;

.field private static final synthetic d:[Lmaps/d/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/d/f;

    const-string v1, "NO"

    invoke-direct {v0, v1, v2}, Lmaps/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/d/f;->a:Lmaps/d/f;

    new-instance v0, Lmaps/d/f;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v3}, Lmaps/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/d/f;->b:Lmaps/d/f;

    new-instance v0, Lmaps/d/f;

    const-string v1, "YES"

    invoke-direct {v0, v1, v4}, Lmaps/d/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/d/f;->c:Lmaps/d/f;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/d/f;

    sget-object v1, Lmaps/d/f;->a:Lmaps/d/f;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/d/f;->b:Lmaps/d/f;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/d/f;->c:Lmaps/d/f;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/d/f;->d:[Lmaps/d/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/d/f;
    .locals 1

    const-class v0, Lmaps/d/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/d/f;

    return-object v0
.end method

.method public static values()[Lmaps/d/f;
    .locals 1

    sget-object v0, Lmaps/d/f;->d:[Lmaps/d/f;

    invoke-virtual {v0}, [Lmaps/d/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/d/f;

    return-object v0
.end method
