.class public final Lmaps/e/a;
.super Landroid/location/Location;


# instance fields
.field private a:Lmaps/bl/a;

.field private b:Lmaps/t/bb;

.field private c:Z

.field private d:J

.field private e:Lmaps/e/w;

.field private f:Lmaps/e/o;


# direct methods
.method private constructor <init>(Lmaps/e/r;)V
    .locals 2

    invoke-static {p1}, Lmaps/e/r;->a(Lmaps/e/r;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lmaps/e/r;->b(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/e/r;->c(Lmaps/e/r;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setAccuracy(F)V

    :cond_0
    invoke-static {p1}, Lmaps/e/r;->d(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lmaps/e/r;->e(Lmaps/e/r;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    :cond_1
    invoke-static {p1}, Lmaps/e/r;->f(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lmaps/e/r;->g(Lmaps/e/r;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setBearing(F)V

    :cond_2
    invoke-static {p1}, Lmaps/e/r;->h(Lmaps/e/r;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    invoke-static {p1}, Lmaps/e/r;->i(Lmaps/e/r;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    invoke-static {p1}, Lmaps/e/r;->j(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lmaps/e/r;->k(Lmaps/e/r;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setSpeed(F)V

    :cond_3
    invoke-static {p1}, Lmaps/e/r;->l(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lmaps/e/r;->m(Lmaps/e/r;)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setTime(J)V

    :cond_4
    invoke-static {p1}, Lmaps/e/r;->l(Lmaps/e/r;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/a;->c:Z

    invoke-static {p1}, Lmaps/e/r;->n(Lmaps/e/r;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lmaps/e/r;->o(Lmaps/e/r;)J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lmaps/e/a;->d:J

    invoke-static {p1}, Lmaps/e/r;->p(Lmaps/e/r;)Landroid/os/Bundle;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-static {p1}, Lmaps/e/r;->q(Lmaps/e/r;)Lmaps/bl/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/a;->a:Lmaps/bl/a;

    invoke-static {p1}, Lmaps/e/r;->r(Lmaps/e/r;)Lmaps/t/bb;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/a;->b:Lmaps/t/bb;

    invoke-static {p1}, Lmaps/e/r;->s(Lmaps/e/r;)Lmaps/e/w;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/a;->e:Lmaps/e/w;

    invoke-static {p1}, Lmaps/e/r;->t(Lmaps/e/r;)Lmaps/e/o;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/a;->f:Lmaps/e/o;

    return-void

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lmaps/e/r;Lmaps/e/q;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/e/a;-><init>(Lmaps/e/r;)V

    return-void
.end method

.method public static a(Lmaps/e/a;)Lmaps/bl/a;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZDZD)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    cmpl-double v2, p2, p5

    if-nez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p4, :cond_0

    move v1, v0

    goto :goto_0
.end method

.method private a(ZJZJ)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    cmp-long v2, p2, p5

    if-nez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p4, :cond_0

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/e/a;)J
    .locals 2

    iget-wide v0, p0, Lmaps/e/a;->d:J

    return-wide v0
.end method

.method static synthetic c(Lmaps/e/a;)Lmaps/e/w;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->e:Lmaps/e/w;

    return-object v0
.end method

.method static synthetic d(Lmaps/e/a;)Lmaps/e/o;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->f:Lmaps/e/o;

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/bl/a;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->a:Lmaps/bl/a;

    return-object v0
.end method

.method public b()Lmaps/t/bb;
    .locals 1

    iget-object v0, p0, Lmaps/e/a;->b:Lmaps/t/bb;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/a;->c:Z

    return v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lmaps/e/a;->d:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    instance-of v0, p1, Lmaps/e/a;

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lmaps/e/a;

    invoke-virtual {p1}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lmaps/e/a;->b()Lmaps/t/bb;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/a;->b()Lmaps/t/bb;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/e/a;->hasAccuracy()Z

    move-result v1

    invoke-virtual {p1}, Lmaps/e/a;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lmaps/e/a;->hasAccuracy()Z

    move-result v4

    invoke-virtual {p0}, Lmaps/e/a;->getAccuracy()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v7

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lmaps/e/a;->hasAltitude()Z

    move-result v1

    invoke-virtual {p1}, Lmaps/e/a;->getAltitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lmaps/e/a;->hasAltitude()Z

    move-result v4

    invoke-virtual {p0}, Lmaps/e/a;->getAltitude()D

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v7

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lmaps/e/a;->hasBearing()Z

    move-result v1

    invoke-virtual {p1}, Lmaps/e/a;->getBearing()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lmaps/e/a;->hasBearing()Z

    move-result v4

    invoke-virtual {p0}, Lmaps/e/a;->getBearing()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v7

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lmaps/e/a;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/a;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v7

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lmaps/e/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lmaps/e/a;->getLatitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v7

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lmaps/e/a;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lmaps/e/a;->getLongitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v7

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lmaps/e/a;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/a;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v7

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Lmaps/e/a;->hasSpeed()Z

    move-result v1

    invoke-virtual {p1}, Lmaps/e/a;->getSpeed()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lmaps/e/a;->hasSpeed()Z

    move-result v4

    invoke-virtual {p0}, Lmaps/e/a;->getSpeed()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v7

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, Lmaps/e/a;->c()Z

    move-result v1

    invoke-virtual {p1}, Lmaps/e/a;->getTime()J

    move-result-wide v2

    invoke-virtual {p0}, Lmaps/e/a;->c()Z

    move-result v4

    invoke-virtual {p0}, Lmaps/e/a;->getTime()J

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmaps/e/a;->a(ZJZJ)Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v7

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lmaps/e/a;->d()J

    move-result-wide v0

    invoke-virtual {p0}, Lmaps/e/a;->d()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    move v0, v7

    goto/16 :goto_0

    :cond_c
    iget-object v0, p1, Lmaps/e/a;->e:Lmaps/e/w;

    iget-object v1, p0, Lmaps/e/a;->e:Lmaps/e/w;

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v7

    goto/16 :goto_0

    :cond_d
    iget-object v0, p1, Lmaps/e/a;->f:Lmaps/e/o;

    iget-object v1, p0, Lmaps/e/a;->f:Lmaps/e/o;

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v7

    goto/16 :goto_0

    :cond_e
    move v0, v8

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/e/a;->a:Lmaps/bl/a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/e/a;->b:Lmaps/t/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lmaps/e/a;->getProvider()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lmaps/e/a;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lmaps/e/a;->e:Lmaps/e/w;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lmaps/e/a;->f:Lmaps/e/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/ap/e;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->getTime()J

    move-result-wide v1

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->d()J

    move-result-wide v1

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->getBearing()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->getAltitude()D

    move-result-wide v1

    double-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lmaps/e/a;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public setAccuracy(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAltitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBearing(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLatitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLongitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setSpeed(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTime(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    const-string v0, "GmmLocation["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "source = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/e/a;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", point = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lmaps/e/a;->a:Lmaps/bl/a;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", accuracy = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lmaps/e/a;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmaps/e/a;->getAccuracy()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", speed = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lmaps/e/a;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmaps/e/a;->getSpeed()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m/s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", bearing = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lmaps/e/a;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmaps/e/a;->getBearing()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " degrees"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", time = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, Lmaps/e/a;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", relativetime = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, Lmaps/e/a;->d()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", level = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lmaps/e/a;->b:Lmaps/t/bb;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/e/a;->b:Lmaps/t/bb;

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lmaps/e/a;->e:Lmaps/e/w;

    if-eqz v0, :cond_0

    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRoad = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->e:Lmaps/e/w;

    iget-boolean v2, v2, Lmaps/e/w;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRteCon = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->e:Lmaps/e/w;

    iget-wide v2, v2, Lmaps/e/w;->e:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isProjected = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->e:Lmaps/e/w;

    iget-boolean v2, v2, Lmaps/e/w;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lmaps/e/a;->f:Lmaps/e/o;

    if-eqz v0, :cond_1

    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGps = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->f:Lmaps/e/o;

    iget-boolean v2, v2, Lmaps/e/o;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGpsAccurate = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->f:Lmaps/e/o;

    iget-boolean v2, v2, Lmaps/e/o;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", numSatInFix = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmaps/e/a;->f:Lmaps/e/o;

    iget v2, v2, Lmaps/e/o;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v0, "n/a"

    goto/16 :goto_0

    :cond_3
    const-string v0, "n/a"

    goto/16 :goto_1

    :cond_4
    const-string v0, "n/a"

    goto/16 :goto_2

    :cond_5
    const-string v0, "n/a"

    goto/16 :goto_3
.end method
