.class public Lmaps/e/f;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/e/k;
.implements Lmaps/e/v;


# static fields
.field private static volatile m:Lmaps/e/f;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field c:Z

.field d:Z

.field private final e:Lmaps/e/c;

.field private volatile f:Ljava/util/Map;

.field private g:D

.field private h:Lmaps/e/a;

.field private i:Lmaps/e/a;

.field private final j:Lmaps/e/ad;

.field private final k:Lmaps/ae/d;

.field private l:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;Lmaps/ae/d;Landroid/content/Context;Lmaps/e/ad;)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/e/t;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmaps/e/t;-><init>(Lmaps/e/j;)V

    iput-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-static {}, Lmaps/f/cs;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/f;->f:Ljava/util/Map;

    const-wide/high16 v0, -0x3c20000000000000L

    iput-wide v0, p0, Lmaps/e/f;->g:D

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/e/f;->b:Ljava/util/List;

    iput-boolean v2, p0, Lmaps/e/f;->c:Z

    iput-boolean v2, p0, Lmaps/e/f;->d:Z

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/f;->l:Z

    iput-object p5, p0, Lmaps/e/f;->j:Lmaps/e/ad;

    iput-object p3, p0, Lmaps/e/f;->k:Lmaps/ae/d;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->l:Z

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/cv/a;

    invoke-virtual {p0}, Lmaps/e/f;->b()Lmaps/ae/d;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-direct {v0, v1, v2, v3}, Lmaps/cv/a;-><init>(Lmaps/ae/d;J)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-static {p2}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-direct {p0}, Lmaps/e/f;->h()V

    :cond_2
    invoke-direct {p0, p4}, Lmaps/e/f;->a(Landroid/content/Context;)V

    invoke-direct {p0, p4}, Lmaps/e/f;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static a(Lmaps/bl/a;)Lmaps/bl/a;
    .locals 1

    invoke-static {p0}, Lmaps/e/g;->d(Lmaps/bl/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/e/g;->e()Lmaps/e/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmaps/e/g;->c(Lmaps/bl/a;)Lmaps/bl/a;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a()Lmaps/e/v;
    .locals 1

    sget-object v0, Lmaps/e/f;->m:Lmaps/e/f;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lmaps/e/j;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p1}, Lmaps/e/j;-><init>(Lmaps/e/f;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-static {p1, v0}, Lmaps/e/x;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private a(Lmaps/e/a;)V
    .locals 3

    if-eqz p1, :cond_1

    iput-object p1, p0, Lmaps/e/f;->h:Lmaps/e/a;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/ae;

    iget-object v2, p0, Lmaps/e/f;->h:Lmaps/e/a;

    invoke-interface {v0, v2}, Lmaps/e/ae;->a(Lmaps/e/a;)Lmaps/e/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/f;->h:Lmaps/e/a;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/e/f;->h:Lmaps/e/a;

    iput-object v0, p0, Lmaps/e/f;->i:Lmaps/e/a;

    iget-object v0, p0, Lmaps/e/f;->h:Lmaps/e/a;

    invoke-virtual {v0}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v0

    invoke-static {v0}, Lmaps/e/f;->a(Lmaps/bl/a;)Lmaps/bl/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/f;->h:Lmaps/e/a;

    invoke-virtual {v1}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/bl/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lmaps/e/r;

    invoke-direct {v1}, Lmaps/e/r;-><init>()V

    iget-object v2, p0, Lmaps/e/f;->h:Lmaps/e/a;

    invoke-virtual {v1, v2}, Lmaps/e/r;->a(Landroid/location/Location;)Lmaps/e/r;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/e/r;->a(Lmaps/bl/a;)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/f;->i:Lmaps/e/a;

    :cond_1
    return-void
.end method

.method public static a(Lmaps/e/f;)V
    .locals 0

    sput-object p0, Lmaps/e/f;->m:Lmaps/e/f;

    return-void
.end method

.method static synthetic a(Lmaps/e/f;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/e/f;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lmaps/e/aa;)Z
    .locals 1

    invoke-virtual {p0}, Lmaps/e/f;->f()Lmaps/e/aa;

    move-result-object v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lmaps/e/aa;->n()Lmaps/e/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/e/f;->a(Lmaps/e/a;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static a(Lmaps/e/aa;Lmaps/e/aa;)Z
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_4

    sget-boolean v2, Lmaps/ae/h;->y:Z

    if-eqz v2, :cond_2

    invoke-interface {p0}, Lmaps/e/aa;->l()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Lmaps/e/aa;->l()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, Lmaps/e/aa;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-interface {p0}, Lmaps/e/aa;->n()Lmaps/e/a;

    move-result-object v2

    invoke-interface {p1}, Lmaps/e/aa;->n()Lmaps/e/a;

    move-result-object v3

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lmaps/e/a;->getTime()J

    move-result-wide v4

    invoke-virtual {v3}, Lmaps/e/a;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x2af8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    invoke-virtual {v2}, Lmaps/e/a;->hasAccuracy()Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {v3}, Lmaps/e/a;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lmaps/e/a;->getAccuracy()F

    move-result v2

    invoke-virtual {v3}, Lmaps/e/a;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private declared-synchronized b(Landroid/content/Context;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lmaps/e/f;->l:Z

    invoke-static {p1}, Lmaps/e/x;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/e/f;->l:Z

    iget-boolean v0, p0, Lmaps/e/f;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_2

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-direct {p0}, Lmaps/e/f;->j()V

    iget-boolean v0, p0, Lmaps/e/f;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/f;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/e/f;->i()V

    iget-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, Lmaps/e/c;->a(Lmaps/bl/a;Lmaps/e/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/aa;

    invoke-interface {v0, p0}, Lmaps/e/aa;->a(Lmaps/e/k;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/f;->h:Lmaps/e/a;

    iput-object v0, p0, Lmaps/e/f;->i:Lmaps/e/a;

    return-void
.end method

.method private j()V
    .locals 3

    iget-boolean v0, p0, Lmaps/e/f;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/f;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/f;->d:Z

    iget-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/aa;

    iget-object v2, p0, Lmaps/e/f;->j:Lmaps/e/ad;

    invoke-interface {v2, v0}, Lmaps/e/ad;->b(Lmaps/e/aa;)V

    invoke-interface {v0}, Lmaps/e/aa;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/aa;

    invoke-interface {v0}, Lmaps/e/aa;->b()V

    iget-object v2, p0, Lmaps/e/f;->j:Lmaps/e/ad;

    invoke-interface {v2, v0}, Lmaps/e/ad;->a(Lmaps/e/aa;)V

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public a(ILmaps/e/aa;)V
    .locals 1

    invoke-virtual {p0}, Lmaps/e/f;->f()Lmaps/e/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    invoke-interface {v0, p1, p0}, Lmaps/e/c;->a(ILmaps/e/f;)V

    goto :goto_0
.end method

.method public a(Lmaps/bl/a;Lmaps/e/aa;)V
    .locals 1

    invoke-direct {p0, p2}, Lmaps/e/f;->a(Lmaps/e/aa;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    invoke-interface {v0, p1, p0}, Lmaps/e/c;->a(Lmaps/bl/a;Lmaps/e/f;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/e/z;)V
    .locals 3

    iget-boolean v0, p0, Lmaps/e/f;->d:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLP Observer is being added, but GLP is already started:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    invoke-interface {v0, p1}, Lmaps/e/c;->a(Lmaps/e/z;)V

    return-void
.end method

.method public b()Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/e/f;->k:Lmaps/ae/d;

    return-object v0
.end method

.method public b(Lmaps/e/z;)V
    .locals 1

    iget-object v0, p0, Lmaps/e/f;->e:Lmaps/e/c;

    invoke-interface {v0, p1}, Lmaps/e/c;->b(Lmaps/e/z;)V

    return-void
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/e/f;->f()Lmaps/e/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/e/aa;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/f;->c:Z

    invoke-direct {p0}, Lmaps/e/f;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/f;->c:Z

    invoke-direct {p0}, Lmaps/e/f;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected f()Lmaps/e/aa;
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lmaps/e/f;->l:Z

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lmaps/e/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/aa;

    invoke-static {v0, v1}, Lmaps/e/f;->a(Lmaps/e/aa;Lmaps/e/aa;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public g()Lmaps/e/a;
    .locals 1

    iget-object v0, p0, Lmaps/e/f;->i:Lmaps/e/a;

    return-object v0
.end method

.method public declared-synchronized run()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/e/f;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
